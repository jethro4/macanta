import useValue from '@macanta/hooks/base/useValue';
import {getSectionsData} from '@macanta/selectors/field.selector';
import useAllFields from '@macanta/hooks/dataObject/useAllFields';

const useSections = ({groupId, ...args}, options) => {
  const fieldsQuery = useAllFields({
    groupId,
    subheaders: false,
    options,
    ...args,
  });

  const fields = fieldsQuery.data;

  const [data] = useValue(() => getSectionsData(fields), [fields]);

  return {...fieldsQuery, data};
};

export default useSections;
