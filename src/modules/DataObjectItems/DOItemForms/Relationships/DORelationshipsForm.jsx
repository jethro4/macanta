import React from 'react';

import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import useDOToDORelationships, {
  transformItem,
} from '@macanta/hooks/relationship/useDOToDORelationships';
import DODirectRelationshipsForm from '@macanta/modules/DataObjectItems/DOItemForms/Relationships/DODirectRelationshipsForm';
import DOIndirectRelationshipsForm from '@macanta/modules/DataObjectItems/DOItemForms/Relationships/DOIndirectRelationshipsForm';
import {arrayConcatOrUpdateByKey, remove} from '@macanta/utils/array';
import useValue from '@macanta/hooks/base/useValue';

const DORelationshipsForm = ({
  data,
  deletedData,
  doItemId,
  groupId,
  onChange,
  options,
}) => {
  const [deletedItems, setDeletedItems] = useValue(deletedData);

  const directDORelationshipsQuery = useDOToDORelationships(
    {
      doItemId,
      groupId,
      type: 'direct',
      otherData: data,
      deletedData,
    },
    options,
  );

  const handleConnect = (items) => {
    const filteredItems = items.filter(
      (d) =>
        !directDORelationshipsQuery.result?.some(
          (r) => r.doItemId === d.doItemId,
        ),
    );

    const updatedItems = data.concat(
      filteredItems.map((item) => {
        return {
          doItemId: item.doItemId,
          groupId: item.groupId,
          data: item.data,
        };
      }),
    );

    directDORelationshipsQuery.setAllData((state) => {
      return state.concat(
        items.map((d) =>
          transformItem({
            groupId: d.groupId,
            itemIdA: doItemId,
            itemIdB: d.doItemId,
            type: 'direct',
            data: d.data,
          }),
        ),
      );
    });

    onChange({
      doItemId,
      groupId,
      data: updatedItems,
    });

    const updatedDeletedItems = deletedItems.filter(
      (r) => !items.some((d) => r.itemIdB === d.doItemId),
    );

    setDeletedItems(updatedDeletedItems);

    onChange({
      isDelete: true,
      data: updatedDeletedItems,
    });
  };

  const handleDelete = (deletedItem) => {
    const updatedItems = remove({
      arr: directDORelationshipsQuery.allData,
      key: 'doItemId',
      value: deletedItem.doItemId,
    });

    directDORelationshipsQuery.setAllData(updatedItems);

    onChange({
      doItemId,
      groupId,
      data: remove({
        arr: data,
        key: 'doItemId',
        value: deletedItem.doItemId,
      }),
    });

    if (
      directDORelationshipsQuery.result?.some(
        (r) => r.doItemId === deletedItem.doItemId,
      )
    ) {
      const updatedDeletedItems = arrayConcatOrUpdateByKey({
        arr: deletedItems,
        item: {
          itemIdA: doItemId,
          itemIdB: deletedItem.doItemId,
          groupId: deletedItem.groupId,
        },
        key: 'itemIdB',
      });

      setDeletedItems(updatedDeletedItems);

      onChange({
        isDelete: true,
        data: updatedDeletedItems,
      });
    }
  };

  return (
    <NoteTaskHistoryStyled.FullHeightGrid container>
      <NoteTaskHistoryStyled.FullHeightFlexGridColumn item xs={12} md={6.5}>
        <DODirectRelationshipsForm
          loading={directDORelationshipsQuery.loading}
          filteredDirectItems={directDORelationshipsQuery.allData}
          groupedData={directDORelationshipsQuery.groupedData}
          doItemId={doItemId}
          onConnect={handleConnect}
          onDelete={handleDelete}
        />
      </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
      <NoteTaskHistoryStyled.FullHeightFlexGridColumn item xs={12} md={5.5}>
        <DOIndirectRelationshipsForm
          loading={directDORelationshipsQuery.loading}
          doItemId={doItemId}
          filteredDirectItems={directDORelationshipsQuery.allData}
          // origDirectItems={directDORelationshipsQuery.result}
          options={options}
        />
      </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
    </NoteTaskHistoryStyled.FullHeightGrid>
  );
};

DORelationshipsForm.defaultProps = {
  data: [],
  deletedData: [],
};

export default DORelationshipsForm;
