import {experimentalStyled as styled} from '@mui/material/styles';

import Box from '@mui/material/Box';
import SearchIconComp from '@mui/icons-material/Search';
import TextField from '@macanta/components/Forms';
import Button, {IconButton} from '@macanta/components/Button';

export const Root = styled(Box)`
  display: flex;
`;

export const SearchSelect = styled(TextField)`
  ${({theme}) => theme.breakpoints.up('sm')} {
    min-width: 120px;
  }
`;

export const SearchTextField = styled(TextField)`
  width: 100%;
`;

export const ActionButtonsContainer = styled(Box)`
  position: absolute;
  display: flex;
  align-items: center;
  pointer-events: auto;
  top: 0;
  bottom: 0;
  right: 4px;
  justify-content: flex-end;
`;

export const ActionButton = styled(Button)`
  border-radius: 0;
  box-shadow: none;
`;

export const SearchIconButton = styled(IconButton)`
  height: 36px;
  width: 36px;
`;

export const SearchIcon = styled(SearchIconComp)``;
