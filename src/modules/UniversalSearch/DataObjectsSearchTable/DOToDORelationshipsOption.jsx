import React, {useState, useEffect} from 'react';
import isEqual from 'lodash/isEqual';
import Box from '@mui/material/Box';
import Modal from '@macanta/components/Modal';
import Button from '@macanta/components/Button';
import TurnedInIcon from '@mui/icons-material/TurnedIn';
import DORelationshipsForm from '@macanta/modules/DataObjectItems/DOItemForms/Relationships/DORelationshipsForm';
import * as FormStyled from '@macanta/modules/NoteTaskForms/styles';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';

const initValues = {
  directDORelationships: {
    data: [],
  },
  deletedDirectDORelationships: {
    data: [],
  },
};

const DOToDORelationshipsOption = ({
  doItemId,
  groupId,
  modal,
  showModal: showModalProp = false,
  onClose,
  onSave,
}) => {
  const [changes, setChanges] = useState(initValues);
  const [showModal, setShowModal] = useState(showModalProp);

  const handleClose = () => {
    setShowModal(false);

    onClose && onClose();
  };

  const handleValueChange = (formData) => {
    if (!formData.isDelete) {
      setChanges((state) => ({
        ...state,
        directDORelationships: {
          ...formData,
          data: formData.data,
        },
      }));
    } else {
      setChanges((state) => ({
        ...state,
        deletedDirectDORelationships: {
          data: formData.data,
        },
      }));
    }
  };

  const handleSubmit = async () => {
    await onSave(changes);

    setChanges(initValues);
  };

  useEffect(() => {
    setShowModal(showModalProp);
  }, [showModalProp]);

  let doRelationships = (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
      }}>
      <Box
        sx={{
          flex: 1,
          padding: '1rem 0',
          backgroundColor: 'grayLight.main',
        }}>
        <DORelationshipsForm
          data={changes.directDORelationships.data}
          deletedData={changes.deletedDirectDORelationships.data}
          doItemId={doItemId}
          groupId={groupId}
          onChange={handleValueChange}
          options={{
            fetchPolicy: FETCH_POLICIES.CACHE_AND_NETWORK,
          }}
        />
      </Box>

      <FormStyled.Footer
        style={{
          padding: '1rem',
          margin: 0,
          borderTop: '1px solid #eee',
          width: '100%',
          // backgroundColor: '#f5f6fa',
        }}>
        <Button
          disabled={isEqual(initValues, changes)}
          variant="contained"
          startIcon={<TurnedInIcon />}
          onClick={handleSubmit}
          size="medium">
          Save Changes
        </Button>
      </FormStyled.Footer>
    </Box>
  );

  if (modal) {
    doRelationships = (
      <Modal
        headerTitle={doItemId}
        open={showModal}
        onClose={handleClose}
        contentWidth={1000}
        contentHeight={'90%'}>
        {doRelationships}
      </Modal>
    );
  }

  return doRelationships;
};

export default DOToDORelationshipsOption;
