import {LIST_DO_TO_DO_RELATIONSHIPS} from '@macanta/graphql/relationships';
import useSessionQuery from '@macanta/hooks/apollo/useSessionQuery';
import useValue from '@macanta/hooks/base/useValue';
import {
  groupBy,
  sortArray,
  sortArrayByKeyValuesIncludeAll,
  sortArrayByObjectKey,
  sortArrayByPriority,
  uniqByKeys,
} from '@macanta/utils/array';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import useAppSettings from '@macanta/hooks/admin/useAppSettings';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import useDOFields from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOFields';

export const transformId = ({groupId, itemIdA, itemIdB, type}) => {
  const itemIds = sortArray([itemIdA, itemIdB]);

  return [groupId, type, ...itemIds].join('-');
};

export const transformItem = ({groupId, itemIdA, itemIdB, type, data = []}) => {
  return {
    id: transformId({groupId, itemIdA, itemIdB, type}),
    doItemId: itemIdB,
    groupId,
    type,
    data,
  };
};

const useDOToDORelationships = (
  {doItemId, groupId, type = 'direct', otherData, deletedData},
  options = {},
) => {
  const appSettingsQuery = useAppSettings({
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });
  const doTypesQuery = useDOTypes({
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });
  const doFieldsQuery = useDOFields(groupId, {
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });
  const doRelationshipsQuery = useSessionQuery(LIST_DO_TO_DO_RELATIONSHIPS, {
    ...options,
    dependencies: [doItemId],
    variables: {
      doItemId,
      groupId,
      type,
    },
    skip: !doItemId || options?.skip,
  });

  const macantaTabOrder = appSettingsQuery.data?.macantaTabOrder;
  const doTypes = doTypesQuery.data?.listDataObjectTypes;
  const doFields = doFieldsQuery.data?.listDataObjectFields;
  const data = doRelationshipsQuery.result;

  const [allData, setAllData] = useValue(() => {
    const updatedData = [...(data || []), ...(otherData || [])].filter(
      (d) => !deletedData?.some((r) => r.itemIdB === d.doItemId),
    );
    const uniqData = uniqByKeys(updatedData, ['doItemId', 'groupId']);
    const filteredData = uniqData.filter((d) => d.doItemId !== doItemId);
    const transformedData = filteredData.map((d) =>
      transformItem({
        groupId: d.groupId,
        itemIdA: doItemId,
        itemIdB: d.doItemId,
        type: 'direct',
        data: d.data,
      }),
    );

    return transformedData;
  }, [data, otherData, deletedData]);

  const [sortedFieldsData] = useValue(() => {
    if (doFields) {
      const displayFields = sortArrayByObjectKey(
        doFields.filter((field) => field.showInTable),
        'showOrder',
      );

      return allData.map((d) => ({
        ...d,
        data: sortArrayByPriority(
          d.data,
          'fieldName',
          displayFields.map((field) => field.name),
        ),
      }));
    }

    return allData;
  }, [doFields, allData]);

  const [groupedData] = useValue(() => {
    const groupedResult = groupBy(sortedFieldsData, ['type', 'groupId']);
    const result = groupedResult.find((d) => d.type === type) || {
      items: [],
    };

    const sortedResult =
      !macantaTabOrder || !doTypes
        ? result.items
        : sortArrayByKeyValuesIncludeAll(
            result.items,
            'groupId',
            macantaTabOrder
              .map((tab) => doTypes.find((d) => d.title === tab)?.id)
              .filter((tabId) => !!tabId),
          );

    return sortedResult;
  }, [sortedFieldsData, macantaTabOrder, doTypes]);

  return {
    ...doRelationshipsQuery,
    allData: sortedFieldsData,
    setAllData,
    groupedData,
  };
};

export default useDOToDORelationships;
