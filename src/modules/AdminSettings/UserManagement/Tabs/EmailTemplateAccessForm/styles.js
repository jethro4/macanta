import {experimentalStyled as styled} from '@mui/material/styles';
import Box from '@mui/material/Box';
import * as applicationStyles from '@macanta/themes/applicationStyles';

export const Root = styled(Box)`
  ${applicationStyles.absoluteFill}
`;
