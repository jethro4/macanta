import {useEffect, useRef} from 'react';

const useNextEffect = (callback, dependencies) => {
  const hasInitializedRef = useRef(false);

  useEffect(() => {
    if (!hasInitializedRef.current) {
      hasInitializedRef.current = true;
      return;
    }

    callback();
  }, dependencies);
};

export default useNextEffect;
