import React, {useState} from 'react';
import {useFormikContext} from 'formik';
import TurnedInIcon from '@mui/icons-material/TurnedIn';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import Button from '@macanta/components/Button';
import Switch from '@macanta/components/Forms/ChoiceFields/Switch';
import useAllFields from '@macanta/hooks/dataObject/useAllFields';
import useEventResize from '@macanta/hooks/base/useEventResize';
import {arrayConcatOrUpdateByKey} from '@macanta/utils/array';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {
  ALL_MIME_TYPE_CATEGORIES_WITH_ALL,
  UPLOAD_TYPES,
} from '@macanta/constants/form';
import IntegrationDisplayFieldsSelect from './IntegrationDisplayFieldsSelect';
import {
  validateLabelExists,
  getCategories,
  getDerivedCategory,
} from './helpers';
import * as Styled from './styles';

const FORM_TYPES = ['Select', 'Radio'];
const ORDER = [
  {
    label: 'Ascending',
    value: 'ASC',
  },
  {
    label: 'Descending',
    value: 'DESC',
  },
];

const FieldSettingsForm = ({item: itemProp, onClose}) => {
  const {values, setValues} = useFormikContext();

  const isAlwaysRequired = ['field_email', 'field_integration'].includes(
    itemProp.fieldId,
  );
  const isIntegration = itemProp.fieldId === 'field_integration';
  const isUpload = itemProp.type === 'file';

  const [item, setItem] = useState({
    fieldId: itemProp.fieldId,
    label: itemProp.label || '',
    tooltip: itemProp.tooltip || '',
    required: isAlwaysRequired || !!itemProp.required,
    ...(isIntegration && {
      limit: values.integrationField.limit || 5,
      type: values.integrationField.type || 'Select',
      displayFields: values.integrationField.displayFields || '',
      order: values.integrationField.order || 'ASC',
      orderBy: values.integrationField.orderBy,
    }),
    ...(isUpload && {
      referenceFieldId: itemProp.referenceFieldId,
      multiple: itemProp.multiple,
      mimeTypes: itemProp.mimeTypes,
    }),
  });

  const doFieldsQuery = useAllFields({
    groupId: values.doType,
    includeNone: !isUpload,
    isDataObjectType: true,
    includeAllTypes: true,
    options: {
      fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
    },
  });

  const doFields = doFieldsQuery.data;
  const uploadDetails = {
    uploadFieldType: itemProp.referenceFieldId ? 'fieldSpecific' : 'regular',
    ...(isUpload && !itemProp.referenceFieldId
      ? {
          uploadType: !itemProp.multiple ? 'single' : 'multiple',
          category: getDerivedCategory(itemProp.mimeTypes),
        }
      : {
          referenceFieldName: doFields.find(
            (field) => field.id === itemProp.referenceFieldId,
          )?.name,
        }),
  };
  const labelAlreadyUsed = validateLabelExists({
    fieldId: item.fieldId,
    label: item.label,
    groups: [...values.contactGroups, ...values.doGroups],
  });
  const requiredDisplayFields = isIntegration && !item.displayFields.length;

  const handleChange = (key) => (value) => {
    setItem((state) => ({...state, [key]: value}));
  };

  const handleAdd = () => {
    setValues((state) => {
      return {
        ...state,
        ...(isIntegration && {
          integrationField: {
            ...state.integrationField,
            displayFields: item.displayFields,
            limit: item.limit,
            type: item.type || 'Select',
            order: item.order || 'ASC',
            orderBy: item.orderBy,
          },
        }),
        contactGroups: state.contactGroups.map((group) => {
          if (group.fields.some((field) => field.fieldId === item.fieldId)) {
            return {
              ...group,
              fields: arrayConcatOrUpdateByKey({
                arr: group.fields,
                item: {
                  ...itemProp,
                  ...item,
                },
                key: 'fieldId',
              }),
            };
          }

          return group;
        }),
        doGroups: state.doGroups.map((group) => {
          if (group.fields.some((field) => field.fieldId === item.fieldId)) {
            return {
              ...group,
              fields: arrayConcatOrUpdateByKey({
                arr: group.fields,
                item: {
                  ...itemProp,
                  ...item,
                },
                key: 'fieldId',
              }),
            };
          }

          return group;
        }),
      };
    });

    onClose();
  };

  useEventResize([!!item.orderBy, labelAlreadyUsed]);

  return (
    <>
      <Box
        style={{
          padding: '1rem',
          display: 'flex',
          flexDirection: 'column',
          overflowY: 'auto',
        }}>
        {isIntegration && (
          <>
            <Typography
              sx={{
                color: 'text.blue',
              }}
              style={{
                fontSize: 15,
                lineHeight: '15px',
                fontWeight: 'bold',
              }}>
              Integration Field
            </Typography>
            <Divider
              style={{
                margin: '16px 0',
                borderBottom: '1px solid #eee',
              }}
            />

            <Styled.SettingsField
              row
              type="Radio"
              options={FORM_TYPES}
              labelPosition="normal"
              size="small"
              value={item.type}
              onChange={handleChange('type')}
              variant="outlined"
            />
            <Styled.SettingsField
              type="Number"
              labelPosition="normal"
              size="small"
              value={item.limit}
              onChange={handleChange('limit')}
              label="Limit"
              variant="outlined"
            />
            <Styled.SettingsField
              type="Select"
              options={doFields}
              labelPosition="normal"
              size="small"
              value={item.orderBy}
              onChange={handleChange('orderBy')}
              label="Order By"
              variant="outlined"
            />
            {item.orderBy && (
              <Styled.SettingsField
                type="Select"
                options={ORDER}
                labelPosition="normal"
                size="small"
                value={item.order}
                onChange={handleChange('order')}
                label="Order"
                variant="outlined"
              />
            )}
            <Divider
              style={{
                marginBottom: '1.5rem',
                borderBottom: '1px solid #eee',
              }}
            />
          </>
        )}
        {isUpload && (
          <>
            <Typography
              sx={{
                color: 'text.blue',
              }}
              style={{
                fontSize: 15,
                lineHeight: '15px',
                fontWeight: 'bold',
              }}>
              Upload Type:
              <Typography
                component="span"
                sx={{
                  color: 'text.primary',
                }}
                style={{
                  marginLeft: 5,
                  fontSize: 'inherit',
                  lineHeight: 'inherit',
                }}>
                {uploadDetails.uploadFieldType === 'regular'
                  ? 'Regular'
                  : `Field (${uploadDetails.referenceFieldName})`}
              </Typography>
            </Typography>

            <Divider
              style={{
                margin: '16px 0',
                borderBottom: '1px solid #eee',
              }}
            />

            {uploadDetails.uploadFieldType === 'regular' && (
              <>
                <Styled.SettingsField
                  row
                  type="Radio"
                  options={UPLOAD_TYPES}
                  labelPosition="normal"
                  size="small"
                  value={uploadDetails.uploadType}
                  onChange={(val) =>
                    handleChange('multiple')(val === 'multiple')
                  }
                  variant="outlined"
                />
                <Styled.SettingsField
                  labelPosition="normal"
                  type="Select"
                  size="small"
                  options={ALL_MIME_TYPE_CATEGORIES_WITH_ALL}
                  value={uploadDetails.category}
                  onChange={(val) =>
                    handleChange('mimeTypes')(getCategories(val))
                  }
                  label="Mime Types"
                  variant="outlined"
                />
                <Divider
                  style={{
                    marginBottom: '1.5rem',
                    borderBottom: '1px solid #eee',
                  }}
                />
              </>
            )}
          </>
        )}
        <Styled.SettingsField
          required
          labelPosition="normal"
          size="small"
          value={item.label}
          error={labelAlreadyUsed}
          onChange={handleChange('label')}
          label="Label"
          variant="outlined"
        />
        {isIntegration && (
          <IntegrationDisplayFieldsSelect
            groupId={values.doType}
            item={values.integrationField}
            value={item.displayFields}
            onChange={handleChange('displayFields')}
          />
        )}
        <Styled.SettingsField
          style={{
            marginBottom: 8,
          }}
          labelPosition="normal"
          size="small"
          value={item.tooltip}
          onChange={handleChange('tooltip')}
          label="Tooltip"
          variant="outlined"
        />

        <Box
          style={{
            display: 'flex',
            alignItems: 'center',
            alignSelf: 'flex-end',
          }}>
          <Switch
            label="Required"
            labelPlacement="start"
            disabled={isAlwaysRequired}
            checked={item.required}
            onChange={(event) => handleChange('required')(event.target.checked)}
          />
        </Box>
      </Box>
      <Box
        style={{
          display: 'flex',
          justifyContent: 'flex-end',
          padding: '12px 1rem',
          borderTop: '1px solid #eee',
        }}>
        <Button
          disabled={requiredDisplayFields || labelAlreadyUsed || !item.label}
          variant="contained"
          startIcon={<TurnedInIcon />}
          onClick={handleAdd}
          size="small">
          Save Field
        </Button>
      </Box>
    </>
  );
};

FieldSettingsForm.defaultProps = {
  subGroups: [],
  item: {
    fields: [],
    columns: 3,
  },
};

export default FieldSettingsForm;
