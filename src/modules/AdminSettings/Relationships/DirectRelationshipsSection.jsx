import React from 'react';
import Section from '@macanta/containers/Section';
import useQuery from '@macanta/hooks/apollo/useQuery';
import {LIST_DIRECT_CONTACT_RELATIONSHIPS} from '@macanta/graphql/contacts';
import {sortArrayByObjectKey} from '@macanta/utils/array';
import ContactRelationshipsTable from './ContactRelationshipsTable';
import AddOrEditDirectRelationshipOptionBtn from './AddOrEditDirectRelationshipOptionBtn';

const DirectRelationshipsSection = () => {
  const allDirectContactRelationshipsQuery = useQuery(
    LIST_DIRECT_CONTACT_RELATIONSHIPS,
  );

  const listDirectContactRelationships = sortArrayByObjectKey(
    allDirectContactRelationshipsQuery.data?.listDirectContactRelationships,
    'name',
  );

  return (
    <Section
      fullHeight
      headerStyle={{
        backgroundColor: 'white',
      }}
      bodyStyle={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
      }}
      style={{
        marginTop: 0,
      }}
      title="Direct Relationships"
      HeaderRightComp={<AddOrEditDirectRelationshipOptionBtn />}>
      <ContactRelationshipsTable
        data={listDirectContactRelationships}
        loading={allDirectContactRelationshipsQuery.loading}
        hideEmptyMessage={allDirectContactRelationshipsQuery.loading}
      />
    </Section>
  );
};

export default DirectRelationshipsSection;
