import React, {useState} from 'react';
import ConfirmationDialog from '@macanta/containers/ConfirmationDialog';
import ButtonIndex from './index';

const DialogButton = ({DialogProps, ...props}) => {
  const [open, setOpen] = useState(false);

  const {actions: actionsProp, onActions, ...otherDialogProps} = DialogProps;

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  const actions = onActions ? onActions(handleClose) : actionsProp;

  return (
    <>
      <ButtonIndex onClick={handleOpen} {...props} />

      <ConfirmationDialog
        open={open}
        onClose={handleClose}
        actions={actions}
        {...otherDialogProps}
      />
    </>
  );
};

export default DialogButton;
