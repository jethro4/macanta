import React, {useState} from 'react';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import Tooltip from '@macanta/components/Tooltip';
import Popover from '@macanta/components/Popover';
import ListAssignmentQueryOption from './ListAssignmentQueryOption';
import ManualUnassignmentQueryOption from './ManualUnassignmentQueryOption';
import DeleteAssignmentQueryOption from './DeleteAssignmentQueryOption';
import AssignmentIndIcon from '@mui/icons-material/AssignmentInd';
import PersonRemoveIcon from '@mui/icons-material/PersonRemove';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import * as DataObjectsSearchTableStyled from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/styles';

const OptionView = ({
  id,
  name,
  doType,
  selectedType,
  onClose,
  onGoToAssignList,
}) => {
  let optionView = null;

  switch (selectedType) {
    case 'listAssignment': {
      optionView = (
        <ListAssignmentQueryOption
          modal
          showModal
          onClose={onClose}
          id={id}
          name={name}
          doType={doType}
        />
      );
      break;
    }
    case 'manualUnassignment': {
      optionView = (
        <ManualUnassignmentQueryOption
          modal
          showModal
          onClose={onClose}
          id={id}
          name={name}
          onGoToAssignList={onGoToAssignList}
        />
      );
      break;
    }
    case 'delete': {
      optionView = (
        <DeleteAssignmentQueryOption
          showDialog
          onClose={onClose}
          id={id}
          name={name}
        />
      );
      break;
    }
  }

  return optionView;
};

const MoreOptions = ({id, name, doType, ...props}) => {
  const [anchorEl, setAnchorEl] = useState(false);
  const [selectedType, setSelectedType] = useState('');

  const openMoreOptions = Boolean(anchorEl);

  const handleMoreOptions = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleCloseMoreOptions = () => {
    setAnchorEl(null);
  };

  const handleSelectType = (type) => () => {
    setSelectedType(type);
    handleCloseMoreOptions();
  };

  const handleClose = () => {
    setSelectedType('');
  };

  const handleGoToAssignList = () => {
    handleSelectType('listAssignment')();
  };

  return (
    <>
      <Tooltip title="More">
        <DataObjectsSearchTableStyled.ActionButton
          style={{
            color: '#aaa',
          }}
          show={openMoreOptions}
          onClick={handleMoreOptions}
          {...props}>
          <MoreVertIcon />
        </DataObjectsSearchTableStyled.ActionButton>
      </Tooltip>
      <Popover
        open={openMoreOptions}
        anchorEl={anchorEl}
        onClose={handleCloseMoreOptions}
        anchorOrigin={{
          vertical: 'center',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'center',
          horizontal: 'right',
        }}>
        <div
          style={{
            width: 250,
          }}>
          <List
            style={{
              padding: 0,
            }}>
            <NoteTaskHistoryStyled.SelectionItem
              button
              onClick={handleSelectType('listAssignment')}>
              <AssignmentIndIcon />
              <NoteTaskHistoryStyled.SelectionItemText primary="List Assignment" />
            </NoteTaskHistoryStyled.SelectionItem>
            <Divider />
            <NoteTaskHistoryStyled.SelectionItem
              button
              onClick={handleSelectType('manualUnassignment')}>
              <PersonRemoveIcon />
              <NoteTaskHistoryStyled.SelectionItemText primary="Manual Un-Assignment" />
            </NoteTaskHistoryStyled.SelectionItem>
            <Divider />
            <NoteTaskHistoryStyled.SelectionItem
              button
              onClick={handleSelectType('delete')}>
              <DeleteForeverIcon />
              <NoteTaskHistoryStyled.SelectionItemText primary="Delete" />
            </NoteTaskHistoryStyled.SelectionItem>
          </List>
        </div>
      </Popover>
      <OptionView
        id={id}
        name={name}
        doType={doType}
        selectedType={selectedType}
        onClose={handleClose}
        onGoToAssignList={handleGoToAssignList}
      />
    </>
  );
};

MoreOptions.defaultProps = {
  relationships: [],
  connectedContacts: [],
  refetch: () => {},
};

export default MoreOptions;
