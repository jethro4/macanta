import {nextFactoryId} from './factory';

const typename = 'Contact';

export default (overwrites) => {
  const {id, currentIndex} = nextFactoryId(typename);

  return {
    __typename: typename,
    id,
    email: `test${currentIndex}@macantacrm.com`,
    firstName: `FirstName${currentIndex}`,
    lastName: `LastName${currentIndex}`,
    ...overwrites,
  };
};
