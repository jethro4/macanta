import './setEnvVars';
import './setupMocks';
import React from 'react';
import {ApolloProvider} from '@apollo/client';
import {client} from '@macanta/containers/ApolloProviderWrapper';
import BaseTheme from '@macanta/modules/BaseTheme';

export default class Wrapper extends React.Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <BaseTheme>{this.props.children}</BaseTheme>
      </ApolloProvider>
    );
  }
}
