import {useState} from 'react';

const useLazyFetch = () => {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);

  const callFetch = (url, options) => {
    setError(null);
    setLoading(true);

    const finalOptions = options && {
      ...options,
      body: JSON.stringify(options.body),
    };

    return fetch(url, finalOptions)
      .then((res) => res.json())
      .then((res) => {
        setData(res);

        return {data: res, error: null};
      })
      .catch((err) => {
        console.error(err);

        setData(null);

        setError(err);

        return {data: null, error: err};
      })
      .finally(() => {
        setLoading(false);
      });
  };

  return [callFetch, {loading, data, error}];
};

export default useLazyFetch;
