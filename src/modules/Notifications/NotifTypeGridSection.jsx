import React from 'react';
import Section from '@macanta/containers/Section';
import Switch from '@macanta/components/Forms/ChoiceFields/Switch';
import Button from '@macanta/components/Button';
import VirtualizedList from '@macanta/containers/List/VirtualizedList';
import {useQuery, useMutation} from '@apollo/client';
import {
  DELETE_NOTIFICATIONS,
  GET_NOTIFICATIONS_STATUS,
} from '@macanta/graphql/notifications';
import CloseIcon from '@mui/icons-material/Close';
import {timeDiff} from '@macanta/utils/time';
import ShowMoreNoteBtn from '@macanta/modules/NoteTaskHistory/NoteItem/ShowMoreNoteBtn';
import * as Storage from '@macanta/utils/storage';
import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import * as Styled from './styles';

const NotifTypeGridSection = ({
  notifType,
  data,
  onClickNotif,
  onDeleteNotif,
  onClearAll,
  onToggleSilent,
}) => {
  const type = notifType.type.toLowerCase();
  const {userId} = Storage.getItem('userDetails');

  const notificationsStatusQuery = useQuery(GET_NOTIFICATIONS_STATUS, {
    variables: {
      userId,
      type,
    },
  });

  const isEnabled =
    notificationsStatusQuery.data?.getNotificationsStatus?.status === 'active';

  const [callDeleteNotifsMutation, deleteNotifsMutation] = useMutation(
    DELETE_NOTIFICATIONS,
    {
      onError() {},
    },
  );

  const getKeyExtractor = (index, itemData) => {
    const item = itemData[index];

    return item.id;
  };

  const handleEnablePopupChange = (event) => {
    onToggleSilent(type, event.target.checked);
  };

  const handleDeleteNotif = (item) => async (event) => {
    event.stopPropagation();

    await callDeleteNotifsMutation({
      variables: {
        deleteNotificationsInput: {
          id: item.id,
          userId,
        },
        __mutationkey: 'deleteNotificationsInput',
        removeAppInfo: true,
      },
    });

    onDeleteNotif(item);
  };

  const handleClearAll = async () => {
    await callDeleteNotifsMutation({
      variables: {
        deleteNotificationsInput: {
          userId,
          type: type,
        },
        __mutationkey: 'deleteNotificationsInput',
        removeAppInfo: true,
      },
    });

    onClearAll(type);
  };

  const renderNotificationRow = ({data: itemData, index, style}) => {
    const item = itemData[index];
    const isNew = item.status === 'New';

    return (
      <Styled.NotificationItemContainer style={style}>
        <Styled.NotificationItemCard>
          <Styled.CardActionArea isNew={isNew} onClick={onClickNotif(item)}>
            <Styled.Header>
              <Styled.HeaderText>
                {timeDiff(item.createdDate)}
              </Styled.HeaderText>
              <Styled.DismissButton
                aria-haspopup="true"
                onClick={handleDeleteNotif(item)}>
                <CloseIcon
                  style={{
                    color: '#aaa',
                    fontSize: '1.2rem',
                  }}
                />
              </Styled.DismissButton>
            </Styled.Header>
            <ShowMoreNoteBtn
              note={item.message}
              noteFontSize={13}
              style={{
                bottom: 8,
                right: 12,
                fontSize: '0.75rem',
              }}
              previewStyle={{
                padding: 0,
              }}
            />
          </Styled.CardActionArea>
        </Styled.NotificationItemCard>
      </Styled.NotificationItemContainer>
    );
  };

  return (
    <NoteTaskHistoryStyled.FullHeightFlexGridColumn item xs={12} md={4}>
      <Section
        loading={deleteNotifsMutation.loading}
        title={notifType.label}
        fullHeight
        bodyStyle={{
          padding: 0,
          backgroundColor: '#f5f6fa',
        }}
        HeaderLeftComp={
          <Switch
            style={{
              marginLeft: 12,
            }}
            color="success"
            checked={isEnabled}
            onChange={handleEnablePopupChange}
          />
        }
        HeaderRightComp={
          <Button
            disabled={deleteNotifsMutation.loading}
            style={{
              color: '#bbb',
              fontSize: '0.875rem',
              fontWeight: 'bold',
            }}
            onClick={handleClearAll}>
            Clear
          </Button>
        }>
        <VirtualizedList
          data={data}
          keyExtractor={getKeyExtractor}
          renderRow={renderNotificationRow}
          height={462}
          width="100%"
          itemSize={120}
          style={{
            paddingBottom: '0.5rem',
          }}
        />
      </Section>
    </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
  );
};

export default NotifTypeGridSection;
