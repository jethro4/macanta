import useQuery from '@macanta/hooks/apollo/useQuery';
import {GET_SMS_ACTIONS} from '@macanta/graphql/automation';

const FIELD_KEY_MAPPING = {
  queryId: 'id',
  queryType: 'type',
  queryName: 'name',
  queryDescription: 'description',
  queryPhoneField: 'phoneField',
  querySMSMessage: 'message',
};

const useSmsActions = (options) => {
  const query = useQuery(GET_SMS_ACTIONS, {
    keyMapping: FIELD_KEY_MAPPING,
    transformResult: (result) => result?.items,
    ...options,
  });

  return query;
};

export default useSmsActions;
