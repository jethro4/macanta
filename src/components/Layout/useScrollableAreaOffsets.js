import {useContext} from 'react';
import ScrollableAreaContext from './ScrollableAreaContext';

const useScrollableAreaOffsets = () => {
  return useContext(ScrollableAreaContext);
};

export default useScrollableAreaOffsets;
