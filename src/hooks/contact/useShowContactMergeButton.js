import useQuery from '@macanta/hooks/apollo/useQuery';
import {GET_ACCESS_PERMISSIONS} from '@macanta/graphql/permissions';
import * as Storage from '@macanta/utils/storage';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';

const useShowContactMergeButton = () => {
  const {email: userEmail} = Storage.getItem('userDetails');

  const {data} = useQuery(GET_ACCESS_PERMISSIONS, {
    variables: {
      email: userEmail,
    },
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
    onError() {},
  });

  const isAdmin = data?.getAccessPermissions?.isAdmin;

  const showContactMergeButton = !!data?.getAccessPermissions?.permissionMeta
    ?.allowUserToMergeContacts;

  return isAdmin || (!isAdmin && showContactMergeButton);
};

export default useShowContactMergeButton;
