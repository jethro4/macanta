import React, {useState} from 'react';
import {useFormikContext} from 'formik';
import SendAndArchiveIcon from '@mui/icons-material/SendAndArchive';
import SendIcon from '@mui/icons-material/Send';
import Typography from '@mui/material/Typography';
import Form from '@macanta/components/Form';
import Button from '@macanta/components/Button';
import Tooltip from '@macanta/components/Tooltip';
import EmailAttachments from '@macanta/modules/ContactDetails/EmailAttachments';
import EmailPreview from '@macanta/modules/ContactDetails/EmailPreview';
import EmailSignatureBtn from '@macanta/modules/ContactDetails/EmailSignatureBtn';
import EmailAttachmentOptions from '@macanta/modules/ContactDetails/EmailAttachmentOptions';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import {useMutation} from '@apollo/client';
import {EMAIL} from '@macanta/graphql/admin';
import EmailBodyRTE from '@macanta/modules/ContactDetails/EmailBodyRTE';
import useSignature from '@macanta/hooks/admin/useSignature';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import useDeepCompareEffect from '@macanta/hooks/base/useDeepCompareEffect';
import useEmailWidgetsByPermissions from '@macanta/hooks/useEmailWidgetsByPermissions';
import * as FormStyled from '@macanta/modules/NoteTaskForms/styles';
import * as ContactDetailsStyled from '@macanta/modules/ContactDetails/styles';
import * as Storage from '@macanta/utils/storage';

const EmailBatchWidgetsFormsContainer = ({selectedWidgetIds, ...props}) => {
  const loggedInUserDetails = Storage.getItem('userDetails');

  const getInitValues = () => {
    return {
      widgetIds: selectedWidgetIds,
      from: loggedInUserDetails.email,
      subject: '',
      message: '',
      signature: data,
      files: [],
    };
  };

  const [initValues, setInitValues] = useState(() => getInitValues());

  const {data, loading: loadingSignature} = useSignature();

  useDeepCompareEffect(() => {
    setInitValues(getInitValues());
  }, [selectedWidgetIds]);

  return (
    <Form
      enableReinitialize
      initialValues={initValues}
      // validationSchema={emailValidationSchema}
    >
      <EmailBatchWidgetsForms
        {...props}
        data={data}
        loadingSignature={loadingSignature}
      />
    </Form>
  );
};

const EmailBatchWidgetsForms = ({
  widgets,
  onSuccess,
  loadingSignature,
  disableSelectRecipient,
  ...props
}) => {
  const loggedInUserDetails = Storage.getItem('userDetails');

  const {values, errors, handleChange, setFieldValue} = useFormikContext();
  const {displayMessage} = useProgressAlert();

  const queryWidgets = useEmailWidgetsByPermissions(widgets);

  const [callEmailMutation, emailMutation] = useMutation(EMAIL, {
    onCompleted(data) {
      if (data.email?.success) {
        onSuccess();
      }
    },
    onError() {},
  });

  const [callTestEmailMutation, testEmailMutation] = useMutation(EMAIL, {
    onCompleted() {
      displayMessage('Test email sent!');
    },
    onError() {},
  });

  const handleSendEmail = (isTestEmail) => {
    if (!isTestEmail && !values.widgetIds?.length) {
      alert('Select at least 1 widget!');
      return;
    }

    if (!values.subject) {
      const confirmed = confirm('Send this email without a subject?');

      if (!confirmed) {
        return;
      }
    }

    if (!values.message) {
      const confirmed = confirm('Send this email without a message?');

      if (!confirmed) {
        return;
      }
    }

    const callSendEmail = !isTestEmail
      ? callEmailMutation
      : callTestEmailMutation;

    const selectedWidgets = queryWidgets.filter((widget) =>
      values.widgetIds.includes(widget.id),
    );

    const queryIds = selectedWidgets.map((w) => w.queryId).join(',');
    const widgetNames = selectedWidgets.map((w) => w.title).join(',');
    const groupNames = selectedWidgets.map((w) => w.groupName).join(',');

    callSendEmail({
      variables: {
        emailInput: {
          ...(isTestEmail
            ? {
                userId: loggedInUserDetails.userId,
                sendMethod: 'single',
                mode: 'Live',
                toContacts: loggedInUserDetails.userId,
                subject: values.subject,
                message: values.message,
                signature: values.signature,
                uploadedFileAttachments: values.files.map((f) => ({
                  fileName: f.fileName,
                  content: f.thumbnail,
                })),
              }
            : {
                userId: loggedInUserDetails.userId,
                sendMethod: selectedWidgets.length === 1 ? 'widget' : 'widgets',
                subject: values.subject,
                message: values.message,
                signature: values.signature,
                uploadedFileAttachments: values.files.map((f) => ({
                  fileName: f.fileName,
                  content: f.thumbnail,
                })),
                queryId: queryIds,
                widgetName: widgetNames,
                groupName: groupNames,
              }),
        },
        __mutationkey: 'emailInput',
      },
    });
  };

  const error = emailMutation.error;

  return (
    <ContactDetailsStyled.EmailFormsContainer {...props}>
      {loadingSignature && <LoadingIndicator fill align="top" />}
      {!loadingSignature && (
        <>
          <ContactDetailsStyled.EmailFormsHeaderContainer>
            <ContactDetailsStyled.EmailTextField
              disabled={disableSelectRecipient}
              select
              multiple
              chipped
              options={queryWidgets}
              labelKey="title"
              valueKey="id"
              error={errors.widgetIds}
              onChange={(val) => {
                setFieldValue('widgetIds', val);
              }}
              placeholder="Query Widgets"
              variant="filled"
              value={values.widgetIds}
              fullWidth
              size="medium"
            />

            <ContactDetailsStyled.EmailTextField
              noBorder
              onChange={handleChange('subject')}
              placeholder="Subject"
              variant="filled"
              value={values.subject}
              fullWidth
            />
          </ContactDetailsStyled.EmailFormsHeaderContainer>

          <ContactDetailsStyled.EmailRichTextFieldContainer>
            <EmailBodyRTE
              types="sender,contact,do_all"
              whitelist={queryWidgets
                .filter(
                  (widget) =>
                    values.widgetIds.length === 1 &&
                    values.widgetIds.includes(widget.id),
                )
                .reduce((acc, widget) => {
                  const types = [];

                  if (!acc.includes(widget.groupName)) {
                    types.push(widget.groupName);
                  }

                  return acc.concat(types);
                }, [])}
              error={errors.message}
              onChange={handleChange('message')}
              placeholder="Type Your Message Here"
              value={values.message}
            />
          </ContactDetailsStyled.EmailRichTextFieldContainer>

          <EmailAttachments
            files={values.files}
            onDelete={(fileId) => {
              setFieldValue(
                'files',
                values.files.filter((file) => fileId !== file.id),
              );
            }}
          />

          <FormStyled.Footer
            style={{
              margin: '8px 0',
              padding: '0 1rem',
            }}>
            {!!error && (
              <Typography
                style={{
                  flex: 1,
                  lineHeight: '0.875rem',
                }}
                color="error"
                variant="subtitle2"
                align="center">
                {error.message}
              </Typography>
            )}
            <EmailSignatureBtn
              value={values.signature}
              onEdit={(signature) => {
                setFieldValue('signature', signature);
              }}
            />
            <EmailAttachmentOptions
              onSelect={async (data) => {
                const updatedFiles = values.files.concat(data);

                setFieldValue('files', updatedFiles);
              }}
            />
            <EmailPreview values={values} />
            <Tooltip title="Send Me Test Email" placement="top">
              <div>
                <ContactDetailsStyled.EmailFooterIconButton
                  color="secondary"
                  onClick={() => {
                    handleSendEmail(true);
                  }}
                  size="small">
                  <SendAndArchiveIcon />
                </ContactDetailsStyled.EmailFooterIconButton>
              </div>
            </Tooltip>
            <Button
              style={{
                marginLeft: '1.5rem',
              }}
              // disabled={!values.message}
              variant="contained"
              startIcon={<SendIcon />}
              onClick={() => {
                handleSendEmail();
              }}
              size="medium">
              Send Email
            </Button>
          </FormStyled.Footer>
        </>
      )}
      <LoadingIndicator
        modal
        loading={emailMutation.loading || testEmailMutation.loading}
      />
    </ContactDetailsStyled.EmailFormsContainer>
  );
};

export default EmailBatchWidgetsFormsContainer;
