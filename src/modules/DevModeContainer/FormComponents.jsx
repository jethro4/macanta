import React, {useState, useEffect} from 'react';
import RawHTML from '@macanta/components/RawHTML';
import envConfig from '@macanta/config/envConfig';
import {getAppInfo} from '@macanta/utils/app';

const getQueryStringParams = (query) => {
  return query
    ? (/^[?#]/.test(query) ? query.slice(1) : query)
        .split('&')
        .reduce((params, param) => {
          let [key, value] = param.split('=');
          params[key] = value
            ? decodeURIComponent(value.replace(/\+/g, ' '))
            : '';
          return params;
        }, {})
    : {};
};

const FormComponents = () => {
  const [appName] = getAppInfo();

  const [formId, setFormId] = useState();

  useEffect(() => {
    const params = getQueryStringParams(
      window.location.href.replace(
        `${window.location.origin}${window.location.pathname}`,
        '',
      ),
    );

    setFormId(params.formId);
  }, []);

  return !formId ? null : (
    <RawHTML>{`
          <div id="MacantaWebform" style="max-width: 800px;margin: 0 auto;"><script type="text/javascript" src="https://${appName}.${envConfig.apiDomain}/webform/js/${formId}"></script></div>
        `}</RawHTML>
  );
};

export default FormComponents;
