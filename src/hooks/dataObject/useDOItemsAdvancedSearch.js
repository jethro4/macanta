import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import useDOFields from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOFields';
import useDOItems from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOItems';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';

const useDOItemsAdvancedSearch = ({
  search,
  groupId,
  contactId,
  onCompleted,
  onError,
  page,
  limit,
  order,
  orderBy,
}) => {
  const doTypesQuery = useDOTypes({
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
    skip: !search,
  });
  const allFieldsQuery = useDOFields(groupId, {
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
    skip: !groupId || !search,
  });

  const doTypes = doTypesQuery.data?.listDataObjectTypes;
  const allFields = allFieldsQuery.data?.listDataObjectFields;

  let base64Search = '';

  if (search && doTypes && allFields) {
    const queryCDField = [];
    const doType = doTypes?.find((type) => type.id === groupId);
    let fields = allFields.filter((item) => !!item.showInTable);

    if (!fields.length) {
      fields = allFields.slice(0, 4);
    }

    fields.forEach((item) => {
      queryCDField.push({
        queryCDFieldLogic: 'or',
        queryCDFieldName: item.name,
        queryCDFieldOperator: 'contains',
        queryCDFieldValue: search,
        queryCDFieldValues: null,
      });
    });

    base64Search = window.btoa(
      JSON.stringify({
        queryCDField,
        queryConnectedDataType: doType?.title,
      }),
    );
  }

  const doItemsQuery = useDOItems(
    {
      q: base64Search,
      groupId,
      contactId,
      page,
      limit,
      order,
      orderBy,
    },
    {
      onCompleted,
      onError,
      skip: search && (!doTypes || !allFields),
    },
  );

  const loading =
    doItemsQuery.loading || doTypesQuery.loading || allFieldsQuery.loading;

  return {...doItemsQuery, loading};
};

export default useDOItemsAdvancedSearch;
