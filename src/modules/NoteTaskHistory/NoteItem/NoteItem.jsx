import React from 'react';
import {NOTE_TAG} from '@macanta/constants/noteTaskTypes';
import Typography from '@mui/material/Typography';
import * as HistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import * as Styled from './styles';
import Tags from '@macanta/modules/NoteTaskHistory/Tags';
import Box from '@mui/material/Box';
import useNotesPermissions from '@macanta/hooks/useNotesPermissions';
import moment from 'moment';
import ShowMoreNoteBtn from './ShowMoreNoteBtn';

export const includeDefaultTag = (tags = []) => {
  return tags?.includes(NOTE_TAG.DEFAULT) ? tags : [NOTE_TAG.DEFAULT, ...tags];
};

const NoteItem = ({item, NoteFormsBtnComp}) => {
  const notesPermission = useNotesPermissions();

  const tags = includeDefaultTag(item.tags);

  return (
    <Styled.Root>
      <Styled.Header>
        <Box
          style={{
            display: 'flex',
            flexDirection: 'column',
          }}>
          <Typography variant="caption">
            {moment(item.creationDate).format('ddd DD MMM YYYY hh:mmA')}
          </Typography>
          <Styled.Title>{item.title}</Styled.Title>
        </Box>
        <Box
          style={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'flex-end',
            justifyContent: 'center',
          }}>
          {!!item.createdBy?.trim() && (
            <Typography
              variant="caption"
              style={{
                color: '#888',
                fontSize: '.68rem',
              }}>
              Created By: {item.createdBy}
            </Typography>
          )}
          {!notesPermission.isReadOnly && NoteFormsBtnComp}
        </Box>
      </Styled.Header>
      <HistoryStyled.Divider />
      {item.note && (
        <Styled.Body>
          <ShowMoreNoteBtn note={item.note} />
        </Styled.Body>
      )}
      <Tags data={tags} />
    </Styled.Root>
  );
};

export default NoteItem;
