import {experimentalStyled as styled} from '@mui/material/styles';
import MUIButton from '@mui/material/Button';
import MUIIconButton from '@mui/material/IconButton';
import MUIListItem from '@mui/material/ListItem';
import MUIListItemText from '@mui/material/ListItemText';

export const Button = styled(MUIButton)`
  text-transform: capitalize;
  white-space: nowrap;

  ${({variant}) =>
    variant === 'text' &&
    `
    padding: 0;

    .MuiButton-startIcon {
      margin-right: 4px;
    }

    .MuiButton-endIcon {
      margin-left: 4px;
    }
  `}
`;

export const IconButton = styled(MUIIconButton)`
  ${({theme, color, disabled}) =>
    color &&
    !disabled &&
    theme.palette[color] &&
    `color: ${theme.palette[color].main} !important;`}
`;

export const SelectListItem = styled(MUIListItem)`
  padding: 8px;
`;

export const SelectListItemText = styled(MUIListItemText)``;
