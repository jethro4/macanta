export const FETCH_POLICIES = Object.freeze({
  CACHE_ONLY: 'cache-only',
  CACHE_FIRST: 'cache-first',
  CACHE_AND_NETWORK: 'cache-and-network',
  NETWORK_ONLY: 'network-only',
  NO_CACHE: 'no-cache',
});

export const POLICIES_WITH_NETWORK_CALL = Object.freeze([
  FETCH_POLICIES.CACHE_AND_NETWORK,
  FETCH_POLICIES.NETWORK_ONLY,
  FETCH_POLICIES.NO_CACHE,
]);
