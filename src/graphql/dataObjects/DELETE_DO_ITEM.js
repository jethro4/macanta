import {gql} from '@apollo/client';

export default gql`
  mutation deleteDataObjectItem($input: DeleteDataObjectItemInput!) {
    deleteDataObjectItem(input: $input) {
      success
      id
    }
  }
`;
