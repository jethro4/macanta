import React from 'react';
import * as Styled from './styles';

const Divider = (props) => <Styled.Root {...props} />;

export default Divider;
