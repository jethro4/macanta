import React from 'react';

import Typography from '@mui/material/Typography';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import SendIcon from '@mui/icons-material/Send';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import TextField from '@macanta/components/Forms';
import Form from '@macanta/components/Form';
import {useMutation} from '@apollo/client';
import {FORGOT_PASSWORD} from '@macanta/graphql/auth';
import {resetPasswordValidationSchema} from '@macanta/validations/forgotPassword';
import FormHeader from './FormHeader';
import * as Styled from './styles';

const FormsResetPassword = ({email, code, onLogin, onForgotPWSuccess}) => {
  const [callForgotPwStep3, forgotPwStep3Mutation] = useMutation(
    FORGOT_PASSWORD,
    {
      onCompleted(data) {
        if (data.forgotPassword?.success) {
          onForgotPWSuccess();
        }
      },
      onError() {},
    },
  );

  const handleFormSubmit = (values) => {
    callForgotPwStep3({
      variables: {
        forgotPasswordInput: {
          step: 3,
          email,
          code,
          newPassword: values.newPassword,
        },
        __mutationkey: 'forgotPasswordInput',
      },
    });
  };

  return (
    <>
      <FormHeader
        title="Reset Password"
        description="Please type in your new password."
      />

      <Form
        initialValues={{newPassword: ''}}
        validationSchema={resetPasswordValidationSchema}
        onSubmit={handleFormSubmit}>
        {({
          values,
          errors,
          handleChange,
          handleSubmit,
          /* and other goodies */
        }) => (
          <>
            <TextField
              autoComplete="off"
              error={errors.newPassword}
              password
              onChange={handleChange('newPassword')}
              label="New Password"
              variant="filled"
              value={values.newPassword}
              fullWidth
              onEnterPress={handleSubmit}
            />

            {!!forgotPwStep3Mutation.error && (
              <Typography
                color="error"
                variant="subtitle2"
                align="center"
                dangerouslySetInnerHTML={{
                  __html: forgotPwStep3Mutation.error.message,
                }}
              />
            )}

            <Styled.GridItem container>
              <Styled.GridItem item xs={12} sm={8}>
                <Styled.ButtonLeft
                  onClick={onLogin}
                  startIcon={
                    <ChevronLeftIcon
                      style={{
                        fontSize: '1.5rem',
                      }}
                    />
                  }>
                  Back to login
                </Styled.ButtonLeft>
              </Styled.GridItem>
              <Styled.GridItem item xs={12} sm={4}>
                <Styled.ButtonRight
                  onClick={handleSubmit}
                  color="primary"
                  variant="contained"
                  startIcon={<SendIcon />}>
                  Submit
                </Styled.ButtonRight>
              </Styled.GridItem>
            </Styled.GridItem>
          </>
        )}
      </Form>
      <LoadingIndicator fill loading={forgotPwStep3Mutation.loading} />
    </>
  );
};

export default FormsResetPassword;
