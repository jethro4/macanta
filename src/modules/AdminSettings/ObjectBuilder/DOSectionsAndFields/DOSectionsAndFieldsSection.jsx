import React, {useState, useLayoutEffect, useMemo} from 'react';
import {useFormikContext} from 'formik';
import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import FormField from '@macanta/containers/FormField';
import {PopoverButton, DialogButton} from '@macanta/components/Button';
import SectionForm from './SectionForm';
import SubGroupForm from './SubGroupForm';
import SubGroupColumn from './SubGroupColumn';

const MAX_SUBGROUPS = 4;

const DOSectionsAndFieldsSection = () => {
  const {values, setValues, setFieldValue} = useFormikContext();

  const [selectedSection, setSelectedSection] = useState('');

  // const allRelationshipsQuery = useDORelationships(null, {
  //   fetchAll: true,
  //   fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  // });

  // const allRelationships = allRelationshipsQuery.data?.listRelationships;

  const handleDeleteSection = (id) => {
    if (id === selectedSection) {
      const selectedIndex = sectionNames.findIndex((s) => s.value === id);
      const newSelectedSection =
        sectionNames[selectedIndex + 1]?.value ||
        sectionNames[selectedIndex - 1]?.value;
      setSelectedSection(newSelectedSection || '');
    }

    setValues((state) => {
      const newFieldsToDelete = [];
      const sectionItem = state.sectionsData.find(
        (section) => section.id === id,
      );

      sectionItem.subGroups.forEach((subGroup) => {
        subGroup.data.forEach((d) => {
          if (!d.isNew) {
            newFieldsToDelete.push(d.id);
          }
        });
      });

      return {
        ...state,
        sectionsData: state.sectionsData.filter((section) => section.id !== id),
        fieldsToDelete: state.fieldsToDelete.concat(newFieldsToDelete),
      };
    });
  };

  const sectionNames = useMemo(() => {
    if (values.sectionsData.length) {
      return values.sectionsData.map((section) => ({
        label: section.sectionName,
        value: section.id,
      }));
    }

    return [];
  }, [values.sectionsData]);

  const selectedSectionExists = sectionNames.some(
    (section) => section.value === selectedSection,
  );

  useLayoutEffect(() => {
    if (sectionNames.length) {
      if (!selectedSection || !selectedSectionExists) {
        setSelectedSection(sectionNames[0].value);
      } else if (values.updatedId) {
        setSelectedSection(values.updatedId);

        setFieldValue('updatedId', null);
      }
    }
  }, [sectionNames, values.updatedId]);

  const subGroups = useMemo(() => {
    if (selectedSection && values.sectionsData.length) {
      const filteredSectionsData = values.sectionsData.find(
        (section) => section.id === selectedSection,
      );

      return (
        filteredSectionsData?.subGroups?.map((subGroup) => ({
          label: subGroup.subGroupName,
          value: subGroup.id,
          data: subGroup.data,
        })) || []
      );
    }

    return [];
  }, [values.sectionsData, selectedSection]);

  const canAddSubGroup =
    !!sectionNames.length && subGroups.length < MAX_SUBGROUPS;

  return (
    <>
      <Box
        style={{
          padding: '0 1rem',
          marginTop: 8,
        }}>
        <FormField
          sx={{
            '& .MuiFormControlLabel-root': {
              '&:after': {
                content: '""',
                borderRight: '1px solid #ddd',
                marginLeft: '16px',
                marginRight: '2px',
                width: '1px',
                height: '18px',
              },
            },
            '& .radio-selected': {
              position: 'relative',

              '&:after': {
                content: '""',
                position: 'absolute',
                bottom: '-8px',
                left: '50%',
                transform: 'translateX(-50%)',
                height: '2px',
                display: 'block',
                width: '80px',
                backgroundColor: 'tertiary.main',
                opacity: 0.4,
              },
            },
          }}
          style={{
            marginBottom: 0,
          }}
          fieldStyle={{
            flexWrap: 'wrap',
            alignItems: 'center',
          }}
          itemContainerStyle={{
            backgroundColor: 'white',
            padding: '0 12px',
            borderRadius: '2rem',
            marginTop: 8,
            marginRight: 12,
            boxShadow: '#ccc 0px 2px 4px -2px',
          }}
          type="Radio"
          row
          options={sectionNames}
          value={selectedSection}
          // error={errors.automationStatus}
          onChange={setSelectedSection}
          fullWidth
          size="small"
          variant="outlined"
          renderRightComp={() => (
            <Box
              style={{
                marginTop: 6,
              }}>
              <PopoverButton
                size="small"
                variant="contained"
                startIcon={<AddIcon />}
                PopoverProps={{
                  title: 'Add Section',
                  contentWidth: 380,
                  anchorOrigin: {
                    vertical: 'bottom',
                    horizontal: 'center',
                  },
                  transformOrigin: {
                    vertical: -8,
                    horizontal: 'center',
                  },
                  // bodyStyle: {
                  //   backgroundColor: '#f5f6fA',
                  // },
                }}
                renderContent={(handleClose) => (
                  <SectionForm
                    position={sectionNames.length + 1}
                    lastPosition={sectionNames.length}
                    onClose={handleClose}
                  />
                )}>
                Add Section
              </PopoverButton>
            </Box>
          )}
          renderItemRightComp={(item, sectionIndex) => (
            <Box
              style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <PopoverButton
                style={{
                  padding: 0,
                }}
                icon
                sx={{
                  padding: '0.25rem',
                  color: 'info.main',
                }}
                // onClick={handleAdd}
                size="small"
                TooltipProps={{
                  title: 'Edit Section',
                }}
                PopoverProps={{
                  title: 'Edit Section',
                  contentWidth: 380,
                  anchorOrigin: {
                    vertical: 'bottom',
                    horizontal: 'center',
                  },
                  transformOrigin: {
                    vertical: -8,
                    horizontal: 'center',
                  },
                }}
                renderContent={(handleClose) => (
                  <SectionForm
                    id={item.value}
                    name={item.label}
                    position={sectionIndex + 1}
                    lastPosition={sectionNames.length}
                    onClose={handleClose}
                  />
                )}>
                <EditIcon
                  style={{
                    fontSize: 18,
                  }}
                />
              </PopoverButton>
              <DialogButton
                style={{
                  padding: 0,
                }}
                icon
                size="small"
                TooltipProps={{title: 'Delete Section'}}
                DialogProps={{
                  title: 'Are you sure?',
                  description: [
                    `You are about to delete "${item.label}" from sections.`,
                    'This will also remove all sub-sections and fields within it. Do you want to proceed?',
                  ],
                  onActions: (handleClose) => {
                    return [
                      {
                        label: 'Delete',
                        onClick: () => {
                          handleDeleteSection(item.value);
                          handleClose();
                        },
                        startIcon: <DeleteForeverIcon />,
                      },
                    ];
                  },
                }}>
                <DeleteForeverIcon
                  style={{
                    fontSize: 18,
                  }}
                  sx={{
                    color: 'grayDarker.main',
                  }}
                />
              </DialogButton>
            </Box>
          )}
        />
      </Box>
      {!!selectedSection && (
        <Divider
          style={{
            marginTop: '1.2rem',
          }}
        />
      )}
      <Box
        style={{
          display: 'flex',
          flexWrap: 'wrap',
          flex: 1,
          paddingBottom: 16,
        }}>
        {subGroups.map((item, subGroupIndex) => (
          <SubGroupColumn
            key={item.value}
            sectionId={selectedSection}
            id={item.value}
            name={item.label}
            data={item.data}
            position={subGroupIndex + 1}
            lastPosition={subGroups.length}
          />
        ))}
        {canAddSubGroup && (
          <Box
            style={{
              marginTop: '1.2rem',
              marginLeft: '1rem',
            }}>
            <PopoverButton
              size="small"
              variant="contained"
              startIcon={<AddIcon />}
              PopoverProps={{
                title: 'Add Sub-Section',
                contentWidth: 380,
                anchorOrigin: {
                  vertical: 'bottom',
                  horizontal: 'left',
                },
                transformOrigin: {
                  vertical: -8,
                  horizontal: 'left',
                },
                // bodyStyle: {
                //   backgroundColor: '#f5f6fA',
                // },
              }}
              renderContent={(handleClose) => (
                <SubGroupForm
                  sectionId={selectedSection}
                  position={subGroups.length + 1}
                  lastPosition={subGroups.length}
                  onClose={handleClose}
                />
              )}>
              Add Sub-Section
            </PopoverButton>
          </Box>
        )}
      </Box>
    </>
  );
};

export default DOSectionsAndFieldsSection;
