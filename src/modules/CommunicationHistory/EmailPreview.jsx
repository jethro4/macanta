import React, {useState} from 'react';
import useRetryQuery from '@macanta/hooks/apollo/useRetryQuery';
import {LIST_COMMUNICATION_HISTORY} from '@macanta/graphql/contacts';
import * as Styled from './styles';

const EmailPreview = ({emailId, subject, attachments}) => {
  const [open, setOpen] = useState(false);

  const {data, initLoading} = useRetryQuery(LIST_COMMUNICATION_HISTORY, {
    variables: {
      emailId,
    },
    onError() {},
    skip: !emailId || !open,
  });

  const emailHTML = data?.listCommunicationHistory?.html;

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Styled.EmailPreview
      open={open}
      values={{
        message: emailHTML,
        subject,
      }}
      attachments={attachments}
      loading={initLoading}
      PopoverProps={{
        open,
        onOpen: handleOpen,
        onClose: handleClose,
      }}
    />
  );
};

export default EmailPreview;
