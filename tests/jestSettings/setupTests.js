// react-testing-library renders your components to document.body,
// this adds jest-dom's custom assertions
import '@testing-library/jest-dom';
// import {client} from '@macanta/containers/ApolloProviderWrapper';
import {server} from '@tests/jestSettings/mockServer';
import {resetFactoryIds} from '@tests/jestSettings/mockFactory/factory';
import {initialize} from '@tests/jestSettings/setupMocks';

const MAX_TIMEOUT = 20000;

jest.setTimeout(MAX_TIMEOUT);

beforeAll(() => {
  // Enable the mocking in tests.
  server.listen({
    //This helps to throw errors if the components send requests to any unhandled queries.
    onUnhandledRequest: (req, print) => {
      req.json().then((jsonResult) => {
        process.stdout.write(
          `Unhandled Request: ${jsonResult.operationName} - ${JSON.stringify(
            jsonResult.variables,
          )}\n\n`,
        );
      });
      // if (req.headers.get('User-Agent') === 'supertest') {
      //   return;
      // }
      print.error();
    },
  });
});

beforeEach(() => {
  // Ensure Apollo cache is cleared between tests.
  // https://www.apollographql.com/docs/react/api/core/ApolloClient/#ApolloClient.clearStore
  initialize();

  jest.clearAllMocks();

  // return client.clearStore();
});

afterEach(async () => {
  // await client.resetStore();

  // Reset any runtime handlers tests may use.
  resetFactoryIds();

  server.resetHandlers();
});

afterAll(() => {
  // Clean up once the tests are done.
  server.close();
});
