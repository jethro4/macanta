import uniqBy from 'lodash/uniqBy';
import useValue from '@macanta/hooks/base/useValue';

const useAccumulatedItems = (items = [], key) => {
  const [accItems] = useValue(
    (prevValue = []) => uniqBy([...prevValue, ...items], key),
    [items, key],
  );

  return {items: accItems};
};

export default useAccumulatedItems;
