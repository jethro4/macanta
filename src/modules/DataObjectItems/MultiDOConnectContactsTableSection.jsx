import React, {useState, useEffect} from 'react';
import Section from '@macanta/containers/Section';
import DataTable from '@macanta/containers/DataTable';
import Typography from '@mui/material/Typography';
import Pagination from '@macanta/components/Pagination';
import DataObjectsQuery from '@macanta/modules/hoc/DataObjectsQuery';
import DOColumnsWithFieldsAndRelationships from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/DOColumnsWithFieldsAndRelationships';
import DOSearch from './DOSearch';
import MultiDOContactFilter from './MultiDOContactFilter';
import MultiDORelationshipSelect from './MultiDORelationshipSelect';
import useContactId from '@macanta/hooks/useContactId';
import * as NoteTaskFormsStyled from '@macanta/modules/NoteTaskForms/styles';
import * as Styled from './styles';

const MultiDOConnectContactsTableSection = ({
  doTitle,
  groupId,
  onChangeRelationships,
  ...props
}) => {
  const contactId = useContactId();

  const [searchFilter, setSearchFilter] = useState('');
  const [filters, setFilters] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(150);
  const [sort, setSort] = useState();
  const [contactIds, setContactIds] = useState([]);
  const [filterOptions, setFilterOptions] = useState([
    {
      type: 'noRelationships',
      filterVal: contactId,
    },
    {
      type: 'contactIds',
      filterVal: [],
    },
  ]);

  const handleSearch = ({search}) => {
    setSearchFilter(search);

    setPage(0);
  };

  const handleFilters = (filterVal) => {
    setFilters(filterVal);

    setPage(0);
  };

  const handleChangePage = (event, value) => {
    setPage(value);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value));
    setPage(0);
  };

  const handleChangeRelationship = (itemId) => (relationships) => {
    onChangeRelationships(itemId, relationships);
  };

  const handleSort = (property, sortDirection) => {
    setSort({
      order: sortDirection,
      orderBy: property,
    });
  };

  useEffect(() => {
    setFilterOptions(
      filterOptions.map((filterOption) => {
        if (filterOption.type === 'contactIds') {
          return {
            ...filterOption,
            filterVal: contactIds,
          };
        }

        return filterOption;
      }),
    );
  }, [contactIds]);

  return (
    <DataObjectsQuery
      search={searchFilter}
      filters={filters}
      groupId={groupId}
      page={page}
      limit={rowsPerPage}
      order={sort?.order}
      orderBy={sort?.orderBy}>
      {({data = {}, selectedDO, loading: doLoading}) => {
        const total = data?.total;

        return (
          <DOColumnsWithFieldsAndRelationships
            groupId={groupId}
            items={selectedDO?.items}
            filterOptions={filterOptions}
            hideContacts>
            {({
              columns,
              doItemsData,
              loading: columnsLoading,
              relationships,
            }) => {
              const loading = Boolean(doLoading || columnsLoading);

              return (
                <Section
                  fullHeight
                  headerStyle={{
                    backgroundColor: 'white',
                  }}
                  bodyStyle={{
                    overflowY: 'hidden',
                  }}
                  style={{
                    margin: 0,
                    flex: 1,
                  }}
                  loading={loading}
                  HeaderLeftComp={
                    <Typography color="textPrimary">{doTitle}</Typography>
                  }
                  HeaderRightComp={
                    <Styled.HeaderRightContainer>
                      <DOSearch
                        style={{
                          marginRight: '1rem',
                        }}
                        FilterProps={{
                          PopoverProps: {
                            title: `${doTitle} Filters`,
                          },
                        }}
                        onSearch={handleSearch}
                        onFilter={handleFilters}
                        filters={filters}
                        groupId={groupId}
                      />
                      <MultiDOContactFilter
                        contactIds={contactIds}
                        onChange={setContactIds}
                      />
                    </Styled.HeaderRightContainer>
                  }
                  FooterComp={
                    <NoteTaskFormsStyled.Footer
                      style={{
                        margin: 0,
                        borderTop: '1px solid #eee',
                        width: '100%',
                        backgroundColor: 'white',
                        justifyContent: 'space-between',
                      }}>
                      <Pagination
                        table
                        count={total}
                        page={page}
                        onPageChange={handleChangePage}
                        rowsPerPage={rowsPerPage}
                        onRowsPerPageChange={handleChangeRowsPerPage}
                        labelRowsPerPage={''}
                        rowsPerPageOptions={[]}
                        labelDisplayedRows={() => `Page ${page + 1}`}
                      />
                    </NoteTaskFormsStyled.Footer>
                  }
                  {...props}>
                  <DataTable
                    fullHeight
                    hideEmptyMessage={loading}
                    columns={columns}
                    data={doItemsData}
                    onSort={handleSort}
                    numOfTextLines={2}
                    actionColumn={{
                      label: 'Add As',
                      style: {
                        width: 270,
                      },
                    }}
                    renderActionButtons={({item}) => {
                      return (
                        <MultiDORelationshipSelect
                          connectedContacts={item.connectedContacts}
                          relationships={relationships}
                          onChange={handleChangeRelationship(item.id)}
                        />
                      );
                    }}
                    hidePagination
                    rowsPerPage={rowsPerPage}
                    noLimit
                  />
                </Section>
              );
            }}
          </DOColumnsWithFieldsAndRelationships>
        );
      }}
    </DataObjectsQuery>
  );
};

export default MultiDOConnectContactsTableSection;
