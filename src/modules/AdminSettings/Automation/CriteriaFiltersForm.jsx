import React from 'react';
import CriteriaForms from '@macanta/modules/AdminSettings/QueryBuilder/CriteriaForms';
import useAllFields from '@macanta/hooks/dataObject/useAllFields';
import {DEFAULT_OPERATOR} from '@macanta/constants/operators';
import AddConditionBtn from '@macanta/modules/AdminSettings/QueryBuilder/AddConditionBtn';
import BodySection from '@macanta/modules/AdminSettings/UserManagement/BodySection';
import {useTheme} from '@mui/material/styles';
import useDORelationships from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDORelationships';
import useValue from '@macanta/hooks/base/useValue';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';

export const getNewItem = (type, logic) => {
  switch (type) {
    case 'doConditions': {
      return {
        id: Date.now(),
        name: '',
        logic,
        operator: DEFAULT_OPERATOR,
        values: null,
        value: '',
      };
    }
    case 'contactConditions': {
      return {
        id: Date.now(),
        relationship: '',
        logic,
      };
    }
    default: {
      return {
        id: Date.now(),
        name: '',
        value: '',
      };
    }
  }
};

const CriteriaFiltersForm = ({
  hideContactConditions,
  filters,
  groupId,
  onChange,
  style,
  ContactConditionProps,
  DOConditionProps,
}) => {
  const theme = useTheme();

  const allFieldsQuery = useAllFields({
    groupId,
    options: {
      fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
    },
  });
  const doRelationshipsQuery = useDORelationships(groupId, {
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const handleChange = (type) => (value) => {
    const updatedFilters = {
      ...filters,
      [type]: value[type],
    };

    onChange?.(updatedFilters);
  };

  const handleAdd = (type) => (logic) => {
    const updatedFilters = {
      ...filters,
      [type]: [...filters[type], getNewItem(type, logic)],
    };

    onChange?.(updatedFilters);
  };

  const [doRelationships] = useValue(() => {
    return (
      doRelationshipsQuery.result?.map((r) => ({
        label: r.role,
        value: r.role,
      })) || []
    );
  }, [doRelationshipsQuery.result]);

  const doConditionFilters = filters.customKeyValues || filters.doConditions;
  const contactConditionFilters = filters.contactConditions;

  const doConditionsEmpty = !doConditionFilters?.length;
  const contactConditionsEmpty = !contactConditionFilters?.length;

  return (
    <>
      {!hideContactConditions && (
        <BodySection
          title={ContactConditionProps?.title || 'Contact Relationship'}
          style={{
            margin: '1.5rem 0 0',
            height: 'auto',

            ...(contactConditionsEmpty && {
              boxShadow: 'none',
            }),

            ...style,
          }}
          headerStyle={{
            ...(contactConditionsEmpty && {
              borderBottom: 0,
              borderRadius: 4,
            }),
          }}
          bodyStyle={{
            padding: '1rem',
            backgroundColor: theme.palette.common.white,

            ...(contactConditionsEmpty && {
              padding: 0,
            }),
          }}
          HeaderRightComp={
            contactConditionsEmpty && (
              <AddConditionBtn
                icon
                title="Add"
                onAddQuery={handleAdd(
                  ContactConditionProps?.name || 'contactConditions',
                )}
                disableSelection={contactConditionsEmpty}
              />
            )
          }>
          <FilterCriteriaForms
            loading={doRelationshipsQuery.initLoading}
            filters={contactConditionFilters}
            type={ContactConditionProps?.name || 'contactConditions'}
            options={doRelationships}
            onChange={handleChange(
              ContactConditionProps?.name || 'contactConditions',
            )}
            {...ContactConditionProps}
          />
        </BodySection>
      )}

      <BodySection
        title={DOConditionProps?.title || 'Field Conditions'}
        style={{
          margin: '1.5rem 0 0',
          height: 'auto',

          ...(doConditionsEmpty && {
            boxShadow: 'none',
          }),

          ...style,
        }}
        headerStyle={{
          ...(doConditionsEmpty && {
            borderBottom: 0,
            borderRadius: 4,
          }),
        }}
        bodyStyle={{
          padding: '1rem',
          backgroundColor: theme.palette.common.white,

          ...(doConditionsEmpty && {
            padding: 0,
          }),
        }}
        HeaderRightComp={
          doConditionsEmpty && (
            <AddConditionBtn
              icon
              title="Add"
              onAddQuery={handleAdd(DOConditionProps?.name || 'doConditions')}
              disableSelection={doConditionsEmpty}
            />
          )
        }>
        <FilterCriteriaForms
          loading={allFieldsQuery.initLoading}
          filters={doConditionFilters}
          type={DOConditionProps?.name || 'doConditions'}
          options={allFieldsQuery.data}
          onChange={handleChange(DOConditionProps?.name || 'doConditions')}
          {...DOConditionProps}
        />
      </BodySection>
    </>
  );
};

const FilterCriteriaForms = ({
  loading,
  filters,
  options,
  onChange,
  ...props
}) => {
  return (
    <CriteriaForms
      loading={loading}
      headerStyle={{
        display: 'none',
        boxShadow: 'none',
      }}
      style={{
        marginTop: 0,
        borderRadius: 0,
        margin: 0,
        boxShadow: 'none',
      }}
      bodyStyle={{
        padding: 0,
      }}
      data={filters}
      choices={options}
      getNewItem={getNewItem}
      onChange={onChange}
      {...props}
    />
  );
};

CriteriaFiltersForm.defaultProps = {
  filters: [],
};

export default CriteriaFiltersForm;
