import React, {useState, useEffect, useRef} from 'react';
import ContentCopyIcon from '@mui/icons-material/ContentCopy';
import Button from '@macanta/components/Button';
import Tooltip from '@macanta/components/Tooltip';

const CopyButton = ({
  label,
  text,
  tooltipTitle,
  iconComp,
  children,
  TooltipProps,
  open,
  ...props
}) => {
  const [showTooltip, setShowTooltip] = useState(false);

  const showEmptyWarningIntervalRef = useRef(null);

  const handleClick = () => {
    navigator.clipboard.writeText(text?.trim());

    setShowTooltip(true);

    clearTimeout(showEmptyWarningIntervalRef.current);

    showEmptyWarningIntervalRef.current = setTimeout(() => {
      setShowTooltip(false);
    }, 800);
  };

  useEffect(() => {
    if (open) {
      handleClick();
    }
  }, []);

  return (
    <Tooltip
      open={showTooltip}
      title={tooltipTitle}
      placement="left"
      {...TooltipProps}>
      <div>
        <Button
          color="info"
          variant="contained"
          startIcon={iconComp || <ContentCopyIcon />}
          onClick={handleClick}
          size="small"
          {...props}>
          {children || label}
        </Button>
      </div>
    </Tooltip>
  );
};

CopyButton.defaultProps = {
  label: 'Copy',
  text: '',
  tooltipTitle: 'Copied',
};

export default CopyButton;
