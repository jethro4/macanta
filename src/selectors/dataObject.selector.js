import {
  CONTACT_DEFAULT_FIELDS,
  CONTACT_DEFAULT_ADDITIONAL_FIELDS,
} from '@macanta/constants/contactFields';
import {getFieldValue} from '@macanta/selectors/field.selector';
import * as Storage from '@macanta/utils/storage';

const DEFAULT_CONTACT_FIELDS = [
  ...CONTACT_DEFAULT_FIELDS,
  ...CONTACT_DEFAULT_ADDITIONAL_FIELDS,
];

export const transformConnectedContacts = (
  connectedContacts = [],
  relationships = [],
) => {
  return connectedContacts.map((connectedContact) => {
    const filteredCRelationships = connectedContact.relationships.filter((cr) =>
      relationships.some((r) => r.id === cr.id),
    );
    const cRelationships = filteredCRelationships.map(({id: relId}) => {
      const {role} = relationships.find((r) => r.id === relId);

      return {
        id: relId,
        role,
      };
    });
    return {
      ...connectedContact,
      relationships: cRelationships,
    };
  });
};

export const getDOItemsDataWithValue = (
  doItems,
  fields,
  relationships,
  contactId,
) => {
  const doItemsData = doItems.reduce((acc, item) => {
    const connectedContacts = transformConnectedContacts(
      item.connectedContacts,
      relationships,
    );
    const primaryConnectedContact = connectedContacts[0];
    const itemObj = {
      id: item.id,
      ID: item.id,
      connectedContacts,
      'Connected Contacts': !primaryConnectedContact
        ? ''
        : [
            {
              id: primaryConnectedContact.contactId,
              value: `${primaryConnectedContact.firstName} ${primaryConnectedContact.lastName}`.trim(),
              label: `${primaryConnectedContact.firstName} ${primaryConnectedContact.lastName}`.trim(),
              subtext: primaryConnectedContact.relationships
                .map(({role}) => role)
                .join(', '),
              link: `/app/contact/${primaryConnectedContact.contactId}/notes`,
            },
          ],
    };

    fields.forEach((f) => {
      const data = item.data.find((d) => d.fieldId === f.id);
      itemObj[f.name] = getFieldValue({
        isEdit: false,
        field: f,
        itemData: data,
        contactId: contactId || primaryConnectedContact?.contactId,
      });
      itemObj[`${f.name}-contactSpecificValues`] = data?.contactSpecificValues;
    });

    return acc.concat(itemObj);
  }, []);

  return doItemsData;
};

export const filterDOItems = (filter, doItems, filterOptions = []) => {
  return doItems.reduce((acc, item) => {
    let filterString = '';

    if (filter) {
      const connectedContactValues = item.connectedContacts
        ?.map((d) => `${d.contactId}-${d.email}-${d.firstName}-${d.lastName}`)
        .join(' ');
      const dataValues = item.data.map((d) => d.value).join(' ');
      filterString = `${connectedContactValues || ''} ${dataValues}`
        .trim()
        .toLowerCase();
    }

    let shouldShow = filter
      ? filterString.includes(filter.toLowerCase())
      : true;

    filterOptions.forEach((filterOption) => {
      if (filterOption.type === 'noRelationships') {
        const contactIdToRemove = filterOption.filterVal;

        shouldShow =
          shouldShow &&
          !item.connectedContacts.some(
            (connectedContact) =>
              connectedContact.contactId === contactIdToRemove,
          );
      }
      if (
        filterOption.type === 'contactIds' &&
        filterOption.filterVal?.length
      ) {
        const primaryConnectedContact =
          item.connectedContacts && item.connectedContacts[0];

        shouldShow =
          shouldShow &&
          !!primaryConnectedContact &&
          filterOption.filterVal.includes(primaryConnectedContact.contactId);
      }
      if (filterOption.type === 'field') {
        shouldShow =
          shouldShow &&
          item.data.some(
            (d) =>
              d.fieldId === filterOption.fieldId &&
              d.value === filterOption.filterVal,
          );
      }
    });

    if (shouldShow) {
      return acc.concat(item);
    }

    return acc;
  }, []);
};

export const filterAndTransformFieldValues = (values = {}, fields) => {
  let filteredValues = {};
  const checkboxFields =
    fields?.filter((f) => f.type === 'Checkbox').map((f) => f.name) || [];

  Object.entries(values).forEach(([key, value]) => {
    if (checkboxFields.includes(key)) {
      filteredValues[key] = `~overwrite~${value ? ',' : ''}` + value;
    } else {
      filteredValues[key] = value;
    }
  });

  return filteredValues;
};

export const filterFieldsForTable = ({fields, hideContacts, transform}) => {
  const tableFields = fields.filter((field) => field.showInTable);
  const fieldsData = tableFields.length
    ? tableFields.slice().sort((item1, item2) => {
        return item1.showOrder - item2.showOrder;
      })
    : fields.slice(0, 3);

  // let columnsWithId = [{name: 'ID'}].concat(fieldsData);
  let columnsWithId = fieldsData;

  if (!hideContacts) {
    columnsWithId = columnsWithId.concat({
      name: 'Connected Contacts',
      minWidth: 200,
    });
  }

  columnsWithId = columnsWithId.map((field) => ({
    id: field.name,
    label:
      DEFAULT_CONTACT_FIELDS.find(
        (defaultField) => defaultField.name === field.name,
      )?.label || field.name,
    subtext: field.subtext,
    minWidth: field.minWidth || 170,
    ...(field.name !== 'Connected Contacts' && {
      transform,
    }),
  }));

  return columnsWithId;
};

export const getDefaultConnectedContactRelationships = ({
  contactId,
  relationships,
  defaultConnectedContact,
}) => {
  const loggedInUser = Storage.getItem('userDetails');

  const isLoggedInUser = loggedInUser.userId === contactId;

  const loggedInUserRelationships =
    relationships?.filter((r) => r.autoAssignLoggedInUser) || [];
  const contactRelationships =
    relationships?.filter((r) => r.autoAssignContact) || [];

  let relationshipsArr = [];

  if (isLoggedInUser || (!isLoggedInUser && loggedInUserRelationships.length)) {
    relationshipsArr.push({
      contactId: loggedInUser.userId,
      email: loggedInUser.email,
      firstName: loggedInUser.firstName,
      lastName: loggedInUser.lastName,
      relationships: loggedInUserRelationships,
    });
  }

  if (!isLoggedInUser && defaultConnectedContact) {
    relationshipsArr.push({
      contactId: defaultConnectedContact.id,
      email: defaultConnectedContact.email,
      firstName: defaultConnectedContact.firstName,
      lastName: defaultConnectedContact.lastName,
      relationships: contactRelationships,
    });
  }

  return relationshipsArr;
};

export const getDefaultConnectedContacts = ({
  connectedContacts,
  relationships,
  contactId,
  defaultConnectedContact,
}) => {
  return connectedContacts
    ? transformConnectedContacts(connectedContacts, relationships)
    : getDefaultConnectedContactRelationships({
        contactId,
        relationships,
        defaultConnectedContact,
      });
};

export const transformAttachments = ({
  attachments,
  connectedContacts,
  field: fieldArg,
  fields: fieldsArr,
  contactId,
}) => {
  let fieldsMap = {};

  if (fieldsArr) {
    fieldsMap =
      fieldsArr?.reduce((acc, f) => {
        acc[f.id] = {
          fieldId: f.id,
          ...f,
        };

        return acc;
      }, {}) || {};
  }

  return (
    attachments
      ?.filter((attachment) => {
        if (!fieldArg && !attachment.meta?.referenceFieldId) {
          return true;
        }

        let field = fieldArg || fieldsMap[attachment.meta?.referenceFieldId];

        return (
          field &&
          field.id === attachment.meta?.referenceFieldId &&
          (!field.contactSpecificField ||
            attachment.meta?.contactId === contactId)
        );
      })
      .map((attachment) => {
        const connectedContact = connectedContacts.find(
          (contact) => contact.contactId === contactId,
        );

        let field = fieldArg || fieldsMap[attachment.meta?.referenceFieldId];

        return !connectedContact
          ? null
          : {
              ...attachment,
              contactName: `${connectedContact.firstName || ''} ${
                connectedContact.lastName || ''
              }`.trim(),
              fieldName: field?.name,
              isGeneral: !field,
            };
      })
      .filter((attachment) => attachment) || []
  );
};
