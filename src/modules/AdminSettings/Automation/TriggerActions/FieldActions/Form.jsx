import React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import FlexGrid from '@macanta/components/Base/FlexGrid';
import BasicInfoForm from '@macanta/modules/AdminSettings/Automation/TriggerActions/SubForms/BasicInfoForm';
import withForm from '@macanta/components/Form/withForm';
import useDORelationships from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDORelationships';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import BodySection from '@macanta/modules/AdminSettings/UserManagement/BodySection';
import Divider from '@macanta/components/Base/Divider';
import {contains, findItem} from '@macanta/utils/array';
import FieldValueField from '@macanta/modules/AdminSettings/Automation/TriggerActions/FieldActions/FieldValueField';
import FormField from '@macanta/containers/FormField';
import {formatDate, getNow} from '@macanta/utils/time';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import ActionForm from '@macanta/modules/AdminSettings/Automation/TriggerActions/SubForms/ActionForm';
import {FIELD_ACTION_TYPES} from '@macanta/constants/automation';

let Form = (props) => {
  const {setFieldValue, values} = props;

  const doTypesQuery = useDOTypes({
    removeEmail: true,
    includeContactObject: false,
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });
  const doRelationshipsQuery = useDORelationships(values.groupId, {
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });
  // const [data, setData] = useState({});

  // const handleChange = (val) => {};

  // const handleSave = () => {
  //   onSave();
  // };

  const fieldKeys = findItem(FIELD_ACTION_TYPES, {
    value: values.actionType,
  })?.fieldKeys;
  const hasDateFormat = contains(fieldKeys, [
    'fieldNameFormat',
    'birthdayResultFormat',
  ]);

  return (
    <ActionForm>
      <FlexGrid
        {...props}
        cols={2}
        margin={[0, '2rem']}
        // border={[0, 1]}
        sx={{
          padding: '1rem',
          height: '100%',
          ...props.sx,
        }}>
        <Box colWidth={0.7}>
          <BasicInfoForm
            labelPosition="normal"
            actionLabel="Field Action Name"
            renderOtherForms={() => (
              <FormField
                name="doTitle"
                labelPosition="normal"
                loading={doTypesQuery.initLoading}
                type="Select"
                options={doTypesQuery.result}
                onChange={(val, item) => setFieldValue('groupId', item.id)}
                labelKey="title"
                valueKey="title"
                label="Data Object Type"
                fullWidth
                size="small"
                variant="outlined"
              />
            )}
          />
        </Box>

        <BodySection
          title="Field Settings"
          bodyStyle={{
            padding: '1rem',
          }}>
          <FlexGrid cols={2} margin={['1rem', '1.5rem']}>
            <FormField
              name="actionType"
              labelPosition="normal"
              type="Select"
              options={FIELD_ACTION_TYPES}
              valueKey="name"
              label="Action Type"
              size="small"
              variant="outlined"
            />

            {!!values.groupId && (
              <FormField
                name="contactRelationship"
                labelPosition="normal"
                loading={doRelationshipsQuery.initLoading}
                type="Select"
                options={doRelationshipsQuery.result}
                labelKey="role"
                valueKey="id"
                label="Contact Relationship"
                size="small"
                variant="outlined"
              />
            )}
          </FlexGrid>

          {!!values.actionType && (
            <>
              <Divider
                style={{
                  margin: '1.5rem 0 1rem',
                  borderBottom: '1px dashed #eee',
                }}
              />

              <Typography
                sx={{
                  marginBottom: '8px',
                  color: 'primary.main',
                  fontWeight: 'bold',
                }}>
                Field Values
              </Typography>

              <FlexGrid
                cols={3}
                margin={['1rem', '1rem']}
                style={{
                  marginBottom: '1rem',
                }}>
                {fieldKeys.map((fieldKey) => {
                  return (
                    <FieldValueField
                      key={fieldKey}
                      fieldKey={fieldKey}
                      groupId={values.groupId}
                      actionType={values.actionType}
                    />
                  );
                })}
              </FlexGrid>

              {hasDateFormat && (
                <Typography variant="caption">
                  <Typography
                    style={{
                      marginRight: '4px',
                      fontSize: '1em',
                    }}
                    sx={{
                      color: 'info.main',
                    }}
                    component="span">
                    *
                  </Typography>
                  If "Date Field" is a datetime field, time will be included
                  e.g. {formatDate(getNow(), 'dddd DD MMMM YYYY hh:mmA')}
                </Typography>
              )}
            </>
          )}
        </BodySection>
      </FlexGrid>
    </ActionForm>
  );
};

Form = withForm(Form);

export default Form;
