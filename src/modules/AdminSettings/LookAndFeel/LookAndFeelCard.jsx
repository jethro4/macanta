import React from 'react';
import FormatColorFillIcon from '@mui/icons-material/FormatColorFill';
import SettingCard from '@macanta/modules/AdminSettings/SettingCard';
import {navigate} from 'gatsby';

const LookAndFeelCard = () => {
  /* const navigate = useNavigate();*/

  const handleBlastProgress = () => {
    navigate('/app/admin-settings/look-and-feel');
  };

  return (
    <SettingCard
      IconComp={FormatColorFillIcon}
      title="Look & Feel"
      description="Customize your brand logo and theme color tailored to your company"
      onClick={handleBlastProgress}
    />
  );
};

export default LookAndFeelCard;
