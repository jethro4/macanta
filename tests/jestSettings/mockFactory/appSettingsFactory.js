const typename = 'AppSettings';

export default (overwrites) => {
  return {
    __typename: typename,
    uiColour: '#663399',
    appTimeZone: 'Europe/London',
    macantaTabOrder: ['Notes', 'DO Type 1', 'DO Type 2'],
    sitename: 'macanta | Macanta Staging',
    indirectRelationshipGroups: ['DataObject2', 'DataObject3'],
    filteredEmailDomain: ['google.com', 'gmail.com'],
    macantaPlanId: 'macanta_tier4',
    enableCDImport: true,
    generatedEmailFormat: 'ContactIdFirstNameLastName@staging.com',
    loginDisabledMessage: '',
    customLogo: {
      imageData:
        'iVBORw0KGgoAAAANSUhEUgAAAAgAAAAIAQMAAAD+wSzIAAAABlBMVEX///+/v7+jQ3Y5AAAADklEQVQI12P4AIX8EAgALgAD/aNpbtEAAAAASUVORK5CYII',
      filename: 'logo.png',
      type: 'image/png',
      __typename: 'CustomLogo',
    },
    ...overwrites,
  };
};
