import React from 'react';
import EditIcon from '@mui/icons-material/Edit';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import {PopoverButton, DialogButton} from '@macanta/components/Button';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import Section from '@macanta/containers/Section';
import DataTable from '@macanta/containers/DataTable';
import UserForm from '@macanta/modules/AdminSettings/UserManagement/UserForm';
import AppSubscriptionForm from '@macanta/modules/AdminSettings/UserManagement/AppSubscriptionForm';
import useRetryQuery from '@macanta/hooks/apollo/useRetryQuery';
import useMutation from '@macanta/hooks/apollo/useMutation';
import useValue from '@macanta/hooks/base/useValue';
import useUsers from '@macanta/hooks/admin/useUsers';
import usePermissionTemplates from '@macanta/hooks/admin/usePermissionTemplates';
import useAppSubscription from '@macanta/hooks/app/useAppSubscription';
import {DELETE_USER, LIST_VALID_DOMAIN_EMAILS} from '@macanta/graphql/users';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {transformUser} from '@macanta/selectors/user.selector';
import EmailValidationBtn from './EmailValidationBtn';

const DEFAULT_USERS_COLUMNS = [
  {
    id: 'domain.valid',
    label: '',
    disableSort: true,
    disableResize: true,
    minWidth: 50,
    maxWidth: 50,
    renderBodyCellValue: ({item}) => {
      return <EmailValidationBtn email={item.email} />;
    },
  },
  {
    id: 'email',
    label: 'Email',
    minWidth: 160,
  },
  {
    id: 'contactLevel',
    label: 'User Level',
    minWidth: 90,
    maxWidth: 140,
  },
  {
    id: 'templateId',
    label: 'Permissions Template',
    minWidth: 140,
    renderBodyCellValue: ({value}) => <TemplateValue id={value} />,
  },
];

const UsersSection = (props) => {
  const usersQuery = useUsers({
    fetchPolicy: FETCH_POLICIES.CACHE_AND_NETWORK,
  });

  const validDomainsQuery = useRetryQuery(LIST_VALID_DOMAIN_EMAILS, {
    fetchPolicy: FETCH_POLICIES.CACHE_AND_NETWORK,
  });

  const appSubscriptionQuery = useAppSubscription();

  const [callDeleteUserMutation, deleteUserMutation] = useMutation(
    DELETE_USER,
    {
      successMessage: 'Deleted successfully',
      update(cache, {data: {deleteUser}}) {
        const normalizedId = cache.identify({
          id: deleteUser.id,
          __typename: 'User',
        });
        cache.evict({id: normalizedId});
        cache.gc();
      },
    },
  );
  const [users] = useValue(
    () => usersQuery.data?.map((user) => transformUser(user)) || [],
    [usersQuery.data],
  );

  const handleDelete = (id) => {
    callDeleteUserMutation({
      variables: {
        id,
      },
    });
  };

  return (
    <>
      <Section
        fullHeight
        title="Users"
        style={{
          marginTop: 0,
        }}
        HeaderRightComp={
          appSubscriptionQuery.initLoading ? (
            <LoadingIndicator transparent size={20} />
          ) : (
            <AppSubscriptionForm />
          )
        }
        {...props}>
        <DataTable
          fullHeight
          loading={usersQuery.loading || validDomainsQuery.loading}
          columns={DEFAULT_USERS_COLUMNS}
          data={users}
          actionColumn={{
            show: true,
            style: {
              minWidth: 100,
            },
          }}
          renderActionButtons={({item}) => (
            <Box
              style={{
                marginRight: -10,
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <PopoverButton
                icon
                sx={{
                  padding: '0.25rem',
                  color: 'info.main',
                }}
                // onClick={handleAdd}
                size="small"
                TooltipProps={{
                  title: 'Edit',
                }}
                PopoverProps={{
                  title: `Edit (${item.id})`,
                  contentWidth: 380,
                  anchorOrigin: {
                    vertical: 'bottom',
                    horizontal: 'center',
                  },
                  transformOrigin: {
                    vertical: -8,
                    horizontal: 'center',
                  },
                }}
                renderContent={(handleClose) => (
                  <UserForm item={item} onClose={handleClose} />
                )}>
                <EditIcon
                  style={{
                    fontSize: 19,
                  }}
                />
              </PopoverButton>
              <DialogButton
                icon
                size="small"
                TooltipProps={{
                  title: 'Delete',
                }}
                DialogProps={{
                  title: 'Are you sure?',
                  description: [
                    `You are about to delete "${item.email}" from Users.`,
                    'This cannot be undone. Do you want to proceed?',
                  ],
                  onActions: (handleClose) => {
                    return [
                      {
                        label: 'Delete',
                        disabledDelay: 3,
                        onClick: async () => {
                          await handleDelete(item.id);
                          handleClose();
                        },
                        startIcon: <DeleteForeverIcon />,
                      },
                    ];
                  },
                }}>
                <DeleteForeverIcon
                  sx={{
                    color: 'grayDarker.main',
                  }}
                />
              </DialogButton>
            </Box>
          )}
        />
      </Section>
      <LoadingIndicator modal loading={deleteUserMutation.loading} />
    </>
  );
};

const TemplateValue = ({id}) => {
  const permissionTemplatesQuery = usePermissionTemplates(id);
  const permissionTemplate = permissionTemplatesQuery.selectedTemplate;

  return (
    <Typography
      sx={{
        fontSize: 'inherit',
      }}>
      {permissionTemplate?.name}
    </Typography>
  );
};

export default UsersSection;
