import React, {useState} from 'react';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import AttachFileIcon from '@mui/icons-material/AttachFile';
import Tooltip from '@macanta/components/Tooltip';
import Popover from '@macanta/components/Popover';
import AppsIcon from '@mui/icons-material/Apps';
import ComputerIcon from '@mui/icons-material/Computer';
import LinkIcon from '@mui/icons-material/Link';
import AttachmentOptionDO from './AttachmentOptionDO';
import AttachmentOptionComputer from './AttachmentOptionComputer';
import AttachmentOptionURL from './AttachmentOptionURL';
import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import * as Styled from './styles';

const OptionView = ({
  doItemId,
  selectedAttachments,
  selectedType,
  onSelect,
  onClose,
}) => {
  let optionView = null;

  const handleSave = (data, type) => {
    if (type === 'url') {
      onSelect([
        {
          id: data.id,
          isNew: data.isNew,
          thumbnail: data.thumbnail,
          fileName: data.fileName,
          fileExt: data.fileExt,
          isPDF: data.isPDF,
          isUrl: data.isUrl,
        },
      ]);
    } else if (type === 'do') {
      const files = data.map((d) => {
        return {
          ...d,
          type: 'do',
        };
      });

      onSelect(files, 'do');
    } else {
      const files = data.map((d) => {
        return {
          id: d.id,
          isNew: d.isNew,
          thumbnail: d.thumbnail,
          fileName: d.fileName,
          fileExt: d.fileExt,
          isPDF: d.isPDF,
        };
      });

      onSelect(files);
    }
  };

  switch (selectedType) {
    case 'do': {
      optionView = (
        <AttachmentOptionDO
          showModal
          doItemId={doItemId}
          selectedAttachments={selectedAttachments}
          onClose={onClose}
          onSelect={handleSave}
        />
      );
      break;
    }
    case 'computer': {
      optionView = (
        <AttachmentOptionComputer
          showModal
          onClose={onClose}
          onSelect={handleSave}
        />
      );
      break;
    }
    case 'url': {
      optionView = (
        <AttachmentOptionURL
          showModal
          onClose={onClose}
          onSelect={handleSave}
        />
      );
      break;
    }
  }

  return optionView;
};

const EmailAttachmentOptions = ({onSelect, doItemId, selectedAttachments}) => {
  const [anchorEl, setAnchorEl] = useState(false);
  const [selectedType, setSelectedType] = useState('');

  const openMoreOptions = Boolean(anchorEl);

  const handleMoreOptions = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleCloseMoreOptions = () => {
    setAnchorEl(null);
  };

  const handleSelectType = (type) => () => {
    setSelectedType(type);
    handleCloseMoreOptions();
  };

  const handleClose = () => {
    setSelectedType('');
  };

  return (
    <>
      <Tooltip title="Attach Files" placement="top">
        <div>
          <Styled.EmailFooterIconButton
            color="secondary"
            onClick={handleMoreOptions}
            size="small">
            <AttachFileIcon />
          </Styled.EmailFooterIconButton>
        </div>
      </Tooltip>
      <Popover
        title="Attach Files"
        open={openMoreOptions}
        anchorEl={anchorEl}
        onClose={handleCloseMoreOptions}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}>
        <div
          style={{
            width: 180,
          }}>
          <List
            style={{
              padding: 0,
            }}>
            {!!doItemId && (
              <>
                <NoteTaskHistoryStyled.SelectionItem
                  button
                  onClick={handleSelectType('do')}>
                  <AppsIcon />
                  <NoteTaskHistoryStyled.SelectionItemText primary="DO Attachments" />
                </NoteTaskHistoryStyled.SelectionItem>
                <Divider />
              </>
            )}
            <NoteTaskHistoryStyled.SelectionItem
              button
              onClick={handleSelectType('computer')}>
              <ComputerIcon />
              <NoteTaskHistoryStyled.SelectionItemText primary="Computer" />
            </NoteTaskHistoryStyled.SelectionItem>
            <Divider />
            <NoteTaskHistoryStyled.SelectionItem
              button
              onClick={handleSelectType('url')}>
              <LinkIcon />
              <NoteTaskHistoryStyled.SelectionItemText primary="URL" />
            </NoteTaskHistoryStyled.SelectionItem>
          </List>
        </div>
      </Popover>
      <OptionView
        doItemId={doItemId}
        selectedAttachments={selectedAttachments}
        selectedType={selectedType}
        onSelect={onSelect}
        onClose={handleClose}
      />
    </>
  );
};

export default EmailAttachmentOptions;
