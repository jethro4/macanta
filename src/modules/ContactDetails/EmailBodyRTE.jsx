import React from 'react';
import Box from '@mui/material/Box';
import {useLazyQuery} from '@apollo/client';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import {GET_EMAIL_TEMPLATE} from '@macanta/graphql/admin';
import useValue from '@macanta/hooks/base/useValue';
import useEmailMergeFields from '@macanta/hooks/useEmailMergeFields';
import useEmailTemplates from '@macanta/hooks/useEmailTemplates';
import * as Styled from './styles';

const EmailBodyRTE = ({
  types,
  value: valueProp,
  whitelist,
  onChange,
  ...props
}) => {
  const [value, setValue] = useValue(valueProp);

  const {
    senderFields,
    contactFields,
    doMergeItems,
    loading: loadingMergeFields,
  } = useEmailMergeFields({
    types,
  });
  const emailTemplatesQuery = useEmailTemplates();

  const [callEmailTemplateQuery, emailTemplateQuery] = useLazyQuery(
    GET_EMAIL_TEMPLATE,
    {
      onCompleted(data) {
        if (data.getEmailTemplate) {
          setValue(data.getEmailTemplate.html);
        }
      },
    },
  );

  const handleChange = (newVal) => {
    setValue(newVal);
    onChange(newVal);
  };

  return (
    <Box
      style={{
        position: 'relative',
        display: 'flex',
        flexDirection: 'column',
        flex: 1,
      }}>
      <Styled.EmailRichTextField
        key={
          whitelist.join(',') +
          String(loadingMergeFields) +
          String(emailTemplatesQuery.initLoading)
        }
        loading={emailTemplatesQuery.initLoading}
        richText
        value={value}
        onChange={handleChange}
        customMenu={[
          {
            id: 'mergeFields',
            title: 'Merge Fields',
            menuItems: [
              {
                id: 'senderFields',
                title: 'Sender Fields',
                items: [
                  {title: 'Signature', content: '~Sender.Signature~'},
                ].concat(
                  senderFields.map((d) => ({
                    title: d.label,
                    content: d.value,
                  })),
                ),
              },
              {
                id: 'contactFields',
                title: 'Contact Fields',
                items: contactFields.map((d) => ({
                  title: d.label,
                  content: d.value,
                })),
              },
              ...doMergeItems
                .filter((f) => whitelist.includes(f.category))
                .map((f) => ({
                  id: f.category,
                  title: f.category,
                  items: f.entries.map((d) => ({
                    title: d.label,
                    content: d.value,
                  })),
                })),
            ],
          },
          {
            id: 'templates',
            title: 'Use Template',
            menuItems: !emailTemplatesQuery.initLoading
              ? emailTemplatesQuery.templates.length
                ? emailTemplatesQuery.templates.map((template) => ({
                    id: template.templateId,
                    title: template.title,
                    onAction: () => {
                      callEmailTemplateQuery({
                        variables: {
                          templateId: template.templateId,
                        },
                      });
                    },
                  }))
                : [
                    {
                      id: 'noTemplates',
                      title: 'No templates found',
                      disabled: true,
                      onAction: () => {},
                    },
                  ]
              : [
                  {
                    id: 'loading',
                    title: 'Loading Templates...',
                    disabled: true,
                    onAction: () => {},
                  },
                ],
          },
        ]}
        {...props}
      />
      {emailTemplateQuery.loading && <LoadingIndicator fill />}
    </Box>
  );
};

EmailBodyRTE.defaultProps = {
  whitelist: [],
};

export default EmailBodyRTE;
