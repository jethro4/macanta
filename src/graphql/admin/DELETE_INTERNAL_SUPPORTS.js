import {gql} from '@apollo/client';

export default gql`
  mutation deleteInternalSupports(
    $deleteInternalSupportsInput: DeleteInternalSupportsInput!
  ) {
    deleteInternalSupports(input: $deleteInternalSupportsInput) {
      success
    }
  }
`;
