import {gql} from '@apollo/client';

export const DO_TO_DO_RELATIONSHIP_FRAGMENT = gql`
  fragment doToDORelationshipFragment on DOToDORelationship {
    id
    doItemId
    groupId
    type
    data {
      fieldName
      value
    }
  }
`;
