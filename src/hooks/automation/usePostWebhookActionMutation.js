import useMutation from '@macanta/hooks/apollo/useMutation';
import {POST_WEBHOOK_ACTION} from '@macanta/graphql/automation';

const usePostWebhookActionMutation = (options) => {
  const mutation = useMutation(POST_WEBHOOK_ACTION, options);

  return mutation;
};

export default usePostWebhookActionMutation;
