import {experimentalStyled as styled} from '@mui/material/styles';
import MUIStepper from '@mui/material/Stepper';
import MUIStep from '@mui/material/Step';
import MUIStepLabel from '@mui/material/StepLabel';
import MUIStepConnector from '@mui/material/StepConnector';

export const Stepper = styled(MUIStepper)``;

export const Step = styled(MUIStep)``;

export const StepLabel = styled(MUIStepLabel)``;

export const StepConnector = styled(MUIStepConnector)``;
