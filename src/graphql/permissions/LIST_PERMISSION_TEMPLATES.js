import {gql} from '@apollo/client';
import {PERMISSIONS_FRAGMENT} from './fragments';

export default gql`
  query listPermissionTemplates {
    listPermissionTemplates {
      id
      name
      description
      status
      permission {
        ...permissionsFragment
      }
    }
  }
  ${PERMISSIONS_FRAGMENT}
`;
