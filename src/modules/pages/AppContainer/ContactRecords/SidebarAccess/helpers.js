export const getDOMetaData = (metadataPath = '') => {
  const params = metadataPath.split('/');
  const groupId = params[0];
  const doTitle = params[1];

  return {groupId, doTitle};
};
