import {useState, useLayoutEffect} from 'react';
import useUserGuides from '@macanta/hooks/useUserGuides';

const useNotesTasksGuide = () => {
  const [notesTasksGuide, setNotesTasks] = useState();
  const {general} = useUserGuides();

  useLayoutEffect(() => {
    setNotesTasks(general.find((guide) => guide.subType === 'Notes & Tasks'));
  }, [general]);

  return notesTasksGuide;
};

export default useNotesTasksGuide;
