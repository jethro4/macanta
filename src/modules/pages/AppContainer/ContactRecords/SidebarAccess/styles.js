import {experimentalStyled as styled} from '@mui/material/styles';

import {ListItemLink} from '@macanta/components/List';
import MUIListItemText from '@mui/material/ListItemText';

export const ListItem = styled(ListItemLink)`
  height: 40px;
  padding: 4px 1rem;
`;

export const ListItemText = styled(MUIListItemText)`
  user-select: none;
`;
