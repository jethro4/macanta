import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import useQuery from '@macanta/hooks/apollo/useQuery';
import {GET_EMAIL_SIGNATURE} from '@macanta/graphql/admin';
import {isLoggedIn} from '@macanta/utils/app';
import * as Storage from '@macanta/utils/storage';

const useSignature = (options) => {
  const userDetails = Storage.getItem('userDetails');

  const {data, loading} = useQuery(GET_EMAIL_SIGNATURE, {
    variables: {
      userId: userDetails?.userId,
    },
    skip: !isLoggedIn(),
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
    onError() {},
    ...options,
  });

  const emailSignature = data?.getEmailSignature?.html;

  return {data: emailSignature, loading};
};

export default useSignature;
