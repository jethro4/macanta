import {useState} from 'react';
import isString from 'lodash/isString';
import {getIsSubHeader} from '@macanta/components/Forms/ChoiceFields/SelectField';
import {insert, sortArrayByObjectKey} from '@macanta/utils/array';
import useDeepCompareEffect from './useDeepCompareEffect';

export const transformChoices = (
  items,
  sort,
  keys = {},
  IconComp,
  getOptionLabel,
  blankOption,
) => {
  const {
    labelKey = 'label',
    valueKey = 'value',
    subLabelKey = 'subLabel',
    captionKey = 'caption',
  } = keys;

  const transformedItems = !items
    ? []
    : items
        .filter((item, index) => {
          if (item?.isSubheader) {
            const showSubheader = getIsSubHeader({
              isSubheader: item.isSubheader,
              nextIsSubheader: items[index + 1]?.isSubheader,
              isLastItem: index === items.length - 1,
            });

            return showSubheader;
          }

          return !!item;
        })
        .map((item) =>
          Object.assign(
            {},
            !isString(item)
              ? {
                  ...(IconComp && {
                    IconComp,
                  }),
                  ...item,
                  ...(item[valueKey] && {
                    value: item[valueKey],
                  }),
                  ...(item[subLabelKey] && {
                    subLabel: item[subLabelKey],
                  }),
                  ...(item[captionKey] && {
                    caption: item[captionKey],
                  }),
                }
              : {
                  value: item,
                },
            getOptionLabel
              ? {
                  label: getOptionLabel(item),
                }
              : !isString(item)
              ? item[labelKey] && {
                  label: item[labelKey],
                }
              : {
                  label: item,
                },
          ),
        );

  let sortedItems = !sort
    ? transformedItems
    : sortArrayByObjectKey(transformedItems, 'label', sort);

  if (blankOption) {
    const blankOptionLabel = isString(blankOption) ? blankOption : 'None';

    sortedItems = insert({
      arr: sortedItems,
      item: {label: blankOptionLabel, value: ''},
      index: 0,
    });
  }

  return sortedItems;
};

const useTransformChoices = ({
  options,
  sort,
  IconComp,
  getOptionLabel,
  blankOption,
  ...keys
}) => {
  const [choices, setChoices] = useState(() =>
    transformChoices(
      options,
      sort,
      keys,
      IconComp,
      getOptionLabel,
      blankOption,
    ),
  );

  useDeepCompareEffect(() => {
    setChoices(
      transformChoices(
        options,
        sort,
        keys,
        IconComp,
        getOptionLabel,
        blankOption,
      ),
    );
  }, [options]);

  return choices;
};

export default useTransformChoices;
