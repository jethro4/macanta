import React from 'react';
import AddIcon from '@material-ui/icons/Add';
import DataTable from '@macanta/containers/DataTable';
import {
  useDeleteAutomationsMutation,
  useTriggerConditions,
  usePostTriggerConditionMutation,
} from '@macanta/hooks/automation';
import ItemActionButtons from '@macanta/modules/AdminSettings/Automation/TriggerActions/ItemActionButtons';
import Section from '@macanta/containers/Section';
import {writeItemInListFragment} from '@macanta/graphql/helpers';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import TriggerConditionForm from './TriggerConditionForm';
import {TRIGGER_CONDITIONS_FRAGMENT} from '@macanta/graphql/automation/GET_TRIGGER_CONDITIONS';
import EditButton from '@macanta/containers/buttons/EditButton';

const MODAL_WIDTH = 1250;

const COLUMNS = [
  {
    id: 'name',
    label: 'Name',
    maxWidth: '25%',
  },
  {
    id: 'description',
    label: 'Description',
    maxWidth: '30%',
  },
  {
    id: 'restartAfter',
    label: 'Restart After',
    maxWidth: '30%',
    emptyMessage: 'never',
    transform: ({value}) => (value === 'never' ? '' : value?.trim()),
  },
  {
    id: 'doTitle',
    label: 'Data Object Type',
    maxWidth: '20%',
  },
  {
    id: 'action',
    label: '',
    disableSort: true,
    disableResize: true,
    minWidth: 100,
    maxWidth: 100,
    renderBodyCellValue: ({item, handlers}) => {
      return (
        <ItemActionButtons
          modal
          type="Trigger Conditions"
          containerProps={{
            contentWidth: MODAL_WIDTH,
          }}
          item={item}
          onDelete={handlers.onDelete}
          renderFormComponent={(handleClose) => (
            <TriggerConditionForm
              options={{
                variables: {
                  id: item.id,
                },
              }}
              initialValues={item}
              onSubmit={async (values) => {
                const {result} = await handlers.onSave(values);

                if (result) {
                  handleClose();
                }
              }}
            />
          )}
        />
      );
    },
  },
];

const TriggerConditionsSection = (props) => {
  const triggerConditionsQuery = useTriggerConditions();

  const [callPostMutation, actionMutation] = usePostTriggerConditionMutation({
    update(cache, {result}, options) {
      console.debug('result:', result);
      writeItemInListFragment({
        isCreate: !options.variables.input?.id,
        keyField: 'queryId',
        item: result,
        typeName: 'TriggerConditionsItems',
        fragment: TRIGGER_CONDITIONS_FRAGMENT,
        parentKey: 'getTriggerConditions',
        listKey: 'items',
        position: 'first',
      });
    },
  });

  const [callDeleteMutation, deleteMutation] = useDeleteAutomationsMutation({
    typeName: 'TriggerConditionsItems',
  });

  const handleSave = async (values) => {
    const variables = {
      id: values.id,
      doType: values.doTitle,
      name: values.name,
      description: values.description,
      restartAfter: values.restartAfter,
      fileAttachedByUser: values.fileAttachedByUser,
      fileAttachedByContact: values.fileAttachedByContact,
      doConditions: values.doConditions.map((item) => ({
        fieldName: item.name,
        logic: item.logic,
        operator: item.operator,
        value: item.value,
        values: item.values,
      })),
      contactConditions: values.contactConditions.map((item) => ({
        relationship: item.relationship,
        logic: item.logic || 'and',
      })),
    };

    return callPostMutation({
      variables,
    });
  };

  const handleDelete = (id) => {
    return callDeleteMutation({
      variables: {
        id,
      },
    });
  };

  return (
    <>
      <Section
        style={{
          margin: 0,
        }}
        fullHeight
        title={`Trigger Conditions`}
        HeaderRightComp={
          <EditButton
            modal
            size="small"
            variant="contained"
            startIcon={<AddIcon />}
            containerProps={{
              headerTitle: `Add Trigger Condition`,
              contentWidth: MODAL_WIDTH,
              bodyStyle: {
                backgroundColor: '#f5f6fA',
              },
            }}
            renderContent={(handleClose) => (
              <TriggerConditionForm
                onSubmit={async (values) => {
                  const {result} = await handleSave(values);

                  if (result) {
                    handleClose();
                  }
                }}
              />
            )}>
            Add Trigger Condition
          </EditButton>
        }>
        <DataTable
          loading={triggerConditionsQuery.loading}
          data={triggerConditionsQuery.result}
          fullHeight
          rowsPerPage={25}
          rowsPerPageOptions={[25, 50, 100]}
          {...props}
          columns={COLUMNS}
          handlers={{
            onSave: handleSave,
            onDelete: handleDelete,
          }}
        />
      </Section>
      <LoadingIndicator
        modal
        loading={actionMutation.loading || deleteMutation.loading}
      />
    </>
  );
};

export default TriggerConditionsSection;
