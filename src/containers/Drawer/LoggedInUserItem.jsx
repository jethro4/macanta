import React from 'react';
import Tooltip from '@macanta/components/Tooltip';
import * as Styled from './styles';
import * as Storage from '@macanta/utils/storage';

const LoggedInUserItem = ({expanded}) => {
  const {firstName, lastName, userId} = Storage.getItem('userDetails');

  const fullName = `${firstName || ''} ${lastName || ''}`.trim();

  return (
    <Tooltip title={fullName} placement="right">
      <Styled.UserListItem
        id={`user-record-button`}
        avatar
        to={`/app/contact/${userId}/notes`}
        label={fullName}
        expanded={expanded}
      />
    </Tooltip>
  );
};

export default LoggedInUserItem;
