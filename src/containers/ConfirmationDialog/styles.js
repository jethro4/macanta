import {experimentalStyled as styled} from '@mui/material/styles';

import Box from '@mui/material/Box';
import DialogComp from '@macanta/components/Dialog';

export const Root = styled(DialogComp)`
  display: flex;
  flex-direction: column;
`;

export const TitleContainer = styled(Box)`
  display: flex;
  align-items: center;
  min-width: 300px;
`;
