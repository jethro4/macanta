import {delay, syncPromises, asyncPromises} from '@macanta/utils/promise';

describe('Promise Utils', () => {
  describe('delay', () => {
    it('happy - delay with timeout', async () => {
      expect.assertions(1);
      return expect(delay(10, 'SampleVal')).resolves.toEqual('SampleVal');
    });
  });

  describe('syncPromises', () => {
    it('happy - resolve promise chain synchronously', async () => {
      const promiseArr = [
        () => delay(20, 'Delay 20'),
        () => delay(30, 'Delay 30'),
        () => delay(10, 'Delay 10'),
      ];

      const actualResult = syncPromises(promiseArr);
      const expectedResult = ['Delay 20', 'Delay 30', 'Delay 10'];

      return expect(actualResult).resolves.toEqual(expectedResult);
    });

    it('happy - resolve promise chain synchronously default return value', async () => {
      const values = ['SampleVal1', 'SampleVal2', 'SampleVal3'];

      const actualResult = syncPromises(values);
      const expectedResult = values;

      return expect(actualResult).resolves.toEqual(expectedResult);
    });

    it('happy - resolve promise chain synchronously with transform callback and default Value', async () => {
      const values = ['SampleVal1', 'SampleVal2', 'SampleVal3'];

      const actualResult = syncPromises(
        values,
        async (val, index) => await delay((index + 1) * 10, val),
      );
      const expectedResult = values;

      return expect(actualResult).resolves.toEqual(expectedResult);
    });

    it('happy - synchronously resolve promises as synchronous in nested arrays', async () => {
      const promiseArr = [
        [() => delay(20, 'Delay 20'), () => delay(40, 'Delay 40')],
        () => delay(30, 'Delay 30'),
        [() => delay(10, 'Delay 10'), () => delay(10, 'Delay 10')],
      ];

      const actualResult = syncPromises(promiseArr);
      const expectedResult = [
        ['Delay 20', 'Delay 40'],
        'Delay 30',
        ['Delay 10', 'Delay 10'],
      ];

      return expect(actualResult).resolves.toEqual(expectedResult);
    });

    it('happy - synchronously resolve promises as synchronous in nested arrays with primitive values', async () => {
      const values = [
        ['SampleVal1', 'SampleVal4'],
        'SampleVal2',
        ['SampleVal3', 'SampleVal5'],
      ];

      const actualResult = syncPromises(values);
      const expectedResult = values;

      return expect(actualResult).resolves.toEqual(expectedResult);
    });

    it('happy - synchronously resolve promises as synchronous in nested arrays with primitive values with transform callback', async () => {
      const values = [
        ['SampleVal1', 'SampleVal4'],
        ['SampleVal2'],
        ['SampleVal3', 'SampleVal5'],
        'SampleVal6',
      ];

      const actualResult = syncPromises(
        values,
        async (val, index) => `${val}-transformed${index}`,
      );
      const expectedResult = [
        'SampleVal1,SampleVal4-transformed0',
        'SampleVal2-transformed1',
        'SampleVal3,SampleVal5-transformed2',
        'SampleVal6-transformed3',
      ];

      return expect(actualResult).resolves.toEqual(expectedResult);
    });
  });

  describe('asyncPromises', () => {
    it('happy - resolve promise chain asynchronously', async () => {
      const promiseArr = [
        () => delay(20, 'Delay 20'),
        () => delay(30, 'Delay 30'),
        () => delay(10, 'Delay 10'),
      ];

      const actualResult = asyncPromises(promiseArr);
      const expectedResult = ['Delay 20', 'Delay 30', 'Delay 10'];

      return expect(actualResult).resolves.toEqual(expectedResult);
    });

    it('happy - resolve promise chain asynchronously default return value', async () => {
      const values = ['SampleVal1', 'SampleVal2', 'SampleVal3'];

      const actualResult = asyncPromises(values);
      const expectedResult = values;

      return expect(actualResult).resolves.toEqual(expectedResult);
    });

    it('happy - resolve promise chain asynchronously with transform callback and default Value', async () => {
      const values = ['SampleVal1', 'SampleVal2', 'SampleVal3'];

      const actualResult = asyncPromises(
        values,
        async (val, index) => await delay((index + 1) * 10, val),
      );
      const expectedResult = values;

      return expect(actualResult).resolves.toEqual(expectedResult);
    });
  });

  it('happy - asynchronously resolve promises as synchronous in nested arrays', async () => {
    const promiseArr = [
      [() => delay(20, 'Delay 20'), () => delay(40, 'Delay 40')],
      () => delay(30, 'Delay 30'),
      [() => delay(10, 'Delay 10'), () => delay(10, 'Delay 10')],
    ];

    const actualResult = asyncPromises(promiseArr);
    const expectedResult = [
      ['Delay 20', 'Delay 40'],
      'Delay 30',
      ['Delay 10', 'Delay 10'],
    ];

    return expect(actualResult).resolves.toEqual(expectedResult);
  });

  it('happy - asynchronously resolve promises as synchronous in nested arrays with primitive values', async () => {
    const values = [
      ['SampleVal1', 'SampleVal4'],
      'SampleVal2',
      ['SampleVal3', 'SampleVal5'],
    ];

    const actualResult = asyncPromises(values);
    const expectedResult = values;

    return expect(actualResult).resolves.toEqual(expectedResult);
  });

  it('happy - asynchronously resolve promises as synchronous in nested arrays with primitive values with transform callback', async () => {
    const values = [
      ['SampleVal1', 'SampleVal4'],
      ['SampleVal2'],
      ['SampleVal3', 'SampleVal5'],
      'SampleVal6',
    ];

    const actualResult = asyncPromises(
      values,
      async (val, index) => `${val}-transformed${index}`,
    );
    const expectedResult = [
      'SampleVal1,SampleVal4-transformed0',
      'SampleVal2-transformed1',
      'SampleVal3,SampleVal5-transformed2',
      'SampleVal6-transformed3',
    ];

    return expect(actualResult).resolves.toEqual(expectedResult);
  });
});
