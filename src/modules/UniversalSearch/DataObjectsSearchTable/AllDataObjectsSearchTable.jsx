import React, {useState, useEffect, useLayoutEffect, useRef} from 'react';
import DataObjectsQuery from '@macanta/modules/hoc/DataObjectsQuery';
import DataTable from '@macanta/containers/DataTable';
import DOTypeSelectTable from './DOTypeSelectTable';
import DOActionButtons from './DOActionButtons';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import DOSearch from '@macanta/modules/DataObjectItems/DOSearch';
import DOContactIdContext from '@macanta/modules/DataObjectItems/DOContactIdContext';
import Pagination from '@macanta/components/Pagination';
import Box from '@mui/material/Box';

const DOTableWithResetPage = ({groupId, ...props}) => {
  const tableRef = useRef(null);

  useEffect(() => {
    tableRef.current.resetPage();
  }, [groupId]);

  return <DataTable ref={tableRef} {...props} />;
};

const AllDataObjectsSearchTable = ({
  searchQuery: searchQueryProp,
  columns: selectedColumns,
  SearchProps,
  ...props
}) => {
  const [searchQuery, setSearchQuery] = useState(searchQueryProp);
  const [filters, setFilters] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(25);

  const handleSearch = ({search}) => {
    setSearchQuery({search});

    setPage(0);
  };

  const handleFilters = (filterVal) => {
    setFilters(filterVal);

    setPage(0);
  };

  const handleChangePage = (event, value) => {
    setPage(value);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value));
    setPage(0);
  };

  useLayoutEffect(() => {
    setSearchQuery(searchQueryProp);
  }, [searchQueryProp]);

  return (
    <DataObjectsQuery
      {...searchQuery}
      filters={filters}
      page={page}
      limit={rowsPerPage}>
      {({data = {}, loading: doLoading, refetch}) => {
        const dataObjects = data?.items;

        return (
          <>
            <DOTypeSelectTable
              loading={doLoading}
              dataObjects={dataObjects}
              onCreate={refetch}
              renderSearch={(doFields, doTitle, groupId) => (
                <FilteredDOSearch
                  style={{
                    width: 300,
                    margin: '0 1rem 1rem',
                  }}
                  FilterProps={{
                    PopoverProps: {
                      title: `${doTitle} Filters`,
                    },
                  }}
                  groupId={groupId}
                  autoFocus
                  size="small"
                  defaultValue={searchQuery?.search}
                  autoSearch
                  onSearch={handleSearch}
                  onFilter={handleFilters}
                  filters={filters}
                  disableCategories
                  disableSuggestions
                  {...SearchProps}
                />
              )}>
              {({
                groupId,
                columns,
                doItemsData,
                loading: columnsLoading,
                doFields,
                relationships,
                doTitle,
              }) => {
                const loading = Boolean(doLoading || columnsLoading);

                return (
                  <>
                    <DOTableWithResetPage
                      rowsPerPage={rowsPerPage}
                      fullHeight
                      hideEmptyMessage={loading}
                      hidePagination
                      columns={selectedColumns || columns}
                      data={doItemsData}
                      groupId={groupId}
                      renderActionButtons={(actionBtnProps) => {
                        const primaryContactId =
                          actionBtnProps.item.connectedContacts &&
                          actionBtnProps.item.connectedContacts[0]?.contactId;

                        return (
                          <DOContactIdContext.Provider
                            value={{
                              contactId: primaryContactId,
                            }}>
                            <DOActionButtons
                              {...actionBtnProps}
                              groupId={groupId}
                              doFields={doFields}
                              relationships={relationships}
                              onSave={refetch}
                              doTitle={doTitle}
                            />
                          </DOContactIdContext.Provider>
                        );
                      }}
                      numOfTextLines={2}
                      {...props}
                    />
                    {loading && <LoadingIndicator fill />}
                  </>
                );
              }}
            </DOTypeSelectTable>
            <Box
              style={{
                borderTop: '1px solid #eee',
                boxShadow: '0px -4px 10px -2px #eee',
                zIndex: 1,
              }}>
              <Pagination
                table
                count={data?.total}
                page={page}
                onPageChange={handleChangePage}
                rowsPerPage={rowsPerPage}
                rowsPerPageOptions={[25, 50, 100]}
                onRowsPerPageChange={handleChangeRowsPerPage}
              />
            </Box>
          </>
        );
      }}
    </DataObjectsQuery>
  );
};

const FilteredDOSearch = ({groupId, ...props}) => {
  useEffect(() => {
    props.onFilter([]);
  }, [groupId]);

  return <DOSearch groupId={groupId} {...props} />;
};

AllDataObjectsSearchTable.defaultProps = {};

export default AllDataObjectsSearchTable;
