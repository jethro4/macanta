import {gql} from '@apollo/client';

export default gql`
  query appCheck($appName: String!, $apiKey: String!) {
    appCheck(appName: $appName, apiKey: $apiKey) {
      success
    }
  }
`;
