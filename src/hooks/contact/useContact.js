import {client} from '@macanta/containers/ApolloProviderWrapper';
import useRetryQuery from '@macanta/hooks/apollo/useRetryQuery';
import {LIST_CONTACTS} from '@macanta/graphql/contacts';

export const DEFAULT_PAGE = 0;
export const DEFAULT_PAGE_LIMIT = 1;

export const getVariables = (id) => ({
  q: id,
  page: DEFAULT_PAGE,
  limit: DEFAULT_PAGE_LIMIT,
});

const useContact = (id, options) => {
  const variables = getVariables(id);

  const contactsQuery = useRetryQuery(LIST_CONTACTS, {
    variables,
    ...options,
    skip: !id || options?.skip,
  });

  const item = contactsQuery.data?.listContacts?.items?.[0];

  return {...contactsQuery, data: item};
};

export const writeQueryItem = (item) => {
  const variables = getVariables(item.id);

  client.writeQuery({
    query: LIST_CONTACTS,
    data: {
      listContacts: {
        __typename: 'ContactItems',
        items: [item],
        count: 1,
        total: 1,
      },
    },
    variables,
  });
};

export default useContact;
