import {experimentalStyled as styled} from '@mui/material/styles';

import Box from '@mui/material/Box';

export const AssignedContainer = styled(Box)`
  padding: 0 ${({theme}) => theme.spacing(2)};
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-top: ${({theme}) => theme.spacing(2)};
`;
