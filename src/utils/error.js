export const graphqlError = (Exception, message, topStackName) => {
  const {[topStackName]: errorFunc} = {
    [topStackName]: () => {
      return new Exception(message);
    },
  };

  return errorFunc();
};
