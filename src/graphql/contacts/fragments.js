import {gql} from '@apollo/client';

export const CONTACT_ATTRIBUTES = gql`
  fragment contactResults on Contact {
    id
    email
    email2
    email3
    firstName
    middleName
    lastName
    birthday
    title
    company
    website
    jobTitle
    phoneNumbers
    streetAddress1
    streetAddress2
    city
    state
    postalCode
    country
    address2Street1
    address2Street2
    city2
    state2
    postalCode2
    country2
    loggedInUserPermission
    phoneNumberExts
    createdDate
    customFields {
      name
      value
    }
  }
`;

export const CONTACT_RELATIONSHIP_DIRECT_FRAGMENT = gql`
  fragment contactRelationshipDirectFragment on ContactRelationshipDirect {
    id
    name
    description
  }
`;
