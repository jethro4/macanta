import {gql} from '@apollo/client';

export const USER_ACTIONS_FRAGMENT = gql`
  fragment userActionsFragment on UserActionItems {
    queryId
    queryType
    queryName
    queryDescription
    queryConnectedDataType
    queryContactRelationship
    queryMacantaUser
    queryUserActionDetails
  }
`;

export default gql`
  query getUserActions($id: ID) {
    getUserActions(id: $id) {
      action
      status
      count
      items {
        ...userActionsFragment
      }
    }
  }
  ${USER_ACTIONS_FRAGMENT}
`;
