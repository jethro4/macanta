import isArray from 'lodash/isArray';
import range from 'lodash/range';
import {isNumeric} from '@macanta/utils/numeric';

export const convertWithUnit = (value) =>
  isNumeric(value || 0) ? `${value}px` : value;

const getDirectionValues = (values = [0, 0]) => {
  const transformedValues = !isArray(values) ? [values] : values;
  let arr = [];

  switch (transformedValues.length) {
    case 1: {
      arr = range(4).map(() => transformedValues[0]);

      break;
    }
    case 2: {
      const verticalValue = transformedValues[0];
      const horizontalValue = transformedValues[1];

      arr = [verticalValue, horizontalValue, verticalValue, horizontalValue];

      break;
    }
    case 3: {
      const topValue = transformedValues[0];
      const horizontalValue = transformedValues[1];
      const bottomValue = transformedValues[0];

      arr = [topValue, horizontalValue, bottomValue, horizontalValue];

      break;
    }
    case 4: {
      arr = transformedValues;

      break;
    }
  }

  return arr.map((value) => convertWithUnit(value));
};

const useUIState = (optionsArg) => {
  const {margin, padding, border} = Object.assign({}, optionsArg);

  const [marginTop, marginRight, marginBottom, marginLeft] = getDirectionValues(
    margin,
  );
  const [
    paddingTop,
    paddingRight,
    paddingBottom,
    paddingLeft,
  ] = getDirectionValues(padding);
  const borderValues = getDirectionValues(border);
  const [borderTop, borderRight, borderBottom, borderLeft] = borderValues;

  const verticalMargin = marginTop;
  const horizontalMargin = marginRight;
  const verticalPadding = paddingTop;
  const horizontalPadding = paddingRight;
  const addBorder = borderValues.some((value) => Boolean(parseInt(value)));

  return {
    horizontalMargin,
    verticalMargin,
    horizontalPadding,
    verticalPadding,
    addBorder,
    margin: {
      top: marginTop,
      right: marginRight,
      bottom: marginBottom,
      left: marginLeft,
    },
    padding: {
      top: paddingTop,
      right: paddingRight,
      bottom: paddingBottom,
      left: paddingLeft,
    },
    border: {
      top: borderTop,
      right: borderRight,
      bottom: borderBottom,
      left: borderLeft,
    },
  };
};

export default useUIState;
