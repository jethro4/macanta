import {gql} from '@apollo/client';

export default gql`
  query listEmailTemplates($appName: String!, $apiKey: String!) {
    listEmailTemplates(appName: $appName, apiKey: $apiKey) {
      items {
        templateGroupId
        templateId
        title
      }
      count
    }
  }
`;
