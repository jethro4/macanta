import React, {useState} from 'react';
import {useFormikContext} from 'formik';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Collapse from '@macanta/components/Collapse';
import Button from '@macanta/components/Button';
import Switch from '@macanta/components/Forms/ChoiceFields/Switch';
import ContactMailIcon from '@mui/icons-material/ContactMail';
import LocalShippingIcon from '@mui/icons-material/LocalShipping';
import range from 'lodash/range';
import AddIcon from '@mui/icons-material/Add';
import {SubSection} from '@macanta/containers/Section';
import useContactFields from '@macanta/modules/ContactDetails/useContactFields';
import useIsAdmin from '@macanta/hooks/useIsAdmin';
import useIsUser from '@macanta/hooks/admin/useIsUser';
import useAppSettings from '@macanta/hooks/admin/useAppSettings';
import {CONTACT_FIELDS} from '@macanta/constants/contactFields';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import AsyncValidationField from './AsyncValidationField';
import FindAddressBtn from './FindAddressBtn';
import * as Styled from './styles';

const DEFAULT_CONTACT_FIELDS_OBJ = CONTACT_FIELDS.reduce((acc, field) => {
  const accObj = {...acc};

  accObj[field.name] = field.key;

  return accObj;
}, {});

export const billingAddressKeys = [
  'streetAddress1',
  'streetAddress2',
  'city',
  'state',
  'postalCode',
  'country',
];

export const shippingAddressKeys = [
  'address2Street1',
  'address2Street2',
  'city2',
  'state2',
  'postalCode2',
  'country2',
];

const isExpandedAddress = (contact, keys) => {
  const expanded = keys.reduce((acc, key) => {
    const value = contact[key];

    return acc || value;
  }, false);

  return expanded;
};

const FormsGeneral = ({readOnly, contact, style}) => {
  const isEdit = !!contact?.id;

  const {values, errors, handleChange, setFieldValue} = useFormikContext();

  const isAdmin = useIsAdmin();
  const contactIsUser = useIsUser(contact?.id);
  const appSettingsQuery = useAppSettings({
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const allowGenerateTempEmail = !!appSettingsQuery.data?.generatedEmailFormat;

  const [emailAmt, setEmailAmt] = useState(
    ![contact?.email2, contact?.email3].filter((e) => !!e).length
      ? []
      : range(0, [contact?.email2, contact?.email3].filter((e) => !!e).length),
  );
  const [phoneAmt, setPhoneAmt] = useState(
    !contact?.phoneNumbers?.length
      ? [0]
      : range(0, contact.phoneNumbers.length),
  );
  const [phoneExtAmt, setPhoneExtAmt] = useState(
    !contact?.phoneNumberExts?.length
      ? [0]
      : range(0, contact.phoneNumberExts.length),
  );
  const [expandedAddress, setExpandedAddress] = useState({
    billing: readOnly || isExpandedAddress(contact, billingAddressKeys),
    shipping: readOnly || isExpandedAddress(contact, shippingAddressKeys),
  });

  const contactDefaultFieldsQuery = useContactFields(
    null,
    'contactDefaultFields',
    {
      removeEmail: true,
    },
  );

  const contactDefaultFields =
    contactDefaultFieldsQuery.data?.listDataObjectFields;
  const contactDefaultFieldsObj = CONTACT_FIELDS.reduce((acc, defaultField) => {
    const accObj = {...acc};

    const field = contactDefaultFields?.find(
      (f) => f.name === defaultField.name,
    );

    accObj[DEFAULT_CONTACT_FIELDS_OBJ[defaultField.name]] = field || {
      type: defaultField.type,
    };

    return accObj;
  }, {});

  const setAddressValues = (keys, contactObj) => {
    if (contactObj) {
      keys.forEach((key) => {
        setFieldValue(key, contactObj[key]);
      });
    } else {
      keys.forEach((key) => {
        setFieldValue(key, '');
      });
    }
  };

  const handleAddEmail = () => {
    setEmailAmt((prevState) =>
      prevState.concat(prevState[prevState.length - 1] + 1),
    );
  };

  const handleAddPhone = () => {
    setPhoneAmt((prevState) =>
      prevState.concat(prevState[prevState.length - 1] + 1),
    );
  };

  const handleAddPhoneExt = () => {
    setPhoneExtAmt((prevState) =>
      prevState.concat(prevState[prevState.length - 1] + 1),
    );
  };

  const handleExpandedBillingAddress = () => {
    const expanded = !expandedAddress.billing;
    const val = {
      ...expandedAddress,
      billing: expanded,
    };

    setAddressValues(billingAddressKeys, expanded && values);
    setExpandedAddress(val);
  };

  const handleExpandedShippingAddress = () => {
    const expanded = !expandedAddress.shipping;
    const val = {
      ...expandedAddress,
      shipping: expanded,
    };

    setAddressValues(shippingAddressKeys, expanded && values);
    setExpandedAddress(val);
  };

  const handleSelect = ({type, isCopyTo}) => {
    let val;

    if (isCopyTo) {
      val = {
        billing: true,
        shipping: true,
      };
    } else {
      val =
        type === 'billing'
          ? {
              ...expandedAddress,
              billing: true,
            }
          : {
              ...expandedAddress,
              shipping: true,
            };
    }

    setExpandedAddress(val);
  };

  return (
    <Box
      style={{
        display: 'flex',
        paddingBottom: '1rem',
        ...style,
      }}>
      <SubSection
        style={{
          width: 350,
          minWidth: 350,
          alignSelf: 'flex-start',
        }}>
        <Styled.TextField
          required
          name="firstName"
          error={errors.firstName}
          onChange={handleChange('firstName')}
          label="First Name"
          variant="outlined"
          value={values.firstName}
          fullWidth
          size="small"
          readOnly={readOnly}
          type={contactDefaultFieldsObj['firstName']?.type}
          options={contactDefaultFieldsObj['firstName']?.choices}
          defaultValue={contactDefaultFieldsObj['firstName']?.default}
          placeholder={contactDefaultFieldsObj['firstName']?.placeholder}
          headingText={contactDefaultFieldsObj['firstName']?.headingText}
          helperText={contactDefaultFieldsObj['firstName']?.helpText}
          addDivider={contactDefaultFieldsObj['firstName']?.addDivider}
        />

        <Styled.TextField
          error={errors.middleName}
          onChange={handleChange('middleName')}
          label="Middle Name"
          variant="outlined"
          value={values.middleName}
          fullWidth
          size="small"
          readOnly={readOnly}
          type={contactDefaultFieldsObj['middleName']?.type}
          required={contactDefaultFieldsObj['middleName']?.required}
          options={contactDefaultFieldsObj['middleName']?.choices}
          defaultValue={contactDefaultFieldsObj['middleName']?.default}
          placeholder={contactDefaultFieldsObj['middleName']?.placeholder}
          headingText={contactDefaultFieldsObj['middleName']?.headingText}
          helperText={contactDefaultFieldsObj['middleName']?.helpText}
          addDivider={contactDefaultFieldsObj['middleName']?.addDivider}
        />

        <Styled.TextField
          error={errors.lastName}
          onChange={handleChange('lastName')}
          label="Last Name"
          variant="outlined"
          value={values.lastName}
          fullWidth
          size="small"
          readOnly={readOnly}
          type={contactDefaultFieldsObj['lastName']?.type}
          required={contactDefaultFieldsObj['lastName']?.required}
          options={contactDefaultFieldsObj['lastName']?.choices}
          defaultValue={contactDefaultFieldsObj['lastName']?.default}
          placeholder={contactDefaultFieldsObj['lastName']?.placeholder}
          headingText={contactDefaultFieldsObj['lastName']?.headingText}
          helperText={contactDefaultFieldsObj['lastName']?.helpText}
          addDivider={contactDefaultFieldsObj['lastName']?.addDivider}
        />

        <Styled.TextField
          error={errors.birthday}
          onChange={handleChange('birthday')}
          label="DOB / Birthday"
          variant="outlined"
          value={values.birthday}
          fullWidth
          size="small"
          readOnly={readOnly}
          type={contactDefaultFieldsObj['birthday']?.type}
          required={contactDefaultFieldsObj['birthday']?.required}
          options={contactDefaultFieldsObj['birthday']?.choices}
          defaultValue={contactDefaultFieldsObj['birthday']?.default}
          placeholder={contactDefaultFieldsObj['birthday']?.placeholder}
          headingText={contactDefaultFieldsObj['birthday']?.headingText}
          helperText={contactDefaultFieldsObj['birthday']?.helpText}
          addDivider={contactDefaultFieldsObj['birthday']?.addDivider}
        />

        <Styled.TextField
          error={errors.title}
          autoComplete="honorific-prefix"
          onChange={handleChange('title')}
          label="Title"
          variant="outlined"
          value={values.title}
          fullWidth
          size="small"
          readOnly={readOnly}
          type={contactDefaultFieldsObj['title']?.type}
          required={contactDefaultFieldsObj['title']?.required}
          options={contactDefaultFieldsObj['title']?.choices}
          defaultValue={contactDefaultFieldsObj['title']?.default}
          placeholder={contactDefaultFieldsObj['title']?.placeholder}
          headingText={contactDefaultFieldsObj['title']?.headingText}
          helperText={contactDefaultFieldsObj['title']?.helpText}
          addDivider={contactDefaultFieldsObj['title']?.addDivider}
        />

        <Styled.TextField
          error={errors.jobTitle}
          autoComplete="organization-title"
          onChange={handleChange('jobTitle')}
          label="Job Title"
          variant="outlined"
          value={values.jobTitle}
          fullWidth
          size="small"
          readOnly={readOnly}
          type={contactDefaultFieldsObj['jobTitle']?.type}
          required={contactDefaultFieldsObj['jobTitle']?.required}
          options={contactDefaultFieldsObj['jobTitle']?.choices}
          defaultValue={contactDefaultFieldsObj['jobTitle']?.default}
          placeholder={contactDefaultFieldsObj['jobTitle']?.placeholder}
          headingText={contactDefaultFieldsObj['jobTitle']?.headingText}
          helperText={contactDefaultFieldsObj['jobTitle']?.helpText}
          addDivider={contactDefaultFieldsObj['jobTitle']?.addDivider}
        />

        <Styled.TextField
          error={errors.company}
          autoComplete="organization"
          onChange={handleChange('company')}
          label="Company"
          variant="outlined"
          value={values.company}
          fullWidth
          size="small"
          readOnly={readOnly}
          type={contactDefaultFieldsObj['company']?.type}
          required={contactDefaultFieldsObj['company']?.required}
          options={contactDefaultFieldsObj['company']?.choices}
          defaultValue={contactDefaultFieldsObj['company']?.default}
          placeholder={contactDefaultFieldsObj['company']?.placeholder}
          headingText={contactDefaultFieldsObj['company']?.headingText}
          helperText={contactDefaultFieldsObj['company']?.helpText}
          addDivider={contactDefaultFieldsObj['company']?.addDivider}
        />

        <Styled.TextField
          error={errors.website}
          onChange={handleChange('website')}
          label="Website"
          variant="outlined"
          value={values.website}
          fullWidth
          size="small"
          readOnly={readOnly}
          type={contactDefaultFieldsObj['website']?.type}
          required={contactDefaultFieldsObj['website']?.required}
          options={contactDefaultFieldsObj['website']?.choices}
          defaultValue={contactDefaultFieldsObj['website']?.default}
          placeholder={contactDefaultFieldsObj['website']?.placeholder}
          headingText={contactDefaultFieldsObj['website']?.headingText}
          helperText={contactDefaultFieldsObj['website']?.helpText}
          addDivider={contactDefaultFieldsObj['website']?.addDivider}
        />
      </SubSection>

      <SubSection
        style={{
          width: 350,
          minWidth: 350,
          alignSelf: 'flex-start',
        }}>
        <Box
          style={{
            display: 'flex',
            flexDirection: 'column',
          }}>
          <AsyncValidationField
            {...(!isEdit && {
              style: {
                marginBottom: 4,
              },
            })}
            disabled={
              values.isTempEmail || (!isAdmin && !!contactIsUser && isEdit)
            }
            required
            name="email"
            error={!values.isTempEmail && errors.email}
            autoComplete="email"
            onChange={handleChange('email')}
            label="Email"
            variant="outlined"
            value={values.email}
            fullWidth
            size="small"
            readOnly={readOnly}
            type={contactDefaultFieldsObj['email']?.type}
            options={contactDefaultFieldsObj['email']?.choices}
            defaultValue={contactDefaultFieldsObj['email']?.default}
            placeholder={contactDefaultFieldsObj['email']?.placeholder}
            headingText={contactDefaultFieldsObj['email']?.headingText}
            helperText={contactDefaultFieldsObj['email']?.helpText}
            addDivider={contactDefaultFieldsObj['email']?.addDivider}
          />
          {!isEdit && allowGenerateTempEmail && (
            <Box
              style={{
                display: 'flex',
                alignItems: 'center',
                alignSelf: 'flex-end',
              }}>
              <Typography
                style={{
                  fontSize: 13,
                }}>
                Generate Temporary Email
              </Typography>
              <Switch
                style={{
                  marginLeft: 12,
                }}
                checked={values.isTempEmail}
                onChange={(event) =>
                  setFieldValue('isTempEmail', event.target.checked)
                }
              />
            </Box>
          )}
        </Box>

        {!values.isTempEmail &&
          emailAmt.map((amt, emailIndex) => {
            return (
              <Styled.TextField
                key={emailIndex}
                error={errors[`email${emailIndex + 2}`]}
                onChange={handleChange(`email${emailIndex + 2}`)}
                label={`Email ${emailIndex + 2}`}
                variant="outlined"
                value={values[`email${emailIndex + 2}`]}
                fullWidth
                size="small"
                readOnly={readOnly}
                type={contactDefaultFieldsObj[`email${emailIndex + 2}`]?.type}
                required={
                  contactDefaultFieldsObj[`email${emailIndex + 2}`]?.required
                }
                options={
                  contactDefaultFieldsObj[`email${emailIndex + 2}`]?.choices
                }
                defaultValue={
                  contactDefaultFieldsObj[`email${emailIndex + 2}`]?.default
                }
                placeholder={
                  contactDefaultFieldsObj[`email${emailIndex + 2}`]?.placeholder
                }
                headingText={
                  contactDefaultFieldsObj[`email${emailIndex + 2}`]?.headingText
                }
                helperText={
                  contactDefaultFieldsObj[`email${emailIndex + 2}`]?.helpText
                }
                addDivider={
                  contactDefaultFieldsObj[`email${emailIndex + 2}`]?.addDivider
                }
              />
            );
          })}
        {!values.isTempEmail && !readOnly && emailAmt.length < 2 && (
          <Button
            style={{
              marginBottom: '1rem',
            }}
            onClick={handleAddEmail}
            startIcon={<AddIcon />}>
            Add Email
          </Button>
        )}

        {phoneAmt.map((amt, phoneIndex) => {
          const hasCountryCode = !!parseInt(values.countryCode);
          const countryCodeText = `Country Code = ${values.countryCode}`;
          return (
            <AsyncValidationField
              name={`phone${phoneIndex}`}
              key={phoneIndex}
              error={
                errors[`phone${phoneIndex}`] &&
                errors[`phone${phoneIndex}`] +
                  (hasCountryCode ? `. ${countryCodeText}` : '')
              }
              autoComplete="tel"
              onChange={handleChange(`phone${phoneIndex}`)}
              label={`Phone ${phoneIndex + 1}`}
              variant="outlined"
              value={values[`phone${phoneIndex}`]}
              fullWidth
              size="small"
              readOnly={readOnly}
              {...(!errors[`phone${phoneIndex}`] &&
                hasCountryCode &&
                !!values[`phone${phoneIndex}`] && {
                  helperText: countryCodeText,
                })}
              type={contactDefaultFieldsObj[`phone${phoneIndex + 1}`]?.type}
              required={
                contactDefaultFieldsObj[`phone${phoneIndex + 1}`]?.required
              }
              options={
                contactDefaultFieldsObj[`phone${phoneIndex + 1}`]?.choices
              }
              defaultValue={
                contactDefaultFieldsObj[`phone${phoneIndex + 1}`]?.default
              }
              placeholder={
                contactDefaultFieldsObj[`phone${phoneIndex + 1}`]?.placeholder
              }
              headingText={
                contactDefaultFieldsObj[`phone${phoneIndex + 1}`]?.headingText
              }
              helperText={
                contactDefaultFieldsObj[`phone${phoneIndex + 1}`]?.helpText
              }
              addDivider={
                contactDefaultFieldsObj[`phone${phoneIndex + 1}`]?.addDivider
              }
            />
          );
        })}
        {!readOnly && phoneAmt.length < 3 && (
          <Button
            style={{
              marginBottom: '1rem',
            }}
            onClick={handleAddPhone}
            startIcon={<AddIcon />}>
            Add Phone
          </Button>
        )}
        {phoneExtAmt.map((amt, phoneIndex) => {
          return (
            <Styled.TextField
              key={phoneIndex}
              error={errors[`phoneNumberExts[${phoneIndex}]`]}
              autoComplete="tel"
              onChange={handleChange(`phoneNumberExts[${phoneIndex}]`)}
              label={`Phone ${phoneIndex + 1} Ext`}
              variant="outlined"
              value={
                values.phoneNumberExts && values.phoneNumberExts[phoneIndex]
              }
              fullWidth
              size="small"
              readOnly={readOnly}
              type={contactDefaultFieldsObj[`phone${phoneIndex + 1}Ext`]?.type}
              required={
                contactDefaultFieldsObj[`phone${phoneIndex + 1}Ext`]?.required
              }
              options={
                contactDefaultFieldsObj[`phone${phoneIndex + 1}Ext`]?.choices
              }
              defaultValue={
                contactDefaultFieldsObj[`phone${phoneIndex + 1}Ext`]?.default
              }
              placeholder={
                contactDefaultFieldsObj[`phone${phoneIndex + 1}Ext`]
                  ?.placeholder
              }
              headingText={
                contactDefaultFieldsObj[`phone${phoneIndex + 1}Ext`]
                  ?.headingText
              }
              helperText={
                contactDefaultFieldsObj[`phone${phoneIndex + 1}Ext`]?.helpText
              }
              addDivider={
                contactDefaultFieldsObj[`phone${phoneIndex + 1}Ext`]?.addDivider
              }
            />
          );
        })}
        {!readOnly && phoneExtAmt.length < 3 && (
          <Button
            style={{
              marginBottom: '1rem',
            }}
            onClick={handleAddPhoneExt}
            startIcon={<AddIcon />}>
            Add Phone Ext
          </Button>
        )}
      </SubSection>

      <SubSection
        style={{
          width: 350,
          minWidth: 350,
          alignSelf: 'flex-start',
        }}>
        <Collapse in={expandedAddress.billing} timeout="auto">
          <Styled.TextField
            error={errors.streetAddress1}
            autoComplete="street-address"
            onChange={handleChange('streetAddress1')}
            label="Address 1"
            variant="outlined"
            value={values.streetAddress1}
            fullWidth
            size="small"
            readOnly={readOnly}
            type={contactDefaultFieldsObj['streetAddress1']?.type}
            required={contactDefaultFieldsObj['streetAddress1']?.required}
            options={contactDefaultFieldsObj['streetAddress1']?.choices}
            defaultValue={contactDefaultFieldsObj['streetAddress1']?.default}
            placeholder={contactDefaultFieldsObj['streetAddress1']?.placeholder}
            headingText={contactDefaultFieldsObj['streetAddress1']?.headingText}
            helperText={contactDefaultFieldsObj['streetAddress1']?.helpText}
            addDivider={contactDefaultFieldsObj['streetAddress1']?.addDivider}
          />
          <Styled.TextField
            error={errors.streetAddress2}
            onChange={handleChange('streetAddress2')}
            label="Address 2"
            variant="outlined"
            value={values.streetAddress2}
            fullWidth
            size="small"
            readOnly={readOnly}
            type={contactDefaultFieldsObj['streetAddress2']?.type}
            required={contactDefaultFieldsObj['streetAddress2']?.required}
            options={contactDefaultFieldsObj['streetAddress2']?.choices}
            defaultValue={contactDefaultFieldsObj['streetAddress2']?.default}
            placeholder={contactDefaultFieldsObj['streetAddress2']?.placeholder}
            headingText={contactDefaultFieldsObj['streetAddress2']?.headingText}
            helperText={contactDefaultFieldsObj['streetAddress2']?.helpText}
            addDivider={contactDefaultFieldsObj['streetAddress2']?.addDivider}
          />
          <Styled.TextField
            error={errors.city}
            autoComplete="address-level2"
            onChange={handleChange('city')}
            label="City"
            variant="outlined"
            value={values.city}
            fullWidth
            size="small"
            readOnly={readOnly}
            type={contactDefaultFieldsObj['city']?.type}
            required={contactDefaultFieldsObj['city']?.required}
            options={contactDefaultFieldsObj['city']?.choices}
            defaultValue={contactDefaultFieldsObj['city']?.default}
            placeholder={contactDefaultFieldsObj['city']?.placeholder}
            headingText={contactDefaultFieldsObj['city']?.headingText}
            helperText={contactDefaultFieldsObj['city']?.helpText}
            addDivider={contactDefaultFieldsObj['city']?.addDivider}
          />
          <Styled.TextField
            error={errors.state}
            autoComplete="address-level1"
            onChange={handleChange('state')}
            label="State"
            variant="outlined"
            value={values.state}
            fullWidth
            size="small"
            readOnly={readOnly}
            type={contactDefaultFieldsObj['state']?.type}
            required={contactDefaultFieldsObj['state']?.required}
            options={contactDefaultFieldsObj['state']?.choices}
            defaultValue={contactDefaultFieldsObj['state']?.default}
            placeholder={contactDefaultFieldsObj['state']?.placeholder}
            headingText={contactDefaultFieldsObj['state']?.headingText}
            helperText={contactDefaultFieldsObj['state']?.helpText}
            addDivider={contactDefaultFieldsObj['state']?.addDivider}
          />
          <Styled.TextField
            error={errors.postalCode}
            autoComplete="postal-code"
            onChange={handleChange('postalCode')}
            label="Postal Code"
            variant="outlined"
            value={values.postalCode}
            fullWidth
            size="small"
            readOnly={readOnly}
            type={contactDefaultFieldsObj['postalCode']?.type}
            required={contactDefaultFieldsObj['postalCode']?.required}
            options={contactDefaultFieldsObj['postalCode']?.choices}
            defaultValue={contactDefaultFieldsObj['postalCode']?.default}
            placeholder={contactDefaultFieldsObj['postalCode']?.placeholder}
            headingText={contactDefaultFieldsObj['postalCode']?.headingText}
            helperText={contactDefaultFieldsObj['postalCode']?.helpText}
            addDivider={contactDefaultFieldsObj['postalCode']?.addDivider}
          />
          <Styled.TextField
            error={errors.country}
            autoComplete="country-name"
            onChange={handleChange('country')}
            label="Country"
            variant="outlined"
            value={values.country}
            fullWidth
            size="small"
            readOnly={readOnly}
            type={contactDefaultFieldsObj['country']?.type}
            required={contactDefaultFieldsObj['country']?.required}
            options={contactDefaultFieldsObj['country']?.choices}
            defaultValue={contactDefaultFieldsObj['country']?.default}
            placeholder={contactDefaultFieldsObj['country']?.placeholder}
            headingText={contactDefaultFieldsObj['country']?.headingText}
            helperText={contactDefaultFieldsObj['country']?.helpText}
            addDivider={contactDefaultFieldsObj['country']?.addDivider}
          />
        </Collapse>
        {!readOnly && (
          <>
            <Button
              fullWidth
              onClick={handleExpandedBillingAddress}
              variant="contained"
              color={!expandedAddress.billing ? 'primary' : 'error'}
              startIcon={<ContactMailIcon />}>
              {!expandedAddress.billing ? 'Add' : 'Remove'} Billing Address
            </Button>
            <FindAddressBtn type="billing" onSelect={handleSelect} />
          </>
        )}
      </SubSection>

      <SubSection
        style={{
          width: 350,
          minWidth: 350,
          alignSelf: 'flex-start',
        }}>
        <Collapse in={expandedAddress.shipping} timeout="auto">
          <Styled.TextField
            error={errors.address2Street1}
            autoComplete="street-address"
            onChange={handleChange('address2Street1')}
            label="Address 1"
            variant="outlined"
            value={values.address2Street1}
            fullWidth
            size="small"
            readOnly={readOnly}
            type={contactDefaultFieldsObj['address2Street1']?.type}
            required={contactDefaultFieldsObj['address2Street1']?.required}
            options={contactDefaultFieldsObj['address2Street1']?.choices}
            defaultValue={contactDefaultFieldsObj['address2Street1']?.default}
            placeholder={
              contactDefaultFieldsObj['address2Street1']?.placeholder
            }
            headingText={
              contactDefaultFieldsObj['address2Street1']?.headingText
            }
            helperText={contactDefaultFieldsObj['address2Street1']?.helpText}
            addDivider={contactDefaultFieldsObj['address2Street1']?.addDivider}
          />
          <Styled.TextField
            error={errors.address2Street2}
            onChange={handleChange('address2Street2')}
            label="Address 2"
            variant="outlined"
            value={values.address2Street2}
            fullWidth
            size="small"
            readOnly={readOnly}
            type={contactDefaultFieldsObj['address2Street2']?.type}
            required={contactDefaultFieldsObj['address2Street2']?.required}
            options={contactDefaultFieldsObj['address2Street2']?.choices}
            defaultValue={contactDefaultFieldsObj['address2Street2']?.default}
            placeholder={
              contactDefaultFieldsObj['address2Street2']?.placeholder
            }
            headingText={
              contactDefaultFieldsObj['address2Street2']?.headingText
            }
            helperText={contactDefaultFieldsObj['address2Street2']?.helpText}
            addDivider={contactDefaultFieldsObj['address2Street2']?.addDivider}
          />
          <Styled.TextField
            error={errors.city2}
            autoComplete="address-level2"
            onChange={handleChange('city2')}
            label="City"
            variant="outlined"
            value={values.city2}
            fullWidth
            size="small"
            readOnly={readOnly}
            type={contactDefaultFieldsObj['city2']?.type}
            required={contactDefaultFieldsObj['city2']?.required}
            options={contactDefaultFieldsObj['city2']?.choices}
            defaultValue={contactDefaultFieldsObj['city2']?.default}
            placeholder={contactDefaultFieldsObj['city2']?.placeholder}
            headingText={contactDefaultFieldsObj['city2']?.headingText}
            helperText={contactDefaultFieldsObj['city2']?.helpText}
            addDivider={contactDefaultFieldsObj['city2']?.addDivider}
          />
          <Styled.TextField
            error={errors.state2}
            autoComplete="address-level1"
            onChange={handleChange('state2')}
            label="State"
            variant="outlined"
            value={values.state2}
            fullWidth
            size="small"
            readOnly={readOnly}
            type={contactDefaultFieldsObj['state2']?.type}
            required={contactDefaultFieldsObj['state2']?.required}
            options={contactDefaultFieldsObj['state2']?.choices}
            defaultValue={contactDefaultFieldsObj['state2']?.default}
            placeholder={contactDefaultFieldsObj['state2']?.placeholder}
            headingText={contactDefaultFieldsObj['state2']?.headingText}
            helperText={contactDefaultFieldsObj['state2']?.helpText}
            addDivider={contactDefaultFieldsObj['state2']?.addDivider}
          />
          <Styled.TextField
            error={errors.postalCode2}
            autoComplete="postal-code"
            onChange={handleChange('postalCode2')}
            label="Postal Code"
            variant="outlined"
            value={values.postalCode2}
            fullWidth
            size="small"
            readOnly={readOnly}
            type={contactDefaultFieldsObj['postalCode2']?.type}
            required={contactDefaultFieldsObj['postalCode2']?.required}
            options={contactDefaultFieldsObj['postalCode2']?.choices}
            defaultValue={contactDefaultFieldsObj['postalCode2']?.default}
            placeholder={contactDefaultFieldsObj['postalCode2']?.placeholder}
            headingText={contactDefaultFieldsObj['postalCode2']?.headingText}
            helperText={contactDefaultFieldsObj['postalCode2']?.helpText}
            addDivider={contactDefaultFieldsObj['postalCode2']?.addDivider}
          />
          <Styled.TextField
            error={errors.country2}
            autoComplete="country-name"
            onChange={handleChange('country2')}
            label="Country"
            variant="outlined"
            value={values.country2}
            fullWidth
            size="small"
            readOnly={readOnly}
            type={contactDefaultFieldsObj['country2']?.type}
            required={contactDefaultFieldsObj['country2']?.required}
            options={contactDefaultFieldsObj['country2']?.choices}
            defaultValue={contactDefaultFieldsObj['country2']?.default}
            placeholder={contactDefaultFieldsObj['country2']?.placeholder}
            headingText={contactDefaultFieldsObj['country2']?.headingText}
            helperText={contactDefaultFieldsObj['country2']?.helpText}
            addDivider={contactDefaultFieldsObj['country2']?.addDivider}
          />
        </Collapse>
        {!readOnly && (
          <>
            <Button
              fullWidth
              onClick={handleExpandedShippingAddress}
              variant="contained"
              color={!expandedAddress.shipping ? 'primary' : 'error'}
              startIcon={<LocalShippingIcon />}>
              {!expandedAddress.shipping ? 'Add' : 'Remove'} Shipping Address
            </Button>
            <FindAddressBtn type="shipping" onSelect={handleSelect} />
          </>
        )}
      </SubSection>
    </Box>
  );
};

export default FormsGeneral;
