import React from 'react';
import Search from '@macanta/containers/Search';
import DOFiltersForm from './DOFiltersForm';

const DOSearch = ({onSearch, onFilter, filters, groupId, style, ...props}) => {
  const handleSearch = (value) => {
    onSearch({search: value});
  };

  return (
    <Search
      size="xsmall"
      style={{
        minWidth: 240,
        ...style,
      }}
      autoSearch
      disableCategories
      disableSuggestions
      onSearch={handleSearch}
      {...props}
      {...(onFilter && {
        FilterProps: {
          badgeCount: filters.filter((f) => !f.default).length,
          renderContent: (handleClose) => (
            <DOFiltersForm
              filters={filters}
              groupId={groupId}
              onSubmit={(val) => {
                onFilter(val);

                handleClose();
              }}
            />
          ),
          ...props.FilterProps,
        },
      })}
    />
  );
};

DOSearch.defaultProps = {
  onSelectItem: () => {},
  autoSelect: false,
  modal: false,
};

export default DOSearch;
