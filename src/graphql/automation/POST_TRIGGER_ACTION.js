import {gql} from '@apollo/client';
import {TRIGGER_ACTIONS_FRAGMENT} from '@macanta/graphql/automation/GET_TRIGGER_ACTIONS';

export default gql`
  mutation postTriggerAction($input: TriggerActionInput!) {
    postTriggerAction(input: $input) {
      ...triggerActionsFragment
    }
  }
  ${TRIGGER_ACTIONS_FRAGMENT}
`;
