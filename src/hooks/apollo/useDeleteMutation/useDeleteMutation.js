import useMutation from '@macanta/hooks/apollo/useMutation';
import {deleteInCache} from '@macanta/graphql/helpers';

const useDeleteMutation = (graphqlTag, optionsArg) => {
  const {keyField, typeName, update, ...options} = Object.assign(
    {
      keyField: 'id',
      successMessage: 'Deleted successfully',
    },
    optionsArg,
  );

  if (!typeName) {
    throw `Missing "typeName" is required in options`;
  }

  const mutation = useMutation(graphqlTag, {
    ...options,
    update(...args) {
      const result = args[1].result;

      deleteInCache(result[keyField], typeName);

      update?.(...args);
    },
  });

  return mutation;
};

export default useDeleteMutation;
