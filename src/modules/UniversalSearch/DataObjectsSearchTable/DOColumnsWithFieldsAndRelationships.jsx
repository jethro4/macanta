import React, {useMemo} from 'react';
import useDOFields from './useDOFields';
import useDORelationships from './useDORelationships';
import {
  getDOItemsDataWithValue,
  filterDOItems,
  filterFieldsForTable,
} from '@macanta/selectors/dataObject.selector';
import MembershipsBalancePaymentBtn from '@macanta/modules/Metadata/MembershipsBalancePaymentBtn';

const DOColumnsWithFieldsAndRelationships = ({
  groupId,
  children,
  items: doItems,
  filter,
  filterOptions,
  hideContacts,
  contactId,
}) => {
  const doFieldsQuery = useDOFields(groupId);
  const relationshipsQuery = useDORelationships(groupId);

  const fields = doFieldsQuery.data?.listDataObjectFields;
  const relationships = relationshipsQuery.data?.listRelationships;
  const loading = Boolean(doFieldsQuery.loading || relationshipsQuery.loading);

  const [columns] = useMemo(() => {
    if (fields) {
      const columnsWithId = filterFieldsForTable({fields, hideContacts});

      return [columnsWithId];
    }

    return [];
  }, [fields]);

  const filteredDOItems = useMemo(() => {
    if ((filter || filterOptions?.length) && doItems) {
      const filteredItems = filterDOItems(filter, doItems, filterOptions);

      return filteredItems;
    }

    return doItems;
  }, [filter, filterOptions, doItems]);

  const [doItemsData] = useMemo(() => {
    if (filteredDOItems && fields && relationships) {
      let doItemsDataWithId = getDOItemsDataWithValue(
        filteredDOItems,
        fields,
        relationships,
        contactId,
      );

      // Code Specific to tk217 Memberships Balance Payment URL
      if (groupId === 'ci_jt739fbp') {
        const outstandingBalanceField = fields.find(
          (f) => f.id === 'field_klug1rau',
        );

        if (outstandingBalanceField) {
          doItemsDataWithId = doItemsDataWithId.map((item) => {
            const value = item[outstandingBalanceField.name];

            if (parseFloat(value)) {
              item[outstandingBalanceField.name] = [
                {
                  value,
                  renderLabel: () => {
                    const valueWithDecimal = parseFloat(value).toFixed(2);

                    return (
                      <MembershipsBalancePaymentBtn
                        itemId={item.id}
                        value={valueWithDecimal}
                      />
                    );
                  },
                },
              ];
            }

            return item;
          });
        }
      }

      return [doItemsDataWithId];
    }

    return [];
  }, [filteredDOItems, fields, relationships]);

  return children({
    groupId,
    columns,
    doItemsData,
    loading,
    doFields: fields,
    relationships,
  });
};

export default DOColumnsWithFieldsAndRelationships;
