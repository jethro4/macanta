import React, {useState, useLayoutEffect} from 'react';
import {Responsive, WidthProvider} from 'react-grid-layout';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import {sortArrayByObjectKey, moveItem} from '@macanta/utils/array';
import RelationshipCard from './RelationshipCard';
import * as WidgetStyled from '@macanta/modules/pages/AppContainer/Widgets/styles';
import * as TableStyled from '@macanta/components/Table/styles';

const ResponsiveReactGridLayout = WidthProvider(Responsive);

const DORelationshipsList = ({
  data,
  loading,
  emptyMessage,
  onSort,
  onDelete,
}) => {
  const [layouts, setLayouts] = useState({});

  const handleDragStop = (layout, oldItem, newItem) => {
    const hasSwitched = oldItem.x !== newItem.x || oldItem.y !== newItem.y;
    const sortedLayout = sortArrayByObjectKey(layout, 'y');

    if (hasSwitched) {
      const updatedLayouts = generateLayouts(
        sortedLayout.map((l) => ({id: l.i})),
      );

      setLayouts(updatedLayouts);

      handleSortData(oldItem.y, newItem.y);
    }
  };

  const handleSortData = (fromIndex, toIndex) => {
    const sortedData = moveItem({
      arr: data,
      fromIndex,
      toIndex,
    });

    onSort(sortedData);
  };

  useLayoutEffect(() => {
    const updatedLayouts = generateLayouts(data);

    setLayouts(updatedLayouts);
  }, [data]);

  return (
    <Box
      style={{
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
        flexGrow: 1,
        flexBasis: 0,
        overflowX: 'hidden',
      }}>
      <Paper
        style={{
          flex: 1,
          overflow: 'auto',
          borderTop: '1px solid #F5F5F5',
          backgroundColor: '#e4e4e4',
        }}>
        {!!data.length && (
          <ResponsiveReactGridLayout
            layouts={layouts}
            rowHeight={50}
            margin={[12, 12]}
            cols={{lg: 1, md: 1, sm: 1, xs: 1, xxs: 1}}
            isBounded
            isResizable={false}
            compactType="vertical"
            onDragStop={handleDragStop}>
            {data.map((item) => {
              return (
                <WidgetStyled.ReactGridItemContainer key={item.id}>
                  <RelationshipCard item={item} onDelete={onDelete} />
                </WidgetStyled.ReactGridItemContainer>
              );
            })}
          </ResponsiveReactGridLayout>
        )}

        {!loading && data.length === 0 && (
          <TableStyled.EmptyMessageContainer>
            <Typography
              style={{
                paddingBottom: '2rem',
              }}
              align="center"
              color="#888">
              {emptyMessage}
            </Typography>
          </TableStyled.EmptyMessageContainer>
        )}
      </Paper>
    </Box>
  );
};

const generateLayouts = (items) => {
  return ['lg', 'md', 'sm', 'xs', 'xxs'].reduce((acc, col) => {
    if (items) {
      acc[col] = items.map((item, index) => {
        return {
          x: 0,
          y: index,
          w: 1,
          h: 1,
          i: String(item.id),
        };
      });
    }

    return acc;
  }, {});
};

DORelationshipsList.defaultProps = {
  emptyMessage: 'No relationships found',
};

export default DORelationshipsList;
