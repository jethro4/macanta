import React from 'react';
import NylasForms from './NylasForms';
import Modal from '@macanta/components/Modal';

const NylasModal = ({showModal = false, onClose, onPostpone}) => {
  return (
    <Modal
      headerTitle="Connect Email Account"
      open={showModal}
      onClose={onClose}
      contentWidth={650}
      contentHeight={360}>
      <NylasForms onPostpone={onPostpone} />
    </Modal>
  );
};

export default NylasModal;
