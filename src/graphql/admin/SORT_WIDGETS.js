import {gql} from '@apollo/client';

export default gql`
  mutation sortWidgets($sortWidgetsInput: SortWidgetsInput!) {
    sortWidgets(input: $sortWidgetsInput) {
      success
    }
  }
`;
