import {experimentalStyled as styled} from '@mui/material/styles';
import Box from '@mui/material/Box';
import ButtonGroup from '@macanta/components/ButtonGroup';
import Button from '@macanta/components/Button';
import Popover from '@macanta/components/Popover';

export const TitleCrumbBtn = styled(Button)`
  padding: 0;
`;

export const DOItemNavBtnGroup = styled(ButtonGroup)``;

export const DOItemNavBtn = styled(Button)`
  font-size: 0.9rem;
  padding-left: ${({theme}) => theme.spacing(1.6)};
  padding-right: ${({theme}) => theme.spacing(1.6)};
`;

export const HeaderRightContainer = styled(Box)`
  display: flex;
  align-items: center;
`;

export const MultiDOContactFilterPopover = styled(Popover)`
  & .MuiPopover-paper {
    overflow-y: hidden;
  }
`;
