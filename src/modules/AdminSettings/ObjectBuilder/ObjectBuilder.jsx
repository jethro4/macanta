import React from 'react';
import Breadcrumbs from '@mui/material/Breadcrumbs';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import Typography from '@mui/material/Typography';
import Section from '@macanta/containers/Section';
import {ROOT_PATH_ADMIN} from '@macanta/modules/AdminSettings/AdminSettings';
import AllDORelationshipsSection from './AllDORelationshipsSection';
import ObjectBuilderSection from './ObjectBuilderSection';
import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import * as AdminSettingsStyled from '@macanta/modules/AdminSettings/styles';
import {navigate} from 'gatsby';

const ObjectBuilder = () => {
  /* const navigate = useNavigate();*/

  const handleBack = () => {
    navigate(ROOT_PATH_ADMIN);
  };

  return (
    <Section
      fullHeight
      bodyStyle={{
        backgroundColor: '#f5f6fa',
        padding: '1rem 0',
      }}
      HeaderLeftComp={
        <Breadcrumbs aria-label="breadcrumb">
          <AdminSettingsStyled.TitleCrumbBtn
            startIcon={
              <ArrowBackIcon
                style={{
                  color: '#333',
                }}
              />
            }
            onClick={handleBack}
            disabled={false}>
            <Typography color="textPrimary">Admin Settings</Typography>
          </AdminSettingsStyled.TitleCrumbBtn>
          <Typography>Object Builder</Typography>
        </Breadcrumbs>
      }>
      <NoteTaskHistoryStyled.FullHeightGrid container>
        <NoteTaskHistoryStyled.FullHeightFlexGridColumn item xs={12} md={4}>
          <AllDORelationshipsSection />
        </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
        <NoteTaskHistoryStyled.FullHeightFlexGridColumn item xs={12} md={4}>
          <ObjectBuilderSection />
        </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
      </NoteTaskHistoryStyled.FullHeightGrid>
    </Section>
  );
};

export default ObjectBuilder;
