import {makeVarWrapper} from '@macanta/graphql/cache/vars';
import * as Storage from '@macanta/utils/storage';

export const makeVarFunc = (name, defaultValue) => {
  return makeVarWrapper(name, defaultValue, {isSession: true});
};

export const queriesSessionActiveVar = makeVarFunc('queriesSessionActive', {});
export const apiQueueVar = makeVarFunc('apiQueue', []);
export const batchAPIQueueVar = makeVarFunc('batchAPIQueue', []);

export const clearSessionVars = () => {
  Storage.setItem('cache', {}, true);

  queriesSessionActiveVar.clearVar();
  apiQueueVar.clearVar();
  batchAPIQueueVar.clearVar();
};
