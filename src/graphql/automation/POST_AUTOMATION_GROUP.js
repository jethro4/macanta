import {gql} from '@apollo/client';
import {AUTOMATION_GROUP_FRAGMENT} from '@macanta/graphql/automation/GET_AUTOMATION_GROUPS.js';

export default gql`
  mutation postAutomationGroup($input: AutomationGroupInput!) {
    postAutomationGroup(input: $input) {
      ...automationGroupFragment
    }
  }
  ${AUTOMATION_GROUP_FRAGMENT}
`;
