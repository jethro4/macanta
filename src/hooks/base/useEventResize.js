import {useEffect} from 'react';
import isNumber from 'lodash/isNumber';

const useEventResize = (dependencies, timeout) => {
  useEffect(() => {
    if (!isNumber(timeout)) {
      window.dispatchEvent(new Event('resize'));
    } else {
      setTimeout(function () {
        window.dispatchEvent(new Event('resize'));
      }, timeout);
    }
  }, dependencies);
};

export default useEventResize;
