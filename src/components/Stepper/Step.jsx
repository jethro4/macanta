import React from 'react';
import * as Styled from './styles';

const Step = (props) => {
  return <Styled.Step {...props} />;
};

export default Step;
