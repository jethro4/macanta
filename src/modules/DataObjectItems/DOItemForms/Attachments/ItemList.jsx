import React from 'react';
import Image from '@macanta/components/Image';
import List from '@macanta/containers/List';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import GetAppIcon from '@mui/icons-material/GetApp';
import DeleteIcon from '@mui/icons-material/Delete';
import PermissionMetaQuery from '@macanta/modules/hoc/PermissionMetaQuery';
import {transformImageThumbnail} from '@macanta/utils/file';
import * as Styled from './styles';

const ItemList = ({
  items,
  onViewFile,
  onDelete,
  onDownload,
  renderListActions,
}) => {
  const getKeyExtractor = (item, index) => item.id || index;

  const renderRow = (item) => {
    const base64Img = item.isNew
      ? transformImageThumbnail(item.fileExt, item.thumbnail)
      : `data:image/png;base64,${item.thumbnail}`;

    const fileExt = item.downloadUrl?.trim().split('.').pop();
    const isPDF = fileExt === 'pdf';

    return (
      <Styled.ListItemContainer>
        <Styled.ItemActionArea>
          <Styled.ItemActionButton
            style={{
              display: 'flex',
              justifyContent: 'flex-start',
              alignItems: 'center',
              padding: '1rem',
            }}
            onClick={onViewFile(
              item.downloadUrl || item.thumbnail,
              item.fileName,
              item.fileExt || fileExt,
              item.isPDF || isPDF,
            )}>
            <ListItemAvatar
              style={{
                height: '2.5rem',
                width: '3.5rem',
                marginRight: '1rem',
                display: 'flex',
                alignItems: 'center',
              }}>
              <Image
                style={{
                  maxHeight: '100%',
                }}
                src={base64Img}
              />
            </ListItemAvatar>
            <Styled.GridItemLabel
              style={{
                fontSize: '0.8125rem',
                textAlign: 'left',
                marginLeft: '0.5rem',
              }}
              variant="caption"
              color="textSecondary">
              {item.fileName}
            </Styled.GridItemLabel>
          </Styled.ItemActionButton>
        </Styled.ItemActionArea>
        {renderListActions ? (
          renderListActions(item)
        ) : (
          <Styled.ItemActions disableSpacing className="card-actions">
            <PermissionMetaQuery validate="deleteDO">
              <Styled.ActionIconButton onClick={onDelete(item.id)}>
                <DeleteIcon
                  style={{
                    fontSize: '0.875rem',
                    color: 'white',
                  }}
                />
              </Styled.ActionIconButton>
            </PermissionMetaQuery>
            <Styled.ActionIconButton
              onClick={onDownload(item.id)}
              aria-label="download">
              <GetAppIcon
                style={{
                  fontSize: '0.875rem',
                  color: 'white',
                }}
              />
            </Styled.ActionIconButton>
          </Styled.ItemActions>
        )}
      </Styled.ListItemContainer>
    );
  };

  return (
    items.length > 0 && (
      <List
        style={{
          marginTop: '1rem',
          borderBottom: '1px solid #ddd',
        }}
        data={items}
        keyExtractor={getKeyExtractor}
        renderRow={renderRow}
      />
    )
  );
};

export default ItemList;
