import React, {useState} from 'react';
import AddIcon from '@mui/icons-material/Add';
import VisibilityIcon from '@mui/icons-material/Visibility';
import Modal from '@macanta/components/Modal';
import Button from '@macanta/components/Button';
import Tooltip from '@macanta/components/Tooltip';
import DirectRelationshipOptionForms from './DirectRelationshipOptionForms';
import * as DataObjectsSearchTableStyled from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/styles';

const AddOrEditDirectRelationshipOptionBtn = ({item}) => {
  const [showModal, setShowModal] = useState(false);

  const handleShowModal = () => {
    setShowModal(true);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  const handleSuccess = () => {
    handleCloseModal();
  };

  const initValues = {
    id: item?.id,
    name: item?.name || '',
    description: item?.description || '',
  };

  return (
    <>
      {!item ? (
        <Button
          onClick={handleShowModal}
          size="small"
          variant="contained"
          startIcon={<AddIcon />}>
          Add Item
        </Button>
      ) : (
        <Tooltip title="Show Details">
          <DataObjectsSearchTableStyled.ActionButton
            show={showModal}
            onClick={handleShowModal}
            size="small">
            <VisibilityIcon />
          </DataObjectsSearchTableStyled.ActionButton>
        </Tooltip>
      )}
      <Modal
        headerTitle={!item ? 'Add Item' : item.id}
        open={showModal}
        onClose={handleCloseModal}
        contentWidth={600}>
        <DirectRelationshipOptionForms
          initValues={initValues}
          onSuccess={handleSuccess}
        />
      </Modal>
    </>
  );
};

export default AddOrEditDirectRelationshipOptionBtn;
