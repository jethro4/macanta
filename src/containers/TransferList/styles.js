import {experimentalStyled as styled} from '@mui/material/styles';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import SearchComp from '@macanta/containers/Search';

export const Root = styled(Box)`
  height: 100%;
  display: flex;
  justify-content: space-between;
`;

export const ButtonsContainer = styled(Box)`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  margin: ${({theme}) => theme.spacing(2)} 0;
`;

export const Search = styled(SearchComp)`
  width: 100%;

  .MuiInputBase-root {
    height: 36px;

    input {
      padding-top: 0.5px !important;
    }
  }
`;

export const EmptyMessage = styled(Typography)`
  padding: ${({theme}) => theme.spacing(2)} 0;
  color: #aaa;
  font-size: 0.875rem;
`;
