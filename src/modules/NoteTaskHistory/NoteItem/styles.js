import {experimentalStyled as styled} from '@mui/material/styles';

import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Button from '@macanta/components/Button';
import * as applicationStyles from '@macanta/themes/applicationStyles';

export const Root = styled(Box)``;

export const Title = styled(Typography)`
  font-weight: bold;
  color: ${({theme}) => theme.palette.primary.main};
`;

export const Header = styled(Box)`
  display: flex;
  justify-content: space-between;
  padding: ${({theme}) => theme.spacing(2)} ${({theme}) => theme.spacing(2)}
    ${({theme}) => theme.spacing(1)};
`;

export const Body = styled(Box)`
  position: relative;
  padding: ${({theme}) => theme.spacing(2)};
`;

export const NoteContainer = styled(Box)`
  overflow: hidden;
`;

export const Note = styled(Typography)`
  ${({disableLineClamp}) =>
    !disableLineClamp && applicationStyles.getLineText(2)}
  white-space: pre-line;
  padding-right: 22%;
`;

export const ShowMoreButton = styled(Button)`
  font-size: 0.75rem;
  position: absolute;
  padding: 0;
  bottom: 1rem;
  right: 1rem;
`;

export const ShowMore = styled(Typography)`
  white-space: pre-line;
  padding: 1rem;
  flex: 1;
  overflow-y: auto;
`;
