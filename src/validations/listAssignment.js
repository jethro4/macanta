import * as yup from 'yup';

const checkRequiredRoundRobin = function (val) {
  const assignmentType = this.parent?.assignmentType;

  if (assignmentType === 'roundRobin' && !val) {
    return false;
  }

  return true;
};

const checkRequiredSelectedUsers = function (values) {
  const assignmentType = this.parent?.assignmentType;

  if (assignmentType === 'selectedUsers' && !values.length) {
    return false;
  }

  return true;
};

const schema = yup.object().shape({
  assignmentType: yup.string().required('Required'),
  unassignmentBehavior: yup.string().required('Required'),
  assignedRelationship: yup.string().required('Required'),
  selectedRoundRobin: yup
    .string()
    .test('checkRequiredRoundRobin', 'Required', checkRequiredRoundRobin),
  selectedUsers: yup
    .array()
    .test(
      'checkRequiredSelectedUsers',
      'Required at least one user',
      checkRequiredSelectedUsers,
    ),
});

export default schema;
