import React from 'react';
import Form from '@macanta/components/Form';
import DataTable from '@macanta/containers/DataTable';
import useQueries from '@macanta/hooks/admin/useQueries';
import * as ContactFormStyled from '@macanta/modules/ContactForms/styles';

const DEFAULT_COLUMNS = [
  {id: 'name', label: 'Name', minWidth: 240},
  {id: 'doType', label: 'DO Type', minWidth: 170},
  {id: 'description', label: 'Description', minWidth: 170},
];

const SingleQuerySearch = ({selectedQueryIds, onSuccess}) => {
  const {loading, items: queryBuilders} = useQueries();

  const initValues = {
    queryBuilder: undefined,
  };

  const handleFormSubmit = ({queryBuilder}) => {
    onSuccess(queryBuilder);
  };

  return (
    <Form initialValues={initValues} onSubmit={handleFormSubmit}>
      {({values, handleSubmit, setFieldValue}) => (
        <>
          <DataTable
            fullHeight
            loading={loading}
            hideEmptyMessage={loading}
            noOddRowStripes
            highlight
            disabledIds={selectedQueryIds}
            orderBy="name"
            columns={DEFAULT_COLUMNS}
            selectable
            data={queryBuilders}
            onSelectItem={(queryBuilder) =>
              setFieldValue('queryBuilder', queryBuilder)
            }
          />
          <ContactFormStyled.FloatingButton
            style={{
              zIndex: 11,
              width: 90,
            }}
            disabled={!values.queryBuilder}
            variant="contained"
            onClick={handleSubmit}
            size="medium">
            Select
          </ContactFormStyled.FloatingButton>
        </>
      )}
    </Form>
  );
};

export default SingleQuerySearch;
