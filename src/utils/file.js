export const transformImageThumbnail = (fileExt = '', src = '') => {
  if (fileExt === 'pdf') {
    return '/placeholder_pdf.png';
  } else if (fileExt === 'csv') {
    return '/placeholder_csv.png';
  } else if (fileExt === 'txt') {
    return '/placeholder_txt.png';
  } else if (fileExt.startsWith('doc')) {
    return '/placeholder_doc.png';
  } else if (fileExt.startsWith('xls')) {
    return '/placeholder_xls.png';
  } else if (fileExt === 'html') {
    return '/placeholder_html.png';
  }

  return src;
};

export const getBase64Prefix = (fileExt = '') => {
  if (fileExt === 'pdf') {
    return 'data:application/pdf;base64,';
  } else if (fileExt === 'csv') {
    return 'data:text/csv;base64,';
  } else if (fileExt === 'txt') {
    return 'data:text/plain;base64,';
  } else if (fileExt.startsWith('doc')) {
    return 'data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,';
  } else if (fileExt.startsWith('xls')) {
    return 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,';
  } else if (fileExt.startsWith('jp')) {
    return 'data:image/jpeg;base64,';
  }

  return 'data:image/png;base64,';
};

export const getMIMEType = (fileExt = '') => {
  if (fileExt === 'csv') {
    return 'text/csv';
  } else if (fileExt === 'xls') {
    return 'application/vnd.ms-excel';
  } else if (fileExt === 'xlsx') {
    return 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
  }

  return '';
};

export const iframeIncompatible = (fileExt = '', additionalFiles = []) => {
  if (!fileExt) {
    return false;
  }

  return (
    fileExt.startsWith('doc') ||
    fileExt.startsWith('xls') ||
    additionalFiles.includes(fileExt)
  );
};

export const isURI = (uri) => {
  return Boolean(
    uri?.startsWith('https') ||
      uri?.startsWith('http') ||
      uri?.startsWith('data:'),
  );
};

export const getURI = (url, fileExt) => {
  const base64Prefix = getBase64Prefix(fileExt);

  const source = !isURI(url) ? `${base64Prefix}${url}` : url;

  return source;
};

export const toDataURL = (url) =>
  fetch(url)
    .then((response) => response.blob())
    .then(
      (blob) =>
        new Promise((resolve, reject) => {
          const reader = new FileReader();
          reader.onloadend = () => resolve(reader.result);
          reader.onerror = reject;
          reader.readAsDataURL(blob);
        }),
    );

export const download = (data, filename, mime, bom) => {
  var blobData = typeof bom !== 'undefined' ? [bom, data] : [data];
  var blob = new Blob(blobData, {type: mime || 'application/octet-stream'});
  if (typeof window.navigator.msSaveBlob !== 'undefined') {
    // IE workaround for "HTML7007: One or more blob URLs were
    // revoked by closing the blob for which they were created.
    // These URLs will no longer resolve as the data backing
    // the URL has been freed."
    window.navigator.msSaveBlob(blob, filename);
  } else {
    var blobURL =
      window.URL && window.URL.createObjectURL
        ? window.URL.createObjectURL(blob)
        : window.webkitURL.createObjectURL(blob);
    var tempLink = document.createElement('a');
    tempLink.style.display = 'none';
    tempLink.href = blobURL;
    tempLink.setAttribute('download', filename);

    // Safari thinks _blank anchor are pop ups. We only want to set _blank
    // target if the browser does not support the HTML5 download attribute.
    // This allows you to download files in desktop safari if pop up blocking
    // is enabled.
    if (typeof tempLink.download === 'undefined') {
      tempLink.setAttribute('target', '_blank');
    }

    document.body.appendChild(tempLink);
    tempLink.click();

    // Fixes "webkit blob resource error 1"
    setTimeout(function () {
      document.body.removeChild(tempLink);
      window.URL.revokeObjectURL(blobURL);
    }, 200);
  }
};
