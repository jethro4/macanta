import {experimentalStyled as styled} from '@mui/material/styles';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import SwitchComp from '@macanta/components/Forms/ChoiceFields/Switch';

export const HeadCell = styled(Box)`
  display: flex;
  width: 100%;
`;

export const Label = styled(Typography)`
  flex: 1;
  color: #aaa;
  text-align: center;
  font-size: 13px;
  white-space: nowrap;
`;

export const Switch = styled(SwitchComp)`
  flex: 1;
`;
