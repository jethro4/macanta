import React from 'react';
import Typography from '@mui/material/Typography';
import Form from '@macanta/components/Form';
import SendIcon from '@mui/icons-material/Send';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import {useMutation} from '@apollo/client';
import {EMAIL_CONTACT_SUPPORT} from '@macanta/graphql/admin';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import * as NoteTaskFormsStyled from '@macanta/modules/NoteTaskForms/styles';
import * as Storage from '@macanta/utils/storage';

const ContactSupportForms = ({
  contactId,
  contactName,
  contactEmail,
  onSuccess,
  ...props
}) => {
  const {firstName, lastName, userId, email} = Storage.getItem('userDetails');

  const reportedByFullName = `${firstName || ''} ${lastName || ''}`.trim();

  const {displayMessage} = useProgressAlert();

  const [callEmailContactSupport, emailContactSupportMutation] = useMutation(
    EMAIL_CONTACT_SUPPORT,
    {
      onCompleted(data) {
        if (data?.emailContactSupport?.success) {
          displayMessage('Your message will be reviewed!');

          onSuccess();
        }
      },
      onError() {},
    },
  );

  const handleSendEmail = (values) => {
    const messageWithContact =
      values.message +
      `<br><br>Contact: ${contactName} (${contactId}) (${contactEmail})` +
      `<br>Reported By: ${reportedByFullName} (${userId}) (${email})`;

    callEmailContactSupport({
      variables: {
        emailContactSupportInput: {
          subject: values.subject,
          message: messageWithContact,
        },
        __mutationkey: 'emailContactSupportInput',
        removeAppInfo: true,
      },
    });
  };

  const initValues = {
    subject: '',
    message: '',
  };

  return (
    <NoteTaskFormsStyled.Root {...props}>
      <NoteTaskFormsStyled.Container>
        <Form initialValues={initValues} onSubmit={handleSendEmail}>
          {({values, errors, handleChange, handleSubmit}) => (
            <>
              <NoteTaskFormsStyled.FormGroup>
                <NoteTaskFormsStyled.TextField
                  labelPosition="normal"
                  error={errors.subject}
                  onChange={handleChange('subject')}
                  label="Subject"
                  variant="outlined"
                  value={values.subject}
                  fullWidth
                  size="medium"
                />

                <NoteTaskFormsStyled.TextField
                  labelPosition="normal"
                  type="TextArea"
                  hideExpand
                  error={errors.message}
                  onChange={handleChange('message')}
                  label="Message"
                  variant="outlined"
                  value={values.message}
                  fullWidth
                  size="medium"
                />
              </NoteTaskFormsStyled.FormGroup>

              <Typography
                style={{
                  fontSize: '0.75rem',
                  color: '#888',
                }}>
                Contact: {contactName} ({contactId}) ({contactEmail})
              </Typography>
              <Typography
                style={{
                  fontSize: '0.75rem',
                  color: '#888',
                }}>
                Reported By: {reportedByFullName} ({userId}) ({email})
              </Typography>

              {!!emailContactSupportMutation.error && (
                <Typography
                  color="error"
                  style={{
                    fontSize: '0.75rem',
                    textAlign: 'center',
                    marginTop: '1rem',
                  }}>
                  {emailContactSupportMutation.error.message}
                </Typography>
              )}

              <NoteTaskFormsStyled.Footer>
                <NoteTaskFormsStyled.FooterButton
                  disabled={!values.subject || !values.message}
                  variant="contained"
                  startIcon={<SendIcon />}
                  onClick={handleSubmit}
                  size="medium">
                  Send
                </NoteTaskFormsStyled.FooterButton>
              </NoteTaskFormsStyled.Footer>
            </>
          )}
        </Form>
      </NoteTaskFormsStyled.Container>
      <LoadingIndicator modal loading={emailContactSupportMutation.loading} />
    </NoteTaskFormsStyled.Root>
  );
};

export default ContactSupportForms;
