import {useState, useEffect, useRef} from 'react';
import {useReactiveVar} from '@apollo/client';
import moment from 'moment';
import usePrevious from '@macanta/hooks/usePrevious';
import {loggedInVar} from '@macanta/graphql/cache/vars';
import * as Storage from '@macanta/utils/storage';
import {setIsNewVersionAvailable} from '@macanta/utils/app';

const pjson = require('../../package.json');

const useCacheBuster = () => {
  const loggedIn = useReactiveVar(loggedInVar);
  const prevLoggedIn = usePrevious(loggedIn);

  const [loading, setLoading] = useState(true);

  const checkIntervalRef = useRef(null);

  const checkForUpdates = (showConfirm) => {
    fetch(`/version.json?date=${Date.now()}`)
      .then((res) => res.json())
      .then((res) => {
        const hasUpdated = Storage.getItem('hasUpdated');

        if (!hasUpdated && res.version > pjson.version) {
          Storage.setItem('hasUpdated', true);
          console.info('Clearing cache and hard reloading...');
          if (caches) {
            // Service worker cache should be cleared with caches.delete()
            caches.keys().then(function (names) {
              for (const name of names) caches.delete(name);
            });
          }

          if (!showConfirm) {
            window.location.reload();
          } else {
            setIsNewVersionAvailable(true);
          }
        } else {
          Storage.setItem('hasUpdated', false);

          setLoading(false);
        }
      })
      .catch((err) => {
        console.error(err);

        setLoading(false);
      })
      .finally(() => {});
  };

  const startCheckForUpdatesInterval = () => {
    stopCheckForUpdatesInterval();

    checkIntervalRef.current = setInterval(() => {
      checkForUpdates(true);
    }, 180000); // check every 3 minutes
  };

  const stopCheckForUpdatesInterval = () => {
    clearInterval(checkIntervalRef.current);
  };

  useEffect(() => {
    checkForUpdates();

    startCheckForUpdatesInterval();

    let timeStart = moment();

    const logData = () => {
      if (document.visibilityState !== 'hidden') {
        const hiddenDuration = moment().diff(timeStart, 'second');

        if (hiddenDuration >= 30) {
          checkForUpdates(true);
        }

        startCheckForUpdatesInterval();
      } else {
        timeStart = moment();

        stopCheckForUpdatesInterval();
      }
    };

    document.addEventListener('visibilitychange', logData);

    return () => {
      document.removeEventListener('visibilitychange', logData);

      stopCheckForUpdatesInterval();
    };
  }, []);

  useEffect(() => {
    if (prevLoggedIn === false && loggedIn) {
      checkForUpdates(true);
    }
  }, [loggedIn]);

  return {loading};
};

export default useCacheBuster;
