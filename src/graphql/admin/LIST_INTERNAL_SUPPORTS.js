import {gql} from '@apollo/client';

export default gql`
  query listInternalSupports {
    listInternalSupports {
      email
      fullName
    }
  }
`;
