import * as yup from 'yup';

const schema = yup.object().shape({
  fileName: yup.string().required('Required'),
  url: yup.string().required('Required'),
});

export default schema;
