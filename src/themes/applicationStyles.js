export const spacing = 8;
export const HTML_FONT_SIZE = 16;

export const getFontRelativeSize = (val) => {
  return `${val / HTML_FONT_SIZE}em`;
};

export const DEFAULT_HEAD_ROW_HEIGHT = 54;
export const DEFAULT_BODY_ROW_HEIGHT = 54;
export const DEFAULT_BODY_SUBHEADER_ROW_HEIGHT = 44;
export const DEFAULT_COLUMN_MIN_WIDTH = 170;
export const DEFAULT_COLUMN_MAX_WIDTH = 300;
export const DEFAULT_SECTION_HEADER_MIN_HEIGHT = '3rem';
export const DEFAULT_FOOTER_HEIGHT = 58;
export const DEFAULT_FORM_FONT_SIZE = '0.96875rem';

export const fullHeightFlexRow = `
  height: 100%;
  display: flex;
`;

export const fullHeightFlexColumn = `
  height: 100%;
  display: flex;
  flex-direction: column;
`;

export const flexCenter = `
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const rowCenterJustifyStart = `
  ${flexCenter}
  justify-content: flex-start;
`;

export const rowCenterJustifyBetween = `
  ${flexCenter}
  justify-content: space-between;
`;

export const rowCenterJustifyEnd = `
  ${flexCenter}
  justify-content: flex-end;
`;

export const oneLineText = `
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

export const getLineText = (lines = 1) => {
  return `
    ${oneLineText}
    -webkit-line-clamp: ${lines};
    display: -webkit-box;
    -webkit-box-orient: vertical;
    white-space: unset;
  `;
};

export const footer = `
  padding: 0px ${({theme}) => theme.spacing(2)};
  display: flex;
  align-items: center;
  background-color: white;
  box-shadow: 0px -4px 10px -2px #eee;
  z-index: 10;
  border-bottom-left-radius: 4px;
  border-bottom-right-radius: 4px;
`;

export const absoluteFill = `
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
`;

export const fillScroll = `
  ${absoluteFill}
  overflow-y: auto;
  display: flex;
  flex-direction: column;
`;
