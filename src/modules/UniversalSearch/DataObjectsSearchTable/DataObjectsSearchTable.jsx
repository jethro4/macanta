import React, {useState, useLayoutEffect} from 'react';
import DataObjectsQuery from '@macanta/modules/hoc/DataObjectsQuery';
import DataTable from '@macanta/containers/DataTable';
import DOTypeWithFields from './DOTypeWithFields';
import DOActionButtons from './DOActionButtons';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import DOSearch from '@macanta/modules/DataObjectItems/DOSearch';
import DOContactIdContext from '@macanta/modules/DataObjectItems/DOContactIdContext';
import Pagination from '@macanta/components/Pagination';
import Box from '@mui/material/Box';
import AddDOItemBtn from '@macanta/modules/DataObjectItems/AddDOItemBtn';

const DataObjectsSearchTable = ({
  searchQuery: searchQueryProp,
  columns: selectedColumns,
  groupId,
  doTitle,
  SearchProps,
  hideContacts,
  hideAdd,
  paginationProps,
  ...props
}) => {
  const [searchQuery, setSearchQuery] = useState(searchQueryProp);
  const [filters, setFilters] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(25);
  const [sort, setSort] = useState();

  const handleSearch = ({search}) => {
    setSearchQuery({search});

    setPage(0);
  };

  const handleFilters = (filterVal) => {
    setFilters(filterVal);

    setPage(0);
  };

  const handleChangePage = (event, value) => {
    setPage(value);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value));
    setPage(0);
  };

  const handleSort = (property, sortDirection) => {
    setSort({
      order: sortDirection,
      orderBy: property,
    });

    setPage(0);
  };

  useLayoutEffect(() => {
    setSearchQuery(searchQueryProp);
  }, [searchQueryProp]);

  return (
    <DataObjectsQuery
      {...searchQuery}
      filters={filters}
      groupId={groupId}
      page={page}
      limit={rowsPerPage}
      order={sort?.order}
      orderBy={sort?.orderBy}>
      {({data = {}, selectedDO, loading: doLoading, refetch}) => {
        return (
          <>
            <DOTypeWithFields
              loading={doLoading}
              groupId={groupId}
              items={selectedDO?.items}
              hideContacts={hideContacts}>
              {({
                columns,
                doItemsData,
                loading: columnsLoading,
                doFields,
                relationships,
              }) => {
                const loading = Boolean(doLoading || columnsLoading);

                return (
                  <>
                    <Box
                      style={{
                        padding: '1rem',
                        display: 'flex',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        height: '3.75rem',
                      }}>
                      <DOSearch
                        style={{
                          width: 300,
                        }}
                        FilterProps={{
                          PopoverProps: {
                            title: `${doTitle} Filters`,
                          },
                        }}
                        autoFocus
                        size="small"
                        defaultValue={searchQuery?.search}
                        autoSearch
                        onSearch={handleSearch}
                        onFilter={handleFilters}
                        filters={filters}
                        groupId={groupId}
                        disableCategories
                        disableSuggestions
                        {...SearchProps}
                      />

                      {!hideAdd && (
                        <AddDOItemBtn
                          doTitle={doTitle}
                          groupId={groupId}
                          doFields={doFields}
                          relationships={relationships}
                          onSave={refetch}
                        />
                      )}
                    </Box>
                    <DataTable
                      onSort={handleSort}
                      rowsPerPage={rowsPerPage}
                      fullHeight
                      hideEmptyMessage={loading}
                      hidePagination
                      columns={selectedColumns || columns}
                      data={doItemsData}
                      renderActionButtons={(actionBtnProps) => {
                        const primaryContactId =
                          actionBtnProps.item.connectedContacts &&
                          actionBtnProps.item.connectedContacts[0]?.contactId;

                        return (
                          <DOContactIdContext.Provider
                            value={{
                              contactId: primaryContactId,
                            }}>
                            <DOActionButtons
                              {...actionBtnProps}
                              groupId={groupId}
                              doFields={doFields}
                              relationships={relationships}
                              onSave={refetch}
                              doTitle={doTitle}
                              closeAfterSave
                            />
                          </DOContactIdContext.Provider>
                        );
                      }}
                      numOfTextLines={2}
                      {...props}
                    />
                    {loading && <LoadingIndicator fill align="top" />}
                  </>
                );
              }}
            </DOTypeWithFields>
            <Box
              style={{
                borderTop: '1px solid #eee',
                boxShadow: '0px -4px 10px -2px #eee',
                zIndex: 1,
              }}>
              <Pagination
                table
                count={data?.total}
                page={page}
                onPageChange={handleChangePage}
                rowsPerPage={rowsPerPage}
                rowsPerPageOptions={[25, 50, 100]}
                onRowsPerPageChange={handleChangeRowsPerPage}
                {...paginationProps}
              />
            </Box>
          </>
        );
      }}
    </DataObjectsQuery>
  );
};

DataObjectsSearchTable.defaultProps = {};

export default DataObjectsSearchTable;
