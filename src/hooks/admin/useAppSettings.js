import useQuery from '@macanta/hooks/apollo/useQuery';
import {GET_APP_SETTINGS} from '@macanta/graphql/app';

const useAppSettings = (options) => {
  const appSettingsQuery = useQuery(GET_APP_SETTINGS, {
    ...options,
  });

  const appSettings = appSettingsQuery.data?.getAppSettings;

  return {...appSettingsQuery, data: appSettings};
};

export default useAppSettings;
