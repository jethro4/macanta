import React, {useState} from 'react';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import Tooltip from '@macanta/components/Tooltip';
import ConfirmationDialog from '@macanta/containers/ConfirmationDialog';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import {
  LIST_INTERNAL_SUPPORTS,
  DELETE_INTERNAL_SUPPORTS,
} from '@macanta/graphql/admin';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {useQuery, useMutation} from '@apollo/client';
import * as DataObjectsSearchTableStyled from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/styles';

const DeleteEmailBtn = ({email, fullName}) => {
  const {displayMessage} = useProgressAlert();

  const [showDeleteDialog, setShowDeleteDialog] = useState(false);

  const internalSupportsQuery = useQuery(LIST_INTERNAL_SUPPORTS, {
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const internalSupports = internalSupportsQuery.data?.listInternalSupports;

  const [
    callDeleteInternalSupports,
    deleteInternalSupportsMutation,
  ] = useMutation(DELETE_INTERNAL_SUPPORTS, {
    onError() {},
  });

  const handleClose = () => {
    setShowDeleteDialog(false);
  };

  const handleShowDialog = () => {
    setShowDeleteDialog(true);
  };

  const handleDelete = async () => {
    const {data} = await callDeleteInternalSupports({
      variables: {
        deleteInternalSupportsInput: {
          emails: [email],
        },
        __mutationkey: 'deleteInternalSupportsInput',
        removeAppInfo: true,
      },
    });

    if (data?.deleteInternalSupports?.success) {
      internalSupportsQuery.client.writeQuery({
        query: LIST_INTERNAL_SUPPORTS,
        data: {
          listInternalSupports: internalSupports.filter(
            (item) => item.email !== email,
          ),
        },
      });

      displayMessage('Deleted successfully!');

      handleClose();
    }
  };

  return (
    <>
      <Tooltip title="Delete">
        <DataObjectsSearchTableStyled.ActionButton
          style={{
            color: '#aaa',
          }}
          show={showDeleteDialog}
          onClick={handleShowDialog}>
          <DeleteForeverIcon />
        </DataObjectsSearchTableStyled.ActionButton>
      </Tooltip>
      <ConfirmationDialog
        open={!!showDeleteDialog}
        onClose={handleClose}
        title="Are you sure?"
        description={[
          `You are about to delete "${fullName} (${email})" from internal support emails.`,
          'This cannot be undone. Do you want to proceed?',
        ]}
        actions={[
          {
            label: 'Delete',
            onClick: handleDelete,
            startIcon: <DeleteForeverIcon />,
          },
        ]}
      />
      <LoadingIndicator
        modal
        loading={deleteInternalSupportsMutation.loading}
      />
    </>
  );
};

export default DeleteEmailBtn;
