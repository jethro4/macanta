import React, {useEffect, useState} from 'react';
import NotificationsIcon from '@mui/icons-material/Notifications';
import Badge from '@mui/material/Badge';
import Section from '@macanta/containers/Section';
import Typography from '@mui/material/Typography';
import Popover from '@macanta/components/Popover';
import Alert from '@macanta/containers/Alert';
import {useMutation} from '@apollo/client';
import usePollingQuery from '@macanta/hooks/apollo/usePollingQuery';
import {
  LIST_NOTIFICATIONS,
  DISMISS_NOTIFICATION,
  SILENT_NOTIFICATIONS,
  GET_NOTIFICATIONS_STATUS,
} from '@macanta/graphql/notifications';
import * as Storage from '@macanta/utils/storage';
import * as HeaderStyled from '@macanta/containers/Header/styles';
import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import NotifDOItemEdit from './NotifDOItemEdit';
import NotifTypeGridSection from './NotifTypeGridSection';
import NotifDeepworkSelect from './NotifDeepworkSelect';
import Modal from '@macanta/components/Modal';
import Button from '@macanta/components/Button';
import Tooltip from '@macanta/components/Tooltip';
import {navigate} from 'gatsby';

const NOTIFICATION_TYPES = [
  {
    type: 'Task',
    types: ['Task'],
    label: 'Tasks',
  },
  {
    type: 'Relationship',
    types: ['Relationship'],
    label: 'Relationships',
  },
  {
    type: 'Alert',
    types: ['Alert', 'API.Alert'],
    label: 'Alerts',
  },
];

const NotificationsIconBtn = ({mobile}) => {
  const {userId} = Storage.getItem('userDetails');
  /* const navigate = useNavigate();*/

  const [anchorEl, setAnchorEl] = useState(false);
  const [latestNotification, setLatestNotification] = useState(null);
  const [openAlert, setOpenAlert] = useState(false);
  const [badgeCount, setBadgeCount] = useState(0);
  const [showEditModal, setShowEditModal] = useState(false);
  const [selectedDOItemId, setSelectedDOItemId] = useState(null);
  const openNotifications = Boolean(anchorEl);

  const notificationsQuery = usePollingQuery(LIST_NOTIFICATIONS, {
    variables: {
      userId,
    },
    pollInterval: 45000, // 45 seconds interval
  });

  const [callDismissNotifMutation] = useMutation(DISMISS_NOTIFICATION, {
    onCompleted(data) {
      notificationsQuery.client.writeQuery({
        query: LIST_NOTIFICATIONS,
        data: {
          listNotifications: {
            __typename: 'NotificationItems',
            items: notifications.map((n) => {
              if (n.id === data.dismissNotification.notificationId) {
                return {
                  ...n,
                  status: 'Viewed',
                };
              }

              return n;
            }),
            total,
          },
        },
        variables: {
          userId,
        },
      });
    },
    onError() {},
  });

  const [callSilentNotifsMutation] = useMutation(SILENT_NOTIFICATIONS, {
    onError() {},
  });

  const notifications = notificationsQuery.data?.listNotifications?.items;
  const total = notificationsQuery.data?.listNotifications?.total;

  const handleViewNotifications = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleCloseNotifications = () => {
    setAnchorEl(null);
  };

  const handleCloseProgressAlert = () => {
    setOpenAlert(false);
  };

  const handleCloseEditModal = () => {
    setShowEditModal(false);
  };

  const handleSave = () => {
    handleCloseEditModal();
  };

  const handleClickNotif = (item) => () => {
    if (item.type !== 'Task') {
      if (item.meta?.itemId) {
        setSelectedDOItemId(item.meta.itemId);
        setShowEditModal(true);
      }
    } else {
      navigate(`/app/contact/${item.userContactId}/notes`);
      handleCloseNotifications();
    }

    handleCloseProgressAlert();

    const isNew = item.status === 'New';

    if (isNew) {
      handleDismissNotif(item);
    }
  };

  const handleDismissNotif = (item) => {
    callDismissNotifMutation({
      variables: {
        dismissNotificationInput: {
          id: item.id,
          userId,
        },
        __mutationkey: 'dismissNotificationInput',
        removeAppInfo: true,
      },
    });
  };

  const handleDeleteNotif = (item) => {
    notificationsQuery.client.writeQuery({
      query: LIST_NOTIFICATIONS,
      data: {
        listNotifications: {
          __typename: 'NotificationItems',
          items: notifications.filter((n) => n.id !== item.id),
          total: total - 1,
        },
      },
      variables: {
        userId,
      },
    });
  };

  const handleClearAll = (type) => {
    const updatedNotifs = notifications.filter(
      (n) => n.type.toLowerCase() !== type,
    );

    notificationsQuery.client.writeQuery({
      query: LIST_NOTIFICATIONS,
      data: {
        listNotifications: {
          __typename: 'NotificationItems',
          items: updatedNotifs,
          total: updatedNotifs.length,
        },
      },
      variables: {
        userId,
      },
    });
  };

  const handleToggleSilent = (type, isEnabled) => {
    callSilentNotifsMutation({
      variables: {
        silentNotificationsInput: {
          userId,
          type,
          action: isEnabled ? 'on' : 'off',
        },
        __mutationkey: 'silentNotificationsInput',
        removeAppInfo: true,
      },
    });

    notificationsQuery.client.writeQuery({
      query: GET_NOTIFICATIONS_STATUS,
      data: {
        getNotificationsStatus: {
          __typename: 'NotificationsStatus',
          status: isEnabled ? 'active' : 'inactive',
          startTime: '',
          duration: 0,
        },
      },
      variables: {
        userId,
        type,
      },
    });
  };

  const handleSelectDeepworkDuration = (duration) => {
    callSilentNotifsMutation({
      variables: {
        silentNotificationsInput: {
          userId,
          duration,
        },
        __mutationkey: 'silentNotificationsInput',
        removeAppInfo: true,
      },
    });
  };

  useEffect(() => {
    if (notifications) {
      const newNotifications = notifications.filter((n) => n.status === 'New');
      const newLength = newNotifications.length;

      setBadgeCount(newLength);

      if (newNotifications[0]?.id) {
        if (newNotifications[0]?.id !== latestNotification?.id) {
          setOpenAlert(true);
        }

        setLatestNotification(newNotifications[0]);
      }
    }
  }, [notifications]);

  return (
    <>
      {mobile ? (
        <HeaderStyled.NavMobileMenuItem>
          <HeaderStyled.NavIconButton
            id={`notification-centre-button`}
            mobile
            onClick={handleViewNotifications}>
            <Badge badgeContent={badgeCount} color="info">
              <NotificationsIcon />
            </Badge>
          </HeaderStyled.NavIconButton>
          <p>Notifications</p>
        </HeaderStyled.NavMobileMenuItem>
      ) : (
        <Tooltip title="Notifications">
          <HeaderStyled.NavIconButton
            id={`notification-centre-button`}
            onClick={handleViewNotifications}>
            <Badge badgeContent={badgeCount} color="info">
              <NotificationsIcon />
            </Badge>
          </HeaderStyled.NavIconButton>
        </Tooltip>
      )}

      <Alert
        open={openAlert}
        message={
          <Button
            style={{
              color: 'white',
              textAlign: 'left',
              whiteSpace: 'normal',
            }}
            onClick={handleClickNotif(latestNotification)}>
            <div
              dangerouslySetInnerHTML={{__html: latestNotification?.message}}
            />
          </Button>
        }
        style={{
          alignItems: 'center',
          paddingTop: 0,
          paddingBottom: 0,
        }}
        onClose={handleCloseProgressAlert}
        snackbarProps={{
          anchorOrigin: {
            vertical: 'top',
            horizontal: 'right',
          },
          autoHideDuration: 7000,
          style: {
            top: 56,
            right: '2.2rem',
          },
        }}
      />

      <Popover
        open={openNotifications}
        anchorEl={anchorEl}
        onClose={handleCloseNotifications}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}>
        <div
          style={{
            width: '74vw',
            height: 600,
          }}>
          <Section
            fullHeight
            style={{
              margin: 0,
              boxShadow: 'none',
            }}
            bodyStyle={{backgroundColor: '#f5f6fa'}}
            HeaderLeftComp={
              <Typography color="grey" fontWeight="bold">
                Notifications
              </Typography>
            }
            HeaderRightComp={
              <NotifDeepworkSelect
                onSelectDuration={handleSelectDeepworkDuration}
              />
            }>
            <NoteTaskHistoryStyled.FullHeightGrid
              container
              style={{
                paddingBottom: '1rem',
              }}>
              {notifications &&
                NOTIFICATION_TYPES.map((notifType) => {
                  const filteredNotifs = notifications.filter((n) =>
                    notifType.types.includes(n.type),
                  );

                  return (
                    <NotifTypeGridSection
                      key={notifType.type}
                      notifType={notifType}
                      data={filteredNotifs}
                      onClickNotif={handleClickNotif}
                      onDeleteNotif={handleDeleteNotif}
                      onClearAll={handleClearAll}
                      onToggleSilent={handleToggleSilent}
                    />
                  );
                })}
            </NoteTaskHistoryStyled.FullHeightGrid>
          </Section>
        </div>
      </Popover>
      <Modal
        headerTitle={'Edit Window'}
        open={showEditModal}
        onClose={handleCloseEditModal}
        contentWidth={'100%'}
        contentHeight={'94%'}>
        <NotifDOItemEdit itemId={selectedDOItemId} onSave={handleSave} />
      </Modal>
    </>
  );
};

export default NotificationsIconBtn;
