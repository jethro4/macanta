import React, {useState, useEffect} from 'react';
import CloseIcon from '@mui/icons-material/Close';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import isNil from 'lodash/isNil';
import * as Styled from './styles';

const Modal = ({
  showContentDelay,
  children,
  headerTitle,
  loading,
  onClose,
  contentMinWidth,
  contentWidth,
  contentMaxWidth,
  contentMinHeight,
  contentHeight,
  contentMaxHeight,
  innerStyle,
  headerStyle,
  bodyStyle,
  disableScroll,
  hideHeader,
  disableBackdropClose,
  ...props
}) => {
  const handleBackdropClose = () => {
    !disableBackdropClose && onClose();
  };

  const handleStopMouseEvent = (event) => {
    event.stopPropagation();
  };

  return (
    <Styled.Root
      onMouseUp={handleStopMouseEvent}
      onMouseDown={handleStopMouseEvent}
      onClose={onClose}
      {...props}>
      {loading ? (
        <LoadingIndicator fill />
      ) : (
        <>
          <Styled.Container
            style={Object.assign(
              {},
              !isNil(contentHeight) && {
                height: '100%',
                maxHeight: contentHeight,
              },
              !isNil(contentMinHeight) && {
                minHeight: contentMinHeight,
              },
              !isNil(contentMaxHeight) && {
                maxHeight: contentMaxHeight,
              },
            )}>
            <Styled.InnerContainer
              style={Object.assign(
                {},
                !isNil(contentWidth) && {
                  width: '100%',
                  maxWidth: contentWidth,
                  ...innerStyle,
                },
                !isNil(contentMinWidth) && {
                  minWidth: contentMinWidth,
                },
                !isNil(contentMaxWidth) && {
                  maxWidth: contentMaxWidth,
                },
              )}>
              {!hideHeader && (
                <Styled.ModalHeader style={headerStyle}>
                  <Styled.HeaderTitle>{headerTitle}</Styled.HeaderTitle>
                  <Styled.CloseButton
                    aria-haspopup="true"
                    onClick={handleBackdropClose}
                    {...(headerTitle
                      ? {
                          style: {
                            padding: 6,
                            marginRight: 6,
                          },
                        }
                      : {
                          style: {
                            padding: 4,
                            marginRight: 8,
                          },
                        })}>
                    <CloseIcon style={{color: 'white'}} />
                  </Styled.CloseButton>
                </Styled.ModalHeader>
              )}
              <ModalBody
                showContentDelay={showContentDelay}
                bodyStyle={bodyStyle}
                disableScroll={disableScroll}>
                {children}
              </ModalBody>
            </Styled.InnerContainer>
          </Styled.Container>
        </>
      )}
    </Styled.Root>
  );
};

const ModalBody = ({children, showContentDelay, bodyStyle, disableScroll}) => {
  const [showContent, setShowContent] = useState(!showContentDelay);

  useEffect(() => {
    if (!showContent) {
      setTimeout(() => {
        setShowContent(true);
      }, showContentDelay);
    }
  }, []);

  return (
    <Styled.ModalBody
      style={{
        ...(disableScroll && {overflow: 'initial'}),
        ...bodyStyle,
      }}>
      {showContent ? children : <LoadingIndicator fill />}
    </Styled.ModalBody>
  );
};

Modal.defaultProps = {
  disableEscapeKeyDown: true,
};

export default Modal;
