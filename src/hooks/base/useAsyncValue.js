import {useState} from 'react';
import isFunction from 'lodash/isFunction';
import useDeepCompareEffect from './useDeepCompareEffect';

const useAsyncValue = (valueProp, dependencyKeys) => {
  const [value, setValue] = useState(valueProp);
  const valueIsFunc = isFunction(valueProp);
  const dependencies =
    valueIsFunc || dependencyKeys ? [...dependencyKeys] : [valueProp];

  const runAsync = async () => {
    const val = valueIsFunc ? await valueProp(value) : valueProp;

    setValue(val);
  };

  useDeepCompareEffect(() => {
    runAsync();
  }, dependencies);

  return [value, setValue];
};

export default useAsyncValue;
