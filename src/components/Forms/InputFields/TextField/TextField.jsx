import React, {useCallback} from 'react';
import PropTypes from 'prop-types';
import isString from 'lodash/isString';
import isArray from 'lodash/isArray';
import isNil from 'lodash/isNil';
import isEqual from 'lodash/isEqual';
import debounce from 'lodash/debounce';
import InputAdornment from '@mui/material/InputAdornment';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import useValue from '@macanta/hooks/base/useValue';
import * as Styled from './styles';

let TextField = ({
  loading,
  onEnterPress,
  onBackSpace,
  onKeyDown,
  onKeyUp,
  error,
  value: valueProp,
  onChange,
  timeout,
  maxLength,
  InputProps,
  inputProps,
  ariaLabel,
  ...props
}) => {
  const [value, setValue] = useValue(valueProp);

  const handleDebouncedChange = useCallback(
    debounce(
      (newVal) => {
        onChange(newVal);
      },
      timeout,
      {leading: false, trailing: true},
    ),
    [],
  );

  const handleChangeText = (event) => {
    const newVal = event.target.value;

    if (
      !hasChanged({
        newVal,
        oldVal: value,
        isMultiple: props.SelectProps?.multiple,
      })
    ) {
      return;
    }

    setValue(newVal);

    if (onChange) {
      if (timeout) {
        handleDebouncedChange(newVal);
      } else {
        onChange(newVal);
      }
    }
  };

  const hasChanged = ({newVal = '', oldVal, isMultiple}) => {
    if (!isMultiple) {
      return newVal !== oldVal;
    } else {
      const updatedVal = newVal.filter((v) => v !== undefined);

      return !isEqual(updatedVal, oldVal);
    }
  };

  const handleKeyDown = (e) => {
    if (e.keyCode == 13) {
      if (!props.multiline) {
        e.target.blur();
      }

      onEnterPress && onEnterPress(e);
    } else if (e.keyCode == 8) {
      onBackSpace && onBackSpace(e);
    } else {
      onKeyDown && onKeyDown(e);
    }
  };

  const handleKeyUp = (e) => {
    onKeyUp && onKeyUp(e);
  };

  return (
    <Styled.TextField
      loading={loading}
      onKeyDown={handleKeyDown}
      onKeyUp={handleKeyUp}
      {...(!!error &&
        isString(error) && {
          helperText: error,
        })}
      error={!!error}
      value={!isNil(value) ? (isArray(value) ? value : String(value)) : ''}
      onChange={handleChangeText}
      InputProps={{
        ...InputProps,
        ...(loading && {
          endAdornment: (
            <InputAdornment position="end">
              <LoadingIndicator transparent size={16} loading={loading} />
            </InputAdornment>
          ),
        }),
      }}
      inputProps={{
        maxLength,
        ...inputProps,
        ...(!!ariaLabel && {
          'aria-label': ariaLabel,
        }),
      }}
      hiddenLabel={!props.label}
      {...props}
    />
  );
};

TextField.propTypes = {
  loading: PropTypes.bool,
  timeout: PropTypes.number,
  onEnterPress: PropTypes.func,
  onKeyDown: PropTypes.func,
  /** The size of the button */
  size: PropTypes.oneOf(['xsmall', 'small', 'medium']),
};

TextField.defaultProps = {
  loading: false,
  timeout: 0,
};

export default TextField;
