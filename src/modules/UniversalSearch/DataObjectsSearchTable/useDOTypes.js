import useSessionQuery from '@macanta/hooks/apollo/useSessionQuery';
import {LIST_DATA_OBJECT_TYPES} from '@macanta/graphql/dataObjects';
import * as Storage from '@macanta/utils/storage';
import {useMemo} from 'react';
import {sortArrayByPriority} from '@macanta/utils/array';

const useDOTypes = (optionsProp = {}) => {
  const {email: userEmail} = Storage.getItem('userDetails');

  const {removeEmail, includeContactObject, ...options} = optionsProp;

  const doTypesQuery = useSessionQuery(LIST_DATA_OBJECT_TYPES, {
    variables: {
      ...(!removeEmail && {
        userEmail,
      }),
      ...(includeContactObject === true && {
        includeContactObject,
      }),
    },
    ...options,
  });

  const sortedDOTypes = useMemo(() => {
    let result = doTypesQuery.result || [];

    if (includeContactObject) {
      result = sortArrayByPriority(result, 'id', 'co_customfields');
    }

    return result;
  }, [doTypesQuery.result]);

  return {...doTypesQuery, result: sortedDOTypes};
};

export default useDOTypes;
