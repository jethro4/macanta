import React, {useState} from 'react';
import Button from '@macanta/components/Button';
import Modal from '@macanta/components/Modal';
import ContactNoteForms from '@macanta/modules/NoteTaskForms/NoteForms/ContactNoteForms';
import AddIcon from '@mui/icons-material/Add';
import Popover from '@macanta/components/Popover';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import * as Styled from './styles';
import ContactTaskForms from '@macanta/modules/NoteTaskForms/TaskForms/ContactTaskForms';

const NoteTaskFormsBtn = ({contactId, onSuccess, ...props}) => {
  const [showModal, setShowModal] = useState(false);
  const [selectedType, setSelectedType] = useState('note');
  const [anchorEl, setAnchorEl] = useState(false);

  const openSelection = Boolean(anchorEl);

  const handleShowSelection = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleCloseSelection = () => {
    setAnchorEl(null);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  const handleSuccess = (task) => {
    onSuccess && onSuccess(task);

    handleCloseSelection();
    handleCloseModal();
  };

  const handleSelectType = (type) => () => {
    setSelectedType(type);
    handleCloseSelection();
    setShowModal(true);
  };

  const isNote = selectedType === 'note';
  const FormsComponent = isNote ? ContactNoteForms : ContactTaskForms;

  return (
    <>
      <Button
        onClick={handleShowSelection}
        size="small"
        variant="contained"
        startIcon={<AddIcon />}
        {...props}>
        Add Item
      </Button>
      <Popover
        hideHeader
        open={openSelection}
        anchorEl={anchorEl}
        onClose={handleCloseSelection}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: -8,
          horizontal: 'right',
        }}>
        <div
          style={{
            width: 140,
          }}>
          <List
            style={{
              padding: 0,
            }}>
            <Styled.SelectionItem button onClick={handleSelectType('note')}>
              <AddIcon />
              <Styled.SelectionItemText primary="Note" />
            </Styled.SelectionItem>
            <Divider />
            <Styled.SelectionItem button onClick={handleSelectType('task')}>
              <AddIcon />
              <Styled.SelectionItemText primary="Task" />
            </Styled.SelectionItem>
          </List>
        </div>
      </Popover>
      <Modal
        headerTitle={`Add ${isNote ? 'Note' : 'Task'}`}
        open={showModal}
        onClose={handleCloseModal}>
        <FormsComponent contactId={contactId} onSuccess={handleSuccess} />
      </Modal>
    </>
  );
};

export default NoteTaskFormsBtn;
