import {gql} from '@apollo/client';

export default gql`
  mutation deleteFormBuilder($deleteFormBuilderInput: DeleteFormBuilderInput!) {
    deleteFormBuilder(input: $deleteFormBuilderInput) {
      id
      success
    }
  }
`;
