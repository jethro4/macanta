import {gql} from '@apollo/client';

export default gql`
  mutation deleteWidget($deleteWidgetInput: DeleteWidgetInput!) {
    deleteWidget(input: $deleteWidgetInput) {
      success
      widgetId
    }
  }
`;
