import RegexBuilder from '@macanta/helpers/regex/RegexBuilder';

export const capitalizeFirstLetter = (string) => {
  return string.charAt(0).toUpperCase() + string.slice(1);
};

export const uncapitalizeFirstLetter = (string) => {
  return string.charAt(0).toLowerCase() + string.slice(1);
};

export const ordinalNumberOf = (i) => {
  var j = i % 10,
    k = i % 100;
  if (j == 1 && k != 11) {
    return i + 'st';
  }
  if (j == 2 && k != 12) {
    return i + 'nd';
  }
  if (j == 3 && k != 13) {
    return i + 'rd';
  }
  return i + 'th';
};

export const insertString = ({text = '', stringToInsert = '', index}) => {
  const position = index ?? text.length;
  return text.slice(0, position) + stringToInsert + text.slice(position);
};

export const getMatches = (text, regexStringOrOptions) => {
  const matches = [
    ...text.match(RegexBuilder.generateRegex(regexStringOrOptions, 'g')),
  ];

  return matches;
};

export const replaceAll = (text, stringOrRegex, replaceWith = '') => {
  return text.replaceAll(stringOrRegex, replaceWith);
};

export const replaceAllSpace = (text, replaceWith = '') => {
  return text.replace(/\s+/g, replaceWith);
};

export const replaceAllSpecialChars = (text, replaceWith = '') => {
  return text.replace(/\W+/g, replaceWith);
};

export const replaceStrings = (text, stringsToReplaceMapping = {}) => {
  const updatedText = Object.entries(stringsToReplaceMapping).reduce(
    (acc, [word, newWord]) => replaceAll(acc, word, newWord),
    text,
  );

  return updatedText;
};

export const replaceWords = (
  text,
  stringsToReplaceMapping = {},
  flags = 'g',
) => {
  const updatedText = Object.entries(stringsToReplaceMapping).reduce(
    (acc, [word, newWord]) => {
      const regexp = RegexBuilder.generateRegex(
        {
          start: '\\b',
          end: '\\b',
          between: word,
        },
        flags,
      );

      return replaceAll(acc, regexp, newWord);
    },
    text,
  );

  return updatedText;
};

export const generateUniqueID = () =>
  String(Math.floor(Math.random() * Math.floor(Math.random() * Date.now())));

export const camelToSnakeCase = (str) =>
  uncapitalizeFirstLetter(str).replace(
    /[A-Z]/g,
    (letter) => `_${letter.toLowerCase()}`,
  );

export const trimEndString = (text = '', strLength = 0) => {
  return text.slice(0, text.length - strLength);
};
