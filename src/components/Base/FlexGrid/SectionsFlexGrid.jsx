import React from 'react';
import Box from '@mui/material/Box';
import * as Styled from './styles';
import TaskSwitch from '@macanta/modules/UserSchedules/TaskSwitch';
// import DORelationshipItemsData from '@macanta/modules/DataObjectItems/DOItemForms/Relationships/DORelationshipItemsData';

const SectionsFlexGrid = ({data, titleKey, dataKey, renderData}) => {
  return !data
    ? null
    : data.map((gd) => {
        const items = gd[dataKey];

        return (
          <Styled.ItemContainer key={gd[titleKey]} className="item-group">
            <Styled.ItemHeader className="item-group-title">
              {gd[titleKey]}
            </Styled.ItemHeader>
            <Styled.ItemBody>
              {items.map((d) => {
                return (
                  <Styled.ItemBodyContainer key={d.id} className="item-data">
                    <Box
                      sx={{
                        flex: 1,
                        overflowX: 'hidden',
                      }}>
                      {renderData(d)}
                    </Box>

                    <TaskSwitch id={d.id} />
                  </Styled.ItemBodyContainer>
                );
              })}
            </Styled.ItemBody>
          </Styled.ItemContainer>
        );
      });
};

SectionsFlexGrid.defaultProps = {
  data: [],
  titleKey: 'title',
  dataKey: 'items',
};

export default SectionsFlexGrid;
