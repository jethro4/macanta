import {experimentalStyled as styled} from '@mui/material/styles';

import PaperComp from '@macanta/components/Paper';
import Box from '@mui/material/Box';
import MUIDivider from '@mui/material/Divider';
import Typography from '@mui/material/Typography';
import LoadingIndicatorComp from '@macanta/components/LoadingIndicator';
import * as applicationStyles from '@macanta/themes/applicationStyles';

export const Root = styled(PaperComp)`
  padding: 0;
  margin: ${({theme}) => theme.spacing(3)} ${({theme}) => theme.spacing(2)} 0;
  background-color: white;
`;

export const SectionHeader = styled(Box)`
  padding: 0 ${({theme}) => theme.spacing(2)};
  display: flex;
  justify-content: space-between;
  align-items: center;
  min-height: ${applicationStyles.DEFAULT_SECTION_HEADER_MIN_HEIGHT};
  box-sizing: content-box;
  border-bottom: 1px solid ${({theme}) => theme.palette.tertiary.main};
  box-shadow: 0px 4px 10px -6px #ccc;
  z-index: 10;
  border-top-left-radius: 4px;
  border-top-right-radius: 4px;
`;

export const SectionBody = styled(Box)`
  flex: 1;
  position: relative;
`;

export const Divider = styled(MUIDivider)`
  background-color: ${({theme}) => theme.palette.primary.main};
`;

export const TitleContainer = styled(Box)`
  display: flex;
  align-items: center;
`;

export const LoadingIndicator = styled(LoadingIndicatorComp)``;

export const Title = styled(Typography)`
  white-space: nowrap;
`;

export const SectionFooter = styled(Box)`
  ${applicationStyles.footer}
`;

export const BodyChildren = styled(Box)`
  border-bottom-left-radius: 4px;
  border-bottom-right-radius: 4px;
  ${(props) => props.fullHeight && applicationStyles.fillScroll}
`;
