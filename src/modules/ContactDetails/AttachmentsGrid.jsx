import React, {useState} from 'react';
import Image from '@macanta/components/Image';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import DeleteIcon from '@mui/icons-material/Delete';
import FileViewer from '@macanta/containers/FileViewer';
import {
  transformImageThumbnail,
  iframeIncompatible,
  getURI,
} from '@macanta/utils/file';
import * as AttachmentsStyled from '@macanta/modules/DataObjectItems/DOItemForms/Attachments/styles';
import * as WidgetsStyled from '@macanta/modules/pages/AppContainer/Widgets/styles';
import * as Styled from './styles';

const AttachmentsGrid = ({
  items,
  disablePreview,
  single,
  onDelete,
  disableDelete,
  style,
  imageContainerStyle,
  hideFilename,
  actionContainerStyle,
}) => {
  const [selectedFilePreview, setSelectedFilePreview] = useState(null);

  const handleViewFile = (item) => () => {
    if (!disablePreview) {
      let source = item.downloadUrl;

      if (!source) {
        source = getURI(item.thumbnail, item.fileExt);
      }

      setSelectedFilePreview({
        src: source,
        fileName: item.fileName,
        fileExt: item.fileExt,
        isPDF: item.isPDF,
      });
    }
  };

  const handleCloseFileViewer = () => {
    setSelectedFilePreview(null);
  };

  return items?.length === 0 ? null : (
    <Box
      style={{
        overflowX: 'auto',
        padding: '1rem 0',
        whiteSpace: 'nowrap',
        ...(single && {
          backgroundColor: '#f2f2f2',
          borderRadius: 4,
          display: 'flex',
          justifyContent: 'center',
        }),
        ...style,
      }}>
      {items.map((item, index) => {
        const base64Img = item.isNew
          ? transformImageThumbnail(item.fileExt, item.thumbnail)
          : `data:image/png;base64,${item.thumbnail}`;

        return (
          <Card
            key={item.id || index}
            style={{
              display: 'inline-flex',
              marginLeft: '1rem',
              backgroundColor: '#fefefe',
            }}>
            <AttachmentsStyled.GridItemContainer>
              <AttachmentsStyled.CardActionArea
                onClick={handleViewFile(item)}
                style={{
                  width: 140,
                  height: 120,
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'center',
                  paddingTop: '1rem',
                  ...(disablePreview && {
                    pointerEvents: 'none',
                  }),
                  ...actionContainerStyle,
                }}>
                <Box
                  style={{
                    flex: 1,
                    overflow: 'hidden',
                    ...imageContainerStyle,
                  }}>
                  <CardMedia
                    style={{
                      maxHeight: '100%',
                    }}
                    component={(props) => {
                      return <Image {...props} src={base64Img} />;
                    }}
                    src={base64Img}
                  />
                </Box>
                {!hideFilename && (
                  <Styled.EmailAttachmentCardContent>
                    <Styled.EmailAttachmentLabel
                      variant="caption"
                      color="textSecondary">
                      {item.fileName}
                    </Styled.EmailAttachmentLabel>
                  </Styled.EmailAttachmentCardContent>
                )}
              </AttachmentsStyled.CardActionArea>
              {!disableDelete && (
                <WidgetsStyled.CardActions
                  disableSpacing
                  style={{
                    borderRadius: '100%',
                  }}>
                  <WidgetsStyled.ActionIconButton onClick={onDelete(item.id)}>
                    <DeleteIcon
                      style={{
                        fontSize: '0.875rem',
                        color: 'white',
                      }}
                    />
                  </WidgetsStyled.ActionIconButton>
                </WidgetsStyled.CardActions>
              )}
            </AttachmentsStyled.GridItemContainer>
          </Card>
        );
      })}

      <FileViewer
        src={selectedFilePreview?.src}
        headerTitle={selectedFilePreview?.fileName}
        isPDF={selectedFilePreview?.isPDF}
        download={iframeIncompatible(selectedFilePreview?.fileExt, ['csv'])}
        onClose={handleCloseFileViewer}
        ModalProps={{
          contentWidth: '100%',
          contentHeight: '90%',
        }}
      />
    </Box>
  );
};

export default AttachmentsGrid;
