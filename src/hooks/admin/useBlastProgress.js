import React, {useMemo} from 'react';
import Typography from '@mui/material/Typography';
import useQuery from '@macanta/hooks/apollo/useQuery';
import {LIST_BLAST_PROGRESS} from '@macanta/graphql/admin';
import {timeDiff} from '@macanta/utils/time';
import * as Storage from '@macanta/utils/storage';

const useBlastProgress = (userId) => {
  const userDetails = Storage.getItem('userDetails');

  const blastProgressQuery = useQuery(LIST_BLAST_PROGRESS, {
    variables: {
      userId: userId || userDetails.userId,
    },
    onError() {},
  });

  const blastProgressItems = blastProgressQuery.data?.listBlastProgress?.items;

  const {
    formattedBlastProgressData,
    pendingItems,
    pendingProgress,
    doneItems,
    updatedSince,
  } = useMemo(() => {
    if (blastProgressItems) {
      const formattedBlastProgressItems = blastProgressItems.map((item) => ({
        ...item,
        queryId: item.queryId && item.queryId.split('|').join(', '),
        groupName: item.groupName && item.groupName.split('|').join(', '),
        widgetName: item.widgetName && item.widgetName.split('|').join(', '),
        updated: item.updated
          ? [
              {
                value: item.updated,
                renderLabel: () => {
                  return (
                    <Typography
                      style={{
                        color: '#888',
                        fontSize: 'inherit',
                      }}>
                      {timeDiff(item.updated)}
                    </Typography>
                  );
                },
              },
            ]
          : '',
        status: [
          {
            value: item.status,
            renderLabel: () => {
              return (
                <Typography
                  style={{
                    fontSize: 'inherit',
                    fontWeight: 'bold',
                  }}
                  sx={{
                    color: (theme) =>
                      item.status.toLowerCase() === 'done'
                        ? theme.palette.success.main
                        : theme.palette.warning.main,
                  }}>
                  {item.status}
                </Typography>
              );
            },
          },
        ],
        runtime: item.runtime ? `${item.runtime} sec` : '',
      }));

      const pending = blastProgressItems.filter(
        (item) => item.status.toLowerCase() !== 'done',
      );
      const pProgress = pending.reduce(
        (acc, item) => {
          const progressArr = item.progress ? item.progress.split('of') : '';
          const finishedCount =
            acc[0] + parseInt(progressArr[0] ? progressArr[0].trim() : 0);
          const totalCount =
            acc[1] + parseInt(progressArr[1] ? progressArr[1].trim() : 0);

          return [finishedCount, totalCount];
        },
        [0, 0],
      );
      const done = blastProgressItems.filter(
        (item) => item.status.toLowerCase() === 'done',
      );

      return {
        formattedBlastProgressData: formattedBlastProgressItems,
        pendingItems: pending,
        pendingProgress: pProgress.join(' of '),
        doneItems: done,
        updatedSince: blastProgressItems.reduce((acc, item) => {
          if (item.updated > acc) {
            return item.updated;
          }

          return acc;
        }, ''),
      };
    }

    return {
      pendingItems: [],
      pendingProgress: '',
      doneItems: [],
      updatedSince: '',
    };
  }, [blastProgressItems]);

  return {
    data: formattedBlastProgressData,
    loading: blastProgressQuery.loading,
    pendingItems,
    pendingProgress,
    doneItems,
    updatedSince,
  };
};

export default useBlastProgress;
