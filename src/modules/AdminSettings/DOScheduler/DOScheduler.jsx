import React from 'react';
import {navigate} from 'gatsby';
import Breadcrumbs from '@mui/material/Breadcrumbs';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import Typography from '@mui/material/Typography';
import Section from '@macanta/containers/Section';
import {ROOT_PATH_ADMIN} from '@macanta/modules/AdminSettings/AdminSettings';
import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import * as AdminSettingsStyled from '@macanta/modules/AdminSettings/styles';
import DOTypesSelectButton from '@macanta/modules/AdminSettings/DOScheduler/DOTypesSelectButton';
import DOSchedulesList from '@macanta/modules/AdminSettings/DOScheduler/DOSchedulesList';
import useMutation from '@macanta/hooks/apollo/useMutation';
import {
  CREATE_OR_UPDATE_DO_SCHEDULE,
  DELETE_DO_SCHEDULE,
  DO_SCHEDULE_FRAGMENT,
} from '@macanta/graphql/scheduler';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import {writeItems} from '@macanta/graphql/helpers';

const DOScheduler = () => {
  /* const navigate = useNavigate();*/

  const [callCreateOrUpdateDOSchedule, createOrUpdateDOSchedule] = useMutation(
    CREATE_OR_UPDATE_DO_SCHEDULE,
    {
      update(cache, {data: {createOrUpdateDOSchedule}}, options) {
        writeItems({
          isCreate: !options.variables.input?.id,
          item: createOrUpdateDOSchedule,
          typeName: 'DOSchedule',
          fragment: DO_SCHEDULE_FRAGMENT,
          listKey: 'listDOSchedules',
          position: 'last',
        });
      },
    },
  );
  const [callDeleteDOSchedule, deleteDOSchedule] = useMutation(
    DELETE_DO_SCHEDULE,
    {
      update(cache, {data: {deleteDOSchedule}}) {
        const normalizedId = cache.identify({
          id: deleteDOSchedule.id,
          __typename: 'DOSchedule',
        });
        cache.evict({id: normalizedId});
        cache.gc();
      },
    },
  );

  const handleBack = () => {
    navigate(ROOT_PATH_ADMIN);
  };

  const handleSave = async (values) => {
    await callCreateOrUpdateDOSchedule({
      variables: {
        id: values.id,
        groupId: values.groupId,
        name: values.name,
        description: values.description,
        userRelationshipId: values.userRelationshipId,
        contactRelationshipId: values.contactRelationshipId,
        startDateFieldId: values.startDateFieldId,
        endDateFieldId: values.endDateFieldId || '',
        schedulerType: values.schedulerType,
      },
    });
  };

  const handleDelete = async (id) => {
    await callDeleteDOSchedule({
      variables: {
        id,
      },
    });
  };

  return (
    <>
      <NoteTaskHistoryStyled.FullHeightGrid container>
        <NoteTaskHistoryStyled.FullHeightFlexGridColumn item xs={12}>
          <Section
            fullHeight
            HeaderLeftComp={
              <Breadcrumbs aria-label="breadcrumb">
                <AdminSettingsStyled.TitleCrumbBtn
                  startIcon={
                    <ArrowBackIcon
                      style={{
                        color: '#333',
                      }}
                    />
                  }
                  onClick={handleBack}
                  disabled={false}>
                  <Typography color="textPrimary">Admin Settings</Typography>
                </AdminSettingsStyled.TitleCrumbBtn>
                <Typography>Scheduler</Typography>
              </Breadcrumbs>
            }
            HeaderRightComp={
              <DOTypesSelectButton
                onSave={handleSave}
                getModalProps={(doType) => ({
                  headerTitle: `Add Schedule (${doType.title})`,
                })}>
                Add Schedule
              </DOTypesSelectButton>
            }>
            <DOSchedulesList onSave={handleSave} onDelete={handleDelete} />
          </Section>
        </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
      </NoteTaskHistoryStyled.FullHeightGrid>

      <LoadingIndicator
        modal
        loading={createOrUpdateDOSchedule.loading || deleteDOSchedule.loading}
      />
    </>
  );
};

export default DOScheduler;
