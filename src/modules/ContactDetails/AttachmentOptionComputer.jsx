import React, {useState} from 'react';
import UploadForms from '@macanta/modules/DataObjectItems/DOItemForms/Attachments/UploadForms';
import Modal from '@macanta/components/Modal';

const AttachmentOptionComputer = ({
  showModal: showModalProp = false,
  onClose,
  onSelect,
}) => {
  const [showModal, setShowModal] = useState(showModalProp);

  const handleCloseModal = () => {
    setShowModal(false);

    onClose && onClose();
  };

  const handleSuccess = (...args) => {
    handleCloseModal();

    onSelect(...args);
  };

  return (
    <Modal
      headerTitle="Attach Files"
      open={showModal}
      onClose={handleCloseModal}
      contentWidth={800}
      contentHeight={600}>
      <UploadForms onSubmit={handleSuccess} />
    </Modal>
  );
};

export default AttachmentOptionComputer;
