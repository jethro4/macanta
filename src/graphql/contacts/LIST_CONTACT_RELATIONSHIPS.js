import {gql} from '@apollo/client';

export default gql`
  query listContactRelationships($id: ID, $type: String) {
    listContactRelationships(id: $id, type: $type) {
      contactId
      name
      email
      relationships {
        relId
        name
      }
      directRelationId
    }
  }
`;
