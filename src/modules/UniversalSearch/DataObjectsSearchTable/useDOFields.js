import useSessionQuery from '@macanta/hooks/apollo/useSessionQuery';
import {LIST_DATA_OBJECT_FIELDS} from '@macanta/graphql/dataObjects';
import * as Storage from '@macanta/utils/storage';

const useDOFields = (groupId, optionsProp = {}) => {
  const {email: loggedInUserEmail} = Storage.getItem('userDetails');

  const {removeEmail, userEmail, ...options} = optionsProp;

  const doFieldsQuery = useSessionQuery(LIST_DATA_OBJECT_FIELDS, {
    variables: {
      groupId,
      ...(!removeEmail && {
        userEmail: userEmail || loggedInUserEmail,
      }),
    },
    ...options,
    skip: !groupId || options?.skip,
  });

  return {...doFieldsQuery, items: doFieldsQuery.data?.listDataObjectFields};
};

export default useDOFields;
