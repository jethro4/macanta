import {gql} from '@apollo/client';

export default gql`
  query getWidgetCounts($userId: String!, $queryId: String!) {
    getWidgetCounts(userId: $userId, queryId: $queryId) {
      currentCount
      last7DayCount
      last30DayCount
      last90DayCount
      allCount
    }
  }
`;
