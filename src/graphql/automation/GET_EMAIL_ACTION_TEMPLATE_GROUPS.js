import {gql} from '@apollo/client';

export const EMAIL_ACTION_TEMPLATE_GROUPS_FRAGMENT = gql`
  fragment emailActionTemplateGroupsFragment on EmailActionTemplateGroups {
    action
    status
    count
    data {
      Id
      Title
      UserContactId
      Created
      Updated
    }
  }
`;

export default gql`
  query getEmailActionTemplateGroups($appName: String!, $apiKey: String!) {
    getEmailActionTemplateGroups(appName: $appName, apiKey: $apiKey) {
      ...emailActionTemplateGroupsFragment
    }
  }
  ${EMAIL_ACTION_TEMPLATE_GROUPS_FRAGMENT}
`;
