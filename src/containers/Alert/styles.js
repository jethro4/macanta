import {experimentalStyled as styled} from '@mui/material/styles';
import Box from '@mui/material/Box';
import SnackbarComp from '@mui/material/Snackbar';
import AlertComp from '@macanta/components/Alert';

export const Snackbar = styled(SnackbarComp)``;

export const Alert = styled(AlertComp)``;

export const MessageContainer = styled(Box)`
  * {
    max-width: 500px;
    color: white !important;
  }
`;
