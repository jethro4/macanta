import React from 'react';
import SectionComp from './Section';
import SubSection from './SubSection';

const Section = ({subsection, ...props}) => {
  if (subsection) {
    return <SubSection {...props} />;
  }

  return <SectionComp {...props} />;
};

export default Section;

export {SubSection};
