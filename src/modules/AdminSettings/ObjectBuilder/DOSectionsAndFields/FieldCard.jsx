import React from 'react';
import EditIcon from '@mui/icons-material/Edit';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import {OverflowTip} from '@macanta/components/Tooltip';
import {PopoverButton, DialogButton} from '@macanta/components/Button';
import {FIELD_TYPES} from '@macanta/constants/form';
import useValue from '@macanta/hooks/base/useValue';
import FlexGrid from '@macanta/components/Base/FlexGrid';

const FieldCard = ({item, hideFieldName, onDelete, ...props}) => {
  const type = item.dataReference ? 'DataReference' : item.type;

  const [{typeLabel, otherLabels}] = useValue(() => {
    const obj = {
      typeLabel: FIELD_TYPES.find((t) => t.value === type)?.label,
      otherLabels: [],
    };

    if (item.contactSpecificField) {
      obj.otherLabels.push('Contact Specific');
    }

    return obj;
  }, [type, item.contactSpecificField]);

  return (
    <Box
      style={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        backgroundColor: 'white',
        padding: '4px 12px',
        borderRadius: 8,
        boxShadow: '#ccc 0px 2px 4px -2px',
        height: '100%',
      }}>
      <Box
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <OverflowTip
          style={{
            fontSize: 14,
          }}>
          {item.label || item.name}
          {item.required && (
            <Typography
              style={{
                marginLeft: '4px',
                fontSize: '1em',
              }}
              component="span"
              color="error">
              *
            </Typography>
          )}
        </OverflowTip>

        <Box
          style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            marginLeft: '1rem',
            marginTop: '-2px',
          }}>
          <PopoverButton
            style={{
              padding: 0,
            }}
            icon
            sx={{
              padding: '0.25rem',
              color: 'info.main',
            }}
            // onClick={handleAdd}
            size="small"
            TooltipProps={{
              title: 'Edit Field',
            }}
            PopoverProps={{
              title: `Edit Field ${!hideFieldName ? `(${item.id})` : ''}`,
              contentWidth: 1000,
              anchorOrigin: {
                vertical: 'bottom',
                horizontal: 'center',
              },
              transformOrigin: {
                vertical: -8,
                horizontal: 'center',
              },
            }}
            {...props}>
            <EditIcon
              style={{
                fontSize: 18,
              }}
            />
          </PopoverButton>
          <DialogButton
            className="delete-field-btn"
            style={{
              padding: 0,
            }}
            icon
            size="small"
            TooltipProps={{title: 'Delete Field'}}
            DialogProps={{
              title: 'Are you sure?',
              description: [
                `You are about to delete "${
                  item.label || item.name
                }" from fields.`,
                'Do you want to proceed?',
              ],
              onActions: (handleClose) => {
                return [
                  {
                    label: 'Delete',
                    onClick: () => {
                      onDelete(item.id);
                      handleClose();
                    },
                    startIcon: <DeleteForeverIcon />,
                  },
                ];
              },
            }}>
            <DeleteForeverIcon
              style={{
                fontSize: 18,
              }}
              sx={{
                color: 'grayDarker.main',
              }}
            />
          </DialogButton>
        </Box>
      </Box>
      <FlexGrid
        style={{
          marginTop: 4,
          justifyContent: 'space-between',
          alignItems: 'flex-end',
        }}>
        <Typography
          style={{
            fontSize: 13,
            color: '#aaa',
          }}>
          {typeLabel}
        </Typography>

        <Typography
          colWidth="fit-content"
          sx={{
            fontSize: '11px',
            fontWeight: 'bold',
            color: '#bbb',
          }}>
          {otherLabels.join(' - ')}
        </Typography>
      </FlexGrid>
    </Box>
  );
};

export default FieldCard;
