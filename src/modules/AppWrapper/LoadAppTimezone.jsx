import {useLayoutEffect} from 'react';
import useAppSettings from '@macanta/hooks/admin/useAppSettings';
import moment from 'moment-timezone';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import * as Storage from '@macanta/utils/storage';

const LoadAppTimezone = () => {
  const cachedAppTimezone = Storage.getItem('appTimezone');

  const appSettingsQuery = useAppSettings({
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const appTimezone = appSettingsQuery.data?.appTimeZone || cachedAppTimezone;

  useLayoutEffect(() => {
    if (appTimezone) {
      console.info(`Timezone: ${appTimezone}`);

      moment.tz.setDefault(appTimezone);

      Storage.setItem('appTimezone', appTimezone);
    }
  }, [appTimezone]);

  return null;
};

export default LoadAppTimezone;
