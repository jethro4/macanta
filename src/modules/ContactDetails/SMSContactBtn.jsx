import React, {useState} from 'react';
import Modal from '@macanta/components/Modal';
import SMSContactForms from './SMSContactForms';
import TextsmsIcon from '@mui/icons-material/Textsms';
import Button from '@macanta/components/Button';
import useProgressAlert from '@macanta/hooks/useProgressAlert';

const SMSContactBtn = ({
  defaultPhoneNumber,
  phoneNumbers,
  contactId,
  ...props
}) => {
  const [showModal, setShowModal] = useState(false);
  const {displayMessage} = useProgressAlert();

  const handleAddContact = () => {
    setShowModal(true);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  const onSuccess = () => {
    displayMessage('SMS sent!');
    setShowModal(false);
  };

  return (
    <>
      <Button
        onClick={handleAddContact}
        size="small"
        startIcon={<TextsmsIcon />}
        {...props}>
        SMS
      </Button>
      <Modal
        headerTitle={`Send SMS`}
        open={showModal}
        onClose={handleCloseModal}>
        <SMSContactForms
          defaultPhoneNumber={defaultPhoneNumber}
          phoneNumbers={phoneNumbers}
          onSuccess={onSuccess}
          contactId={contactId}
        />
      </Modal>
    </>
  );
};

export default SMSContactBtn;
