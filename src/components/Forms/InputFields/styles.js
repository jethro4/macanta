import {experimentalStyled as styled} from '@mui/material/styles';

import MUIListItem from '@mui/material/ListItem';

export const SelectListItem = styled(MUIListItem)`
  .MuiListItemIcon-root {
    min-width: 38px;
  }

  .MuiListItemText-root {
    margin: 0;
  }

  &.MuiAutocomplete-option.Mui-focusVisible {
    border-left: 1px solid ${({theme}) => theme.palette.tertiary.main};
  }
`;
