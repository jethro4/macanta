import useValue from '@macanta/hooks/base/useValue';
import {ACCESS_LEVELS} from '@macanta/constants/accessLevels';

const CONTACT_VIEW_PERMISSION_TYPES = [
  {id: 'TypeA', label: 'Contact with which the user shares a Data object'},
  {id: 'TypeB', label: 'Contact with a specific relationship to a Data object'},
  {id: 'TypeC', label: 'Contact with NO relationship to this Data object'},
];

const CONTACT_VIEW_GLOBAL_PERMISSION_TYPE = [{id: 'TypeA', label: 'Global'}];

const useContactViewPermissions = ({groupId, permission}) => {
  const [data] = useValue(() => {
    const permissionTypes =
      groupId === 'Global'
        ? CONTACT_VIEW_GLOBAL_PERMISSION_TYPE
        : CONTACT_VIEW_PERMISSION_TYPES;

    return permissionTypes.map((type) => {
      const contactViewAccess = permission?.contacts?.find(
        (p) => p.groupId === groupId,
      )?.access;
      const permissionItem = contactViewAccess?.find((a) => type.id === a.name);
      const contactViewPermission =
        permissionItem?.permission || ACCESS_LEVELS.NO_ACCESS;

      return {
        ...type,
        groupId,
        hasAccess: [ACCESS_LEVELS.READ_ONLY, ACCESS_LEVELS.READ_WRITE].includes(
          contactViewPermission,
        ),
        permission: contactViewPermission,
        ...(permissionItem?.name === 'TypeB' && {
          relationships: permissionItem.relationships || [],
        }),
      };
    });
  }, [groupId, permission]);

  return {data};
};

export default useContactViewPermissions;
