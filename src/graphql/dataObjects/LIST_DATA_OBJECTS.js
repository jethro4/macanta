import {gql} from '@apollo/client';

export default gql`
  query listDataObjects(
    $q: String
    $groupId: ID
    $contactId: ID
    $relationship: String
    $page: Int
    $limit: Int
    $order: String
    $orderBy: String
    $contactField: Boolean
    $filter: String
  ) {
    listDataObjects(
      q: $q
      groupId: $groupId
      contactId: $contactId
      relationship: $relationship
      page: $page
      limit: $limit
      order: $order
      orderBy: $orderBy
      contactField: $contactField
      filter: $filter
    ) {
      total
      items {
        groupId
        type
        items {
          id
          data {
            id
            fieldId
            value
            contactSpecificValues {
              contactId
              value
            }
          }
          connectedContacts {
            contactId
            email
            firstName
            lastName
            relationships {
              id
            }
          }
        }
      }
    }
  }
`;
