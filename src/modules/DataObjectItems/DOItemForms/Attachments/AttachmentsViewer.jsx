import React, {useState, useMemo} from 'react';
import groupBy from 'lodash/groupBy';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import ButtonGroup from '@macanta/components/ButtonGroup';
import Button from '@macanta/components/Button';
import FileViewer from '@macanta/containers/FileViewer';
import AppsIcon from '@mui/icons-material/Apps';
import ListIcon from '@mui/icons-material/List';
import DownloadIcon from '@mui/icons-material/Download';
import {getAppInfo} from '@macanta/utils/app';
import {iframeIncompatible} from '@macanta/utils/file';
import {
  sortArrayByKeyCondition,
  sortArrayByObjectKey,
} from '@macanta/utils/array';
import ItemGrid from './ItemGrid';
import ItemList from './ItemList';
import * as ListStyled from '@macanta/containers/List/styles';
import * as Styled from './styles';
import envConfig from '@macanta/config/envConfig';

const AttachmentsViewer = ({
  loading,
  attachments,
  doItemId,
  onDelete,
  renderGridActions,
  renderListActions,
}) => {
  const [selectedFile, setSelectedFile] = useState(null);
  const [itemViewStyle, setItemViewStyle] = useState('grid');

  const handleViewFile = (src, fileName, fileExt, isPDF) => () => {
    setSelectedFile({src, fileName, fileExt, isPDF});
  };

  const handleCloseFileViewer = () => {
    setSelectedFile(null);
  };

  const handleDownload = (fileId) => (event) => {
    event.stopPropagation();

    const [appName, apiKey] = getAppInfo();

    const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/data_object/download_attachments/${doItemId}/file/${fileId}?api_key=${apiKey}`;

    window.location.href = url;
  };

  const handleDownloadAll = () => {
    const [appName, apiKey] = getAppInfo();

    const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/data_object/download_attachments/${doItemId}?api_key=${apiKey}`;

    window.location.href = url;
  };

  const handleViewStyle = (viewStyle) => () => {
    setItemViewStyle(viewStyle);
  };

  const sortedAttachments = useMemo(() => {
    const sortedItems = sortArrayByKeyCondition(
      sortArrayByObjectKey(attachments, 'fieldName'),
      'isGeneral',
      (value) => !!value,
    );
    const groupedItemsObj = groupBy(sortedItems, 'isGeneral');
    const fieldNamesObj = groupBy(groupedItemsObj['false'], 'fieldName');
    const generalItems = {
      General: [
        ...(groupedItemsObj['undefined'] || []),
        ...(groupedItemsObj['true'] || []),
      ],
    };
    const items = {
      ...generalItems,
      ...fieldNamesObj,
    };

    return items;
  }, [attachments]);

  return (
    <>
      {attachments.length > 0 && (
        <Styled.SubButtons>
          <Styled.SubButtonIcons>
            <ButtonGroup
              size="small"
              variant="text"
              aria-label="primary button group">
              <Styled.SubButtonIcon
                style={{
                  color: '#888',
                }}
                onClick={handleViewStyle('grid')}>
                <AppsIcon />
              </Styled.SubButtonIcon>
              <Styled.SubButtonIcon
                style={{
                  color: '#888',
                }}
                onClick={handleViewStyle('list')}>
                <ListIcon />
              </Styled.SubButtonIcon>
            </ButtonGroup>
          </Styled.SubButtonIcons>
          <Button
            onClick={handleDownloadAll}
            color="gray"
            size="small"
            variant="contained"
            startIcon={<DownloadIcon />}>
            Download All
          </Button>
        </Styled.SubButtons>
      )}
      {!loading && attachments.length === 0 && (
        <ListStyled.EmptyMessage align="center" color="#888">
          No attachments found
        </ListStyled.EmptyMessage>
      )}
      {Object.entries(sortedAttachments).map(([title, items]) => {
        return (
          !!items.length && (
            <>
              <Divider
                style={{
                  margin: '1rem',
                  borderBottom: '1px dashed #eee',
                }}
              />
              <Typography
                sx={{
                  color: 'grayDarkest.main',
                  fontSize: '0.875rem',
                  fontWeight: 'bold',
                  marginLeft: '1rem',
                }}>
                {title}
              </Typography>
              {itemViewStyle === 'grid' ? (
                <ItemGrid
                  items={items}
                  onViewFile={handleViewFile}
                  onDelete={onDelete}
                  onDownload={handleDownload}
                  renderGridActions={renderGridActions}
                />
              ) : (
                <ItemList
                  items={items}
                  onViewFile={handleViewFile}
                  onDelete={onDelete}
                  onDownload={handleDownload}
                  renderListActions={renderListActions}
                />
              )}
            </>
          )
        );
      })}
      <FileViewer
        src={selectedFile?.src}
        headerTitle={selectedFile?.fileName}
        isPDF={selectedFile?.isPDF}
        download={iframeIncompatible(selectedFile?.fileExt)}
        onClose={handleCloseFileViewer}
        ModalProps={{
          contentWidth: '100%',
          contentHeight: '90%',
        }}
      />
    </>
  );
};

AttachmentsViewer.defaultProps = {
  loading: false,
  attachments: [],
};

export default AttachmentsViewer;
