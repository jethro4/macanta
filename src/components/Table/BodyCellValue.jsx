import React from 'react';
import {ButtonLink} from '@macanta/components/Button';

import * as Styled from './styles';

let BodyCellValue = ({data}) => {
  let label = '';

  if (data.link) {
    label = (
      <ButtonLink
        to={data.link}
        style={{
          color: '#2196f3',
          padding: 0,
        }}>
        {data.label || data.value}
      </ButtonLink>
    );
  } else if (data.renderLabel) {
    label = data.renderLabel();
  } else {
    label = data.label || data.value;
  }

  return (
    <React.Fragment>
      {label}
      {!!data.subtext && <Styled.Subtext>{data.subtext}</Styled.Subtext>}
    </React.Fragment>
  );
};

export default BodyCellValue;
