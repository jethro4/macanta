import {useState} from 'react';
import isFunction from 'lodash/isFunction';
import useNextEffect from './useNextEffect';

const useValueShallow = (valueProp, dependencyKeys) => {
  const [value, setValue] = useState(valueProp);
  const valueIsFunc = isFunction(valueProp);
  const dependencies =
    valueIsFunc || dependencyKeys ? [...dependencyKeys] : [valueProp];

  useNextEffect(() => {
    setValue((prevValue) => (valueIsFunc ? valueProp(prevValue) : valueProp));
  }, dependencies);

  return [value, setValue];
};

export default useValueShallow;
