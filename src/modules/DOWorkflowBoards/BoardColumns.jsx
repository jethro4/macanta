import React, {useState, useLayoutEffect} from 'react';
import Box from '@mui/material/Box';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import useWorkflowBoards from '@macanta/hooks/admin/useWorkflowBoards';
import useDOFields from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOFields';
import useDORelationships from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDORelationships';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import BoardColumn from './BoardColumn';

const BoardColumns = ({groupId, boardId}) => {
  const [boardChoices, setBoardChoices] = useState([]);
  const [latestDroppedChoice, setLatestDroppedChoice] = useState('');

  const workflowBoardsQuery = useWorkflowBoards({
    id: boardId,
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });
  const doFieldsQuery = useDOFields(groupId, {
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });
  const relationshipsQuery = useDORelationships(groupId, {
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const selectedBoard = workflowBoardsQuery.selectedBoard;
  const doFields = doFieldsQuery.data?.listDataObjectFields;
  const relationships = relationshipsQuery.data?.listRelationships;

  useLayoutEffect(() => {
    if (boardId) {
      setBoardChoices([]);
    }
  }, [boardId]);

  useLayoutEffect(() => {
    if (selectedBoard && doFields) {
      const doField = doFields.find(
        (field) => selectedBoard.keyField === field.name,
      );
      const choices = doField?.choices || [doField?.name];

      setBoardChoices(choices);
    }
  }, [selectedBoard, doFields]);

  return (
    <Box
      style={{
        display: 'flex',
        padding: '0 8px',
        flex: 1,
      }}>
      {boardChoices.map((item, index) => (
        <BoardColumn
          key={`${boardId}-${item}`}
          choice={item}
          index={index}
          groupId={groupId}
          boardId={selectedBoard?.id}
          type={selectedBoard?.type}
          relationship={selectedBoard?.relationship}
          activeColumnsColor={selectedBoard?.activeColumnsColor}
          doFields={doFields}
          relationships={relationships}
          keyField={selectedBoard?.keyField}
          displayFields={selectedBoard?.displayFields}
          latestDroppedChoice={latestDroppedChoice}
          setLatestDroppedChoice={setLatestDroppedChoice}
        />
      ))}
      <LoadingIndicator
        fill
        align="top"
        style={{
          top: '2.5rem',
        }}
        loading={!doFields && doFieldsQuery.loading}
      />
    </Box>
  );
};

export default BoardColumns;
