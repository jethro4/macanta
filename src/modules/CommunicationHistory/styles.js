import isNumber from 'lodash/isNumber';
import {experimentalStyled as styled} from '@mui/material/styles';
import EmailPreviewModule from '@macanta/modules/ContactDetails/EmailPreview';
import SMSPreviewModule from './SMSPreview';
import ResendButtonModule from './ResendButton';
import * as TableStyled from '@macanta/components/Table/styles';

export const Text = styled(TableStyled.TableBodyCellLabel)`
  line-height: 20px;
`;

export const Status = styled(Text)`
  color: ${({success, theme}) =>
    success ? theme.palette.success.main : theme.palette.error.main};
  font-weight: bold;
`;

export const Stats = styled(Text)`
  color: ${({children, theme}) =>
    isNumber(children)
      ? children
        ? theme.palette.success.main
        : theme.palette.text.primary
      : theme.palette.text.secondary};
  font-weight: bold;
`;

export const ToFrom = styled(Text)`
  font-weight: bold;
`;

export const Timezone = styled(Text)`
  font-size: 12px;
`;

export const EmailPreview = styled(EmailPreviewModule)`
  padding: 0.25rem;
  color: ${({theme}) => theme.palette.info.main};
  margin-left: 0;
`;

export const SMSPreview = styled(SMSPreviewModule)`
  padding: 0.25rem;
  color: ${({theme}) => theme.palette.info.main};
  margin-left: 0;
`;

export const ResendButton = styled(ResendButtonModule)`
  padding: 0.25rem;
  color: ${({theme}) => theme.palette.info.main};
  margin-left: 0;
`;
