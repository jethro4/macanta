import {experimentalStyled as styled} from '@mui/material/styles';

import Box from '@mui/material/Box';
import MUIFormControlLabel from '@mui/material/FormControlLabel';
import MUIRadio from '@mui/material/Radio';

export const Root = styled(Box)`
  display: flex;
  flex-direction: ${({row}) => (!row ? 'column' : 'row')};
  flex-wrap: ${({row}) => (!row ? 'nowrap' : 'wrap')};
  position: relative;

  .MuiFormControlLabel-root:not(.Mui-disabled) {
    transition: transform 0.25s ease-in-out;

    &:hover .MuiTypography-root {
      opacity: 0.8;
    }
  }
`;

export const FormControlLabel = styled(MUIFormControlLabel)`
  margin-left: -9px;

  ${({noLabel}) =>
    noLabel &&
    `
      .MuiRadio-root {
        margin-right: 0;

        .MuiSvgIcon-root {
          font-size: 22px;
        }
      }
    `}
`;

export const Radio = styled(MUIRadio)`
  ${({disabled, checked, theme}) =>
    `
      ${
        disabled
          ? `color: ${checked ? theme.palette.primary.main : '#aaa'}!important;`
          : ''
      }
    `}
`;
