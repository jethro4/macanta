import {gql} from '@apollo/client';

export default gql`
  query listMergeFields($appName: String!, $apiKey: String!, $types: String!) {
    listMergeFields(appName: $appName, apiKey: $apiKey, types: $types) {
      mergeFields {
        category
        entries {
          label
          value
        }
      }
    }
  }
`;
