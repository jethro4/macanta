import {gql} from '@apollo/client';

export default gql`
  query getValidEmail($email: String!, $userId: ID!) {
    getValidEmail(email: $email, userId: $userId) {
      mxFound
    }
  }
`;
