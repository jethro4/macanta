import {gql} from '@apollo/client';

export default gql`
  mutation deleteUser($input: DeleteUserInput!) {
    deleteUser(input: $input) {
      success
      id
    }
  }
`;
