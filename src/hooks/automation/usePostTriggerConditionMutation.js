import useMutation from '@macanta/hooks/apollo/useMutation';
import {POST_TRIGGER_CONDITION} from '@macanta/graphql/automation';

const usePostTriggerConditionMutation = (options) => {
  const mutation = useMutation(POST_TRIGGER_CONDITION, options);

  return mutation;
};

export default usePostTriggerConditionMutation;
