import React, {useState, useMemo} from 'react';
import range from 'lodash/range';
import {useFormikContext} from 'formik';
import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';
import Box from '@mui/material/Box';
import Button from '@macanta/components/Button';
import FormField from '@macanta/containers/FormField';
import {ordinalNumberOf} from '@macanta/utils/string';
import {arrayConcatOrUpdateByKey, moveItem} from '@macanta/utils/array';

const SectionForm = ({id, name, position, lastPosition, onClose}) => {
  const {setValues} = useFormikContext();

  const [item, setItem] = useState({
    id,
    name,
    position,
  });

  const isNew = !item.id;

  const handleChange = (key) => (value) => {
    setItem((state) => ({...state, [key]: value}));
  };

  const handleAddOrEdit = () => {
    const updatedId = item.id || String(Date.now());

    setValues((state) => {
      const sectionItem = state.sectionsData.find(
        (section) => section.id === updatedId,
      ) || {
        id: updatedId,
        sectionName: item.name,
        subGroups: [],
      };
      const sectionsData = arrayConcatOrUpdateByKey({
        arr: state.sectionsData,
        item: {
          ...sectionItem,
          sectionName: item.name,
        },
      });
      const sortedSectionsData = moveItem({
        arr: sectionsData,
        fromIndex: position - 1,
        toIndex: item.position - 1,
      });

      return {
        ...state,
        sectionsData: sortedSectionsData,
        updatedId,
      };
    });

    onClose();
  };

  const ordinalPositions = useMemo(() => {
    return range(1, lastPosition + (!isNew ? 1 : 2)).map((pos) => ({
      label: ordinalNumberOf(pos),
      value: String(pos),
    }));
  }, []);

  return (
    <>
      <Box
        style={{
          padding: '1rem',
        }}>
        <FormField
          required
          autoFocus={isNew}
          labelPosition="normal"
          value={item.name}
          onChange={handleChange('name')}
          label="Section Name"
          size="small"
          variant="outlined"
        />

        <FormField
          labelPosition="normal"
          type="Select"
          options={ordinalPositions}
          value={String(item.position)}
          onChange={handleChange('position')}
          label="Position"
          size="small"
          variant="outlined"
        />
      </Box>
      <Box
        style={{
          display: 'flex',
          justifyContent: 'flex-end',
          padding: '12px 1rem',
          borderTop: '1px solid #eee',
        }}>
        <Button
          disabled={!item.name}
          variant="contained"
          startIcon={isNew ? <AddIcon /> : <EditIcon />}
          onClick={handleAddOrEdit}
          size="medium">
          {isNew ? 'Add' : 'Save Details'}
        </Button>
      </Box>
    </>
  );
};

export default SectionForm;
