import React, {useState} from 'react';
import PropTypes from 'prop-types';
import TextField from '@macanta/components/Forms/InputFields/TextField';
import InputAdornment from '@mui/material/InputAdornment';
import {IconButton} from '@macanta/components/Button';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';

const PasswordField = ({allowToggle, ...props}) => {
  const [values, setValues] = useState({
    password: '',
    showPassword: false,
  });

  const handleClickShowPassword = () => {
    setValues({
      ...values,
      showPassword: !values.showPassword,
    });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  return (
    <TextField
      {...props}
      type={values.showPassword ? 'text' : 'password'}
      {...(allowToggle && {
        InputProps: {
          endAdornment: (
            <InputAdornment position="end">
              <IconButton
                aria-label="toggle password visibility"
                onClick={handleClickShowPassword}
                onMouseDown={handleMouseDownPassword}
                edge="end">
                {values.showPassword ? <Visibility /> : <VisibilityOff />}
              </IconButton>
            </InputAdornment>
          ),
        },
      })}
    />
  );
};

PasswordField.propTypes = {
  allowToggle: PropTypes.bool,
};

PasswordField.defaultProps = {
  allowToggle: true,
};

export default PasswordField;
