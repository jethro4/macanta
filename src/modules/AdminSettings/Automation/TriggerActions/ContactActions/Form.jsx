import React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import FlexGrid from '@macanta/components/Base/FlexGrid';
import BasicInfoForm from '@macanta/modules/AdminSettings/Automation/TriggerActions/SubForms/BasicInfoForm';
import withForm from '@macanta/components/Form/withForm';
import useDORelationships from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDORelationships';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import BodySection from '@macanta/modules/AdminSettings/UserManagement/BodySection';
import Divider from '@macanta/components/Base/Divider';
import FormField from '@macanta/containers/FormField';
import NoteForms from '@macanta/modules/NoteTaskForms/NoteForms';
import TaskForms from '@macanta/modules/NoteTaskForms/TaskForms';
import SelectUserField from '@macanta/modules/NoteTaskForms/SelectUserField';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import ActionForm from '@macanta/modules/AdminSettings/Automation/TriggerActions/SubForms/ActionForm';

const actionTypes = ['Note', 'Task'];
const assignTypes = ['User', 'Relationship'];

let Form = (props) => {
  const {values, setValues, setFieldValue} = props;

  const isNote = values.actionType === 'Note';

  const initValues = isNote
    ? {
        title: values.title,
        note: values.note,
        tags: values.tags,
      }
    : {
        title: values.title,
        // userId: values.userId,
        userEmail: values.userEmail,
        note: values.note,
        tags: values.tags,
        actionDate: values.actionDate,
      };

  const doTypesQuery = useDOTypes({
    removeEmail: true,
    includeContactObject: true,
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const doRelationshipsQuery = useDORelationships(values.groupId, {
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const handleChangeNoteTask = (val, key) => {
    setValues((state) => ({...state, [key]: val}));
  };

  const FormsComponent = isNote ? NoteForms : TaskForms;

  return (
    <ActionForm>
      <FlexGrid
        {...props}
        cols={2}
        margin={[0, '2rem']}
        // border={[0, 1]}
        sx={{
          padding: '1rem',
          height: '100%',
          ...props.sx,
        }}>
        <Box colWidth={0.7}>
          <BasicInfoForm
            labelPosition="normal"
            actionLabel="Contact Action Name"
          />
        </Box>

        <BodySection
          title="Settings"
          bodyStyle={{
            padding: '1rem',
          }}>
          <FlexGrid cols={2} margin={['1rem', '1.5rem']}>
            <FormField
              name="actionType"
              loading={doTypesQuery.initLoading}
              labelPosition="normal"
              type="Select"
              options={actionTypes}
              label="Action Type"
              size="small"
              variant="outlined"
            />

            {!!values.actionType && !isNote && (
              <FormField
                name="assignType"
                loading={doTypesQuery.initLoading}
                labelPosition="normal"
                type="Select"
                options={assignTypes}
                label="Task Relationship"
                size="small"
                variant="outlined"
              />
            )}

            {!!values.actionType &&
              !isNote &&
              values.assignType === 'Relationship' && (
                <FormField
                  name="doTitle"
                  labelPosition="normal"
                  loading={doTypesQuery.initLoading}
                  type="Select"
                  options={doTypesQuery.result}
                  onChange={(val, item) => setFieldValue('groupId', item.id)}
                  labelKey="title"
                  valueKey="title"
                  label="Data Object Type"
                  fullWidth
                  size="small"
                  variant="outlined"
                />
              )}

            {!!values.actionType && !isNote && (
              <>
                {values.assignType === 'Relationship' ? (
                  values.groupId !== 'co_customfields' && (
                    <FormField
                      name="assignTypeValue"
                      labelPosition="normal"
                      loading={doRelationshipsQuery.initLoading}
                      type="Select"
                      options={doRelationshipsQuery.result}
                      labelKey="role"
                      valueKey="id"
                      label="Contact Relationship"
                      size="small"
                      variant="outlined"
                    />
                  )
                ) : (
                  <SelectUserField
                    name="assignTypeValue"
                    labelPosition="normal"
                    fieldValueKey="id"
                    label="Assign To (Salesferry User)"
                    variant="outlined"
                    size="small"
                  />
                )}
              </>
            )}
          </FlexGrid>

          {!!values.actionType && (
            <>
              <Divider
                style={{
                  margin: '0 0 1rem',
                  borderBottom: '1px dashed #eee',
                }}
              />

              <Typography
                sx={{
                  marginBottom: '8px',
                  color: 'primary.main',
                  fontWeight: 'bold',
                }}>
                {values.actionType}
              </Typography>

              <FormsComponent
                initValues={initValues}
                hideSelectUser
                hideFooter
                size="small"
                style={{
                  padding: '0.5rem 0 0',
                }}
                hideTags={!isNote}
                onChange={handleChangeNoteTask}
              />
            </>
          )}
        </BodySection>
      </FlexGrid>
    </ActionForm>
  );
};

Form = withForm(Form, {
  mapPropsToValues: (props) => {
    return {
      ...props.initialValues,
      groupId: props.initialValues?.groupId || 'co_customfields',
      doTitle: props.initialValues?.doTitle || 'Contact Object',
      tags: props.initialValues?.tags || [],
    };
  },
});

export default Form;
