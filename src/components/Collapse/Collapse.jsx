import React from 'react';
import MUICollapse from '@mui/material/Collapse';

const Collapse = (props) => <MUICollapse {...props} />;

export default Collapse;
