import React from 'react';
import Box from '@mui/material/Box';
import FlexGrid from '@macanta/components/Base/FlexGrid';
import BasicInfoForm from '@macanta/modules/AdminSettings/Automation/TriggerActions/SubForms/BasicInfoForm';
import withForm from '@macanta/components/Form/withForm';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import BodySection from '@macanta/modules/AdminSettings/UserManagement/BodySection';
import FormField from '@macanta/containers/FormField';
import CriteriaFiltersForm from '@macanta/modules/AdminSettings/Automation/CriteriaFiltersForm';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import ActionForm from '@macanta/modules/AdminSettings/Automation/TriggerActions/SubForms/ActionForm';
import withPropHandler from '@macanta/hoc/withPropHandler';
import {dispatchResize} from '@macanta/utils/react';

let Form = (props) => {
  const {values, setValues, setFieldValue} = props;

  const doTypesQuery = useDOTypes({
    removeEmail: true,
    includeContactObject: false,
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const handleChangeFieldValue = (val) => {
    setValues((state) => ({...state, ...val}));
  };

  const handleChangeCustomValue = (val) => {
    setValues((state) => ({...state, ...val}));
  };

  return (
    <ActionForm>
      <FlexGrid
        {...props}
        cols={2}
        margin={[0, '2rem']}
        // border={[0, 1]}
        sx={{
          padding: '1rem',
          height: '100%',
          ...props.sx,
        }}>
        <Box colWidth={0.7}>
          <BasicInfoForm
            labelPosition="normal"
            actionLabel="Webhook Action Name"
            renderOtherForms={() => (
              <FormField
                name="doTitle"
                labelPosition="normal"
                loading={doTypesQuery.initLoading}
                type="Select"
                options={doTypesQuery.result}
                onChange={(val, item) => setFieldValue('groupId', item.id)}
                labelKey="title"
                valueKey="title"
                label="Data Object Type"
                fullWidth
                size="small"
                variant="outlined"
              />
            )}
          />
        </Box>

        <Box
          height="100%"
          sx={{
            display: 'flex',
            flexDirection: 'column',
          }}>
          <BodySection
            title="Settings"
            bodyStyle={{
              padding: '1rem',
            }}>
            <FlexGrid cols={2} margin={['1rem', '1.5rem']}>
              <FormField
                name="postURL"
                type="URL"
                labelPosition="normal"
                label="HTTP POST URL"
                fullWidth
                size="small"
                variant="outlined"
              />
            </FlexGrid>
          </BodySection>

          <CriteriaFiltersForm
            style={{
              margin: '1.5rem 1rem 0',
            }}
            filters={{
              doConditions: values.doConditions,
              contactConditions: values.contactConditions,
            }}
            groupId={values.groupId}
            onChange={handleChangeFieldValue}
            DOConditionProps={{
              title: 'Field Values',
              noLogic: true,
              placeholder: 'Leave blank to use current value',
            }}
          />

          <CriteriaFiltersForm
            style={{
              margin: '1.5rem 1rem 0',
            }}
            hideContactConditions
            filters={{
              customKeyValues: values.customKeyValues,
            }}
            groupId={values.groupId}
            onChange={handleChangeCustomValue}
            DOConditionProps={{
              name: 'customKeyValues',
              title: 'Custom Values',
              noSelection: true,
              noLogic: true,
              placeholder: 'Enter Value',
            }}
          />
        </Box>
      </FlexGrid>
    </ActionForm>
  );
};

Form = withPropHandler(Form, {
  propHandlers: {
    doConditions: dispatchResize,
    contactConditions: dispatchResize,
    customKeyValues: dispatchResize,
  },
  parentPath: 'values',
  conditionCallback: (currValue, prevValue) =>
    currValue?.length !== prevValue?.length,
});

Form = withForm(Form);

export default Form;
