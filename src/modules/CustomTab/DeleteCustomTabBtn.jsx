import React, {useState} from 'react';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import Tooltip from '@macanta/components/Tooltip';
import ConfirmationDialog from '@macanta/containers/ConfirmationDialog';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import {DELETE_CUSTOM_TAB} from '@macanta/graphql/admin';
import {useMutation} from '@apollo/client';
import * as DataObjectsSearchTableStyled from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/styles';

const DeleteCustomTabBtn = ({id, name}) => {
  const {displayMessage} = useProgressAlert();

  const [showDeleteDialog, setShowDeleteDialog] = useState(false);

  const [callDeleteCustomTabMutation, deleteCustomTabMutation] = useMutation(
    DELETE_CUSTOM_TAB,
    {
      update(cache) {
        const normalizedId = cache.identify({
          id,
          __typename: 'CustomTabItem',
        });
        cache.evict({id: normalizedId});
        cache.gc();
      },
      onCompleted(data) {
        if (data.deleteCustomTab?.success) {
          displayMessage('Deleted custom tab successfully');

          handleClose();
        }
      },
      onError() {},
    },
  );

  const handleClose = () => {
    setShowDeleteDialog(false);
  };

  const handleShowDialog = () => {
    setShowDeleteDialog(true);
  };

  const handleDelete = () => {
    callDeleteCustomTabMutation({
      variables: {
        deleteCustomTabInput: {
          id,
        },
        __mutationkey: 'deleteCustomTabInput',
        removeAppInfo: true,
      },
    });
  };

  return (
    <>
      <Tooltip title="Delete">
        <DataObjectsSearchTableStyled.ActionButton
          style={{
            color: '#aaa',
          }}
          show={showDeleteDialog}
          onClick={handleShowDialog}>
          <DeleteForeverIcon />
        </DataObjectsSearchTableStyled.ActionButton>
      </Tooltip>
      <ConfirmationDialog
        open={!!showDeleteDialog}
        onClose={handleClose}
        title="Are you sure?"
        description={[
          `You are about to delete "${name}" from Custom Tabs.`,
          'This cannot be undone. Do you want to proceed?',
        ]}
        actions={[
          {
            label: 'Delete',
            disabledDelay: 3,
            onClick: handleDelete,
            startIcon: <DeleteForeverIcon />,
          },
        ]}
      />
      <LoadingIndicator modal loading={deleteCustomTabMutation.loading} />
    </>
  );
};

export default DeleteCustomTabBtn;
