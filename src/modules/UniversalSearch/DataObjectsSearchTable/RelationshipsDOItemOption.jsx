import React, {useState, useEffect, useMemo} from 'react';
import useQuery from '@macanta/hooks/apollo/useQuery';
import Modal from '@macanta/components/Modal';
import Button from '@macanta/components/Button';
import TurnedInIcon from '@mui/icons-material/TurnedIn';
import {GET_DATA_OBJECT} from '@macanta/graphql/dataObjects';
import Relationships from '@macanta/modules/DataObjectItems/DOItemForms/Relationships';
import {transformConnectedContacts} from '@macanta/selectors/dataObject.selector';
import useDOHasConnectedUser from '@macanta/hooks/useDOHasConnectedUser';
import useOtherUserDOPermission from '@macanta/hooks/useOtherUserDOPermission';
import * as FormStyled from '@macanta/modules/NoteTaskForms/styles';

const RelationshipsDOItemOption = ({
  itemId,
  modal,
  showModal: showModalProp = false,
  relationships,
  onClose,
  onSave,
  loading: relationshipsLoading,
}) => {
  const [changes, setChanges] = useState([]);
  const [showModal, setShowModal] = useState(showModalProp);

  const {loading, data} = useQuery(GET_DATA_OBJECT, {
    variables: {
      itemId,
    },
  });

  const connectedContacts = data?.getDataObject?.connectedContacts;
  const transformedConnectedContacts = useMemo(() => {
    return transformConnectedContacts(connectedContacts, relationships);
  }, [connectedContacts]);

  const {hasConnectedUser, hasConnectedRelationships} = useDOHasConnectedUser(
    connectedContacts,
    true,
  );

  const {
    hasAccess: hasOtherUserDOPermissionRelationship,
  } = useOtherUserDOPermission({
    validate: 'relationship',
    metaData: {
      hasConnectedUser,
      hasConnectedRelationships,
    },
  });

  const handleCloseDOHistory = () => {
    setShowModal(false);

    onClose && onClose();
  };

  const handleValueChange = (formData, options = {}) => {
    const rel = changes.find(
      (r) => r.connectedContactId === formData.connectedContactId,
    );
    let updatedRelationships = [];

    if (rel) {
      if (
        options.type === 'delete' &&
        !transformedConnectedContacts?.some((contact) => {
          return contact.contactId === formData.connectedContactId;
        })
      ) {
        updatedRelationships = changes.filter(
          (r) => r.connectedContactId !== formData.connectedContactId,
        );
      } else {
        updatedRelationships = changes.map((r) => {
          if (r.connectedContactId === formData.connectedContactId) {
            return {
              ...r,
              connectedContactId: formData.connectedContactId,
              relationships: formData.relationships,
              ...(options.type === 'delete' && {
                deleted: true,
              }),
            };
          }

          return r;
        });
      }
    } else {
      const {firstName, lastName, email} = formData.contact;

      updatedRelationships = changes.concat({
        firstName,
        lastName,
        email,
        connectedContactId: formData.connectedContactId,
        relationships: formData.relationships,
        ...(options.type === 'delete' && {
          deleted: true,
        }),
      });
    }

    setChanges(updatedRelationships);
  };

  const handleSubmit = () => {
    const hasRequiredRelationships = connectedContacts.some((contact) => {
      if (
        !changes.some((rel) => rel.connectedContactId === contact.contactId) &&
        !contact.relationships.length
      ) {
        return true;
      }

      return false;
    });

    const errorHasRequiredRelationships =
      hasRequiredRelationships ||
      changes.some((rel) => !rel.deleted && !rel.relationships.length);

    if (errorHasRequiredRelationships) {
      let errorMessage = '';

      errorMessage += `\nRelationships (Cannot have empty relationship)\n`;

      alert(errorMessage);

      return;
    }

    onSave({relationships: changes});
  };

  useEffect(() => {
    setShowModal(showModalProp);
  }, [showModalProp]);

  let doRelationships = (
    <Relationships
      isEdit
      style={{
        margin: 0,
      }}
      selectedValueRelationships={changes}
      connectedContacts={transformedConnectedContacts}
      relationships={relationships}
      onChange={handleValueChange}
      loading={loading || relationshipsLoading}
      hasConnectedUser={hasConnectedUser}
      hasOtherUserDOPermission={hasOtherUserDOPermissionRelationship}
      footerStyle={{
        flexDirection: 'column',
        alignItems: 'flex-start',
        boxShadow: 'none',
      }}
      renderFooterComp={(Comp) => {
        return (
          <>
            {Comp}
            <FormStyled.Footer
              style={{
                padding: '1rem',
                margin: 0,
                borderTop: '1px solid #eee',
                width: '100%',
                backgroundColor: '#f5f6fa',
              }}>
              <Button
                disabled={!changes.length}
                variant="contained"
                startIcon={<TurnedInIcon />}
                onClick={handleSubmit}
                size="medium">
                Save Changes
              </Button>
            </FormStyled.Footer>
          </>
        );
      }}
    />
  );

  if (modal) {
    doRelationships = (
      <Modal
        headerTitle={itemId}
        open={showModal}
        onClose={handleCloseDOHistory}
        contentWidth={600}
        contentHeight={'90%'}>
        {doRelationships}
      </Modal>
    );
  }

  return doRelationships;
};

export default RelationshipsDOItemOption;
