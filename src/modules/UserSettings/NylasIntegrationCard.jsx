import React, {useState} from 'react';
import SyncIcon from '@mui/icons-material/Sync';
import Button from '@macanta/components/Button';
import {nylasPostponedVar} from '@macanta/graphql/cache/vars';
import NylasModal from '@macanta/modules/Nylas/NylasModal';
import useNylasInfo from '@macanta/hooks/admin/useNylasInfo';
import SettingCard from './SettingCard';
import SettingInfo from './SettingInfo';
import * as Storage from '@macanta/utils/storage';

const NylasIntegrationCard = () => {
  const userDetails = Storage.getItem('userDetails');

  const [showModal, setShowModal] = useState(false);

  const {
    loading: loadingNylas,
    allowEmailSync,
    isValidToken,
    status,
  } = useNylasInfo();

  const handleSetupEmailSync = () => {
    setShowModal(true);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  const handlePostpone = () => {
    nylasPostponedVar(true);
    handleCloseModal();
  };

  return (
    <>
      <SettingCard
        loading={loadingNylas}
        title="2-Way Email Sync"
        BodyComp={
          <>
            <SettingInfo label="Email" value={userDetails.email} />
            <SettingInfo label="Status" value={status} />
          </>
        }
        FooterComp={
          allowEmailSync &&
          !isValidToken && (
            <Button
              style={{
                width: '75%',
              }}
              variant="contained"
              onClick={handleSetupEmailSync}
              startIcon={<SyncIcon />}
              size="medium">
              Set Up Email Sync
            </Button>
          )
        }
      />
      {showModal && (
        <NylasModal
          showModal
          onClose={handleCloseModal}
          onPostpone={handlePostpone}
        />
      )}
    </>
  );
};

export default NylasIntegrationCard;
