import React, {useState} from 'react';
import PersonAddIcon from '@mui/icons-material/PersonAdd';
import Modal from '@macanta/components/Modal';
import ContactForms from '@macanta/modules/ContactForms';
import VisibilityIcon from '@mui/icons-material/Visibility';
import Tooltip from '@macanta/components/Tooltip';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import * as HeaderStyled from '@macanta/containers/Header/styles';
import * as DataObjectsSearchTableStyled from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/styles';

const ContactFormsIconBtn = ({renderButton, mobile, item, onSaveContact}) => {
  const isEdit = !!item;

  const [showModal, setShowModal] = useState(false);
  const {displayMessage} = useProgressAlert();

  const handleAddContact = () => {
    setShowModal(true);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  const onSuccess = (contact) => {
    displayMessage('Saved successfully!');
    onSaveContact && onSaveContact(contact);
    setShowModal(false);
  };

  const root = isEdit ? (
    <Tooltip title="View Contact Details">
      <DataObjectsSearchTableStyled.ActionButton
        id={`edit-contact-button`}
        show
        onClick={handleAddContact}
        size="small">
        <VisibilityIcon />
      </DataObjectsSearchTableStyled.ActionButton>
    </Tooltip>
  ) : (
    <>
      {mobile ? (
        <HeaderStyled.NavMobileMenuItem>
          <HeaderStyled.NavIconButton
            id={`add-contact-button`}
            mobile
            onClick={handleAddContact}>
            <PersonAddIcon />
          </HeaderStyled.NavIconButton>
          <p>Add Contact</p>
        </HeaderStyled.NavMobileMenuItem>
      ) : (
        <Tooltip title="Add Contact">
          <HeaderStyled.NavIconButton
            id={`add-contact-button`}
            onClick={handleAddContact}>
            <PersonAddIcon />
          </HeaderStyled.NavIconButton>
        </Tooltip>
      )}
    </>
  );

  return (
    <>
      {renderButton ? renderButton(handleAddContact) : root}
      <Modal
        headerTitle={
          !isEdit ? 'Add Contact' : `View Contact Details (${item.id})`
        }
        open={showModal}
        onClose={handleCloseModal}
        contentWidth={1600}
        contentHeight={'96%'}>
        <ContactForms contact={item} onSuccess={onSuccess} />
      </Modal>
    </>
  );
};

export default ContactFormsIconBtn;
