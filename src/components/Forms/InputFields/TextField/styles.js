import {experimentalStyled as styled} from '@mui/material/styles';

import isEmpty from 'lodash/isEmpty';
import MUITextField from '@mui/material/TextField';
import * as applicationStyles from '@macanta/themes/applicationStyles';

export const TextField = styled(MUITextField)`
  min-width: 100px;

  .MuiFormHelperText-root.Mui-error {
    margin-left: 0;
  }

  .MuiInputBase-inputHiddenLabel > .MuiTypography-root {
    ${applicationStyles.oneLineText}
  }

  .MuiInputBase-input {
    box-sizing: border-box;

    &::placeholder {
      color: #555;
    }

    .select-placeholder {
      color: #bbb;
    }
  }

  .Mui-focused .MuiOutlinedInput-notchedOutline {
    border-width: 1px;
  }

  ${({multiline}) =>
    multiline &&
    `
    .MuiInputBase-root {
      padding: 0;

      .MuiInputBase-input {
        height: auto;
        padding: 8.5px 14px;
      }
    }
  `}

  ${({select, loading}) =>
    select &&
    loading &&
    `
    .MuiSvgIcon-root {
      display: none;
    }
  `}

  ${({disabled}) =>
    disabled &&
    `
    .Mui-disabled:not(.MuiInputLabel-root) {
      color: #aaa!important;
      background-color: #eee!important;
      -webkit-text-fill-color: #aaa!important;
    }
  `}

  ${({variant}) =>
    variant === 'standard' &&
    `
      .MuiInputBase-root:before {
        border-bottom: none !important;
      }

      .MuiInputBase-input {
        width: 100%;
        border-bottom: 1px solid #ddd;
        box-sizing: border-box;
      }
  `}

  ${({noBorder}) =>
    noBorder &&
    `
      .MuiInputBase-root {
        &:before {
          border-bottom: none !important;
        }

        &:after {
          border-bottom: none !important;
        }
      }

      .MuiInputBase-input {
        border-bottom: none !important;
      }
  `}

  ${({size}) =>
    (!size || size === 'medium') &&
    `
      .MuiInputBase-input {
        height: 56px;
        font-size: 1rem;

        .MuiTypography-root {
          font-size: 1rem;
        }
      }
  `}

  ${({size, value}) =>
    size === 'small' &&
    `
    ${
      isEmpty(value)
        ? `
      .MuiInputLabel-root:not(.Mui-focused) {
        transform: translate(14px, 8px) scale(1);
      }
    `
        : ''
    }

    .MuiInputBase-input {
      height: 38px;
      font-size: ${applicationStyles.DEFAULT_FORM_FONT_SIZE};
  
      .MuiTypography-root {
        font-size: ${applicationStyles.DEFAULT_FORM_FONT_SIZE};
      }
    }
  `}

  ${({size, value}) =>
    size === 'xsmall' &&
    `
      ${
        isEmpty(value)
          ? `
        .MuiInputLabel-root:not(.Mui-focused) {
          transform: translate(14px, 7px) scale(1);
        }
      `
          : ''
      }

      .MuiInputBase-input {
        height: 34px;
        font-size: ${applicationStyles.DEFAULT_FORM_FONT_SIZE};
  
        .MuiTypography-root {
          font-size: ${applicationStyles.DEFAULT_FORM_FONT_SIZE};
        }

        display: flex;
        align-items: center;
      }
  `}
`;
