import useDimensions from '@macanta/hooks/base/useDimensions';
import useValue from '@macanta/hooks/base/useValue';

const useItemsSlice = (items, minWidth = 100) => {
  const {dimensions, setRef} = useDimensions();

  const [slicedItems] = useValue(() => {
    if (dimensions.width) {
      const sliceFactor = Math.floor(dimensions.width / minWidth);

      return items.slice(0, sliceFactor);
    }

    return items;
  }, [items, minWidth, dimensions.width]);

  return {items: slicedItems, setRef};
};

export default useItemsSlice;
