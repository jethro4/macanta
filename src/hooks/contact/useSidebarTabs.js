import {useMemo} from 'react';
import useQuery from '@macanta/hooks/apollo/useQuery';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import {GET_APP_SETTINGS} from '@macanta/graphql/app';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {sortArrayByKeyValuesIncludeAll} from '@macanta/utils/array';
import useCustomTabs from '@macanta/hooks/admin/useCustomTabs';

const useSidebarTabs = (optionsArg) => {
  const {hideNotes, hideCommunicationHistory, ...options} = Object.assign(
    {},
    optionsArg,
  );

  const doTypesQuery = useDOTypes(options);
  const customTabsQuery = useCustomTabs(options);
  const appSettingsQuery = useQuery(GET_APP_SETTINGS, {
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const listDOTypes = doTypesQuery.data?.listDataObjectTypes;
  const customTabs = customTabsQuery.data;
  const macantaTabOrder =
    appSettingsQuery.data?.getAppSettings?.macantaTabOrder;
  const loading = Boolean(
    doTypesQuery.loading || customTabsQuery.loading || appSettingsQuery.loading,
  );
  const initLoading = Boolean(
    doTypesQuery.initLoading ||
      customTabsQuery.initLoading ||
      appSettingsQuery.initLoading,
  );

  const tabs = useMemo(() => {
    if (listDOTypes && customTabs) {
      let allTabs = [];

      if (!hideNotes) {
        allTabs = allTabs.concat({
          id: 'notes',
          title: 'Notes',
          type: 'notes',
        });
      }

      if (listDOTypes.length) {
        allTabs = allTabs.concat(
          listDOTypes.map((doType) => ({
            ...doType,
            type: 'data-object',
          })),
        );
      }

      if (customTabs.length) {
        allTabs = allTabs.concat(
          customTabs.map((customTab) => ({
            ...customTab,
            type: 'custom',
          })),
        );
      }

      if (!hideCommunicationHistory) {
        allTabs.push({
          id: 'communication',
          title: 'Communication History',
          type: 'communication',
        });
      }

      const sortedTabs = !macantaTabOrder
        ? allTabs
        : sortArrayByKeyValuesIncludeAll(allTabs, 'title', macantaTabOrder);

      return sortedTabs;
    }

    return [];
  }, [
    hideNotes,
    hideCommunicationHistory,
    listDOTypes,
    customTabs,
    macantaTabOrder,
  ]);

  return {data: tabs, loading, initLoading};
};

export default useSidebarTabs;
