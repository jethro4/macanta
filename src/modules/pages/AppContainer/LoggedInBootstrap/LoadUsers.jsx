import useUsers from '@macanta/hooks/admin/useUsers';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';

const LoadUsers = () => {
  useUsers({
    fetchPolicy: FETCH_POLICIES.CACHE_AND_NETWORK,
  });

  return null;
};

export default LoadUsers;
