export const calculateGridInfo = ({index, cols, total}) => {
  const rows = Math.ceil(total / cols);

  const positionX = index % cols;
  const positionY = Math.floor(index / cols);

  const isFirstItemInColumn = positionX === 0;
  const isFirstItemInRow = positionY === 0;

  const isLastItemInColumn = positionX === cols - 1;
  const isLastItemInRow = positionY === rows - 1;

  return {
    positionX,
    positionY,
    isFirstItemInColumn,
    isFirstItemInRow,
    isLastItemInColumn,
    isLastItemInRow,
  };
};
