import {nextFactoryId} from './factory';

const typename = 'CustomTabItem';

export default (overwrites) => {
  const {id, currentIndex} = nextFactoryId(typename);

  return {
    __typename: typename,
    id,
    title: `Custom Tab ${currentIndex}`,
    ...overwrites,
  };
};
