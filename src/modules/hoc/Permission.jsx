import useAccessPermissions from '@macanta/hooks/admin/useAccessPermissions';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';

const Permission = ({children, keys, condition = 'allTrue'}) => {
  const accessPermissionsQuery = useAccessPermissions({
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const isAdmin = accessPermissionsQuery.data?.isAdmin;
  const permissionMeta = accessPermissionsQuery.data?.permissionMeta;
  let hasAccess = false;

  if (isAdmin) {
    hasAccess = true;
  } else if (permissionMeta && keys?.length) {
    if (condition === 'allTrue') {
      hasAccess = keys.reduce((acc, key) => {
        return acc && permissionMeta[key] === true;
      }, true);
    } else if (condition === 'allFalse') {
      hasAccess = keys.reduce((acc, key) => {
        return acc && permissionMeta[key] === false;
      }, true);

      hasAccess = !hasAccess;
    }
  }

  return hasAccess ? children : null;
};

export default Permission;
