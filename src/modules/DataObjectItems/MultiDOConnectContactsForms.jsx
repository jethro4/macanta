import React, {useState} from 'react';
import PersonAddIcon from '@mui/icons-material/PersonAdd';
import ContactQuery from '@macanta/modules/hoc/ContactQuery';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import useContactId from '@macanta/hooks/useContactId';
import useDOItemMutation from '@macanta/modules/DataObjectItems/DOItemForms/useDOItemMutation';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import Form from '@macanta/components/Form';
import MultiDOConnectContactsTableSection from './MultiDOConnectContactsTableSection';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import * as ContactFormStyled from '@macanta/modules/ContactForms/styles';
import * as ContactAnotherContactFormStyled from '@macanta/modules/DataObjectItems/DOItemForms/Relationships/ConnectAnotherContactForms/styles';

const MultiDOConnectContactsForms = ({
  doTitle,
  groupId,
  onSuccess,
  ...props
}) => {
  const contactId = useContactId();

  const [saving, setSaving] = useState(false);

  const {save} = useDOItemMutation(null, groupId, null);
  const {displayMessage} = useProgressAlert();

  const initValues = {
    selectedContactRels: {},
  };

  const handleSave = async (selectedContactRels = {}) => {
    await Promise.all(
      Object.entries(selectedContactRels).map(
        async ([itemId, relationships]) => {
          await save(
            {
              relationships: [
                {
                  connectedContactId: contactId,
                  relationships,
                },
              ],
            },
            {
              itemId,
            },
          );
        },
      ),
    );
  };

  const handleFormSubmit = async (values) => {
    setSaving(true);

    await handleSave(values.selectedContactRels);

    setSaving(false);

    displayMessage('Connected successfully!');

    onSuccess();
  };

  return (
    <ContactAnotherContactFormStyled.Root
      style={{
        paddingTop: 0,
      }}
      {...props}>
      <Form
        initialValues={initValues}
        // validationSchema={connectContactValidationSchema}
        onSubmit={handleFormSubmit}>
        {({values, setFieldValue, handleSubmit, dirty}) => (
          <>
            <MultiDOConnectContactsTableSection
              doTitle={doTitle}
              groupId={groupId}
              onChangeRelationships={(itemId, relationships) => {
                setFieldValue('selectedContactRels', {
                  ...values.selectedContactRels,
                  [itemId]: relationships,
                });
              }}
            />
            <ContactFormStyled.FloatingButton
              style={{
                zIndex: 11,
              }}
              disabled={!dirty}
              variant="contained"
              startIcon={<PersonAddIcon />}
              onClick={handleSubmit}
              size="medium">
              <ContactQuery
                id={contactId}
                options={{fetchPolicy: FETCH_POLICIES.CACHE_FIRST}}>
                {({data: updatedContact}) => {
                  const name = updatedContact
                    ? `${updatedContact.firstName} ${updatedContact.lastName}`.trim()
                    : '';

                  return `Connect${name ? ` ${name}` : ''}`;
                }}
              </ContactQuery>
            </ContactFormStyled.FloatingButton>
          </>
        )}
      </Form>
      <LoadingIndicator modal loading={saving} />
    </ContactAnotherContactFormStyled.Root>
  );
};

export default MultiDOConnectContactsForms;
