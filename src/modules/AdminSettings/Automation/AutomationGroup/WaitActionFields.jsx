import React from 'react';
import FlexGrid from '@macanta/components/Base/FlexGrid';
import FormField from '@macanta/containers/FormField';
import {AUTOMATION_SET_TIME_UNITS} from '@macanta/constants/automation';

const WaitActionFields = ({item, onChange, ...props}) => {
  const handleEdit = ({key, value}) => {
    const updatedItem = {
      ...item,
      [key]: value,
    };

    onChange?.(updatedItem);
  };

  return (
    <FlexGrid
      {...props}
      cols={2}
      margin={['1.5rem', '1.5rem']}
      style={{
        alignItems: 'center',
        ...props.style,
      }}>
      <FormField
        labelPosition="normal"
        value={item.waitFor}
        onChange={(val) => handleEdit({item, key: 'waitFor', value: val})}
        type="Number"
        label="Wait For"
        size="small"
        variant="outlined"
      />

      <FormField
        labelPosition="normal"
        value={item.unit}
        onChange={(val) => handleEdit({item, key: 'unit', value: val})}
        type="Select"
        options={AUTOMATION_SET_TIME_UNITS}
        label="Unit"
        size="small"
        variant="outlined"
      />

      <FormField
        labelPosition="normal"
        value={item.time}
        onChange={(val) => handleEdit({item, key: 'time', value: val})}
        type="Time"
        label="Then Run At"
        size="small"
        variant="outlined"
      />

      <FormField
        labelPosition="normal"
        type="Switch"
        label="Re-Validate"
        value={item.revalidate}
        onChange={(value) => handleEdit({item, key: 'revalidate', value})}
      />
    </FlexGrid>
  );
};

export default WaitActionFields;
