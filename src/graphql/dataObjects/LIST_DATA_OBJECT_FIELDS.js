import {gql} from '@apollo/client';

export default gql`
  query listDataObjectFields(
    $appName: String!
    $apiKey: String!
    $groupId: ID!
    $userEmail: String
  ) {
    listDataObjectFields(
      appName: $appName
      apiKey: $apiKey
      groupId: $groupId
      userEmail: $userEmail
    ) {
      id
      name
      type
      helpText
      placeholder
      default
      required
      subGroup
      sectionTag
      showInTable
      showOrder
      choices
      contactSpecificField
      permission
      headingText
      addDivider
      dataReference
      fieldApplicableTo
      fileUpload {
        multiple
        mimeTypes
      }
    }
  }
`;
