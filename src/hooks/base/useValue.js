import {useState} from 'react';
import isFunction from 'lodash/isFunction';
import useDeepCompareEffect from './useDeepCompareEffect';

const useValue = (valueProp, dependencyKeys) => {
  const [value, setValue] = useState(valueProp);
  const valueIsFunc = isFunction(valueProp);
  const dependencies =
    valueIsFunc || dependencyKeys ? [...dependencyKeys] : [valueProp];

  useDeepCompareEffect(() => {
    setValue((prevValue) => (valueIsFunc ? valueProp(prevValue) : valueProp));
  }, dependencies);

  return [value, setValue];
};

export default useValue;
