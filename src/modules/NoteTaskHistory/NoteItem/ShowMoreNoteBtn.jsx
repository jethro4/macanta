import React, {useState, useEffect, useRef} from 'react';
import Popover from '@macanta/components/Popover';
import Markdown from '@macanta/components/Markdown';
import useVisible from '@macanta/hooks/useVisible';
import VisibilityIcon from '@mui/icons-material/Visibility';
import * as Styled from './styles';

const ShowMoreNoteBtn = ({note, style, previewStyle, noteFontSize = 15}) => {
  const noteLineHeight = 1.5 * noteFontSize;

  const noteRef = useRef();
  const visible = useVisible();
  const [showMore, setShowMore] = useState(false);
  const [anchorEl, setAnchorEl] = useState(false);

  const openShowMore = Boolean(anchorEl);

  const handleShowMore = (event) => {
    handleStopMouseEvent(event);

    setAnchorEl(event.currentTarget);
  };

  const handleCloseNotifications = () => {
    setAnchorEl(null);
  };

  const handleStopMouseEvent = (event) => {
    event.stopPropagation();
  };

  useEffect(() => {
    if (noteRef.current) {
      const {height} = noteRef.current.getBoundingClientRect();
      const roundedOffHeight = parseFloat(height.toFixed(2));
      const roundedOffLineHeight = parseFloat(noteLineHeight.toFixed(2));

      if (roundedOffHeight / roundedOffLineHeight > 2) {
        setShowMore(true);
      }
    }
  }, [note, visible]);

  return (
    <>
      <Styled.NoteContainer
        style={{
          maxHeight: noteLineHeight * 2,
        }}>
        <Styled.Note
          ref={noteRef}
          disableLineClamp={!showMore}
          style={{
            ...previewStyle,
            fontSize: noteFontSize,
            lineHeight: `${noteLineHeight}px`,
          }}
          dangerouslySetInnerHTML={{__html: note}}
        />
      </Styled.NoteContainer>
      {showMore && (
        <>
          <Styled.ShowMoreButton
            style={style}
            onMouseUp={handleStopMouseEvent}
            onMouseDown={handleStopMouseEvent}
            onClick={handleShowMore}
            size="small"
            startIcon={<VisibilityIcon />}>
            Show More
          </Styled.ShowMoreButton>
          <Popover
            open={openShowMore}
            anchorEl={anchorEl}
            onClose={handleCloseNotifications}
            onMouseUp={handleStopMouseEvent}
            onMouseDown={handleStopMouseEvent}
            onClick={handleStopMouseEvent}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'right',
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'right',
            }}>
            <div
              style={{
                maxWidth: 500,
                minWidth: 250,
                maxHeight: 500,
                display: 'flex',
                flexDirection: 'column',
                overflowY: 'auto',
                whiteSpace: 'pre-line',
                padding: '1rem',
                fontSize: '0.875rem',
              }}>
              <Markdown>{note}</Markdown>
            </div>
          </Popover>
        </>
      )}
    </>
  );
};

ShowMoreNoteBtn.defaultProps = {
  note: '',
};

export default ShowMoreNoteBtn;
