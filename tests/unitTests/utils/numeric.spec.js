import {every} from '@macanta/utils/array';
import {isNumeric, roundOff, average, zeroPad} from '@macanta/utils/numeric';

describe('Numberic Utils', () => {
  describe('isNumeric', () => {
    it('number values', () => {
      const wholeNumbers = [0, 1, 2];
      const floatNumbers = [-2.2, -1.1, 0.5, 1, 9];
      const withNegativeNumbers = [-2, -1, 0, 1];

      const wholeNumbersResult = every(wholeNumbers, isNumeric);
      const floatNumbersResult = every(floatNumbers, isNumeric);
      const withNegativeNumbersResult = every(withNegativeNumbers, isNumeric);

      expect(wholeNumbersResult).toEqual(true);
      expect(floatNumbersResult).toEqual(true);
      expect(withNegativeNumbersResult).toEqual(true);
    });

    it('string values', () => {
      const wholeNumberString = '1';
      const floatNumberString = '1.1';
      const negativeNumberString = '-1';

      const wholeNumberStringResult = isNumeric(wholeNumberString);
      const floatNumberStringResult = isNumeric(floatNumberString);
      const negativeNumberStringResult = isNumeric(negativeNumberString);

      expect(wholeNumberStringResult).toEqual(true);
      expect(floatNumberStringResult).toEqual(true);
      expect(negativeNumberStringResult).toEqual(true);
    });

    it('negative paths', () => {
      const withString = isNumeric('1px');
      const emptyString = isNumeric('');
      const boolResult = isNumeric(true);
      const objectResult = isNumeric({});
      const undefinedResult = isNumeric();
      const nullResult = isNumeric(null);

      expect(withString).toEqual(false);
      expect(emptyString).toEqual(false);
      expect(boolResult).toEqual(false);
      expect(objectResult).toEqual(false);
      expect(undefinedResult).toEqual(false);
      expect(nullResult).toEqual(false);
    });
  });

  describe('roundOff', () => {
    it('2 decimal places', () => {
      const stringValue = roundOff('33.333333');
      const floatValueDefaultDecimal = roundOff(66.666666);
      const floatValueSpecificDecimal = roundOff(66.666666, 2);

      expect(stringValue).toEqual(33.33);
      expect(floatValueDefaultDecimal).toEqual(66.67);
      expect(floatValueSpecificDecimal).toEqual(66.67);
    });
  });

  describe('average', () => {
    it('whole numbers', () => {
      const sameValues = average(5, 5);
      const differentValues = average(2, 3);

      expect(sameValues).toEqual(5);
      expect(differentValues).toEqual(2.5);
    });

    it('negative paths', () => {
      const singleValue = average(5);
      const noValue = average(0);
      const undefinedResult = average(undefined);
      const nullResult = average(null);

      expect(singleValue).toEqual(5);
      expect(noValue).toEqual(0);
      expect(undefinedResult).toEqual(0);
      expect(nullResult).toEqual(0);
    });
  });

  describe('zeroPad', () => {
    it('single digits', () => {
      const zero = zeroPad(0);
      const two = zeroPad(2);

      expect(zero).toEqual('00');
      expect(two).toEqual('02');
    });

    it('multiple digits', () => {
      const doubleDigits = zeroPad(10);
      const tripleDigits = zeroPad(100);
      const quadDigits = zeroPad(1000);

      expect(doubleDigits).toEqual('10');
      expect(tripleDigits).toEqual('100');
      expect(quadDigits).toEqual('1000');
    });
  });
});
