import React, {useEffect, useMemo} from 'react';
import EditIcon from '@mui/icons-material/Edit';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import Box from '@mui/material/Box';
import Tooltip from '@macanta/components/Tooltip';
import {transformValue} from '@macanta/components/Forms/InputFields/helpers';
import AddConditionBtn from './AddConditionBtn';
import {OPERATORS} from '@macanta/constants/operators';
import {FIELDS_WITH_CHOICES} from '@macanta/constants/form';
import * as Styled from './styles';

const ColumnDividerContainer = ({width}) => (
  <Box
    style={{
      width: width || '1rem',
    }}
  />
);

const CriteriaConditionForms = ({
  loading,
  condition,
  type,
  lastItem,
  required,
  choices,
  onAddQuery,
  onEditQuery,
  onDeleteQuery,
  oneItem,
  noSelection,
  noLogic,
  placeholder,
}) => {
  const getDefaultOperator = (operatorVal, isLabel) => {
    let operators = OPERATORS;

    if (type === 'userConditions') {
      operators = OPERATORS.filter((o) => ['is', 'is not'].includes(o.value));
    }

    const val = operatorVal || operators[0].value;
    const selected = operators.find((o) => o.value === val);

    return (isLabel ? selected?.label : selected?.value) || '';
  };

  const hasNoValue = (operatorVal) => {
    return operatorVal === 'is null' || operatorVal === 'not null';
  };

  const selectedName = condition?.name || condition?.relationship || '';
  const fieldType = choices.find((choice) => choice.value === selectedName)
    ?.type;
  const operator = getDefaultOperator(condition?.operator);

  const selectedChoice =
    !['contactConditions', 'userConditions'].includes(type) &&
    selectedName &&
    choices.find((c) => c.value === selectedName);
  const isMultiSelect = FIELDS_WITH_CHOICES.includes(fieldType);
  const hasChoices =
    FIELDS_WITH_CHOICES.includes(fieldType) &&
    Array.isArray(selectedChoice?.choices);

  const operators = useMemo(() => {
    if (type === 'userConditions') {
      return OPERATORS.filter((o) => ['is', 'is not'].includes(o.value));
    }

    return OPERATORS;
  }, []);

  const value = useMemo(() => {
    if (type === 'userConditions') {
      return condition?.userId || '';
    }

    const conditionValue = condition?.values || condition?.value;
    const finalValue = transformValue(conditionValue, isMultiSelect);

    return finalValue;
  }, [isMultiSelect, condition?.values, condition?.value, condition?.userId]);

  const handleChangeName = (val) => {
    onEditQuery(
      val,
      !['contactConditions', 'userConditions'].includes(type)
        ? 'name'
        : 'relationship',
    );
  };

  const handleChangeOperator = (val) => {
    onEditQuery(val, 'operator');
  };

  const handleChangeValue = (val) => {
    onEditQuery(
      val,
      type === 'userConditions' ? 'userId' : isMultiSelect ? 'values' : 'value',
    );
  };

  const handleChangeLogic = (val) => {
    onEditQuery(val, 'logic');
  };

  const handleNameRenderValue = (val) => {
    const selected = choices?.find(({value: nameVal}) => nameVal === val);

    return selected?.label;
  };

  const handleOperatorRenderValue = (val) => {
    return getDefaultOperator(val, true);
  };

  useEffect(() => {
    if (hasNoValue(operator)) {
      handleChangeValue(isMultiSelect ? [] : '');
    }
  }, [operator]);

  const noValue = hasNoValue(operator);
  const ValueField =
    type === 'userConditions' ? Styled.SelectUserField : Styled.ValueField;

  return (
    <Styled.FormsContainer
      style={{
        ...(lastItem && {
          marginBottom: 0,
        }),
      }}>
      <Styled.NameField
        loading={loading}
        style={{
          flexGrow: 1,
          flexBasis: 0,
          marginBottom: 0,
        }}
        type="Select"
        options={choices}
        value={selectedName}
        // error={errors[field.name]}
        onChange={handleChangeName}
        placeholder="Select Field..."
        fullWidth
        size="small"
        variant="outlined"
        renderValue={handleNameRenderValue}
        {...(noSelection && {
          type: 'Text',
          placeholder: 'Enter Key Name',
        })}
      />
      <Box
        style={{
          flexGrow: 1,
          flexBasis: 0,
          overflowX: 'hidden',
          display: 'flex',
          alignItems: 'center',
          ...(type === 'contactConditions' && {
            opacity: 0,
            pointerEvents: 'none',
          }),
        }}>
        <ColumnDividerContainer />
        {!noLogic && (
          <>
            <Styled.OperatorField
              style={{
                flexGrow: 1,
                flexBasis: 50,
                marginBottom: 0,
              }}
              type="Select"
              options={operators}
              value={operator}
              // error={errors[field.name]}
              onChange={handleChangeOperator}
              placeholder="Operator..."
              fullWidth
              size="small"
              variant="outlined"
              renderValue={handleOperatorRenderValue}
            />
            <ColumnDividerContainer />
          </>
        )}
        <ValueField
          style={{
            flexGrow: 1,
            flexBasis: 100,
            overflowX: 'hidden',
            marginBottom: 0,
          }}
          disabled={noValue}
          value={value}
          // error={errors[field.name]}
          onChange={handleChangeValue}
          placeholder={placeholder || (noValue ? 'N/A' : 'Enter Value')}
          fullWidth
          size="small"
          variant="outlined"
          {...(type === 'userConditions'
            ? {
                fieldValueKey: 'id',
                withLoggedInUser: true,
              }
            : {
                type: fieldType,
              })}
          {...(isMultiSelect && {
            type: 'MultiSelect',
          })}
          {...(hasChoices && {
            options: selectedChoice?.choices,
          })}
        />
      </Box>
      <ColumnDividerContainer />
      {!oneItem && (
        <>
          {noLogic || lastItem ? (
            <AddConditionBtn onAddQuery={onAddQuery} noLogic={noLogic} />
          ) : (
            <AddConditionBtn
              onAddQuery={handleChangeLogic}
              label={condition?.logic === 'and' ? 'AND' : <>OR&nbsp;&nbsp;</>}
              title="Change Logic"
              color="primary"
              variant="outlined"
              isEdit
              startIcon={<EditIcon />}
            />
          )}

          <ColumnDividerContainer width="0.5rem" />
          <Tooltip title="Delete" disableHoverListener={required}>
            <div>
              <Styled.ConditionIconButton
                disabled={required}
                onClick={onDeleteQuery}>
                <DeleteForeverIcon />
              </Styled.ConditionIconButton>
            </div>
          </Tooltip>
        </>
      )}
    </Styled.FormsContainer>
  );
};

CriteriaConditionForms.defaultProps = {
  choices: [],
};

export default CriteriaConditionForms;
