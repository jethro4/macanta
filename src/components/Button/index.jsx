import React from 'react';
import ButtonComp from './Button';
import ButtonLink from './ButtonLink';
import IconButton from './IconButton';
import ModalButton from './ModalButton';
import PopoverButton from './PopoverButton';
import DialogButton from './DialogButton';
import DisabledDelay from './DisabledDelay';

const Button = ({icon, disabledDelay, ...props}) => {
  let buttonComp;

  if (icon) {
    buttonComp = <IconButton {...props} />;
  } else {
    buttonComp = <ButtonComp {...props} />;
  }

  if (disabledDelay) {
    buttonComp = (
      <DisabledDelay delay={disabledDelay} {...props}>
        {buttonComp}
      </DisabledDelay>
    );
  }

  return buttonComp;
};

export default Button;

export {ButtonLink, IconButton, ModalButton, PopoverButton, DialogButton};
