import {experimentalStyled as styled} from '@mui/material/styles';

import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import MUIAccordion from '@mui/material/Accordion';
import MUIAccordionSummary from '@mui/material/AccordionSummary';
import MUIAccordionDetails from '@mui/material/AccordionDetails';

export const Root = styled(Box)`
  height: 100%;
  padding: 8px 0 0px;
`;

export const Panel = styled(MUIAccordion)`
  margin-bottom: 4px;
  box-shadow: none;
  border-bottom-width: none !important;
  border-bottom-color: transparent !important;
`;

export const PanelTitle = styled(Typography)``;

export const PanelHeader = styled(MUIAccordionSummary)`
  border-bottom: 1px solid #eee;
  min-height: auto !important;
  height: 44px;

  .MuiTypography-root {
    letter-spacing: 0.5px;
    color: ${({theme}) => theme.palette.text.gray};
    font-size: 0.875rem;
    font-weight: bold;
  }

  &.Mui-expanded {
    .MuiTypography-root,
    .MuiSvgIcon-root {
    }
  }
`;

export const PanelBody = styled(MUIAccordionDetails)`
  padding: 1.5rem 1rem;
`;
