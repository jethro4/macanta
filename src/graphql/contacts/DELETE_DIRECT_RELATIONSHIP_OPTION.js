import {gql} from '@apollo/client';

export default gql`
  mutation deleteDirectRelationshipOption(
    $deleteDirectRelationshipOptionInput: DeleteDirectRelationshipOptionInput!
  ) {
    deleteDirectRelationshipOption(
      input: $deleteDirectRelationshipOptionInput
    ) {
      success
    }
  }
`;
