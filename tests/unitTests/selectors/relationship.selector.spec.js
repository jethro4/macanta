import {
  getContactRelationships,
  getFilteredRelationshipsByLimit,
  getRelationshipOptions,
} from '@macanta/selectors/relationship.selector';

describe('DO Relationships Data Transformer', () => {
  let connectedContacts;
  let allRelationships;

  const initialConnectedContacts = [
    {
      id: '554753',
      firstName: 'Geover',
      lastName: 'Zamora',
      email: 'geover@gmail.com',
      relationships: [
        {
          id: 're_34cc668d5636f556',
          role: 'Macanta User',
        },
        {
          id: 're_34cc668d5636f556',
          role: 'Organiser',
        },
      ],
    },
    {
      id: '554840',
      firstName: 'Jethro',
      lastName: 'Estrada',
      email: 'jethro@macantacrm.com',
      relationships: [
        {
          id: 're_34cc668d5636f556',
          role: 'Macanta User',
        },
        {
          id: 're_bb6b8310ad9c9301',
          role: 'Passenger',
        },
        {
          id: 're_fc8b2c6db507c5e3',
          role: 'Organiser',
        },
      ],
    },
  ];

  const initialAllRelationships = [
    {
      id: 're_34cc668d5636f556',
      role: 'Macanta User',
      exclusive: 'no',
      limit: 0,
      autoAssignLoggedInUser: false,
      autoAssignContact: true,
    },
    {
      id: 're_295d08cb3ce03081',
      role: 'App Owner',
      exclusive: 'no',
      limit: 0,
      autoAssignLoggedInUser: false,
      autoAssignContact: false,
    },
    {
      id: 're_6eefea0b59e4f03b',
      role: 'Developer Agent',
      exclusive: 'yes',
      limit: 0,
      autoAssignLoggedInUser: true,
      autoAssignContact: false,
    },
    {
      id: 're_bb6b8310ad9c9301',
      role: 'Passenger',
      exclusive: 'no',
      limit: 0,
      autoAssignLoggedInUser: false,
      autoAssignContact: false,
    },
    {
      id: 're_fc8b2c6db507c5e3',
      role: 'Organiser',
      exclusive: 'no',
      limit: 2,
      autoAssignLoggedInUser: false,
      autoAssignContact: false,
    },
  ];

  beforeEach(() => {
    connectedContacts = initialConnectedContacts;
    allRelationships = initialAllRelationships;
  });

  it('happy - getContactRelationships', () => {
    const actualResult = getContactRelationships(connectedContacts);
    const expectedResult = [
      'Macanta User',
      'Organiser',
      'Macanta User',
      'Passenger',
      'Organiser',
    ];

    expect(actualResult).toEqual(expectedResult);
  });

  it('happy - getFilteredRelationshipsByLimit', () => {
    const contactRelationships = getContactRelationships(connectedContacts);
    const actualResult = getFilteredRelationshipsByLimit(
      contactRelationships,
      allRelationships,
    );

    const expectedResult = [
      'Macanta User',
      'App Owner',
      'Developer Agent',
      'Passenger',
    ];

    expect(actualResult).toEqual(expectedResult);
  });

  it('happy - getRelationshipOptions - relationship select options - with selected role that is limited to 1 selection', () => {
    const filteredRelationships = ['Macanta User', 'App Owner', 'Passenger'];
    const selectedValues = [
      'Macanta User',
      'App Owner',
      'Developer Agent', // role that is limited to 1 selection
      'Passenger',
    ];
    const allRelationshipRoles = allRelationships.map((r) => r.role);

    const actualResult = getRelationshipOptions(
      allRelationshipRoles,
      selectedValues,
      filteredRelationships,
    );

    const expectedResult = [
      'Macanta User',
      'App Owner',
      'Developer Agent',
      'Passenger',
    ].map((item) => ({label: item, value: item}));

    expect(actualResult).toEqual(expectedResult);
  });

  it('happy - getRelationshipOptions - relationship select options - with no selected options', () => {
    const filteredRelationships = ['Macanta User', 'App Owner', 'Passenger'];
    const selectedValues = [];
    const allRelationshipRoles = allRelationships.map((r) => r.role);

    const actualResult = getRelationshipOptions(
      allRelationshipRoles,
      selectedValues,
      filteredRelationships,
    );

    const expectedResult = [
      'Macanta User',
      'App Owner',
      'Passenger',
    ].map((item) => ({label: item, value: item}));

    expect(actualResult).toEqual(expectedResult);
  });
});
