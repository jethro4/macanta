import React from 'react';
import {
  DEFAULT_COLUMN_MIN_WIDTH,
  DEFAULT_COLUMN_MAX_WIDTH,
} from '@macanta/themes/applicationStyles';
import TableBodyCell from './TableBodyCell';

const EnhancedTableBodyCell = ({
  column,
  item,
  numOfTextLines,
  disableLineClamp,
  handlers,
}) => {
  const origValue = item[column.id];
  const value = column.transform
    ? column.transform({value: origValue, item})
    : origValue;
  const subtext = item[column.subtext];
  const colMinWidth = column.minWidth || DEFAULT_COLUMN_MIN_WIDTH;
  const colMaxWidth = column.maxWidth || DEFAULT_COLUMN_MAX_WIDTH;

  return (
    <TableBodyCell
      item={item}
      column={column}
      numOfTextLines={numOfTextLines}
      disableLineClamp={disableLineClamp}
      origValue={origValue}
      value={value}
      subtext={subtext}
      colMinWidth={colMinWidth}
      colMaxWidth={colMaxWidth}
      handlers={handlers}
    />
  );
};

export default EnhancedTableBodyCell;
