import React, {useState, useEffect} from 'react';
import {getPersistor} from '@macanta/config/apolloConfig';
import Head from '@macanta/components/Head';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import ErrorBoundary from '@macanta/containers/ErrorBoundary';
import NewVersionAlert from '@macanta/modules/NewVersionAlert';
import AppWrapper from '@macanta/modules/AppWrapper';

const ApolloProviderBody = ({children}) => {
  const [cacheLoaded, setCacheLoaded] = useState(false);

  useEffect(() => {
    async function init() {
      const persistor = getPersistor();

      await persistor.restore();

      setCacheLoaded(true);
    }

    init().catch(console.error);
  }, []);

  return (
    <>
      <Head />
      {cacheLoaded ? (
        <ErrorBoundary allowReactExternalLogging allowWindowExternalLogging>
          <NewVersionAlert />
          <AppWrapper>{children}</AppWrapper>
        </ErrorBoundary>
      ) : (
        <LoadingIndicator modal loading />
      )}
    </>
  );
};

export default ApolloProviderBody;
