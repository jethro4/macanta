import React from 'react';
import HelperTextBtn from '@macanta/containers/FormField/HelperTextBtn';
import useDOSectionGuide from '@macanta/hooks/useDOSectionGuide';

const DOSectionGuideBtn = ({sectionName}) => {
  const doSectionGuide = useDOSectionGuide(sectionName);

  return (
    <HelperTextBtn
      style={{
        marginLeft: '1.5rem',
      }}>
      {doSectionGuide?.value}
    </HelperTextBtn>
  );
};

export default DOSectionGuideBtn;
