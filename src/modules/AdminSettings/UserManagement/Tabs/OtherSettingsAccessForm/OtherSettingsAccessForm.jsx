import React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Switch from '@macanta/components/Forms/ChoiceFields/Switch';
import BodySection from '@macanta/modules/AdminSettings/UserManagement/BodySection';
import PermissionsTable from '@macanta/modules/AdminSettings/UserManagement/Tabs/PermissionsTable';
import SpecificRelationshipsSelect from '@macanta/modules/AdminSettings/UserManagement/Tabs/ContactViewAccessForm/SpecificRelationshipsSelect';
import useValue from '@macanta/hooks/base/useValue';
import useOtherSettingsPermissions from '@macanta/hooks/permission/useOtherSettingsPermissions';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import DOTypesMultiSelect from './DOTypesMultiSelect';
import * as DataObjectsSearchTableStyled from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/styles';

// const ACCESS_PERMISSIONS_WITH_LABEL = [
//   {
//     label: 'Read Only',
//     value: ACCESS_LEVELS.READ_ONLY,
//   },
//   {
//     label: 'Read Write',
//     value: ACCESS_LEVELS.READ_WRITE,
//   },
// ];

const OTHER_SETTINGS_ACCESS_COLUMNS = [
  {
    id: 'label',
    label: 'Access Conditions',
    maxWidth: '50%',
    renderBodyCellValue: ({item, value, handlers}) => {
      if (item.id === 'doEditConnectedUserWithRelationship') {
        return (
          <Box>
            <Typography
              style={{
                fontSize: 'inherit',
                marginBottom: 12,
              }}>
              {value}
            </Typography>
            <SpecificRelationshipsSelect
              style={{
                width: 270,
              }}
              fetchAll
              value={item.permissionValue.otherUserRelationship}
              onChange={(values) => {
                handlers.onDOEditAccess({
                  item,
                  value: values,
                  key: 'otherUserRelationship',
                });
              }}
            />
          </Box>
        );
      }

      return value;
    },
  },
  {
    id: 'access',
    label: '',
    disableSort: true,
    disableResize: true,
    maxWidth: '50%',
    renderBodyCellValue: ({item, handlers}) => {
      let comp;

      if (item.hasDOEditOptions) {
        const doEditKey =
          item.id === 'doEditConnectedUser'
            ? 'withOtherUserDataToggle'
            : 'withOtherUserRelationshipDataToggle';
        const doEditWithRelationshipKey =
          item.id === 'doEditConnectedUserWithRelationship'
            ? 'withOtherUserRelationshipToggle'
            : 'withOtherUserRelationshipRelationshipToggle';

        comp = (
          <DataObjectsSearchTableStyled.ActionButtonContainer
            style={{
              width: 270,
              padding: 0,
              justifyContent: 'space-between',
            }}>
            <Switch
              activeColor="primary"
              label="Data"
              checked={item.permissionValue[doEditKey]}
              onChange={(event) => {
                handlers.onDOEditAccess({
                  item,
                  value: event.target.checked,
                  key: doEditKey,
                });
              }}
            />
            <Switch
              activeColor="primary"
              label="Relationships"
              checked={item.permissionValue[doEditWithRelationshipKey]}
              onChange={(event) => {
                handlers.onDOEditAccess({
                  item,
                  value: event.target.checked,
                  key: doEditWithRelationshipKey,
                });
              }}
            />
          </DataObjectsSearchTableStyled.ActionButtonContainer>
        );
      } else if (
        ['hideActionButtonsForGroups', 'hideConnectButtonsForGroups'].includes(
          item.id,
        )
      ) {
        comp = (
          <DOTypesMultiSelect
            style={{
              width: 270,
            }}
            value={item.permissionValue}
            onChange={(values) => {
              handlers.onOtherAccess({item, values});
            }}
          />
        );
      } else {
        comp = (
          <Switch
            activeColor="primary"
            checked={item.permissionValue}
            onChange={(event) => {
              handlers.onChangeAccess({
                item,
                value: event.target.checked,
              });
            }}
          />
        );
      }

      return (
        <DataObjectsSearchTableStyled.ActionButtonContainer
          style={{
            width: '100%',
            padding: 0,
            justifyContent: 'flex-start',
          }}>
          {comp}
        </DataObjectsSearchTableStyled.ActionButtonContainer>
      );
    },
  },
];

const OtherSettingsAccessForm = ({permission, onChange, ...props}) => {
  const permissionMetaQuery = useOtherSettingsPermissions(
    {
      permission,
    },
    {
      fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
    },
  );

  const handleChangeAccess = ({item, value}) => {
    const updatedPermission = {
      ...permission,
      permissionMeta: {
        ...permission.permissionMeta,
        [item.id]: value
          ? item.hasOptions
            ? []
            : true
          : item.hasOptions
          ? null
          : false,
      },
    };

    onChange(updatedPermission);
  };

  const handleOtherAccess = ({item, values}) => {
    const updatedPermission = {
      ...permission,
      permissionMeta: {
        ...permission.permissionMeta,
        [item.id]: values,
      },
    };

    onChange(updatedPermission);
  };

  const handleDOEditAccess = ({value, key}) => {
    const updatedPermission = {
      ...permission,
      permissionMeta: {
        ...permission.permissionMeta,
        userDOEditPermission: {
          ...permission.permissionMeta?.userDOEditPermission,
          [key]: value,
        },
      },
    };

    onChange(updatedPermission);
  };

  const [data] = useValue(() => {
    return permissionMetaQuery.data.reduce((acc, d) => {
      const accArr = [...acc];

      accArr.push({
        isSubheader: true,
        title: d.title,
      });

      const permissionValues = Object.values(d.permissionsMap);

      permissionValues.forEach((item) => {
        if (item.id === 'doEditConnectedUser') {
          accArr.push({
            isSubheader: true,
            title: 'Allow Edit If Another User Is Connected',
            nested: true,
          });
        }
        accArr.push(item);
      });

      return accArr;
    }, []);
  }, [permissionMetaQuery.data]);

  return (
    <BodySection {...props}>
      <PermissionsTable
        bodyRowHeight={54}
        columns={OTHER_SETTINGS_ACCESS_COLUMNS}
        data={data}
        handlers={{
          onChangeAccess: handleChangeAccess,
          onOtherAccess: handleOtherAccess,
          onDOEditAccess: handleDOEditAccess,
        }}
        getIsAutoHeight={(item) => item.hasOptions || item.hasDOEditOptions}
      />
    </BodySection>
  );
};

export default OtherSettingsAccessForm;
