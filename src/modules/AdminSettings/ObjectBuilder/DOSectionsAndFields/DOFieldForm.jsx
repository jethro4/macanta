import React, {useState, useMemo, useEffect} from 'react';
import {useApolloClient} from '@apollo/client';
import {useFormikContext} from 'formik';
import PreviewIcon from '@mui/icons-material/Preview';
import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';
import Box from '@mui/material/Box';
import Button, {PopoverButton} from '@macanta/components/Button';
import Markdown from '@macanta/components/Markdown';
import FormField from '@macanta/containers/FormField';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import {LIST_DATA_OBJECT_FIELDS} from '@macanta/graphql/dataObjects';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {FIELD_APPLICABLE_TO} from '@macanta/constants/contactFields';
import {
  FIELD_TYPES,
  ALL_MIME_TYPE_CATEGORIES_WITH_ALL,
  UPLOAD_TYPES,
} from '@macanta/constants/form';
import {
  getCategories,
  getDerivedCategory,
} from '@macanta/modules/AdminSettings/FormBuilder/Forms/helpers';
import {arrayConcatOrUpdateByKey} from '@macanta/utils/array';
import MARKDOWN_GUIDE_HTML from '@macanta/fixtures/assets/markdownGuide';
import * as Storage from '@macanta/utils/storage';
import ChoicesList from './ChoicesList';

const fieldTypes = [...FIELD_TYPES];

fieldTypes.splice(0, 0, {
  isSubheader: true,
  label: 'Basic',
});
fieldTypes.splice(7, 0, {
  isSubheader: true,
  label: 'Choice',
});
fieldTypes.splice(11, 0, {
  isSubheader: true,
  label: 'Other',
});

export const YES_NO = [
  {
    label: 'Yes',
    value: true,
  },
  {
    label: 'No',
    value: false,
  },
];

const TYPES_WITH_CHOICES = ['Select', 'Radio', 'Checkbox', 'FileUpload'];

const DOFieldForm = ({
  item: itemProp,
  sectionId: sectionIdProp,
  subGroupId: subGroupIdProp,
  onClose,
  type,
  hideFieldName,
}) => {
  const client = useApolloClient();

  const {values, setValues} = useFormikContext();

  const doTypesQuery = useDOTypes({
    removeEmail: true,
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const isNew = !itemProp.id;
  const doTypes = doTypesQuery?.data?.listDataObjectTypes;

  const [item, setItem] = useState(itemProp);
  const [sectionId, setSectionId] = useState(sectionIdProp);
  const [subGroupId, setSubGroupId] = useState(subGroupIdProp);
  const [doTypesKeys, setDOTypesKeys] = useState({});
  const [loading, setLoading] = useState(false);

  const uploadDetails = !item.fileUpload
    ? {}
    : {
        uploadType: !item.fileUpload.multiple ? 'single' : 'multiple',
        category: getDerivedCategory(item.fileUpload.mimeTypes),
      };

  const handleChange = (key) => (value) => {
    setItem((state) => ({
      ...state,
      [key]: value,
      ...(key === 'type' &&
        !TYPES_WITH_CHOICES.includes(value) && {
          choices: [],
        }),
      ...(key === 'type' &&
        state.dataReference && {
          dataReference: null,
        }),
      ...(key === 'type' &&
        value === 'FileUpload' && {
          fileUpload: {mimeTypes: getCategories('all'), multiple: false},
          choices: ['Not Uploaded', 'Uploaded'],
          default: 'Not Uploaded',
        }),
    }));
  };

  const handleAddOrEdit = () => {
    const updatedId = item.id || String(Date.now());

    setValues((state) => {
      const itemData = {
        isNew,
        id: updatedId,
        ...item,
        ...(!TYPES_WITH_CHOICES.includes(item.type)
          ? {
              choices: [],
            }
          : {
              choices: item.choices,
            }),
      };

      const hasSectionConditionNameChanged = state.sectionConditions?.some(
        (sectionCondition) =>
          sectionCondition.conditions.some(
            (condition) =>
              itemData.id === condition.fieldId &&
              itemData.name !== condition.name,
          ),
      );

      return {
        ...state,
        sectionsData: state.sectionsData.map((section) => {
          if (section.id === sectionIdProp || sectionId === section.id) {
            return {
              ...section,
              subGroups: section.subGroups.map((subGroup) => {
                let finalSubGroup = subGroup;

                if (
                  subGroup.id === subGroupIdProp &&
                  subGroupId !== subGroupIdProp
                ) {
                  finalSubGroup = {
                    ...subGroup,
                    data: subGroup.data.filter((d) => d.id !== itemData.id),
                  };
                }

                if (subGroupId === finalSubGroup.id) {
                  finalSubGroup = {
                    ...finalSubGroup,
                    data: arrayConcatOrUpdateByKey({
                      arr: finalSubGroup.data,
                      item: itemData,
                    }),
                  };
                }

                return finalSubGroup;
              }),
            };
          }

          return section;
        }),
        ...(hasSectionConditionNameChanged && {
          sectionConditions: state.sectionConditions.map((sectionCondition) => {
            return {
              ...sectionCondition,
              conditions: sectionCondition.conditions.map((condition) => {
                const exists = itemData.id === condition.fieldId;

                if (exists) {
                  return {...condition, id: itemData.id, name: itemData.name};
                }

                return condition;
              }),
            };
          }),
          updatedSectionConditions: state.sectionConditions.map(
            (sectionCondition) => {
              return {
                ...sectionCondition,
                conditions: sectionCondition.conditions.map((condition) => {
                  const exists = itemData.id === condition.fieldId;

                  if (exists) {
                    return {
                      ...condition,
                      id: itemData.id,
                      name: itemData.name,
                      hasChanged: true,
                    };
                  }

                  return condition;
                }),
              };
            },
          ),
        }),
      };
    });

    onClose();
  };

  const handleChangeSectionIds = (sectionSubGroupId) => {
    if (sectionSubGroupId) {
      const [sectionIdVal, subGroupIdVal] = sectionSubGroupId.split('|');

      setSectionId(sectionIdVal);
      setSubGroupId(subGroupIdVal);
    }
  };

  const generateDOTypesKeys = async (doTypesArr) => {
    const {email: loggedInUserEmail} = Storage.getItem('userDetails');

    setLoading(true);

    await doTypesArr.reduce(async (acc, t) => {
      const accObj = {...(await acc)};

      const {data} = await client.query({
        query: LIST_DATA_OBJECT_FIELDS,
        fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
        variables: {
          groupId: t.id,
          userEmail: loggedInUserEmail,
        },
      });

      accObj[t.title] = data?.listDataObjectFields || [];

      setDOTypesKeys(accObj);

      return accObj;
    }, {});

    setLoading(false);
  };

  const sectionSubGroupNames = useMemo(() => {
    if (values.sectionsData.length) {
      return values.sectionsData.reduce((acc, section) => {
        const sectionsArr = [...acc];
        const hideSection = section.id === 'section-Basic Info';

        section.subGroups.forEach((subGroup) => {
          if (!hideSection) {
            sectionsArr.push({
              label: `${section.sectionName} - ${subGroup.subGroupName}`,
              value: `${section.id}|${subGroup.id}`,
            });
          }
        });

        return sectionsArr;
      }, []);
    }

    return [];
  }, [values.sectionsData]);

  useEffect(() => {
    if (itemProp) {
      setItem(itemProp);
    }
  }, [itemProp]);

  useEffect(() => {
    if (doTypes) {
      generateDOTypesKeys(doTypes.filter((t) => t.id !== values.id));
    }
  }, [doTypes]);

  const doTypesWithFields = useMemo(() => {
    return Object.entries(doTypesKeys).reduce((acc, [title, data]) => {
      const fieldsArr = [...acc];

      fieldsArr.push({
        isSubheader: true,
        label: title,
      });

      data.forEach((d) => {
        fieldsArr.push({
          label: d.name,
          value: d.id,
        });
      });

      return fieldsArr;
    }, []);
  }, [doTypesKeys]);

  return (
    <>
      <Box
        style={{
          display: 'flex',
        }}>
        <Box
          style={{
            flexGrow: 1,
            flexBasis: 0,
            overflowX: 'hidden',
            padding: '1rem',
          }}>
          {!hideFieldName && (
            <FormField
              autoFocus={isNew}
              style={{
                marginBottom: '1rem',
              }}
              required
              value={item.name}
              onChange={handleChange('name')}
              label="Field Name"
              size="small"
              variant="outlined"
            />
          )}

          {type === 'contact' && (
            <FormField
              disabled={hideFieldName}
              style={{
                marginBottom: '1rem',
              }}
              type="Select"
              options={FIELD_APPLICABLE_TO}
              value={item.fieldApplicableTo}
              onChange={handleChange('fieldApplicableTo')}
              label="Field Applicable To"
              size="small"
              variant="outlined"
            />
          )}

          {(type !== 'contact' || !hideFieldName) && (
            <FormField
              style={{
                marginBottom: '1rem',
              }}
              type="Select"
              options={sectionSubGroupNames}
              value={`${sectionId}|${subGroupId}`}
              onChange={handleChangeSectionIds}
              label="Section & Sub-Section"
              size="small"
              variant="outlined"
            />
          )}

          <FormField
            style={{
              marginBottom: '1rem',
            }}
            value={item.placeholder}
            onChange={handleChange('placeholder')}
            label="Placeholder"
            size="small"
            variant="outlined"
          />

          <FormField
            style={{
              marginBottom: '1rem',
            }}
            value={item.headingText}
            onChange={handleChange('headingText')}
            label="Heading Above This Field"
            size="small"
            variant="outlined"
          />

          <FormField
            style={{
              marginBottom: '1rem',
            }}
            type="TextArea"
            label="Helper Text"
            placeholder="Type markdown here..."
            value={item.helpText}
            onChange={handleChange('helpText')}
            helperText={MARKDOWN_GUIDE_HTML}
            HelperProps={{
              PopoverProps: {
                title: 'Markdown Guide',
                anchorOrigin: {
                  vertical: 'center',
                  horizontal: 'right',
                },
                transformOrigin: {
                  vertical: 'center',
                  horizontal: 'left',
                },
              },
              TooltipProps: {
                title: 'Markdown Guide',
              },
            }}
            TopRightComp={
              !!item.helpText && (
                <PopoverButton
                  color="secondary"
                  icon
                  // onClick={handleAdd}
                  size="small"
                  TooltipProps={{
                    title: 'Preview',
                  }}
                  PopoverProps={{
                    contenWidth: 600,
                    contentMinWidth: 250,
                    bodyStyle: {
                      margin: '1rem',
                    },
                    anchorOrigin: {
                      vertical: 'center',
                      horizontal: 'right',
                    },
                    transformOrigin: {
                      vertical: 'center',
                      horizontal: 'left',
                    },
                  }}
                  renderContent={() =>
                    !!item.helpText && <Markdown>{item.helpText}</Markdown>
                  }>
                  <PreviewIcon />
                </PopoverButton>
              )
            }
          />
        </Box>

        <Box
          style={{
            flexGrow: 1,
            flexBasis: 0,
            overflowX: 'hidden',
            padding: '1rem',
          }}>
          <FormField
            required
            style={{
              marginBottom: '1rem',
            }}
            type="Select"
            allowSearch={false}
            options={
              type !== 'contact'
                ? fieldTypes
                : fieldTypes.filter((t) => t.value !== 'DataReference')
            }
            value={item.type}
            onChange={handleChange('type')}
            label="Type"
            size="small"
            variant="outlined"
          />

          {item.type !== 'FileUpload' &&
            TYPES_WITH_CHOICES.includes(item.type) && (
              <Box
                style={{
                  marginBottom: '1.5rem',
                }}>
                <ChoicesList
                  data={item.choices}
                  onChange={handleChange('choices')}
                />
              </Box>
            )}

          {item.type === 'DataReference' && (
            <FormField
              required
              loading={loading}
              style={{
                marginBottom: '1rem',
              }}
              type="Select"
              options={doTypesWithFields}
              value={item.dataReference}
              onChange={handleChange('dataReference')}
              label="Reference Field"
              size="small"
              variant="outlined"
            />
          )}

          {item.type === 'FileUpload' && (
            <>
              <FormField
                style={{
                  marginBottom: '1rem',
                }}
                row
                type="Radio"
                options={UPLOAD_TYPES}
                labelPosition="normal"
                size="small"
                value={uploadDetails.uploadType}
                onChange={(val) =>
                  handleChange('fileUpload')({
                    ...item.fileUpload,
                    multiple: val === 'multiple',
                  })
                }
                variant="outlined"
              />
              <FormField
                style={{
                  marginBottom: '1rem',
                }}
                type="Select"
                size="small"
                options={ALL_MIME_TYPE_CATEGORIES_WITH_ALL}
                value={uploadDetails.category}
                onChange={(val) =>
                  handleChange('fileUpload')({
                    ...item.fileUpload,
                    mimeTypes: getCategories(val),
                  })
                }
                label="Mime Types"
                variant="outlined"
              />
            </>
          )}

          <FormField
            style={{
              marginBottom: '1rem',
            }}
            value={item.default}
            onChange={handleChange('default')}
            label="Default Value"
            size="small"
            variant="outlined"
            {...(TYPES_WITH_CHOICES.includes(item.type) && {
              type: 'Select',
              options: item.choices,
            })}
          />
        </Box>

        <Box
          style={{
            flexGrow: 0.7,
            flexBasis: 0,
            overflowX: 'hidden',
            marginLeft: 8,
            padding: '1rem',
          }}>
          <FormField
            style={{
              marginBottom: '1rem',
            }}
            type="Radio"
            row
            options={YES_NO}
            value={item.required}
            onChange={handleChange('required')}
            label="Required Field"
            size="small"
            variant="outlined"
          />

          {type !== 'contact' && (
            <FormField
              style={{
                marginBottom: '1rem',
              }}
              type="Radio"
              row
              options={YES_NO}
              value={item.contactSpecificField}
              onChange={handleChange('contactSpecificField')}
              label="Contact Specific"
              size="small"
              variant="outlined"
            />
          )}

          <FormField
            style={{
              marginBottom: '1rem',
            }}
            type="Radio"
            row
            options={YES_NO}
            value={item.addDivider}
            onChange={handleChange('addDivider')}
            label="Add Divider After This Field"
            size="small"
            variant="outlined"
          />
        </Box>
      </Box>
      <Box
        style={{
          display: 'flex',
          justifyContent: 'flex-end',
          padding: '12px 1rem',
          borderTop: '1px solid #eee',
        }}>
        <Button
          disabled={!item.name}
          variant="contained"
          startIcon={isNew ? <AddIcon /> : <EditIcon />}
          onClick={handleAddOrEdit}
          size="medium">
          {isNew ? 'Add' : 'Save'} Field Details
        </Button>
      </Box>
    </>
  );
};

DOFieldForm.defaultProps = {
  item: {
    type: 'Text',
    required: false,
    dataReference: false,
    contactSpecificField: false,
    showInTable: false,
    addDivider: false,
    choices: [],
    fieldApplicableTo: 'All',
  },
};

export default DOFieldForm;
