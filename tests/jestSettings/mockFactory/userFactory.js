import {nextFactoryId} from './factory';

const typename = 'User';

export default (overwrites) => {
  const {currentIndex} = nextFactoryId(typename);

  return {
    __typename: typename,
    id: `Contact${currentIndex}`,
    email: `test${currentIndex}@macantacrm.com`,
    firstName: `FirstName${currentIndex}`,
    lastName: `LastName${currentIndex}`,
    templateId: `PermissionTemplate${currentIndex}`,
    contactLevel: 'User',
    ...overwrites,
  };
};
