import React, {useState, useEffect, useMemo} from 'react';
import isArray from 'lodash/isArray';
import Modal from '@macanta/components/Modal';
import EmailBatchWidgetsForms from './EmailBatchWidgetsForms';
import EmailIcon from '@mui/icons-material/Email';
import Button from '@macanta/components/Button';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import * as Storage from '@macanta/utils/storage';

const EmailBatchWidgetsIconBtn = ({
  addedWidgets: addedWidgetsProp,
  showModal: showModalProp,
  onShowModal,
  onCloseModal,
  selectedWidgetIds,
  disableSelectRecipient,
  ...props
}) => {
  const loggedInUserDetails = Storage.getItem('userDetails');

  const [showModal, setShowModal] = useState(showModalProp);
  const {displayMessage} = useProgressAlert();

  const handleAddContact = () => {
    if (!onShowModal) {
      setShowModal(true);
    } else {
      onShowModal && onShowModal();
    }
  };

  const handleCloseModal = () => {
    if (!onCloseModal) {
      setShowModal(false);
    } else {
      onCloseModal && onCloseModal();
    }
  };

  const onSuccess = () => {
    displayMessage('Batch email sent!');
    setShowModal(false);
  };

  useEffect(() => {
    setShowModal(showModalProp);
  }, [showModalProp]);

  const addedWidgets = useMemo(() => {
    if (addedWidgetsProp) {
      const transformedWidgets = isArray(addedWidgetsProp)
        ? addedWidgetsProp
        : [addedWidgetsProp];

      return transformedWidgets;
    }
  }, [addedWidgetsProp]);

  return (
    <>
      <Button
        disabled={!addedWidgets.length}
        onClick={handleAddContact}
        size="small"
        variant="contained"
        startIcon={<EmailIcon />}
        {...props}>
        Send Email
      </Button>
      <Modal
        headerTitle={`Compose Email (${loggedInUserDetails.email})`}
        open={showModal}
        onClose={handleCloseModal}
        contentWidth={'86%'}
        contentHeight={'94%'}>
        <EmailBatchWidgetsForms
          selectedWidgetIds={selectedWidgetIds}
          widgets={addedWidgets}
          onSuccess={onSuccess}
          disableSelectRecipient={disableSelectRecipient}
        />
      </Modal>
    </>
  );
};

EmailBatchWidgetsIconBtn.defaultProps = {
  showModal: false,
  addedWidgets: [],
  selectedWidgetIds: [],
};

export default EmailBatchWidgetsIconBtn;
