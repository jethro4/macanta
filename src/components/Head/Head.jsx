/**
 * Head component that queries for data with
 *  Gatsby's useStaticQuery React hook
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React, {useEffect} from 'react';
import Helmet from 'react-helmet';
import {GET_APP_SETTINGS} from '@macanta/graphql/app';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import useQuery from '@macanta/hooks/apollo/useQuery';
import {ALERT_VERSION_UPDATE} from '@macanta/config/versionConfig';
import * as Storage from '@macanta/utils/storage';
var pjson = require('../../../package.json');

const Head = ({lang, translate, meta, title}) => {
  const cachedSitename = Storage.getItem('sitename');

  const appSettingsQuery = useQuery(GET_APP_SETTINGS, {
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
    skip: !!title,
  });

  const sitename =
    appSettingsQuery.data?.getAppSettings?.sitename || cachedSitename;

  useEffect(() => {
    if (sitename) {
      Storage.setItem('sitename', sitename);
    }
  }, [sitename]);

  return (
    <Helmet
      htmlAttributes={{
        lang,
        translate,
      }}
      title={title || sitename}
      defer={false}
      meta={[
        {
          name: 'viewport',
          content:
            'minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no',
        },
        {
          name: 'version',
          content: pjson.version,
        },
        {
          name: 'alert_version_update',
          content: ALERT_VERSION_UPDATE,
        },
      ].concat(meta)}
    />
  );
};

Head.defaultProps = {
  lang: 'en',
  translate: 'no',
  meta: [],
};

export default Head;
