import React from 'react';

const RawCheckbox = ({id, options, ...props}) => {
  return options.map((o, index) => {
    const transformedId = `${id}-${index}`;

    return (
      <div key={transformedId} className="form-check">
        <input type="checkbox" id={transformedId} value={o} {...props} />
        <label className="form-check-label" htmlFor={transformedId}>
          {o}
        </label>
      </div>
    );
  });
};

export default RawCheckbox;
