import {gql} from '@apollo/client';
import {DO_TO_DO_RELATIONSHIP_FRAGMENT} from '@macanta/graphql/relationships/fragments';

export default gql`
  mutation connectDOToDORelationship($input: ConnectDOToDORelationshipInput!) {
    connectDOToDORelationship(input: $input) {
      ...doToDORelationshipFragment
    }
  }
  ${DO_TO_DO_RELATIONSHIP_FRAGMENT}
`;
