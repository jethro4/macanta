import React from 'react';
import EditQueryPopover from './EditQueryPopover';
import MoreOptions from './MoreOptions';
import * as DataObjectsSearchTableStyled from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/styles';

const ItemButtons = ({item}) => {
  return (
    <DataObjectsSearchTableStyled.ActionButtonContainer>
      <EditQueryPopover item={item} />
      <MoreOptions id={item.id} name={item.name} doType={item.doType} />
    </DataObjectsSearchTableStyled.ActionButtonContainer>
  );
};

export default ItemButtons;
