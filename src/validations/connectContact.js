import * as yup from 'yup';

const connectContactValidationSchema = yup.object().shape({
  contact: yup.object().required('Select a contact'),
  relationships: yup
    .array()
    .required('Required')
    .min(1, 'Required at least one'),
});

export default connectContactValidationSchema;
