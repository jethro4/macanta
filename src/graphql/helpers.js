import {client} from '@macanta/containers/ApolloProviderWrapper';
import {getTagFieldName} from '@macanta/utils/graphql';

const getItemWithTypename = ({item, typeName}) => ({
  ...item,
  __typename: typeName,
});

export const readQuery = ({graphqlTag, variables}) => {
  const data = client.cache.readQuery({
    query: graphqlTag,
    variables,
  });

  const tagFieldName = getTagFieldName(graphqlTag);
  const result = data?.[tagFieldName];

  return result;
};

export const readItemFragment = ({id, typeName, fragment}) => {
  return client.cache.readFragment({
    id: `${typeName}:${id}`,
    fragment,
  });
};

export const writeItemFragment = ({
  keyField = 'id',
  item,
  typeName,
  fragment,
  fragmentName,
}) => {
  if (item[keyField]) {
    return client.cache.writeFragment({
      fragment,
      data: getItemWithTypename({item, typeName}),
      fragmentName,
    });
  }
};

export const writeItemInListFragment = ({
  isCreate,
  keyField = 'id',
  item,
  typeName,
  fragment,
  fragmentName,
  parentKey,
  listKey,
  position,
}) => {
  if (item?.[keyField]) {
    const itemWithType = getItemWithTypename({item, typeName});

    client.cache.modify({
      ...(!isCreate &&
        item[keyField] && {id: client.cache.identify(itemWithType)}),
      fields: {
        [parentKey](parent) {
          let existingItems = parent[listKey];

          const newItemRef = writeItemFragment({
            keyField,
            item,
            typeName,
            fragment,
            fragmentName,
          });

          if (position === 'last') {
            existingItems = [...existingItems, newItemRef];
          } else {
            existingItems = [newItemRef, ...existingItems];
          }

          return {
            ...parent,
            [listKey]: existingItems,
          };
        },
      },
    });
  }
};

export const writeItemInListQuery = ({
  graphqlTag,
  typeName,
  item,
  existingItems,
  position,
}) => {
  const graphqlTagFieldName = graphqlTag.definitions?.[0]?.name.value;
  const updatedItems =
    position === 'last'
      ? existingItems.concat(item)
      : [item].concat(existingItems);

  client.writeQuery({
    query: graphqlTag,
    data: {
      [graphqlTagFieldName]: {
        __typename: typeName,
        items: updatedItems,
        count: updatedItems.length,
        total: updatedItems.length,
      },
    },
  });
};

export const writeItems = ({
  isCreate,
  keyField = 'id',
  item,
  typeName,
  fragment,
  fragmentName,
  listKey,
  position,
}) => {
  if (item?.[keyField]) {
    const itemWithType = getItemWithTypename({item, typeName});

    client.cache.modify({
      ...(!isCreate &&
        item[keyField] && {id: client.cache.identify(itemWithType)}),
      fields: {
        [listKey](existingItems = []) {
          const newItemRef = writeItemFragment({
            keyField,
            item,
            typeName,
            fragment,
            fragmentName,
          });

          // // Quick safety check - if the new comment is already
          // // present in the cache, we don't need to add it again.
          // if (
          //   existingItems.some(
          //     (ref) => readField(keyField, ref) === newItemRef[keyField],
          //   )
          // ) {
          //   return existingItems;
          // }

          if (position === 'last') {
            return [...existingItems, newItemRef];
          }

          return [newItemRef, ...existingItems];
        },
      },
    });
  }
};

export const deleteItem = ({keyField = 'id', value, typeName}) => {
  const normalizedId = client.cache.identify({
    [keyField]: value,
    __typename: typeName,
  });
  client.cache.evict({[keyField]: normalizedId});
  client.cache.gc();
};

export const extractCache = () => {
  const serializedState = client.cache.extract();

  return serializedState;
};

export const getAllItemsByFragment = (typeName, fragment) => {
  const serializedState = extractCache();

  const items = Object.values(serializedState)
    .filter((item) => item.__typename === typeName)
    .map((item) =>
      client.readFragment({
        fragmentName: 'FragmentName',
        fragment,
        id: item.id,
      }),
    );

  return items;
};

export const resetQueriesCache = ({fieldName, args, ...options}) => {
  client.cache.evict({fieldName, args, ...options});
  // client.cache.gc();
};

export const deleteInCache = (queryId, typeName) => {
  if (queryId && typeName) {
    const normalizedId = client.cache.identify({
      queryId,
      __typename: typeName,
    });
    client.cache.evict({id: normalizedId});
    client.cache.gc();
  }
};
