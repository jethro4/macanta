import useMutation from '@macanta/hooks/apollo/useMutation';
import {POST_TRIGGER_ACTION} from '@macanta/graphql/automation';

const usePostTriggerActionMutation = (options) => {
  const mutation = useMutation(POST_TRIGGER_ACTION, options);

  return mutation;
};

export default usePostTriggerActionMutation;
