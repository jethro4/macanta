import React, {useState} from 'react';
import SearchIcon from '@mui/icons-material/Search';
import MenuList from '@mui/material/MenuList';
import {SelectMenuItems} from '@macanta/components/Forms/ChoiceFields/SelectField';
import Modal from '@macanta/components/Modal';
import useQueries from '@macanta/hooks/admin/useQueries';
import useQueryWidgets from '@macanta/hooks/admin/useQueryWidgets';
import useValue from '@macanta/hooks/base/useValue';
import WidgetData from '@macanta/modules/pages/AppContainer/Widgets/WidgetData';

const DOItemQueries = ({item, doTitle, onSave}) => {
  const [selectedWidget, setSelectedWidget] = useState(null);

  const showModal = item && !!selectedWidget;

  const handleCloseModal = () => {
    setSelectedWidget(null);
  };

  const {loading: loadingQueries, items: queryBuilders} = useQueries({
    doType: doTitle,
  });
  const {loading: loadingQueryWidgets, items: queryWidgets} = useQueryWidgets();

  const [filteredQueryWidgets] = useValue(() => {
    const queryWidgetsArr =
      queryWidgets?.filter((widget) =>
        queryBuilders?.some(
          (queryBuilder) => queryBuilder.id === widget.queryId,
        ),
      ) || [];

    return queryWidgetsArr.map((widget) => ({
      label: widget.title,
      value: widget.queryId,
      IconComp: (
        <SearchIcon
          color="primary"
          style={{
            marginLeft: -4,
            marginRight: 10,
          }}
        />
      ),
    }));
  }, [queryBuilders, queryWidgets]);

  const handleSelectWidget = (queryId) => {
    const widget = queryWidgets.find(
      (queryWidget) => queryWidget.queryId === queryId,
    );
    setSelectedWidget(widget);
  };

  const loading = loadingQueries || loadingQueryWidgets;

  return (
    <>
      <MenuList
        style={{
          height: '100%',
          padding: 0,
        }}>
        <SelectMenuItems
          loading={loading && !queryWidgets}
          popover
          withDivider
          // value={value}
          options={filteredQueryWidgets}
          allowSearch={filteredQueryWidgets.length > 8}
          onChange={(newVal) => {
            handleSelectWidget(newVal);
            // handleClose();
          }}
          emptyMessage="All sections added"
        />
      </MenuList>
      <Modal
        headerTitle={`${selectedWidget?.title} (${item?.id})`}
        open={showModal}
        onClose={handleCloseModal}
        contentWidth={'96%'}
        contentHeight={'92%'}>
        <WidgetData
          itemId={item?.id}
          loading={loading}
          widget={selectedWidget}
          range="main"
          hideQueriesBtn
          onSave={onSave}
        />
      </Modal>
    </>
  );
};

DOItemQueries.defaultProps = {};

export default DOItemQueries;
