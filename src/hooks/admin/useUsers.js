import useRetryQuery from '@macanta/hooks/apollo/useRetryQuery';
import {LIST_USERS} from '@macanta/graphql/users';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';

const useUsers = (options) => {
  const usersQuery = useRetryQuery(LIST_USERS, {
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
    ...options,
  });

  const updatedUsers = usersQuery.data?.listUsers?.items;
  const total = usersQuery.data?.listUsers?.total;

  return {...usersQuery, data: updatedUsers, total};
};

export default useUsers;
