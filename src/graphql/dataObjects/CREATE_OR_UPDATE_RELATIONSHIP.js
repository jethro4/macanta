import {gql} from '@apollo/client';

export default gql`
  mutation createOrUpdateRelationship(
    $createOrUpdateRelationshipInput: CreateOrUpdateRelationshipInput!
  ) {
    createOrUpdateRelationship(input: $createOrUpdateRelationshipInput) {
      id
      title
      description
    }
  }
`;
