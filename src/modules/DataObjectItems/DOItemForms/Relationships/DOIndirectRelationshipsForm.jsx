import React from 'react';
import flatten from 'lodash/flatten';
import {useTheme} from '@mui/material/styles';
import Section from '@macanta/containers/Section';
import * as ListStyled from '@macanta/containers/List/styles';
import {LIST_DO_TO_DO_RELATIONSHIPS} from '@macanta/graphql/relationships';
import {
  groupBy,
  sortArrayByKeyValuesIncludeAll,
  uniqByKeys,
} from '@macanta/utils/array';
import * as Styled from './styles';
import DORelationshipItems from '@macanta/modules/DataObjectItems/DOItemForms/Relationships/DORelationshipItems';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import useBatchQuery from '@macanta/hooks/apollo/useBatchQuery';
import useValue from '@macanta/hooks/base/useValue';
import Delay from '@macanta/components/Delay';
import useAppSettings from '@macanta/hooks/admin/useAppSettings';

const DOIndirectRelationshipsForm = ({
  loading,
  doItemId,
  filteredDirectItems,
  options,
  ...props
}) => {
  const theme = useTheme();

  const appSettingsQuery = useAppSettings({
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const doTypesQuery = useDOTypes({
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const macantaTabOrder = appSettingsQuery.data?.macantaTabOrder;
  const doTypes = doTypesQuery.data?.listDataObjectTypes;

  const indirectDOBatchQuery = useBatchQuery(LIST_DO_TO_DO_RELATIONSHIPS, {
    dependencies: [doItemId],
    ...options,
    variablesArr: filteredDirectItems.map((item) => ({
      ...item,
      type: 'indirect',
    })),
  });

  const [indirectItems] = useValue(() => {
    if (indirectDOBatchQuery.data) {
      const allIndirectItems = uniqByKeys(flatten(indirectDOBatchQuery.data), [
        'doItemId',
        'groupId',
      ]);
      const filteredItems = allIndirectItems.filter(
        (d) =>
          d.doItemId !== doItemId &&
          !filteredDirectItems.some((r) => r.doItemId === d.doItemId),
      );
      const groupedResult = groupBy(filteredItems, ['groupId']);

      const sortedResult =
        !macantaTabOrder || !doTypes
          ? groupedResult
          : sortArrayByKeyValuesIncludeAll(
              groupedResult,
              'groupId',
              macantaTabOrder
                .map((tab) => doTypes.find((d) => d.title === tab)?.id)
                .filter((tabId) => !!tabId),
            );

      return sortedResult;
    }

    return [];
  }, [
    indirectDOBatchQuery.data,
    filteredDirectItems,
    macantaTabOrder,
    doTypes,
  ]);

  return (
    <Section
      data-testid="DOIndirectRelationshipsForm"
      loading={loading || indirectDOBatchQuery.loading}
      fullHeight
      title="Indirect DO Relationships"
      {...props}
      bodyStyle={{
        padding: 0,
        backgroundColor: theme.palette.grayLight.main,
        ...props.bodyStyle,
      }}
      style={{
        marginTop: 0,
        ...props.style,
      }}>
      {!loading && filteredDirectItems.length === 0 && (
        <Delay>
          <ListStyled.EmptyMessage align="center" color="#888">
            No indirect relationships
          </ListStyled.EmptyMessage>
        </Delay>
      )}
      {indirectItems?.map?.((gd) => {
        const doType = doTypes?.find((d) => d.id === gd.groupId);

        return (
          <Styled.ItemContainer key={gd.groupId}>
            <Styled.ItemHeader>{doType?.title}</Styled.ItemHeader>
            <Styled.ItemBody className={`indirect-${gd.groupId}`}>
              <DORelationshipItems items={gd.items} doTitle={doType?.title} />
            </Styled.ItemBody>
          </Styled.ItemContainer>
        );
      })}
    </Section>
  );
};

DOIndirectRelationshipsForm.defaultProps = {
  filteredDirectItems: [],
  origDirectItems: [],
};

export default DOIndirectRelationshipsForm;
