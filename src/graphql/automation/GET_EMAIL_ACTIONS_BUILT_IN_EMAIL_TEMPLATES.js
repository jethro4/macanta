import {gql} from '@apollo/client';

export const EMAIL_ACTIONS_BUILT_IN_EMAIL_TEMPLATES_FRAGMENT = gql`
  fragment emailActionsBuiltInEmailTemplatesFragment on EmailActionsBuiltInEmailTemplates {
    action
    count
    status
    data {
      EmailId
      Title
      Version
      CompiledEmailHTML
      CompiledEmailAmpHTML
      EmailHTML
      EmailCSS
      TemplateHTML
      TemplateCSS
      Screenshot
      Global
      TemplateGroupId
      Type
      Status
      Updated
      Created
      Thumbnail
    }
  }
`;

export default gql`
  query getEmailActionsBuiltInEmailTemplates(
    $appName: String!
    $apiKey: String!
    $id: ID!
  ) {
    getEmailActionsBuiltInEmailTemplates(
      appName: $appName
      apiKey: $apiKey
      id: $id
    ) {
      ...emailActionsBuiltInEmailTemplatesFragment
    }
  }
  ${EMAIL_ACTIONS_BUILT_IN_EMAIL_TEMPLATES_FRAGMENT}
`;
