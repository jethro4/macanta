import React from 'react';
import EditIcon from '@mui/icons-material/Edit';
import VisibilityIcon from '@mui/icons-material/Visibility';
import {ModalButton, PopoverButton} from '@macanta/components/Button';

const EditButton = ({
  id,
  renderFormComponent,
  showDetails,
  modal,
  containerProps,
  icon,
  children,
  ...props
}) => {
  const Button = modal ? ModalButton : PopoverButton;
  const containerPropsKey = modal ? 'ModalProps' : 'PopoverProps';
  const titlePropKey = modal ? 'headerTitle' : 'title';
  const iconComp = showDetails ? (
    <VisibilityIcon />
  ) : (
    <EditIcon
      style={{
        fontSize: 20,
      }}
    />
  );
  const childrenComp = icon ? iconComp : children;
  const otherProps = Object.assign(
    {},
    icon
      ? {
          color: 'info',
          sx: {
            padding: '0.25rem',
          },
          TooltipProps: {
            title: showDetails ? 'Show Details' : 'Edit',
          },
        }
      : {
          startIcon: iconComp,
          variant: 'contained',
        },
  );

  return (
    <Button
      icon={icon}
      size="small"
      {...{
        [containerPropsKey]: {
          [titlePropKey]: showDetails ? id : `Edit (${id})`,
          contentWidth: 1100,
          contentMinHeight: 350,
          bodyStyle: {
            backgroundColor: '#f5f6fA',
          },
          ...containerProps,
        },
      }}
      renderContent={(handleClose) => renderFormComponent?.(handleClose)}
      {...otherProps}
      {...props}
      sx={{
        ...otherProps.sx,
        ...props.sx,
      }}>
      {childrenComp}
    </Button>
  );
};

EditButton.defaultProps = {
  icon: false,
};

export default EditButton;
