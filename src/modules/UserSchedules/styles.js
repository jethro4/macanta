import {experimentalStyled as styled} from '@mui/material/styles';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import {OverflowTip} from '@macanta/components/Tooltip';
import {getFontRelativeSize} from '@macanta/themes/applicationStyles';
import * as DOWorkflowBoardsStyled from '@macanta/modules/DOWorkflowBoards/styles';

export const ItemBodyContainer = styled(Box)`
  display: flex;
`;

export const ItemBodyDataContainer = styled(Box)`
  display: flex;

  & > *:not(:last-child) {
    margin-right: 1.5em;
  }
`;

export const ItemId = styled(Typography)`
  margin-bottom: 10px;
  text-align: left;
  color: ${({theme}) => theme.palette.text.secondary};
  font-size: 0.875em;
  line-height: 0.875em;
`;

export const FieldContainer = styled(Box)`
  min-width: 100px;
  flex-basis: 100%;
  overflow-x: hidden;
  display: flex;
  flex-direction: column;
`;

export const FieldName = styled(OverflowTip)`
  margin-bottom: 4px;
  font-size: 0.8125em;
  font-weight: 700;
  color: ${({theme}) => `${theme.palette.text.blue}`};
`;

export const FieldValue = styled(OverflowTip)`
  font-size: 0.8125em;
`;

export const CalendarHeader = styled(Box)`
  background-color: ${({theme}) => `${theme.palette.common.white}`};
  display: flex;
  align-items: flex-end;
`;

export const CalendarItem = styled(Box)`
  font-size: ${getFontRelativeSize(16)};
  background-color: ${({theme}) => `${theme.palette.common.white}`};
`;

export const CalendarItemHeader = styled(OverflowTip)`
  margin-bottom: ${getFontRelativeSize(8)};
  font-size: ${getFontRelativeSize(14)};
  color: ${({theme}) => `${theme.palette.text.gray}`};
`;

export const CalendarData = styled(Box)``;

export const CalendarDataItem = styled(Box)`
  &:not(:last-child) {
    margin-bottom: ${getFontRelativeSize(6)};
  }
  &:last-child {
    margin-bottom: ${getFontRelativeSize(8)};
  }

  padding: ${getFontRelativeSize(2)} ${getFontRelativeSize(8)};
  border-radius: ${getFontRelativeSize(4)};
`;

export const CalendarDataTitle = styled(OverflowTip)`
  font-size: ${getFontRelativeSize(13)};
`;

export const HighlightedInfoContainer = styled(
  DOWorkflowBoardsStyled.BoardDetailsInfoContainer,
)``;

export const InfoContainer = styled(Box)`
  display: flex;
  align-items: center;
  height: 2rem;
  padding: 0 1rem;

  & > *:not(:last-child) {
    margin-right: 2rem;
  }
`;

export const InfoBox = styled(DOWorkflowBoardsStyled.BoardDetailsInfo)``;

export const InfoLabel = styled(DOWorkflowBoardsStyled.BoardDetailsInfoLabel)``;

export const InfoValue = styled(DOWorkflowBoardsStyled.BoardDetailsInfoValue)``;
