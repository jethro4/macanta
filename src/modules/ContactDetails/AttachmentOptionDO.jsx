import React, {useState} from 'react';
import Modal from '@macanta/components/Modal';
import Checkbox from '@mui/material/Checkbox';
import CloudUploadIcon from '@mui/icons-material/CloudUpload';
import AttachmentsViewer from '@macanta/modules/DataObjectItems/DOItemForms/Attachments/AttachmentsViewer';
import useQuery from '@macanta/hooks/apollo/useQuery';
import {GET_DATA_OBJECT} from '@macanta/graphql/dataObjects';
import * as FormStyled from '@macanta/modules/NoteTaskForms/styles';
import * as Styled from './styles';

const AttachmentOptionDO = ({
  showModal: showModalProp,
  doItemId,
  selectedAttachments: selectedAttachmentsProp,
  onClose,
  onSelect,
}) => {
  const [showModal, setShowModal] = useState(showModalProp);
  const [selectedAttachments, setSelectedAttachments] = useState(
    selectedAttachmentsProp,
  );

  const {loading, data} = useQuery(GET_DATA_OBJECT, {
    variables: {
      itemId: doItemId,
    },
  });

  const attachments = data?.getDataObject?.attachments;

  const handleCloseModal = () => {
    setShowModal(false);

    onClose && onClose();
  };

  const handleSuccess = (...args) => {
    handleCloseModal();

    onSelect(...args);
  };

  const handleAttach = () => {
    onSelect(selectedAttachments, 'do');

    handleCloseModal();
  };

  const renderGridActions = (file, isList) => {
    const isSelected = selectedAttachments.some((f) => f.id === file.id);
    const ActionContainer = !isList
      ? Styled.GridItemCheckboxContainer
      : Styled.ListItemCheckboxContainer;

    return (
      <ActionContainer>
        <Styled.GridItemCheckboxFormControl
          label={!isSelected ? 'Select' : 'Remove'}
          control={
            <Checkbox
              color="primary"
              checked={isSelected}
              onChange={() => {
                if (!isSelected) {
                  setSelectedAttachments(selectedAttachments.concat(file));
                } else {
                  setSelectedAttachments(
                    selectedAttachments.filter((f) => f.id !== file.id),
                  );
                }
              }}
            />
          }
        />
      </ActionContainer>
    );
  };

  const renderListActions = (file) => {
    return renderGridActions(file, true);
  };

  return (
    <Modal
      headerTitle="Attach Files"
      open={showModal}
      onClose={handleCloseModal}
      contentWidth={600}
      contentHeight={'84%'}>
      <Styled.AttachmentsViewerContainer>
        <AttachmentsViewer
          loading={loading}
          attachments={attachments}
          doItemId={doItemId}
          renderGridActions={renderGridActions}
          renderListActions={renderListActions}
          onSuccess={handleSuccess}
        />
      </Styled.AttachmentsViewerContainer>
      <FormStyled.Footer
        style={{
          padding: '1rem',
          margin: 0,
          borderTop: '1px solid #eee',
        }}>
        <FormStyled.FooterButton
          disabled={selectedAttachments.length === 0}
          variant="contained"
          startIcon={<CloudUploadIcon />}
          onClick={handleAttach}
          size="medium">
          Attach
        </FormStyled.FooterButton>
      </FormStyled.Footer>
    </Modal>
  );
};

AttachmentOptionDO.defaultProps = {
  showModal: false,
  selectedAttachments: [],
};

export default AttachmentOptionDO;
