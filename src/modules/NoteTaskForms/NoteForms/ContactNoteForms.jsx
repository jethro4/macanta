import React from 'react';
import noteValidationSchema from '@macanta/validations/note';
import useNoteMutation from './useNoteMutation';
import NoteForms from './NoteForms';
import * as Storage from '@macanta/utils/storage';

const ContactNoteForms = ({
  id,
  contactId,
  title = '',
  note = '',
  tags,
  onSuccess,
}) => {
  const isEdit = !!id;

  const initValues = {
    ...(isEdit ? {id} : {contactId}),
    title,
    note,
    tags,
  };

  const {
    callCreateNoteMutation,
    callUpdateNoteMutation,
    loading,
    error,
  } = useNoteMutation({onSuccess});

  const handleFormSubmit = (values) => {
    if (!isEdit) {
      const session = Storage.getItem('session');

      callCreateNoteMutation({
        variables: {
          createNoteInput: {...values, sessionName: session?.sessionId},
          __mutationkey: 'createNoteInput',
        },
      });
    } else {
      callUpdateNoteMutation({
        variables: {
          updateNoteInput: values,
          __mutationkey: 'updateNoteInput',
        },
      });
    }
  };

  return (
    <NoteForms
      initValues={initValues}
      validationSchema={noteValidationSchema}
      onSubmit={handleFormSubmit}
      error={error}
      loading={loading}
    />
  );
};

ContactNoteForms.defaultProps = {
  tags: [],
};

export default ContactNoteForms;
