import {getDomain} from '@macanta/selectors/user.selector';

describe('User Selector', () => {
  let validDomains;

  beforeEach(() => {
    validDomains = [
      {
        domain: 'macanta.org',
        username: 'pkdevilliers',
        valid: true,
        subdomain: 'em9133',
        legacy: false,
        dns: [
          {
            data: 'u15315898.wl149.sendgrid.net',
            host: 'em9133.macanta.org',
            type: 'cname',
            valid: true,
          },
        ],
      },
    ];
  });

  describe('getDomain()', () => {
    it('happy - get existing domain object by email', () => {
      const email = 'test@macanta.org';

      const actualResult = getDomain({email, validDomains});

      const expectedResult = {
        name: 'macanta.org',
        username: 'pkdevilliers',
        subdomain: 'em9133',
        legacy: false,
        dns: [
          {
            data: 'u15315898.wl149.sendgrid.net',
            host: 'em9133.macanta.org',
            type: 'cname',
            valid: true,
          },
        ],
        valid: true,
        dnsAdded: true,
      };

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - get non-existing domain object by email', () => {
      const email = 'test@bluepeg.org';

      const actualResult = getDomain({email, validDomains});

      const expectedResult = {
        name: 'bluepeg.org',
      };

      expect(actualResult).toEqual(expectedResult);
    });
  });
});
