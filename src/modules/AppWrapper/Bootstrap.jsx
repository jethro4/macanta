import React from 'react';
import CheckAppLocked from './CheckAppLocked';
import LoadAppTimezone from './LoadAppTimezone';
import LoadSentrySettings from './LoadSentrySettings';

const Bootstrap = () => (
  <>
    <CheckAppLocked />
    <LoadSentrySettings />
    <LoadAppTimezone />
  </>
);

export default Bootstrap;
