import {gql} from '@apollo/client';

export default gql`
  query listWidgetsData(
    $appName: String!
    $apiKey: String!
    $type: String!
    $range: String
    $startDate: String
    $endDate: String
    $userId: String!
    $itemId: String
    $queryId: String
    $page: Int
    $limit: Int
    $q: String
    $filter: String
    $order: String
    $orderBy: String
  ) {
    listWidgetsData(
      appName: $appName
      apiKey: $apiKey
      type: $type
      range: $range
      startDate: $startDate
      endDate: $endDate
      userId: $userId
      itemId: $itemId
      queryId: $queryId
      page: $page
      limit: $limit
      q: $q
      filter: $filter
      order: $order
      orderBy: $orderBy
    ) {
      items {
        data {
          id
          isContactField
          fieldKey
          fieldType
          value
          choices
        }
      }
      fields
      total
    }
  }
`;
