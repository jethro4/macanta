Object.defineProperty(window, 'localStorage', {
  value: {
    session: {
      sessionId: 'sample-session-id',
      apiKey: '1552679244144731',
    },
    userDetails: {},
  },
});

Object.defineProperty(window, 'sessionStorage', {
  value: {},
});
