import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import {NON_TEXT_FIELDS} from '@macanta/constants/form';
import {textContent} from '@macanta/utils/react';
import HelperTextBtn from './HelperTextBtn';
import ExpandFieldBtn from './ExpandFieldBtn';
import UrlFieldBtn from './UrlFieldBtn';
import {getFieldOptions} from './helpers';
import * as Styled from './styles';

const displayFormValue = (value) => {
  if (Array.isArray(value)) {
    return value.join(', ');
  }

  return value;
};

const FormField = ({
  children,
  type,
  required,
  label,
  subLabel: subLabelProp,
  labelPosition,
  helperText,
  headingText,
  addDivider,
  style,
  fieldStyle,
  TopRightComp,
  FooterComp,
  timeout,
  readOnly,
  hideExpand,
  className,
  HelperProps,
  renderFieldComponent,
  ...props
}) => {
  const ariaLabel = [type, label || props.placeholder]
    .filter((t) => textContent(t))
    .join(' - ');
  let fieldChildren = children;
  let field;

  const fieldOptions = getFieldOptions(type);
  const subLabel = subLabelProp || fieldOptions.subLabel;
  const isNonTextField = NON_TEXT_FIELDS.includes(type);
  const isTopLabelPosition = labelPosition === 'top' || isNonTextField;

  if (readOnly && !['Radio', 'Checkbox'].includes(type)) {
    field = (
      <Styled.Value type={type}>
        {props.renderValue
          ? props.renderValue(props.value)
          : displayFormValue(props.value)}
      </Styled.Value>
    );
  } else if (renderFieldComponent) {
    field = renderFieldComponent(props);
  } else if (fieldOptions.Component) {
    field = (
      <fieldOptions.Component
        style={fieldStyle}
        readOnly={readOnly}
        {...props}
        {...(!isTopLabelPosition && {
          label,
          placeholder: props.placeholder || '',
          required,
        })}
      />
    );
  }

  if (!field) {
    field = (
      <>
        <Styled.TextField
          timeout={timeout}
          style={fieldStyle}
          {...props}
          {...fieldOptions}
          {...(!isTopLabelPosition && {
            label,
            placeholder: props.placeholder || '',
            required,
          })}
          ariaLabel={ariaLabel}>
          {fieldChildren}
        </Styled.TextField>
        {fieldOptions.multiline && !hideExpand && (
          <ExpandFieldBtn timeout={timeout} {...props} {...fieldOptions}>
            {props.value}
          </ExpandFieldBtn>
        )}
      </>
    );
  }

  const labelComp = isTopLabelPosition && label && (
    <Styled.LabelContainer>
      <Box
        style={{
          display: 'flex',
          flexDirection: 'column',
        }}>
        <Styled.Label variant="caption">{label}</Styled.Label>
        {!!subLabel && (
          <Typography
            sx={{
              color: 'text.secondary',
            }}
            style={{
              fontSize: '0.75rem',
            }}>
            {subLabel}
          </Typography>
        )}
      </Box>
      {required && (
        <Typography
          style={{
            marginLeft: '4px',
            lineHeight: '1.180625rem',
          }}
          component="span"
          color="error">
          *
        </Typography>
      )}
      <HelperTextBtn {...HelperProps}>{helperText}</HelperTextBtn>
    </Styled.LabelContainer>
  );

  const topComp =
    !TopRightComp && type !== 'URL' ? (
      labelComp
    ) : (
      <Styled.TopComp>
        {labelComp}
        <Box
          style={{
            display: 'flex',
            alignItems: 'center',
          }}>
          {type === 'URL' && <UrlFieldBtn value={props.value} />}
          {TopRightComp}
        </Box>
      </Styled.TopComp>
    );

  const headingTextComp = !!headingText && (
    <Styled.HeadingTextContainer>
      <Styled.HeadingText>{headingText}</Styled.HeadingText>
      {<HelperTextBtn>{helperText}</HelperTextBtn>}
    </Styled.HeadingTextContainer>
  );

  const dividerComp = addDivider && <Styled.Divider />;

  return (
    <>
      <Styled.Root style={style} className={className}>
        {headingTextComp}
        {labelPosition === 'top' && topComp}
        {field}
        {labelPosition === 'normal' && type === 'URL' && (
          <Box
            style={{
              position: 'absolute',
              top: 0,
              right: 12,
              bottom: 0,
              display: 'flex',
              alignItems: 'center',
            }}>
            <UrlFieldBtn value={props.value} />
          </Box>
        )}
        {FooterComp}
      </Styled.Root>
      {dividerComp}
    </>
  );
};

FormField.propTypes = {
  type: PropTypes.number,
  labelPosition: PropTypes.string,
};

FormField.defaultProps = {
  type: 'Text',
  labelPosition: 'top',
  size: 'medium',
};

export default FormField;
