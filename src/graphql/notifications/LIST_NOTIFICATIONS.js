import {gql} from '@apollo/client';

export default gql`
  query listNotifications($userId: ID) {
    listNotifications(userId: $userId) {
      items {
        id
        contactId
        userContactId
        email
        name
        message
        meta {
          itemId
          groupId
          groupTitle
        }
        status
        type
        createdDate
      }
      total
    }
  }
`;
