export const getDefaultUserGuide = ({type, subType, doType}) => ({
  guideId: [type, doType, subType].filter((t) => !!t).join('-'),
  type,
  value: '',
  subType,
  ...(doType && {
    meta: [
      {
        name: 'doType',
        value: doType,
      },
    ],
  }),
});

export const getKeyExtractor = (item) => {
  return item.subType;
};
