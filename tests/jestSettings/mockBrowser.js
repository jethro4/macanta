import {setupWorker} from 'msw';
import handlers from './globalHandlers';

export const worker = setupWorker(...handlers);
