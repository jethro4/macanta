import {experimentalStyled as styled} from '@mui/material/styles';

import Page from '@macanta/components/Page';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import * as applicationStyles from '@macanta/themes/applicationStyles';

export const Root = styled(Page)`
  flex-direction: row;
`;

export const FullHeightGrid = styled(Grid)`
  height: 100%;
`;

export const FullHeightFlexGridColumn = styled(Grid)`
  ${applicationStyles.fullHeightFlexColumn}
`;

export const AppBodyContainer = styled(Box)`
  flex-grow: 1;
  ${applicationStyles.fullHeightFlexColumn}
`;

export const AppBody = styled(Box)`
  height: 100%;
  display: flex;
  flex-direction: column;
  padding-bottom: ${({theme}) => theme.spacing(2)};
  overflow-y: auto;
  position: relative;
`;
