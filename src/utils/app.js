import {getSubdomain} from './uri';
import {
  loggedInVar,
  newVersionAvailableVar,
  sessionExpiredVar,
} from '@macanta/graphql/cache/vars';
import * as Storage from './storage';
// import envConfig from '@macanta/config/envConfig';

const isBrowser = typeof window !== 'undefined';

export const getAppInfo = () => {
  const appName = getSubdomain(isBrowser ? window.location.hostname : '');

  const session = Storage.getItem('session');
  const apiKey = session?.apiKey;

  return [appName, apiKey];
};

export const getSessionInfo = () => {
  const session = Storage.getItem('session');
  const expired = sessionExpiredVar();

  return {
    sessionId: session?.sessionId,
    expired,
  };
};

export const isLoggedIn = () => {
  return loggedInVar();
};

export const setIsLoggedIn = (val) => {
  return loggedInVar(val);
};

export const setIsNewVersionAvailable = (val) => {
  return newVersionAvailableVar(val);
};
