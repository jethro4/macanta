import React from 'react';

import ApolloProviderContainer from './ApolloProviderContainer';
import ApolloProviderBody from './ApolloProviderBody';

const ApolloProviderWrapper = ({children}) => {
  return (
    <ApolloProviderContainer>
      <ApolloProviderBody>{children}</ApolloProviderBody>
    </ApolloProviderContainer>
  );
};

export default ApolloProviderWrapper;
