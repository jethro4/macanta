import React, {useState, useEffect} from 'react';
import Typography from '@mui/material/Typography';
import CompanyLogo from '@macanta/modules/CompanyLogo';
import Show from '@macanta/containers/Show';
import FormsLogin from './FormsLogin';
import FormsForgotPassword from './FormsForgotPassword';
import FormsVerificationCode from './FormsVerificationCode';
import FormsResetPassword from './FormsResetPassword';
import FormsForgotPasswordSuccess from './FormsForgotPasswordSuccess';
import {
  loginDisabledMessageVar,
  sessionExpiredVar,
} from '@macanta/graphql/cache/vars';
import {useReactiveVar} from '@apollo/client';
import * as Styled from './styles';
import * as Storage from '@macanta/utils/storage';

const LoginPage = ({onLogin}) => {
  const session = Storage.getItem('session');

  const loginDisabledMessage = useReactiveVar(loginDisabledMessageVar);
  const sessionExpired = useReactiveVar(sessionExpiredVar);

  const [selectedTab, setSelectedTab] = useState('login');
  const [forgotPWParams, setForgotPWParams] = useState();

  const handleShowForm = (key) => (params) => {
    setForgotPWParams(params && {email: params.email, code: params.code});
    setSelectedTab(key);
  };

  useEffect(() => {
    if (session?.lastUrl || session?.loggingOut) {
      Storage.setItem('session', {
        lastUrl: session?.lastUrl,
        loggingOut: false,
      });
    }
  }, []);

  return (
    <Styled.Root>
      <Styled.Content>
        <CompanyLogo />
        <Styled.FormPaper elevation={3}>
          {loginDisabledMessage ? (
            <Typography
              dangerouslySetInnerHTML={{
                __html: loginDisabledMessage,
              }}
            />
          ) : (
            <>
              <Show in={selectedTab === 'login'}>
                <FormsLogin
                  onForgotPassword={handleShowForm('forgotPassword')}
                  onLogin={onLogin}
                />
              </Show>
              <Show removeHidden in={selectedTab === 'forgotPassword'}>
                <FormsForgotPassword
                  onLogin={handleShowForm('login')}
                  onVerificationCode={handleShowForm('verificationCode')}
                />
              </Show>
              <Show removeHidden in={selectedTab === 'verificationCode'}>
                <FormsVerificationCode
                  {...forgotPWParams}
                  onLogin={handleShowForm('login')}
                  onResetPassword={handleShowForm('resetPassword')}
                />
              </Show>
              <Show removeHidden in={selectedTab === 'resetPassword'}>
                <FormsResetPassword
                  {...forgotPWParams}
                  onLogin={handleShowForm('login')}
                  onForgotPWSuccess={handleShowForm('forgotPWSuccess')}
                />
              </Show>
              <Show removeHidden in={selectedTab === 'forgotPWSuccess'}>
                <FormsForgotPasswordSuccess onLogin={handleShowForm('login')} />
              </Show>
            </>
          )}
        </Styled.FormPaper>
        {!loginDisabledMessage && sessionExpired && (
          <Typography
            color="error"
            style={{
              fontSize: '1rem',
              textAlign: 'center',
              marginTop: '2rem',
            }}>
            Your session has expired
          </Typography>
        )}
      </Styled.Content>
    </Styled.Root>
  );
};

export default LoginPage;
