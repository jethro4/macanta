import React from 'react';
import {navigate} from 'gatsby';
import TableRowsIcon from '@mui/icons-material/TableRows';
import SettingCard from '@macanta/modules/AdminSettings/SettingCard';

const CustomTabsCard = () => {
  /* const navigate = useNavigate();*/

  const handleCustomTabs = () => {
    navigate('/app/admin-settings/custom-tabs');
  };

  return (
    <SettingCard
      IconComp={TableRowsIcon}
      title="Custom Tabs"
      description="Create tabs with customized content to include in contact page sidebar tabs"
      onClick={handleCustomTabs}
    />
  );
};

export default CustomTabsCard;
