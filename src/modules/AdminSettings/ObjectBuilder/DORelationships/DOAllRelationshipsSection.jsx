import React from 'react';
import Section from '@macanta/containers/Section';
import FormField from '@macanta/containers/FormField';
import useDORelationships from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDORelationships';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';

const DOAllRelationshipsSection = ({
  selectedRelationships,
  onChange,
  ...props
}) => {
  const allRelationshipsQuery = useDORelationships(null, {
    fetchAll: true,
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const allRelationships = allRelationshipsQuery.data?.listRelationships;

  const handleChange = (val) => {
    onChange(val, allRelationships);
  };

  return (
    <Section
      title="Relationship Options"
      bodyContainerStyle={{
        overflowY: 'auto',
      }}
      {...props}>
      <FormField
        style={{
          padding: '0.5rem 1rem',
          marginBottom: 0,
          overflowY: 'auto',
          height: '100%',
        }}
        required
        type="Checkbox"
        sort="asc"
        options={allRelationships}
        labelKey="role"
        valueKey="id"
        value={selectedRelationships}
        // error={errors.exportTypes}
        onChange={handleChange}
        fullWidth
        size="small"
        variant="outlined"
      />
    </Section>
  );
};

export default DOAllRelationshipsSection;
