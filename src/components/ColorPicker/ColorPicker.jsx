import React, {useCallback} from 'react';
import {HexColorPicker, HexColorInput} from 'react-colorful';
import debounce from 'lodash/debounce';
import useValue from '@macanta/hooks/base/useValue';
import * as Styled from './styles';

export const ColorPicker = ({color, onChange, style}) => {
  const [value, setValue] = useValue(color);

  const handleChange = useCallback(
    debounce(
      (newVal) => {
        setValue(newVal);

        onChange && onChange(newVal);
      },
      200,
      {leading: false, trailing: true},
    ),
    [],
  );

  return (
    <Styled.Root style={style}>
      <HexColorPicker color={value} onChange={handleChange} />
      <Styled.ActionContainer>
        <Styled.InputContainer>
          <HexColorInput
            color={value}
            onChange={handleChange}
            style={{
              borderLeft: `40px solid ${value}`,
              borderLeftColor: value,
            }}
          />
        </Styled.InputContainer>
      </Styled.ActionContainer>
    </Styled.Root>
  );
};

export default ColorPicker;
