import React, {useState} from 'react';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import ErrorIcon from '@mui/icons-material/Error';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Button from '@macanta/components/Button';
import Tooltip from '@macanta/components/Tooltip';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import DataTable from '@macanta/containers/DataTable';
import useQuery from '@macanta/hooks/apollo/useQuery';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import {LIST_VALID_DOMAIN_EMAILS} from '@macanta/graphql/users';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {arrayConcatOrUpdateByKey} from '@macanta/utils/array';
import {checkDomainValid} from '@macanta/selectors/user.selector';

const DEFAULT_DNS_COLUMNS = [
  {
    id: 'valid',
    label: '',
    disableSort: true,
    disableResize: true,
    minWidth: 50,
    maxWidth: 50,
    renderBodyCellValue: ({value}) => {
      return (
        <Tooltip title={value ? 'Valid' : 'Invalid'}>
          {value ? (
            <CheckCircleIcon
              style={{
                color: '#4caf50',
              }}
              size={20}
            />
          ) : (
            <ErrorIcon
              sx={{
                color: 'warning.main',
              }}
              size={20}
            />
          )}
        </Tooltip>
      );
    },
  },
  {
    id: 'host',
    label: 'Host',
    minWidth: 220,
    maxWidth: 270,
  },
  {
    id: 'type',
    label: 'Type',
    minWidth: 90,
    maxWidth: 120,
    transform: ({value}) => {
      return value?.toUpperCase();
    },
  },
  {
    id: 'data',
    label: 'Data',
    minWidth: 220,
    maxWidth: 400,
  },
];

const getUserDomain = (domainEmails, domainName) => {
  return domainEmails?.find((u) => u.domain === domainName);
};

const ValidateDomainEmailForm = ({
  item,
  onClose,
  // refresh
}) => {
  const {displayMessage} = useProgressAlert();

  const [validating, setValidating] = useState(false);

  const validateDomainEmailQuery = useQuery(LIST_VALID_DOMAIN_EMAILS, {
    skip: item.valid,
    variables: {
      ...(!item.dnsAdded && {
        domain: item.name,
      }),
    },
    fetchPolicy: FETCH_POLICIES.CACHE_AND_NETWORK,
    onCompleted: (data) => {
      updateUserDomainCache(data?.listValidDomainEmails);
    },
  });

  const updateUserDomainCache = (domainEmails) => {
    const userDomain = getUserDomain(domainEmails, item.name);

    if (userDomain) {
      const updatedDomainEmails = arrayConcatOrUpdateByKey({
        arr: domainEmails,
        item: userDomain,
        key: 'domain',
        includeAll: true,
      });

      validateDomainEmailQuery.client.writeQuery({
        query: LIST_VALID_DOMAIN_EMAILS,
        data: {
          listValidDomainEmails: updatedDomainEmails,
        },
      });
    }
  };

  const handleValidate = async () => {
    setValidating(true);

    const {data} = await validateDomainEmailQuery.refetch({
      domain: item.name,
    });

    const domainEmails = data?.listValidDomainEmails;

    const userDomain = getUserDomain(domainEmails, item.name);

    if (userDomain?.valid) {
      displayMessage('Validated successfully!');

      onClose();
    } else {
      setValidating(false);
    }
  };

  const domainValid = checkDomainValid(item.dns);

  return (
    <>
      <Box>
        {!item.valid && (
          <Typography
            style={{
              padding: '1rem',
            }}>
            Please add the CNAME records to your Domain Settings and when ready
            please click &rdquo;Validate&rdquo;
          </Typography>
        )}

        <DataTable
          noOddRowStripes
          hidePagination
          noLimit
          columns={DEFAULT_DNS_COLUMNS}
          data={item.dns}
        />
      </Box>
      {!item.valid && (
        <Box
          style={{
            display: 'flex',
            justifyContent: 'flex-end',
            padding: '12px 1rem',
            borderTop: '1px solid #eee',
          }}>
          <Box
            style={{
              flex: 1,
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            {!validating && !domainValid && (
              <Typography
                sx={{
                  color: 'grayDarkest.main',
                }}
                style={{
                  fontSize: '0.75rem',
                  textAlign: 'center',
                }}>
                Please make sure all CNAME records are valid or Contact your
                Support. Thank you.
              </Typography>
            )}
          </Box>
          <Button
            // disabled={!domainValid}
            variant="contained"
            startIcon={<CheckCircleIcon />}
            onClick={handleValidate}
            size="medium">
            Validate
          </Button>
        </Box>
      )}
      <LoadingIndicator fill loading={validateDomainEmailQuery.loading} />
      <LoadingIndicator modal loading={validating} />
    </>
  );
};

export default ValidateDomainEmailForm;
