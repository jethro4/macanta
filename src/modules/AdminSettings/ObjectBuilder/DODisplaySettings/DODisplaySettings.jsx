import React from 'react';
import DOSectionConditionsSection from './DOSectionConditionsSection';
import DOTableColumnsSection from './DOTableColumnsSection';
import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';

const DODisplaySettings = ({hideSectionConditions}) => {
  return (
    <NoteTaskHistoryStyled.FullHeightGrid container>
      <NoteTaskHistoryStyled.FullHeightFlexGridColumn item xs={12} sm={6}>
        <DOTableColumnsSection
          style={{
            marginTop: '1rem',
            marginBottom: '1.5rem',
            flexGrow: 1,
            flexBasis: 0,
            overflowY: 'hidden',
          }}
        />
      </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
      {!hideSectionConditions && (
        <NoteTaskHistoryStyled.FullHeightFlexGridColumn item xs={12} sm={6}>
          <DOSectionConditionsSection
            style={{
              marginTop: '1rem',
              marginBottom: '1.5rem',
              flexGrow: 1,
              flexBasis: 0,
              overflowY: 'hidden',
            }}
          />
        </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
      )}
    </NoteTaskHistoryStyled.FullHeightGrid>
  );
};

export default DODisplaySettings;
