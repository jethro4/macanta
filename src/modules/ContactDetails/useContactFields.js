import useDOFields from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOFields';

const useContactFields = (
  email,
  type = 'contactDefaultFields',
  options = {},
) => {
  const {loading, data, error} = useDOFields(type, {
    userEmail: email,
    ...options,
  });

  return {loading, data, error};
};

export default useContactFields;
