import React from 'react';
import ContactDetails from '@macanta/modules/ContactDetails';
import SidebarAccess from './SidebarAccess';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import ContactQuery from '@macanta/modules/hoc/ContactQuery';
import {recentContactsVar} from '@macanta/graphql/cache/vars';
import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import * as Styled from './styles';
import * as Storage from '@macanta/utils/storage';
import {navigate} from 'gatsby';
const ContactRecords = ({contactId, children}) => {
  const {userId: loggedInUserId} = Storage.getItem('userDetails');

  /* const navigate = useNavigate();*/

  const handleContactViewError = (errMessage) => {
    alert(errMessage);

    navigate(`/app/contact/${loggedInUserId}/notes`, {
      replace: true,
    });
  };

  const handleSearchContacts = (newContact) => {
    if (!newContact) {
      handleContactViewError(
        "You don't have permission to view this contact or it does not exist",
      );

      return;
    } else {
      const recentContacts = recentContactsVar();

      if (newContact.id !== loggedInUserId) {
        const updatedRecentContacts = [newContact].concat(
          recentContacts.filter((contact) => contact.id !== newContact.id),
        );
        const latest10RecentContacts = updatedRecentContacts.slice(0, 10);

        recentContactsVar(latest10RecentContacts);
      }
    }
  };

  return (
    <ContactQuery id={contactId} onCompleted={handleSearchContacts}>
      {({data: updatedContact, loading: contactLoading}) => {
        return updatedContact ? (
          <>
            <ContactDetails contact={updatedContact} loading={contactLoading} />
            <NoteTaskHistoryStyled.FullHeightGrid
              container
              style={{
                minHeight: '450px',
              }}>
              <Styled.GridAccess item xs={12} sm={2.3} lg={2} xl={1.8}>
                <SidebarAccess contactId={contactId} />
              </Styled.GridAccess>
              <NoteTaskHistoryStyled.FullHeightFlexGridColumn
                item
                xs={12}
                sm={9.7}
                lg={10}
                xl={10.2}>
                {children}
              </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
            </NoteTaskHistoryStyled.FullHeightGrid>
          </>
        ) : (
          <LoadingIndicator fill align="top" />
        );
      }}
    </ContactQuery>
  );
};

export default ContactRecords;
