import useQuery from '@macanta/hooks/apollo/useQuery';
import {GET_EMAIL_ACTION} from '@macanta/graphql/automation';

export const FIELD_KEY_MAPPING = {
  queryId: 'id',
  queryType: 'type',
  queryName: 'name',
  queryDescription: 'description',
  queryFromAddress: 'fromAddress',
  queryFromName: 'fromName',
  querySubject: 'subject',
  queryAttachment: 'attachment',
  querySenderSigRelationship: 'signature',
  queryPreviewText: 'previewText',
  emailTemplate: 'emailTemplate',
  EmailId: 'emailId',
  CompiledEmailHTML: 'emailHtml',
  ccEmail: 'ccEmail',
  MergedField: 'mergedField',
  BCC: 'bcc',
};

const useEmailAction = (options) => {
  const query = useQuery(GET_EMAIL_ACTION, {
    keyMapping: FIELD_KEY_MAPPING,
    ...options,
  });

  return query;
};

export default useEmailAction;
