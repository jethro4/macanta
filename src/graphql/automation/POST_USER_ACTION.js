import {gql} from '@apollo/client';
import {USER_ACTIONS_FRAGMENT} from '@macanta/graphql/automation/GET_USER_ACTIONS';

export default gql`
  mutation postUserAction($input: UserActionInput!) {
    postUserAction(input: $input) {
      ...userActionsFragment
    }
  }
  ${USER_ACTIONS_FRAGMENT}
`;
