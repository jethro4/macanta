import React, {useState} from 'react';
import Modal from '@macanta/components/Modal';
import QueryBuilderForms from './QueryBuilderForms';
import AddIcon from '@mui/icons-material/Add';
import Button from '@macanta/components/Button';

const AddQueryBtn = ({onSuccess, ...props}) => {
  const [showModal, setShowModal] = useState(false);

  const handleShowModal = () => {
    setShowModal(true);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  const handleSave = (...args) => {
    onSuccess?.(...args);

    handleCloseModal();
  };

  return (
    <>
      <Button
        onClick={handleShowModal}
        size="small"
        variant="contained"
        startIcon={<AddIcon />}
        {...props}>
        Add Query
      </Button>
      <Modal
        headerTitle={`Add Query`}
        open={showModal}
        onClose={handleCloseModal}
        contentWidth={1400}
        contentHeight={'96%'}>
        <QueryBuilderForms onSave={handleSave} />
      </Modal>
    </>
  );
};

export default AddQueryBtn;
