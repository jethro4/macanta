import React, {forwardRef, useMemo} from 'react';
import {Link} from '@reach/router';
import Button from './Button';

const ButtonLink = ({to, style, ...props}) => {
  const CustomLink = useMemo(
    () =>
      forwardRef((linkProps, ref) => <Link ref={ref} to={to} {...linkProps} />),
    [to],
  );

  return (
    <Button
      component={CustomLink}
      style={{
        display: 'inline',
        ...style,
      }}
      {...props}
    />
  );
};

export default ButtonLink;
