import React from 'react';
import {TASK_TAG} from '@macanta/constants/noteTaskTypes';
import Form from '@macanta/components/Form';
import Typography from '@mui/material/Typography';
import TurnedInIcon from '@mui/icons-material/TurnedIn';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import FormField from '@macanta/containers/FormField';
import SelectUserField from '@macanta/modules/NoteTaskForms/SelectUserField';
import * as Styled from '@macanta/modules/NoteTaskForms/styles';

export const filterTaskTags = (tags) => {
  return tags.filter((v) => !TASK_TAG.TYPES.includes(v.toLowerCase()));
};

const TaskForms = ({
  initValues,
  onSubmit,
  error,
  validationSchema,
  loading,
  size,
  hideSelectUser,
  hideFooter,
  hideTags,
  onChange,
  ...props
}) => {
  const handleSubmitFilterTags = (values) => {
    onSubmit({...values, tags: filterTaskTags(values.tags)});
  };

  const handleChange = (...args) => {
    onChange?.(...args);
  };

  return (
    <>
      <Styled.Root {...props}>
        <Form
          initialValues={initValues}
          validationSchema={validationSchema}
          onSubmit={handleSubmitFilterTags}>
          {({values, errors, handleSubmit, setFieldValue, dirty}) => (
            <>
              {!hideSelectUser && (
                <SelectUserField
                  labelPosition="normal"
                  error={errors.userEmail}
                  onChange={(val, item) => {
                    setFieldValue('userEmail', val);
                    setFieldValue('contactId', item.id);

                    handleChange(val, 'userEmail');
                    handleChange(item.id, 'contactId');
                  }}
                  label="Assign To (Salesferry User)"
                  variant="outlined"
                  value={values.userEmail}
                  fullWidth
                  size={size}
                />
              )}

              <Styled.TextField
                labelPosition="normal"
                error={errors.title}
                onChange={(val) => {
                  setFieldValue('title', val);

                  handleChange(val, 'title');
                }}
                label="Title"
                variant="outlined"
                value={values.title}
                fullWidth
                size={size}
              />

              <Styled.TextField
                date
                labelPosition="normal"
                error={errors.actionDate}
                onChange={(val) => {
                  setFieldValue('actionDate', val);

                  handleChange(val, 'actionDate');
                }}
                label="Due Date"
                variant="outlined"
                value={values.actionDate}
                fullWidth
                size={size}
              />

              <FormField
                type="TextArea"
                labelPosition="normal"
                error={errors.note}
                onChange={(val) => {
                  setFieldValue('note', val);

                  handleChange(val, 'note');
                }}
                label="Description"
                variant="outlined"
                value={values.note}
                fullWidth
                size={size}
              />

              {!hideTags && (
                <Styled.AddTagsField
                  error={errors.tags}
                  onChange={(val) => {
                    setFieldValue('tags', val);

                    handleChange(val, 'tags');
                  }}
                  label="Tags"
                  variant="outlined"
                  defaultTags={filterTaskTags(values.tags)}
                  fullWidth
                  size={size}
                />
              )}

              {!!error && (
                <Typography
                  color="error"
                  variant="subtitle2"
                  align="center"
                  style={{
                    marginTop: '1rem',
                  }}>
                  {error.message}
                </Typography>
              )}

              {!hideFooter && (
                <Styled.Footer>
                  <Styled.FooterButton
                    disabled={!dirty}
                    variant="contained"
                    startIcon={<TurnedInIcon />}
                    onClick={handleSubmit}
                    size={size}>
                    Save
                  </Styled.FooterButton>
                </Styled.Footer>
              )}
            </>
          )}
        </Form>
      </Styled.Root>
      <LoadingIndicator modal loading={loading} />
    </>
  );
};

TaskForms.defaultProps = {
  size: 'medium',
};

export default TaskForms;
