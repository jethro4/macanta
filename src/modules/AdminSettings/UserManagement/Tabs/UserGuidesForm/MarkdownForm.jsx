import React, {useState} from 'react';
import EditIcon from '@mui/icons-material/Edit';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import Button from '@macanta/components/Button';
import Markdown from '@macanta/components/Markdown';
import FormField from '@macanta/containers/FormField';
import HelperTextBtn from '@macanta/containers/FormField/HelperTextBtn';
import MARKDOWN_GUIDE_HTML from '@macanta/fixtures/assets/markdownGuide';
import * as NoteTaskFormsStyled from '@macanta/modules/NoteTaskForms/styles';
import * as applicationStyles from '@macanta/themes/applicationStyles';

const MarkdownForm = ({value: valueProp, onChange}) => {
  const [value, setValue] = useState(valueProp);

  const handleSubmit = () => {
    onChange(value);
  };

  return (
    <Box
      style={{
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
      }}>
      <Box
        style={{
          flex: 1,
          overflowY: 'hidden',
          display: 'flex',
          flexWrap: 'wrap',
        }}>
        <Box
          style={{
            flexGrow: 1,
            flexBasis: 0,
            minWidth: 360,
            minHeight: 300,
            display: 'flex',
            flexDirection: 'column',
            padding: '1rem',
            // backgroundColor: '#f5f6fa',
          }}>
          <Box
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              marginBottom: '12px',
            }}>
            <Typography fontWeight="bold">Edit Markdown</Typography>

            <HelperTextBtn
              PopoverProps={{
                anchorOrigin: {
                  vertical: 'center',
                  horizontal: 'right',
                },
                transformOrigin: {
                  vertical: 'center',
                  horizontal: 'left',
                },
              }}>
              {MARKDOWN_GUIDE_HTML}
            </HelperTextBtn>
          </Box>

          <Box
            style={{
              flex: 1,
            }}>
            <FormField
              autoFocus={!value}
              autoSize
              type="TextArea"
              size="xsmall"
              placeholder="Type markdown here..."
              style={{
                marginBottom: 0,
                height: '100%',
              }}
              sx={{
                '& textarea': {
                  minHeight: 160,
                  backgroundColor: '#f5f6fa',
                  fontSize: '0.875rem !important',
                  lineHeight: '1.2rem !important',
                },
              }}
              hideExpand
              labelPosition="normal"
              value={value}
              onChange={setValue}
              fullWidth
            />
          </Box>
        </Box>

        <Divider
          vertical
          sx={{
            border: 0,
            width: '1px',
            height: '100%',
            backgroundColor: 'border.main',
          }}
        />

        <Box
          style={{
            flexGrow: 1,
            flexBasis: 0,
            maxHeight: '100%',
            display: 'flex',
            flexDirection: 'column',
            padding: '1rem',
          }}
          sx={{
            backgroundColor: 'gray.main',
          }}>
          <Typography
            fontWeight="bold"
            sx={{
              color: 'grayDarkest.main',
            }}
            style={{
              marginBottom: '12px',
            }}>
            Preview:
          </Typography>

          {!value ? (
            <Typography color="#bbb">No Preview</Typography>
          ) : (
            <Box
              style={{
                minHeight: 60,
                minWidth: 250,
                maxWidth: 600,
                border: '1px solid #ccc',
                borderRadius: 4,
                alignSelf: 'flex-start',
                overflowY: 'auto',
              }}
              sx={{
                backgroundColor: 'white',

                '.markdown-container': {
                  margin: '1rem',

                  ' p': {
                    whiteSpace: 'pre',
                  },
                },
              }}>
              <Markdown>{value?.replace(/\n/gi, '\r<p>')}</Markdown>
            </Box>
          )}
        </Box>
      </Box>
      <NoteTaskFormsStyled.Footer
        sx={applicationStyles.footer}
        style={{
          padding: '1rem',
          margin: 0,
          borderTop: '1px solid #eee',
          width: '100%',
          // boxShadow: 'none',
          // backgroundColor: '#f5f6fa',
        }}>
        <Button
          variant="contained"
          startIcon={<EditIcon />}
          onClick={handleSubmit}
          size="medium">
          Update Guide
        </Button>
      </NoteTaskFormsStyled.Footer>
    </Box>
  );
};

export default MarkdownForm;
