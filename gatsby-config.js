if (process.env.STAGING) {
  require('dotenv').config({
    path: `.env.staging`,
  });
} else {
  require('dotenv').config({
    path: `.env.${process.env.NODE_ENV}`,
  });
}

module.exports = {
  pathPrefix: '/',
  siteMetadata: {
    title: `Macanta`,
  },
  plugins: [
    {
      resolve: '@sentry/gatsby',
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/assets/images/favicon.png`, // This path is relative to the root of the site.
      },
    },
    `gatsby-plugin-offline`,
    {
      resolve: `gatsby-alias-imports`,
      options: {
        aliases: {
          '@macanta': `src`,
          '@mui/material': 'node_modules/@material-ui/core',
          '@mui/icons-material': 'node_modules/@material-ui/icons',
          '@mui/x-date-pickers': 'node_modules/@material-ui/lab',
          '@tests': `tests`,
        },
      },
    },
    {
      resolve: 'gatsby-plugin-remove-console',
      options: {
        exclude: ['error', 'warn', 'info'],
      },
    },
    `gatsby-plugin-react-helmet`,
    'gatsby-theme-material-ui',
    'gatsby-plugin-emotion',
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // 'gatsby-theme-apollo',
  ],
};
