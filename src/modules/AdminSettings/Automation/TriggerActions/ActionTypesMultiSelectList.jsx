import React from 'react';
import SettingsIcon from '@mui/icons-material/Settings';
import Switch from '@macanta/components/Forms/ChoiceFields/Switch';
import DataTable from '@macanta/containers/DataTable';
import {ModalButton} from '@macanta/components/Button';
import {findItem} from '@macanta/utils/array';
import {TRIGGER_ACTION_TYPES} from '@macanta/constants/automation';

const ACTION_COLUMNS = [
  {
    id: 'hasAccess',
    label: '',
    disableSort: true,
    disableResize: true,
    minWidth: 80,
    maxWidth: 80,
    renderBodyCellValue: ({item, value, handlers}) => {
      return (
        <Switch
          color="success"
          checked={value}
          TooltipProps={{
            checkedTitle: 'Remove Trigger',
            uncheckedTitle: 'Add Trigger',
          }}
          onChange={(event) => {
            handlers.onSelectActionType(item.type, event.target.checked);
          }}
        />
      );
    },
  },
  {
    id: 'label',
    label: 'Type',
    maxWidth: '55%',
    getBodyCellStyle: ({item}) =>
      !item.hasAccess && {
        color: 'grayDarkest.main',
      },
  },
  {
    id: 'value',
    label: '',
    disableSort: true,
    disableResize: true,
    maxWidth: '45%',
    textStyle: {
      right: 8,
    },
    renderBodyCellValue: ({item, value, handlers}) => {
      const selectedType = findItem(TRIGGER_ACTION_TYPES, {type: item.type});

      return (
        item.hasAccess && (
          <selectedType.selectComponent
            value={value}
            onChange={(val) => handlers.onActionChange(item.type, val)}
          />
        )
      );
    },
  },
  {
    label: '',
    disableSort: true,
    disableResize: true,
    minWidth: 36,
    maxWidth: 36,
    renderBodyCellValue: ({item}) => {
      const selectedType = findItem(TRIGGER_ACTION_TYPES, {type: item.type});

      return (
        item.hasAccess && (
          <ModalButton
            icon
            size="small"
            variant="contained"
            sx={{
              color: 'info.main',
            }}
            TooltipProps={{
              title: 'Settings',
            }}
            ModalProps={{
              contentWidth: 1100,
              contentHeight: '80%',
              bodyStyle: {
                backgroundColor: '#f5f6fA',
              },
              sx: {
                '.MuiBackdrop-root': {
                  backgroundColor: 'rgba(0, 0, 0, 0.7)',
                },
              },
            }}
            renderContent={() => <selectedType.component />}>
            <SettingsIcon />
          </ModalButton>
        )
      );
    },
  },
];

const ActionTypesMultiSelectList = ({
  data,
  onChange,
  onSelectActionType,
  ...props
}) => {
  // const sidebarTabsQuery = useSidebarTabsWithPermission(permission, {
  //   fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  // });

  return (
    <DataTable
      // loading={sidebarTabsQuery.loading}
      columns={ACTION_COLUMNS}
      data={data}
      handlers={{
        onSelectActionType,
        onActionChange: onChange,
      }}
      highlightSelected={false}
      headRowHeight={50}
      bodyRowHeight={50}
      noOddRowStripes
      noLimit
      hidePagination
      rowsPerPage={25}
      rowsPerPageOptions={[25, 50, 100]}
      selectable
      isSelectable={(item) => !item.hasAccess}
      onSelectItem={(item) => {
        onSelectActionType(item.type);
      }}
      {...props}
    />
  );
};

export default ActionTypesMultiSelectList;
