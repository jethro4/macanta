import {experimentalStyled as styled} from '@mui/material/styles';
import Box from '@mui/material/Box';

export const Root = styled(Box)`
  height: 100%;
  padding: ${({theme}) => theme.spacing(2)} 0;
`;
