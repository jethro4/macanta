import React from 'react';
import Switch from '@macanta/components/Forms/ChoiceFields/Switch';
import useMutation from '@macanta/hooks/apollo/useMutation';
import {COMPLETE_TASK} from '@macanta/graphql/notesTasks';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
// import DORelationshipItemsData from '@macanta/modules/DataObjectItems/DOItemForms/Relationships/DORelationshipItemsData';

const TaskSwitch = ({id}) => {
  const [callCompleteTaskMutation, completeTaskMutation] = useMutation(
    COMPLETE_TASK,
    {
      successMessage: 'Task completed',
      update(cache, {data: {completeTask}}) {
        const normalizedId = cache.identify({
          id: completeTask.id,
          __typename: 'TaskScheduleItem',
        });
        cache.evict({id: normalizedId});
        cache.gc();
      },
    },
  );

  const handleCompleteTask = () => {
    callCompleteTaskMutation({
      variables: {
        id,
      },
    });
  };

  return completeTaskMutation.loading ? (
    <LoadingIndicator
      style={{
        marginTop: 8,
        flex: 0.2,
        justifyContent: 'flex-end',
      }}
      transparent
      size={20}
    />
  ) : (
    <Switch
      style={{
        flex: 0.2,
        justifyContent: 'flex-end',
      }}
      labelPlacement="start"
      confirmDialog={{
        title: 'Complete task?',
        description: "You won't be able to undo this action.",
        actionLabel: 'Complete',
        onAction: handleCompleteTask,
      }}
    />
  );
};

export default TaskSwitch;
