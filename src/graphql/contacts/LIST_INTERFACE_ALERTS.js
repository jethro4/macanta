import {gql} from '@apollo/client';

export default gql`
  query listInterfaceAlerts(
    $appName: String!
    $apiKey: String!
    $userId: ID
    $contactId: ID
  ) {
    listInterfaceAlerts(
      appName: $appName
      apiKey: $apiKey
      userId: $userId
      contactId: $contactId
    ) {
      messages
    }
  }
`;
