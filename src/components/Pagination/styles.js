import React from 'react';
import {experimentalStyled as styled} from '@mui/material/styles';

import MUIPagination from '@mui/material/Pagination';
import MUITablePagination from '@mui/material/TablePagination';
import * as applicationStyles from '@macanta/themes/applicationStyles';

const Pagination = ({table, ...props}) => {
  const PaginationComp = !table ? MUIPagination : MUITablePagination;
  return <PaginationComp {...props} />;
};

export const Root = styled(Pagination)`
  height: ${applicationStyles.DEFAULT_FOOTER_HEIGHT - 1}px;
  display: flex;
  align-items: center;
  overflow-y: hidden;
  border: none;

  .MuiToolbar-root {
    min-height: 100%;
  }
`;
