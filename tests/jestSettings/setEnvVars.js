process.env.GATSBY_API_URL = 'http://localhost:3000/graphql';
process.env.AWS_REGION = 'sample-aws-region';
process.env.AWS_AUTHENTICATION_TYPE = 'us-west-1';
process.env.GATSBY_API_KEY = 'API_KEY';
