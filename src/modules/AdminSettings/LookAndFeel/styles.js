import {experimentalStyled as styled} from '@mui/material/styles';
import Box from '@mui/material/Box';

export const LogoPickerContainer = styled(Box)`
  height: 100%;
  display: flex;
  flex-direction: column;
`;

export const ColorPickerContainer = styled(Box)`
  padding: 1rem;

  .react-colorful {
    border-radius: 8px;
    box-shadow: 0 4px 12px #ddd;
  }
`;
