import React, {useState} from 'react';
import {GET_NYLAS_URL} from '@macanta/graphql/admin';
import {useLazyQuery} from '@apollo/client';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import * as FormStyled from '@macanta/modules/NoteTaskForms/styles';
import * as Styled from './styles';
import * as Storage from '@macanta/utils/storage';

const NylasForms = ({onPostpone}) => {
  const userDetails = Storage.getItem('userDetails');

  const [loading, setLoading] = useState(false);

  const [callNylasUrlQuery] = useLazyQuery(GET_NYLAS_URL, {
    onCompleted(data) {
      if (data.getNylasUrl) {
        window.location.href = data.getNylasUrl.url;
      }
    },
    onError() {
      setLoading(false);
    },
  });

  const handleGenerate = () => {
    setLoading(true);

    callNylasUrlQuery({
      variables: {
        email: userDetails.email,
      },
    });
  };

  return (
    <Styled.Root>
      <Styled.Container>
        <Styled.Header>2-Way Email Sync</Styled.Header>
        <Styled.Description>
          Please click `Connect` below to connect your User Email Account to
          Macanta.
        </Styled.Description>
        <Styled.Description>This will give you 2-way sync.</Styled.Description>
        <Styled.Description
          style={{
            marginTop: '1rem',
          }}>
          All emails between you and contacts in Macanta will be displayed in
          the contact&apos;s Communication History, even when sent directly from
          your email, and will also include replies from the contacts back to
          you.
        </Styled.Description>
      </Styled.Container>
      <FormStyled.Footer
        style={{
          padding: '1rem',
          margin: 0,
          borderTop: '1px solid #eee',
        }}>
        <FormStyled.FooterButton
          color="grayDarker"
          onClick={onPostpone}
          size="medium">
          I&apos;ll do that later
        </FormStyled.FooterButton>
        <FormStyled.FooterButton
          variant="contained"
          onClick={handleGenerate}
          size="medium">
          Connect
        </FormStyled.FooterButton>
      </FormStyled.Footer>
      <LoadingIndicator modal loading={loading} />
    </Styled.Root>
  );
};

export default NylasForms;
