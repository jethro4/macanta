import React from 'react';
import ItemActionButtons from '@macanta/modules/AdminSettings/Automation/TriggerActions/ItemActionButtons';
import useFixedValue from '@macanta/hooks/base/useFixedValue';

const useColumns = () => {
  const columns = useFixedValue([
    {
      id: 'name',
      label: 'Name',
      minWidth: 280,
      maxWidth: 400,
    },
    {
      id: 'description',
      label: 'Description',
      maxWidth: '100%',
    },
    {
      id: 'action',
      label: '',
      disableSort: true,
      disableResize: true,
      minWidth: 100,
      maxWidth: 100,
      renderBodyCellValue: ({item}) => {
        return <ItemActionButtons item={item} />;
      },
    },
  ]);

  return columns;
};

export default useColumns;
