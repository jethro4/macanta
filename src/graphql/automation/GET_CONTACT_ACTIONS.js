import {gql} from '@apollo/client';

export const CONTACT_ACTION_FRAGMENT = gql`
  fragment contactActionFragment on ContactActionItem {
    queryId
    queryType
    queryDescription
    queryConnectedDataType
    queryName
    actionType
    noteTitle
    noteTags
    noteText
    taskTitle
    taskText
    taskDate
    assignType
    assignTypeValue
  }
`;

export default gql`
  query getContactActions($id: ID) {
    getContactActions(id: $id) {
      action
      status
      count
      items {
        ...contactActionFragment
      }
    }
  }
  ${CONTACT_ACTION_FRAGMENT}
`;
