import React from 'react';
import pickBy from 'lodash/pickBy';
import isFunction from 'lodash/isFunction';
import useValue from '@macanta/hooks/base/useValue';
import {isNotObject} from '@macanta/utils/value';

const withProps = (WrappedComponent, propsMapping) => {
  if (!propsMapping) {
    throw `Missing "propsMapping" is required`;
  }

  const EnhancedComponent = (props) => {
    const keys = Object.keys(propsMapping);
    const keyPropValues = pickBy(props, (prop) => !isFunction(prop));

    const [derivedProps] = useValue(() => {
      return keys.reduce((acc, key) => {
        let accObj = {...acc};

        const valueOrFunc = propsMapping[key];
        const derivedValue = isFunction(valueOrFunc)
          ? valueOrFunc(props[key], props)
          : valueOrFunc;
        const nonObject = isNotObject(derivedValue);

        Object.assign(accObj, nonObject ? {[key]: derivedValue} : derivedValue);

        return accObj;
      }, {});
    }, [keyPropValues]);

    const getEventProps = () => {
      const funcKeys = Object.keys(pickBy(props, isFunction));

      return funcKeys.reduce((acc, key) => {
        let accObj = {...acc};

        const func = propsMapping[key];

        if (func) {
          accObj[key] = func(props[key]);
        }

        return accObj;
      }, {});
    };

    return (
      <WrappedComponent {...props} {...derivedProps} {...getEventProps()} />
    );
  };

  return EnhancedComponent;
};

export default withProps;
