import useValue from '@macanta/hooks/base/useValue';
import {ACCESS_LEVELS} from '@macanta/constants/accessLevels';
import useSidebarTabs from './useSidebarTabs';

const useSidebarTabsWithPermission = (permission, options) => {
  const tabsQuery = useSidebarTabs({removeEmail: true, ...options});

  const tabs = tabsQuery.data;

  const [data] = useValue(() => {
    return tabs.map((tab) => {
      let groupId;

      switch (tab.type) {
        case 'notes': {
          groupId = 'notes_tab';
          break;
        }
        case 'communication': {
          groupId = 'com_history';
          break;
        }
        default: {
          groupId = tab.id;
        }
      }

      const permissionItem = permission?.tabs?.find(
        (p) => p.groupId === groupId,
      );
      const tabPermission =
        permissionItem?.permission || ACCESS_LEVELS.NO_ACCESS;

      return {
        ...tab,
        ...permissionItem,
        groupId,
        hasAccess: [ACCESS_LEVELS.READ_ONLY, ACCESS_LEVELS.READ_WRITE].includes(
          tabPermission,
        ),
        permission: tabPermission,
        hideButtonValues: getHideButtonValues(
          permission?.permissionMeta,
          tab.id,
        ),
      };
    });
  }, [tabs, permission]);

  return {...tabsQuery, data};
};

const getHideButtonValues = (permissionMeta, id) => {
  const hideButtonValues = [];

  if (permissionMeta?.hideActionButtonsForGroups?.includes(id)) {
    hideButtonValues.push('hideAddButton');
  }

  if (permissionMeta?.hideConnectButtonsForGroups?.includes(id)) {
    hideButtonValues.push('hideConnectButton');
  }

  return hideButtonValues;
};

export default useSidebarTabsWithPermission;
