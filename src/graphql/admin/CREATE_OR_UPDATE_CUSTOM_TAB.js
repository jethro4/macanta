import {gql} from '@apollo/client';
import {CUSTOM_TAB_FRAGMENT} from './fragments';

export default gql`
  mutation createOrUpdateCustomTab(
    $createOrUpdateCustomTabInput: CreateOrUpdateCustomTabInput!
  ) {
    createOrUpdateCustomTab(input: $createOrUpdateCustomTabInput) {
      ...customTabFragment
    }
  }
  ${CUSTOM_TAB_FRAGMENT}
`;
