import React from 'react';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import * as Styled from './styles';

const ExpandItem = ({expanded, onClick}) => {
  const handleClick = () => {
    onClick();
  };

  return (
    <ListItem button onClick={handleClick} disableRipple>
      <ListItemText primary="" />
      <Styled.ExpandIcon expanded={expanded} />
    </ListItem>
  );
};

export default ExpandItem;
