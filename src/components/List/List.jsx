import * as React from 'react';
import * as Styled from './styles';

const List = ({...props}) => <Styled.Root {...props} />;

export default List;
