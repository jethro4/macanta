import React, {forwardRef, useMemo} from 'react';
import ListItem from '@mui/material/ListItem';
import {Link} from '@reach/router';

const ListItemLink = ({to, ...props}) => {
  const CustomLink = useMemo(
    () =>
      forwardRef((linkProps, ref) => <Link ref={ref} to={to} {...linkProps} />),
    [to],
  );

  return <ListItem button component={CustomLink} {...props} />;
};

export default ListItemLink;
