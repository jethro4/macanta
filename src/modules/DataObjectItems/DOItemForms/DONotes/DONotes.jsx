import React, {useState, useLayoutEffect} from 'react';
import Divider from '@mui/material/Divider';
import List from '@macanta/containers/List';
import NoteItem from '@macanta/modules/NoteTaskHistory/NoteItem';
import NoteFormsBtn from '@macanta/modules/NoteTaskForms/NoteFormsBtn';
import Section from '@macanta/containers/Section';
import {mergeAndAddArraysBySameKeys} from '@macanta/utils/array';
import DONoteForms from './DONoteForms';
import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import * as Storage from '@macanta/utils/storage';

const DONotes = ({
  loading,
  data,
  selectedValueNotes,
  onChange,
  style,
  ...props
}) => {
  const [notes, setNotes] = useState([]);

  const handleSuccess = (savedNote) => {
    const note = {
      ...savedNote,
      ...(!savedNote.id && !savedNote.tempId && {tempId: Date.now()}),
    };

    onChange(note);

    if (!savedNote.id && !savedNote.tempId) {
      const userDetails = Storage.getItem('userDetails');

      setNotes((state) =>
        [
          {
            createdBy: `${userDetails.firstName || ''} ${
              userDetails.lastName || ''
            }`.trim(),
            ...note,
          },
        ].concat(state),
      );
    } else {
      setNotes((state) =>
        state.map((n) => {
          if (n.id ? n.id === note.id : n.tempId === note.tempId) {
            return {...n, ...note};
          }

          return n;
        }),
      );
    }
  };

  const getKeyExtractor = (item, index) => item.id || index;

  const renderRow = (item) => {
    return (
      <NoteTaskHistoryStyled.ItemContainer>
        <NoteItem
          item={item}
          NoteFormsBtnComp={
            <NoteFormsBtn
              isEdit={true}
              NoteFormsComp={
                <DONoteForms {...item} onSuccess={handleSuccess} />
              }
            />
          }
        />
        <Divider
          style={{
            backgroundColor: '#fafafa',
          }}
        />
      </NoteTaskHistoryStyled.ItemContainer>
    );
  };

  useLayoutEffect(() => {
    const userDetails = Storage.getItem('userDetails');

    const mergedNotes = mergeAndAddArraysBySameKeys(
      data,
      selectedValueNotes.map((n) => ({
        ...n,
        ...(!n.createdBy &&
          !!n.tempId && {
            createdBy: `${userDetails.firstName || ''} ${
              userDetails.lastName || ''
            }`.trim(),
          }),
      })),
      {
        keysToCheck: ['id', 'tempId'],
        includeAll: true,
      },
    ).filter(({deleted}) => !deleted);

    setNotes(mergedNotes);
  }, [data, selectedValueNotes]);

  return (
    <Section
      loading={loading}
      fullHeight
      headerStyle={{
        backgroundColor: 'white',
      }}
      style={{
        marginTop: 0,
        ...(notes.length && {
          backgroundColor: '#f5f6fa',
        }),
        ...style,
      }}
      title="Notes"
      HeaderRightComp={
        <NoteFormsBtn
          NoteFormsComp={<DONoteForms onSuccess={handleSuccess} />}
        />
      }
      {...props}>
      <List
        hideEmptyMessage={loading}
        data={notes}
        keyExtractor={getKeyExtractor}
        renderRow={renderRow}
      />
    </Section>
  );
};

DONotes.defaultProps = {
  data: [],
  selectedValueNotes: [],
};

export default DONotes;
