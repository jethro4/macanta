import React, {useState, useEffect} from 'react';
import Tooltip, {OverflowTip} from '@macanta/components/Tooltip';
import ConfirmationDialog from '@macanta/containers/ConfirmationDialog';
import * as Styled from './styles';

const Switch = ({
  label,
  labelStyle,
  checked: checkedProp,
  onChange,
  confirmDialog,
  style,
  size,
  className,
  activeColor,
  inactiveColor,
  readOnly,
  disabled,
  labelPlacement,
  TooltipProps,
  ...props
}) => {
  const [checked, setChecked] = useState(checkedProp);
  const [openDialog, setOpenDialog] = useState(false);

  const handleChange = function (event) {
    if (confirmDialog) {
      setOpenDialog(true);
    } else {
      setChecked(event.target.checked);

      onChange && onChange(...arguments);
    }
  };

  const handleCloseDialog = () => {
    setOpenDialog(false);
  };

  useEffect(() => {
    setChecked(checkedProp);

    if (checkedProp) {
      setOpenDialog(false);
    }
  }, [checkedProp]);

  let switchComp = (
    <Styled.Root
      style={style}
      className={className}
      size={size}
      color={checked ? activeColor : inactiveColor}>
      <Styled.FormControlLabel
        labelPlacement={labelPlacement}
        {...(label && {
          label: (
            <OverflowTip
              style={{
                width: '100%',
                ...(labelPlacement === 'start' && {
                  marginRight: 12,
                }),
                ...(labelPlacement === 'end' && {
                  marginLeft: 12,
                }),
                ...labelStyle,
              }}>
              {label}
            </OverflowTip>
          ),
        })}
        control={
          <Styled.Switch
            checked={checked}
            onChange={handleChange}
            disabled={readOnly || disabled}
          />
        }
        {...props}
      />

      {confirmDialog && (
        <ConfirmationDialog
          open={!!openDialog}
          onClose={handleCloseDialog}
          title={confirmDialog.title}
          description={confirmDialog.description}
          actions={[
            {
              label: confirmDialog.actionLabel,
              onClick: confirmDialog.onAction,
            },
          ]}
        />
      )}
    </Styled.Root>
  );

  const tooltipTitle =
    (checked ? TooltipProps?.checkedTitle : TooltipProps?.uncheckedTitle) ||
    TooltipProps?.title;

  if (tooltipTitle) {
    switchComp = (
      <Tooltip {...TooltipProps} title={tooltipTitle}>
        {switchComp}
      </Tooltip>
    );
  }

  return switchComp;
};

Switch.defaultProps = {
  activeColor: 'success',
  inactiveColor: '#333',
  labelPlacement: 'end',
};

export default Switch;
