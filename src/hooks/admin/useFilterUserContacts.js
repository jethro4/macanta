import useUsers from '@macanta/hooks/admin/useUsers';

const useFilterUserContacts = (contacts = []) => {
  const usersQuery = useUsers();

  const updatedUsers = usersQuery.data;

  return contacts.filter(
    (contact) => !!updatedUsers?.some((item) => item.id === contact.contactId),
  );
};

export default useFilterUserContacts;
