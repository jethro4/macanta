import {experimentalStyled as styled} from '@mui/material/styles';

import Grid from '@mui/material/Grid';

export const GridAccess = styled(Grid)`
  height: 100%;
  display: flex;
  flex-direction: column;
`;
