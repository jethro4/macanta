import useQuery from '@macanta/hooks/apollo/useQuery';
import {GET_USER_ACTIONS} from '@macanta/graphql/automation';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import {findItem} from '@macanta/utils/array';

const FIELD_KEY_MAPPING = {
  queryId: 'id',
  queryType: 'type',
  queryName: 'name',
  queryDescription: 'description',
  queryConnectedDataType: 'doTitle',
  queryContactRelationship: 'contactRelationship',
  queryMacantaUser: 'appUser',
  queryUserActionDetails: 'actionDetails',
};

const useUserActions = (options) => {
  const doTypesQuery = useDOTypes({
    removeEmail: true,
    includeContactObject: true,
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const query = useQuery(GET_USER_ACTIONS, {
    keyMapping: FIELD_KEY_MAPPING,
    transformResult: (result) =>
      result?.items.map((item) => {
        const doType = findItem(
          doTypesQuery.result,
          (d) => d.title === item.doTitle,
        );

        return {
          ...item,
          groupId: doType?.id,
        };
      }),
    dependencies: [doTypesQuery.result],
    ...options,
  });

  return query;
};

export default useUserActions;
