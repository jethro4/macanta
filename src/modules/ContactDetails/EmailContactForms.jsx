import React from 'react';
import SendAndArchiveIcon from '@mui/icons-material/SendAndArchive';
import SendIcon from '@mui/icons-material/Send';
import Typography from '@mui/material/Typography';
import Form from '@macanta/components/Form';
import Button from '@macanta/components/Button';
import Tooltip from '@macanta/components/Tooltip';
import EmailAttachments from './EmailAttachments';
import EmailPreview from './EmailPreview';
import EmailSignatureBtn from './EmailSignatureBtn';
import EmailAttachmentOptions from './EmailAttachmentOptions';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import {useMutation} from '@apollo/client';
import {EMAIL} from '@macanta/graphql/admin';
import EmailBodyRTE from './EmailBodyRTE';
import useSignature from '@macanta/hooks/admin/useSignature';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import * as FormStyled from '@macanta/modules/NoteTaskForms/styles';
import * as Styled from './styles';
import * as Storage from '@macanta/utils/storage';

const EmailContactForms = ({to, contactId, onSuccess, ...props}) => {
  const loggedInUserDetails = Storage.getItem('userDetails');

  const signatureQuery = useSignature();
  const {displayMessage} = useProgressAlert();

  const initValues = {
    to,
    from: loggedInUserDetails.email,
    subject: '',
    message: '',
    signature: signatureQuery.data,
    files: [],
  };

  const [callEmailMutation, emailMutation] = useMutation(EMAIL, {
    onCompleted(data) {
      if (data.email?.success) {
        onSuccess();
      }
    },
    onError() {},
  });

  const [callTestEmailMutation, testEmailMutation] = useMutation(EMAIL, {
    onCompleted() {
      displayMessage('Test email sent!');
    },
    onError() {},
  });

  const handleSendEmail = (values) => {
    if (!values.subject) {
      const confirmed = confirm('Send this email without a subject?');

      if (!confirmed) {
        return;
      }
    }
    if (!values.message) {
      const confirmed = confirm('Send this email without a message?');

      if (!confirmed) {
        return;
      }
    }

    const callSendEmail = !values.isTestEmail
      ? callEmailMutation
      : callTestEmailMutation;

    callSendEmail({
      variables: {
        emailInput: {
          userId: loggedInUserDetails.userId,
          sendMethod: 'single',
          mode: 'Live',
          toContacts: !values.isTestEmail
            ? contactId
            : loggedInUserDetails.userId,
          subject: values.subject,
          message: values.message,
          signature: values.signature,
          uploadedFileAttachments: values.files.map((f) => ({
            fileName: f.fileName,
            content: f.thumbnail,
          })),
        },
        __mutationkey: 'emailInput',
      },
    });
  };

  const error = emailMutation.error;

  return (
    <Styled.EmailFormsContainer {...props}>
      {signatureQuery.loading && <LoadingIndicator fill align="top" />}
      {!signatureQuery.loading && (
        <Form
          initialValues={initValues}
          // validationSchema={emailValidationSchema}
          onSubmit={handleSendEmail}>
          {({values, errors, handleChange, setFieldValue, handleSubmit}) => (
            <>
              <Styled.EmailFormsHeaderContainer>
                <Styled.EmailTextContainer>
                  <Styled.EmailTextLabel>To:</Styled.EmailTextLabel>
                  <Styled.EmailTextValue>{values.to}</Styled.EmailTextValue>
                </Styled.EmailTextContainer>

                <Styled.EmailTextField
                  autoFocus
                  noBorder
                  onChange={handleChange('subject')}
                  placeholder="Subject"
                  variant="filled"
                  value={values.subject}
                  fullWidth
                />
              </Styled.EmailFormsHeaderContainer>

              <Styled.EmailRichTextFieldContainer>
                <EmailBodyRTE
                  error={errors.message}
                  onChange={handleChange('message')}
                  placeholder="Type Your Message Here"
                  value={values.message}
                />
              </Styled.EmailRichTextFieldContainer>

              <EmailAttachments
                files={values.files}
                onDelete={(fileId) => {
                  setFieldValue(
                    'files',
                    values.files.filter((file) => fileId !== file.id),
                  );
                }}
              />

              <FormStyled.Footer
                style={{
                  margin: '8px 0',
                  padding: '0 1rem',
                }}>
                {!!error && (
                  <Typography
                    style={{
                      flex: 1,
                      lineHeight: '0.875rem',
                    }}
                    color="error"
                    variant="subtitle2"
                    align="center">
                    {error.message}
                  </Typography>
                )}
                <EmailSignatureBtn
                  value={values.signature}
                  onEdit={(signature) => {
                    setFieldValue('signature', signature);
                  }}
                />
                <EmailAttachmentOptions
                  onSelect={async (data) => {
                    const updatedFiles = values.files.concat(data);

                    setFieldValue('files', updatedFiles);
                  }}
                />
                <EmailPreview values={values} />
                <Tooltip title="Send Me Test Email" placement="top">
                  <div>
                    <Styled.EmailFooterIconButton
                      color="secondary"
                      onClick={() => {
                        setFieldValue('isTestEmail', true);
                        handleSubmit();
                      }}
                      size="small">
                      <SendAndArchiveIcon />
                    </Styled.EmailFooterIconButton>
                  </div>
                </Tooltip>
                <Button
                  style={{
                    marginLeft: '1.5rem',
                  }}
                  // disabled={!values.message}
                  variant="contained"
                  startIcon={<SendIcon />}
                  onClick={() => {
                    setFieldValue('isTestEmail', false);
                    handleSubmit();
                  }}
                  size="medium">
                  Send Email
                </Button>
              </FormStyled.Footer>
            </>
          )}
        </Form>
      )}
      <LoadingIndicator
        modal
        loading={emailMutation.loading || testEmailMutation.loading}
      />
    </Styled.EmailFormsContainer>
  );
};

export default EmailContactForms;
