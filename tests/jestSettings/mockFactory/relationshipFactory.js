import {nextFactoryId} from './factory';

const typename = 'Relationship';

export default (overwrites) => {
  const {id, currentIndex} = nextFactoryId(typename);

  return {
    __typename: typename,
    id,
    role: `Relationship ${currentIndex}`,
    groupId: null,
    description: '',
    exclusive: null,
    limit: null,
    autoAssignLoggedInUser: null,
    autoAssignContact: null,
    ...overwrites,
  };
};
