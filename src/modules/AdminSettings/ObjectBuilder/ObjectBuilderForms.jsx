import React, {useState, useEffect, useMemo} from 'react';
import KeyboardArrowLeftIcon from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@mui/icons-material/KeyboardArrowRight';
import TurnedInIcon from '@mui/icons-material/TurnedIn';
import RestartAltIcon from '@mui/icons-material/RestartAlt';
import {useTheme} from '@mui/material/styles';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Button, {DialogButton} from '@macanta/components/Button';
import ButtonGroup from '@macanta/components/ButtonGroup';
import Form from '@macanta/components/Form';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import Section from '@macanta/containers/Section';
import FormField from '@macanta/containers/FormField';
import Show from '@macanta/containers/Show';
import useDORelationships from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDORelationships';
import useDOFields from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOFields';
import useContactFields from '@macanta/modules/ContactDetails/useContactFields';
import {getSectionsData} from '@macanta/selectors/field.selector';
import useDOSectionConditions from '@macanta/hooks/useDOSectionConditions';
import useDOTypeMutation from '@macanta/hooks/dataObject/useDOTypeMutation';
import objectBuilderValidationSchema from '@macanta/validations/objectBuilder';
import {CONTACT_DEFAULT_FIELDS} from '@macanta/constants/contactFields';
import {AUTO_ASSIGN_OPTIONS} from './DORelationships/DORelationshipForms';
import DORelationships from './DORelationships/DORelationships';
import DOSectionsAndFieldsSection from './DOSectionsAndFields/DOSectionsAndFieldsSection';
import DODisplaySettings from './DODisplaySettings/DODisplaySettings';
import * as DOStyled from '@macanta/modules/DataObjectItems/styles';
import * as FormStyled from '@macanta/modules/NoteTaskForms/styles';

const OBJECT_BUILDER_FORM_TABS = [
  {
    label: 'Relationships',
    value: 'relationships',
  },
  {
    label: 'Sections & Fields',
    value: 'sectionFields',
  },
  {
    label: 'Display Settings',
    value: 'displaySettings',
  },
];

const ObjectBuilderForms = ({item, onSave}) => {
  const theme = useTheme();

  const [step, setStep] = useState(0);

  const selectedTab = OBJECT_BUILDER_FORM_TABS[step].value;

  const relationshipsQuery = useDORelationships(item?.id);
  const doFieldsQuery = useDOFields(item?.id);
  const doTypeMutation = useDOTypeMutation();
  const doSectionConditionsQuery = useDOSectionConditions({
    groupId: item?.id,
    options: {
      skip: !item?.id,
    },
  });
  const contactCustomFieldsQuery = useContactFields(
    null,
    'contactCustomFields',
    {
      removeEmail: true,
    },
  );

  const doSectionConditions = doSectionConditionsQuery.data;

  const isNew = !item;
  const relationships = relationshipsQuery.data?.listRelationships;
  const fields = doFieldsQuery.data?.listDataObjectFields;
  const contactCustomFields =
    contactCustomFieldsQuery.data?.listDataObjectFields;
  const currentIndex = OBJECT_BUILDER_FORM_TABS.findIndex(
    (d) => d.value === selectedTab,
  );
  const isFirstStep = currentIndex === 0;
  const isLastStep = currentIndex === OBJECT_BUILDER_FORM_TABS.length - 1;
  const isBack = !isFirstStep;
  const isNext = !isLastStep;

  const handleSelectStep = (newIndex) => () => {
    setStep(newIndex);
  };

  const handleBack = () => {
    setStep((state) => state - 1);
  };

  const handleNext = () => {
    setStep((state) => state + 1);
  };

  const handleSave = async (values) => {
    const variables = {
      id: values.id,
      name: values.name,
      relationships: values.relationships.map((d) => ({
        role: d.role,
        exclusive: d.exclusive || 'no',
        limit: d.limit || 0,
        autoAssignLoggedInUser: d.autoAssignLoggedInUser || false,
        autoAssignContact: d.autoAssignContact || false,
      })),
      fieldSections: values.sectionsData.map((section, sectionIndex) => ({
        name: section.sectionName,
        position: sectionIndex + 1,
        subSections: section.subGroups.map((subGroup, subGroupIndex) => ({
          name: subGroup.subGroupName,
          position: subGroupIndex + 1,
          fields: subGroup.data.map((d) => ({
            ...(!d.isNew && {
              id: d.id,
            }),
            name: d.name,
            type: d.type === 'DataReference' ? d.dataReference : d.type,
            helpText: d.helpText,
            placeholder: d.placeholder,
            default: d.default,
            required: d.required,
            showInTable: d.showInTable,
            showOrder: d.showOrder,
            choices: d.choices,
            contactSpecificField: d.contactSpecificField,
            headingText: d.headingText,
            addDivider: d.addDivider,
            ...(d.type === 'FileUpload' && {
              fileUpload: {
                multiple: d.fileUpload.multiple,
                mimeTypes: d.fileUpload.mimeTypes,
              },
            }),
          })),
        })),
      })),
    };

    await doTypeMutation.save(
      variables,
      values.fieldsToDelete,
      values.updatedSectionConditions.map((sectionCondition) => ({
        sectionName: sectionCondition.sectionName,
        conditions: sectionCondition.conditions.map((condition) => ({
          name: condition.name,
          logic: condition.logic,
          operator: condition.operator,
          values: condition.values,
          value: condition.value,
        })),
      })),
      values.deletedSectionConditions,
    );

    onSave && onSave();
  };

  useEffect(() => {
    if (selectedTab) {
      setTimeout(function () {
        window.dispatchEvent(new Event('resize'));
      }, 0);
    }
  }, [selectedTab]);

  const initValues = useMemo(() => {
    let sectionsData = [];

    if (fields) {
      sectionsData = getSectionsData(fields);
    }

    return {
      id: item?.id,
      name: item?.title || '',
      relationships:
        relationships?.map((d) => ({
          ...d,
          autoAssignOptions: AUTO_ASSIGN_OPTIONS.filter(
            (o) => !!d[o.value],
          ).map((r) => r.value),
        })) || [],
      sectionsData: sectionsData.map((section) => ({
        id: `section-${section.sectionName}`,
        ...section,
        subGroups: section.subGroups.map((subGroup) => ({
          id: `section-${section.sectionName}-subGroup-${subGroup.subGroupName}`,
          ...subGroup,
          data: subGroup.data.map((d) => ({
            ...d,
            type: d.dataReference ? 'DataReference' : d.type,
            choices: d.dataReference ? [] : d.choices,
          })),
        })),
      })),
      updatedId: null,
      sectionConditions:
        fields && doSectionConditions
          ? doSectionConditions?.map((sectionCondition) => ({
              ...sectionCondition,
              id: `section-${sectionCondition.sectionName}`,
              conditions: sectionCondition.conditions
                .map((condition) => {
                  const id = JSON.stringify(condition);
                  const defaultField = CONTACT_DEFAULT_FIELDS.find(
                    (f) => f.name === condition.name,
                  );
                  let field = {
                    id: defaultField?.name || '',
                    name: defaultField?.name || '',
                  };

                  if (!field.id) {
                    const contactCustomField = contactCustomFields?.find(
                      (f) => f.name === condition.name,
                    );
                    field = {
                      id: contactCustomField?.id || '',
                      name: contactCustomField?.name || '',
                    };
                  }

                  if (!field.id) {
                    const doField = fields.find(
                      (f) => f.name === condition.name,
                    );
                    field = {
                      id: doField?.id || '',
                      name: doField?.name || '',
                    };
                  }

                  return {
                    ...condition,
                    id,
                    fieldId: field.id,
                    name: field.name,
                  };
                })
                .filter((condition) => !!condition.fieldId),
            }))
          : [],
      fieldsToDelete: [],
      updatedSectionConditions: [],
      deletedSectionConditions: [],
    };
  }, [relationships, fields, contactCustomFields, doSectionConditions]);

  let showTab = true;
  let loading = false;

  switch (selectedTab) {
    case 'relationships': {
      showTab = !!relationships;
      break;
    }
    case 'sectionFields': {
      showTab = !!fields;
      break;
    }
    case 'displaySettings': {
      showTab = !!fields && !!doSectionConditions;
      break;
    }
  }

  switch (selectedTab) {
    case 'relationships': {
      loading = relationshipsQuery.loading;
      break;
    }
    case 'sectionFields': {
      loading = doFieldsQuery.loading;
      break;
    }
    case 'displaySettings': {
      loading = doFieldsQuery.loading || doSectionConditionsQuery.loading;
      break;
    }
  }

  showTab = isNew || showTab;
  loading = isNew ? false : loading;

  return (
    <>
      <Form
        initialValues={initValues}
        enableReinitialize={true}
        validationSchema={objectBuilderValidationSchema}
        onSubmit={handleSave}>
        {({
          values,
          errors,
          handleChange,
          dirty,
          handleSubmit,
          setValues,
          setErrors,
          validateForm,
        }) => {
          let errorMessage = '';

          switch (selectedTab) {
            case 'relationships': {
              errorMessage = errors.relationships;
              break;
            }
            case 'sectionFields': {
              errorMessage = errors.sectionsData;
              break;
            }
          }

          if (!errorMessage && doTypeMutation.error) {
            errorMessage = doTypeMutation.error.message;
          }

          return (
            <Section
              loading={loading}
              fullHeight
              headerStyle={{
                // boxShadow: 'none',
                borderBottom: '1px solid #eee',
                // backgroundColor: theme.palette.grayLight.main,
                padding: '4px 1rem',
              }}
              style={{
                margin: 0,
                boxShadow: 'none',
              }}
              bodyStyle={{
                backgroundColor: theme.palette.grayLight.main,
              }}
              HeaderLeftComp={
                <FormField
                  autoFocus={isNew}
                  sx={{
                    '&.MuiTextField-root': {
                      flexDirection: 'row',

                      '.MuiInputBase-input': {
                        width: '220px',
                      },

                      '.MuiFormHelperText-root.Mui-error': {
                        marginLeft: '1rem',
                        marginTop: '10px',
                      },
                    },
                  }}
                  style={{
                    margin: 0,
                  }}
                  InputProps={{
                    style: {
                      backgroundColor: 'white',
                    },
                  }}
                  labelPosition="normal"
                  onChange={handleChange('name')}
                  label="Data Object Name"
                  value={values.name}
                  error={errors.name}
                  fullWidth
                  size="xsmall"
                />
              }
              HeaderRightComp={
                <ButtonGroup selectedButtonIndex={step} variant="text">
                  {OBJECT_BUILDER_FORM_TABS.map((tab, index) => {
                    return (
                      <DOStyled.DOItemNavBtn
                        key={tab.value}
                        onClick={handleSelectStep(index)}
                        {...(isNew &&
                          index !== step && {
                            disabled: true,
                          })}>
                        {tab.label}
                      </DOStyled.DOItemNavBtn>
                    );
                  })}
                </ButtonGroup>
              }
              FooterComp={
                <FormStyled.Footer
                  style={{
                    padding: '12px 1rem',
                    margin: 0,
                    borderTop: '1px solid #eee',
                    width: '100%',
                    backgroundColor: '#f5f6fa',
                    justifyContent: 'space-between',
                  }}>
                  <Box
                    style={{
                      width: 320,
                      marginRight: 50,
                      display: 'flex',
                    }}>
                    <DialogButton
                      disabled={!dirty}
                      variant="outlined"
                      startIcon={<RestartAltIcon />}
                      DialogProps={{
                        title: 'Are you sure?',
                        description: [
                          `This will reset ALL changes made.`,
                          'Do you want to proceed?',
                        ],
                        onActions: (handleClose) => {
                          return [
                            {
                              label: 'Reset All',
                              onClick: () => {
                                setValues(initValues);
                                setErrors({});
                                setStep(0);
                                handleClose();
                              },
                            },
                          ];
                        },
                      }}>
                      Reset All
                    </DialogButton>
                    {isBack && (
                      <Button
                        style={{
                          marginLeft: '1rem',
                        }}
                        variant="contained"
                        startIcon={<KeyboardArrowLeftIcon />}
                        onClick={handleBack}
                        size="medium">
                        {`Back - ${
                          OBJECT_BUILDER_FORM_TABS[currentIndex - 1].label
                        }`}
                      </Button>
                    )}
                  </Box>
                  {!!errorMessage && (
                    <Typography
                      color="error"
                      variant="subtitle2"
                      align="center">
                      {errorMessage}
                    </Typography>
                  )}
                  <Box
                    style={{
                      width: 370,
                      display: 'flex',
                      justifyContent: 'flex-end',
                    }}>
                    {isNext && (
                      <Button
                        disabled={isNew && !dirty}
                        variant="contained"
                        endIcon={<KeyboardArrowRightIcon />}
                        onClick={async () => {
                          if (isNew) {
                            const formErrors = await validateForm(values);

                            switch (selectedTab) {
                              case 'relationships': {
                                if (!formErrors.relationships) {
                                  handleNext();
                                }
                                break;
                              }
                              case 'sectionFields': {
                                if (!formErrors.sectionsData) {
                                  handleNext();
                                }
                                break;
                              }
                            }
                          } else {
                            handleNext();
                          }
                        }}
                        size="medium">
                        {`Next - ${
                          OBJECT_BUILDER_FORM_TABS[currentIndex + 1].label
                        }`}
                      </Button>
                    )}
                    {((isNew && isLastStep) || !isNew) && (
                      <Button
                        style={{
                          marginLeft: '1rem',
                        }}
                        disabled={!dirty}
                        variant="contained"
                        startIcon={<TurnedInIcon />}
                        onClick={handleSubmit}
                        size="medium">
                        {isNew ? 'Create Object' : 'Save Changes'}
                      </Button>
                    )}
                  </Box>
                </FormStyled.Footer>
              }>
              {showTab && (
                <>
                  <Show removeHidden in={selectedTab === 'relationships'}>
                    <DORelationships />
                  </Show>
                  <Show removeHidden in={selectedTab === 'sectionFields'}>
                    <DOSectionsAndFieldsSection />
                  </Show>
                  <Show removeHidden in={selectedTab === 'displaySettings'}>
                    <DODisplaySettings />
                  </Show>
                </>
              )}
            </Section>
          );
        }}
      </Form>
      <LoadingIndicator modal loading={doTypeMutation.saving} />
    </>
  );
};

export default ObjectBuilderForms;
