import React, {useState} from 'react';
import Box from '@mui/material/Box';
import TurnedInIcon from '@mui/icons-material/TurnedIn';
import Button from '@macanta/components/Button';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import SettingInfo from '@macanta/modules/UserSettings/SettingInfo';
import useAppSettings from '@macanta/hooks/admin/useAppSettings';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import {useMutation} from '@apollo/client';
import {GET_APP_SETTINGS, UPDATE_APP_SETTINGS} from '@macanta/graphql/app';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {getAppInfo} from '@macanta/utils/app';
import {sortArrayByObjectKey, sortArrayByPriority} from '@macanta/utils/array';
import ChoicesList from '@macanta/modules/AdminSettings/ObjectBuilder/DOSectionsAndFields/ChoicesList';
import * as NoteTaskFormsStyled from '@macanta/modules/NoteTaskForms/styles';
import * as applicationStyles from '@macanta/themes/applicationStyles';

const [appName] = getAppInfo();

const EMAIL_FORMAT_OPTIONS = [
  {
    label: 'Contact Id',
    value: 'ContactId',
  },
  {
    label: 'First Name',
    value: 'FirstName',
  },
  {
    label: 'Last Name',
    value: 'LastName',
  },
];

const EMAIL_FORMAT_VALUES = EMAIL_FORMAT_OPTIONS.map((o) => o.value);
const DEFAULT_EMAIL_FORMAT = `${EMAIL_FORMAT_VALUES.join('')}@${appName}.com`;
const DEFAULT_SUFFIX = `@${appName}.com`;

const EmailFormatForm = ({onSave}) => {
  const {displayMessage} = useProgressAlert();

  const appSettingsQuery = useAppSettings({
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const generatedEmailFormat = appSettingsQuery.data?.generatedEmailFormat;

  const [emailFormat, setEmailFormat] = useState(
    generatedEmailFormat || DEFAULT_EMAIL_FORMAT,
  );

  const selectedOptions = sortArrayByObjectKey(
    EMAIL_FORMAT_VALUES.filter((val) =>
      emailFormat.includes(val),
    ).map((val) => ({val, index: emailFormat.indexOf(val)})),
    'index',
  ).map((item) => item.val);
  const emailFormatOptions = sortArrayByPriority(
    EMAIL_FORMAT_OPTIONS,
    'value',
    selectedOptions,
  );

  const [callUpdateAppSettings, updateAppSettingsMutation] = useMutation(
    UPDATE_APP_SETTINGS,
    {
      onError() {},
      onCompleted: async (data) => {
        if (data?.updateAppSettings?.success) {
          displayMessage('Saved successfully!');
        }
      },
    },
  );

  const handleFormatChange = (optionsVal) => {
    setEmailFormat(`${optionsVal.join('')}${DEFAULT_SUFFIX}`);
  };

  const handleSave = async () => {
    const {data} = await callUpdateAppSettings({
      variables: {
        updateAppSettingsInput: {
          type: 'AutoGenerateEmailFormat',
          value: emailFormat,
        },
        __mutationkey: 'updateAppSettingsInput',
      },
    });

    if (data?.updateAppSettings?.success) {
      appSettingsQuery.client.writeQuery({
        query: GET_APP_SETTINGS,
        data: {
          getAppSettings: {
            __typename: 'AppSettings',
            ...appSettingsQuery.data,
            generatedEmailFormat: emailFormat,
          },
        },
      });

      onSave();
    }
  };

  return (
    <>
      <Box
        style={{
          display: 'flex',
          flexDirection: 'column',
        }}>
        <Box
          style={{
            padding: '1rem',
          }}>
          <Box>
            <ChoicesList
              label="Options"
              data={emailFormatOptions}
              onChange={handleFormatChange}
              hideSearch
              hideDelete
            />
          </Box>

          <SettingInfo
            style={{
              margin: '2rem 0 1rem',
            }}
            label="Generated Format"
            value={emailFormat}
          />
        </Box>

        <NoteTaskFormsStyled.Footer
          sx={applicationStyles.footer}
          style={{
            padding: '1rem',
            margin: 0,
            borderTop: '1px solid #eee',
            width: '100%',
            backgroundColor: 'white',
          }}>
          <Button
            disabled={emailFormat === generatedEmailFormat}
            variant="contained"
            startIcon={<TurnedInIcon />}
            onClick={handleSave}
            size="medium">
            Save
          </Button>
        </NoteTaskFormsStyled.Footer>
      </Box>
      <LoadingIndicator modal loading={updateAppSettingsMutation.loading} />
    </>
  );
};

export default EmailFormatForm;
