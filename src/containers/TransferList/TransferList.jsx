import React, {useState, useEffect} from 'react';
import isEqual from 'lodash/isEqual';
import KeyboardArrowRightIcon from '@mui/icons-material/KeyboardArrowRight';
import Button from '@macanta/components/Button';
import Tooltip from '@macanta/components/Tooltip';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import {transformValue} from '@macanta/components/Forms/InputFields/helpers';
import useTransformChoices from '@macanta/hooks/base/useTransformChoices';
import useDeepCompareNextEffect from '@macanta/hooks/base/useDeepCompareNextEffect';
import useNextEffect from '@macanta/hooks/base/useNextEffect';
import {sortArrayByPriority, mergeArraysBySameKeys} from '@macanta/utils/array';
import SideList from './SideList';
import SelectedList from './SelectedList';
import * as Styled from './styles';

const not = (a, b) => {
  return a.filter(({value: aVal}) => !b.includes(aVal));
};

const intersection = (a, b) => {
  return a.filter(({value: aVal}) => b.includes(aVal));
};

const TransferList = ({
  options: optionsProp,
  selectedItems: selectedItemsProp,
  emptyMessage,
  hideEmptyMessage,
  onChangeFields,
  style,
  labelKey,
  valueKey,
  loading,
  timeout,
  error,
}) => {
  const options = useTransformChoices({
    options: optionsProp,
    labelKey,
    valueKey,
  });

  const [leftCheckedVal, setLeftChecked] = useState([]);
  const [left, setLeft] = useState(
    not(options, transformValue(selectedItemsProp, true)),
  );
  const [right, setRight] = useState(
    sortArrayByPriority(
      intersection(options, transformValue(selectedItemsProp, true)),
      'value',
      selectedItemsProp,
    ),
  );
  const [filteredLeft, setFilteredLeft] = useState(left);
  const [filteredRight, setFilteredRight] = useState(right);
  const [leftSearch, setLeftSearch] = useState('');
  const [rightSearch, setRightSearch] = useState('');

  const leftChecked = intersection(options, leftCheckedVal);

  const handleAllLeft = () => {
    if (!rightSearch) {
      setLeft(options);
      setRight([]);
    } else {
      const filteredItems = sortArrayByPriority(
        filteredRight.concat(left),
        'label',
        options.map((item) => item.label),
      );

      setLeft(filteredItems);
      setRight(
        not(
          right,
          filteredItems.map(({value}) => value),
        ),
      );
    }

    setLeftChecked([]);
  };

  const handleAllRight = () => {
    if (!leftSearch) {
      setRight(filteredRight.concat(filteredLeft.filter((d) => !!d.value)));
      setLeft([]);
    } else {
      const filteredItems = filteredLeft.filter((d) => !!d.value).concat(right);

      setRight(filteredItems);
      setLeft(
        not(
          left,
          filteredItems.map(({value}) => value),
        ),
      );
    }

    setLeftChecked([]);
  };

  const handleCheckedLeft = () => {
    setRight(right.concat(leftChecked));
    setLeftChecked([]);
    setLeft(not(left, leftCheckedVal));
  };

  const handleDelete = (id) => {
    setLeft(
      sortArrayByPriority(
        left.concat(intersection(options, [id])),
        'label',
        options.map((item) => item.label),
      ),
    );
    setRight(not(right, [id]));
  };

  const handleLeftSearch = (searchVal) => {
    setLeftSearch(searchVal);
  };

  const handleRightSearch = (searchVal) => {
    setRightSearch(searchVal);
  };

  useEffect(() => {
    setFilteredLeft(
      left.filter(
        ({isSubheader, label}) =>
          !leftSearch ||
          !!isSubheader ||
          label.toLowerCase().includes(leftSearch.toLowerCase()),
      ),
    );
  }, [left, leftSearch]);

  useEffect(() => {
    setFilteredRight(
      right.filter(
        ({label}) =>
          !rightSearch ||
          label.toLowerCase().includes(rightSearch.toLowerCase()),
      ),
    );
  }, [right, rightSearch]);

  useNextEffect(() => {
    if (options) {
      setLeft(
        not(
          options,
          right.map(({value}) => value),
        ),
      );
      if (options.length) {
        setRight(
          mergeArraysBySameKeys(right, options, {
            keysToCheck: ['value'],
            keysToMerge: ['label'],
          }),
        );
      }
    }
  }, [options]);

  useDeepCompareNextEffect(() => {
    if (
      !isEqual(
        selectedItemsProp,
        right.map(({value}) => value),
      )
    ) {
      const updatedSelectedItems = transformValue(selectedItemsProp, true);

      setLeft(not(options, updatedSelectedItems));
      setRight(
        sortArrayByPriority(
          intersection(options, updatedSelectedItems),
          'value',
          updatedSelectedItems,
        ),
      );
    }
  }, [options, selectedItemsProp]);

  useDeepCompareNextEffect(() => {
    setTimeout(() => {
      if (
        !isEqual(
          selectedItemsProp,
          right.map(({value}) => value),
        )
      ) {
        onChangeFields(right.map(({value}) => value));
      }
    }, timeout);
  }, [right]);

  return (
    <Styled.Root style={style}>
      <SideList
        options={filteredLeft}
        search
        checked={leftCheckedVal}
        onToggle={setLeftChecked}
        onSearch={handleLeftSearch}
        hideEmptyMessage={hideEmptyMessage}
        emptyMessage={leftSearch ? 'No items found' : 'No items available'}
      />
      <Styled.ButtonsContainer>
        <Tooltip title="Transfer All" placement="bottom">
          <div>
            <Button
              sx={{my: 0.5}}
              color="info"
              variant="outlined"
              size="small"
              onClick={handleAllRight}
              disabled={filteredLeft.length === 0}
              aria-label="move all right">
              ≫
            </Button>
          </div>
        </Tooltip>
        <Tooltip title="Select" placement="top">
          <div>
            <Button
              sx={{my: 1}}
              variant="contained"
              size="small"
              onClick={handleCheckedLeft}
              disabled={leftCheckedVal.length === 0}
              aria-label="move selected right">
              <KeyboardArrowRightIcon
                style={{
                  fontSize: 28,
                }}
              />
            </Button>
          </div>
        </Tooltip>
        <Tooltip title="Remove All" placement="top">
          <div>
            <Button
              sx={{my: 0.5}}
              color="info"
              variant="outlined"
              size="small"
              onClick={handleAllLeft}
              disabled={filteredRight.length === 0}
              aria-label="move all left">
              ≪
            </Button>
          </div>
        </Tooltip>
      </Styled.ButtonsContainer>
      <SelectedList
        data={filteredRight}
        search
        onSearch={handleRightSearch}
        onSort={setRight}
        onDelete={handleDelete}
        hideEmptyMessage={hideEmptyMessage}
        emptyMessage={rightSearch ? 'No items found' : emptyMessage}
        error={error}
      />
      <LoadingIndicator fill backdrop="light" loading={loading} />
    </Styled.Root>
  );
};

TransferList.defaultProps = {
  loading: false,
  selectedItems: [],
  labelKey: 'label',
  valueKey: 'value',
  timeout: 0,
};

export default TransferList;
