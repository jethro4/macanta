import {experimentalStyled as styled} from '@mui/material/styles';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import FormControlLabel from '@mui/material/FormControlLabel';
import CardContent from '@mui/material/CardContent';
import Button, {IconButton} from '@macanta/components/Button';
import {SubSection} from '@macanta/containers/Section';
import TextFieldComp from '@macanta/components/Forms';
import * as applicationStyles from '@macanta/themes/applicationStyles';

export const DetailsBody = styled(Box)`
  padding: 0px ${({theme}) => theme.spacing(2)};
`;

export const DetailsBodyHeader = styled(Box)`
  padding: ${({theme}) => theme.spacing(2)} 0px;
`;

export const DetailLabel = styled(Typography)`
  color: ${({theme}) => `${theme.palette.text.blue}`};
  font-weight: 700;
  margin-right: 5px;
  max-width: 50%;
`;

export const DetailValue = styled(Typography)`
  ${({disableOneLine}) => !disableOneLine && applicationStyles.oneLineText}
`;

export const BackgroundBox = styled(Box)`
  background-color: #f5f6fa;
  border-bottom: 1px solid ${({theme}) => `${theme.palette.text.blue}`};
  border-top: 1px solid ${({theme}) => `${theme.palette.text.blue}`};
  display: flex;
  justify-content: space-between;
  padding: 10px;
  width: 100%;
`;

export const Root = styled(Box)``;

export const HeaderRightContainer = styled(Box)`
  display: flex;
  align-items: center;
`;

export const InterfaceAlertsContainer = styled(Box)`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 2.5rem;
  margin: 0 2rem;
  flex: 1;
  overflow: hidden;
`;

export const InterfaceAlertsBox = styled(Box)`
  box-sizing: border-box;
  background-color: #f5f6fa;
  border-radius: 4px;
  display: flex;
  align-items: center;
  height: 2rem;
  padding: 0 1rem;
  max-width: 60vw;
`;

export const InterfaceAlert = styled(Typography)`
  font-size: 0.8125rem;
  color: #e84118;
  ${applicationStyles.oneLineText}
`;

export const ShowMoreBtn = styled(Button)``;

export const SectionGroup = styled(Box)`
  position: relative;
  padding: ${({theme}) => theme.spacing(2)} 0px ${({theme}) => theme.spacing(4)};
  height: 100%;
  min-height: 216px;
`;

export const SubSectionGroup = styled(SubSection)`
  margin-top: 0;
  margin-left: 0;
`;

export const EmailFormsContainer = styled(Box)`
  flex: 1;
  display: flex;
  flex-direction: column;
  background-color: white;
`;

export const EmailFormsHeaderContainer = styled(Box)`
  display: flex;
  flex-direction: column;
  background-color: ${({theme}) => theme.palette.grayLight.main};
  border-bottom: 1px solid #e8e8e8;
`;

export const EmailFormsBodyContainer = styled(Box)`
  flex: 1;
`;

export const EmailFormsFooterContainer = styled(Box)`
  padding: ${({theme}) => theme.spacing(2)};
`;

export const EmailTextContainer = styled(Box)`
  height: 3rem;
  margin: 0 1rem;
  cursor: not-allowed;
  border-bottom: 1px solid #e8e8e8;
  display: flex;
  align-items: center;
`;

export const EmailTextLabel = styled(Typography)`
  color: #9c9c9f;
  margin-right: 0.5rem;
`;

export const EmailTextValue = styled(Typography)`
  color: #aaa;
`;

export const EmailTextField = styled(TextFieldComp)`
  .MuiInputBase-root {
    height: auto !important;
    min-height: 3rem !important;

    &::after {
      border-bottom: none;
    }

    ${({noBorder}) =>
      !noBorder &&
      `

      &::before {
        display: block;
        border-bottom: 1px solid #ddd !important;
        left: 1rem;
        right: 1rem;
        z-index: 1;
      }
    `}

    .MuiInputBase-input {
      padding: 0 1rem !important;
      display: flex !important;
      flex-direction: column !important;
      justify-content: center;
      height: auto !important;
      min-height: 100% !important;
    }
  }

  .MuiFilledInput-root {
    height: 3rem;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
  }

  .MuiFilledInput-input {
    padding-left: 1rem;

    &,
    &:focus {
      background-color: ${({theme}) => theme.palette.grayLight.main} !important;

      .MuiFilledInput-input {
        background-color: ${({theme}) =>
          theme.palette.grayLight.main} !important;
      }
    }

    &:hover {
      background-color: #eee !important;
    }
  }
`;

export const EmailRichTextField = styled(TextFieldComp)`
  flex: 1;
`;

export const EmailRichTextFieldContainer = styled(Box)`
  flex: 1;
  display: flex;
  flex-direction: column;
  border-bottom: 1px solid #f1f1f1;
`;

export const EmailFooterIconButton = styled(IconButton)`
  padding: ${({theme}) => theme.spacing(0.5)};
  margin-left: ${({theme}) => theme.spacing(1)};
`;

export const EmailSignatureContainer = styled(Box)`
  padding: 1rem;

  & p {
    margin: 0;
  }
`;

export const ContactHeaderInfoContainer = styled(Box)`
  display: flex;
  align-items: center;

  & .MuiButton-root {
    padding: 0;
  }
`;

export const GridItemCheckboxContainer = styled(Box)`
  border-top: 1px solid #eee;
`;

export const GridItemCheckboxFormControl = styled(FormControlLabel)`
  width: 100%;
  margin: 0;
  justify-content: center;

  & .MuiFormControlLabel-label {
    font-size: 0.875rem;
    color: #888;
  }
`;

export const ListItemCheckboxContainer = styled(Box)`
  position: absolute;
  top: 50%;
  transform: translateY(-50%);
  right: 1rem;
  border-radius: 4px;
  overflow: hidden;
`;

export const AttachmentsViewerContainer = styled(Box)`
  flex: 1;
  overflow: auto;
`;

export const EmailAttachmentCardContent = styled(CardContent)`
  ${applicationStyles.oneLineText}
  font-size: 0.625rem;
  width: 140px;
  text-align: center;
`;

export const EmailAttachmentLabel = styled(Typography)`
  word-break: break-all;
  font-size: 0.625rem;
  text-align: center;
`;
