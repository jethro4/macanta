import React from 'react';
import MUISlide from '@mui/material/Slide';
import Container from './Container';

const Slide = ({children, ...props}) => {
  return (
    <MUISlide {...props}>
      <Container>{children}</Container>
    </MUISlide>
  );
};

export default Slide;
