import {gql} from '@apollo/client';
import {formatVariables} from '@macanta/modules/AdminSettings/UserManagement/UserPermissionsTemplateForm/helpers';
import useMutation from '@macanta/hooks/apollo/useMutation';
import {
  // LIST_PERMISSION_TEMPLATES,
  CREATE_OR_UPDATE_PERMISSION_TEMPLATE,
} from '@macanta/graphql/permissions';

const usePermissionTemplateMutation = (item, options = {}) => {
  const mutation = useMutation(CREATE_OR_UPDATE_PERMISSION_TEMPLATE, {
    formatVariables,
    update(cache, {data: {createOrUpdatePermissionTemplate}}) {
      cache.modify({
        ...(item?.id && {id: cache.identify(createOrUpdatePermissionTemplate)}),
        fields: {
          listPermissionTemplates(existingItems = []) {
            // const newItemRef = cache.writeQuery({
            //   data: {
            //     listPermissionTemplates: createOrUpdatePermissionTemplate,
            //   },
            //   query: LIST_PERMISSION_TEMPLATES,
            //   variables: {},
            // });
            const newItemRef = cache.writeFragment({
              data: createOrUpdatePermissionTemplate,
              fragment: gql`
                fragment NewPermissionTemplate on PermissionTemplate {
                  id
                  name
                  description
                  status
                }
              `,
            });

            return [...existingItems, newItemRef];
          },
        },
      });
    },
    ...options,
  });

  return mutation;
};

export default usePermissionTemplateMutation;
