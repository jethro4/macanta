import React from 'react';
import DataTable from '@macanta/containers/DataTable';
import ItemButtons from './ItemButtons';

const CUSTOM_TABS_COLUMNS = [
  {
    id: 'title',
    label: 'Title',
    maxWidth: '100%',
  },
];

const CustomTabsTable = ({data, ...props}) => {
  return (
    <DataTable
      fullHeight
      columns={CUSTOM_TABS_COLUMNS}
      data={data}
      order="desc"
      orderBy="created"
      rowsPerPage={25}
      rowsPerPageOptions={[25, 50, 100]}
      renderActionButtons={(actionBtnProps) => (
        <ItemButtons {...actionBtnProps} />
      )}
      actionColumn={{
        show: true,
        style: {
          minWidth: 100,
        },
      }}
      {...props}
    />
  );
};

CustomTabsTable.defaultProps = {
  data: [],
};

export default CustomTabsTable;
