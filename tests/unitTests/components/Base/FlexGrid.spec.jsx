import React from 'react';
import range from 'lodash/range';
import {renderApp} from '@tests/jestSettings/utils/renderUtils';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import FlexGrid, {calculateGridInfo} from '@macanta/components/Base/FlexGrid';
import {hasTexts} from '@tests/jestSettings/utils/assertUtils';

const setup = async (totalItems, props) => {
  const nums = range(1, totalItems + 1);

  const renderItem = (label) => (
    <Box>
      <Typography>{label}</Typography>
    </Box>
  );

  renderApp(
    <FlexGrid {...props}>
      {nums.map((num) => renderItem(`Test ${num}`))}
    </FlexGrid>,
  );
};

describe('FlexGrid', () => {
  describe('Render Data', () => {
    describe('2 columns - 6 total items', () => {
      const total = 6;
      const cols = 2;

      it('show all data sorted by columns and rows', async () => {
        setup(total, {cols});

        await hasTexts('.grid-item', [
          'Test 1',
          'Test 2',
          'Test 3',
          'Test 4',
          'Test 5',
          'Test 6',
        ]);

        await hasTexts('.grid-row-0', ['Test 1', 'Test 2']);
        await hasTexts('.grid-row-1', ['Test 3', 'Test 4']);
        await hasTexts('.grid-row-2', ['Test 5', 'Test 6']);

        await hasTexts('.grid-column-0', ['Test 1', 'Test 3', 'Test 5']);
        await hasTexts('.grid-column-1', ['Test 2', 'Test 4', 'Test 6']);
      });
    });
  });

  describe('Helpers', () => {
    describe('calculateGridInfo', () => {
      describe('1 column - 3 total items', () => {
        const total = 3;
        const cols = 1;

        it('1st item', async () => {
          const index = 0;

          const {
            positionX,
            positionY,
            isFirstItemInColumn,
            isFirstItemInRow,
            isLastItemInColumn,
            isLastItemInRow,
          } = calculateGridInfo({index, cols, total});

          expect(positionX).toEqual(0);
          expect(positionY).toEqual(0);
          expect(isFirstItemInColumn).toEqual(true);
          expect(isFirstItemInRow).toEqual(true);
          expect(isLastItemInColumn).toEqual(true);
          expect(isLastItemInRow).toEqual(false);
        });

        it('2nd item', async () => {
          const index = 1;

          const {
            positionX,
            positionY,
            isFirstItemInColumn,
            isFirstItemInRow,
            isLastItemInColumn,
            isLastItemInRow,
          } = calculateGridInfo({index, cols, total});

          expect(positionX).toEqual(0);
          expect(positionY).toEqual(1);
          expect(isFirstItemInColumn).toEqual(true);
          expect(isFirstItemInRow).toEqual(false);
          expect(isLastItemInColumn).toEqual(true);
          expect(isLastItemInRow).toEqual(false);
        });

        it('3rd item', async () => {
          const index = 2;

          const {
            positionX,
            positionY,
            isFirstItemInColumn,
            isFirstItemInRow,
            isLastItemInColumn,
            isLastItemInRow,
          } = calculateGridInfo({index, cols, total});

          expect(positionX).toEqual(0);
          expect(positionY).toEqual(2);
          expect(isFirstItemInColumn).toEqual(true);
          expect(isFirstItemInRow).toEqual(false);
          expect(isLastItemInColumn).toEqual(true);
          expect(isLastItemInRow).toEqual(true);
        });
      });

      describe('2 columns - 6 total items', () => {
        const total = 6;
        const cols = 2;

        it('1st item', async () => {
          const index = 0;

          const {
            positionX,
            positionY,
            isFirstItemInColumn,
            isFirstItemInRow,
            isLastItemInColumn,
            isLastItemInRow,
          } = calculateGridInfo({index, cols, total});

          expect(positionX).toEqual(0);
          expect(positionY).toEqual(0);
          expect(isFirstItemInColumn).toEqual(true);
          expect(isFirstItemInRow).toEqual(true);
          expect(isLastItemInColumn).toEqual(false);
          expect(isLastItemInRow).toEqual(false);
        });

        it('2nd item', async () => {
          const index = 1;

          const {
            positionX,
            positionY,
            isFirstItemInColumn,
            isFirstItemInRow,
            isLastItemInColumn,
            isLastItemInRow,
          } = calculateGridInfo({index, cols, total});

          expect(positionX).toEqual(1);
          expect(positionY).toEqual(0);
          expect(isFirstItemInColumn).toEqual(false);
          expect(isFirstItemInRow).toEqual(true);
          expect(isLastItemInColumn).toEqual(true);
          expect(isLastItemInRow).toEqual(false);
        });

        it('3rd item', async () => {
          const index = 2;

          const {
            positionX,
            positionY,
            isFirstItemInColumn,
            isFirstItemInRow,
            isLastItemInColumn,
            isLastItemInRow,
          } = calculateGridInfo({index, cols, total});

          expect(positionX).toEqual(0);
          expect(positionY).toEqual(1);
          expect(isFirstItemInColumn).toEqual(true);
          expect(isFirstItemInRow).toEqual(false);
          expect(isLastItemInColumn).toEqual(false);
          expect(isLastItemInRow).toEqual(false);
        });

        it('4th item', async () => {
          const index = 3;

          const {
            positionX,
            positionY,
            isFirstItemInColumn,
            isFirstItemInRow,
            isLastItemInColumn,
            isLastItemInRow,
          } = calculateGridInfo({index, cols, total});

          expect(positionX).toEqual(1);
          expect(positionY).toEqual(1);
          expect(isFirstItemInColumn).toEqual(false);
          expect(isFirstItemInRow).toEqual(false);
          expect(isLastItemInColumn).toEqual(true);
          expect(isLastItemInRow).toEqual(false);
        });

        it('5th item', async () => {
          const index = 4;

          const {
            positionX,
            positionY,
            isFirstItemInColumn,
            isFirstItemInRow,
            isLastItemInColumn,
            isLastItemInRow,
          } = calculateGridInfo({index, cols, total});

          expect(positionX).toEqual(0);
          expect(positionY).toEqual(2);
          expect(isFirstItemInColumn).toEqual(true);
          expect(isFirstItemInRow).toEqual(false);
          expect(isLastItemInColumn).toEqual(false);
          expect(isLastItemInRow).toEqual(true);
        });

        it('6th item', async () => {
          const index = 5;

          const {
            positionX,
            positionY,
            isFirstItemInColumn,
            isFirstItemInRow,
            isLastItemInColumn,
            isLastItemInRow,
          } = calculateGridInfo({index, cols, total});

          expect(positionX).toEqual(1);
          expect(positionY).toEqual(2);
          expect(isFirstItemInColumn).toEqual(false);
          expect(isFirstItemInRow).toEqual(false);
          expect(isLastItemInColumn).toEqual(true);
          expect(isLastItemInRow).toEqual(true);
        });
      });
    });
  });
});
