import React from 'react';
import {useMutation} from '@apollo/client';
import TurnedInIcon from '@mui/icons-material/TurnedIn';
import Box from '@mui/material/Box';
import Button from '@macanta/components/Button';
import Form from '@macanta/components/Form';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import FormField from '@macanta/containers/FormField';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import {
  CREATE_OR_UPDATE_RELATIONSHIP,
  LIST_RELATIONSHIPS,
} from '@macanta/graphql/dataObjects';
import relationshipOptionValidationSchema from '@macanta/validations/relationshipOption';

const RelationshipOptionForm = ({item: itemProp, onClose}) => {
  const {displayMessage} = useProgressAlert();

  const isNew = !itemProp.id;

  const [
    callCreateOrUpdateRelationship,
    createOrUpdateRelationshipMutation,
  ] = useMutation(CREATE_OR_UPDATE_RELATIONSHIP, {
    update(cache, {data: {createOrUpdateRelationship}}) {
      cache.modify({
        fields: {
          listRelationships(listRelationshipsRef) {
            cache.modify({
              id: listRelationshipsRef.__ref,
              fields: {
                items(existingItems = []) {
                  const newItemRef = cache.writeQuery({
                    data: createOrUpdateRelationship,
                    query: LIST_RELATIONSHIPS,
                  });

                  return [newItemRef, ...existingItems];
                },
              },
            });
          },
        },
      });
    },
    onCompleted: () => {
      displayMessage('Saved successfully!');

      onClose();
    },
  });

  const handleSave = (values) => {
    callCreateOrUpdateRelationship({
      variables: {
        createOrUpdateRelationshipInput: {
          ...(values.id && {
            id: values.id,
          }),
          title: values.title,
          description: values.description,
        },
        __mutationkey: 'createOrUpdateRelationshipInput',
        removeAppInfo: true,
      },
    });
  };

  return (
    <>
      <Form
        initialValues={{
          id: itemProp.id,
          title: itemProp.role,
          description: itemProp.description,
        }}
        enableReinitialize={true}
        validationSchema={relationshipOptionValidationSchema}
        onSubmit={handleSave}>
        {({values, errors, handleChange, handleSubmit, dirty}) => {
          return (
            <>
              <Box
                style={{
                  padding: '1rem',
                }}>
                <FormField
                  autoFocus={isNew}
                  required
                  labelPosition="normal"
                  value={values.title}
                  error={errors.title}
                  onChange={handleChange('title')}
                  label="Title"
                  fullWidth
                  size="small"
                  variant="outlined"
                />

                <FormField
                  type="TextArea"
                  hideExpand
                  labelPosition="normal"
                  value={values.description}
                  onChange={handleChange('description')}
                  label="Description"
                  fullWidth
                  size="small"
                  variant="outlined"
                />
              </Box>
              <Box
                style={{
                  display: 'flex',
                  justifyContent: 'flex-end',
                  padding: '12px 1rem',
                  borderTop: '1px solid #eee',
                }}>
                <Button
                  disabled={!dirty}
                  variant="contained"
                  startIcon={<TurnedInIcon />}
                  onClick={handleSubmit}
                  size="medium">
                  Save
                </Button>
              </Box>
            </>
          );
        }}
      </Form>
      <LoadingIndicator
        modal
        loading={createOrUpdateRelationshipMutation.loading}
      />
    </>
  );
};

RelationshipOptionForm.defaultProps = {
  item: {
    role: '',
    description: '',
  },
};

export default RelationshipOptionForm;
