import React from 'react';
import Typography from '@mui/material/Typography';
import * as Styled from './styles';

const FormHeader = ({title, description}) => (
  <>
    <Typography color="primary" variant="h6" align="center">
      {title}
    </Typography>
    <Styled.Description variant="body2" align="center">
      {description}
    </Styled.Description>
  </>
);

export default FormHeader;
