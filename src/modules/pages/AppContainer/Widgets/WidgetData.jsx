import React, {useState, useEffect, useLayoutEffect, useMemo} from 'react';
import Button from '@macanta/components/Button';
import useRetryQuery from '@macanta/hooks/apollo/useRetryQuery';
import {LIST_WIDGETS_DATA, WIDGET_DATA_FRAGMENT} from '@macanta/graphql/admin';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import OpenInNewIcon from '@mui/icons-material/OpenInNew';
import DownloadIcon from '@mui/icons-material/Download';
import Tooltip from '@macanta/components/Tooltip';
import Pagination from '@macanta/components/Pagination';
import useNextEffect from '@macanta/hooks/base/useNextEffect';
import useIsAdmin from '@macanta/hooks/useIsAdmin';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import useDOFields from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOFields';
import useDORelationships from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDORelationships';
import DOSearch from '@macanta/modules/DataObjectItems/DOSearch';
import CompleteTaskForms from '@macanta/modules/NoteTaskHistory/TaskItem/CompleteTaskForms';
import {DEFAULT_SELECTED_TAB} from '@macanta/modules/pages/AppContainer/ContactRecords/SidebarAccess/SidebarAccess';
import EmailContactBtn from '@macanta/modules/ContactDetails/EmailContactBtn';
import SMSContactBtn from '@macanta/modules/ContactDetails/SMSContactBtn';
import WidgetQueryDOItemButtons from './WidgetQueryDOItemButtons';
import moment from 'moment';
import WidgetContactPopover from './WidgetContactPopover';
import InlineEditingForm from './InlineEditingForm';
import ExportQueryBtn from './ExportQueryBtn';
import Permission from '@macanta/modules/hoc/Permission';
import DOContactIdContext from '@macanta/modules/DataObjectItems/DOContactIdContext';
import {filterDOItems} from '@macanta/selectors/dataObject.selector';
import {getAppInfo} from '@macanta/utils/app';
import * as TableStyled from '@macanta/components/Table/styles';
import * as DOSearchTableStyled from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/styles';
import * as Styled from './styles';
import * as Storage from '@macanta/utils/storage';
import envConfig from '@macanta/config/envConfig';
import {navigate} from 'gatsby';
const WidgetData = ({
  itemId: doItemId,
  widget,
  range,
  data: dataProp,
  loading: loadingProp,
  hideDateRange,
  hideSearch,
  onTaskComplete,
  options,
  HeaderLeftComp,
  onChangePage,
  onChangeRowsPerPage,
  onSort,
  PaginationRightComp,
  hideQueriesBtn,
  onSave,
}) => {
  const {userId: loggedInUserId} = Storage.getItem('userDetails');

  /* const navigate = useNavigate();*/

  const [columns, setColumns] = useState([]);
  const [tableData, setTableData] = useState([]);
  const [total, setTotal] = useState(0);
  const [searchFilter, setSearchFilter] = useState('');
  const [filters, setFilters] = useState([]);
  const [sort, setSort] = useState();
  const [startDate, setStartDate] = useState();
  const [endDate, setEndDate] = useState();
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(25);
  const [groupId, setGroupId] = useState();

  const {
    loading: loadingData,
    data: dataWidgets,
    previousData: previousDataWidgets,
    client,
    refetch,
  } = useRetryQuery(
    LIST_WIDGETS_DATA,
    {
      variables: {
        itemId: doItemId,
        range: startDate || endDate ? null : range,
        startDate,
        endDate,
        type: widget.type,
        userId:
          widget.type === 'MacantaTaskSearch' ? widget.queryId : loggedInUserId,
        queryId: widget.queryId,
        page,
        limit: rowsPerPage,
        q: widget.type === 'MacantaTaskSearch' ? '' : searchFilter,
        ...(filters.length && {
          filter: JSON.stringify(
            filters.map((f, index) => ({
              queryCDFieldLogic: index === 0 ? 'AND' : f.logic.toUpperCase(),
              queryCDFieldName: f.name,
              queryCDFieldOperator: f.operator,
              queryCDFieldValue: f.value,
              queryCDFieldValues: f.values,
            })),
          ),
        }),
        order: sort?.order,
        orderBy: sort?.orderBy,
      },
      skip: !widget.queryId,
      ...options,
    },
    {
      maxRetries: 2,
    },
  );
  const isAdmin = useIsAdmin();
  const doTypes = useDOTypes({
    removeEmail: true,
    includeContactObject: true,
    skip: widget.type === 'MacantaTaskSearch',
  });

  const data =
    previousDataWidgets || !dataProp ? dataWidgets?.listWidgetsData : dataProp;
  const widgetFields = data?.fields;
  const widgetItemsResponse = data?.items;
  const doType = doTypes?.data?.listDataObjectTypes?.find(
    (type) => type.title === widget.groupName,
  );

  useLayoutEffect(() => {
    if (doType?.id) {
      setGroupId(doType?.id);
    }
  }, [doType?.id]);

  const doFieldsQuery = useDOFields(groupId, {
    removeEmail: true,
  });
  const relationshipsQuery = useDORelationships(groupId);

  const fields = doFieldsQuery.data?.listDataObjectFields;
  const relationships = relationshipsQuery.data?.listRelationships;

  const inlineLoading =
    (!groupId || !relationships) &&
    Boolean(doTypes.loading || relationshipsQuery.loading);
  const columnsLoading =
    (!fields || !relationships) &&
    Boolean(doFieldsQuery.loading || relationshipsQuery.loading);
  const loading = Boolean(
    loadingData || inlineLoading || columnsLoading || loadingProp,
  );

  const widgetItems = useMemo(() => {
    if (
      widget.type === 'MacantaTaskSearch' &&
      searchFilter &&
      widgetItemsResponse
    ) {
      const filteredItems = filterDOItems(searchFilter, widgetItemsResponse);

      return filteredItems;
    }

    return widgetItemsResponse;
  }, [searchFilter, widgetItemsResponse]);

  const handleChangeDateFrom = async (dateFrom) => {
    setStartDate(
      dateFrom && dateFrom !== 'Invalid date' ? dateFrom : undefined,
    );
  };

  const handleChangeDateTo = async (dateTo) => {
    setEndDate(dateTo && dateTo !== 'Invalid date' ? dateTo : undefined);
  };

  const handleDownloadAll = () => {
    const [appName, apiKey] = getAppInfo();

    let url;

    const rangeVal = startDate || endDate ? null : range;
    let startDateVal = startDate;
    let endDateVal = endDate ? `${endDate} 23:59` : '';

    switch (rangeVal) {
      case 'main': {
        startDateVal = encodeURIComponent('1 DAY');
        break;
      }
      case '7': {
        startDateVal = encodeURIComponent(`7 day`);
        break;
      }
      case '30': {
        startDateVal = encodeURIComponent(`30 day`);
        break;
      }
      case '90': {
        startDateVal = encodeURIComponent(`90 day`);
        break;
      }
      case 'all': {
        startDateVal = '2010-01-01';
        break;
      }
    }

    if (widget.type === 'MacantaTaskSearch') {
      url = `https://${appName}.${envConfig.apiDomain}/rest/v1/search/download/task/user/${widget.userId}?api_key=${apiKey}`;
    } else {
      url = `https://${appName}.${envConfig.apiDomain}/rest/v1/search/download/widget/queryId/${widget.queryId}?api_key=${apiKey}&userId=${widget.userId}`;

      if (!hideDateRange) {
        url += `&startDate=${startDateVal}&endDate=${endDateVal}`;
      }
    }

    window.location.href = url;
  };

  const handleTaskComplete = (task) => {
    setTableData((state) =>
      state.map((d) => {
        if (d.id === task.id) {
          return {
            ...d,
            'Completion Date': moment().format('YYYY-MM-DD'),
          };
        }

        return d;
      }),
    );

    onTaskComplete && onTaskComplete(widget.id, task);
  };

  const handleSaveQueryDO = ({values, itemId, contactId, isDOQuery}) => {
    const tableDataItem = tableData.find(
      (d) => d.itemId === itemId && d.contactId === contactId,
    );

    if (tableDataItem) {
      Object.keys(tableDataItem).forEach((fieldName) => {
        if (values?.details.hasOwnProperty(fieldName)) {
          client.writeFragment({
            id: `WidgetData:${tableDataItem.id}-${fieldName}`,
            fragment: WIDGET_DATA_FRAGMENT,
            data: {
              value: values?.details[fieldName],
            },
          });
        }
      });
    }

    isDOQuery && refetch();

    onSave && onSave({isDOQuery: !!doItemId});
  };

  const handleOpenContactView = (contactId) => () => {
    navigate(`/app/contact/${contactId}/${DEFAULT_SELECTED_TAB}`);
  };

  const handleSearch = ({search}) => {
    setSearchFilter(search);
  };

  const handleFilters = (filterVal) => {
    setFilters(filterVal);

    setPage(0);
  };

  const handleChangePage = (event, value) => {
    setPage(value);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value));
    setPage(0);
  };

  const handleSort = (property, sortDirection) => {
    setSort({
      order: sortDirection,
      orderBy: property,
    });

    onSort &&
      onSort({
        order: sortDirection,
        orderBy: property,
      });
  };

  useEffect(() => {
    if (page !== 0) {
      setPage(0);
    }
  }, [startDate, endDate, searchFilter, sort]);

  useNextEffect(() => {
    onChangePage && onChangePage(page);
  }, [page]);

  useNextEffect(() => {
    onChangeRowsPerPage && onChangeRowsPerPage(rowsPerPage);
  }, [rowsPerPage]);

  useLayoutEffect(() => {
    if (!loading || data?.total) {
      setTotal(data?.total);
    }
  }, [loading, data?.total]);

  useLayoutEffect(() => {
    if (widgetFields) {
      setColumns(
        widgetFields
          .filter(
            (f) =>
              f.toLowerCase() !== 'id' &&
              f !== 'ItemId' &&
              f !== 'ContactId' &&
              f !== 'EditPermission',
          )
          .map((f) => ({
            id: f,
            label: f,
            minWidth: 200,
          })),
      );
    }
  }, [widgetFields]);

  useLayoutEffect(() => {
    if (
      widgetItems &&
      widgetFields &&
      (widget.type === 'MacantaTaskSearch' ||
        (widget.type !== 'MacantaTaskSearch' &&
          groupId &&
          fields &&
          relationships))
    ) {
      const widgetsData = widgetItems.reduce((acc, item) => {
        const itemObj = {};
        const itemData = item.data;
        const idField = itemData.find((d) => d.fieldKey === 'Id');
        const itemIdField = itemData.find((d) => d.fieldKey === 'ItemId');
        const contactIdField = itemData.find((d) =>
          ['ContactId', 'Contact ID'].includes(d.fieldKey),
        );
        const relationshipsField = itemData.find(
          (d) => d.fieldKey === 'Relationships',
        );
        const editPermissionField = itemData.find(
          (d) => d.fieldKey === 'EditPermission',
        );
        itemObj.id =
          widget.type === 'MacantaTaskSearch'
            ? idField.value
            : `${itemIdField.value}-${contactIdField.value}`;
        itemObj.itemId = itemIdField?.value;

        itemData.forEach((d) => {
          const field = widgetFields.find((f) => f === d.fieldKey);

          if (d.fieldKey === 'ContactId') {
            itemObj['contactId'] = d.value;
          } else if (
            (widget.type === 'MacantaTaskSearch' &&
              d.fieldKey === 'First Name') ||
            d.fieldKey === 'Relationships'
          ) {
            const contactFieldKey =
              widget.type === 'MacantaTaskSearch'
                ? 'First Name'
                : 'Relationships';

            itemObj[contactFieldKey] = [
              {
                value: d.value,
                renderLabel: () => {
                  return (
                    <Box
                      style={{
                        display: 'flex',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        paddingRight: '1rem',
                      }}>
                      <TableStyled.TableBodyCellLabel>
                        {d.value}
                      </TableStyled.TableBodyCellLabel>
                      <Styled.ContactActionButtonsContainer>
                        <Tooltip title="Open Contact">
                          <DOSearchTableStyled.ActionButton
                            style={{
                              color: '#aaa',
                            }}
                            onClick={handleOpenContactView(
                              contactIdField.value,
                            )}
                            size="small">
                            <OpenInNewIcon />
                          </DOSearchTableStyled.ActionButton>
                        </Tooltip>
                        <WidgetContactPopover
                          contactId={contactIdField.value}
                          // onSave={onSave}
                        />
                      </Styled.ContactActionButtonsContainer>
                    </Box>
                  );
                },
              },
            ];
          } else {
            let OtherButtonComp;

            if (d.value && d.fieldKey === 'Email') {
              OtherButtonComp = (
                <EmailContactBtn
                  color="info"
                  variant="contained"
                  to={d.value}
                  contactId={contactIdField.value}
                />
              );
            } else if (d.value && d.fieldKey === 'Phone1') {
              OtherButtonComp = (
                <SMSContactBtn
                  color="info"
                  variant="contained"
                  defaultPhoneNumber={d.value}
                  phoneNumbers={[d.value]}
                  contactId={contactIdField.value}
                />
              );
            }

            itemObj[field] =
              widget.type === 'MacantaTaskSearch' ||
              (!isAdmin && editPermissionField?.value !== 'yes')
                ? !OtherButtonComp
                  ? d.value
                  : [
                      {
                        value: d.value,
                        renderLabel: () => {
                          return (
                            <Styled.InlineEditingContainer
                              key={d.value}
                              style={{
                                height: '32px',
                                display: 'flex',
                                justifyContent: 'space-between',
                                alignItems: 'center',
                              }}>
                              <TableStyled.TableBodyCellLabel>
                                {d.value}
                              </TableStyled.TableBodyCellLabel>
                              <Box
                                className="inline-edit"
                                style={{
                                  position: 'absolute',
                                  right: 0,
                                }}>
                                {OtherButtonComp}
                              </Box>
                            </Styled.InlineEditingContainer>
                          );
                        },
                      },
                    ]
                : [
                    {
                      value: d.value,
                      renderLabel: () => {
                        return (
                          <InlineEditingForm
                            key={d.value}
                            value={d.value}
                            groupId={groupId}
                            relationships={relationshipsField?.value}
                            loading={inlineLoading}
                            isContactField={d.isContactField}
                            fieldName={d.fieldKey}
                            fieldType={d.fieldType}
                            options={d.choices}
                            itemId={itemIdField?.value}
                            contactId={contactIdField.value}
                            hideEdit={widget.type === 'MacantaTaskSearch'}
                            OtherButtonComp={OtherButtonComp}
                            onSave={handleSaveQueryDO}
                            {...(d.fieldType === 'TextArea' && {
                              inputStyle: {
                                marginTop: '60px',
                              },
                            })}
                          />
                        );
                      },
                    },
                  ];
          }
        });

        return acc.concat(itemObj);
      }, []);

      setTableData(widgetsData);
    }
  }, [widgetItems, widgetFields, groupId, fields, relationships]);

  return (
    <>
      <Box
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
          padding: '1rem',
        }}>
        <Box
          style={{
            display: 'flex',
            alignItems: 'center',
          }}>
          {HeaderLeftComp}

          {!hideDateRange && (
            <>
              <Typography
                style={{
                  marginRight: '0.5rem',
                  fontSize: '0.875rem',
                }}>
                Date From
              </Typography>

              <Styled.DateRangeField
                date
                onChange={handleChangeDateFrom}
                placeholder="Date From"
                variant="outlined"
                size="xsmall"
              />

              <Typography
                style={{
                  marginLeft: '1.3rem',
                  marginRight: '0.5rem',
                  fontSize: '0.875rem',
                }}>
                Date To
              </Typography>

              <Styled.DateRangeField
                date
                onChange={handleChangeDateTo}
                placeholder="Date To"
                variant="outlined"
                size="xsmall"
              />
            </>
          )}
        </Box>

        <Box
          style={{
            display: 'flex',
            alignItems: 'center',
          }}>
          {!hideSearch && (
            <DOSearch
              style={{
                marginRight: '1rem',
              }}
              onSearch={handleSearch}
              onFilter={handleFilters}
              filters={filters}
              groupId={groupId}
            />
          )}
          <Button
            style={{
              marginRight: widget.type === 'MacantaQuery' ? '1rem' : 0,
            }}
            onClick={handleDownloadAll}
            color="gray"
            size="small"
            variant="contained"
            startIcon={<DownloadIcon />}>
            Download CSV
          </Button>
          {widget.type === 'MacantaQuery' && (
            <Permission
              keys={[
                'allowContactExport',
                'allowDataObjectExport',
                'allowNoteTaskExport',
              ]}
              condition="allFalse">
              <ExportQueryBtn id={widget.queryId} name={widget.title} />
            </Permission>
          )}
        </Box>
      </Box>
      <Styled.WidgetsDataTable
        fullHeight
        loading={loading}
        hideEmptyMessage={loading}
        columns={columns}
        data={tableData}
        numOfTextLines={2}
        actionColumn={{
          frontLabel: widget.type === 'MacantaTaskSearch' ? 'Status' : '',
          show: true,
        }}
        {...(widget.type !== 'MacantaTaskSearch' && {
          rowsPerPage,
          hidePagination: true,
        })}
        renderFrontActionButtons={
          widget.type === 'MacantaTaskSearch'
            ? ({item}) => (
                <CompleteTaskForms
                  key={!!item['Completion Date']}
                  id={item.id}
                  completed={!!item['Completion Date']}
                  onSuccess={handleTaskComplete}
                />
              )
            : null
        }
        renderActionButtons={({item}) => {
          return (
            item.itemId && (
              <DOContactIdContext.Provider
                value={{
                  contactId: item.contactId,
                }}>
                <WidgetQueryDOItemButtons
                  doTitle={widget.groupName}
                  itemId={item.itemId}
                  onSave={handleSaveQueryDO}
                  fields={fields}
                  relationships={relationships}
                  loading={columnsLoading}
                  setGroupId={setGroupId}
                  groupId={groupId}
                  hideQueriesBtn={hideQueriesBtn}
                />
              </DOContactIdContext.Provider>
            )
          );
        }}
        {...(widget.type !== 'MacantaTaskSearch' && {
          onSort: handleSort,
        })}
      />
      {widget.type !== 'MacantaTaskSearch' && (
        <Box
          style={{
            boxShadow: '0px -4px 10px -2px #eee',
            zIndex: 1,
          }}>
          <Pagination
            table
            count={total}
            page={page}
            onPageChange={handleChangePage}
            rowsPerPage={rowsPerPage}
            rowsPerPageOptions={[25, 50, 100]}
            // {...((startDate || endDate) && {labelDisplayedRows: () => ''})}
            onRowsPerPageChange={handleChangeRowsPerPage}
            RightComp={PaginationRightComp}
          />
        </Box>
      )}
    </>
  );
};

WidgetData.defaultProps = {
  widget: {},
};

export default WidgetData;
