import useAccessPermissions from '@macanta/hooks/admin/useAccessPermissions';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';

const useWorkflowBoardPermission = (optionsProp = {}) => {
  const {boardId, ...options} = optionsProp;

  const accessPermissionsQuery = useAccessPermissions({
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
    ...options,
    key: 'workflowBoards',
  });

  const workflowBoards = accessPermissionsQuery.data;
  const accessScope =
    (boardId &&
      workflowBoards?.find((access) => access.boardId === boardId)?.scope) ||
    'CurrentUser';

  return {isCurrentUserOnly: accessScope === 'CurrentUser', accessScope};
};

export default useWorkflowBoardPermission;
