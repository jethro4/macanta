import {makeVar} from '@apollo/client';
import isNil from 'lodash/isNil';
import isEqual from 'lodash/isEqual';
import * as Storage from '@macanta/utils/storage';

export const makeVarWrapper = (
  name,
  defaultValue,
  {isSession} = {isSession: false},
) => {
  const cache = Storage.getItem('cache', isSession);
  const cachedValue = cache?.[name];
  const wrappedVar = makeVar(cachedValue || defaultValue);

  if (isNil(cachedValue)) {
    Storage.setItem(
      'cache',
      Object.assign({}, cache, {[name]: defaultValue}),
      isSession,
    );
  }

  const wrappedVarFunc = (newValue) => {
    const updatedCache = Storage.getItem('cache', isSession);

    if (!isNil(newValue)) {
      Storage.setItem(
        'cache',
        Object.assign({}, updatedCache, {[name]: newValue}),
        isSession,
      );
      wrappedVar(newValue);
      return;
    }

    if (
      updatedCache?.hasOwnProperty(name) &&
      !isEqual(updatedCache[name], wrappedVar())
    ) {
      wrappedVar(updatedCache[name]);
    }

    return wrappedVar();
  };

  wrappedVarFunc.onNextChange = wrappedVar.onNextChange;
  wrappedVarFunc.forgetCache = wrappedVar.forgetCache;
  wrappedVarFunc.clearVar = () => {
    wrappedVar(defaultValue);
  };

  return wrappedVarFunc;
};

export const nylasPostponedVar = makeVarWrapper('nylasPostponed', false);
export const newVersionAvailableVar = makeVarWrapper(
  'newVersionAvailable',
  false,
);
export const sessionExpiredVar = makeVarWrapper('sessionExpired', false);
export const loggedInVar = makeVarWrapper('loggedIn', false);
export const recentContactsVar = makeVarWrapper('recentContacts', []);
export const loginDisabledMessageVar = makeVarWrapper(
  'loginDisabledMessage',
  '',
);

export const clearVars = () => {
  Storage.setItem('cache', {});

  nylasPostponedVar.clearVar();
  newVersionAvailableVar.clearVar();
  sessionExpiredVar.clearVar();
  loggedInVar.clearVar();
  recentContactsVar.clearVar();
};
