import React, {useState} from 'react';
import Modal from '@macanta/components/Modal';
import ListAssignmentForms from './ListAssignmentForms';

const ListAssignmentQueryOption = ({
  modal,
  showModal: showModalProp = false,
  onClose,
  id,
  name,
  doType,
}) => {
  const [showModal, setShowModal] = useState(showModalProp);

  const handleClose = () => {
    setShowModal(false);

    onClose();
  };

  let listAssignmentForms = (
    <ListAssignmentForms id={id} doType={doType} onSuccess={handleClose} />
  );

  if (modal) {
    listAssignmentForms = (
      <Modal
        headerTitle={`List Assignment (${name} - ${id})`}
        open={showModal}
        onClose={handleClose}
        contentWidth={1200}
        contentHeight={580}>
        {listAssignmentForms}
      </Modal>
    );
  }

  return listAssignmentForms;
};

export default ListAssignmentQueryOption;
