import React, {useState} from 'react';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import useValue from '@macanta/hooks/base/useValue';
import {MONTHS} from '@macanta/constants/time';
import FlexGrid from '@macanta/components/Base/FlexGrid';
import Button, {PopoverButton, IconButton} from '@macanta/components/Button';
import Colors from '@macanta/utils/colors';
import {getMonthOfYear, getNow, getYear} from '@macanta/utils/time';

const MonthPickerField = ({PickerProps, value, onChange, sx, ...props}) => {
  const [options, setOptions] = useState(value);

  const [data] = useValue(() => {
    return {
      label: `${MONTHS[options.month].label} ${options.year}`,
    };
  }, [options]);

  const handleChange = (obj) => {
    const updatedObj = {
      ...options,
      ...obj,
    };

    setOptions(updatedObj);

    onChange(updatedObj);
  };

  const handlePrevious = () => {
    const updatedMonth = options.month - 1;
    const isPrevYear = updatedMonth < 0;
    const optionsObj = {
      month: !isPrevYear ? updatedMonth : 11,
      year: options.year - (!isPrevYear ? 0 : 1),
    };

    handleChange(optionsObj);
  };

  const handleNext = () => {
    const updatedMonth = options.month + 1;
    const isNextYear = updatedMonth > 11;
    const optionsObj = {
      month: !isNextYear ? updatedMonth : 0,
      year: options.year + (!isNextYear ? 0 : 1),
    };

    handleChange(optionsObj);
  };

  const handleMonthChange = (month) => {
    handleChange({month});
  };

  const handleYearChange = (year) => () => {
    handleChange({year});
  };

  const handleToday = () => {
    const nowDate = getNow();

    handleChange({
      month: getMonthOfYear(nowDate),
      year: getYear(nowDate),
    });
  };

  return (
    <Box
      sx={{
        display: 'flex',
        alignItems: 'center',
        ...sx,
      }}
      {...props}>
      <Box
        sx={{
          display: 'flex',
          width: '120px',
          justifyContent: 'flex-end',
        }}>
        <PopoverButton
          variant="text"
          color="info"
          sx={{
            fontSize: '1rem',
            lineHeight: 1,
            letterSpacing: 0.3,
          }}
          PopoverProps={{
            hideHeader: true,
            anchorOrigin: {
              vertical: 'bottom',
              horizontal: 'center',
            },
            transformOrigin: {
              vertical: -8,
              horizontal: 'center',
            },
            ...PickerProps,
          }}
          renderContent={(handleClose) => (
            <Box
              sx={{
                width: '220px',
                padding: '8px 12px',
              }}>
              <Box
                sx={{
                  marginBottom: '8px',
                  display: 'flex',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                <IconButton
                  style={{
                    height: 26,
                    width: 26,
                  }}
                  TooltipProps={{
                    title: 'Previous year',
                  }}
                  disableFocusRipple
                  disableRipple
                  onClick={handleYearChange(options.year - 1)}>
                  <ChevronLeftIcon
                    style={{
                      color: '#aaa',
                    }}
                  />
                </IconButton>

                <Typography sx={{fontSize: '14px'}}>{options.year}</Typography>

                <IconButton
                  style={{
                    height: 26,
                    width: 26,
                  }}
                  TooltipProps={{
                    title: 'Next year',
                  }}
                  disableFocusRipple
                  disableRipple
                  onClick={handleYearChange(options.year + 1)}>
                  <ChevronRightIcon
                    style={{
                      color: '#aaa',
                    }}
                  />
                </IconButton>
              </Box>
              <FlexGrid cols={4} margin={[12, 8]}>
                {MONTHS.map((item) => {
                  const isActive = options.month === item.value;

                  return (
                    <Box
                      key={String(item.value)}
                      sx={{
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <Button
                        onClick={() => {
                          handleMonthChange(item.value);

                          handleClose();
                        }}
                        variant="text"
                        sx={Object.assign(
                          {
                            alignSelf: 'flex-start',
                            fontSize: '14px',
                            color: 'text.primary',
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',

                            minWidth: '38px',
                            height: '36px',
                            borderRadius: '38px',
                          },
                          isActive && {
                            color: 'common.white',
                            backgroundColor: `${Colors.info}`,

                            '&:hover': {
                              backgroundColor: `${Colors.info}cc`,
                            },
                          },
                        )}>
                        {item.short}
                      </Button>
                    </Box>
                  );
                })}
              </FlexGrid>
            </Box>
          )}>
          {data.label}
        </PopoverButton>
      </Box>

      <IconButton
        style={{
          height: 28,
          width: 28,
          marginLeft: 4,
          marginRight: 4,
        }}
        TooltipProps={{
          title: 'Previous month',
        }}
        disableFocusRipple
        disableRipple
        onClick={handlePrevious}>
        <ChevronLeftIcon
          style={{
            color: '#aaa',
          }}
        />
      </IconButton>

      <IconButton
        style={{
          height: 28,
          width: 28,
        }}
        TooltipProps={{
          title: 'Next month',
        }}
        disableFocusRipple
        disableRipple
        onClick={handleNext}>
        <ChevronRightIcon
          style={{
            color: '#aaa',
          }}
        />
      </IconButton>

      <Button
        color="grayDarkest"
        onClick={handleToday}
        variant="text"
        sx={{
          fontSize: '0.9375rem',
        }}>
        Today
      </Button>
    </Box>
  );
};

export default MonthPickerField;
