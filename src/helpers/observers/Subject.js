class Subject {
  static data = {};
  static observers = {};
  static clearAll = () => {
    Subject.data = {};
    Subject.observers = {};
  };

  constructor(key, data) {
    this.key = key;

    Subject.data[key] = Subject.data[key] || data;
    Subject.observers[key] = Subject.observers[key] || [];
  }

  subscribe = (observer) => {
    Subject.observers[this.key].push(observer);

    const unsubscribe = () => {
      var index = Subject.observers[this.key].indexOf(observer);
      if (index > -1) {
        Subject.observers[this.key].splice(index, 1);
      }
    };

    return unsubscribe;
  };

  updateAndNotify = (data) => {
    Subject.data[this.key] = data;

    this.notifyAll();
  };

  notify = (observer) => {
    const index = Subject.observers[this.key].indexOf(observer);

    if (index > -1) {
      Subject.observers[this.key][index].notify(Subject.data[this.key]);
    }
  };

  notifyAll = () => {
    Subject.observers[this.key].forEach(this.notify);
  };
}

export default Subject;
