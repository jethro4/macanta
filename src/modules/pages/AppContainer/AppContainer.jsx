import React, {useEffect} from 'react';
import DashboardHeader from './DashboardHeader';
import {experimentalStyled as styled, fade} from '@mui/material/styles';
import UniversalSearchComp from '@macanta/modules/UniversalSearch';
import {DEFAULT_SELECTED_TAB} from './ContactRecords/SidebarAccess/SidebarAccess';
import AdvancedSearchBtn from './AdvancedSearch/AdvancedSearchBtn';
import SearchGuideBtn from './SearchGuideBtn';
import LoggedInBootstrap from './LoggedInBootstrap';
import * as Styled from './styles';
import * as DrawerStyled from '@macanta/containers/Drawer/styles';
import {isLoggedIn} from '@macanta/utils/app';
import {navigate} from 'gatsby';
export const UniversalSearch = styled(UniversalSearchComp)`
  position: relative;
  border-radius: ${({theme}) => theme.shape.borderRadius};
  background-color: ${({theme}) => fade(theme.palette.common.white, 0.15)};
  &:hover {
    background-color: ${({theme}) => fade(theme.palette.common.white, 0.25)};
  }
  margin-right: ${({theme}) => theme.spacing(2)};
  width: 100%;
  ${({theme}) => theme.breakpoints.up('sm')} {
    max-width: 378px;
    margin-right: ${({theme}) => theme.spacing(4)};
  }
`;

const AppContainer = ({children}) => {
  /* const navigate = useNavigate();*/

  useEffect(() => {
    if (isLoggedIn() && window.location.pathname === `/app`) {
      console.info('Showing dashboard...');

      navigate('/app/dashboard');
    }
  }, []);

  const handleSelectContact = (selectedContact) => {
    navigate(`/app/contact/${selectedContact.id}/${DEFAULT_SELECTED_TAB}`);
  };

  return (
    <>
      <LoggedInBootstrap />
      <Styled.Root>
        <DashboardHeader onSaveContact={handleSelectContact}>
          <UniversalSearch
            id={`universal-search-input`}
            style={{
              marginRight: '1rem',
            }}
            modal
            autoSelect
            onSelectItem={handleSelectContact}
            SearchProps={{
              autoSearch: 'with-value-only',
            }}
            CategoryProps={{
              style: {
                width: 128,
              },
            }}
          />
          <AdvancedSearchBtn
            style={{
              marginRight: '0.5rem',
            }}
          />
          <SearchGuideBtn />
        </DashboardHeader>
        <Styled.AppBodyContainer>
          <DrawerStyled.ContentToolbarSpacer />
          <Styled.AppBody>{children}</Styled.AppBody>
        </Styled.AppBodyContainer>
      </Styled.Root>
    </>
  );
};

export default AppContainer;
