import React, {useState} from 'react';
import Button from '@macanta/components/Button';
import AddIcon from '@mui/icons-material/Add';
import Modal from '@macanta/components/Modal';
import AddDomainForms from './AddDomainForms';

const AddCustomTabBtn = ({onSave}) => {
  const [showForms, setShowForms] = useState(false);

  const handleAdd = () => {
    setShowForms(true);
  };

  const handleClose = () => {
    setShowForms(false);
  };

  const handleSave = () => {
    handleClose();
    onSave && onSave();
  };

  return (
    <>
      <Button
        onClick={handleAdd}
        size="small"
        variant="contained"
        startIcon={<AddIcon />}>
        Add Domain
      </Button>

      <Modal
        headerTitle="Add Domain"
        open={showForms}
        onClose={handleClose}
        contentWidth={300}
        contentHeight={250}>
        <AddDomainForms onSave={handleSave} />
      </Modal>
    </>
  );
};

export default AddCustomTabBtn;
