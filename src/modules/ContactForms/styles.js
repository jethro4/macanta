import {experimentalStyled as styled} from '@mui/material/styles';
import Box from '@mui/material/Box';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormField from '@macanta/containers/FormField';
import Button from '@macanta/components/Button';
import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import * as applicationStyles from '@macanta/themes/applicationStyles';

export const Root = styled(Box)`
  position: relative;
  height: 100%;
  display: flex;
  flex-direction: column;
  background-color: #f5f6fa;
`;

export const TextField = styled(FormField)`
  .MuiInputBase-root {
    background-color: ${({theme}) => theme.palette.common.white};
  }
`;

export const FormGroup = styled(Box)`
  margin: ${({theme}) => theme.spacing(2)} 0px;
  padding: 0px ${({theme}) => theme.spacing(2)};
`;

export const FloatingButton = styled(Button)`
  position: absolute;
  bottom: ${(applicationStyles.DEFAULT_FOOTER_HEIGHT - 36) / 2}px;
  right: ${({theme}) => theme.spacing(2)};
`;

export const SubGroupDivider = styled(Box)`
  margin-bottom: ${({theme}) => theme.spacing(1)};
`;

export const FormsContainer = styled(Box)`
  flex: 1;
  overflow-y: auto;
  display: flex;
  flex-direction: column;

  .react-grid-item.cssTransforms {
    transition-property: none;
  }
  .animated .react-grid-item.cssTransforms {
    transition-property: none;
  }
`;

export const CheckboxContainer = styled(FormControlLabel)`
  .MuiTypography-root {
    color: #999;
    font-size: 0.875rem;
  }
`;

export const AddressRow = styled(NoteTaskHistoryStyled.SelectionItem)`
  ${({highlight, theme}) =>
    highlight &&
    `
      opacity: 1 !important;
      background-color: ${theme.palette.primary.main};

      .MuiTypography-root, .MuiSvgIcon-root {
        color: white
      }
  `}
`;
