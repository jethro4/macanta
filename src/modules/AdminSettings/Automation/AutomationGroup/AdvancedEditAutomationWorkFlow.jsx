import React, {useState} from 'react';
// eslint-disable-next-line import/no-named-as-default
import Section from '@macanta/containers/Section';
// eslint-disable-next-line no-restricted-imports
// eslint-disable-next-line no-restricted-imports
import FlexGrid from '@macanta/components/Base/FlexGrid';
import Breadcrumbs from '@macanta/components/Breadcrumbs';
import {ROOT_PATH_AUTOMATION} from '@macanta/modules/AdminSettings/Automation/Automation';
import MenuList from '@mui/material/MenuList';
import ListItem from '@mui/material/ListItem';
import Divider from '@macanta/components/Base/Divider';
import Box from '@mui/material/Box';
import Show from '@macanta/containers/Show';
import {TRIGGER_ACTION_TYPES} from '@macanta/constants/automation';
import TriggerConditionsSection from '@macanta/modules/AdminSettings/Automation/TriggerConditions/TriggerConditionsSection';
import TriggerActionsSection from '@macanta/modules/AdminSettings/Automation/TriggerActions/TriggerActionsSection';
import withProps from '@macanta/hoc/withProps';

const ADVANCED_EDITOR_TABS = [
  {
    label: 'Trigger Conditions',
    value: 'triggerConditions',
    component: TriggerConditionsSection,
  },
  {
    label: 'Trigger Actions',
    value: 'triggerActions',
    component: TriggerActionsSection,
  },
  ...TRIGGER_ACTION_TYPES.map((item) => ({
    label: `${item.label} Actions`,
    value: item.type,
    component: withProps(item.component, {
      modal: true,
    }),
  })),
];

const AdvancedEditAutomationWorkFlow = () => {
  /* const navigate = useNavigate();*/

  const [selectedTab, setSelectedTab] = useState(ADVANCED_EDITOR_TABS[0]);

  return (
    <>
      <Breadcrumbs
        sx={{
          margin: '1.5rem 1rem 0',
        }}
        title="Workflow Advanced Editor"
        backTitle="Automation Settings"
        backPath={ROOT_PATH_AUTOMATION}
      />

      <FlexGrid height="100%" padding="1.5rem 1rem 0" margin={['2rem']}>
        <Section colWidth="250px" title="Settings">
          <MenuList
            style={{
              height: '100%',
              padding: '0.5rem 0',
            }}>
            {ADVANCED_EDITOR_TABS.map((tab, index) => {
              return (
                <>
                  <ListItem
                    key={tab.value}
                    button
                    selected={tab.value === selectedTab.value}
                    onClick={() => setSelectedTab(ADVANCED_EDITOR_TABS[index])}>
                    {tab.label}
                  </ListItem>
                  <Divider
                    style={{
                      borderBottom: '1px solid #f5f5f5',
                    }}
                  />
                </>
              );
            })}
          </MenuList>
        </Section>

        <Box height="100%">
          {ADVANCED_EDITOR_TABS.map((tab) => {
            return (
              <Show
                key={tab.value}
                removeHidden
                in={selectedTab.value === tab.value}>
                <tab.component
                  style={{
                    margin: 0,
                  }}
                />
              </Show>
            );
          })}
        </Box>
      </FlexGrid>
    </>
  );
};

export default AdvancedEditAutomationWorkFlow;
