import {experimentalStyled as styled} from '@mui/material/styles';
import Box from '@mui/material/Box';

export const PreviewWrapper = styled(Box)``;

export const PDFDocumentWrapper = styled(Box)`
  overflow: auto;

  .react-pdf__Page__textContent {
    border: 1px solid darkgrey;
    box-shadow: 5px 5px 5px 1px #ccc;
    border-radius: 5px;
  }

  .react-pdf__Page__annotations.annotationLayer {
    padding: 20px;
  }

  .react-pdf__Page__canvas {
    margin: 0 auto;
  }
`;
