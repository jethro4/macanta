import React, {useState, useEffect} from 'react';
import AttachmentsGrid from './AttachmentsGrid';

const EmailAttachments = ({files, onDelete, disableDelete}) => {
  const [attachments, setAttachments] = useState([]);

  const handleDelete = (fileId) => async (event) => {
    event.stopPropagation();

    onDelete(fileId);
  };

  useEffect(() => {
    setAttachments(files);
  }, [files]);

  if (!attachments?.length) {
    return null;
  }

  return (
    <AttachmentsGrid
      items={attachments}
      hideDownload
      onDelete={handleDelete}
      disableDelete={disableDelete}
    />
  );
};

EmailAttachments.defaultProps = {
  files: [],
};

export default EmailAttachments;
