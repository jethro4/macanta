import React from 'react';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import {DialogButton, IconButton} from '@macanta/components/Button';

const DeleteButton = ({id, name, type, onDelete, dialog, ...props}) => {
  const label = name ? `"${name}"` : 'this item';

  const handleDelete = async () => {
    await onDelete(id);
  };

  const iconComp = (
    <DeleteForeverIcon
      sx={{
        color: 'grayDarker.main',
      }}
    />
  );

  return !dialog ? (
    <IconButton
      style={{
        padding: 4,
      }}
      disableFocusRipple
      disableRipple
      onClick={handleDelete}
      TooltipProps={{
        title: 'Delete',
      }}>
      {iconComp}
    </IconButton>
  ) : (
    <DialogButton
      icon
      size="small"
      TooltipProps={{
        title: 'Delete',
      }}
      DialogProps={{
        title: 'Are you sure?',
        description: [
          `You are about to delete ${label}${type ? `from ${type}` : ''}.`,
          'This cannot be undone. Do you want to proceed?',
        ],
        onActions: (handleClose) => {
          return [
            {
              label: 'Delete',
              onClick: async () => {
                await handleDelete();

                handleClose();
              },
              startIcon: <DeleteForeverIcon />,
            },
          ];
        },
      }}
      {...props}>
      {iconComp}
    </DialogButton>
  );
};

DeleteButton.defaultProps = {
  dialog: false,
};

export default DeleteButton;
