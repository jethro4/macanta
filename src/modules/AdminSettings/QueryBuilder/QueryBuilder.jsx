import React, {useState, useLayoutEffect} from 'react';
import DataTable from '@macanta/containers/DataTable';
import Breadcrumbs from '@mui/material/Breadcrumbs';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import Typography from '@mui/material/Typography';
import Section from '@macanta/containers/Section';
import {ROOT_PATH_ADMIN} from '@macanta/modules/AdminSettings/AdminSettings';
import useQueries from '@macanta/hooks/admin/useQueries';
import AddQueryBtn from './AddQueryBtn';
import ItemButtons from './ItemButtons';
import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import * as AdminSettingsStyled from '@macanta/modules/AdminSettings/styles';
import {navigate} from 'gatsby';

const DEFAULT_QUERY_BUILDER_COLUMNS = [
  {
    id: 'name',
    label: 'Name',
    minWidth: 170,
  },
  {
    id: 'description',
    label: 'Description',
    minWidth: 170,
    maxWidth: 370,
  },
  {id: 'doType', label: 'Data Object Type', minWidth: 170},
  {
    id: 'contactRelationships',
    label: 'Contact Relationships',
    minWidth: 170,
    maxWidth: 370,
  },
  {
    id: 'userRelationships',
    label: 'User Relationships',
    minWidth: 170,
    maxWidth: 370,
  },
];

const QueryBuilder = () => {
  /* const navigate = useNavigate();*/

  const [tableData, setTableData] = useState([]);

  const {loading, items: queryBuilders} = useQueries();

  const handleBack = () => {
    navigate(ROOT_PATH_ADMIN);
  };

  useLayoutEffect(() => {
    if (queryBuilders) {
      setTableData(
        queryBuilders.map((item) => ({
          ...item,
          contactRelationships: item.contactConditions
            .map((c) => c.relationship)
            .join(', '),
          userRelationships: item.userConditions
            .map((c) => c.relationship)
            .join(', '),
        })),
      );
    }
  }, [queryBuilders]);

  return (
    <NoteTaskHistoryStyled.FullHeightGrid container>
      <NoteTaskHistoryStyled.FullHeightFlexGridColumn item xs={12}>
        <Section
          fullHeight
          bodyStyle={{
            padding: 0,
          }}
          HeaderLeftComp={
            <Breadcrumbs aria-label="breadcrumb">
              <AdminSettingsStyled.TitleCrumbBtn
                startIcon={
                  <ArrowBackIcon
                    style={{
                      color: '#333',
                    }}
                  />
                }
                onClick={handleBack}
                disabled={false}>
                <Typography color="textPrimary">Admin Settings</Typography>
              </AdminSettingsStyled.TitleCrumbBtn>
              <Typography>Query Builder</Typography>
            </Breadcrumbs>
          }
          HeaderRightComp={<AddQueryBtn />}>
          <DataTable
            fullHeight
            loading={loading}
            hideEmptyMessage={loading}
            columns={DEFAULT_QUERY_BUILDER_COLUMNS}
            data={tableData}
            orderBy="name"
            renderActionButtons={(actionBtnProps) => (
              <ItemButtons {...actionBtnProps} />
            )}
            numOfTextLines={2}
          />
        </Section>
      </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
    </NoteTaskHistoryStyled.FullHeightGrid>
  );
};

export default QueryBuilder;
