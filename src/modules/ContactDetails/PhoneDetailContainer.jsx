import React from 'react';
import SMSContactBtn from './SMSContactBtn';
import DetailWithActionArea from './DetailWithActionArea';

const PhoneDetailContainer = ({
  label,
  defaultPhoneNumber,
  phoneNumbers,
  noMargin,
  disableGrid,
  contactId,
}) => {
  return (
    <DetailWithActionArea
      disableGrid={disableGrid}
      label={label}
      value={defaultPhoneNumber}
      noMargin={noMargin}
      ButtonComp={
        !!defaultPhoneNumber && (
          <SMSContactBtn
            defaultPhoneNumber={defaultPhoneNumber}
            phoneNumbers={phoneNumbers}
            contactId={contactId}
          />
        )
      }
    />
  );
};

PhoneDetailContainer.defaultProps = {
  phoneNumbers: [],
};

export default PhoneDetailContainer;
