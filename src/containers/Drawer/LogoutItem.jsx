import React from 'react';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import ExitToAppIcon from '@mui/icons-material/ExitToApp';
import Tooltip from '@macanta/components/Tooltip';
import {useApolloClient} from '@apollo/client';
import * as Storage from '@macanta/utils/storage';
import {navigate} from 'gatsby';
import {clearVars} from '@macanta/graphql/cache/vars';

const LogoutItem = () => {
  const client = useApolloClient();

  const handleLogout = async () => {
    const userDetails = Storage.getItem('userDetails');

    console.info('Manual logging out: ' + userDetails?.userId);

    Storage.setItem('session', {loggingOut: true});
    clearVars();
    client.clearStore();
    await navigate('/', {replace: true});
  };

  return (
    <Tooltip title="Log Out" placement="right">
      <ListItem id={`log-out-button`} button onClick={handleLogout}>
        <ListItemIcon>
          <ExitToAppIcon />
        </ListItemIcon>
        <ListItemText primary={'Log Out'} />
      </ListItem>
    </Tooltip>
  );
};

export default LogoutItem;
