import {findItem} from '@macanta/utils/array';
import React, {useEffect} from 'react';
import {getSelectedVariantAttrs} from './helpers';
import * as Styled from './styles';
import {getHash, setHash} from '@macanta/utils/uri';
import useValue from '@macanta/hooks/base/useValue';
import {isNumeric} from '@macanta/utils/numeric';

const ButtonGroup = ({
  children,
  selectedButtonIndex,
  selectedValue,
  withHash,
  ...props
}) => {
  const childrenArray = React.Children.toArray(children);

  const [selectedHash, setSelectedHash] = useValue(() => {
    let selectedChildHash = selectedValue;

    if (!selectedChildHash) {
      selectedChildHash = getHash('group');
    }

    return selectedChildHash;
  }, [selectedValue]);

  const [selectedIndex, setSelectedIndex] = useValue(() => {
    let selectedChildIndex = selectedButtonIndex;

    if (selectedHash) {
      selectedChildIndex = childrenArray.findIndex(
        (child) => child.props.value === selectedHash,
      );
    }

    return selectedChildIndex;
  }, [selectedHash, selectedButtonIndex]);

  const handleClick = (onClick, index, hash) => () => {
    if (withHash) {
      setSelectedHash(hash);
    } else {
      setSelectedIndex(index);
    }

    onClick(index);
  };

  useEffect(() => {
    if (withHash && selectedHash) {
      setHash('group', selectedHash);
    }
  }, [withHash, selectedHash]);

  useEffect(() => {
    if (withHash) {
      const selectedChild = findItem(
        childrenArray,
        (child) => child.props.value === selectedHash,
      );

      if (selectedChild) {
        selectedChild.props.onClick();
      }
    }
  }, []);

  return (
    <Styled.Root {...props}>
      {childrenArray.map((child, index) => {
        const selected = isNumeric(selectedIndex)
          ? selectedIndex === index
          : selectedHash === child.props.value;
        const selectedVariantAttrs = selected
          ? getSelectedVariantAttrs(props.variant, child.props.style)
          : {};

        return React.cloneElement(child, {
          onClick: handleClick(child.props.onClick, index, child.props.value),
          ...selectedVariantAttrs,
          selected,
          ...(selected && {
            className: 'buttonGroup-selected',
          }),
        });
      })}
    </Styled.Root>
  );
};

ButtonGroup.defaultProps = {
  selectedButtonIndex: 0,
};

export default ButtonGroup;
