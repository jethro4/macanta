import React, {useState} from 'react';
import ContactSelect from '@macanta/modules/AdminSettings/UserManagement/UserForm/ContactSelect';
import {getFullName} from '@macanta/selectors/contact.selector';

const getOptionLabel = (item) => getFullName(item);

const MultiDOContactFilter = ({contactIds, onChange}) => {
  const [selectedContactIds, setSelectedContactIds] = useState(contactIds);

  const handleChange = (updatedSelectedIds) => {
    setSelectedContactIds(updatedSelectedIds);
    onChange(updatedSelectedIds);
  };

  return (
    <ContactSelect
      sx={{
        width: '200px',
      }}
      multiple
      onChange={handleChange}
      labelPosition="normal"
      placeholder="Filter By Contacts"
      fullWidth
      variant="outlined"
      size="xsmall"
      getOptionLabel={getOptionLabel}
      value={selectedContactIds}
    />
  );
};

export default MultiDOContactFilter;
