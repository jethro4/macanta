import React from 'react';
import Custom from './Custom';
import Slide from './Slide';
import Grow from './Grow';
import Collapse from './Collapse';

let Animated = ({variant, ...props}) => {
  let AnimatedElement;

  switch (variant) {
    case 'slide': {
      AnimatedElement = Slide;
      break;
    }
    case 'grow': {
      AnimatedElement = Grow;
      break;
    }
    case 'collapse': {
      AnimatedElement = Collapse;
      break;
    }
    default: {
      AnimatedElement = Custom;
    }
  }

  return <AnimatedElement {...props} />;
};

Animated.defaultProps = {
  mountOnEnter: true,
  unmountOnExit: true,
};

export default Animated;
