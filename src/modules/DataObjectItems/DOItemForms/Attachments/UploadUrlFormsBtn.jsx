import React, {useState} from 'react';
import Modal from '@macanta/components/Modal';
import UploadUrlForms from './UploadUrlForms';
import Button from '@macanta/components/Button';
import LinkIcon from '@mui/icons-material/Link';

const UploadUrlFormsBtn = ({onSuccess, ...props}) => {
  const [showModal, setShowModal] = useState(false);

  const handleShowModal = () => {
    setShowModal(true);
  };
  const handleCloseModal = () => {
    setShowModal(false);
  };
  const handleSuccess = async (attachment, type) => {
    onSuccess && (await onSuccess(attachment, type));
    setShowModal(false);
  };

  return (
    <>
      <Button
        onClick={handleShowModal}
        size="small"
        variant="contained"
        startIcon={<LinkIcon />}
        {...props}>
        URL
      </Button>
      <Modal
        headerTitle="Attach File URL"
        open={showModal}
        onClose={handleCloseModal}>
        <UploadUrlForms onSubmit={handleSuccess} />
      </Modal>
    </>
  );
};

export default UploadUrlFormsBtn;
