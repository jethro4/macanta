import useQuery from '@macanta/hooks/apollo/useQuery';
import {LIST_TASK_SCHEDULES_USER} from '@macanta/graphql/scheduler';
import {groupBy, sortArrayByObjectKey} from '@macanta/utils/array';
import useDOSchedules from '@macanta/hooks/scheduler/useDOSchedules';
import * as Storage from '@macanta/utils/storage';
import useValue from '@macanta/hooks/base/useValue';
import useAccumulatedItems from '@macanta/hooks/base/useAccumulatedItems';

const transformScheduleItem = (type, item, date) => {
  const isTask = type === 'task';

  return {
    type: isTask ? 'Task' : type,
    id: item.id,
    contactId: item.contactId,
    title: item.title,
    description: isTask ? item.note : item.description,
    date,
    startDateTime: item.startDateTime,
    endDateTime: item.endDateTime,
    createdDate: item.creationDate,
  };
};

const useSchedules = ({dateStart, dateEnd} = {}, options) => {
  const {userId: loggedInUserId} = Storage.getItem('userDetails');

  const doSchedulesQuery = useDOSchedules(options);
  // const doSchedulesUserQuery = useQuery(LIST_DO_SCHEDULES_USER, options);
  const taskSchedulesUserQuery = useQuery(LIST_TASK_SCHEDULES_USER, {
    variables: {
      contactId: loggedInUserId,
      dateStart,
      dateEnd,
    },
    ...options,
  });

  const loading = doSchedulesQuery.loading || taskSchedulesUserQuery.loading;
  const initLoading =
    doSchedulesQuery.initLoading || taskSchedulesUserQuery.initLoading;

  const tasks = taskSchedulesUserQuery.result?.items || [];
  const monthlyItems = [...tasks];

  const [monthlyData] = useValue(() => {
    const transformTasks = monthlyItems.map((d) => ({
      ...d,
      items: sortArrayByObjectKey(
        d.items.map((item) => transformScheduleItem('task', item, d.date)),
        'createdDate',
      ),
    }));

    return sortArrayByObjectKey(transformTasks, 'date', 'asc');
  }, [monthlyItems]);

  const [listData] = useValue(() => {
    const allListItems = monthlyData.reduce((acc, d) => {
      let accArr = [...acc];

      accArr = accArr.concat(d.items);

      return accArr;
    }, []);
    const sortedListItems = sortArrayByObjectKey(allListItems, 'date', 'asc');

    return sortedListItems;
  }, [monthlyData]);

  const {items: allItems} = useAccumulatedItems(listData, 'id');

  const [allData] = useValue(() => {
    const groupedData = groupBy(allItems, 'date');

    return groupedData;
  }, [allItems]);

  return {allData, monthlyData, loading, initLoading};
};

export default useSchedules;
