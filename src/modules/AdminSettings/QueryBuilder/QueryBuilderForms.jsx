import React, {useState, useEffect, useMemo} from 'react';
import {useFormikContext} from 'formik';
import TurnedInIcon from '@mui/icons-material/TurnedIn';
import Box from '@mui/material/Box';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import Button from '@macanta/components/Button';
import Form from '@macanta/components/Form';
import {SubSection} from '@macanta/containers/Section';
import FormField from '@macanta/containers/FormField';
import TransferList from '@macanta/containers/TransferList';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import useDOFields from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOFields';
import useDORelationships from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDORelationships';
import RunQueryBtn from '@macanta/modules/pages/AppContainer/AdvancedSearch/RunQueryBtn';
import queryBuilderValidationSchema from '@macanta/validations/queryBuilder';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import usePrevious from '@macanta/hooks/usePrevious';
import useAllFields from '@macanta/hooks/dataObject/useAllFields';
import CriteriaForms from '@macanta/modules/AdminSettings/QueryBuilder/CriteriaForms';
import {useMutation} from '@apollo/client';
import {
  CREATE_OR_UPDATE_QUERY_BUILDER,
  LIST_QUERY_BUILDERS,
} from '@macanta/graphql/admin';
import {
  DEFAULT_OPERATOR,
  DEFAULT_USER_OPERATOR,
} from '@macanta/constants/operators';
import * as NoteTaskFormsStyled from '@macanta/modules/NoteTaskForms/styles';
import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import * as applicationStyles from '@macanta/themes/applicationStyles';
import * as Styled from './styles';

export const getType = (type) => {
  if (['Date', 'DateTime', 'TextArea'].includes(type)) {
    return 'Text';
  }

  return type;
};

const getNewItem = (type, logic) => {
  switch (type) {
    case 'doConditions': {
      return {
        id: Date.now(),
        name: '',
        logic,
        operator: DEFAULT_OPERATOR,
        values: null,
        value: '',
      };
    }
    case 'contactConditions': {
      return {
        id: Date.now(),
        relationship: '',
        logic,
      };
    }
    case 'userConditions': {
      return {
        id: Date.now(),
        relationship: '',
        logic,
        operator: DEFAULT_USER_OPERATOR,
        userId: '',
      };
    }
  }
};

const QueryBuilderFormsContainer = ({item, onSave, ...props}) => {
  const {displayMessage} = useProgressAlert();

  const [
    callCreateOrUpdateQueryBuilder,
    {loading: savingQueryBuilder},
  ] = useMutation(CREATE_OR_UPDATE_QUERY_BUILDER, {
    update(cache, {data: {createOrUpdateQueryBuilder}}) {
      cache.modify({
        fields: {
          listQueryBuilders(listQueryBuildersRef) {
            cache.modify({
              id: listQueryBuildersRef.__ref,
              fields: {
                items(existingItems = []) {
                  const newItemRef = cache.writeQuery({
                    data: createOrUpdateQueryBuilder,
                    query: LIST_QUERY_BUILDERS,
                  });

                  return [...existingItems, newItemRef];
                },
              },
            });
          },
        },
      });
    },
    onError() {},
    onCompleted: (data) => {
      if (data?.createOrUpdateQueryBuilder) {
        displayMessage('Saved successfully!');

        onSave?.(data);
      }
    },
  });

  const getInitValues = () => {
    return {
      id: item?.id,
      doType: item?.doType || '',
      name: item?.name || '',
      description: item?.description || '',
      doConditions: item?.doConditions
        ? item.doConditions.map((condition, index) => {
            const id = Date.now() + index;

            return {
              id,
              ...condition,
            };
          })
        : [
            {
              id: Date.now(),
              name: '',
              logic: '',
              operator: DEFAULT_OPERATOR,
              values: null,
              value: '',
            },
          ],
      contactConditions: item?.contactConditions
        ? item.contactConditions.map((condition, index) => {
            const id = Date.now() + index;

            return {
              id,
              ...condition,
            };
          })
        : [],
      userConditions: item?.userConditions
        ? item.userConditions.map((condition, index) => {
            const id = Date.now() + index;

            return {
              id,
              ...condition,
            };
          })
        : [],
      chosenFields: item?.chosenFields || [],
      criteriaMessageA: item?.criteriaMessageA,
      criteriaMessageB: item?.criteriaMessageB,
    };
  };

  const [initValues, setInitValues] = useState(() => getInitValues());
  const prevInitValues = usePrevious(initValues);

  const removeConditionIdAndTypename = (condition) => {
    const tempCondition = {...condition};

    delete tempCondition.id;
    delete tempCondition.__typename;

    return tempCondition;
  };

  const handleFormSubmit = async (values) => {
    callCreateOrUpdateQueryBuilder({
      variables: {
        createOrUpdateQueryBuilderInput: {
          id: values.id,
          doType: values.doType,
          name: values.name,
          description: values.description,
          doConditions: values.doConditions.map(removeConditionIdAndTypename),
          contactConditions: values.contactConditions.map(
            removeConditionIdAndTypename,
          ),
          userConditions: values.userConditions.map(
            removeConditionIdAndTypename,
          ),
          chosenFields: values.chosenFields,
          criteriaMessageA: values.criteriaMessageA,
          criteriaMessageB: values.criteriaMessageB,
        },
        __mutationkey: 'createOrUpdateQueryBuilderInput',
      },
    });
  };

  useEffect(() => {
    if (item && prevInitValues === initValues) {
      setInitValues(getInitValues());
    }
  }, [item]);

  return (
    <>
      <Form
        initialValues={initValues}
        enableReinitialize
        validationSchema={queryBuilderValidationSchema}
        onSubmit={handleFormSubmit}>
        {({values}) => {
          return <QueryBuilderForms item={values} {...props} />;
        }}
      </Form>
      <LoadingIndicator modal loading={savingQueryBuilder} />
    </>
  );
};

const QueryBuilderForms = ({
  item,
  hideNameDescription,
  hideContactNotifications,
  renderBasicFields,
  renderActionButtons,
  hidePreview,
  ...props
}) => {
  const {
    errors,
    handleChange,
    setValues,
    setFieldValue,
    setFieldError,
    handleSubmit,
    dirty,
  } = useFormikContext();

  const doTypesQuery = useDOTypes({
    removeEmail: true,
    includeContactObject: true,
  });

  const doType = doTypesQuery.data?.listDataObjectTypes?.find(
    (type) => type.title === item?.doType,
  );
  const groupId = doType?.id;

  const doFieldsQuery = useDOFields(groupId, {
    removeEmail: true,
  });
  const relationshipsQuery = useDORelationships(groupId);

  const doTypes = doTypesQuery.data?.listDataObjectTypes;
  const doFields = doFieldsQuery.data?.listDataObjectFields;
  const relationships = relationshipsQuery.data?.listRelationships;
  const isContactObjectType = item?.doType === 'Contact Object';

  const allFieldsQuery = useAllFields({
    groupId,
    isContactObjectType,
  });

  const allFields = allFieldsQuery.data;

  const allRelationships = useMemo(() => {
    if (relationships) {
      return relationships.map((r) => ({
        label: r.role,
        value: r.role,
      }));
    }

    return [];
  }, [relationships]);

  const loading =
    (!groupId || !doFields || !relationships) &&
    Boolean(
      doTypesQuery.loading ||
        doFieldsQuery.loading ||
        relationshipsQuery.loading,
    );

  return (
    <Styled.FormRoot {...props}>
      <Styled.Body>
        <NoteTaskHistoryStyled.FullHeightGrid
          style={{
            flexDirection: 'row',
          }}
          sx={applicationStyles.fillScroll}
          container>
          <NoteTaskHistoryStyled.FullHeightFlexGridColumn item sm={12} md={5}>
            <Styled.SubBody>
              <SubSection
                headerStyle={{
                  backgroundColor: 'white',
                  minHeight: '3rem',
                }}
                style={{
                  marginTop: 0,
                }}
                title="Basic Information">
                {!hideNameDescription && (
                  <>
                    <Styled.TextField
                      required
                      value={item?.name}
                      error={errors.name}
                      onChange={handleChange('name')}
                      label="Name"
                      fullWidth
                      size="small"
                      variant="outlined"
                    />
                    <Styled.TextField
                      type="TextArea"
                      hideExpand
                      value={item?.description}
                      onChange={handleChange('description')}
                      label="Description"
                      fullWidth
                      size="small"
                      variant="outlined"
                    />
                  </>
                )}
                <Styled.TextField
                  required
                  type="Select"
                  options={doTypes}
                  labelKey="title"
                  valueKey="title"
                  value={item?.doType}
                  error={errors.doType}
                  onChange={(val) => {
                    setValues((values) => ({
                      ...values,
                      id: values.id,
                      name: values.name,
                      description: values.description,
                      doType: val,
                      doConditions: [
                        {
                          id: Date.now(),
                          name: '',
                          logic: '',
                          operator: DEFAULT_OPERATOR,
                          values: null,
                          value: '',
                        },
                      ],
                      contactConditions: [],
                      userConditions: [],
                      chosenFields: [],
                      criteriaMessageA: '',
                      criteriaMessageB: '',
                    }));
                  }}
                  label="Data Object Type"
                  fullWidth
                  size="small"
                  variant="outlined"
                />

                {renderBasicFields && renderBasicFields()}
              </SubSection>

              {!hideContactNotifications && (
                <Styled.SubSection
                  disabled={!item?.doType}
                  headerStyle={{
                    backgroundColor: 'white',
                    minHeight: '3rem',
                  }}
                  title="Contact Notifications">
                  <Box
                    style={{
                      display: 'flex',
                    }}>
                    <FormField
                      style={{
                        flex: 1,
                      }}
                      type="TextArea"
                      hideExpand
                      // error={errors[field.name]}
                      value={item?.criteriaMessageA}
                      onChange={handleChange('criteriaMessageA')}
                      label="When All Criteria Met"
                      placeholder="Enter preview text"
                      fullWidth
                      variant="outlined"
                    />
                    <Box width="2rem" />
                    <FormField
                      style={{
                        flex: 1,
                      }}
                      type="TextArea"
                      hideExpand
                      // error={errors[field.name]}
                      value={item?.criteriaMessageB}
                      onChange={handleChange('criteriaMessageB')}
                      label="When Criteria Not Met"
                      placeholder="Enter preview text"
                      fullWidth
                      variant="outlined"
                    />
                  </Box>
                </Styled.SubSection>
              )}
            </Styled.SubBody>
          </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
          <NoteTaskHistoryStyled.FullHeightFlexGridColumn item xs={12} md={7}>
            <Styled.SubBody>
              <CriteriaForms
                loading={loading}
                disabled={!item?.doType}
                style={{
                  marginTop: 0,
                }}
                title={`${item?.doType || 'DO'} Criteria`}
                data={item?.doConditions}
                required
                type="doConditions"
                choices={allFields}
                error={errors.doConditions}
                getNewItem={getNewItem}
              />

              {!isContactObjectType && (
                <>
                  <CriteriaForms
                    loading={loading}
                    disabled={!item?.doType}
                    title="Contact Relationship and Criteria"
                    data={item?.contactConditions}
                    type="contactConditions"
                    choices={allRelationships}
                    error={errors.contactConditions}
                    getNewItem={getNewItem}
                  />
                  <CriteriaForms
                    loading={loading}
                    disabled={!item?.doType}
                    title="User Relationship and Criteria"
                    data={item?.userConditions}
                    type="userConditions"
                    choices={allRelationships}
                    error={errors.userConditions}
                    getNewItem={getNewItem}
                  />
                </>
              )}

              <Styled.SubSection
                disabled={!item?.doType}
                headerStyle={{
                  backgroundColor: 'white',
                  minHeight: '3rem',
                }}
                bodyStyle={{
                  height: 400,
                }}
                title="Result Columns">
                <TransferList
                  error={errors.chosenFields}
                  loading={allFieldsQuery.initLoading}
                  options={allFields}
                  selectedItems={item?.chosenFields}
                  hideEmptyMessage={loading}
                  onChangeFields={(newFields) => {
                    if (newFields.length) {
                      setFieldError('chosenFields', '');
                    }

                    setFieldValue('chosenFields', newFields);
                  }}
                />
              </Styled.SubSection>
            </Styled.SubBody>
          </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
        </NoteTaskHistoryStyled.FullHeightGrid>
        <LoadingIndicator
          fill
          align="top"
          style={{
            top: '2.5rem',
          }}
          loading={loading}
        />
      </Styled.Body>
      <NoteTaskFormsStyled.Footer
        sx={applicationStyles.footer}
        style={{
          padding: '1rem',
          margin: 0,
          borderTop: '1px solid #eee',
          width: '100%',
          backgroundColor: '#f5f6fa',
        }}>
        {!hidePreview && (
          <RunQueryBtn
            headerTitle={`Preview Results (${item.doType})`}
            disabled={!item.doType}
            variant="text"
            label="Preview"
            style={{
              marginRight: '1.5rem',
            }}
          />
        )}

        {renderActionButtons ? (
          renderActionButtons()
        ) : (
          <Button
            disabled={!dirty}
            variant="contained"
            startIcon={<TurnedInIcon />}
            onClick={handleSubmit}
            size="medium">
            Save Query
          </Button>
        )}
      </NoteTaskFormsStyled.Footer>
    </Styled.FormRoot>
  );
};

export default QueryBuilderFormsContainer;
