import useQuery from '@macanta/hooks/apollo/useQuery';
import {GET_WEBHOOK_ACTIONS} from '@macanta/graphql/automation';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {findItem} from '@macanta/utils/array';

const FIELD_KEY_MAPPING = {
  queryId: 'id',
  queryType: 'type',
  queryName: 'name',
  queryDescription: 'description',
  queryConnectedDataType: 'doTitle',
  queryPostURL: 'postURL',
  queryContact: 'contactConditions',
  queryCustomField: 'customKeyValues',
  queryContactRelationshipFieldLogic: 'logic',
  queryContactRelationship: 'relationship',
  queryCDField: 'doConditions',
  queryCDFieldLogic: 'logic',
  queryCDFieldName: 'name',
  queryCDFieldOperator: 'operator',
  queryCDFieldValue: 'value',
  queryCDFieldValues: 'values',
};

const useWebhookActions = (options) => {
  const doTypesQuery = useDOTypes({
    removeEmail: true,
    includeContactObject: true,
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const query = useQuery(GET_WEBHOOK_ACTIONS, {
    keyMapping: FIELD_KEY_MAPPING,
    transformResult: (result) =>
      result?.items.map((item) => {
        const doType = findItem(
          doTypesQuery.result,
          (d) => d.title === item.doTitle,
        );

        return {
          ...item,
          groupId: doType?.id,
        };
      }),
    dependencies: [doTypesQuery.result],
    ...options,
  });

  return query;
};

export default useWebhookActions;
