import {gql} from '@apollo/client';

export const PERMISSIONS_FRAGMENT = gql`
  fragment permissionsFragment on AccessPermissions {
    permissionMeta {
      allowUserDeleteContact
      allowUserDeleteDOItem
      allowUserAttachment
      allowUserDeleteDOAttachment
      allowUserToMergeContacts
      allowEmailSync
      allowAddRemoveEmailDomainFilter
      allowContactExport
      allowDataObjectExport
      allowNoteTaskExport
      allowAddDirectRelationship
      userDOEditPermission {
        otherUserRelationship
        withOtherUserDataToggle
        withOtherUserRelationshipToggle
        withOtherUserRelationshipDataToggle
        withOtherUserRelationshipRelationshipToggle
      }
      hideActionButtonsForGroups
      hideConnectButtonsForGroups
      hidePurchaseButton
    }
    mqbs {
      queryId
      permission
    }
    mqbWidgetAccess {
      queryId
      show
    }
    mqbWidgetEmail {
      queryId
      show
    }
    tasks {
      taskId
      allow
      specificIds
    }
    userGuides {
      guideId
      type
      value
      subType
      meta {
        name
        value
      }
    }
    tabs {
      groupId
      permission
    }
    sections {
      groupId
      access {
        name
        permission
      }
    }
    contacts {
      groupId
      access {
        name
        permission
        relationship
      }
    }
    emailTemplates {
      templateGroupId
      name
    }
    workflowBoards {
      boardId
      scope
    }
  }
`;
