import React, {forwardRef, useMemo} from 'react';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Tooltip from '@macanta/components/Tooltip';
import {useLocation, Link} from '@reach/router';
import UserAvatar from './UserAvatar';
import * as Styled from './styles';

const isUrlPrefixMatch = (toPath = '', currentPath = '') => {
  const toPaths = toPath
    .split('/')
    .filter((path) => !!path)
    .slice(0, 3); //TODO: This assumes the prefix /app/*
  const toPathsString = toPaths.join('/');
  const currentPathPrefixString = currentPath
    .split('/')
    .filter((path) => !!path)
    .slice(0, toPaths.length)
    .join('/');

  return !!toPaths.length && toPathsString === currentPathPrefixString;
};

const DrawerListItem = ({to, label, IconComp, avatar, expanded, ...props}) => {
  const location = useLocation();

  const selected = isUrlPrefixMatch(encodeURI(to), location.pathname);

  const CustomLink = useMemo(
    () =>
      forwardRef((linkProps, ref) => <Link ref={ref} to={to} {...linkProps} />),
    [to],
  );

  return (
    <Tooltip title={label} placement="right">
      <Styled.ListItem
        selected={selected}
        button
        component={CustomLink}
        {...props}>
        {!avatar ? (
          <ListItemIcon>
            {' '}
            <IconComp />
          </ListItemIcon>
        ) : (
          <UserAvatar expanded={expanded} />
        )}
        {(!avatar || (avatar && expanded)) && <ListItemText primary={label} />}
      </Styled.ListItem>
    </Tooltip>
  );
};

export default DrawerListItem;
