import {gql} from '@apollo/client';

export default gql`
  mutation deleteDOToDORelationship($input: DeleteDOToDORelationshipInput!) {
    deleteDOToDORelationship(input: $input) {
      id
      success
    }
  }
`;
