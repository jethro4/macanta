import {gql} from '@apollo/client';

export default gql`
  query getNylasInfo($appName: String!, $apiKey: String!, $userId: String!) {
    getNylasInfo(appName: $appName, apiKey: $apiKey, userId: $userId) {
      isValidToken
      isValidUser
    }
  }
`;
