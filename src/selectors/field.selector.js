import {CONTACT_FIELDS} from '@macanta/constants/contactFields';
import {sortArrayByPriority, insertOrUpdate} from '@macanta/utils/array';

export const getSectionsData = (
  itemData,
  whitelistedSections,
  blacklistedSections,
) => {
  const sectionsObj = convertWithSections(itemData);

  let transformedSectionsData = transformSectionsData(sectionsObj);

  if (whitelistedSections?.length) {
    transformedSectionsData = transformedSectionsData.filter((section) => {
      return (
        whitelistedSections.includes(section.sectionName) &&
        !blacklistedSections?.includes(section.sectionName)
      );
    });
  }

  return transformedSectionsData;
};

export const getSectionNames = (sections, {isContactType}) => {
  let sectionsArr = sections.map((section) => section.sectionName);

  if (isContactType) {
    sectionsArr = insertOrUpdate({
      arr: sectionsArr,
      item: 'Basic Info',
      index: 0,
    });
  }

  return sectionsArr;
};

export const convertWithSections = (itemData) => {
  return itemData.reduce((acc, d) => {
    const sections = {...acc};
    const sectionName = d.sectionTag || 'General';
    if (!sections[sectionName]) {
      sections[sectionName] = {subGroups: {}};
    }

    const section = sections[sectionName];
    const subGroups = section.subGroups;
    const subGroupName = d.subGroup || 'Overview';
    if (!subGroups[subGroupName]) {
      subGroups[subGroupName] = [];
    }

    const subGroup = subGroups[subGroupName];
    subGroup.push(d);

    return sections;
  }, {});
};

export const transformSectionsData = (sectionsObj) => {
  const transformedSectionsData = Object.entries(sectionsObj).map(
    ([sectionName, sectionData]) => {
      const transformedSubGroups = Object.entries(sectionData.subGroups).map(
        ([subGroupName, data]) => {
          return {
            subGroupName,
            data: data.map((d) => ({
              value: d.value || d.default || '',
              ...d,
            })),
          };
        },
      );

      return {
        sectionName,
        subGroups: transformedSubGroups,
      };
    },
  );

  const sortedSectionsData = sortArrayByPriority(
    transformedSectionsData,
    'sectionName',
    ['sectionWithoutName'],
  );

  return sortedSectionsData;
};

export const filterFieldsForTable = (fields, hideContacts) => {
  const tableFields = fields.filter((field) => field.showInTable);
  const fieldsData = tableFields.length
    ? tableFields.slice().sort((item1, item2) => {
        return item1.showOrder - item2.showOrder;
      })
    : fields.slice(0, 3);

  // let columnsWithId = [{name: 'ID'}].concat(fieldsData);
  let columnsWithId = fieldsData;

  if (!hideContacts) {
    columnsWithId = columnsWithId.concat({
      name: 'Connected Contacts',
      minWidth: 200,
    });
  }

  columnsWithId = columnsWithId.map((field) => ({
    id: field.name,
    label:
      CONTACT_FIELDS.find((defaultField) => defaultField.name === field.name)
        ?.label || field.name,
    subtext: field.subtext,
    minWidth: field.minWidth || 170,
  }));

  return columnsWithId;
};

export const getFieldValue = ({isEdit, field, itemData, contactId}) => {
  let value;

  if (
    contactId &&
    field.contactSpecificField &&
    itemData?.contactSpecificValues?.length
  ) {
    const cValue = itemData.contactSpecificValues.find(
      (v) => v.contactId === contactId,
    );

    value = cValue?.value;
  } else if (itemData?.value) {
    value = itemData.value; // if itemData.value is empty, get default value from field.value
  } else if (!isEdit) {
    value = field.default;
  }

  if (!value) {
    value = '';
  }

  return value;
};
