import React, {forwardRef} from 'react';
import Box from '@mui/material/Box';

let Container = (props, ref) => <Box ref={ref} {...props} />;

Container = forwardRef(Container);

export default Container;
