import {useMemo} from 'react';
import useQuery from '@macanta/hooks/apollo/useQuery';
import {LIST_MERGE_FIELDS} from '@macanta/graphql/admin';
import {sortArrayByObjectKey} from '@macanta/utils/array';

const useEmailMergeFields = ({types = 'sender,contact', options}) => {
  const {data, loading} = useQuery(LIST_MERGE_FIELDS, {
    variables: {
      types,
    },
    ...options,
  });

  const mergeFields = data?.listMergeFields.mergeFields;

  const [
    memoizedSenderFields,
    memoizedContactFields,
    memoizedDOMergeItems,
  ] = useMemo(() => {
    if (mergeFields) {
      const senderFields = mergeFields.find(
        (f) => f.category === 'Sender Fields',
      );
      const doMergeItems = mergeFields
        .filter(
          (f) => !['Sender Fields', 'Macanta Contact'].includes(f.category),
        )
        .map((f) => ({
          ...f,
          entries: sortArrayByObjectKey(f.entries, 'label'),
        }));

      return [
        senderFields?.entries || [],
        senderFields?.entries.map((f) => {
          const strArr = f.value.split('.');

          return {
            ...f,
            value: '~Contact.' + strArr[1],
          };
        }) || [],
        doMergeItems,
      ];
    }

    return [[], [], []];
  }, [mergeFields]);

  return {
    senderFields: memoizedSenderFields,
    contactFields: memoizedContactFields,
    doMergeItems: memoizedDOMergeItems,
    loading,
  };
};

export default useEmailMergeFields;
