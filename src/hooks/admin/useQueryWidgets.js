import useRetryQuery from '@macanta/hooks/apollo/useRetryQuery';
import useAvailableWidgetsByPermissions from '@macanta/hooks/useAvailableWidgetsByPermissions';
import useValue from '@macanta/hooks/base/useValue';
import {LIST_WIDGETS} from '@macanta/graphql/admin';
import * as Storage from '@macanta/utils/storage';

const useQueryWidgets = (filters, options) => {
  const {userId: loggedInUserId} = Storage.getItem('userDetails');

  const {type} = filters || {};

  const widgetsQuery = useRetryQuery(LIST_WIDGETS, {
    variables: {
      userId: loggedInUserId,
    },
    ...options,
  });

  const addedWidgets = widgetsQuery.data?.listWidgets?.addedWidgets;

  const availableQueryWidgets = useAvailableWidgetsByPermissions({
    type: 'availableQueryWidgets',
    widgets: widgetsQuery.data?.listWidgets?.availableQueryWidgets,
  });

  const items = type === 'available' ? availableQueryWidgets : addedWidgets;

  const [queryBuilders] = useValue(() => {
    return type === 'available'
      ? items
      : items?.filter((item) => {
          return item.type === 'MacantaQuery';
        });
  }, [items]);

  return {...widgetsQuery, items: queryBuilders};
};

export default useQueryWidgets;
