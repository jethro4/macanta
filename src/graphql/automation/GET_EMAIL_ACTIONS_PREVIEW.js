import {gql} from '@apollo/client';

export const EMAIL_ACTIONS_PREVIEW_FRAGMENT = gql`
  fragment emailActionsPreviewFragment on EmailActionsPreview {
    action
    status
    preview
  }
`;

export default gql`
  query getEmailActionsPreview($appName: String!, $apiKey: String!, $id: ID!) {
    getEmailActionsPreview(appName: $appName, apiKey: $apiKey, id: $id) {
      ...emailActionsPreviewFragment
    }
  }
  ${EMAIL_ACTIONS_PREVIEW_FRAGMENT}
`;
