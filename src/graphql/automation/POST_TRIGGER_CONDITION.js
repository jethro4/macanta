import {gql} from '@apollo/client';
import {TRIGGER_CONDITIONS_FRAGMENT} from '@macanta/graphql/automation/GET_TRIGGER_CONDITIONS';

export default gql`
  mutation postTriggerCondition($input: TriggerConditionInput!) {
    postTriggerCondition(input: $input) {
      ...triggerConditionsFragment
    }
  }
  ${TRIGGER_CONDITIONS_FRAGMENT}
`;
