import React from 'react';
import Button from '@macanta/components/Button';
import {newVersionAvailableVar} from '@macanta/graphql/cache/vars';
import {useReactiveVar} from '@apollo/client';
import * as Styled from './styles';
import {setIsNewVersionAvailable} from '@macanta/utils/app';

const NewVersionAlert = () => {
  const newVersionAvailable = useReactiveVar(newVersionAvailableVar);

  const handleClick = () => {
    window.location.reload();
  };

  const handleClose = () => {
    setIsNewVersionAvailable(false);
  };

  return (
    <Styled.Alert
      open={newVersionAvailable}
      message={
        <Button
          style={{
            color: 'white',
            textAlign: 'left',
          }}
          onClick={handleClick}>
          <div
            dangerouslySetInnerHTML={{
              __html: 'An update is available. Click here to reload',
            }}
          />
        </Button>
      }
      onClose={handleClose}
      snackbarProps={{
        anchorOrigin: {
          vertical: 'top',
          horizontal: 'center',
        },
        autoHideDuration: null,
        style: {
          top: 40,
        },
      }}
    />
  );
};

export default NewVersionAlert;
