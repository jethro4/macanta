import {GET_NYLAS_INFO} from '@macanta/graphql/admin';
import {nylasPostponedVar} from '@macanta/graphql/cache/vars';
import {useReactiveVar, useQuery} from '@apollo/client';
import useAccessPermissions from '@macanta/hooks/admin/useAccessPermissions';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import * as Storage from '@macanta/utils/storage';

const useNylasInfo = (options) => {
  const {userId: loggedInUserId} = Storage.getItem('userDetails');

  const nylasPostponed = useReactiveVar(nylasPostponedVar);

  const accessPermissionsQuery = useAccessPermissions({
    key: 'permissionMeta.allowEmailSync',
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const allowEmailSync = accessPermissionsQuery.data;

  const nylasInfoQuery = useQuery(GET_NYLAS_INFO, {
    variables: {
      userId: loggedInUserId,
    },
    skip: !allowEmailSync,
    onError() {},
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
    notifyOnNetworkStatusChange: true,
    ...options,
  });

  const isValidUser = nylasInfoQuery.data?.getNylasInfo?.isValidUser;
  const isValidToken = nylasInfoQuery.data?.getNylasInfo?.isValidToken;

  let status = 'Inactive';

  if (nylasInfoQuery.data?.getNylasInfo) {
    if (!isValidUser) {
      status = 'Invalid User';
    } else if (isValidToken) {
      status = 'Active';
    }
  }

  return {
    loading: nylasInfoQuery.loading,
    allowEmailSync: !nylasInfoQuery.loading && allowEmailSync,
    isValidUser,
    isValidToken,
    status,
    shouldNylasSync:
      allowEmailSync &&
      !nylasPostponed &&
      !nylasInfoQuery.loading &&
      isValidUser &&
      !isValidToken,
  };
};

export default useNylasInfo;
