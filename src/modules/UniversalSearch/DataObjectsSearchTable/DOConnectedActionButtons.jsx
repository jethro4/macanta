import React from 'react';
import VisibilityIcon from '@mui/icons-material/Visibility';
import DOItemEditSection from './DOItemEditSection';
import {PopoverButton} from '@macanta/components/Button';
import MoreOptions from './MoreOptions';
import * as Styled from './styles';

const DOConnectedActionButtons = ({
  item,
  groupId,
  doFields,
  relationships,
  doTitle,
}) => {
  return (
    <>
      <Styled.ActionButtonContainer className="action-buttons-container">
        <PopoverButton
          icon
          sx={{
            padding: '0.25rem',
            color: 'info.main',
          }}
          // onClick={handleAdd}
          size="small"
          TooltipProps={{
            title: 'Show Details',
          }}
          PopoverProps={{
            title: doTitle,
            contentWidth: 1600,
            contentHeight: '86vh',
            anchorOrigin: {
              vertical: 'center',
              horizontal: 'left',
            },
            transformOrigin: {
              vertical: 'center',
              horizontal: 'right',
            },
          }}
          renderContent={() => (
            <DOItemEditSection
              loadInfo
              item={item}
              groupId={groupId}
              fields={doFields}
              relationships={relationships}
            />
          )}>
          <VisibilityIcon />
        </PopoverButton>
        <MoreOptions
          style={{
            color: '#aaa',
          }}
          itemId={item.id}
          groupId={groupId}
          fields={doFields}
          relationships={relationships}
          connectedContacts={item.connectedContacts}
          doTitle={doTitle}
        />
      </Styled.ActionButtonContainer>
    </>
  );
};

export default DOConnectedActionButtons;
