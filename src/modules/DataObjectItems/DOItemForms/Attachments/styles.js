import {experimentalStyled as styled} from '@mui/material/styles';
import Box from '@mui/material/Box';
import CardActionAreaComp from '@mui/material/CardActionArea';
import CardActionsComp from '@mui/material/CardActions';
import Button, {IconButton} from '@macanta/components/Button';
import Typography from '@mui/material/Typography';
import * as applicationStyles from '@macanta/themes/applicationStyles';

export const FileBackdrop = styled(Box)`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
`;

const cardActionsArea = `
  position: relative;

  & .card-actions {
    display: none;
  }

  &:hover {
    & .card-actions {
      display: flex;
      justify-content: space-around;
    }
  }
`;

export const CardActionArea = styled(CardActionAreaComp)`
  position: relative;
`;

export const ItemActionArea = styled(Box)`
  position: relative;
  border-top: 1px solid #ddd;
`;

export const ItemActionButton = styled(Button)`
  ${applicationStyles.getLineText(2)}
  word-break: break-all;
  position: relative;
  width: 100%;
  &:hover {
    background-color: rgba(0, 0, 0, 0.04);
  }
`;

const cardActions = `
  padding: 0;
  position: absolute;
  background-color: rgba(0, 0, 0, 0.5);
`;

export const CardActions = styled(CardActionsComp)`
  ${cardActions}
  bottom: 0;
  width: 100%;
`;

export const ItemActions = styled(Box)`
  ${cardActions}
  top: 50%;
  transform: translateY(-50%);
  right: 1rem;
  border-radius: 4px;
  overflow: hidden;
`;

export const SubButtons = styled(Box)`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 1rem 1rem 0;
`;

export const SubButtonIcons = styled(Box)``;

export const SubButtonIcon = styled(IconButton)``;

export const ActionIconButton = styled(IconButton)`
  flex: 1;
  border-radius: 0;

  &:hover {
    background-color: rgba(0, 0, 0, 0.8);
  }
`;

export const HeaderRightContainer = styled(Box)`
  display: flex;
`;

export const GridItemContainer = styled(Box)`
  ${cardActionsArea}
`;

export const ListItemContainer = styled(Box)`
  ${cardActionsArea}
`;

export const GridItemLabel = styled(Typography)`
  ${applicationStyles.getLineText(2)}
  word-break: break-all;
  font-size: 0.625rem;
  text-align: center;
`;
