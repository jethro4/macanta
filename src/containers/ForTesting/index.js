import {getAppInfo} from '@macanta/utils/app';

const [appName] = getAppInfo();

export const isForTesting = appName === 'staging';
export {default} from './ForTesting';
