import React from 'react';
import AddIcon from '@material-ui/icons/Add';
import DataTable from '@macanta/containers/DataTable';
import {
  useDeleteAutomationsMutation,
  useContactActions,
  usePostContactActionMutation,
} from '@macanta/hooks/automation';
import ItemActionButtons from '@macanta/modules/AdminSettings/Automation/TriggerActions/ItemActionButtons';
import {ContactActionsForm} from '@macanta/modules/AdminSettings/Automation/TriggerActions/ContactActions';
import Section from '@macanta/containers/Section';
import {writeItemInListFragment} from '@macanta/graphql/helpers';
import {CONTACT_ACTION_FRAGMENT} from '@macanta/graphql/automation/GET_CONTACT_ACTIONS';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import EditButton from '@macanta/containers/buttons/EditButton';

const CONTACT_ACTION_TYPE_COLUMNS = [
  {
    id: 'name',
    label: 'Name',
    maxWidth: '30%',
  },
  {
    id: 'description',
    label: 'Description',
    maxWidth: '40%',
  },
  {
    id: 'actionType',
    label: 'Action Type',
    maxWidth: '30%',
  },
  {
    id: 'action',
    label: '',
    disableSort: true,
    disableResize: true,
    minWidth: 100,
    maxWidth: 100,
    renderBodyCellValue: ({item, handlers}) => {
      return (
        <ItemActionButtons
          modal={handlers.modal}
          type="Contact Actions"
          item={item}
          onDelete={handlers.onDelete}
          renderFormComponent={(handleClose) => (
            <ContactActionsForm
              options={{
                variables: {
                  id: item.id,
                },
              }}
              initialValues={item}
              onSubmit={async (values) => {
                const {result} = await handlers.onSave(values);

                if (result) {
                  handleClose();
                }
              }}
            />
          )}
        />
      );
    },
  },
];

const ContactActionsSection = ({modal, ...props}) => {
  const contactActionsQuery = useContactActions();

  const [callPostMutation, actionMutation] = usePostContactActionMutation({
    update(cache, {result}, options) {
      writeItemInListFragment({
        isCreate: !options.variables.input?.id,
        keyField: 'queryId',
        item: result,
        typeName: 'ContactActionItem',
        fragment: CONTACT_ACTION_FRAGMENT,
        parentKey: 'getContactActions',
        listKey: 'items',
        position: 'first',
      });
    },
  });

  const [callDeleteMutation, deleteMutation] = useDeleteAutomationsMutation({
    typeName: 'ContactActionItem',
  });

  const handleSave = async (values) => {
    const isNote = values.actionType === 'Note';

    const variables = {
      id: values.id,
      name: values.name,
      description: values.description,
      doType: values.doTitle,
      actionType: values.actionType,
      contactRelationship: values.contactRelationship,
      fieldValues: isNote
        ? {
            noteTitle: values.title,
            noteText: values.note,
            noteTags: values.tags?.map((tag) => `#${tag}`).join(','),
          }
        : {
            taskTitle: values.title,
            taskText: values.note,
            taskDate: values.actionDate,
            assignType: values.assignType,
            assignTypeValue: values.assignTypeValue,
          },
    };

    return callPostMutation({
      variables,
    });
  };

  const handleDelete = (id) => {
    return callDeleteMutation({
      variables: {
        id,
      },
    });
  };

  return (
    <>
      <Section
        style={{
          margin: 0,
        }}
        fullHeight
        title={`Contact Actions`}
        HeaderRightComp={
          <EditButton
            modal={modal}
            size="small"
            variant="contained"
            startIcon={<AddIcon />}
            PopoverProps={{
              title: `Add Contact Action`,
              contentWidth: 1100,
              contentMinHeight: 350,
              bodyStyle: {
                backgroundColor: '#f5f6fA',
              },
            }}
            renderContent={(handleClose) => (
              <ContactActionsForm
                onSubmit={async (values) => {
                  const {result} = await handleSave(values);

                  if (result) {
                    handleClose();
                  }
                }}
              />
            )}>
            Add Contact Action
          </EditButton>
        }>
        <DataTable
          loading={contactActionsQuery.loading}
          data={contactActionsQuery.result}
          fullHeight
          rowsPerPage={25}
          rowsPerPageOptions={[25, 50, 100]}
          {...props}
          columns={CONTACT_ACTION_TYPE_COLUMNS}
          handlers={{
            modal,
            onSave: handleSave,
            onDelete: handleDelete,
          }}
        />
      </Section>
      <LoadingIndicator
        modal
        loading={actionMutation.loading || deleteMutation.loading}
      />
    </>
  );
};

export default ContactActionsSection;
