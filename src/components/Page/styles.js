import {experimentalStyled as styled} from '@mui/material/styles';

import Box from '@mui/material/Box';

export const Page = styled(Box)`
  height: 100%;
  display: flex;
  flex-direction: column;
`;
