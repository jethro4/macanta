import React from 'react';
import InfoIcon from '@mui/icons-material/Info';
import KeyboardArrowRightIcon from '@mui/icons-material/KeyboardArrowRight';
import {useTheme} from '@mui/material/styles';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import CardActionArea from '@mui/material/CardActionArea';
import CardContent from '@mui/material/CardContent';
import Radio from '@mui/material/Radio';
import Button from '@macanta/components/Button';
import Card, {CardHeader} from '@macanta/components/Card';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import useAppSubscription from '@macanta/hooks/app/useAppSubscription';
import {capitalizeFirstLetter} from '@macanta/utils/string';

const SelectPlanForm = ({
  selectedOption,
  setSelectedOption,
  options,
  onProceed,
}) => {
  const appSubscriptionQuery = useAppSubscription();

  const theme = useTheme();

  const handleClick = (option) => () => {
    setSelectedOption(option);
  };

  const handleProceed = () => {
    window.open(encodeURI(selectedOption.subscriptionUrl));

    onProceed();
  };

  return (
    <Box
      sx={{
        position: 'relative',
        backgroundColor: 'grayLight.main',
        display: 'flex',
        flexDirection: 'column',
      }}>
      {appSubscriptionQuery.loading ? (
        <LoadingIndicator
          style={{
            padding: '1rem',
            alignSelf: 'center',
          }}
          transparent
        />
      ) : (
        <>
          <Box
            sx={{
              padding: '1rem',
              flex: 1,
              display: 'flex',
            }}>
            <InfoIcon
              sx={{
                color: 'info.main',
              }}
            />
            <Typography
              sx={{
                marginLeft: '0.75rem',
                color: 'grayDarkest.main',
                fontSize: '0.875rem',
              }}>
              Your Macanta subscription is inactive, please re-activate your
              subscription to enable this feature. Thank you!
            </Typography>
          </Box>

          <Divider
            style={{
              borderBottom: '1px dashed #ccc',
            }}
          />

          <Box
            sx={{
              padding: '1rem',
              overflowY: 'auto',
              maxHeight: '420px',

              '& > *:not(:last-child)': {
                marginBottom: '1.5rem',
              },
            }}>
            {options.map((option) => {
              const selected = option.id === selectedOption?.id;

              return (
                <Card
                  key={option.id}
                  sx={{
                    '.MuiCardHeader-title': {
                      letterSpacing: 0.2,
                    },
                    '.MuiCardHeader-subheader': {
                      marginTop: '4px',
                      letterSpacing: 0.3,
                    },

                    ...(selected && {
                      border: `1px solid ${theme.palette.secondary.main}`,
                    }),
                  }}>
                  <CardActionArea onClick={handleClick(option)}>
                    <CardHeader
                      sx={{
                        padding: '1rem 1rem 0.5rem',
                      }}
                      title={option.name}
                      subheader={
                        option.periodUnit
                          ? capitalizeFirstLetter(`${option.periodUnit}ly`)
                          : ''
                      }
                      action={
                        <Radio
                          sx={{
                            marginTop: '-7px',
                          }}
                          checked={selected}
                        />
                      }
                    />
                    <CardContent
                      sx={{
                        padding: '0.5rem 1rem',
                      }}>
                      <Typography
                        sx={{color: 'success.main', fontSize: '0.875rem'}}>
                        {`${option.price} ${option.currencyCode} / ${option.periodUnit}`}
                      </Typography>
                    </CardContent>
                  </CardActionArea>
                </Card>
              );
            })}
          </Box>

          <Box
            sx={{
              padding: '1rem',
              display: 'flex',
              justifyContent: 'flex-end',
              borderTop: '1px solid #eee',
              boxShadow: '0px -4px 10px -2px #eee',
            }}>
            <Button
              disabled={!selectedOption}
              size="medium"
              variant="contained"
              endIcon={<KeyboardArrowRightIcon />}
              onClick={handleProceed}>
              Proceed To Subscription
            </Button>
          </Box>
        </>
      )}
    </Box>
  );
};

SelectPlanForm.defaultProps = {
  options: [],
};

export default SelectPlanForm;
