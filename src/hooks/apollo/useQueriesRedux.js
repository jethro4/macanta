import {useEffect, useReducer} from 'react';
import {insert, remove} from '@macanta/utils/array';

const initialState = {queue: []};

const queriesReducer = (state, action) => {
  switch (action.type) {
    case 'addCall': {
      const queue = insert({
        arr: state.queue,
        item: {id: action.payload},
      });

      return {
        queue,
      };
    }
    case 'removeCall': {
      const queue = remove({
        arr: state.queue,
        key: 'id',
        value: action.payload,
      });

      return {
        queue,
      };
    }
    case 'reset':
      return initialState;
    default:
      throw new Error();
  }
};

const useQueriesRedux = ({tagFieldName, variables}) => {
  const [state, dispatch] = useReducer(queriesReducer, initialState);

  useEffect(() => {
    const callId = `${tagFieldName}-${JSON.stringify(variables)}`;

    dispatch({
      type: 'addCall',
      payload: callId,
    });

    return () => {
      dispatch({
        type: 'removeCall',
        payload: callId,
      });
    };
  }, []);

  return state;
};

export default useQueriesRedux;
