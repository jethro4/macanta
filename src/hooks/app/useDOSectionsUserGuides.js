import useValue from '@macanta/hooks/base/useValue';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import useSectionWithPermission from '@macanta/hooks/dataObject/useSectionsWithPermission';
import useUserGuides from '@macanta/hooks/useUserGuides';
import {getDefaultUserGuide} from '@macanta/modules/AdminSettings/UserManagement/Tabs/UserGuidesForm/helpers';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';

const useDOSectionsUserGuides = ({permission}) => {
  const doTypesQuery = useDOTypes({
    removeEmail: true,
    includeContactObject: true,
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });
  const {doSections} = useUserGuides({permission});

  const doTypes = doTypesQuery.data?.listDataObjectTypes;

  const [selectedType, setSelectedType] = useValue(() => {
    return doTypes?.[0];
  }, [doTypes]);

  const sectionsQuery = useSectionWithPermission(
    {
      groupId: selectedType?.id,
      permission,
      isDataObjectType: true,
    },
    {
      fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
    },
  );

  const [guides] = useValue(() => {
    return (
      sectionsQuery.data?.map(({sectionName}) => {
        return (
          doSections.find(
            (guide) =>
              guide.subType === sectionName &&
              guide.meta.some(
                (m) => m.name === 'doType' && m.value === selectedType?.title,
              ),
          ) ||
          getDefaultUserGuide({
            type: 'DataObject',
            subType: sectionName,
            doType: selectedType?.title,
          })
        );
      }) || []
    );
  }, [selectedType, doSections, sectionsQuery.data]);

  const initLoading = doTypesQuery.initLoading || sectionsQuery.initLoading;

  return {doTypes, selectedType, setSelectedType, initLoading, data: guides};
};

export default useDOSectionsUserGuides;
