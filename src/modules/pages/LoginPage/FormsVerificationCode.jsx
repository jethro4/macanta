import React, {useRef} from 'react';

import Typography from '@mui/material/Typography';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import SendIcon from '@mui/icons-material/Send';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import Box from '@mui/material/Box';
import Button from '@macanta/components/Button';
import Form from '@macanta/components/Form';
import {useMutation} from '@apollo/client';
import {FORGOT_PASSWORD} from '@macanta/graphql/auth';
import {verificationCodeValidationSchema} from '@macanta/validations/forgotPassword';
import FormHeader from './FormHeader';
import * as Styled from './styles';

const FormsVerificationCode = ({email, onLogin, onResetPassword}) => {
  const codeRef = useRef();

  const [callForgotPwStep2, forgotPwStep2Mutation] = useMutation(
    FORGOT_PASSWORD,
    {
      onCompleted(data) {
        if (data.forgotPassword?.success && codeRef.current?.code) {
          onResetPassword(codeRef.current);
        }
      },
      onError() {},
    },
  );

  const handleResendLink = () => {
    callForgotPwStep2({
      variables: {
        forgotPasswordInput: {
          step: 1,
          email,
        },
        __mutationkey: 'forgotPasswordInput',
      },
    });

    codeRef.current = {};
  };

  const handleFormSubmit = (values) => {
    callForgotPwStep2({
      variables: {
        forgotPasswordInput: {
          step: 2,
          email,
          code: values.verificationCode,
        },
        __mutationkey: 'forgotPasswordInput',
      },
    });

    codeRef.current = {email, code: values.verificationCode};
  };

  return (
    <>
      <FormHeader
        title="Hey, we sent you an email!"
        description="Please check the verification code to continue."
      />

      <Form
        initialValues={{verificationCode: ''}}
        validationSchema={verificationCodeValidationSchema}
        onSubmit={handleFormSubmit}>
        {({
          values,
          errors,
          handleChange,
          handleSubmit,
          setFieldValue,
          /* and other goodies */
        }) => (
          <>
            <Box
              style={{
                display: 'flex',
                justifyContent: 'center',
              }}>
              <Styled.VerificationTextField
                autoFocus
                size="large"
                error={errors.verificationCode}
                onChange={handleChange('verificationCode')}
                variant="filled"
                value={values.verificationCode}
                fullWidth
                onEnterPress={handleSubmit}
                maxLength={6}
                InputProps={{
                  disableUnderline: true,
                  style: {
                    fontSize: '2rem',
                  },
                }}
              />
            </Box>

            {!!forgotPwStep2Mutation.error && (
              <Box
                style={{
                  display: 'flex',
                  justifyContent: 'center',
                }}>
                <Button
                  onClick={() => {
                    setFieldValue('verificationCode', '');
                    handleResendLink();
                  }}>
                  <Typography
                    color="error"
                    variant="subtitle2"
                    align="center"
                    dangerouslySetInnerHTML={{
                      __html: forgotPwStep2Mutation.error.message,
                    }}
                  />
                </Button>
              </Box>
            )}

            <Styled.GridItem container>
              <Styled.GridItem item xs={12} sm={8}>
                <Styled.ButtonLeft
                  onClick={onLogin}
                  startIcon={
                    <ChevronLeftIcon
                      style={{
                        fontSize: '1.5rem',
                      }}
                    />
                  }>
                  Back to login
                </Styled.ButtonLeft>
              </Styled.GridItem>
              <Styled.GridItem item xs={12} sm={4}>
                <Styled.ButtonRight
                  onClick={handleSubmit}
                  color="primary"
                  variant="contained"
                  startIcon={<SendIcon />}>
                  Next
                </Styled.ButtonRight>
              </Styled.GridItem>
            </Styled.GridItem>
          </>
        )}
      </Form>
      <LoadingIndicator fill loading={forgotPwStep2Mutation.loading} />
    </>
  );
};

export default FormsVerificationCode;
