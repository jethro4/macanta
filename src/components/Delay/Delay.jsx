import {useState, useEffect} from 'react';
import PropTypes from 'prop-types';

const Delay = ({timeout = 10, children}) => {
  const [show, setShow] = useState(false);

  useEffect(() => {
    if (!show) {
      setTimeout(() => {
        setShow(true);
      }, timeout);
    }
  }, []);

  return show ? children : null;
};

Delay.propTypes = {
  timeout: PropTypes.number,
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
  ]),
};

export default Delay;
