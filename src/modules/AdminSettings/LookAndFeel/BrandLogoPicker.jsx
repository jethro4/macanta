import React, {useState, useEffect} from 'react';
import {useFormikContext} from 'formik';
import Form from '@macanta/components/Form';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import Section from '@macanta/containers/Section';
import UploadForms from '@macanta/modules/DataObjectItems/DOItemForms/Attachments/UploadForms';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import useAppSettings from '@macanta/hooks/admin/useAppSettings';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {useMutation} from '@apollo/client';
import {GET_APP_SETTINGS, UPDATE_APP_SETTINGS} from '@macanta/graphql/app';
import {getBase64Prefix} from '@macanta/utils/file';
import * as Styled from './styles';

const transformImageAttachment = (base64Url = '', filename = '') => {
  const fileExt = filename.split('.').pop();

  return {
    id: Date.now(),
    isNew: false,
    thumbnail: base64Url.replace(getBase64Prefix(fileExt), ''),
    fileName: filename,
    fileExt,
    isPDF: false,
    mimeType: 'image/png',
  };
};

const BrandLogoPickerContainer = () => {
  const {displayMessage} = useProgressAlert();

  const appSettingsQuery = useAppSettings({
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const {imageData: companyLogo, filename} =
    appSettingsQuery.data?.customLogo || {};

  const [initValues, setInitValues] = useState({
    attachments: [transformImageAttachment(companyLogo, filename)],
  });

  const [callUpdateAppSettings, updateAppSettingsMutation] = useMutation(
    UPDATE_APP_SETTINGS,
    {
      onError() {},
      onCompleted: async (data) => {
        if (data?.updateAppSettings?.success) {
          displayMessage('Company logo updated!');
        }
      },
    },
  );

  const handleFormSubmit = async (values) => {
    const {thumbnail: value, fileName, mimeType} = values.attachments[0];

    const {data} = await callUpdateAppSettings({
      variables: {
        updateAppSettingsInput: {
          type: 'macanta_custom_logo',
          value,
          fileName,
          mimeType,
        },
        __mutationkey: 'updateAppSettingsInput',
      },
    });

    if (data?.updateAppSettings?.success) {
      setInitValues({
        attachments: [transformImageAttachment(value, filename)],
      });

      appSettingsQuery.client.writeQuery({
        query: GET_APP_SETTINGS,
        data: {
          getAppSettings: {
            __typename: 'AppSettings',
            ...appSettingsQuery.data,
            customLogo: {
              imageData: value,
              filename: fileName,
              type: mimeType,
            },
          },
        },
      });
    }
  };

  useEffect(() => {
    if (
      initValues.attachments[0] &&
      initValues.attachments[0].thumbnail !== companyLogo
    ) {
      setInitValues({
        attachments: [transformImageAttachment(companyLogo, filename)],
      });
    }
  }, [companyLogo]);

  return (
    <>
      <Form
        enableReinitialize
        initialValues={initValues}
        onSubmit={handleFormSubmit}>
        <BrandLogoPicker />
      </Form>
      <LoadingIndicator modal loading={updateAppSettingsMutation.loading} />
    </>
  );
};

const BrandLogoPicker = () => {
  const {values, setFieldValue, handleSubmit, dirty} = useFormikContext();

  const handleChange = (filesWithBase64Url) => {
    setFieldValue(
      'attachments',
      filesWithBase64Url[0]
        ? [
            transformImageAttachment(
              filesWithBase64Url[0].thumbnail,
              filesWithBase64Url[0].fileName,
            ),
          ]
        : [],
    );
  };

  return (
    <>
      <Section
        fullHeight
        headerStyle={{
          backgroundColor: 'white',
        }}
        bodyStyle={{
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'space-between',
        }}
        style={{
          marginTop: 0,
        }}
        title="Choose Your Logo">
        <Styled.LogoPickerContainer>
          <UploadForms
            acceptedFiles={['image/*']}
            attachments={values.attachments}
            onChange={handleChange}
            onSubmit={handleSubmit}
            uploadLabel="Save Logo"
            style={{
              padding: 0,
            }}
            containerStyle={{
              padding: '1rem',
            }}
            imageContainerStyle={{
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
              padding: '0 8px',
            }}
            footerStyle={{
              padding: '1rem 1rem 0',
              borderTop: '1px solid #eee',
              boxShadow: '0px -4px 10px -2px #eee',
            }}
            filesLimit={1}
            uploadButtonProps={{
              disabled: !dirty || !values.attachments.length,
            }}
            gridProps={{
              disableDelete: true,
              hideFilename: true,
              style: {
                display: 'flex',
                justifyContent: 'center',
                marginTop: '1rem',
              },
              actionContainerStyle: {
                paddingTop: 0,
              },
            }}
          />
        </Styled.LogoPickerContainer>
      </Section>
    </>
  );
};

export default BrandLogoPickerContainer;
