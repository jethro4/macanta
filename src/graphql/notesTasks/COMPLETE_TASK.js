import {gql} from '@apollo/client';
import {TASK_ATTRIBUTES} from './fragments';

export default gql`
  mutation completeTask($input: CompleteTaskInput!) {
    completeTask(input: $input) {
      ...taskResults
    }
  }
  ${TASK_ATTRIBUTES}
`;
