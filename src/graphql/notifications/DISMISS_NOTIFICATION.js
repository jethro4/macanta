import {gql} from '@apollo/client';

export default gql`
  mutation dismissNotification(
    $dismissNotificationInput: DismissNotificationInput!
  ) {
    dismissNotification(input: $dismissNotificationInput) {
      success
      notificationId
    }
  }
`;
