import React, {useState, useEffect} from 'react';
import {useFormikContext} from 'formik';
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import Button from '@macanta/components/Button';
import FormField from '@macanta/containers/FormField';
import Form from '@macanta/components/Form';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import usePrevious from '@macanta/hooks/usePrevious';
import {useQuery, useMutation} from '@apollo/client';
import {
  GET_LIST_ASSIGNMENTS,
  UNASSIGN_LIST_USERS,
  LIST_ASSIGNMENT_FRAGMENT,
} from '@macanta/graphql/admin';
import * as NoteTaskFormsStyled from '@macanta/modules/NoteTaskForms/styles';
import * as Styled from './styles';

const UNASSIGNMENT_ACTIONS = [
  {
    label: 'Un-Assign Active Items',
    value: 'active',
  },
  {
    label: 'Un-Assign Historical Items',
    value: 'historical',
  },
  {
    label: 'Un-Assign Both Active and Historical Items',
    value: 'both',
  },
];

const ManualUnassignmentFormsContainer = ({
  id,
  onSuccess,
  onGoToAssignList,
  ...props
}) => {
  const {displayMessage} = useProgressAlert();

  const listAssignmentQuery = useQuery(GET_LIST_ASSIGNMENTS, {
    variables: {
      queryId: id,
    },
    skip: !id,
  });

  const listAssignment = listAssignmentQuery.data?.getListAssignments;

  const getInitValues = () => {
    return {
      queryId: id,
      action: '',
      assignedRelationship: listAssignment?.assignedRelationship || '',
    };
  };

  const [initValues, setInitValues] = useState(() => getInitValues());
  const prevInitValues = usePrevious(initValues);

  const [
    callUnassignListUsers,
    {client, ...unassignListUsersMutation},
  ] = useMutation(UNASSIGN_LIST_USERS, {
    onError() {},
    onCompleted: (data) => {
      if (data?.unassignListUsers) {
        displayMessage('Un-assign successfully!');

        client.writeFragment({
          id: `ListAssignments:assignment-${id}`,
          fragment: LIST_ASSIGNMENT_FRAGMENT,
          data: {
            status: 'inactive',
          },
        });

        onSuccess && onSuccess();
      }
    },
  });

  const handleFormSubmit = async (values) => {
    callUnassignListUsers({
      variables: {
        unassignListUsersInput: {
          queryId: values.queryId,
          action: values.action,
        },
        __mutationkey: 'unassignListUsersInput',
      },
    });
  };

  useEffect(() => {
    if (listAssignment && prevInitValues === initValues) {
      setInitValues(getInitValues());
    }
  }, [listAssignment]);

  const initLoading = !listAssignment && listAssignmentQuery.loading;

  return (
    <>
      <Form
        initialValues={initValues}
        enableReinitialize={true}
        // validationSchema={queryBuilderValidationSchema}
        onSubmit={handleFormSubmit}>
        {({values}) => {
          return (
            <ManualUnassignmentForms
              item={values}
              hideLoading={initLoading}
              loadingListAssignment={listAssignmentQuery.loading}
              errorUnassignment={unassignListUsersMutation.error}
              onGoToAssignList={onGoToAssignList}
              {...props}
            />
          );
        }}
      </Form>
      <LoadingIndicator fill backdrop loading={initLoading} />
      <LoadingIndicator modal loading={unassignListUsersMutation.loading} />
    </>
  );
};

const ManualUnassignmentForms = ({
  item,
  hideLoading,
  loadingListAssignment,
  errorUnassignment,
  onGoToAssignList,
  ...props
}) => {
  const {errors, handleChange, handleSubmit, dirty} = useFormikContext();

  const handleRenderValue = (choices) => (val) => {
    const selected = choices?.find(({value}) => value === val);

    return selected?.label;
  };

  const loading = !hideLoading && loadingListAssignment;
  const noAssignment = !hideLoading && !item?.assignedRelationship;

  // if (!item?.assignedRelationship) {
  //   return (
  //     <TableStyled.EmptyMessageContainer {...props}>
  //       <Typography align="center" color="#888">
  //         No list assigned
  //       </Typography>
  //     </TableStyled.EmptyMessageContainer>
  //   );
  // }

  return (
    <Styled.FormRoot
      style={{
        backgroundColor: 'white',
      }}
      {...props}>
      <Styled.Body
        style={{
          padding: '1rem 0',
        }}>
        <FormField
          style={{
            padding: '0 1rem',
          }}
          disabled={!item?.assignedRelationship}
          {...(noAssignment && {
            placeholder: 'Cannot un-assign non existing assignment',
          })}
          type="Select"
          options={UNASSIGNMENT_ACTIONS}
          value={item?.action}
          error={errors.action}
          onChange={handleChange('action')}
          label="Un-Assignment Action"
          fullWidth
          size="small"
          variant="outlined"
          renderValue={handleRenderValue(UNASSIGNMENT_ACTIONS)}
        />

        <FormField
          disabled
          style={{
            padding: '0 1rem',
          }}
          value={item?.assignedRelationship || 'N/A'}
          error={errors.assignedRelationship}
          label="Assigned Relationship"
          fullWidth
          size="small"
          variant="outlined"
        />

        <LoadingIndicator
          fill
          align="top"
          style={{
            top: '2.5rem',
          }}
          loading={loading}
        />
      </Styled.Body>
      <NoteTaskFormsStyled.Footer
        style={{
          padding: '12px 1rem',
          margin: 0,
          borderTop: '1px solid #eee',
          width: '100%',
          backgroundColor: 'white',
        }}>
        <Box
          style={{
            flex: 1,
            display: 'flex',
            justifyContent: noAssignment ? 'flex-start' : 'center',
            alignItems: 'center',
          }}>
          {noAssignment && (
            <Button onClick={onGoToAssignList} size="medium">
              Go To Assign List
            </Button>
          )}
          {!!errorUnassignment && (
            <Typography
              color="error"
              style={{
                fontSize: '0.75rem',
                textAlign: 'center',
                marginRight: '1rem',
              }}>
              {errorUnassignment.message}
            </Typography>
          )}
        </Box>
        <Button
          disabled={!dirty}
          variant="contained"
          startIcon={<RemoveCircleIcon />}
          onClick={handleSubmit}
          size="medium">
          Un-Assign
        </Button>
      </NoteTaskFormsStyled.Footer>
    </Styled.FormRoot>
  );
};

export default ManualUnassignmentFormsContainer;
