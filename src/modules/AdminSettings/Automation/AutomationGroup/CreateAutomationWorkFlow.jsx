import React, {useState} from 'react';
import {navigate} from 'gatsby';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import Typography from '@material-ui/core/Typography';
// eslint-disable-next-line import/no-named-as-default
import Section from '@macanta/containers/Section';
import {ROOT_PATH_ADMIN} from '@macanta/modules/AdminSettings/AdminSettings';
import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import * as AdminSettingsStyled from '@macanta/modules/AdminSettings/styles';
import Stepper from '@macanta/components/Stepper';
import {Step, StepLabel, Button} from '@material-ui/core';
import SettingCard from '@macanta/modules/UserSettings/SettingCard';
import * as UserSettingsStyled from '@macanta/modules/UserSettings/styles';
import FormField from '@macanta/containers/FormField';
import Box from '@material-ui/core/Box';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import * as FormStyled from '@macanta/modules/NoteTaskForms/styles';
import Form from '@macanta/components/Form';
import StartAutomationGroupBtn from './StartAutomationGroupBtn';
import ArrowRight from '@material-ui/icons/ArrowRight';

const steps = [
  {label: 'If This..'},
  {label: 'Wait Until..'},
  {label: 'Then That..'},
  {label: 'And Also..'},
  {label: 'Workflow Overview'},
];
const initialValues = {
  workFlowAction: '',
};
const CreateAutomationWorkFlow = () => {
  /* const navigate = useNavigate();*/
  const handleBack = () => {
    navigate(ROOT_PATH_ADMIN);
  };
  const toAutomation = () => {
    navigate('/app/admin-settings/automation');
  };
  const triggerConditionOptions = [
    {
      label: 'Create A New Trigger Condition',
      value: 'create',
    },
    {
      label: 'Select From Existing Trigger Conditions',
      value: 'select',
    },
    {
      label: 'Duplicated An Existing Trigger Condition',
      value: 'duplicate',
    },
  ];
  const [step, setStep] = useState(0);
  const handleStepBack = () => {
    setStep(step - 1);
  };

  const handleStepProceed = () => {
    setStep(step + 1);
  };
  return (
    <>
      <Form
        onSubmit={(values) => {
          setTimeout(() => {
            alert(JSON.stringify(values, null, 2));
          }, 500);
        }}
        initialValues={initialValues}
        enableReinitialize={true}>
        {({handleChange, values}) => {
          return (
            <>
              <Section
                fullHeight
                bodyStyle={{
                  backgroundColor: '#f5f6fa',
                  padding: '1rem 0',
                }}
                HeaderLeftComp={
                  <Breadcrumbs aria-label="breadcrumb">
                    <AdminSettingsStyled.TitleCrumbBtn
                      startIcon={
                        <ArrowBackIcon
                          style={{
                            color: '#333',
                          }}
                        />
                      }
                      onClick={handleBack}
                      disabled={false}>
                      <Typography color="textPrimary">
                        Admin Settings
                      </Typography>
                    </AdminSettingsStyled.TitleCrumbBtn>
                    <AdminSettingsStyled.TitleCrumbBtn
                      onClick={toAutomation}
                      disabled={false}>
                      <Typography color="textPrimary">Automation</Typography>
                    </AdminSettingsStyled.TitleCrumbBtn>
                    <Typography>Create a New Workflow</Typography>
                  </Breadcrumbs>
                }>
                <NoteTaskHistoryStyled.FullHeightGrid container>
                  <NoteTaskHistoryStyled.FullHeightFlexGridColumn
                    item
                    xs={12}
                    md={12}>
                    <Stepper activeStep={step} alternativeLabel steps={steps}>
                      {steps.map(({label}) => (
                        <Step key={label}>
                          <StepLabel>{label}</StepLabel>
                        </Step>
                      ))}
                    </Stepper>
                    <UserSettingsStyled.GridItem
                      item
                      lg={12}
                      md={12}
                      sm={12}
                      xs={12}>
                      <SettingCard
                        title="IF Trigger Condition"
                        description="The System will make sure to match these condition to run the Trigger Actions."
                        style={{
                          padding: '1rem',
                          margin: 0,
                          width: '100%',
                          justifyContent: 'space-between',
                        }}
                        BodyComp={
                          <>
                            <FormField
                              name={'workFlowAction'}
                              style={{
                                marginBottom: 0,
                              }}
                              type={'Select'}
                              onChange={handleChange}
                              options={triggerConditionOptions}
                              value={''}
                              placeholder="Start By"
                              fullWidth
                              size="small"
                              variant="outlined"
                            />
                            <StartAutomationGroupBtn
                              style={{
                                float: 'right',
                                padding: '7px 1rem',
                                marginTop: '10px',
                                borderTop: '1px solid #eee',
                              }}
                              variant="contained"
                              startIcon={<ArrowRight />}
                              size="medium"
                              type="submit"
                              {...values}
                            />
                            {/* <Button
                              style={{
                                float: 'right',
                                padding: '7px 1rem',
                                marginTop: '10px',
                                borderTop: '1px solid #eee',
                              }}
                              variant="contained"
                              size="medium"
                              type="submit">
                              Start
                            </Button> */}
                          </>
                        }
                      />
                    </UserSettingsStyled.GridItem>
                    <FormStyled.Footer
                      style={{
                        margin: 0,
                        padding: '1rem',
                        justifyContent: 'space-between',
                        borderTop: '1px solid #eee',
                        boxShadow: '0px -4px 10px -2px #eee',
                      }}>
                      <Box
                        style={{
                          width: 150,
                        }}>
                        {step > 0 && (
                          <Button
                            variant="outlined"
                            startIcon={<KeyboardArrowLeftIcon />}
                            onClick={handleStepBack}
                            size="medium">
                            Back
                          </Button>
                        )}
                      </Box>
                      <Box
                        style={{
                          width: 150,
                          display: 'flex',
                          justifyContent: 'flex-end',
                        }}>
                        <Button
                          variant="contained"
                          endIcon={<KeyboardArrowRightIcon />}
                          onClick={handleStepProceed}
                          size="medium">
                          Proceed
                        </Button>
                      </Box>
                    </FormStyled.Footer>
                  </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
                </NoteTaskHistoryStyled.FullHeightGrid>
              </Section>
            </>
          );
        }}
      </Form>
    </>
  );
};

export default CreateAutomationWorkFlow;
