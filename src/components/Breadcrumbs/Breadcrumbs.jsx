import React from 'react';
import {navigate} from 'gatsby';
import BreadcrumbsComp from '@material-ui/core/Breadcrumbs';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import Typography from '@material-ui/core/Typography';

import * as AdminSettingsStyled from '@macanta/modules/AdminSettings/styles';

const Breadcrumbs = ({title, backTitle, backPath, ...props}) => {
  const handleBack = () => {
    navigate(backPath);
  };

  let backComp = <Typography color="textPrimary">{backTitle}</Typography>;

  if (backPath) {
    backComp = (
      <AdminSettingsStyled.TitleCrumbBtn
        startIcon={
          <ArrowBackIcon
            style={{
              color: '#333',
            }}
          />
        }
        onClick={handleBack}
        disabled={false}>
        {backComp}
      </AdminSettingsStyled.TitleCrumbBtn>
    );
  }

  return (
    <BreadcrumbsComp aria-label="breadcrumb" {...props}>
      {backComp}
      <Typography>{title}</Typography>
    </BreadcrumbsComp>
  );
};

export default Breadcrumbs;
