import * as yup from 'yup';
import {getAppInfo} from '@macanta/utils/app';
import * as Storage from '@macanta/utils/storage';
import envConfig from '@macanta/config/envConfig';

const validateEmail = (value) => {
  let error = false;

  if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
    error = true;
  }

  return error;
};

const checkPhoneIsValid = ({initValues, isEdit}) =>
  async function (value) {
    const isSame = isEdit && initValues[this.path] === value;

    if (isSame) {
      return true;
    }

    if (value) {
      try {
        const [appName, apiKey] = getAppInfo();
        const {userId: loggedInUserId} = Storage.getItem('userDetails');
        const contactId = this.parent?.id;

        let checkValidPhoneUrl = `https://${appName}.${
          envConfig.apiDomain
        }/rest/v1/validate_phone?userId=${loggedInUserId}&Phone=${encodeURIComponent(
          value,
        )}&api_key=${apiKey}`;

        if (contactId) {
          checkValidPhoneUrl += `&contactId=${contactId}`;
        }

        const res = await fetch(checkValidPhoneUrl);
        const response = await res.json();

        if (response?.CountryPrefix) {
          this.parent.countryCode = response.CountryPrefix;
        }
        if (
          (response?.IsValid && response?.IsValid === 'Yes') ||
          response?.IsValid === 'Maybe'
        ) {
          return true;
        }

        return false;
      } catch (err) {
        console.error('FETCH ERROR checkPhoneIsValid', err);
        return false;
      }
    }

    return true;
  };

const checkEmailUniqueAndExists = ({initValues, isEdit}) =>
  async function (valueArg) {
    const isSame = isEdit && initValues[this.path] === valueArg;

    if (isSame) {
      return true;
    }

    const value = valueArg?.trim();

    this.parent[this.path] = value;

    if (this.parent.isTempEmail) {
      return true;
    } else if (value) {
      try {
        const [appName, apiKey] = getAppInfo();
        const {userId: loggedInUserId} = Storage.getItem('userDetails');

        const checkEmailExistUrl = `https://${appName}.${envConfig.apiDomain}/rest/v1/validate_email?userId=${loggedInUserId}&Email=${value}&api_key=${apiKey}`;

        const res = await fetch(checkEmailExistUrl);
        const response = await res.json();

        if (response.format_valid === 'Invalid Format') {
          return this.createError({
            message: `Invalid Email`,
          });
        } else if (response.mx_found === false) {
          return false;
        } else if (!validateEmail(value)) {
          const checkEmailUniqueUrl = `https://${appName}.${envConfig.apiDomain}/rest/v1/user_info/?access_key=${apiKey}&Email=${value}`;

          const res2 = await fetch(checkEmailUniqueUrl);
          const response2 = await res2.json();

          if (response2?.Data?.Id) {
            return false;
          }
        }

        return true;
      } catch (err) {
        console.error('FETCH ERROR checkEmailUnique', err);
        return false;
      }
    } else {
      return this.createError({
        message: `Required`,
      });
    }
  };

const contactValidationSchema = ({initValues, isEdit}) =>
  yup.object().shape({
    firstName: yup.string().required('Required'),
    lastName: yup.string(),
    email: yup
      .string()
      .test(
        'checkEmailUniqueAndExists',
        'This email is already registered or does not exist',
        checkEmailUniqueAndExists({initValues, isEdit}),
      ),
    company: yup.string(),
    phone0: yup
      .string()
      .test(
        'checkPhoneIsValid',
        'This number is invalid',
        checkPhoneIsValid({initValues, isEdit}),
      ),
    phone1: yup
      .string()
      .test(
        'checkPhoneIsValid',
        'This number is invalid',
        checkPhoneIsValid({initValues, isEdit}),
      ),
    phone2: yup
      .string()
      .test(
        'checkPhoneIsValid',
        'This number is invalid',
        checkPhoneIsValid({initValues, isEdit}),
      ),
  });

export default contactValidationSchema;
