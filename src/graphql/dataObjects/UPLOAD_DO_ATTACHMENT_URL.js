import {gql} from '@apollo/client';

export default gql`
  mutation uploadDOAttachmentUrl(
    $uploadDOAttachmentUrlInput: UploadDONoteAttachmentUrlInput!
  ) {
    uploadDOAttachmentUrl(input: $uploadDOAttachmentUrlInput) {
      success
      fileId
      fileName
      thumbnail
      downloadUrl
    }
  }
`;
