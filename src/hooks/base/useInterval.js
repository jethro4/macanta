import {useState, useEffect, useRef} from 'react';
import moment from 'moment';
import useAppState from './useAppState';
import useNextEffect from './useNextEffect';
import {getNowISOString, diffSeconds} from '@macanta/utils/time';

const DEFAULT_OPTIONS = {
  leading: true,
  interval: 1000,
  maxCount: 0,
  reverseCount: true,
  resumeTriggerDuration: 0,
  callback: () => {},
  skip: false,
};

const useInterval = (optionsArg) => {
  const {
    leading,
    interval,
    maxCount,
    reverseCount,
    resumeTriggerDuration,
    callback,
    skip,
  } = Object.assign(DEFAULT_OPTIONS, optionsArg);

  const appState = useAppState();

  const [startedTime, setStartedTime] = useState('');
  const [triggeredTime, setTriggeredTime] = useState('');

  const intervalRef = useRef(null);

  const secsDiff =
    !startedTime || !triggeredTime
      ? 0
      : diffSeconds(startedTime, triggeredTime);
  const count = !reverseCount ? secsDiff : maxCount - secsDiff;
  const enableTrigger = !skip && (!maxCount || secsDiff < maxCount);
  const done = secsDiff === maxCount;

  const handleCallback = () => {
    setTriggeredTime(getNowISOString());

    callback();
  };

  const startInterval = (opts) => {
    const {callInitial} = Object.assign({callInitial: true}, opts);

    stopInterval();

    if (leading && callInitial) {
      handleCallback();
    }

    setStartedTime(getNowISOString());

    intervalRef.current = setInterval(() => {
      handleCallback();
    }, interval);
  };

  const stopInterval = () => {
    clearInterval(intervalRef.current);

    intervalRef.current = null;
  };

  useNextEffect(() => {
    !enableTrigger ? stopInterval() : startInterval();
  }, [enableTrigger]);

  useEffect(() => {
    if (!enableTrigger) {
      return;
    }

    startInterval();

    let timeStart = moment();

    const handleForeground = () => {
      const hiddenDuration = moment().diff(timeStart, 'millisecond');

      startInterval({
        callInitial:
          !resumeTriggerDuration || hiddenDuration >= resumeTriggerDuration,
      });
    };

    const handleBackground = () => {
      timeStart = moment();

      stopInterval();
    };

    appState.addListener('foreground', handleForeground);
    appState.addListener('background', handleBackground);

    return () => {
      stopInterval();
    };
  }, []);

  return {startedTime, triggeredTime, count, done};
};

export default useInterval;
