import {useMemo} from 'react';
import useBreakpoints from '@macanta/hooks/base/useBreakpoints';
import {getGroupedItems, getDataGrid, getCols} from './helpers';

const useGridLayout = ({data, groupBy: groupByProp, maxColumn, responsive}) => {
  const {
    breakpoints,
    filteredBreakpoints,
    currentBreakpoint,
  } = useBreakpoints();

  const {
    breakpoints: finalBreakpoints,
    itemsWithDataGrid,
    cols,
    compactType,
  } = useMemo(() => {
    if (data?.length) {
      if (!responsive) {
        const groupedItems = !groupByProp
          ? data.map((item, index) => ({
              ...item,
              dataGrid: getDataGrid({
                id: item.id,
                index,
                maxColumn,
              }),
            }))
          : getGroupedItems(data, groupByProp);

        return {
          itemsWithDataGrid: groupedItems,
          cols: maxColumn || Object.keys(breakpoints).length,
        };
      }

      return {
        breakpoints: filteredBreakpoints,
        itemsWithDataGrid: data.map((item, index) => ({
          ...item,
          dataGrid: getDataGrid({
            id: item.id,
            index,
            maxColumn: maxColumn || Object.keys(filteredBreakpoints).length,
          }),
        })),
        cols: getCols(filteredBreakpoints, maxColumn),
      };
    }

    return {
      breakpoints: !responsive ? {} : breakpoints,
      itemsWithDataGrid: [],
      cols: !responsive ? maxColumn : getCols(filteredBreakpoints, maxColumn),
    };
  }, [data, currentBreakpoint]);

  return {
    data: itemsWithDataGrid,
    currentBreakpoint,
    breakpoints: finalBreakpoints,
    cols,
    compactType,
  };
};

export default useGridLayout;
