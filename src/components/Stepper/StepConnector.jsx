import React from 'react';
import * as Styled from './styles';

const StepConnector = (props) => {
  return <Styled.StepConnector {...props} />;
};

export default StepConnector;
