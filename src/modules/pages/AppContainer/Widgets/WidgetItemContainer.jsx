import React from 'react';
import range from 'lodash/range';
import useScrollableAreaOffsets from '@macanta/components/Layout/useScrollableAreaOffsets';
import WidgetItem from '@macanta/modules/pages/AppContainer/Widgets/WidgetItem';

const ROW_HEIGHT = 236;

const WidgetItemContainer = (props) => {
  const {highestY} = useScrollableAreaOffsets();

  const itemLayout = props.layout?.find((l) => l.i === props.widget.id);
  const itemLayoutY = itemLayout?.y || 0;
  const factorY = parseInt(highestY / ROW_HEIGHT);
  const shownItemsYArr = range(0, factorY + 4);
  const showCard = shownItemsYArr.includes(itemLayoutY);

  return <WidgetItem pending={!showCard} {...props} />;
};

export default WidgetItemContainer;
