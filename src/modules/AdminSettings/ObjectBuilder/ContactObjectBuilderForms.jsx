import React, {useState, useEffect, useMemo} from 'react';
import KeyboardArrowLeftIcon from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@mui/icons-material/KeyboardArrowRight';
import TurnedInIcon from '@mui/icons-material/TurnedIn';
import RestartAltIcon from '@mui/icons-material/RestartAlt';
import {useTheme} from '@mui/material/styles';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Button, {DialogButton} from '@macanta/components/Button';
import ButtonGroup from '@macanta/components/ButtonGroup';
import Form from '@macanta/components/Form';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import Section from '@macanta/containers/Section';
import Show from '@macanta/containers/Show';
import useDORelationships from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDORelationships';
import useContactFields from '@macanta/modules/ContactDetails/useContactFields';
import {getSectionsData} from '@macanta/selectors/field.selector';
import useDOTypeMutation from '@macanta/hooks/dataObject/useDOTypeMutation';
import {sortArrayByObjectKey, sortArrayByPriority} from '@macanta/utils/array';
import {CONTACT_FIELDS} from '@macanta/constants/contactFields';
// import {AUTO_ASSIGN_OPTIONS} from './DORelationships/DORelationshipForms';
import ContactSectionsAndFieldsSection from './DOSectionsAndFields/ContactSectionsAndFieldsSection';
import DODisplaySettings from './DODisplaySettings/DODisplaySettings';
import * as DOStyled from '@macanta/modules/DataObjectItems/styles';
import * as FormStyled from '@macanta/modules/NoteTaskForms/styles';

const OBJECT_BUILDER_FORM_TABS = [
  {
    label: 'Sections & Fields',
    value: 'sectionFields',
  },
  {
    label: 'Display Settings',
    value: 'displaySettings',
  },
];

const DEFAULT_COLUMN_1 = [
  'FirstName',
  'MiddleName',
  'LastName',
  'Birthday',
  'Title',
  'JobTitle',
  'Company',
  'Website',
];

const DEFAULT_COLUMN_2 = [
  'Email',
  'EmailAddress2',
  'EmailAddress3',
  'Phone1',
  'Phone2',
  'Phone3',
  'Phone1Ext',
  'Phone2Ext',
  'Phone3Ext',
];

const DEFAULT_COLUMN_3 = [
  'StreetAddress1',
  'StreetAddress2',
  'City',
  'State',
  'PostalCode',
  'Country',
];

const DEFAULT_COLUMN_4 = [
  'Address2Street1',
  'Address2Street2',
  'City2',
  'State2',
  'PostalCode2',
  'Country2',
];

const DEFAULT_COLUMNS = {
  'Column 1': DEFAULT_COLUMN_1,
  'Column 2': DEFAULT_COLUMN_2,
  'Column 3': DEFAULT_COLUMN_3,
  'Column 4': DEFAULT_COLUMN_4,
};

const getDefaultSubGroup = (name) => {
  if (DEFAULT_COLUMN_1.includes(name)) {
    return 'Column 1';
  } else if (DEFAULT_COLUMN_2.includes(name)) {
    return 'Column 2';
  } else if (DEFAULT_COLUMN_3.includes(name)) {
    return 'Column 3';
  } else if (DEFAULT_COLUMN_4.includes(name)) {
    return 'Column 4';
  }
};

const ContactObjectBuilderForms = ({item, onSave}) => {
  const theme = useTheme();

  const [step, setStep] = useState(0);

  const selectedTab = OBJECT_BUILDER_FORM_TABS[step].value;

  const relationshipsQuery = useDORelationships(item?.id);
  const doTypeMutation = useDOTypeMutation();
  const contactCustomFieldsQuery = useContactFields(
    null,
    'contactCustomFields',
    {
      removeEmail: true,
    },
  );
  const contactDefaultFieldsQuery = useContactFields(
    null,
    'contactDefaultFields',
    {
      removeEmail: true,
    },
  );

  const isNew = !item;
  const relationships = relationshipsQuery.data?.listRelationships;
  const contactCustomFields =
    contactCustomFieldsQuery.data?.listDataObjectFields;
  const contactDefaultFields =
    contactDefaultFieldsQuery.data?.listDataObjectFields;
  const currentIndex = OBJECT_BUILDER_FORM_TABS.findIndex(
    (d) => d.value === selectedTab,
  );
  const isFirstStep = currentIndex === 0;
  const isLastStep = currentIndex === OBJECT_BUILDER_FORM_TABS.length - 1;
  const isBack = !isFirstStep;
  const isNext = !isLastStep;

  const handleSelectStep = (newIndex) => () => {
    setStep(newIndex);
  };

  const handleBack = () => {
    setStep((state) => state - 1);
  };

  const handleNext = () => {
    setStep((state) => state + 1);
  };

  const handleSave = async (values) => {
    const variables = {
      id: values.id,
      name: values.name,
      // relationships: values.relationships.map((d) => ({
      //   role: d.role,
      //   exclusive: d.exclusive || 'no',
      //   limit: d.limit || 0,
      //   autoAssignLoggedInUser: d.autoAssignLoggedInUser || false,
      //   autoAssignContact: d.autoAssignContact || false,
      // })),
      fieldSections: values.sectionsData.map((section, sectionIndex) => ({
        name: section.sectionName,
        position: sectionIndex + 1,
        subSections: section.subGroups.map((subGroup, subGroupIndex) => ({
          name: subGroup.subGroupName,
          position: subGroupIndex + 1,
          fields: subGroup.data.map((d) => ({
            ...(!d.isNew && {
              id: d.id,
            }),
            name: d.name,
            type: d.type === 'DataReference' ? d.dataReference : d.type,
            helpText: d.helpText,
            placeholder: d.placeholder,
            default: d.default,
            required: d.required,
            showInTable: d.showInTable,
            showOrder: d.showOrder,
            choices: d.choices,
            contactSpecificField: d.contactSpecificField,
            headingText: d.headingText,
            addDivider: d.addDivider,
            fieldApplicableTo: d.fieldApplicableTo,
          })),
        })),
      })),
    };

    await doTypeMutation.save(variables, values.fieldsToDelete);

    onSave && onSave();
  };

  useEffect(() => {
    if (selectedTab) {
      setTimeout(function () {
        window.dispatchEvent(new Event('resize'));
      }, 0);
    }
  }, [selectedTab]);

  const initValues = useMemo(() => {
    let customFieldsData = [];
    let defaultFieldsData = [];

    if (contactCustomFields) {
      customFieldsData = getSectionsData(
        contactCustomFields
          .map((field) => ({
            ...field,
            fieldApplicableTo: field.fieldApplicableTo || 'All',
          }))
          .filter(
            (field) => !['LastUpdated', 'LastUpdatedBy'].includes(field.name),
          ),
      );
    }

    if (contactDefaultFields) {
      defaultFieldsData = getSectionsData(
        contactDefaultFields.map((field) => ({
          ...field,
          sectionTag: 'Basic Info',
          subGroup: getDefaultSubGroup(field.name),
          label:
            CONTACT_FIELDS.find(
              (defaultField) => defaultField.name === field.name,
            )?.label || field.name,
          fieldApplicableTo: field.fieldApplicableTo || 'All',
        })),
      );

      defaultFieldsData[0].subGroups = sortArrayByObjectKey(
        defaultFieldsData[0].subGroups.filter(
          (subGroup) => subGroup.subGroupName !== 'Overview',
        ),
        'subGroupName',
      );

      defaultFieldsData[0].subGroups = defaultFieldsData[0].subGroups.map(
        (subGroup) => ({
          ...subGroup,
          data: sortArrayByPriority(
            subGroup.data,
            'name',
            DEFAULT_COLUMNS[subGroup.subGroupName],
          ),
        }),
      );
    }

    const sectionsData = [...defaultFieldsData, ...customFieldsData];

    return {
      id: item?.id,
      name: item?.title || '',
      // relationships:
      //   relationships?.map((d) => ({
      //     ...d,
      //     autoAssignOptions: AUTO_ASSIGN_OPTIONS.filter(
      //       (o) => !!d[o.value],
      //     ).map((r) => r.value),
      //   })) || [],
      sectionsData: sectionsData.map((section) => ({
        id: `section-${section.sectionName}`,
        ...section,
        subGroups: section.subGroups.map((subGroup) => ({
          id: `section-${section.sectionName}-subGroup-${subGroup.subGroupName}`,
          ...subGroup,
          data: subGroup.data.map((d) => ({
            ...d,
            type: d.dataReference ? 'DataReference' : d.type,
            choices: d.dataReference ? [] : d.choices,
          })),
        })),
      })),
      updatedId: null,
      fieldsToDelete: [],
    };
  }, [relationships, contactCustomFields, contactDefaultFields]);

  const showTab = !!contactCustomFields && !!contactDefaultFields;
  const loading =
    relationshipsQuery.loading ||
    contactCustomFieldsQuery.loading ||
    contactDefaultFieldsQuery.loading;

  return (
    <>
      <Form
        initialValues={initValues}
        enableReinitialize={true}
        // validationSchema={objectBuilderValidationSchema}
        onSubmit={handleSave}>
        {({
          values,
          errors,
          dirty,
          handleSubmit,
          setValues,
          setErrors,
          validateForm,
        }) => {
          let errorMessage = '';

          switch (selectedTab) {
            case 'sectionFields': {
              errorMessage = errors.sectionsData;
              break;
            }
          }

          if (!errorMessage && doTypeMutation.error) {
            errorMessage = doTypeMutation.error.message;
          }

          return (
            <Section
              loading={loading}
              fullHeight
              headerStyle={{
                // boxShadow: 'none',
                borderBottom: '1px solid #eee',
                // backgroundColor: theme.palette.grayLight.main,
                padding: '4px 1rem',
              }}
              style={{
                margin: 0,
                boxShadow: 'none',
              }}
              bodyStyle={{
                backgroundColor: theme.palette.grayLight.main,
              }}
              HeaderLeftComp={
                <Typography
                  style={{
                    fontWeight: 'bold',
                  }}>
                  Contact Object
                </Typography>
              }
              HeaderRightComp={
                <ButtonGroup selectedButtonIndex={step} variant="text">
                  {OBJECT_BUILDER_FORM_TABS.map((tab, index) => {
                    return (
                      <DOStyled.DOItemNavBtn
                        key={tab.value}
                        onClick={handleSelectStep(index)}
                        {...(isNew &&
                          index !== step && {
                            disabled: true,
                          })}>
                        {tab.label}
                      </DOStyled.DOItemNavBtn>
                    );
                  })}
                </ButtonGroup>
              }
              FooterComp={
                <FormStyled.Footer
                  style={{
                    padding: '12px 1rem',
                    margin: 0,
                    borderTop: '1px solid #eee',
                    width: '100%',
                    backgroundColor: '#f5f6fa',
                    justifyContent: 'space-between',
                  }}>
                  <Box
                    style={{
                      width: 320,
                      marginRight: 50,
                      display: 'flex',
                    }}>
                    <DialogButton
                      disabled={!dirty}
                      variant="outlined"
                      startIcon={<RestartAltIcon />}
                      DialogProps={{
                        title: 'Are you sure?',
                        description: [
                          `This will reset ALL changes made.`,
                          'Do you want to proceed?',
                        ],
                        onActions: (handleClose) => {
                          return [
                            {
                              label: 'Reset All',
                              onClick: () => {
                                setValues(initValues);
                                setErrors({});
                                setStep(0);
                                handleClose();
                              },
                            },
                          ];
                        },
                      }}>
                      Reset All
                    </DialogButton>
                    {isBack && (
                      <Button
                        style={{
                          marginLeft: '1rem',
                        }}
                        variant="contained"
                        startIcon={<KeyboardArrowLeftIcon />}
                        onClick={handleBack}
                        size="medium">
                        {`Back - ${
                          OBJECT_BUILDER_FORM_TABS[currentIndex - 1].label
                        }`}
                      </Button>
                    )}
                  </Box>
                  {!!errorMessage && (
                    <Typography
                      color="error"
                      variant="subtitle2"
                      align="center">
                      {errorMessage}
                    </Typography>
                  )}
                  <Box
                    style={{
                      width: 370,
                      display: 'flex',
                      justifyContent: 'flex-end',
                    }}>
                    {isNext && (
                      <Button
                        disabled={isNew && !dirty}
                        variant="contained"
                        endIcon={<KeyboardArrowRightIcon />}
                        onClick={async () => {
                          if (isNew) {
                            const formErrors = await validateForm(values);

                            switch (selectedTab) {
                              case 'sectionFields': {
                                if (!formErrors.sectionsData) {
                                  handleNext();
                                }
                                break;
                              }
                            }
                          } else {
                            handleNext();
                          }
                        }}
                        size="medium">
                        {`Next - ${
                          OBJECT_BUILDER_FORM_TABS[currentIndex + 1].label
                        }`}
                      </Button>
                    )}
                    {((isNew && isLastStep) || !isNew) && (
                      <Button
                        style={{
                          marginLeft: '1rem',
                        }}
                        disabled={!dirty}
                        variant="contained"
                        startIcon={<TurnedInIcon />}
                        onClick={handleSubmit}
                        size="medium">
                        {isNew ? 'Create Object' : 'Save Changes'}
                      </Button>
                    )}
                  </Box>
                </FormStyled.Footer>
              }>
              {showTab && (
                <>
                  <Show removeHidden in={selectedTab === 'sectionFields'}>
                    <ContactSectionsAndFieldsSection />
                  </Show>
                  <Show removeHidden in={selectedTab === 'displaySettings'}>
                    <DODisplaySettings hideSectionConditions />
                  </Show>
                </>
              )}
            </Section>
          );
        }}
      </Form>
      <LoadingIndicator modal loading={doTypeMutation.saving} />
    </>
  );
};

export default ContactObjectBuilderForms;
