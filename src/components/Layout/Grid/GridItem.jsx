import React, {useEffect, useRef, forwardRef} from 'react';
import GridItemBody from './GridItemBody';
import * as Styled from './styles';

let GridItem = (
  {item, renderGridItem, autoResizeItem, onLoadDimensions, ...props},
  ref,
) => {
  const itemContainerRef = useRef(null);

  useEffect(() => {
    if (autoResizeItem && onLoadDimensions && itemContainerRef.current) {
      const dimensions = itemContainerRef.current.getBoundingClientRect();

      onLoadDimensions({id: item.id, dimensions});
    }
  }, [autoResizeItem, itemContainerRef.current]);

  return (
    <Styled.GridItem ref={ref} {...props}>
      <GridItemBody
        ref={itemContainerRef}
        item={item}
        renderGridItem={renderGridItem}
      />
    </Styled.GridItem>
  );
};

GridItem = forwardRef(GridItem);

export default GridItem;
