import React from 'react';
import {useFormikContext} from 'formik';
import Show from '@macanta/containers/Show';
import FieldForms from './FieldForms';
import Relationships from './Relationships';
import DONotes from './DONotes';
import Attachments from './Attachments';
import useDOHasConnectedUser from '@macanta/hooks/useDOHasConnectedUser';
import useOtherUserDOPermission from '@macanta/hooks/useOtherUserDOPermission';
import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import * as Styled from './styles';
import DORelationshipsForm from '@macanta/modules/DataObjectItems/DOItemForms/Relationships/DORelationshipsForm';

const DOItemForms = ({
  doItemId,
  isEdit,
  groupId,
  relationships,
  fields,
  selectedTab,
  onChange,
  loadingItem,
  data,
  ...props
}) => {
  const {values} = useFormikContext();

  const handleChange = (doFormKey) => (formData, options = {}) => {
    onChange(doFormKey, formData, options);
  };

  const {hasConnectedUser, hasConnectedRelationships} = useDOHasConnectedUser(
    data.connectedContacts,
    isEdit,
  );

  const {hasAccess: hasOtherUserDOPermissionData} = useOtherUserDOPermission({
    validate: 'data',
    metaData: {
      hasConnectedUser,
      hasConnectedRelationships,
    },
  });

  const {
    hasAccess: hasOtherUserDOPermissionRelationship,
  } = useOtherUserDOPermission({
    validate: 'relationship',
    metaData: {
      hasConnectedUser,
      hasConnectedRelationships,
    },
  });

  return (
    <Styled.Root {...props}>
      <Show removeHidden in={selectedTab === 'details'}>
        <FieldForms
          isEdit={isEdit}
          groupId={groupId}
          fields={fields}
          connectedContacts={data.connectedContacts}
          data={data.details}
          onChange={handleChange('details')}
          loading={loadingItem}
          hasConnectedUser={hasConnectedUser}
          hasOtherUserDOPermission={hasOtherUserDOPermissionData}
        />
      </Show>
      <Show removeHidden in={selectedTab === 'relationships'}>
        <NoteTaskHistoryStyled.FullHeightGrid container>
          <NoteTaskHistoryStyled.FullHeightFlexGridColumn item xs={12} md={4.3}>
            <Relationships
              isEdit={isEdit}
              selectedValueRelationships={values.relationships}
              connectedContacts={data.connectedContacts}
              relationships={relationships}
              onChange={handleChange('relationships')}
              loading={loadingItem}
              hasConnectedUser={hasConnectedUser}
              hasOtherUserDOPermission={hasOtherUserDOPermissionRelationship}
            />
          </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
          <NoteTaskHistoryStyled.FullHeightFlexGridColumn item xs={12} md={7.7}>
            <DORelationshipsForm
              data={values.directDORelationships.data}
              deletedData={values.deletedDirectDORelationships.data}
              doItemId={doItemId}
              groupId={groupId}
              onChange={handleChange('directDORelationships')}
            />
          </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
        </NoteTaskHistoryStyled.FullHeightGrid>
      </Show>
      <Show removeHidden in={selectedTab === 'additionalInfo'}>
        <NoteTaskHistoryStyled.FullHeightGrid container>
          <NoteTaskHistoryStyled.FullHeightFlexGridColumn item xs={12} md={4.3}>
            <DONotes
              selectedValueNotes={values.notes}
              data={data.notes}
              onChange={handleChange('notes')}
              loading={loadingItem}
            />
          </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
          <NoteTaskHistoryStyled.FullHeightFlexGridColumn item xs={12} md={4.3}>
            <Attachments
              selectedValueAttachments={values.attachments}
              data={data.attachments}
              doItemId={doItemId}
              onChange={handleChange('attachments')}
              loading={loadingItem}
            />
          </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
        </NoteTaskHistoryStyled.FullHeightGrid>
      </Show>
    </Styled.Root>
  );
};

export default DOItemForms;
