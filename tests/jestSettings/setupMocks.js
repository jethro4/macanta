/* eslint-disable jest/no-export */
/* eslint-disable react/jsx-filename-extension */
import {clearSessionVars} from '@macanta/graphql/cache/sessionVars';
import {clearVars} from '@macanta/graphql/cache/vars';
import React from 'react';

global.console = {
  log: jest.fn(),
  debug: console.debug,
  info: jest.fn(),
  warn: jest.fn(),
  error: jest.fn(),
};

jest.mock('react-pdf/dist/esm/entry.webpack', () => {
  const Page = () => <div>page</div>;

  return {
    pdfjs: {
      GlobalWorkerOptions: {
        workerSrc: 'abc',
      },
    },
    Outline: null,
    Page,
    Document: () => {
      return <Page />;
    },
  };
});

jest.mock('@reach/router', () => {
  const Link = ({to, children}) => <a href={to}>{children}</a>;
  const Router = ({children}) => children;

  return {
    Link,
    Router,
    navigate: jest.fn(),
    useNavigate: jest.fn(),
    useParams: jest.fn(),
    useMatch: jest.fn(),
  };
});

export const initialize = () => {
  clearVars();
  clearSessionVars();

  Object.defineProperty(window, 'localStorage', {
    value: {
      session: {
        sessionId: 'sample-session-id',
        apiKey: '1552679244144731',
      },
      userDetails: {},
    },
  });

  Object.defineProperty(window, 'sessionStorage', {
    value: {},
  });
};

test('use jsdom and set the URL in this test file', () => {
  expect(window.location.href).toBe('http://staging.localhost:8000/');
});
