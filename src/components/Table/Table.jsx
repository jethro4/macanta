import React, {useState, useImperativeHandle, forwardRef} from 'react';
import clsx from 'clsx';
import isString from 'lodash/isString';
import isArray from 'lodash/isArray';
import {makeStyles} from '@mui/material/styles';
import MUITableBody from '@mui/material/TableBody';
import Pagination from '@macanta/components/Pagination';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import {transformValue} from '@macanta/components/Forms/InputFields/helpers';
import Delay from '@macanta/components/Delay';
import useValue from '@macanta/hooks/base/useValue';
import {insertOrRemove} from '@macanta/utils/array';
import TableCellWidthProvider from './TableCellWidthProvider';
import EnhancedTableHead from './EnhancedTableHead';
import EnhancedTableBodyRow from './EnhancedTableBodyRow';
import BodySubheaderRow from './BodySubheaderRow';
import {getComparator, stableSort} from './helpers';
import * as applicationStyles from '@macanta/themes/applicationStyles';
import * as Styled from './styles';

const useStyles = makeStyles((theme) => ({
  container: {},
  containerFullHeight: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  selectedRow: {
    backgroundColor: `${theme.palette.primary.light}!important`,
    '& > td': {
      color: 'white',
    },
  },
  multiSelectedRow: {
    backgroundColor: `#eee!important`,
  },
}));

const getKeyExtractor = (item, index) => item?.id || String(index);

const isSelected = (keyValue, selectedValue) => {
  if (isArray(selectedValue)) {
    return selectedValue.includes(keyValue);
  }

  return keyValue === selectedValue;
};

let Table = (
  {
    keyExtractor = getKeyExtractor,
    columns,
    data: rows,
    disabledIds,
    selectable: selectableProp,
    isSelectable,
    highlight,
    highlightSelected,
    onSelectItem,
    selectedValue: selectedValueProp,
    hidePagination,
    page: pageProps,
    rowsPerPage: rowsPerPageProps,
    rowsPerPageOptions: rowsPerPageOptionsProps,
    onTablePageChange,
    onTableRowsPerPageChange,
    fullHeight,
    containerStyle,
    emptyContainerStyle,
    loading,
    hideEmptyMessage,
    hideHeader,
    numOfTextLines,
    disableLineClamp,
    order: orderProp,
    orderBy: orderByProp,
    onSort,
    noLimit,
    noOddRowStripes,
    sortableColumns,
    footerStyle,
    emptyMessage,
    headRowHeight = applicationStyles.DEFAULT_HEAD_ROW_HEIGHT,
    bodyRowHeight = applicationStyles.DEFAULT_BODY_ROW_HEIGHT,
    bodySubheaderRowHeight = applicationStyles.DEFAULT_BODY_SUBHEADER_ROW_HEIGHT,
    style,
    handlers,
    multiSelect,
    getIsAutoHeight,
    paginationProps,
    transformSubheader,
    renderSubheader,
    actionColumn: actionColumnProp,
    renderFrontActionButtons,
    renderActionButtons,
    stickyFrontActionButtons,
    stickyActionButtons,
    ...props
  },
  ref,
) => {
  const classes = useStyles();
  const [order, setOrder] = useState(orderProp || 'asc');
  const [orderBy, setOrderBy] = useState(orderByProp);

  const [page, setPage] = useValue(pageProps);
  const [rowsPerPage, setRowsPerPage] = useValue(rowsPerPageProps);
  const [selectedValue, setSelectedValue] = useValue(
    () => transformValue(selectedValueProp, multiSelect),
    [selectedValueProp, multiSelect],
  );

  const hasFrontActionButtons =
    !!renderFrontActionButtons && columns.length > 0;
  const hasActionButtons = !!renderActionButtons && columns.length > 0;

  const [actionColumn] = useValue(
    () =>
      Object.assign(
        {
          // frontWidth: 100,
          // backWidth: 100,
        },
        !!actionColumnProp?.width && {
          frontWidth: actionColumnProp?.width,
          backWidth: actionColumnProp?.width,
        },
        actionColumnProp,
      ),
    [actionColumnProp],
  );

  const handleRequestSort = (event, property) => {
    let sortBy = property;
    const isAsc = orderBy === sortBy && order === 'asc';
    let sortDirection = isAsc ? 'desc' : 'asc';

    if (orderBy === sortBy && order === 'desc') {
      sortBy = undefined;
      sortDirection = undefined;
    }

    setOrder(sortDirection);
    setOrderBy(sortBy);

    onSort && onSort(sortBy, sortDirection);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
    onTablePageChange && onTablePageChange(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);

    onTablePageChange && onTablePageChange(0);
    onTableRowsPerPageChange && onTableRowsPerPageChange(+event.target.value);
  };

  const handleSelectItem = (item) => () => {
    const updatedValue = !multiSelect
      ? keyExtractor(item)
      : insertOrRemove({
          arr: selectedValue,
          item: keyExtractor(item),
        });

    setSelectedValue(updatedValue);

    onSelectItem(item, updatedValue, columns);
  };

  useImperativeHandle(
    ref,
    () => ({
      resetPage: () => {
        setPage(0);
      },
    }),
    [],
  );

  const columnTransform = columns.find((col) => col.id === orderBy)?.transform;

  let finalRows = !onSort
    ? stableSort(rows, getComparator(order, orderBy, columnTransform))
    : rows;

  if (!noLimit && !onSort) {
    finalRows = finalRows.slice(
      page * rowsPerPage,
      page * rowsPerPage + rowsPerPage,
    );
  }

  finalRows = finalRows.filter(
    (item) => !disabledIds.includes(keyExtractor(item)),
  );

  const isEmpty = !loading && !hideEmptyMessage && finalRows.length === 0;

  return (
    <TableCellWidthProvider>
      <Styled.Root
        style={{
          ...style,
          ...(fullHeight && {
            height: '100%',
          }),
          ...(isEmpty && {
            minHeight: '200px',
          }),
        }}
        {...props}>
        <Styled.RootContainer>
          <Styled.TableContainer
            className={
              !fullHeight ? classes.container : classes.containerFullHeight
            }
            style={containerStyle}>
            <Styled.Table stickyHeader aria-label="sticky table">
              {!hideHeader && (
                <EnhancedTableHead
                  columns={columns}
                  order={order}
                  orderBy={orderBy}
                  onRequestSort={handleRequestSort}
                  actionColumn={actionColumn}
                  hasFrontActionColumn={hasFrontActionButtons}
                  hasActionColumn={hasActionButtons}
                  sortableColumns={sortableColumns}
                  headRowHeight={headRowHeight}
                />
              )}
              <Delay>
                <MUITableBody>
                  {finalRows.map((item, index) => {
                    const isFirstItem = index === 0;
                    const isSubheader = item.isSubheader;
                    const selected = !multiSelect
                      ? isSelected(keyExtractor(item), selectedValue)
                      : selectedValue?.includes(keyExtractor(item));
                    const isAutoHeight = getIsAutoHeight?.(item, index);
                    const selectable = !isSelectable
                      ? selectableProp
                      : isSelectable(item);

                    return isSubheader ? (
                      <BodySubheaderRow
                        key={item.title || String(index)}
                        item={item}
                        transformSubheader={transformSubheader}
                        renderSubheader={renderSubheader}
                        columnsLength={columns.length}
                        style={{
                          height:
                            bodySubheaderRowHeight - (item.nested ? 16 : 0),
                        }}
                        sx={{
                          '.MuiTableCell-root': Object.assign(
                            {
                              paddingTop: `${item.nested ? 12 : 8}px`,
                            },
                            !isFirstItem && {
                              borderTop: `${
                                item.nested ? 4 : 8
                              }px solid #f5f6fa`,
                            },
                          ),
                        }}
                      />
                    ) : (
                      <EnhancedTableBodyRow
                        key={keyExtractor(item, index)}
                        columns={columns}
                        item={item}
                        style={{
                          ...(!isAutoHeight && {
                            height: bodyRowHeight,
                          }),
                        }}
                        sx={{
                          backgroundColor:
                            !noOddRowStripes && index % 2 !== 0
                              ? 'table.bodyRow'
                              : 'common.white',
                          ...item.sx,
                          ...(isAutoHeight && {
                            '& .cellbody-container': {
                              position: 'static',
                              padding: '0.75rem 0',
                            },
                          }),
                        }}
                        {...(selectable && {
                          onClick: handleSelectItem(item),
                        })}
                        selectable={selectable}
                        show={actionColumn.show}
                        index={index}
                        highlight={highlight && !selected}
                        className={clsx(
                          selectable &&
                            highlightSelected &&
                            selected &&
                            (!multiSelect
                              ? classes.selectedRow
                              : classes.multiSelectedRow),
                        )}
                        actionColumn={actionColumn}
                        hasFrontActionButtons={hasFrontActionButtons}
                        renderFrontActionButtons={renderFrontActionButtons}
                        hasActionButtons={hasActionButtons}
                        renderActionButtons={(args) =>
                          renderActionButtons({
                            ...args,
                            multiSelect,
                            selectedValue,
                          })
                        }
                        numOfTextLines={numOfTextLines}
                        disableLineClamp={disableLineClamp}
                        handlers={handlers}
                        stickyFrontActionButtons={stickyFrontActionButtons}
                        stickyActionButtons={stickyActionButtons}
                      />
                    );
                  })}
                </MUITableBody>
              </Delay>
            </Styled.Table>
            {isEmpty && (
              <Styled.EmptyMessageContainer style={emptyContainerStyle}>
                {isString(emptyMessage) ? (
                  <Styled.EmptyMessage align="center" color="#888">
                    {emptyMessage}
                  </Styled.EmptyMessage>
                ) : (
                  emptyMessage
                )}
              </Styled.EmptyMessageContainer>
            )}
          </Styled.TableContainer>
          {loading && <LoadingIndicator fill align="top" />}
        </Styled.RootContainer>
        {!hidePagination && rows?.length > 0 && (
          <Styled.TableFooter style={footerStyle}>
            <Pagination
              table
              rowsPerPageOptions={rowsPerPageOptionsProps}
              component="div"
              count={rows.length}
              rowsPerPage={rowsPerPage}
              page={page}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
              {...paginationProps}
            />
          </Styled.TableFooter>
        )}
      </Styled.Root>
    </TableCellWidthProvider>
  );
};

Table = forwardRef(Table);

Table.defaultProps = {
  columns: [],
  data: [],
  disabledIds: [],
  highlight: true,
  highlightSelected: true,
  selectable: false,
  onSelectItem: () => {},
  page: 0,
  rowsPerPage: 25,
  rowsPerPageOptions: [25, 50, 100],
  actionColumn: {label: '', show: false},
  emptyMessage: 'No data found',
};

export default Table;
