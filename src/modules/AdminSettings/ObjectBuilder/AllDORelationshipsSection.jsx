import React from 'react';
import {useMutation} from '@apollo/client';
import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import Box from '@mui/material/Box';
import {PopoverButton, DialogButton} from '@macanta/components/Button';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import Section from '@macanta/containers/Section';
import useDORelationships from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDORelationships';
import DataTable from '@macanta/containers/DataTable';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import {
  DELETE_RELATIONSHIP,
  LIST_RELATIONSHIPS,
} from '@macanta/graphql/dataObjects';
import RelationshipOptionForm from './DORelationships/RelationshipOptionForm';

const DEFAULT_ALL_RELATIONSHIPS_COLUMNS = [
  {
    id: 'role',
    label: 'Title',
  },
  {
    id: 'description',
    label: 'Description',
    maxWidth: '100%',
  },
];

const AllDORelationshipsSection = (props) => {
  const {displayMessage} = useProgressAlert();

  const allRelationshipsQuery = useDORelationships(null, {
    fetchAll: true,
  });

  const [
    callDeleteRelationshipMutation,
    deleteRelationshipMutation,
  ] = useMutation(DELETE_RELATIONSHIP, {
    onCompleted: (data) => {
      if (data?.deleteRelationship?.id) {
        allRelationshipsQuery.client.writeQuery({
          query: LIST_RELATIONSHIPS,
          data: {
            listRelationships: allRelationships.filter(
              (r) => r.id !== data.deleteRelationship.id,
            ),
          },
        });

        displayMessage('Deleted successfully!');
      }
    },
  });

  const allRelationships = allRelationshipsQuery.data?.listRelationships;

  const handleDelete = async (id) => {
    await callDeleteRelationshipMutation({
      variables: {
        deleteRelationshipInput: {
          id,
        },
        __mutationkey: 'deleteRelationshipInput',
        removeAppInfo: true,
      },
    });
  };

  return (
    <>
      <Section
        fullHeight
        title="Relationship Options"
        style={{
          marginTop: 0,
        }}
        HeaderRightComp={
          <PopoverButton
            size="small"
            variant="contained"
            startIcon={<AddIcon />}
            PopoverProps={{
              title: 'Add Relationship',
              contentWidth: 380,
              // bodyStyle: {
              //   backgroundColor: '#f5f6fA',
              // },
            }}
            renderContent={(handleClose) => (
              <RelationshipOptionForm onClose={handleClose} />
            )}>
            Add Relationship
          </PopoverButton>
        }
        {...props}>
        <DataTable
          fullHeight
          loading={allRelationshipsQuery.loading}
          columns={DEFAULT_ALL_RELATIONSHIPS_COLUMNS}
          data={allRelationships}
          orderBy="role"
          labelKey="role"
          valueKey="id"
          actionColumn={{
            show: true,
            style: {
              minWidth: 100,
            },
          }}
          renderActionButtons={({item}) => (
            <Box
              style={{
                marginRight: -10,
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <PopoverButton
                icon
                sx={{
                  padding: '0.25rem',
                  color: 'info.main',
                }}
                // onClick={handleAdd}
                size="small"
                TooltipProps={{
                  title: 'Edit',
                }}
                PopoverProps={{
                  title: `Edit (${item.id})`,
                  contentWidth: 380,
                  anchorOrigin: {
                    vertical: 'bottom',
                    horizontal: 'center',
                  },
                  transformOrigin: {
                    vertical: -8,
                    horizontal: 'center',
                  },
                }}
                renderContent={(handleClose) => (
                  <RelationshipOptionForm item={item} onClose={handleClose} />
                )}>
                <EditIcon
                  style={{
                    fontSize: 19,
                  }}
                />
              </PopoverButton>
              <DialogButton
                icon
                size="small"
                TooltipProps={{
                  title: 'Delete',
                }}
                DialogProps={{
                  title: 'Are you sure?',
                  description: [
                    `You are about to delete "${item.role}" from Relationships.`,
                    'This cannot be undone. Do you want to proceed?',
                  ],
                  onActions: (handleClose) => {
                    return [
                      {
                        label: 'Delete',
                        onClick: async () => {
                          await handleDelete(item.id);
                          handleClose();
                        },
                        startIcon: <DeleteForeverIcon />,
                      },
                    ];
                  },
                }}>
                <DeleteForeverIcon
                  sx={{
                    color: 'grayDarker.main',
                  }}
                />
              </DialogButton>
            </Box>
          )}
        />
      </Section>
      <LoadingIndicator modal loading={deleteRelationshipMutation.loading} />
    </>
  );
};

export default AllDORelationshipsSection;
