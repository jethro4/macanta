import React from 'react';
import TurnedInIcon from '@mui/icons-material/TurnedIn';
import Box from '@mui/material/Box';
import Button from '@macanta/components/Button';
import Form from '@macanta/components/Form';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import FormField from '@macanta/containers/FormField';
import ContactSelect from '@macanta/modules/AdminSettings/UserManagement/UserForm/ContactSelect';
import useMutation from '@macanta/hooks/apollo/useMutation';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import usePermissionTemplates from '@macanta/hooks/admin/usePermissionTemplates';
import useUsers from '@macanta/hooks/admin/useUsers';
import {getFullName} from '@macanta/selectors/contact.selector';
import {
  LIST_USERS,
  CREATE_USER,
  UPDATE_USER,
  USER_ATTRIBUTES,
} from '@macanta/graphql/users';
import {
  writeItemInListQuery,
  writeItemInListFragment,
} from '@macanta/graphql/helpers';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';

const getOptionLabel = (item) => getFullName(item);

const CONTACT_LEVELS = ['User', 'Admin'];

const UserForm = ({
  item,
  // onCreate
  onClose,
}) => {
  const isEdit = !!item.id;

  const {displayMessage} = useProgressAlert();

  const permissionTemplatesQuery = usePermissionTemplates(item.templateId, {
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });
  const usersQuery = useUsers({
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const permissionTemplates = permissionTemplatesQuery.data;
  const selectedTemplate = permissionTemplatesQuery.selectedTemplate;

  const [callCreateUser, createUserMutation] = useMutation(CREATE_USER, {
    onCompleted: ({result}) => {
      displayMessage('Saved successfully!');

      // onCreate();
      writeItemInListQuery({
        graphqlTag: LIST_USERS,
        typeName: 'UserItems',
        item: result,
        existingItems: usersQuery.data,
        position: 'last',
      });

      onClose();
    },
  });

  const [callUpdateUser, updateUserMutation] = useMutation(UPDATE_USER, {
    update(cache, {data: {updateUser}}) {
      writeItemInListFragment({
        item: updateUser,
        typeName: 'User',
        fragment: USER_ATTRIBUTES,
        listKey: 'listUsers',
      });
    },
    onCompleted: () => {
      displayMessage('Saved successfully!');

      onClose();
    },
  });

  const handleSave = (values) => {
    if (!isEdit) {
      callCreateUser({
        variables: {
          contactId: values.contact.id,
          email: values.contact.email,
          firstName: values.contact.firstName,
          lastName: values.contact.lastName,
          templateId: values.templateId,
          contactLevel: values.contactLevel,
        },
      });
    } else {
      callUpdateUser({
        variables: {
          id: values.id,
          templateId: values.templateId,
          contactLevel: values.contactLevel,
        },
      });
    }
  };

  return (
    <>
      <Form
        initialValues={{
          id: item.id,
          templateId: selectedTemplate?.id,
          contactLevel: item.contactLevel,
        }}
        enableReinitialize={true}
        onSubmit={handleSave}>
        {({values, handleChange, handleSubmit, setFieldValue, dirty}) => {
          return (
            <>
              <Box
                style={{
                  padding: '1rem',
                }}>
                {!isEdit && (
                  <ContactSelect
                    sx={{
                      marginBottom: '1.5rem',
                    }}
                    onChange={(val, contact) => {
                      setFieldValue('contact', contact);
                    }}
                    labelPosition="normal"
                    label="Contact"
                    fullWidth
                    variant="outlined"
                    size="small"
                    getOptionLabel={getOptionLabel}
                    filteredContacts={usersQuery.data?.map((d) => d.id)}
                  />
                )}

                <FormField
                  type="Select"
                  labelPosition="normal"
                  value={values.contactLevel}
                  options={CONTACT_LEVELS}
                  onChange={handleChange('contactLevel')}
                  label="User Level"
                  fullWidth
                  variant="outlined"
                  size="small"
                />

                <FormField
                  type="Select"
                  labelPosition="normal"
                  value={values.templateId}
                  options={permissionTemplates}
                  labelKey="name"
                  valueKey="id"
                  onChange={handleChange('templateId')}
                  label="Permission Template"
                  fullWidth
                  variant="outlined"
                  size="small"
                />
              </Box>
              <Box
                style={{
                  display: 'flex',
                  justifyContent: 'flex-end',
                  padding: '12px 1rem',
                  borderTop: '1px solid #eee',
                }}>
                <Button
                  disabled={!dirty}
                  variant="contained"
                  startIcon={<TurnedInIcon />}
                  onClick={handleSubmit}
                  size="medium">
                  Save
                </Button>
              </Box>
            </>
          );
        }}
      </Form>
      <LoadingIndicator
        modal
        loading={createUserMutation.loading || updateUserMutation.loading}
      />
    </>
  );
};

UserForm.defaultProps = {
  item: {
    contactLevel: 'User',
    templateId: '',
  },
};

export default UserForm;
