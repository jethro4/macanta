import {gql} from '@apollo/client';

export default gql`
  query getSessionStatus(
    $appName: String!
    $apiKey: String!
    $sessionName: String!
  ) {
    getSessionStatus(
      appName: $appName
      apiKey: $apiKey
      sessionName: $sessionName
    ) {
      expired
    }
  }
`;
