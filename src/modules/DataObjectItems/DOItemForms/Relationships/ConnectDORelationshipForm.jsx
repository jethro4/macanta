import React, {useState} from 'react';
import AddIcon from '@mui/icons-material/Add';
import Checkbox from '@mui/material/Checkbox';
import DataObjectsSearchTable from '@macanta/modules/UniversalSearch/DataObjectsSearchTable';
import Button from '@macanta/components/Button';

const ConnectDORelationshipForm = ({onConnect, ...props}) => {
  const [selectedItems, setSelectedItems] = useState([]);

  const handleSelectItem = (item, selectedValue, columns) => {
    setSelectedItems(
      selectedValue.map(
        (id) =>
          selectedItems.find(
            (selectedItem) => selectedItem.doItemId === id,
          ) || {
            doItemId: id,
            groupId: props.groupId,
            data: columns.map((column) => ({
              fieldName: column.id,
              value: item[column.id],
            })),
          },
      ),
    );
  };

  const handleConnect = () => {
    onConnect(selectedItems);
  };

  return (
    <DataObjectsSearchTable
      data-testid="ConnectDORelationshipForm"
      onSelectItem={handleSelectItem}
      hideAdd
      hideContacts
      selectable
      multiSelect
      actionColumn={{
        label: 'Connect?',
        show: true,
        style: {
          minWidth: 80,
          maxWidth: 80,
        },
      }}
      renderActionButtons={({item, selectedValue}) => (
        <Checkbox color="primary" checked={selectedValue.includes(item.id)} />
      )}
      paginationProps={{
        RightComp: (
          <Button
            disabled={!selectedItems.length}
            variant="contained"
            startIcon={<AddIcon />}
            onClick={handleConnect}
            size="medium">
            Connect ({selectedItems.length})
          </Button>
        ),
      }}
      {...props}
    />
  );
};

export default ConnectDORelationshipForm;
