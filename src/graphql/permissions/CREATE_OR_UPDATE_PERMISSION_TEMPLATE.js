import {gql} from '@apollo/client';
import {PERMISSIONS_FRAGMENT} from '@macanta/graphql/permissions/fragments';

export default gql`
  mutation createOrUpdatePermissionTemplate(
    $input: CreateOrUpdatePermissionTemplateInput!
  ) {
    createOrUpdatePermissionTemplate(input: $input) {
      id
      name
      description
      status
      permission {
        ...permissionsFragment
      }
    }
  }
  ${PERMISSIONS_FRAGMENT}
`;
