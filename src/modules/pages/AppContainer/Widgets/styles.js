import {experimentalStyled as styled} from '@mui/material/styles';

import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardActionsComp from '@mui/material/CardActions';
import Typography from '@mui/material/Typography';
import {OverflowTip} from '@macanta/components/Tooltip';
import Button, {IconButton} from '@macanta/components/Button';
import TextField from '@macanta/components/Forms';
import DataTable from '@macanta/containers/DataTable';
import * as AttachmentStyled from '@macanta/modules/DataObjectItems/DOItemForms/Attachments/styles';
import * as NoteTaskFormsStyled from '@macanta/modules/NoteTaskForms/styles';

export const ReactGridItemContainer = styled(Box)`
  cursor: grab;

  &.react-draggable-dragging,
  &.react-draggable-dragging .MuiButtonBase-root {
    cursor: grabbing;
  }
`;

export const Root = styled(Card)`
  display: flex;
  flex-direction: column;
  height: 100%;

  position: relative;

  & .card-actions {
    display: none;
  }

  &:hover {
    & .card-actions {
      display: flex;
    }
  }
`;

export const Header = styled(Box)`
  width: 100%;
  padding: 12px 0 ${({theme}) => theme.spacing(1)};
  display: flex;
  align-items: center;
  border-bottom: 2px solid ${({theme}) => theme.palette.primary.main};
`;

export const Title = styled(OverflowTip)`
  font-size: 1.05rem;
`;

export const ActionArea = styled(AttachmentStyled.CardActionArea)`
  flex: 1;
  display: flex;
  flex-direction: column;
  padding: 0 ${({theme}) => theme.spacing(2)} 52px;
`;

export const ActionIconButton = styled(IconButton)`
  border-radius: 0;

  &:hover {
    background-color: rgba(0, 0, 0, 0.8);
  }
`;

export const CardActions = styled(CardActionsComp)`
  border-radius: 4px;
  padding: 0;
  position: absolute;
  background-color: rgba(0, 0, 0, 0.5);
  top: 1rem;
  right: 1rem;
  overflow: hidden;
`;

export const Content = styled(CardContent)`
  flex: 1;
  padding: 0;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Quantity = styled(Typography)`
  font-size: 4.3rem;
  font-weight: 300;
`;

export const Footer = styled(Box)`
  display: flex;
  justify-content: center;
  width: 100%;
  position: absolute;
  bottom: 10px;

  & > *:not(:last-child) {
    border-right: 1px solid #dcdde1;
  }
`;

export const FooterButtonContainer = styled(Box)`
  flex: 1;
`;

export const FooterButton = styled(Button)`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const FooterText = styled(Typography)`
  font-size: 1.6rem;
  color: ${({theme}) => `${theme.palette.text.blue}`};
`;

export const HeaderRightContainer = styled(Box)`
  display: flex;
  align-items: center;
`;

export const DateRangeField = styled(TextField)`
  width: 200px;
`;

export const InlineTextField = styled(TextField)`
  background-color: white;
  & input {
    font-size: 14px;
    height: 20px;
  }
`;

export const InlineEditingContainer = styled(Box)`
  & .inline-edit {
    opacity: 0;
  }
  &:hover .inline-edit {
    opacity: 1;
  }
`;

export const MultiSelectChipInput = styled(NoteTaskFormsStyled.TextField)`
  .MuiInputBase-input {
    height: auto !important;
  }
`;

export const WidgetsDataTable = styled(DataTable)`
  & .MuiTextField-root {
    min-width: 150px;
  }
`;

export const ContactActionButtonsContainer = styled(Box)`
  display: flex;
  align-items: center;
  margin-left: 1rem;
`;
