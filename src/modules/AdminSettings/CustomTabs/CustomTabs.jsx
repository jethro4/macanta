import React from 'react';
import {navigate} from 'gatsby';
import Breadcrumbs from '@mui/material/Breadcrumbs';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import Typography from '@mui/material/Typography';
import Section from '@macanta/containers/Section';
import CustomTabsTable from '@macanta/modules/CustomTab/CustomTabsTable';
import useCustomTabs from '@macanta/hooks/admin/useCustomTabs';
import AddCustomTabBtn from '@macanta/modules/CustomTab/AddCustomTabBtn';
import {ROOT_PATH_ADMIN} from '@macanta/modules/AdminSettings/AdminSettings';
import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import * as AdminSettingsStyled from '@macanta/modules/AdminSettings/styles';

const CustomTabs = () => {
  /* const navigate = useNavigate();*/

  const {data, loading, refetch} = useCustomTabs();

  const handleBack = () => {
    navigate(ROOT_PATH_ADMIN);
  };

  return (
    <NoteTaskHistoryStyled.FullHeightGrid container>
      <NoteTaskHistoryStyled.FullHeightFlexGridColumn item xs={12} lg={6}>
        <Section
          fullHeight
          bodyStyle={{
            backgroundColor: 'white',
            padding: 0,
          }}
          HeaderLeftComp={
            <Breadcrumbs aria-label="breadcrumb">
              <AdminSettingsStyled.TitleCrumbBtn
                startIcon={
                  <ArrowBackIcon
                    style={{
                      color: '#333',
                    }}
                  />
                }
                onClick={handleBack}
                disabled={false}>
                <Typography color="textPrimary">Admin Settings</Typography>
              </AdminSettingsStyled.TitleCrumbBtn>
              <Typography>Custom Tabs</Typography>
            </Breadcrumbs>
          }
          HeaderRightComp={<AddCustomTabBtn onSave={refetch} />}>
          <CustomTabsTable
            data={data}
            loading={loading}
            hideEmptyMessage={loading}
          />
        </Section>
      </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
    </NoteTaskHistoryStyled.FullHeightGrid>
  );
};

export default CustomTabs;
