import useQuery from '@macanta/hooks/apollo/useQuery';
import {GET_EMAIL_ACTION_TEMPLATE_GROUPS} from '@macanta/graphql/automation';

const useEmailActionTemplateGroups = (options) => {
  const query = useQuery(GET_EMAIL_ACTION_TEMPLATE_GROUPS, options);

  return query;
};

export default useEmailActionTemplateGroups;
