import React from 'react';
import * as Styled from './styles';

const getKeyExtractor = (item, index) => item?.id || String(index);

const List = ({
  loading,
  data,
  renderRow,
  keyExtractor = getKeyExtractor,
  hideEmptyMessage,
  ...props
}) => {
  const isEmpty = !loading && !hideEmptyMessage && data.length === 0;

  return (
    <Styled.Root disablePadding={true} {...props}>
      {data.map((item, index) => {
        return (
          <Styled.ListItem key={keyExtractor(item, index)}>
            {renderRow(item, index)}
          </Styled.ListItem>
        );
      })}
      {isEmpty && (
        <Styled.EmptyMessage align="center" color="#888">
          No items found
        </Styled.EmptyMessage>
      )}
    </Styled.Root>
  );
};

List.defaultProps = {
  data: [],
  renderRow: () => null,
  keyExtractor: (item, index) => item?.id || index,
};

export default List;
