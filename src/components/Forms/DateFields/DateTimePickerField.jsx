import React, {useState} from 'react';
import clsx from 'clsx';
import {useTheme, makeStyles} from '@mui/material/styles';
import AdapterDateFns from '@mui/x-date-pickers/AdapterDateFns';
import LocalizationProvider from '@mui/x-date-pickers/LocalizationProvider';
import DateTimePicker from '@mui/x-date-pickers/DateTimePicker';
import TextField from '@macanta/components/Forms/InputFields/TextField';
import {
  convertToDate,
  formatDateToLocaleString,
} from '@macanta/components/Forms/InputFields/helpers';
import useNextEffect from '@macanta/hooks/base/useNextEffect';

const DATE_INPUT_FORMAT = 'yyyy-MM-dd hh:mma';
const DATE_INPUT_MASK = '____-__-__ __:___M';
const DATE_OUTPUT_FORMAT = 'YYYY-MM-DD hh:mmA';

const useStyles = makeStyles((theme) => ({
  dateTimePicker: {
    '& .MuiTab-root': {
      backgroundColor: 'white',
      borderTop: `1px solid ${theme.palette.secondary.main}`,

      '& .MuiTab-wrapper': {
        color: theme.palette.secondary.main,
      },

      '&.Mui-selected': {
        backgroundColor: theme.palette.primary.main,
        borderTop: 'none',

        '& .MuiTab-wrapper': {
          color: theme.palette.common.white,
        },
      },
    },
    '& .MuiIconButton-root[class*="MuiClock-"]': {
      padding: 6,
    },
    '& .MuiIconButton-root[class*="MuiClock-am"]': {
      marginLeft: 4,
      marginRight: 16,
    },
    '& .MuiIconButton-root[class*="MuiClock-meridiemButtonSelected"]': {
      backgroundColor: theme.palette.primary.main,
      color: theme.palette.common.white,
    },
  },
}));

const DateTimePickerField = ({
  DateTimePickerProps,
  value,
  onChange,
  ...props
}) => {
  const theme = useTheme();
  const classes = useStyles();

  const [selectedDate, setSelectedDate] = useState(() =>
    convertToDate(value, DATE_OUTPUT_FORMAT),
  );

  const handleDateTimeChange = (newDate) => {
    setSelectedDate(newDate);
    onChange(formatDateToLocaleString(newDate, DATE_OUTPUT_FORMAT));
  };

  useNextEffect(() => {
    setSelectedDate(convertToDate(value, DATE_OUTPUT_FORMAT));
  }, [value]);

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <DateTimePicker
        className={clsx({
          [classes.dateTimePicker]: true,
        })}
        value={selectedDate}
        onChange={handleDateTimeChange}
        renderInput={(params) => (
          <TextField
            {...params}
            {...props}
            helperText=""
            value={selectedDate}
          />
        )}
        OpenPickerButtonProps={{
          style: {
            color: theme.palette.secondary.main,
            padding: 6,
            marginRight: -6,
          },
        }}
        {...DateTimePickerProps}
        inputFormat={DATE_INPUT_FORMAT}
        mask={DATE_INPUT_MASK}
      />
    </LocalizationProvider>
  );
};

export default DateTimePickerField;
