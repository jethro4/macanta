import React, {useEffect} from 'react';
import {ModalButton} from '@macanta/components/Button';
import WidgetCard from './WidgetCard';
import WidgetData from './WidgetData';
import useRetryQuery from '@macanta/hooks/apollo/useRetryQuery';
import useLazyQuery from '@macanta/hooks/apollo/useLazyQuery';
import {GET_WIDGET_COUNTS, WIDGET_COUNT_FRAGMENT} from '@macanta/graphql/admin';
import * as Storage from '@macanta/utils/storage';
import {isNumeric} from '@macanta/utils/numeric';

const WidgetItem = ({
  selectedOptions,
  layout,
  widget,
  onOpen,
  onClose,
  onEdit,
  onDelete,
  onTaskComplete,
  onClick,
  pending,
  tasksCountLoading,
}) => {
  const isOpen = selectedOptions?.id === widget.id;

  const {userId: loggedInUserId} = Storage.getItem('userDetails');

  const widgetCountsQuery = useRetryQuery(GET_WIDGET_COUNTS, {
    skip: pending || widget.type === 'MacantaTaskSearch',
    variables: {
      userId: widget.userId,
      queryId: widget.queryId,
    },
    onCompleted: ({result}) => {
      if (result) {
        const {
          currentCount,
          last7DayCount,
          last30DayCount,
          last90DayCount,
          allCount,
        } = result;

        widgetCountsQuery.client.writeFragment({
          id: `Widget:${widget.id}`,
          fragment: WIDGET_COUNT_FRAGMENT,
          data: {
            currentCount,
            last7DayCount,
            last30DayCount,
            last90DayCount,
            allCount,
          },
        });
      }
    },
  });
  const [callForceWidgetCountsQuery, forceWidgetCountsQuery] = useLazyQuery(
    GET_WIDGET_COUNTS,
  );

  const forceUpdateWidgetCount = (updatedWidget) => {
    if (updatedWidget.type !== 'MacantaTaskSearch') {
      callForceWidgetCountsQuery({
        variables: {
          userId: loggedInUserId,
          queryId: updatedWidget.queryId,
        },
        context: {
          headers: {
            force: true,
          },
        },
      });
    }
  };

  const getRangeDescription = (range) => {
    let description = '';

    if (isNumeric(range)) {
      description = `${range} days`;
    } else if (range === 'main') {
      description = 'Current';
    } else if (range === 'all') {
      description = 'History';
    }

    return description;
  };

  useEffect(() => {
    if (isOpen) {
      forceWidgetCountsQuery.abort();
    }
  }, [isOpen]);

  const widgetCounts = widgetCountsQuery.data?.getWidgetCounts;
  const loading =
    tasksCountLoading ||
    widgetCountsQuery.loading ||
    forceWidgetCountsQuery.loading;

  return (
    <ModalButton
      ModalProps={{
        headerTitle: !widget
          ? ''
          : `${widget?.title} - ${getRangeDescription(
              selectedOptions?.range,
            )} (${widget?.queryId})`,
        contentWidth: '100%',
        contentHeight: '96%',
        onOpen: (range) => {
          onOpen(widget, range);
        },
        onClose: () => {
          forceUpdateWidgetCount(widget);

          onClose();
        },
      }}
      renderButton={(handleOpen) => {
        const handleClick = (...args) => {
          onClick(handleOpen, ...args);
        };

        return (
          <WidgetCard
            loading={pending || loading}
            layout={layout}
            item={widget}
            widgetCounts={widgetCounts}
            onClick={handleClick}
            onEdit={onEdit}
            onDelete={onDelete}
            onForceReload={forceUpdateWidgetCount.bind(null, widget)}
          />
        );
      }}
      renderContent={() =>
        isOpen && (
          <WidgetData
            widget={widget}
            range={selectedOptions.range}
            onTaskComplete={onTaskComplete}
          />
        )
      }
    />
  );
};

WidgetItem.defaultProps = {
  pending: false,
};

export default WidgetItem;
