import {useState} from 'react';
import {getAppInfo} from '@macanta/utils/app';
import {useMutation} from '@apollo/client';
import {UPLOAD_DO_ATTACHMENT_URL} from '@macanta/graphql/dataObjects';
import envConfig from '@macanta/config/envConfig';

const useUpload = () => {
  const [loading, setLoading] = useState();
  const [error, setError] = useState();
  const [callUploadDOAttachmentUrl] = useMutation(UPLOAD_DO_ATTACHMENT_URL, {
    onError() {},
  });

  const callUpload = (file, doItemId) => {
    setError();
    setLoading(true);

    return uploadFile(file, doItemId)
      .then((updatedFile) => {
        return updatedFile;
      })
      .catch((err) => {
        setError(err);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const callUploadURL = async (data, doItemId) => {
    setError();
    setLoading(true);

    try {
      const response = await callUploadDOAttachmentUrl({
        variables: {
          uploadDOAttachmentUrlInput: {
            itemId: doItemId,
            fileName: data.fileName,
            url: data.thumbnail,
          },
          __mutationkey: 'uploadDOAttachmentUrlInput',
        },
      });

      const newFile = response.data?.uploadDOAttachmentUrl;

      return [
        {
          id: newFile.fileId,
          fileName: newFile.fileName,
          thumbnail: newFile.thumbnail,
          downloadUrl: newFile.downloadUrl,
        },
      ];
    } catch (err) {
      setError(err);
    } finally {
      setLoading(false);
    }
  };

  return {callUpload, callUploadURL, loading, error};
};

const uploadFile = async (file, doItemId) => {
  const [appName, apiKey] = getAppInfo();

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/data_object/attache/file/item/${doItemId}`;
  const formData = new FormData();

  formData.append('access_key', apiKey);
  formData.append('CDFileAttachments', file);

  const response = await fetch(url, {
    method: 'POST',
    body: formData,
  });

  const {data} = await response.json();
  const dataFiles = data[doItemId];

  const fileWithThumbnail =
    dataFiles?.find((f) => f.filename === file.fileName) || {};

  return transformUploadedFile(fileWithThumbnail);
};

const transformUploadedFile = (fileWithThumbnail) => {
  const newFile = {
    id: fileWithThumbnail.id,
    thumbnail: fileWithThumbnail.thumbnail,
    fileName: fileWithThumbnail.filename,
    downloadUrl: fileWithThumbnail.download_url,
  };

  return newFile;
};

export default useUpload;
