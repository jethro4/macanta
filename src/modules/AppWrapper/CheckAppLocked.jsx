import {useLayoutEffect} from 'react';
import useIsAppLocked from '@macanta/hooks/permission/useIsAppLocked';
import {loginDisabledMessageVar} from '@macanta/graphql/cache/vars';

const CheckAppLocked = () => {
  const {isAppLocked, loginDisabledMessage} = useIsAppLocked();

  useLayoutEffect(() => {
    if (isAppLocked) {
      loginDisabledMessageVar(loginDisabledMessage);
    } else {
      loginDisabledMessageVar('');
    }
  }, [isAppLocked]);

  return null;
};

export default CheckAppLocked;
