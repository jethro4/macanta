import React from 'react';
import isArray from 'lodash/isArray';
import FormField from '@macanta/containers/FormField';
import {
  DERIVATION_FIELDS_MAPPING,
  DERIVATION_FIELD_KEY_TYPES_MAPPING,
} from '@macanta/constants/automation';
import useAllFields from '@macanta/hooks/dataObject/useAllFields';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import useValue from '@macanta/hooks/base/useValue';
import {formatDate, getNow} from '@macanta/utils/time';
import {DAYS, MONTHS} from '@macanta/constants/time';
import {filterItems} from '@macanta/utils/array';
import {DATE_FIELD_TYPES} from '@macanta/constants/form';

const FieldValueField = ({fieldKey, groupId, actionType, ...props}) => {
  const name = fieldKey;
  const label = DERIVATION_FIELDS_MAPPING[actionType][fieldKey];
  const derivedFieldType = DERIVATION_FIELD_KEY_TYPES_MAPPING[fieldKey];

  // const [data, setData] = useState({});

  // const handleChange = (val) => {};

  // const handleSave = () => {
  //   onSave();
  // };

  const allFieldsQuery = useAllFields({
    groupId,
    isContactObjectType: groupId === 'co_customfields',
    includeAllTypes: true,
    options: {
      fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
    },
  });

  const [fieldType] = useValue(() => {
    let type;

    if (derivedFieldType === 'text') {
      type = 'Text';
    } else if (['number', 'dateNumbers'].includes(derivedFieldType)) {
      type = 'Number';
    } else {
      type = 'Select';
    }

    return type;
  }, [derivedFieldType, allFieldsQuery.data]);

  const [fieldOptions] = useValue(() => {
    let options = [];

    if (isArray(derivedFieldType)) {
      options = derivedFieldType;
    } else if (derivedFieldType === 'dateFormats') {
      options = ['MMMM DD YYYY', 'dddd DD MMMM YYYY', 'MM-DD-YYYY'];
    } else if (derivedFieldType === 'dateMonths') {
      options = MONTHS;
    } else if (derivedFieldType === 'dateDays') {
      options = DAYS;
    } else if (
      derivedFieldType === 'dateFields' ||
      (['NextBirthday', 'TimeDifferenceFields', 'HumaniseDates'].includes(
        actionType,
      ) &&
        fieldKey === 'variable1')
    ) {
      options = filterItems(
        allFieldsQuery.data,
        (item) => item.isSubheader || DATE_FIELD_TYPES.includes(item.type),
      );
    } else if (derivedFieldType === 'fields') {
      options = allFieldsQuery.data;
    }

    return options;
  }, [derivedFieldType, actionType, allFieldsQuery.data]);

  return (
    <FormField
      name={name}
      loading={allFieldsQuery.initLoading}
      labelPosition="normal"
      type={fieldType}
      options={fieldOptions}
      valueKey="name"
      label={label}
      size="small"
      variant="outlined"
      {...(derivedFieldType === 'dateFormats' && {
        valueKey: undefined,
        getOptionLabel: (format) => formatDate(getNow(), format),
      })}
      {...props}
    />
  );
};

export default FieldValueField;
