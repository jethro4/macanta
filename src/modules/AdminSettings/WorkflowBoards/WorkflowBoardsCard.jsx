import React from 'react';
import GroupIcon from '@mui/icons-material/Group';
import SettingCard from '@macanta/modules/AdminSettings/SettingCard';
import {navigate} from 'gatsby';

const WorkflowBoardsCard = () => {
  /* const navigate = useNavigate();*/

  const handleCustomTabs = () => {
    navigate('/app/admin-settings/workflow-boards');
  };

  return (
    <SettingCard
      IconComp={GroupIcon}
      title="Workflow Boards"
      description="Manage Workflow Boards for better overview of state between data objects"
      onClick={handleCustomTabs}
    />
  );
};

export default WorkflowBoardsCard;
