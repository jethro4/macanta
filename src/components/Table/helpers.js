import isArray from 'lodash/isArray';
import isObject from 'lodash/isObject';
import get from 'lodash/get';
import {arrayConcatOrUpdateByKey} from '@macanta/utils/array';

export const descendingComparator = (a, b, orderBy, transform) => {
  const aOrderBy = get(a, orderBy);
  let aValue = isArray(aOrderBy)
    ? aOrderBy[0]?.value
    : isObject(aOrderBy)
    ? aOrderBy.value
    : aOrderBy;

  aValue =
    !aValue && transform ? transform({value: aValue, item: aOrderBy}) : aValue;
  aValue = aValue ? String(aValue).toLowerCase() : '';

  const bOrderBy = get(b, orderBy);
  let bValue = isArray(bOrderBy)
    ? bOrderBy[0]?.value
    : isObject(bOrderBy)
    ? bOrderBy.value
    : bOrderBy;

  bValue =
    !bValue && transform ? transform({value: bValue, item: bOrderBy}) : bValue;
  bValue = bValue ? String(bValue).toLowerCase() : '';

  if (bValue < aValue) {
    return -1;
  }
  if (bValue > aValue) {
    return 1;
  }
  return 0;
};

export const getComparator = (order, orderBy, transform) => {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy, transform)
    : (a, b) => -descendingComparator(a, b, orderBy, transform);
};

export const stableSort = (array, comparator) => {
  const stabilizedThis = array?.map((el, index) => [el, index]) || [];
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
};

export const getColumnSpanOptions = ({colSpans, columns, hasActionButtons}) => {
  let columnsArr = columns;
  let hasActionButtonsBool = hasActionButtons;

  if (colSpans?.length) {
    colSpans.forEach(([colId, colSpan]) => {
      const columnLength = columnsArr.length;
      const colIndex = columnsArr.findIndex((col) => col.id === colId);

      if (hasActionButtonsBool && colIndex + colSpan - 1 > columnLength - 1) {
        hasActionButtonsBool = false;
      }

      columnsArr = arrayConcatOrUpdateByKey({
        arr: columnsArr,
        item: {
          ...columnsArr[colIndex],
          colSpan,
        },
      });
      columnsArr.splice(colIndex + 1, colSpan - 1);
    });
  }

  return {
    columns: columnsArr,
    hasActionButtons: hasActionButtonsBool,
  };
};
