import useQuery from '@macanta/hooks/apollo/useQuery';
import useValue from '@macanta/hooks/base/useValue';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import {LIST_WORKFLOW_BOARDS} from '@macanta/graphql/admin';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {sortArrayByObjectKey} from '@macanta/utils/array';

const useWorkflowBoards = (optionsProp = {}) => {
  const {id, ...options} = optionsProp;

  const doTypesQuery = useDOTypes({
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });
  const workflowBoardsQuery = useQuery(LIST_WORKFLOW_BOARDS, options);

  const doTypes = doTypesQuery?.data?.listDataObjectTypes;
  const workflowBoards = workflowBoardsQuery.data?.listWorkflowBoards;
  const selectedBoard = workflowBoards?.find((board) => board.id === id);

  const [sortedBoards] = useValue(() => {
    if (workflowBoards?.length && doTypes?.length) {
      const filteredBoards = workflowBoards.filter((board) =>
        doTypes.some((type) => type.title === board.type),
      );

      return sortArrayByObjectKey(filteredBoards, 'title');
    }

    return [];
  }, [workflowBoards, doTypes]);

  return {
    ...workflowBoardsQuery,
    data: sortedBoards,
    selectedBoard,
    loading: doTypesQuery.loading || workflowBoardsQuery.loading,
    initLoading: doTypesQuery.initLoading || workflowBoardsQuery.initLoading,
  };
};

export default useWorkflowBoards;
