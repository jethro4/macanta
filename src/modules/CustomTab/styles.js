import {experimentalStyled as styled} from '@mui/material/styles';
import FormField from '@macanta/containers/FormField';

export const TextField = styled(FormField)`
  background-color: #f5f6fa;

  .MuiInputBase-root {
    background-color: ${({theme}) => theme.palette.common.white};
  }

  .MuiFormHelperText-root {
    margin-left: 0;
  }
`;
