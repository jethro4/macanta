import React, {useState, useLayoutEffect} from 'react';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import Pagination from '@macanta/components/Pagination';
import {SubSection} from '@macanta/containers/Section';
import DOSearch from '@macanta/modules/DataObjectItems/DOSearch';
import {filterDOItems} from '@macanta/selectors/dataObject.selector';
import useDOItems, {
  DEFAULT_PAGE_LIMIT,
} from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOItems';
import ContactQuery from '@macanta/modules/hoc/ContactQuery';
import useDOItemMutation from '@macanta/modules/DataObjectItems/DOItemForms/useDOItemMutation';
import useWorkflowBoardPermission from '@macanta/hooks/admin/useWorkflowBoardPermission';
import usePrevious from '@macanta/hooks/usePrevious';
import {
  mergeArraysBySameKeys,
  insert,
  update,
  remove,
} from '@macanta/utils/array';
import {uncapitalizeFirstLetter} from '@macanta/utils/string';
import {CONTACT_DEFAULT_FIELDS} from '@macanta/constants/contactFields';
// import {LIST_DATA_OBJECTS} from '@macanta/graphql/dataObjects';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {Responsive, WidthProvider} from 'react-grid-layout';
import BoardCard from './BoardCard';
import * as WidgetStyled from '@macanta/modules/pages/AppContainer/Widgets/styles';
import * as Styled from './styles';
import * as Storage from '@macanta/utils/storage';
import useDeepCompareEffect from '@macanta/hooks/base/useDeepCompareEffect';

const ResponsiveReactGridLayout = WidthProvider(Responsive);

const WORKFLOW_HEADER_COLORS = [
  '#663399',
  '#B53471',
  '#EE5A24',
  '#F79F1F',
  '#20BF6B',
  '#2D98DA',
  '#E84393',
  '#B33939',
  '#009432',
  '#006266',
  '#005F9C',
  '#833471',
  '#00B894',
  '#9980FA',
  '#E17055',
];

const getContactFieldValue = (contact, fieldName) => {
  if (fieldName === 'Phone1' && contact.phoneNumbers?.[0]) {
    return contact.phoneNumbers[0];
  }

  return (
    contact[uncapitalizeFirstLetter(fieldName)] ||
    contact.customFields?.find((cf) => cf.name === fieldName)?.value
  );
};

const BoardColumn = ({
  choice,
  index: indexProp,
  groupId,
  boardId,
  type,
  relationship,
  activeColumnsColor,
  doFields,
  relationships,
  keyField,
  displayFields,
  latestDroppedChoice,
  setLatestDroppedChoice,
  ...props
}) => {
  const {userId: loggedInUserId} = Storage.getItem('userDetails');

  const [layouts, setLayouts] = useState({});
  const [searchFilter, setSearchFilter] = useState('');
  const [filters, setFilters] = useState([
    {
      logic: 'AND',
      name: keyField,
      operator: 'contains',
      value: choice,
      values: null,
      default: true,
    },
  ]);
  const [filteredItems, setFilteredItems] = useState(null);
  const [page, setPage] = useState(0);
  const [isDragging, setIsDragging] = useState(false);

  const {isCurrentUserOnly} = useWorkflowBoardPermission({
    boardId,
  });

  //TODO: Will remove "qFilter" logic once "contactId" and "q" params filters correctly on backend. Should return to "searchFilter" variable
  const qFilter = '';
  //TODO: Will remove "clientFilterString" client side logic once "contactId" and "q" params filters correctly on backend
  const clientFilterString = searchFilter;
  const contactIdFilter = isCurrentUserOnly ? loggedInUserId : '';
  const relationshipFilter = isCurrentUserOnly ? '' : relationship;

  const prevClientFilterString = usePrevious(clientFilterString);
  const clientFilterStringChanged =
    prevClientFilterString !== clientFilterString;

  const variables = {
    q: qFilter,
    filters,
    groupId,
    contactId: contactIdFilter,
    relationship: relationshipFilter,
    page,
    limit: DEFAULT_PAGE_LIMIT,
    order: keyField,
    orderBy: 'desc',
    contactField: true,
  };

  const doItemsQuery = useDOItems(variables);

  const {save} = useDOItemMutation(null, groupId, null);

  const total = doItemsQuery.data?.total;
  const doItems = doItemsQuery.selectedDO?.items;

  const generateLayouts = (items) => {
    return ['lg', 'md', 'sm', 'xs', 'xxs'].reduce((acc, col) => {
      if (items) {
        acc[col] = items.map((item, index) => {
          return {
            x: 0,
            y: index,
            w: 1,
            h: 1.5,
            i: item.key,
          };
        });
      }

      return acc;
    }, {});
  };

  const splitDOItemsByContacts = (
    items = [],
    filteredRelationship,
    isContactSpecificField,
  ) => {
    return items.reduce((acc, item) => {
      let itemsArr = [...acc];

      item.connectedContacts.forEach((c) => {
        const itemObj = {...item, contactId: c.contactId};
        const passedRelationship =
          !filteredRelationship ||
          c.relationships.some((cr) =>
            relationships.some(
              (r) => r.role === filteredRelationship && r.id === cr.id,
            ),
          );

        if (
          passedRelationship &&
          (isContactSpecificField ||
            !itemsArr.some((item2) => item2.id === itemObj.id))
        ) {
          const filteredCRelationships = c.relationships.filter((cr) =>
            relationships.some((r) => r.id === cr.id),
          );

          itemsArr = itemsArr.concat({
            ...itemObj,
            contactId: c.contactId,
            Email: c.email,
            FirstName: c.firstName,
            LastName: c.lastName,
            contactRelationships: filteredCRelationships
              .map((cr) => {
                const {role} = relationships.find((r) => r.id === cr.id);

                return role;
              })
              .join(','),
            key: `${choice}||${itemObj.id}||${c.contactId}`,
          });
        }
      });

      return itemsArr;
    }, []);
  };

  const mergeContactSpecificData = (data = [], contactId) => {
    return data.reduce((acc, d) => {
      const itemObj = {...d};
      const filteredField = doFields.find((field) => d.fieldId === field.id);

      if (!filteredField?.contactSpecificField) {
        return acc.concat(itemObj);
      } else {
        let itemsArr = [...acc];

        d.contactSpecificValues.forEach((v) => {
          if (v.contactId === contactId) {
            itemsArr = itemsArr.concat({
              ...itemObj,
              value: v.value,
            });
          }
        });

        return itemsArr;
      }
    }, []);
  };

  const mergeAndFilterFieldsData = (data) => {
    return mergeArraysBySameKeys(
      data,
      doFields.map((field) => ({
        ...field,
        fieldId: field.id,
      })),
      {
        keysToCheck: ['fieldId'],
        keysToMerge: ['name', 'contactSpecificField'],
      },
    ).filter((d) => [keyField, ...displayFields].includes(d.name));
  };

  const handleSearch = ({search}) => {
    setSearchFilter(search);
  };

  const handleFilters = (filterVal) => {
    setFilters(filterVal);
  };

  const handleChangePage = (event, value) => {
    setPage(value);
  };

  const handleDrop = async (layout, layoutItem, e) => {
    const fromChoice = e.dataTransfer.getData('fromChoice');
    const draggedItemData = JSON.parse(e.dataTransfer.getData('itemData'));

    if (fromChoice && fromChoice !== choice) {
      setLatestDroppedChoice(choice);

      const updatedKeyArr = draggedItemData.key.split('||');
      updatedKeyArr[0] = choice;
      const updatedKey = updatedKeyArr.join('||');

      const updatedDOItem = {
        ...draggedItemData,
        [keyField]: choice,
        key: updatedKey,
        saving: true,
      };

      const updatedItems = insert({
        arr: filteredItems || [],
        item: updatedDOItem,
        index: layoutItem.y,
      });

      setFilteredItems(updatedItems);

      await save(
        {
          details: {[keyField]: choice},
          primaryRelationship: {
            connectedContactId: draggedItemData.contactId,
            relationships: draggedItemData.contactRelationships,
          },
        },
        {
          itemId: draggedItemData.id,
        },
      );

      setFilteredItems((state) =>
        update({
          arr: state,
          item: {...updatedDOItem, saving: false},
          key: 'key',
        }),
      );

      // updateListQuery(filteredItems);
    }
  };

  // const updateListQuery = (items) => {
  //   doItemsQuery.client.writeQuery({
  //     query: LIST_DATA_OBJECTS,
  //     data: {
  //       listDataObjects: {
  //         __typename: 'DataObjectSearch',
  //         total: items.length || 1,
  //         items: update({
  //           arr: doItemsQuery.data.items,
  //           item: {
  //             __typename: 'DataObjectSearchItems',
  //             groupId,
  //             type,
  //             items: items.map(({id, data, connectedContacts, __typename}) => ({
  //               id,
  //               data,
  //               connectedContacts,
  //               __typename,
  //             })),
  //           },
  //           key: 'groupId',
  //         }),
  //       },
  //     },
  //     variables,
  //   });
  // };

  useLayoutEffect(() => {
    if (doItems && doFields && relationships) {
      const filteredField = doFields.find((field) => keyField === field.name);
      const splittedDOItemsByContacts = splitDOItemsByContacts(
        doItems,
        relationship,
        filteredField?.contactSpecificField,
      );
      const mergedDOItemsWithContactSpecificData = splittedDOItemsByContacts.map(
        (item) => {
          const data = mergeContactSpecificData(item.data, item.contactId);

          return {
            ...item,
            data,
          };
        },
      );

      //TODO: Will remove "clientFilterString" client side logic once "contactId" and "q" params filters correctly on backend. Should return to empty "" string
      const filteredDOItems = filterDOItems(
        clientFilterString,
        mergedDOItemsWithContactSpecificData,
        [
          {
            type: 'field',
            fieldId: filteredField?.id,
            filterVal: choice,
          },
        ],
      );

      // if (!filteredItems || doItemsQuery.loading || clientFilterStringChanged) {
      setFilteredItems(
        filteredDOItems.map((item) => {
          const data = mergeAndFilterFieldsData(item.data);

          return {
            ...item,
            ...data.reduce((acc, d) => {
              acc[d.name] = d.value;

              return acc;
            }, {}),
            saving: false,
          };
        }),
      );
      // }
    }
  }, [doItems, doFields, relationships, clientFilterStringChanged]);

  useDeepCompareEffect(() => {
    const updatedLayouts = generateLayouts(filteredItems);

    setLayouts(updatedLayouts);
  }, [filteredItems]);

  useDeepCompareEffect(() => {
    if (filteredItems?.length) {
      setTimeout(function () {
        window.dispatchEvent(new Event('resize'));
      }, 100);
    }
  }, [filteredItems]);

  const otherContactFields = displayFields.filter(
    (fieldName) =>
      !['FirstName', 'LastName', 'Email'].includes(fieldName) &&
      (CONTACT_DEFAULT_FIELDS.some((field) => field.name === fieldName) ||
        !doFields?.some((field) => field.name === fieldName)),
  );

  return (
    <SubSection
      style={{
        margin: '0 0.5rem',
        minWidth: 250,
      }}
      bodyStyle={{
        backgroundColor: '#e4e4e4',
        overflowY: 'unset',
      }}
      headerStyle={{
        backgroundColor:
          activeColumnsColor === 'active'
            ? WORKFLOW_HEADER_COLORS[indexProp % WORKFLOW_HEADER_COLORS.length]
            : '#999',
        color: 'white',
        borderBottom: 0,
        minHeight: '2.55rem',
      }}
      fullHeight
      title={choice}
      {...props}>
      <DOSearch
        FilterProps={{
          PopoverProps: {
            title: `${type} Filters`,
          },
        }}
        autoSearch
        disableCategories
        disableSuggestions
        onSearch={handleSearch}
        onFilter={handleFilters}
        filters={filters}
        groupId={groupId}
        TextFieldProps={{
          InputProps: {
            style: {
              borderRadius: 0,
              backgroundColor: 'white',
            },
          },
        }}
      />

      <Styled.BoardColumnContainer>
        <ResponsiveReactGridLayout
          {...props}
          layouts={layouts}
          isResizable={false}
          compactType="vertical"
          isDroppable={!isDragging}
          isDraggable={false}
          onDrop={handleDrop}>
          {filteredItems?.map((item) => {
            return (
              <WidgetStyled.ReactGridItemContainer
                key={item.key}
                draggable={true}
                style={{
                  zIndex: 1000,
                }}
                onDragStart={(e) => {
                  // NOTE: In case of using Firefox you should add
                  // `onDragStart={e => e.dataTransfer.setData('text/plain', '')}` attribute
                  // along with `draggable={true}` otherwise this feature will work incorrect.
                  // onDragStart attribute is required for Firefox for a dragging initialization
                  e.dataTransfer.setData('text/plain', '');

                  e.dataTransfer.setData('fromChoice', choice);
                  e.dataTransfer.setData('itemData', JSON.stringify(item));

                  e.target.classList.add('block-dragging');

                  setIsDragging(true);
                }}
                onDragEnd={(e) => {
                  e.target.classList.remove('block-dragging');

                  if (latestDroppedChoice && latestDroppedChoice !== choice) {
                    setFilteredItems((state) =>
                      remove({
                        arr: state,
                        key: 'key',
                        value: item.key,
                      }),
                    );

                    // updateListQuery(filteredItems);
                  }

                  setIsDragging(false);
                }}>
                <ContactQuery
                  id={item.contactId}
                  options={{
                    skip: !otherContactFields.length,
                    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
                  }}>
                  {({data: updatedContact, loading: contactLoading}) => {
                    const itemData = displayFields
                      .slice(0, 4)
                      .map((fieldName) => ({
                        name: fieldName,
                        value:
                          otherContactFields.includes(fieldName) &&
                          updatedContact
                            ? getContactFieldValue(updatedContact, fieldName)
                            : item[fieldName],
                      }));
                    const otherData = displayFields
                      .slice(4, displayFields.length)
                      .map((fieldName) => ({
                        name: fieldName,
                        value:
                          otherContactFields.includes(fieldName) &&
                          updatedContact
                            ? getContactFieldValue(updatedContact, fieldName)
                            : item[fieldName],
                      }));

                    return (
                      <BoardCard
                        item={item}
                        itemData={itemData}
                        groupId={groupId}
                        type={type}
                        doFields={doFields}
                        relationships={relationships}
                        otherContactFields={otherContactFields}
                        otherData={otherData}
                        contactLoading={contactLoading}
                      />
                    );
                  }}
                </ContactQuery>
              </WidgetStyled.ReactGridItemContainer>
            );
          })}
        </ResponsiveReactGridLayout>

        {total > (page + 1) * DEFAULT_PAGE_LIMIT && (
          <Pagination
            table
            count={total}
            size="small"
            page={page}
            onPageChange={handleChangePage}
            labelRowsPerPage={''}
            rowsPerPageOptions={[]}
            labelDisplayedRows={() => `Page ${page + 1}`}
          />
        )}
      </Styled.BoardColumnContainer>
      <LoadingIndicator
        loading={doItemsQuery.loading}
        transparent
        size={20}
        {...(page === 0
          ? {
              fill: true,
              align: 'top',
              style: {
                top: '3.5rem',
              },
            }
          : {
              style: {
                marginTop: '1rem',
                marginBottom: '1rem',
                justifyContent: 'center',
              },
            })}
      />
    </SubSection>
  );
};

BoardColumn.defaultProps = {
  rowHeight: 156,
  cols: {lg: 1, md: 1, sm: 1, xs: 1, xxs: 1},
  displayFields: [],
};

export default BoardColumn;
