import {useState, useLayoutEffect} from 'react';
import useQuery from '@macanta/hooks/apollo/useQuery';
import {GET_ACCESS_PERMISSIONS} from '@macanta/graphql/permissions';
import * as Storage from '@macanta/utils/storage';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';

const useUserGuides = (optionsArg) => {
  const {permission, ...options} = Object.assign({}, optionsArg);

  const userDetails = Storage.getItem('userDetails');

  const [general, setGeneral] = useState([]);
  const [sidebarTabs, setSidebarTabs] = useState([]);
  const [doSections, setDOSections] = useState([]);

  const {loading, data, error} = useQuery(GET_ACCESS_PERMISSIONS, {
    variables: {
      email: userDetails?.email,
    },
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
    onError() {},
    ...options,
  });

  const userGuides = permission
    ? permission.userGuides
    : data?.getAccessPermissions?.userGuides;

  useLayoutEffect(() => {
    if (userGuides) {
      setGeneral(
        userGuides.filter((guide) => guide.type === 'GeneralInterface'),
      );
      setSidebarTabs(
        userGuides.filter((guide) => guide.type === 'SidebarTabs'),
      );
      setDOSections(userGuides.filter((guide) => guide.type === 'DataObject'));
    }
  }, [data, userGuides]);

  return {loading, data, error, general, sidebarTabs, doSections};
};

export default useUserGuides;
