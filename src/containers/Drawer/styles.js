import React from 'react';
import {experimentalStyled as styled} from '@mui/material/styles';
import {Link} from '@reach/router';
import Box from '@mui/material/Box';
import List from '@mui/material/List';
import MUIListItem from '@mui/material/ListItem';
import Typography from '@mui/material/Typography';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import DrawerListItem from './DrawerListItem';
import * as applicationStyles from '@macanta/themes/applicationStyles';

// necessary for content to be below app bar
export const ContentToolbarSpacer = styled(Box)`
  ${({expanded}) =>
    !expanded
      ? applicationStyles.flexCenter
      : `
      ${applicationStyles.rowCenterJustifyStart}
      padding: 0 1rem;
      `}
  ${({theme}) => theme.mixins.toolbar}
`;

export const CompanyLogoLink = styled(Link)`
  display: flex;
  padding: 0;
  color: #888;
  text-decoration: none;
  width: 100%;
`;

export const DrawerContent = styled(Box)`
  height: 100%;
  display: flex;
  flex-direction: column;

  .MuiListItemText-root {
    margin: 0;
  }
`;

export const BottomList = styled(List)`
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
`;

export const ListItem = styled(MUIListItem)`
  ${({selected, theme}) =>
    selected &&
    `
    & .MuiListItemIcon-root {
      color: ${theme.palette.primary.main};
    }
  `}
`;

export const ExpandIcon = styled(({expanded, ...props}) =>
  expanded ? <ChevronLeftIcon {...props} /> : <ChevronRightIcon {...props} />,
)`
  font-size: 1.75rem;
`;

export const CompanyNameContainer = styled(Box)`
  flex: 1;
  ${applicationStyles.rowCenterJustifyEnd}
  margin-left: 1rem;
`;

export const CompanyName = styled(Typography)`
  font-size: 1.1rem;
`;

export const UserListItem = styled(DrawerListItem)`
  ${({expanded}) =>
    !expanded
      ? `
        ${applicationStyles.flexCenter}
        padding: 0.5rem 0;
      `
      : `
        ${applicationStyles.rowCenterJustifyBetween}
        padding: 0.5rem 1rem;
      `}
`;

export const UserAvatar = styled(Box)`
  width: 32px;
  height: 32px;
  border-radius: 32px;
  background-color: ${({theme}) => theme.palette.info.main};
  display: flex;
  justify-content: center;
  align-items: center;
  color: white;
  font-size: 18px;
  font-weight: bold;

  ${({expanded}) => expanded && `margin-right: 24px;`}
`;
