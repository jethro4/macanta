import {getSubdomain, getEmailDomain} from '@macanta/utils/uri';

describe('URI Utils', () => {
  describe('getSubdomain', () => {
    it('happy - parse domain', () => {
      const hostname = 'tk217.macantacrm.app';

      const actualResult = getSubdomain(hostname);
      const expectedResult = 'tk217';

      expect(actualResult).toEqual(expectedResult);
    });
  });

  describe('getEmailDomain', () => {
    it('happy - parse domain', () => {
      const email = 'pieter@bluepeg.org';

      const actualResult = getEmailDomain(email);
      const expectedResult = 'bluepeg.org';

      expect(actualResult).toEqual(expectedResult);
    });

    it('sad - null / undefined', () => {
      const actualResult1 = getEmailDomain(null);
      const actualResult2 = getEmailDomain(undefined);
      const expectedResult = '';

      expect(actualResult1).toEqual(expectedResult);
      expect(actualResult2).toEqual(expectedResult);
    });
  });
});
