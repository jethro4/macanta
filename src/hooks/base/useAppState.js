import {useState, useEffect, useRef} from 'react';

const DEFAULT_OPTIONS = {
  onForeground: () => {},
  onBackground: () => {},
};

const useAppState = (optionsArg) => {
  const options = useRef(Object.assign(DEFAULT_OPTIONS, optionsArg)).current;
  const statusRef = useRef('visible');
  const [status, setStatus] = useState(statusRef.current);

  const addListener = (event, callback) => {
    if (event === 'foreground') {
      options.onForeground = callback;
    } else if (event === 'background') {
      options.onBackground = callback;
    }

    return () => {
      if (event === 'foreground') {
        options.onForeground = DEFAULT_OPTIONS.onForeground;
      } else if (event === 'background') {
        options.onBackground = DEFAULT_OPTIONS.onBackground;
      }
    };
  };

  useEffect(() => {
    const appStateCallback = () => {
      const nextAppState = document.visibilityState;

      if (nextAppState !== 'hidden') {
        console.info('AppState: foreground!');

        options.onForeground();
      } else {
        console.info('AppState: background!');

        options.onBackground();
      }

      statusRef.current = nextAppState;
      setStatus(statusRef.current);
    };

    document.addEventListener('visibilitychange', appStateCallback);

    return () => {
      console.info('AppState: unsubscribe!');

      document.removeEventListener('visibilitychange', appStateCallback);
    };
  }, []);

  return {status, addListener};
};

export default useAppState;
