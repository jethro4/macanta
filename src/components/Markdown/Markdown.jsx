import React from 'react';
import ReactMarkdown from 'react-markdown';
import rehypeRaw from 'rehype-raw';

const Markdown = ({children, ...props}) => {
  return (
    <ReactMarkdown rehypePlugins={[rehypeRaw]} {...props}>
      {children}
    </ReactMarkdown>
  );
};

Markdown.defaultProps = {
  children: '',
  className: 'markdown-container',
};

export default Markdown;
