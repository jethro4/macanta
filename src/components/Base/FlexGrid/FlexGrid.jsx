import React, {forwardRef} from 'react';

import * as Styled from './styles';
import FlexGridItems from '@macanta/components/Base/FlexGrid/FlexGridItems';
import Colors from '@macanta/utils/colors';

let FlexGrid = (
  {
    cols,
    rowHeight,
    margin,
    containerPadding,
    // divider,
    border,
    borderColor,
    borderStyle,
    // loading,
    children,
    fitContent,
    hideOverflow,
    // sx,
    ...props
  },
  ref,
) => {
  const childrenArray = React.Children.toArray(children);
  const hasChildren = childrenArray.length > 0;

  return !hasChildren ? null : (
    <Styled.Root
      ref={ref}
      // sx={Object.assign(
      //   {},
      //   !!divider && {
      //     '& > *:not(:last-child):after': {
      //       content: '""',
      //       backgroundColor: Colors.border,
      //       // position: 'absolute',
      //       width: convertWithUnit(divider.w),
      //       height: convertWithUnit(divider.h),
      //       // top: '50%',
      //       // transform: 'translateY(-50%)',
      //       // right: '25%',
      //       display: 'block',
      //     },
      //   },
      //   sx,
      // )}
      {...props}>
      <FlexGridItems
        cols={cols}
        rowHeight={rowHeight}
        margin={margin}
        containerPadding={containerPadding}
        border={border}
        borderColor={borderColor}
        borderStyle={borderStyle}
        fitContent={fitContent}
        hideOverflow={hideOverflow}>
        {children}
      </FlexGridItems>
    </Styled.Root>
  );
};

FlexGrid = forwardRef(FlexGrid);

FlexGrid.defaultProps = {
  margin: [0, 0],
  border: 0,
  borderColor: Colors.borderLight,
  borderStyle: 'solid',
};

export default FlexGrid;
