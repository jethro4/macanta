import useValue from '@macanta/hooks/base/useValue';
import useQuery from '@macanta/hooks/apollo/useQuery';

const useSelectorQuery = ({
  selector: selectorArg,
  selectorArgs,
  options,
  itemKey,
  itemValue,
}) => {
  const selector = selectorArg || useQuery;

  const selectorArgsArr = [...Object.assign([], selectorArgs), options];
  const selectorQuery = selector(...selectorArgsArr);

  const [result] = useValue(() => {
    const isSpecificData = Boolean(itemKey && itemValue);

    if (isSpecificData) {
      const item = selectorQuery.result?.find((r) => r[itemKey] === itemValue);

      return item;
    }

    return selectorQuery.result;
  }, [itemKey, itemValue, selectorQuery.result]);

  return {...selectorQuery, result};
};

export default useSelectorQuery;
