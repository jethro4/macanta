import {gql} from '@apollo/client';

export default gql`
  mutation deleteDataObjectType(
    $deleteDataObjectTypeInput: DeleteDataObjectTypeInput!
  ) {
    deleteDataObjectType(input: $deleteDataObjectTypeInput) {
      success
      groupId
    }
  }
`;
