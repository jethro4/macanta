const typename = 'AutomationGroup';

export default (overwrites) => {
  return Object.assign(
    {
      __typename: typename,
    },
    {
      active: false,
      conditionsActions: [
        {
          actionId: '',
          active: false,
          conditionId: '',
          revalidate: false,
          time: '',
          unit: '',
          waitFor: null,
        },
      ],
      description: '',
      id: '',
      name: '',
    },
    overwrites,
  );
};
