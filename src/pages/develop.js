/* eslint-disable react/jsx-filename-extension */
import React, {useEffect} from 'react';
import {navigate} from 'gatsby';
import envConfig from '@macanta/config/envConfig';
import DevModeContainer from '@macanta/modules/DevModeContainer';

const DevPage = () => {
  useEffect(() => {
    if (!envConfig.isDev) {
      navigate('/');
    }
  }, []);

  return <DevModeContainer path="develop" />;
};

export default DevPage;
