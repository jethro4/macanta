import React, {useState} from 'react';
import Modal from '@macanta/components/Modal';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import SelectButton from '@macanta/components/Button/SelectButton';
import DOScheduleForm from '@macanta/modules/AdminSettings/DOScheduler/DOScheduleForm';

const DOTypesSelectButton = ({getModalProps, onSave, ...props}) => {
  const [selectedDOType, setSelectedDOType] = useState(null);

  const showModal = !!selectedDOType;

  const doTypesQuery = useDOTypes({
    removeEmail: true,
  });

  const doTypes = doTypesQuery?.data?.listDataObjectTypes;

  const handleSelect = (item) => {
    setSelectedDOType(item);
  };

  const handleClose = () => {
    setSelectedDOType(null);
  };

  const handleSave = async (values) => {
    await onSave(values);

    handleClose();
  };
  return (
    <>
      <SelectButton
        options={doTypes}
        labelKey="title"
        valueKey="id"
        onSelect={handleSelect}
        {...props}
      />
      <Modal
        headerTitle={`Edit Schedule - ${selectedDOType?.title}`}
        open={showModal}
        onClose={handleClose}
        contentWidth={500}
        {...getModalProps?.({title: selectedDOType?.title})}>
        <DOScheduleForm
          item={{
            groupId: selectedDOType?.id,
            schedulerType: 'Date',
          }}
          onSave={handleSave}
        />
      </Modal>
    </>
  );
};

export default DOTypesSelectButton;
