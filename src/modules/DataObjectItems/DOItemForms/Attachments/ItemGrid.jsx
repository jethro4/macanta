import React from 'react';
import Image from '@macanta/components/Image';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import GetAppIcon from '@mui/icons-material/GetApp';
import DeleteIcon from '@mui/icons-material/Delete';
import PermissionMetaQuery from '@macanta/modules/hoc/PermissionMetaQuery';
import {transformImageThumbnail} from '@macanta/utils/file';
import * as Styled from './styles';

const ItemGrid = ({
  items,
  onViewFile,
  onDelete,
  onDownload,
  hideDownload,
  renderGridActions,
}) => {
  return (
    items.length > 0 && (
      <Grid
        container
        style={{
          paddingLeft: '1rem',
          paddingBottom: '1rem',
        }}>
        {items.map((item, index) => {
          const base64Img = item.isNew
            ? transformImageThumbnail(item.fileExt, item.thumbnail)
            : `data:image/png;base64,${item.thumbnail}`;

          const fileExt = item.downloadUrl?.trim().split('.').pop();
          const isPDF = fileExt === 'pdf';

          return (
            <Grid key={item.id || index} item xs={12} md={6} lg={4}>
              <Card
                style={{
                  marginTop: '1rem',
                  marginRight: '1rem',
                }}>
                <Styled.GridItemContainer>
                  <Styled.CardActionArea
                    onClick={onViewFile(
                      item.downloadUrl || item.thumbnail,
                      item.fileName,
                      item.fileExt || fileExt,
                      item.isPDF || isPDF,
                    )}
                    style={{
                      height: 120,
                      display: 'flex',
                      flexDirection: 'column',
                      alignItems: 'center',
                      paddingTop: '1rem',
                    }}>
                    <Box
                      style={{
                        flex: 1,
                        overflow: 'hidden',
                      }}>
                      <CardMedia
                        style={{
                          maxHeight: '100%',
                        }}
                        component={(props) => {
                          return <Image {...props} src={base64Img} />;
                        }}
                        src={base64Img}
                      />
                    </Box>
                    <CardContent>
                      <Styled.GridItemLabel
                        variant="caption"
                        color="textSecondary">
                        {item.fileName}
                      </Styled.GridItemLabel>
                    </CardContent>
                  </Styled.CardActionArea>
                  {renderGridActions ? (
                    renderGridActions(item)
                  ) : (
                    <Styled.CardActions disableSpacing className="card-actions">
                      <PermissionMetaQuery validate="deleteDO">
                        <Styled.ActionIconButton onClick={onDelete(item.id)}>
                          <DeleteIcon
                            style={{
                              fontSize: '0.875rem',
                              color: 'white',
                            }}
                          />
                        </Styled.ActionIconButton>
                      </PermissionMetaQuery>
                      {!hideDownload && (
                        <Styled.ActionIconButton
                          onClick={onDownload(item.id)}
                          aria-label="download">
                          <GetAppIcon
                            style={{
                              fontSize: '0.875rem',
                              color: 'white',
                            }}
                          />
                        </Styled.ActionIconButton>
                      )}
                    </Styled.CardActions>
                  )}
                </Styled.GridItemContainer>
              </Card>
            </Grid>
          );
        })}
      </Grid>
    )
  );
};

export default ItemGrid;
