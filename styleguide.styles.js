module.exports = {
  // Para: {
  //   para: {
  //     fontSize: '20px',
  //     fontWeight: 'bold',
  //   },
  // },
  Playground: {
    preview: {
      border: '1px dashed #e8e8e8',
    },
  },

  // Code: {
  //   code: {
  //     // make inline code example appear the same color as links
  //     color: theme.link,
  //     fontSize: 14,
  //   },
  // },

  // Logo: {
  //   logo: {
  //     // we can now change the color used in the logo item to use the theme's `link` color
  //     color: theme.link,
  //   },
  // },
};

// modules.exports = {
//   Logo: {
//     // We're changing the LogoRenderer component
//     logo: {
//       // We're changing the rsg--logo-XX class name inside the component
//       animation: '$blink ease-in-out 300ms infinite'
//     },
//     '@keyframes blink': {
//       to: { opacity: 0 }
//     }
//   }
// }
