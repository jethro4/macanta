import React from 'react';
import Typography from '@mui/material/Typography';
import PreviewIcon from '@mui/icons-material/Preview';
import {PopoverButton} from '@macanta/components/Button';
import Markdown from '@macanta/components/Markdown';
import EmailAttachments from '@macanta/modules/ContactDetails/EmailAttachments';

const EmailPreview = ({
  loading,
  values,
  attachments,
  style,
  PopoverProps,
  ...props
}) => {
  return (
    <PopoverButton
      icon
      color="secondary"
      loading={loading}
      size="small"
      style={style}
      TooltipProps={{
        title: 'Email Preview',
        placement: 'top',
      }}
      PopoverProps={{
        title: 'Email Preview',
        contentWidth: 900 - 32,
        contentMinHeight: 400 - 32,
        contentMaxHeight: 600 - 32,
        bodyStyle: {
          padding: '1rem',
          overflowY: 'auto',
          boxSizing: 'content-box',
        },
        anchorOrigin: {
          vertical: 'center',
          horizontal: 'left',
        },
        transformOrigin: {
          vertical: 'center',
          horizontal: 'right',
        },
        ...PopoverProps,
      }}
      renderContent={() => (
        <>
          <Typography
            style={{
              fontWeight: 'bold',
              marginBottom: 20,
            }}>
            {values.subject || 'No subject'}
          </Typography>
          <Markdown>{values.message?.replace(/[\n\r]/g, '')}</Markdown>
          {values.signature &&
            !values.message?.includes('~Sender.Signature~') && (
              <Markdown>
                {`<div style="margin-top: 30px;">${values.signature}</div>`}
              </Markdown>
            )}
          {attachments && (
            <EmailAttachments files={attachments} disableDelete />
          )}
        </>
      )}
      {...props}>
      <PreviewIcon />
    </PopoverButton>
  );
};

export default EmailPreview;
