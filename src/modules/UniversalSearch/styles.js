import {experimentalStyled as styled} from '@mui/material/styles';

import ListItem from '@mui/material/ListItem';
import Search from '@macanta/containers/Search';

export const Root = styled(Search)``;

export const ContactsListItem = styled(ListItem)``;
