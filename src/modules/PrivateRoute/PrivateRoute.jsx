import React from 'react';
import {navigate} from 'gatsby';
import useIsAdmin from '@macanta/hooks/useIsAdmin';
import {isLoggedIn} from '@macanta/utils/app';
import * as Styled from './styles';
import * as Storage from '@macanta/utils/storage';

const PrivateRoute = ({
  auth,
  level,
  component: Component,
  children,
  ...props
}) => {
  const isAdmin = useIsAdmin();

  if (auth && !isLoggedIn()) {
    if (window.location.pathname !== `/`) {
      const session = Storage.getItem('session');

      if (!session.loggingOut) {
        Storage.setItem('session', {
          lastUrl: window.location.pathname,
        });
      }

      navigate('/');
    }

    return null;
  } else if (level === 'admin' && !isAdmin) {
    const {userId: loggedInUserId} = Storage.getItem('userDetails');

    navigate(`/app/contact/${loggedInUserId}/notes`, {
      replace: true,
    });

    return null;
  }

  return (
    <Component {...props}>
      <Styled.Root>{children}</Styled.Root>
    </Component>
  );
};
export default PrivateRoute;
