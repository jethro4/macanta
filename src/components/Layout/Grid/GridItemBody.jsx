import React, {forwardRef} from 'react';
import Box from '@mui/material/Box';

let GridItemBody = ({item, renderGridItem, ...props}, ref) => {
  return (
    <Box ref={ref} {...props}>
      {renderGridItem({item})}
    </Box>
  );
};

GridItemBody = forwardRef(GridItemBody);
GridItemBody = React.memo(GridItemBody);

export default GridItemBody;
