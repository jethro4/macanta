import React, {useState, useEffect} from 'react';
import {useFormikContext} from 'formik';
import TurnedInIcon from '@mui/icons-material/TurnedIn';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import Button from '@macanta/components/Button';
import Form from '@macanta/components/Form';
import FormField from '@macanta/containers/FormField';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import usePrevious from '@macanta/hooks/usePrevious';
import useCustomTab from '@macanta/hooks/admin/useCustomTab';
import {useMutation} from '@apollo/client';
import {
  CREATE_OR_UPDATE_CUSTOM_TAB,
  CUSTOM_TAB_ITEM_FRAGMENT,
} from '@macanta/graphql/admin';
import customTabValidationSchema from '@macanta/validations/customTab';
import * as Styled from './styles';
import * as ContactDetailsStyled from '@macanta/modules/ContactDetails/styles';
import * as NoteTaskFormsStyled from '@macanta/modules/NoteTaskForms/styles';
import * as QueryBuilderStyled from '@macanta/modules/AdminSettings/QueryBuilder/styles';
import * as applicationStyles from '@macanta/themes/applicationStyles';

const CustomTabFormsContainer = ({id, title, onSave, ...props}) => {
  const {displayMessage} = useProgressAlert();

  const customTabQuery = useCustomTab({id});

  const customTab = customTabQuery.data;

  const getInitValues = () => {
    return {
      id,
      title: customTab?.title || title,
      content: customTab?.content || '',
    };
  };

  const [initValues, setInitValues] = useState(() => getInitValues());
  const prevInitValues = usePrevious(initValues);

  const [
    callCreateOrUpdateCustomTab,
    {client, ...createOrUpdateCustomTabMutation},
  ] = useMutation(CREATE_OR_UPDATE_CUSTOM_TAB, {
    onError() {},
    onCompleted: (data) => {
      if (data?.createOrUpdateCustomTab) {
        displayMessage('Saved successfully!');

        client.writeFragment({
          id: `CustomTabItem:${id}`,
          fragment: CUSTOM_TAB_ITEM_FRAGMENT,
          data: {
            title: data?.createOrUpdateCustomTab?.title,
          },
        });

        onSave && onSave();
      }
    },
  });

  const handleFormSubmit = async (values) => {
    callCreateOrUpdateCustomTab({
      variables: {
        createOrUpdateCustomTabInput: {
          id: values.id,
          title: values.title,
          content: values.content,
        },
        __mutationkey: 'createOrUpdateCustomTabInput',
      },
    });
  };

  useEffect(() => {
    if (customTab && prevInitValues === initValues) {
      setInitValues(getInitValues());
    }
  }, [customTab]);

  const initLoading = !customTab && customTabQuery.loading;

  return (
    <>
      <Form
        initialValues={initValues}
        enableReinitialize={true}
        validationSchema={customTabValidationSchema}
        onSubmit={handleFormSubmit}>
        {({values}) => {
          return (
            <CustomTabForms
              item={values}
              hideLoading={initLoading}
              loadingCustomTab={customTabQuery.loading}
              errorUnassignment={createOrUpdateCustomTabMutation.error}
              {...props}
            />
          );
        }}
      </Form>
      <LoadingIndicator fill backdrop loading={initLoading} />
      <LoadingIndicator
        modal
        loading={createOrUpdateCustomTabMutation.loading}
      />
    </>
  );
};

const CustomTabForms = ({
  item,
  hideLoading,
  loadingCustomTab,
  errorUnassignment,
  ...props
}) => {
  const {errors, handleChange, handleSubmit, dirty} = useFormikContext();

  const loading = !hideLoading && loadingCustomTab;

  return (
    <QueryBuilderStyled.FormRoot {...props}>
      <QueryBuilderStyled.Body
        style={{
          padding: '1rem',
          display: 'flex',
          flexDirection: 'column',
        }}>
        <Styled.TextField
          autoFocus
          value={item?.title}
          error={errors.title}
          onChange={handleChange('title')}
          label="Title"
          fullWidth
          size="small"
          variant="outlined"
        />

        <ContactDetailsStyled.EmailRichTextFieldContainer>
          <Box
            style={{
              position: 'relative',
              display: 'flex',
              flexDirection: 'column',
              flex: 1,
            }}>
            <FormField
              type="RichText"
              fieldStyle={{
                flex: 1,
              }}
              style={{
                flex: 1,
                marginBottom: 0,
              }}
              label="Content"
              error={errors.content}
              onChange={handleChange('content')}
              placeholder="Type Content Here"
              value={item?.content}
            />
          </Box>
        </ContactDetailsStyled.EmailRichTextFieldContainer>

        <LoadingIndicator
          fill
          align="top"
          style={{
            top: '2.5rem',
          }}
          loading={loading}
        />
      </QueryBuilderStyled.Body>
      <NoteTaskFormsStyled.Footer
        sx={applicationStyles.footer}
        style={{
          padding: '1rem',
          margin: 0,
          borderTop: '1px solid #eee',
          width: '100%',
          backgroundColor: 'white',
        }}>
        <Box
          style={{
            flex: 1,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          {!!errorUnassignment && (
            <Typography
              color="error"
              style={{
                fontSize: '0.75rem',
                textAlign: 'center',
                marginRight: '1rem',
              }}>
              {errorUnassignment.message}
            </Typography>
          )}
        </Box>
        <Button
          disabled={!dirty}
          variant="contained"
          startIcon={<TurnedInIcon />}
          onClick={handleSubmit}
          size="medium">
          Save
        </Button>
      </NoteTaskFormsStyled.Footer>
    </QueryBuilderStyled.FormRoot>
  );
};

export default CustomTabFormsContainer;
