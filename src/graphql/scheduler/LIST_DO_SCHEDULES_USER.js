import {gql} from '@apollo/client';
// import {DO_SCHEDULE_USER_FRAGMENT} from '@macanta/graphql/scheduler/fragments';

export default gql`
  query listDOSchedulesUser(
    $contactId: ID
    $dateStart: String
    $dateEnd: String
  ) {
    listDOSchedulesUser(
      contactId: $contactId
      dateStart: $dateStart
      dateEnd: $dateEnd
    ) {
      total
      items {
        date
        items
        # items {
        #   ...doScheduleUserFragment
        # }
      }
    }
  }
`;
