import {useRef} from 'react';

const useFixedValue = (value) => {
  const valueRef = useRef(value);

  return valueRef.current;
};

export default useFixedValue;
