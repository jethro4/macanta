import useQuery from '@macanta/hooks/apollo/useQuery';
import {LIST_DO_SCHEDULES} from '@macanta/graphql/scheduler';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import useValue from '@macanta/hooks/base/useValue';
import {every, sortArrayByPriority} from '@macanta/utils/array';

const useDOSchedules = (options) => {
  const doSchedulesQuery = useQuery(LIST_DO_SCHEDULES, options);
  const doTypesQuery = useDOTypes({
    removeEmail: true,
  });

  const initLoaded = Boolean(
    every([doSchedulesQuery.initLoaded, doTypesQuery.initLoaded], true),
  );

  const [sortedData] = useValue(() => {
    if (initLoaded) {
      const sortedResults = sortArrayByPriority(
        doSchedulesQuery.result,
        'groupId',
        doTypesQuery.result,
        'id',
      );

      return sortedResults;
    }

    return [];
  }, [initLoaded, doSchedulesQuery.result, doTypesQuery.result]);

  return {...doSchedulesQuery, data: sortedData, initLoaded};
};

export default useDOSchedules;
