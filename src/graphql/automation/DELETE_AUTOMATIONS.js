import {gql} from '@apollo/client';

export default gql`
  mutation deleteAutomations($input: ContactActionInput!) {
    deleteAutomations(input: $input) {
      id
      success
    }
  }
`;
