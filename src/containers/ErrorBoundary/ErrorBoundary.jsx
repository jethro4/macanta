import React from 'react';
import sentryConfig from '@macanta/config/sentryConfig';

class GlobalBoundaryException extends Error {
  constructor(...params) {
    super(...params);
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, GlobalBoundaryException);
    }
    this.name = 'GlobalBoundaryException';

    if (params[1]) {
      this.stack = `${this.name} - ${params[1]}`;
    }
  }
}

class ReactBoundaryException extends Error {
  constructor(...params) {
    super(...params);
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, ReactBoundaryException);
    }
    this.name = 'ReactBoundaryException';

    if (params[1]) {
      this.stack = `${this.name} - ${params[1]}`;
    }
  }
}

class ErrorBoundary extends React.Component {
  static defaultProps = {
    operationName: '',
    errorType: 'ReactUnhandled',
    allowReactExternalLogging: false,
    allowWindowExternalLogging: false,
  };

  componentDidCatch(error) {
    const errTitle = [
      this.props.operationName,
      this.props.errorType,
      error.message,
    ]
      .filter((t) => !!t)
      .join(' - ');

    console.error(new ReactBoundaryException(errTitle, error.stack));

    if (this.props.allowReactExternalLogging) {
      sentryConfig.captureException({
        err: new ReactBoundaryException(errTitle, error.stack),
        useMessageAsFingerprint: true,
      });
    }
  }

  componentDidMount() {
    const handleOnError = (msg, url, lineNo, columnNo, error) => {
      var string = msg.toLowerCase();
      var substring = 'script error';

      let operationName;
      let errorType;
      let errTitle;

      if (string.indexOf(substring) > -1) {
        operationName = '';
        errorType = 'CORSError';
        errTitle = [operationName, errorType, error.message]
          .filter((t) => !!t)
          .join(' - ');

        console.error(new GlobalBoundaryException(errTitle, error.stack));
      } else {
        operationName = '';
        errorType = 'RuntimeError';
        errTitle = [operationName, errorType, error.message]
          .filter((t) => !!t)
          .join(' - ');

        console.error(new GlobalBoundaryException(errTitle, error.stack));
      }

      if (this.props.allowWindowExternalLogging) {
        sentryConfig.captureException({
          err: new GlobalBoundaryException(errTitle, error.stack),
          useMessageAsFingerprint: true,
        });
      }

      return false;
    };

    window.onerror = handleOnError;
  }

  render() {
    // if (this.state.hasError) {
    //   // You can render any custom fallback UI
    //   return <h1>Something went wrong.</h1>;
    // }

    return this.props.children;
  }
}

export default ErrorBoundary;
