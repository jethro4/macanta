import React, {useMemo} from 'react';
import {useFormikContext} from 'formik';
import PreviewIcon from '@mui/icons-material/Preview';
import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import {PopoverButton} from '@macanta/components/Button';
import Markdown from '@macanta/components/Markdown';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import useDORelationships from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDORelationships';
import useNextEffect from '@macanta/hooks/base/useNextEffect';
import usePreviousTruthy from '@macanta/hooks/base/usePreviousTruthy';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {FORM_TYPES} from '@macanta/constants/form';
import {remove} from '@macanta/utils/array';
import * as Styled from './styles';

const SidebarBasicInfo = (props) => {
  const {
    values,
    errors,
    handleChange,
    setFieldValue,
    setValues,
  } = useFormikContext();

  const doTypesQuery = useDOTypes({
    removeEmail: true,
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });
  const relationshipsQuery = useDORelationships(values.doType, {
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
    skip: values.doType === 'contactsOnly',
  });
  const prevType = usePreviousTruthy(values.type);
  const prevDOType = usePreviousTruthy(values.doType);

  const doTypes = doTypesQuery.data?.listDataObjectTypes;
  const relationships = relationshipsQuery.data?.listRelationships;
  const hasChangedType = values.type !== prevType;
  const hasChangedDOType = values.doType !== prevDOType;

  useNextEffect(() => {
    if (hasChangedType || hasChangedDOType) {
      setValues((state) => {
        let doGroups = [...state.doGroups];
        const firstGroup = doGroups[0] || {
          columns: 3,
          id: 'subGroup-General',
          subGroupName: 'General',
          fields: [],
        };
        const firstGroupFields =
          hasChangedDOType || !firstGroup?.fields ? [] : firstGroup.fields;

        if (values.type === 'Integrated') {
          const fields = [
            {
              id: 'field_integration',
              fieldId: 'field_integration',
              label: 'Integration Field',
              required: true,
              tooltip: '',
              column: 1,
              type: 'Select',
              choices: ['Sample 1', 'Sample 2'],
            },
          ].concat(firstGroupFields);

          doGroups[0] = {
            ...firstGroup,
            fields,
          };
        } else {
          doGroups[0] = {
            ...firstGroup,
            fields: remove({
              arr: firstGroupFields,
              key: 'fieldId',
              value: 'field_integration',
            }),
          };
        }

        return {
          ...state,
          doGroups: values.doType === 'contactsOnly' ? [] : doGroups,
          relationships: hasChangedDOType ? [] : state.relationships,
        };
      });
    }
  }, [hasChangedDOType, hasChangedType]);

  useNextEffect(() => {
    if (
      doTypes?.length &&
      values.type !== 'Standard' &&
      values.doType === 'contactsOnly'
    ) {
      setFieldValue('doType', doTypes[0].id);
    }
  }, [doTypes, values.type]);

  const doTypesWithContactObject = useMemo(() => {
    if (doTypes) {
      return [
        {
          id: 'contactsOnly',
          title: 'Contact Object Only',
          disabled: values.type !== 'Standard',
        },
      ].concat(doTypes);
    }

    return [];
  }, [doTypes, values.type]);

  return (
    <Box {...props}>
      <Styled.SidebarField
        required
        value={values.title}
        error={errors.title}
        onChange={handleChange('title')}
        label="Title"
        fullWidth
        size="small"
        variant="outlined"
      />
      <Styled.SidebarField
        type="TextArea"
        rows={2}
        hideExpand
        value={values.description}
        onChange={handleChange('description')}
        label="Description"
        fullWidth
        size="small"
        variant="outlined"
      />
      <Divider
        style={{
          margin: '24px 0 16px',
          borderBottom: '1px solid #eee',
        }}
      />
      <Styled.SidebarField
        required
        type="Select"
        options={FORM_TYPES}
        value={values.type}
        onChange={handleChange('type')}
        label="Form Type"
        fullWidth
        size="small"
        variant="outlined"
      />
      <Styled.SidebarField
        required
        type="Select"
        options={doTypesWithContactObject}
        labelKey="title"
        valueKey="id"
        value={values.doType}
        onChange={handleChange('doType')}
        label="Data Object Type"
        fullWidth
        size="small"
        variant="outlined"
      />
      {values.doType !== 'contactsOnly' && (
        <Styled.SidebarField
          required
          type="MultiSelect"
          options={relationships}
          labelKey="role"
          valueKey="id"
          value={values.relationships}
          error={errors.relationships}
          onChange={(val) => setFieldValue('relationships', val)}
          label="Relationships"
          fullWidth
          size="small"
          variant="outlined"
        />
      )}
      <Divider
        style={{
          margin: '24px 0 16px',
          borderBottom: '1px solid #eee',
        }}
      />
      <Styled.SidebarField
        required
        value={values.submitLabel}
        error={errors.submitLabel}
        onChange={handleChange('submitLabel')}
        label="Submit Button Label"
        fullWidth
        size="small"
        variant="outlined"
      />
      <Styled.SidebarField
        type="URL"
        value={values.redirectUrl}
        onChange={handleChange('redirectUrl')}
        label="Redirect URL"
        fullWidth
        size="small"
        variant="outlined"
      />
      <Styled.SidebarField
        type="TextArea"
        rows={2}
        hideExpand
        value={values.thankMessage}
        onChange={handleChange('thankMessage')}
        label="Thank You Message"
        fullWidth
        size="small"
        variant="outlined"
        TopRightComp={
          <PopoverButton
            disabled={!values.thankMessage}
            color="secondary"
            icon
            // onClick={handleAdd}
            size="small"
            TooltipProps={{
              title: 'Preview',
            }}
            PopoverProps={{
              contenWidth: 600,
              contentMinWidth: 250,
              bodyStyle: {
                margin: '1rem',
              },
              anchorOrigin: {
                vertical: 'center',
                horizontal: 'right',
              },
              transformOrigin: {
                vertical: 'center',
                horizontal: 'left',
              },
            }}
            renderContent={() =>
              !!values.thankMessage && (
                <Markdown>{values.thankMessage}</Markdown>
              )
            }>
            <PreviewIcon />
          </PopoverButton>
        }
      />
    </Box>
  );
};

export default SidebarBasicInfo;
