import {gql} from '@apollo/client';
import {DO_SCHEDULE_FRAGMENT} from '@macanta/graphql/scheduler/fragments';

export default gql`
  query listDOSchedules {
    listDOSchedules {
      ...doScheduleFragment
    }
  }
  ${DO_SCHEDULE_FRAGMENT}
`;
