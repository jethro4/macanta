import React, {useMemo} from 'react';

import {ThemeProvider, createMuiTheme} from '@mui/material/styles';
import useThemeColors from '@macanta/hooks/useThemeColors';
import * as applicationStyles from '@macanta/themes/applicationStyles';

const Theme = ({theme: customTheme, children}) => {
  const {themeColors, renderThemeColorProvider} = useThemeColors();
  const primaryColor = themeColors.primary;
  const secondaryColor = themeColors.secondary;
  const tertiaryColor = themeColors.tertiary;
  const prefersDarkMode = false; //useMediaQuery('(prefers-color-scheme: dark)');

  const theme = useMemo(
    () =>
      createMuiTheme({
        breakpoints: {
          values: {
            xs: 0,
            xsm: 480,
            sm: 768,
            md: 1200,
            lg: 1500,
            xl: 1920,
          },
        },
        spacing: applicationStyles.spacing,
        palette: {
          background: prefersDarkMode ? {} : {default: themeColors.background},
          text: {
            primary: themeColors.textPrimary,
            secondary: themeColors.textSecondary,
            gray: themeColors.textGray,
            grayLight: themeColors.textGrayLight,
            blue: themeColors.textBlue,
            link: themeColors.linkPrimary,
          },
          table: {
            headRow: '#f2f4f5',
            headBorderColor: '#e0e0e0',
            bodyRow: '#fafafa',
          },
          primary: {
            main: prefersDarkMode ? secondaryColor : primaryColor,
            contrastText: themeColors.white,
          },
          secondary: {
            main: prefersDarkMode ? tertiaryColor : secondaryColor,
          },
          tertiary: {
            main: prefersDarkMode ? themeColors.white : tertiaryColor,
          },
          gray: {
            main: themeColors.gray,
            contrastText: themeColors.grayDarker,
          },
          grayLight: {
            main: themeColors.grayLight,
            contrastText: themeColors.grayDarkest,
          },
          grayLighter: {
            main: themeColors.grayLighter,
            contrastText: themeColors.grayDarkest,
          },
          grayLightest: {
            main: themeColors.grayLightest,
            contrastText: themeColors.grayDarkest,
          },
          grayDark: {
            main: themeColors.grayDark,
            contrastText: themeColors.white,
          },
          grayDarker: {
            main: themeColors.grayDarker,
            contrastText: themeColors.white,
          },
          grayDarkest: {
            main: themeColors.grayDarkest,
            contrastText: themeColors.white,
          },
          success: {
            main: themeColors.success,
          },
          info: {
            main: themeColors.info,
          },
          warning: {
            main: themeColors.warning,
          },
          error: {
            main: themeColors.error,
          },
          light: {
            main: themeColors.white,
          },
          border: {
            main: themeColors.border,
          },
          borderLight: {
            main: themeColors.borderLight,
          },
          borderLighter: {
            main: themeColors.borderLighter,
          },
          borderLightest: {
            main: themeColors.borderLightest,
          },
          type: prefersDarkMode ? 'dark' : 'light',
          ...customTheme.palette,
        },
        typography: {
          htmlFontSize: applicationStyles.HTML_FONT_SIZE,
          fontFamily: `"Fira Sans", "Arial", "Helvetica", sans-serif`,
        },
        components: {
          // Set default heights for html, body and gatsby outer elements
          MuiCssBaseline: {
            styleOverrides: {
              html: {
                height: '100%',
              },
              body: {
                height: '100%',
                p: {
                  margin: 0,
                },
              },
              '#___gatsby': {
                height: '100%',
              },
              '#gatsby-focus-wrapper': {
                height: '100%',

                '& > div:first-of-type': {
                  height: '100%',
                },
              },
              '.tox-notification': {
                display: 'none !important',
              },
              '.MuiDrawer-root': {
                backgroundColor: themeColors.white,
              },
              '.MuiTooltip-popper': {
                pointerEvents: 'none !important',
              },
              '.MuiFormLabel-asterisk': {
                color: themeColors.error,
              },
              '.MuiFormControlLabel-root, .MuiListItem-root': {
                '&.MuiFormControlLabel-root': {
                  padding: 7,
                },
                '.MuiCheckbox-root, .MuiRadio-root': {
                  padding: 0,
                  marginRight: 7,

                  '.MuiSvgIcon-root': {
                    fontSize: '18px',
                  },
                },

                '.MuiTypography-root': {
                  fontSize: '0.9375rem',

                  '&.MuiFormControlLabel-label, &.MuiFormControlLabel-label .MuiTypography-root': {
                    lineHeight: '1rem',
                  },
                },
              },
              '.MuiList-root, .MuiAutocomplete-listbox': {
                '.MuiListItem-root, .MuiMenuItem-root': {
                  fontSize: '0.9375rem',

                  '.MuiTypography-root': {
                    fontSize: '0.9375rem',

                    '&.MuiListItemText-secondary': {
                      fontSize: '0.8125rem',
                    },
                  },
                },
              },
              '.MuiAutocomplete-paper': {
                boxShadow:
                  '0px 5px 5px -3px rgb(0 0 0 / 20%), 0px 8px 10px 1px rgb(0 0 0 / 14%), 0px 3px 14px 2px rgb(0 0 0 / 12%) !important',
              },
            },
          },
        },
      }),
    [prefersDarkMode, customTheme, primaryColor, secondaryColor, tertiaryColor],
  );

  return (
    <ThemeProvider theme={theme}>
      {renderThemeColorProvider(children)}
    </ThemeProvider>
  );
};

Theme.defaultProps = {
  theme: {},
};

export default Theme;
