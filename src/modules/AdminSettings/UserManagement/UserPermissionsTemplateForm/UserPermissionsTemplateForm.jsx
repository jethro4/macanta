import React, {useState, useEffect} from 'react';
import {useTheme} from '@mui/material/styles';
import Box from '@mui/material/Box';
import MenuList from '@mui/material/MenuList';
import ListItem from '@mui/material/ListItem';
import Divider from '@mui/material/Divider';
import Form from '@macanta/components/Form';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import Section from '@macanta/containers/Section';
import Show from '@macanta/containers/Show';
import useValue from '@macanta/hooks/base/useValue';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import usePermissionTemplates from '@macanta/hooks/admin/usePermissionTemplates';
import usePermissionTemplateMutation from '@macanta/hooks/permission/usePermissionTemplateMutation';
import TemplateInfoForm from '@macanta/modules/AdminSettings/UserManagement/Tabs/TemplateInfoForm';
import SidebarTabAccessForm from '@macanta/modules/AdminSettings/UserManagement/Tabs/SidebarTabAccessForm';
import SectionAccessForm from '@macanta/modules/AdminSettings/UserManagement/Tabs/SectionAccessForm';
import MQBAccessForm from '@macanta/modules/AdminSettings/UserManagement/Tabs/MQBAccessForm';
import TaskAccessForm from '@macanta/modules/AdminSettings/UserManagement/Tabs/TaskAccessForm';
import EmailTemplateAccessForm from '@macanta/modules/AdminSettings/UserManagement/Tabs/EmailTemplateAccessForm';
import ContactViewAccessForm from '@macanta/modules/AdminSettings/UserManagement/Tabs/ContactViewAccessForm';
import WorkflowBoardAccessForm from '@macanta/modules/AdminSettings/UserManagement/Tabs/WorkflowBoardAccessForm';
import OtherSettingsAccessForm from '@macanta/modules/AdminSettings/UserManagement/Tabs/OtherSettingsAccessForm';
import UserGuidesForm from '@macanta/modules/AdminSettings/UserManagement/Tabs/UserGuidesForm';
import FooterActionButtons from '@macanta/modules/AdminSettings/UserManagement/FooterActionButtons';

export const PERMISSION_TEMPLATE_FORM_TABS = [
  {
    label: 'Template Info',
    value: 'info',
    component: TemplateInfoForm,
  },
  {
    label: 'Tab Access',
    value: 'tabs',
    component: SidebarTabAccessForm,
  },
  {
    label: 'Data Object Sections',
    value: 'sections',
    component: SectionAccessForm,
  },
  {
    label: 'Query Widgets',
    value: 'mqb',
    component: MQBAccessForm,
  },
  {
    label: 'Task Access',
    value: 'tasks',
    component: TaskAccessForm,
  },
  {
    label: 'Inline Email Template Groups',
    value: 'emailTemplates',
    component: EmailTemplateAccessForm,
  },
  {
    label: 'Contact View Permissions',
    value: 'contactView',
    component: ContactViewAccessForm,
  },
  {
    label: 'Workflow Boards',
    value: 'boards',
    component: WorkflowBoardAccessForm,
  },
  {
    label: 'Other Settings',
    value: 'otherSettings',
    component: OtherSettingsAccessForm,
  },
  {
    label: 'User Guides',
    value: 'userGuides',
    component: UserGuidesForm,
  },
];

const UserPermissionsTemplateForm = ({item, onSave}) => {
  const isEdit = !!item;

  const theme = useTheme();

  const [selectedTab, setSelectedTab] = useState(
    PERMISSION_TEMPLATE_FORM_TABS[0],
  );

  const doTypesQuery = useDOTypes({
    removeEmail: true,
  });
  const permissionTemplatesQuery = usePermissionTemplates(item?.id, {
    skip: !item?.id,
  });

  const [
    callCreateOrUpdatePermissionTemplate,
    createOrUpdatePermissionTemplate,
  ] = usePermissionTemplateMutation(item, {
    onCompleted: ({result}) => {
      if (!isEdit || item?.id !== result.id) {
        onSave?.();
      }
    },
  });

  const doTypes = doTypesQuery.data?.listDataObjectTypes;
  const selectedTemplate = permissionTemplatesQuery.selectedTemplate;

  const handleFormSubmit = ({saveAsNew, id, ...values}) => {
    callCreateOrUpdatePermissionTemplate({
      variables: Object.assign(
        {doTypes},
        values,
        !saveAsNew && {
          id,
        },
      ),
    });
  };

  useEffect(() => {
    if (selectedTab) {
      setTimeout(function () {
        window.dispatchEvent(new Event('resize'));
      }, 0);
    }
  }, [selectedTab]);

  const [initValues] = useValue(
    () => ({
      permission: {},
      ...selectedTemplate,
    }),
    [selectedTemplate],
  );

  const loading = doTypesQuery.initLoading || permissionTemplatesQuery.loading;

  return (
    <Box height="100%" data-testid="UserPermissionsTemplateForm">
      <Form
        initialValues={initValues}
        enableReinitialize
        // validationSchema={dataImportsValidationSchema}
        onSubmit={handleFormSubmit}>
        {({values, setFieldValue}) => {
          const handleChange = (updatedPermission) => {
            setFieldValue('permission', {
              ...values.permission,
              ...updatedPermission,
            });
          };

          return (
            <Section
              loading={loading}
              fullHeight
              headerStyle={{
                display: 'none',
              }}
              style={{
                margin: 0,
                boxShadow: 'none',
              }}
              bodyStyle={{
                display: 'flex',
                flexDirection: 'column',
                backgroundColor: theme.palette.grayLight.main,
                overflowY: 'hidden',
              }}
              FooterComp={
                <FooterActionButtons
                  isEdit={isEdit}
                  disabled={doTypesQuery.initLoading}
                />
              }>
              <Box
                style={{
                  display: 'flex',
                  height: '100%',
                }}>
                <Section
                  title="Settings"
                  style={{
                    minWidth: 250,
                    marginTop: '1rem',
                    marginBottom: '1rem',
                    alignSelf: 'flex-start',
                  }}>
                  <MenuList
                    style={{
                      height: '100%',
                      padding: '0.5rem 0',
                    }}>
                    {PERMISSION_TEMPLATE_FORM_TABS.map((tab, index) => {
                      return (
                        <>
                          <ListItem
                            key={tab.value}
                            button
                            selected={tab.value === selectedTab.value}
                            onClick={() =>
                              setSelectedTab(
                                PERMISSION_TEMPLATE_FORM_TABS[index],
                              )
                            }
                            data-testid={`listitem-${tab.value}`}>
                            {tab.label}
                          </ListItem>
                          <Divider
                            style={{
                              borderBottom: '1px solid #f5f5f5',
                            }}
                          />
                        </>
                      );
                    })}
                    {/* <SelectMenuItems
              popover
              withDivider
              value={selectedTab.value}
              options={PERMISSION_TEMPLATE_FORM_TABS}
              allowSearch={false}
              onChange={setSelectedTab}
            /> */}
                  </MenuList>
                </Section>

                <Box
                  style={{
                    flexGrow: 1,
                    flexBasis: 0,
                    marginTop: '1rem',
                    marginBottom: '1rem',
                  }}>
                  {PERMISSION_TEMPLATE_FORM_TABS.map((tab) => {
                    return (
                      <Show
                        key={tab.value}
                        removeHidden
                        in={selectedTab.value === tab.value}
                        data-testid={`itembody-${tab.value}`}>
                        <tab.component
                          title={tab.label}
                          permission={values.permission}
                          {...(tab.value !== 'info' && {
                            onChange: handleChange,
                          })}
                        />
                      </Show>
                    );
                  })}
                </Box>
              </Box>
            </Section>
          );
        }}
      </Form>

      <LoadingIndicator
        modal
        loading={createOrUpdatePermissionTemplate.loading}
      />
    </Box>
  );
};

export default UserPermissionsTemplateForm;
