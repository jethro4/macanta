import React from 'react';
import Typography from '@mui/material/Typography';
import Form from '@macanta/components/Form';
import SendIcon from '@mui/icons-material/Send';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import widgetSmsValidationSchema from '@macanta/validations/widgetSms';
import {useMutation} from '@apollo/client';
import {SMS} from '@macanta/graphql/admin';
import * as NoteTaskFormsStyled from '@macanta/modules/NoteTaskForms/styles';
import * as Styled from './styles';
import * as Storage from '@macanta/utils/storage';

const SMSBatchContactForms = ({widgets, onSuccess, ...props}) => {
  const {userId: loggedInUserId} = Storage.getItem('userDetails');

  const initValues = {
    widgetIds: [],
    message: '',
  };
  const queryWidgets = widgets.filter((w) => w.type === 'MacantaQuery');

  const [calSMSMutation, smsMutation] = useMutation(SMS, {
    onCompleted(data) {
      if (data.sms?.success) {
        onSuccess();
      }
    },
    onError() {},
  });

  const handleSendSMS = (values) => {
    const selectedWidgets = queryWidgets.filter((widget) =>
      values.widgetIds.includes(widget.id),
    );

    const queryIds = selectedWidgets.map((w) => w.queryId).join('|');
    const widgetNames = selectedWidgets.map((w) => w.title).join('|');
    const dataObjectNames = selectedWidgets.map((w) => w.groupName).join('|');

    calSMSMutation({
      variables: {
        smsInput: {
          userId: loggedInUserId,
          sendMethod: 'widgets',
          txtMessage: values.message,
          queryId: queryIds,
          widgetName: widgetNames,
          dataObjectName: dataObjectNames,
        },
        __mutationkey: 'smsInput',
      },
    });
  };

  return (
    <NoteTaskFormsStyled.Root {...props}>
      <NoteTaskFormsStyled.Container>
        <Form
          initialValues={initValues}
          validationSchema={widgetSmsValidationSchema}
          onSubmit={handleSendSMS}>
          {({values, errors, handleChange, setFieldValue, handleSubmit}) => (
            <>
              <NoteTaskFormsStyled.FormGroup>
                <Styled.MultiSelectChipInput
                  labelPosition="normal"
                  select
                  multiple
                  chipped
                  options={queryWidgets}
                  labelKey="title"
                  valueKey="id"
                  // placeholder="Select Widget..."
                  label="Query Widgets"
                  error={errors.widgetIds}
                  onChange={(val) => {
                    setFieldValue('widgetIds', val);
                  }}
                  variant="outlined"
                  value={values.widgetIds}
                  fullWidth
                  size="medium"
                />

                <NoteTaskFormsStyled.TextField
                  labelPosition="normal"
                  type="TextArea"
                  hideExpand
                  error={errors.message}
                  onChange={handleChange('message')}
                  label="Message"
                  variant="outlined"
                  value={values.message}
                  fullWidth
                  size="medium"
                />
              </NoteTaskFormsStyled.FormGroup>

              {!!smsMutation.error && (
                <Typography
                  color="error"
                  style={{
                    fontSize: '0.75rem',
                    textAlign: 'center',
                    marginTop: '1rem',
                  }}>
                  {smsMutation.error.message}
                </Typography>
              )}

              <NoteTaskFormsStyled.Footer>
                <NoteTaskFormsStyled.FooterButton
                  disabled={!values.message}
                  variant="contained"
                  startIcon={<SendIcon />}
                  onClick={handleSubmit}
                  size="medium">
                  Send
                </NoteTaskFormsStyled.FooterButton>
              </NoteTaskFormsStyled.Footer>
            </>
          )}
        </Form>
      </NoteTaskFormsStyled.Container>
      <LoadingIndicator modal loading={smsMutation.loading} />
    </NoteTaskFormsStyled.Root>
  );
};

export default SMSBatchContactForms;
