import {gql} from '@apollo/client';

export default gql`
  query listRoundRobins($appName: String!, $apiKey: String!, $groupId: ID!) {
    listRoundRobins(appName: $appName, apiKey: $apiKey, groupId: $groupId) {
      id
      name
      relationship
    }
  }
`;
