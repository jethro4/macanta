import {gql} from '@apollo/client';
import {WORKFLOW_BOARD_FRAGMENT} from './fragments';

export default gql`
  mutation createOrUpdateWorkflowBoard(
    $createOrUpdateWorkflowBoardInput: CreateOrUpdateWorkflowBoardInput!
  ) {
    createOrUpdateWorkflowBoard(input: $createOrUpdateWorkflowBoardInput) {
      ...workflowBoardFragment
    }
  }
  ${WORKFLOW_BOARD_FRAGMENT}
`;
