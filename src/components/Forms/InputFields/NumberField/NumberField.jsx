import React from 'react';
import PropTypes from 'prop-types';
import TextField from '@macanta/components/Forms/InputFields/TextField';

const wholeNumberRegex = /[0-9]/;
// const decimalNumberRegex = /[0-9]*\.?[0-9]*/;

const NumberField = ({format, ...props}) => {
  const handleKeyPress = (event) => {
    let regex;

    switch (format) {
      // case 'decimal': {
      //   regex = decimalNumberRegex;
      //   break;
      // }
      case 'wholeNumber': {
        regex = wholeNumberRegex;
        break;
      }
    }

    if (regex && !regex.test(event.key)) {
      event.preventDefault();
    }
  };

  let type;

  switch (format) {
    case 'wholeNumber': {
      type = 'number';
      break;
    }
  }

  return (
    <TextField
      {...props}
      {...(type && {
        type,
      })}
      onKeyPress={handleKeyPress}
    />
  );
};

NumberField.propTypes = {
  format: PropTypes.oneOf(['wholeNumber', 'decimal']),
};

NumberField.defaultProps = {
  format: 'wholeNumber',
};

export default NumberField;
