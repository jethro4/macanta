/* eslint-disable react/jsx-filename-extension */
import React from 'react';
import {Router} from '@reach/router';
import LoginPage from '@macanta/modules/pages/LoginPage';
import AppContainer from '@macanta/modules/pages/AppContainer';
import Widgets from '@macanta/modules/pages/AppContainer/Widgets';
import ContactRecords from '@macanta/modules/pages/AppContainer/ContactRecords';
import NoteTaskHistory from '@macanta/modules/NoteTaskHistory';
import DataObjectItems from '@macanta/modules/DataObjectItems';
import CustomTab from '@macanta/modules/CustomTab';
import CommunicationHistory from '@macanta/modules/CommunicationHistory';
import UserSettings from '@macanta/modules/UserSettings';
import AdminSettings from '@macanta/modules/AdminSettings';
import AppDetails from '@macanta/modules/AdminSettings/AppDetails';
import QueryBuilder from '@macanta/modules/AdminSettings/QueryBuilder';
import BlastProgress from '@macanta/modules/AdminSettings/BlastProgress';
import LookAndFeel from '@macanta/modules/AdminSettings/LookAndFeel';
import CustomTabs from '@macanta/modules/AdminSettings/CustomTabs';
import Relationships from '@macanta/modules/AdminSettings/Relationships';
import DataTools from '@macanta/modules/AdminSettings/DataTools';
import WorkflowBoards from '@macanta/modules/AdminSettings/WorkflowBoards';
import InternalSupportEmails from '@macanta/modules/AdminSettings/InternalSupportEmails';
import ExcludeEmailSyncDomains from '@macanta/modules/AdminSettings/ExcludeEmailSyncDomains';
import ObjectBuilder from '@macanta/modules/AdminSettings/ObjectBuilder';
import UserManagement from '@macanta/modules/AdminSettings/UserManagement';
import Automation from '@macanta/modules/AdminSettings/Automation';
import FormBuilder from '@macanta/modules/AdminSettings/FormBuilder';
import DOWorkflowBoards from '@macanta/modules/DOWorkflowBoards';
import BoardColumns from '@macanta/modules/DOWorkflowBoards/BoardColumns';
import UserSchedules from '@macanta/modules/UserSchedules';
import PrivateRoute from '@macanta/modules/PrivateRoute';
import BaseTheme from '@macanta/modules/BaseTheme';

import CreateAutomationWorkFlow from '@macanta/modules/AdminSettings/Automation/AutomationGroup/CreateAutomationWorkFlow';
import EditAutomationWorkFlow from '@macanta/modules/AdminSettings/Automation/AutomationGroup/EditAutomationWorkFlow';
import AdvancedEditAutomationWorkFlow from '@macanta/modules/AdminSettings/Automation/AutomationGroup/AdvancedEditAutomationWorkFlow';

import DOScheduler from '@macanta/modules/AdminSettings/DOScheduler';

const AppPage = () => {
  return (
    <BaseTheme>
      <Router>
        <PrivateRoute auth path="/app" component={AppContainer}>
          <Widgets path="dashboard" />
          <PrivateRoute path="contact/:contactId" component={ContactRecords}>
            <NoteTaskHistory path="notes" />
            <DataObjectItems path="data-object/:groupId/:doTitle" />
            <CustomTab path="custom/:id/:title" />
            <CommunicationHistory path="communication" />
          </PrivateRoute>
          <UserSettings path="user-settings" />
          <PrivateRoute
            level="admin"
            path="admin-settings"
            component={AdminSettings}>
            <AppDetails path="app-details" />
            <QueryBuilder path="query-builder" />
            <BlastProgress path="blast-progress" />
            <LookAndFeel path="look-and-feel" />
            <CustomTabs path="custom-tabs" />
            <Relationships path="contact-relationships" />
            <DataTools path="data-import-export" />
            <WorkflowBoards path="workflow-boards" />
            <InternalSupportEmails path="internal-support-emails" />
            <ExcludeEmailSyncDomains path="exclude-email-sync-domains" />
            <ObjectBuilder path="object-builder" />
            <UserManagement path="user-management" />
            <PrivateRoute path="automation" component={Automation}>
              <CreateAutomationWorkFlow path="new-automation-workflow" />
              <EditAutomationWorkFlow path="edit-automation-workflow" />
              <AdvancedEditAutomationWorkFlow path="advanced-automation-editor" />
            </PrivateRoute>
            <FormBuilder path="form-builder" />
            <DOScheduler path="scheduler" />
          </PrivateRoute>
          <PrivateRoute path="workflow-boards" component={DOWorkflowBoards}>
            <BoardColumns path=":groupId/:boardTitle/:boardId" />
          </PrivateRoute>
          <PrivateRoute path="schedules" component={UserSchedules} />
        </PrivateRoute>
        <LoginPage path="/" />
      </Router>
    </BaseTheme>
  );
};

export default AppPage;
