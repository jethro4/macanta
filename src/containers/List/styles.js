import {experimentalStyled as styled} from '@mui/material/styles';

import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import ListComp from '@macanta/components/List';

export const Root = styled(ListComp)``;

export const ListItem = styled(Box)`
  display: flex;
  flex-direction: column;
`;

export const EmptyMessage = styled(Typography)`
  padding-top: ${({theme}) => theme.spacing(4)};
`;
