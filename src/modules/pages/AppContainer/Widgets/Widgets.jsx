import React, {
  useState,
  useCallback,
  useRef,
  useEffect,
  useLayoutEffect,
} from 'react';
import {WidthProvider, Responsive} from 'react-grid-layout';
import AddWidgetIconBtn from './AddWidgetIconBtn';
import SMSBatchContactBtn from './SMSBatchContactBtn';
import EmailBatchWidgetsIconBtn from './EmailBatchWidgetsIconBtn';
import throttle from 'lodash/throttle';
import debounce from 'lodash/debounce';
import isNumber from 'lodash/isNumber';
import ScrollableAreaProvider from '@macanta/components/Layout/ScrollableAreaProvider';
import Section from '@macanta/containers/Section';
import {useMutation} from '@apollo/client';
import useRetryQuery from '@macanta/hooks/apollo/useRetryQuery';
import {LIST_WIDGETS, SORT_WIDGETS} from '@macanta/graphql/admin';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import useAvailableWidgetsByPermissions from '@macanta/hooks/useAvailableWidgetsByPermissions';
import * as Styled from './styles';
import * as Storage from '@macanta/utils/storage';
import WidgetItemContainer from '@macanta/modules/pages/AppContainer/Widgets/WidgetItemContainer';

const ResponsiveReactGridLayout = WidthProvider(Responsive);
const MAX_COLUMNS = 6;
const DRAGGING_ACTIVE_OFFSET = 4;
const ROW_HEIGHT = 236;

const sortByCoordinates = (items) => {
  const sortedItems = items.slice().sort((item1, item2) => {
    if (item1.y < item2.y) {
      return -1;
    }
    if (item1.y > item2.y) {
      return 1;
    }
    if (item1.x < item2.x) {
      return -1;
    }
    return 0;
  });

  return sortedItems;
};

const Widgets = (props) => {
  const {userId: loggedInUserId} = Storage.getItem('userDetails');

  const {displayMessage} = useProgressAlert();

  const [addedWidgets, setAddedWidgets] = useState([]);
  const [selectedWidgetOptions, setSelectedWidgetOptions] = useState(null);
  const [layouts, setLayouts] = useState({});
  const [gridOptions, setGridOptions] = useState({});

  const draggedRef = useRef(false);
  const baseOffsetRef = useRef(null);
  const largestOffsetDiffRef = useRef(0);
  const breakpointRef = useRef();
  const layoutItemsRef = useRef();

  const {loading, data, client} = useRetryQuery(LIST_WIDGETS, {
    variables: {
      userId: loggedInUserId,
    },
  });

  const [callWidgetMutation, widgetMutation] = useMutation(SORT_WIDGETS, {
    async onCompleted(dataResponse) {
      if (dataResponse.sortWidgets?.success) {
        const layoutWidgets = layoutItemsRef.current.map((item) =>
          addedWidgets.find((aw) => aw.id === item.i),
        );

        updateAddedWidgets({addedWidgets: layoutWidgets});

        displayMessage('Saved widget order');
      }
    },
    onError() {},
  });

  const availableTaskWidgets = useAvailableWidgetsByPermissions({
    type: 'availableTaskWidgets',
    widgets: data?.listWidgets?.availableTaskWidgets,
    disabledWidgets: addedWidgets,
  });
  const availableQueryWidgets = useAvailableWidgetsByPermissions({
    type: 'availableQueryWidgets',
    widgets: data?.listWidgets?.availableQueryWidgets,
    disabledWidgets: addedWidgets,
  });

  const handleDragging = useCallback(
    throttle(
      (layout, oldItem, newItem, p, e) => {
        const newOffset = e.pageX + e.pageY;

        if (!isNumber(baseOffsetRef.current)) {
          baseOffsetRef.current = newOffset;
        } else {
          const offsetDiff = Math.abs(newOffset - baseOffsetRef.current);

          if (offsetDiff > largestOffsetDiffRef.current) {
            largestOffsetDiffRef.current = offsetDiff;
          }
        }

        draggedRef.current = false;
      },
      30,
      {leading: false, trailing: true},
    ),
    [],
  );

  const optimizeLayouts = (layout) => {
    const optimizedLayouts = {};

    layoutItemsRef.current = generateLayouts(breakpointRef.current, layout);

    optimizedLayouts[breakpointRef.current] = layoutItemsRef.current;

    return optimizedLayouts;
  };

  const saveWidgetOrder = useCallback(
    debounce(
      (layout, widget) => {
        callWidgetMutation({
          variables: {
            sortWidgetsInput: {
              userId: loggedInUserId,
              widgetsOrder: layout.map((item) => {
                const addedWidget = addedWidgets.find((w) => w.id === item.i);

                return widget && widget?.id === addedWidget.id
                  ? widget.title
                  : addedWidget.title;
              }),
            },
            __mutationkey: 'sortWidgetsInput',
          },
        });
      },
      400,
      {leading: false, trailing: true},
    ),
    [addedWidgets],
  );

  const handleDragStop = (layout, oldItem, newItem) => {
    const hasSwitched = oldItem.x !== newItem.x || oldItem.y !== newItem.y;
    if (largestOffsetDiffRef.current >= DRAGGING_ACTIVE_OFFSET || hasSwitched) {
      draggedRef.current = true;
    }

    if (hasSwitched) {
      const optimizedLayouts = optimizeLayouts(layout);

      setLayouts(optimizedLayouts);
      saveWidgetOrder(optimizedLayouts[breakpointRef.current]);
    }

    baseOffsetRef.current = null;
    largestOffsetDiffRef.current = 0;
  };

  const handleViewWidgetData = (widget, range) => {
    if (!draggedRef.current) {
      setSelectedWidgetOptions({id: widget.id, range});
    }

    draggedRef.current = false;
  };

  const handleCloseWidgetData = () => {
    setTimeout(() => setSelectedWidgetOptions(null), 250);
  };

  const handleAddWidget = async (newWidget) => {
    const exists = addedWidgets.some((w) => w.id === newWidget.id);
    const updatedWidgets = !exists
      ? addedWidgets.concat(newWidget)
      : addedWidgets.map((w) => {
          if (w.id === newWidget.id) {
            return newWidget;
          }

          return w;
        });

    setAddedWidgets(updatedWidgets);

    updateAddedWidgets({addedWidgets: updatedWidgets});

    if (exists) {
      saveWidgetOrder(layoutItemsRef.current, newWidget);
    }
  };

  const updateAddedWidgets = (widgets) => {
    return client.writeQuery({
      query: LIST_WIDGETS,
      data: {
        listWidgets: {
          __typename: 'Widgets',
          addedWidgets: widgets.addedWidgets,
          availableTaskWidgets:
            widgets.availableTaskWidgets ||
            data?.listWidgets?.availableTaskWidgets,
          availableQueryWidgets:
            widgets.availableQueryWidgets ||
            data?.listWidgets?.availableQueryWidgets,
        },
      },
      variables: {
        userId: loggedInUserId,
      },
    });
  };

  const handleDeleteWidget = async (widgetId) => {
    const updatedWidgets = addedWidgets.filter((w) => w.id !== widgetId);
    const deletedWidget = addedWidgets.find((w) => w.id === widgetId);
    const updatedTaskOrQueryWidgets = {};

    if (
      deletedWidget?.type === 'MacantaQuery' &&
      !data?.listWidgets?.availableQueryWidgets?.some(
        (widget) => widget.queryId === deletedWidget.queryId,
      )
    ) {
      updatedTaskOrQueryWidgets.availableQueryWidgets = data?.listWidgets?.availableQueryWidgets?.concat(
        {
          id: `${deletedWidget.userId}-${deletedWidget.queryId}-MacantaQuery`,
          queryId: deletedWidget.queryId,
          title: deletedWidget.title,
          type: 'MacantaQuery',
          userId: deletedWidget.userId,
          access: '',
          __typename: 'WidgetOption',
        },
      );
    }

    setAddedWidgets(updatedWidgets);

    updateAddedWidgets({
      addedWidgets: updatedWidgets,
      ...updatedTaskOrQueryWidgets,
    });
  };

  const handleTaskComplete = (widgetId) => {
    const updatedWidgets = addedWidgets.map((w) => {
      if (w.id === widgetId) {
        return {
          ...w,
          currentCount: w.currentCount - 1,
          last7DayCount: w.last7DayCount + 1,
          last30DayCount: w.last30DayCount + 1,
          last90DayCount: w.last90DayCount + 1,
          allCount: w.allCount + 1,
        };
      }

      return w;
    });

    setAddedWidgets(updatedWidgets);

    updateAddedWidgets({addedWidgets: updatedWidgets});
  };

  const getMaxColumnByBreakpoint = (breakpoint) => {
    let maxColumn = 6;

    switch (breakpoint) {
      case 'lg': {
        maxColumn = 6;
        break;
      }
      case 'md': {
        maxColumn = 5;
        break;
      }
      case 'sm': {
        maxColumn = 3;
        break;
      }
      case 'xs': {
        maxColumn = 2;
        break;
      }
      case 'xxs': {
        maxColumn = 1;
        break;
      }
    }

    return maxColumn;
  };

  // const handleLayoutChange = useCallback(
  //   throttle(
  //     (layout, newLayouts) => {
  //       const optimizedLayouts = {};

  //       Object.entries(newLayouts).forEach(([breakpointKey, items]) => {
  //         optimizedLayouts[breakpointKey] = generateLayouts(
  //           breakpointKey,
  //           items,
  //         );
  //       });

  //       setLayouts(optimizedLayouts);
  //     },
  //     30,
  //     {leading: false, trailing: true},
  //   ),
  //   [],
  // );

  const handleBreakpointChange = (breakpoint) => {
    breakpointRef.current = breakpoint;

    const optimizedLayouts = optimizeLayouts(
      layoutItemsRef.current || addedWidgets.map((w) => ({...w, i: w.id})),
    );

    setLayouts(optimizedLayouts);

    generateGridOptions(breakpoint);
  };

  const generateLayouts = (breakpoint, items) => {
    const maxColumn = getMaxColumnByBreakpoint(breakpoint);

    const sortedItems = sortByCoordinates(items);

    return sortedItems.map((item, index) => {
      return {
        x: index % maxColumn,
        y: Math.floor(index / maxColumn),
        w: 1,
        h: 1,
        i: item.i,
      };
    });
  };

  const generateGridOptions = (breakpoint) => {
    const maxColumn = getMaxColumnByBreakpoint(breakpoint);

    const maxRows = Math.floor((addedWidgets.length - 1) / maxColumn) + 1;

    setGridOptions({
      maxRows,
    });
  };

  useLayoutEffect(() => {
    if (data?.listWidgets) {
      setAddedWidgets(data.listWidgets.addedWidgets);
    }
  }, [data?.listWidgets]);

  useLayoutEffect(() => {
    if (addedWidgets?.length) {
      if (breakpointRef.current) {
        const optimizedLayouts = optimizeLayouts(
          addedWidgets.map((w) => ({...w, i: w.id})),
        );

        setLayouts(optimizedLayouts);
      }

      generateGridOptions(breakpointRef.current);
    }
  }, [addedWidgets]);

  useEffect(() => {
    if (addedWidgets.length) {
      setTimeout(function () {
        window.dispatchEvent(new Event('resize'));
      }, 0);
    }
  }, [addedWidgets]);

  const breakpointLayouts = layouts[breakpointRef.current];

  return (
    <>
      <Section
        loading={loading || widgetMutation.loading}
        fullHeight
        title="Dashboard"
        bodyStyle={{backgroundColor: '#f5f6fa', overflow: 'hidden'}}
        HeaderRightComp={
          <Styled.HeaderRightContainer>
            <SMSBatchContactBtn
              variant="outlined"
              addedWidgets={addedWidgets}
              style={{
                marginRight: '1rem',
              }}
            />
            <EmailBatchWidgetsIconBtn
              variant="outlined"
              addedWidgets={addedWidgets}
              style={{
                marginRight: '1rem',
              }}
            />
            <AddWidgetIconBtn
              taskWidgets={availableTaskWidgets}
              queryWidgets={availableQueryWidgets}
              onSuccess={handleAddWidget}
            />
          </Styled.HeaderRightContainer>
        }
        {...props}>
        <ScrollableAreaProvider debounceDelay={400}>
          {addedWidgets?.length > 0 && (
            <ResponsiveReactGridLayout
              className="layout"
              breakpoints={{lg: 1700, md: 1200, sm: 768, xs: 480, xxs: 0}}
              cols={{lg: 6, md: 5, sm: 3, xs: 2, xxs: 1}}
              // autoSize={false}
              onDrag={handleDragging}
              onDragStop={handleDragStop}
              isResizable={false}
              rowHeight={ROW_HEIGHT}
              margin={[16, 16]}
              containerPadding={[16, 16]}
              layouts={layouts}
              compactType="horizontal"
              // onLayoutChange={handleLayoutChange}
              onBreakpointChange={handleBreakpointChange}
              {...gridOptions}>
              {!!breakpointLayouts &&
                addedWidgets.map((widget, index) => {
                  const tasksCountLoading =
                    widget.type === 'MacantaTaskSearch' && loading;
                  const checkDragBeforeOpen = (open, ...args) => {
                    if (!draggedRef.current) {
                      open(...args);
                    }

                    draggedRef.current = false;
                  };

                  return (
                    <Styled.ReactGridItemContainer
                      key={widget.id}
                      data-grid={{
                        w: 1,
                        h: 1,
                        x: (index % MAX_COLUMNS) * 2,
                        y: Math.floor(index / MAX_COLUMNS),
                        minW: 1,
                        minH: 1,
                      }}>
                      <WidgetItemContainer
                        key={widget.id}
                        selectedOptions={selectedWidgetOptions}
                        layout={breakpointLayouts}
                        widget={widget}
                        onOpen={handleViewWidgetData}
                        onClose={handleCloseWidgetData}
                        onDelete={handleDeleteWidget}
                        onEdit={handleAddWidget}
                        onClick={checkDragBeforeOpen}
                        onTaskComplete={handleTaskComplete}
                        tasksCountLoading={tasksCountLoading}
                      />
                    </Styled.ReactGridItemContainer>
                  );
                })}
            </ResponsiveReactGridLayout>
          )}
        </ScrollableAreaProvider>
      </Section>
    </>
  );
};

export default Widgets;
