import useQuery from '@macanta/hooks/apollo/useQuery';
import {LIST_DATA_OBJECT_TYPES} from '@macanta/graphql/dataObjects';

const useDOType = (id, options = {}) => {
  const {loading, data, error} = useQuery(LIST_DATA_OBJECT_TYPES, {
    ...options,
    skip: !id || options?.skip === true,
  });

  const doType = data?.listDataObjectTypes.find((type) => type.id === id);

  return {loading, data: doType, error};
};

export default useDOType;
