import React, {useEffect} from 'react';
import {useFormikContext} from 'formik';
import useNextEffect from '@macanta/hooks/base/useNextEffect';
import * as Styled from './styles';

const RequiredField = ({name, ...props}) => {
  const {
    touched,
    setTouched,
    initialValues,
    validateField,
  } = useFormikContext();

  useNextEffect(() => {
    setTimeout(() => {
      validateField(name);
    }, 0);
  }, [props.value]);

  useEffect(() => {
    if (!touched[name] && initialValues[name] !== props.value) {
      setTouched({...touched, [name]: true});
    }
  }, [props.value]);

  return <Styled.TextField timeout={400} {...props} />;
};

export default RequiredField;
