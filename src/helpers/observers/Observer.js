class Observer {
  constructor(callback) {
    this.callback = callback;
  }

  notify = (data) => {
    this.callback(data);
  };
}

export default Observer;
