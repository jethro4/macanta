import React from 'react';
import SelectAction from '@macanta/modules/AdminSettings/Automation/TriggerActions/SubForms/SelectAction';
import {useWebhookActions} from '@macanta/hooks/automation';

const Select = ({...props}) => {
  const webhookActionsQuery = useWebhookActions({
    variables: {
      id: '',
    },
  });

  return (
    <SelectAction
      loading={webhookActionsQuery.initLoading}
      type="Select"
      options={webhookActionsQuery.result}
      labelKey="name"
      valueKey="id"
      {...props}
    />
  );
};

export default Select;
