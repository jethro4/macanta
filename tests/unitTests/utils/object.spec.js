import isNil from 'lodash/isNil';
import isUndefined from 'lodash/isUndefined';
import isNumber from 'lodash/isNumber';
import {
  replaceProperties,
  removeProperties,
  removeNilValues,
  removeUndefinedValues,
  readProperties,
  removeEmptyValues,
  removeFunctions,
  replaceKeys,
} from '@macanta/utils/object';

describe('Object Utils', () => {
  describe('readProperties', () => {
    it('by comparing primitive value', () => {
      const stringValue = '2';
      const nullValue = null;
      const numericValue = 1;
      const nestedStringValue = 'type_1';

      const arr = [
        undefined,
        undefined,
        1,
        {a: 1, b: '2', c: undefined, d: null},
        [
          {e: 'id_1', f: 'sample_1', g: 'type_1'},
          {e: 'id_2', f: 'sample_2', g: 'type_2', h: 'type_1'},
          {e: 'id_3', f: 'sample_3', g: 'type_3'},
          {e: 'id_4', f: 'sample_4', g: 'type_1'},
        ],
      ];

      const stringValueResult = readProperties(arr, stringValue);
      const nullValueResult = readProperties(arr, nullValue);
      const numericValueResult = readProperties(arr, numericValue);
      const nestedStringValueResult = readProperties(arr, nestedStringValue);
      const compareKeyResult1 = readProperties(arr, nestedStringValue, 'g');
      const compareKeyResult2 = readProperties(arr, nestedStringValue, 'h');

      expect(stringValueResult).toEqual([
        {
          key: 'b',
          reference: arr[3],
        },
      ]);
      expect(nullValueResult).toEqual([
        {
          key: 'd',
          reference: arr[3],
        },
      ]);
      expect(numericValueResult).toEqual([
        {
          key: '2',
          reference: arr,
        },
        {
          key: 'a',
          reference: arr[3],
        },
      ]);
      expect(nestedStringValueResult).toEqual([
        {
          key: 'g',
          reference: {e: 'id_1', f: 'sample_1', g: 'type_1'},
        },
        {
          key: 'h',
          reference: {e: 'id_2', f: 'sample_2', g: 'type_2', h: 'type_1'},
        },
        {
          key: 'g',
          reference: {e: 'id_4', f: 'sample_4', g: 'type_1'},
        },
      ]);
      expect(compareKeyResult1).toEqual([
        {e: 'id_1', f: 'sample_1', g: 'type_1'},
        {e: 'id_4', f: 'sample_4', g: 'type_1'},
      ]);
      expect(compareKeyResult2).toEqual([
        {e: 'id_2', f: 'sample_2', g: 'type_2', h: 'type_1'},
      ]);
    });

    it('by comparing callback return value', () => {
      const stringValueCallback = () => '2';
      const nullValueCallback = () => null;
      const numericValueCallback = () => 1;
      const nestedStringValueCallback = () => 'type_1';

      const arr = [
        undefined,
        undefined,
        1,
        {a: 1, b: '2', c: undefined, d: null},
        [
          {e: 'id_1', f: 'sample_1', g: 'type_1'},
          {e: 'id_2', f: 'sample_2', g: 'type_2', h: 'type_1'},
          {e: 'id_3', f: 'sample_3', g: 'type_3'},
          {e: 'id_4', f: 'sample_4', g: 'type_1'},
        ],
      ];

      const stringValue = readProperties(arr, stringValueCallback);
      const nullValue = readProperties(arr, nullValueCallback);
      const numericValue = readProperties(arr, numericValueCallback);
      const nestedStringValue = readProperties(arr, nestedStringValueCallback);
      const compareKeyResult1 = readProperties(
        arr,
        nestedStringValueCallback,
        'g',
      );
      const compareKeyResult2 = readProperties(
        arr,
        nestedStringValueCallback,
        'h',
      );

      expect(stringValue).toEqual([
        {
          key: 'b',
          reference: arr[3],
        },
      ]);
      expect(nullValue).toEqual([
        {
          key: 'd',
          reference: arr[3],
        },
      ]);
      expect(numericValue).toEqual([
        {
          key: '2',
          reference: arr,
        },
        {
          key: 'a',
          reference: arr[3],
        },
      ]);
      expect(nestedStringValue).toEqual([
        {
          key: 'g',
          reference: {e: 'id_1', f: 'sample_1', g: 'type_1'},
        },
        {
          key: 'h',
          reference: {e: 'id_2', f: 'sample_2', g: 'type_2', h: 'type_1'},
        },
        {
          key: 'g',
          reference: {e: 'id_4', f: 'sample_4', g: 'type_1'},
        },
      ]);
      expect(compareKeyResult1).toEqual([
        {e: 'id_1', f: 'sample_1', g: 'type_1'},
        {e: 'id_4', f: 'sample_4', g: 'type_1'},
      ]);
      expect(compareKeyResult2).toEqual([
        {e: 'id_2', f: 'sample_2', g: 'type_2', h: 'type_1'},
      ]);
    });

    it('sad - null / undefined', () => {
      const actualResult1 = readProperties(null);
      const actualResult2 = readProperties(undefined);
      const expectedResult1 = [];
      const expectedResult2 = [];

      expect(actualResult1).toEqual(expectedResult1);
      expect(actualResult2).toEqual(expectedResult2);
    });
  });

  describe('replaceProperties', () => {
    describe('array', () => {
      it('by string value', () => {
        const value1 = 'Edited Undefined';
        const value2 = 'Edited Nil';
        const value3 = 'Edited Number';
        const arr = [
          undefined,
          undefined,
          1,
          {a: 1, b: '2', c: undefined, d: null},
        ];

        const actualResult = replaceProperties(arr, isUndefined, value1);
        const expectedResult = [
          value1,
          value1,
          1,
          {a: 1, b: '2', c: value1, d: null},
        ];
        const actualResult2 = replaceProperties(arr, isNil, value2);
        const expectedResult2 = [
          value2,
          value2,
          1,
          {a: 1, b: '2', c: value2, d: value2},
        ];
        const actualResult3 = replaceProperties(
          arr,
          (item) => isNumber(item),
          value3,
        );
        const expectedResult3 = [
          undefined,
          undefined,
          value3,
          {a: value3, b: '2', c: undefined, d: null},
        ];

        expect(actualResult).toEqual(expectedResult);
        expect(actualResult2).toEqual(expectedResult2);
        expect(actualResult3).toEqual(expectedResult3);
      });

      describe('nested properties - with value callback', () => {
        it('with condition callback', () => {
          const arr = [
            {
              a: 1,
              b: '2',
              c: undefined,
              d: null,
              e: {a: 1, b: '2', c: undefined, d: null},
            },
          ];

          const valueCallback = (val) => `Edited ${val}`;

          const actualResult = replaceProperties(
            arr,
            isUndefined,
            valueCallback,
          );
          const expectedResult = [
            {
              a: 1,
              b: '2',
              c: 'Edited undefined',
              d: null,
              e: {a: 1, b: '2', c: 'Edited undefined', d: null},
            },
          ];
          const actualResult2 = replaceProperties(arr, isNil, valueCallback);
          const expectedResult2 = [
            {
              a: 1,
              b: '2',
              c: 'Edited undefined',
              d: 'Edited null',
              e: {
                a: 1,
                b: '2',
                c: 'Edited undefined',
                d: 'Edited null',
              },
            },
          ];
          const actualResult3 = replaceProperties(
            arr,
            (item) => isNumber(item),
            valueCallback,
          );
          const expectedResult3 = [
            {
              a: 'Edited 1',
              b: '2',
              c: undefined,
              d: null,
              e: {a: 'Edited 1', b: '2', c: undefined, d: null},
            },
          ];

          expect(actualResult).toEqual(expectedResult);
          expect(actualResult2).toEqual(expectedResult2);
          expect(actualResult3).toEqual(expectedResult3);
        });

        it('without condition callback - should process all properties', () => {
          const arr = [
            {
              a: 1,
              b: '2',
              c: undefined,
              d: null,
              e: {a: 1, b: '2', c: undefined, d: null},
            },
          ];

          const valueCallback = (val) => `Edited ${val}`;

          const actualResult = replaceProperties(arr, null, valueCallback);
          const expectedResult = [
            {
              a: 'Edited 1',
              b: 'Edited 2',
              c: 'Edited undefined',
              d: 'Edited null',
              e: {
                a: 'Edited 1',
                b: 'Edited 2',
                c: 'Edited undefined',
                d: 'Edited null',
              },
            },
          ];

          expect(actualResult).toEqual(expectedResult);
        });
      });
    });

    describe('object', () => {
      it('by string value', () => {
        const value1 = 'Edited Undefined';
        const value2 = 'Edited Nil';
        const value3 = 'Edited Number';
        const obj = {a: 1, b: '2', c: undefined, d: null};

        const actualResult = replaceProperties(obj, isUndefined, value1);
        const expectedResult = {a: 1, b: '2', c: value1, d: null};
        const actualResult2 = replaceProperties(obj, isNil, value2);
        const expectedResult2 = {a: 1, b: '2', c: value2, d: value2};
        const actualResult3 = replaceProperties(
          obj,
          (item) => isNumber(item),
          value3,
        );
        const expectedResult3 = {a: value3, b: '2', c: undefined, d: null};

        expect(actualResult).toEqual(expectedResult);
        expect(actualResult2).toEqual(expectedResult2);
        expect(actualResult3).toEqual(expectedResult3);
      });

      describe('nested properties - with value callback', () => {
        it('with condition callback', () => {
          const obj = {
            a: 1,
            b: '2',
            c: undefined,
            d: null,
            e: {a: 1, b: '2', c: undefined, d: null},
          };

          const valueCallback = (val) => `Edited ${val}`;

          const actualResult = replaceProperties(
            obj,
            isUndefined,
            valueCallback,
          );
          const expectedResult = {
            a: 1,
            b: '2',
            c: 'Edited undefined',
            d: null,
            e: {a: 1, b: '2', c: 'Edited undefined', d: null},
          };
          const actualResult2 = replaceProperties(obj, isNil, valueCallback);
          const expectedResult2 = {
            a: 1,
            b: '2',
            c: 'Edited undefined',
            d: 'Edited null',
            e: {
              a: 1,
              b: '2',
              c: 'Edited undefined',
              d: 'Edited null',
            },
          };
          const actualResult3 = replaceProperties(
            obj,
            (item) => isNumber(item),
            valueCallback,
          );
          const expectedResult3 = {
            a: 'Edited 1',
            b: '2',
            c: undefined,
            d: null,
            e: {a: 'Edited 1', b: '2', c: undefined, d: null},
          };

          expect(actualResult).toEqual(expectedResult);
          expect(actualResult2).toEqual(expectedResult2);
          expect(actualResult3).toEqual(expectedResult3);
        });

        it('without condition callback - should process all properties', () => {
          const obj = {
            a: 1,
            b: '2',
            c: undefined,
            d: null,
            e: {a: 1, b: '2', c: undefined, d: null},
          };

          const valueCallback = (val) => `Edited ${val}`;

          const actualResult = replaceProperties(obj, null, valueCallback);
          const expectedResult = {
            a: 'Edited 1',
            b: 'Edited 2',
            c: 'Edited undefined',
            d: 'Edited null',
            e: {
              a: 'Edited 1',
              b: 'Edited 2',
              c: 'Edited undefined',
              d: 'Edited null',
            },
          };

          expect(actualResult).toEqual(expectedResult);
        });
      });
    });

    it('sad - null / undefined', () => {
      const actualResult1 = replaceProperties(null);
      const actualResult2 = replaceProperties(undefined);
      const expectedResult1 = {};
      const expectedResult2 = {};

      expect(actualResult1).toEqual(expectedResult1);
      expect(actualResult2).toEqual(expectedResult2);
    });
  });

  describe('removeProperties - remove object properties by callback', () => {
    it('happy - array', () => {
      const arr = [
        undefined,
        undefined,
        1,
        {a: 1, b: '2', c: undefined, d: null},
      ];

      const actualResult = removeProperties(arr, isUndefined);
      const expectedResult = [1, {a: 1, b: '2', d: null}];
      const actualResult2 = removeProperties(arr, isNil);
      const expectedResult2 = [1, {a: 1, b: '2'}];
      const actualResult3 = removeProperties(arr, (item) => isNumber(item));
      const expectedResult3 = [
        undefined,
        undefined,
        {b: '2', c: undefined, d: null},
      ];

      expect(actualResult).toEqual(expectedResult);
      expect(actualResult2).toEqual(expectedResult2);
      expect(actualResult3).toEqual(expectedResult3);
    });

    it('happy - obj', () => {
      const obj = {a: 1, b: '2', c: null, d: undefined};

      const actualResult = removeProperties(obj, isUndefined);
      const expectedResult = {a: 1, b: '2', c: null};
      const actualResult2 = removeProperties(obj, isNil);
      const expectedResult2 = {a: 1, b: '2'};

      expect(actualResult).toEqual(expectedResult);
      expect(actualResult2).toEqual(expectedResult2);
    });

    it('sad - null / undefined', () => {
      const actualResult1 = removeProperties(null);
      const actualResult2 = removeProperties(undefined);
      const expectedResult1 = {};
      const expectedResult2 = {};

      expect(actualResult1).toEqual(expectedResult1);
      expect(actualResult2).toEqual(expectedResult2);
    });
  });

  describe('removeNilValues - remove object properties that are null/undefined', () => {
    it('happy - obj', () => {
      const obj = {a: 1, b: '2', c: null};

      const actualResult = removeNilValues(obj);
      const expectedResult = {a: 1, b: '2'};

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - array of objects', () => {
      const obj = [
        {a: 1, b: '2', c: null},
        {a: 0, b: null, c: ''},
      ];

      const actualResult = removeNilValues(obj);
      const expectedResult = [
        {a: 1, b: '2'},
        {a: 0, c: ''},
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - nested 1', () => {
      const obj = {d: {a: 0, b: null, c: ''}};

      const actualResult = removeNilValues(obj);
      const expectedResult = {d: {a: 0, c: ''}};

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - nested 2', () => {
      const obj = {d: [{a: 0, b: null, c: ''}]};

      const actualResult = removeNilValues(obj);
      const expectedResult = {d: [{a: 0, c: ''}]};

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - nested 3', () => {
      const obj = [{a: 1, b: '2', c: null}, {d: [{a: 0, b: null, c: ''}]}];

      const actualResult = removeNilValues(obj);
      const expectedResult = [{a: 1, b: '2'}, {d: [{a: 0, c: ''}]}];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - nested 4', () => {
      const obj = [
        {fontSize: 14.395733333333332},
        [{color: '#FFFFFF'}, undefined],
      ];

      const actualResult = removeNilValues(obj);
      const expectedResult = [
        {fontSize: 14.395733333333332},
        [{color: '#FFFFFF'}],
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - with string array', () => {
      const obj = {
        connectedContactId: '554840',
        relationships: ['Macanta User'],
        test: [{a: [null, 'Test', undefined]}],
      };

      const actualResult = removeNilValues(obj);
      const expectedResult = {
        connectedContactId: '554840',
        relationships: ['Macanta User'],
        test: [{a: ['Test']}],
      };

      expect(actualResult).toEqual(expectedResult);
    });
  });

  describe('removeUndefinedValues - remove object properties that are undefined', () => {
    it('happy - array', () => {
      const arr = [
        undefined,
        undefined,
        1,
        {a: 1, b: '2', c: undefined, d: null},
      ];

      const actualResult = removeUndefinedValues(arr);
      const expectedResult = [1, {a: 1, b: '2', d: null}];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - obj', () => {
      const obj = {a: 1, b: '2', c: undefined, d: null};

      const actualResult = removeUndefinedValues(obj);
      const expectedResult = {a: 1, b: '2', d: null};

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - with string array', () => {
      const obj = {
        connectedContactId: '554840',
        relationships: ['Macanta User'],
        test: [{a: [undefined, 'Test']}],
      };

      const actualResult = removeUndefinedValues(obj);
      const expectedResult = {
        connectedContactId: '554840',
        relationships: ['Macanta User'],
        test: [{a: ['Test']}],
      };

      expect(actualResult).toEqual(expectedResult);
    });
  });

  describe('removeEmptyValues - remove object properties that are falsy but not numeric', () => {
    it('happy - array', () => {
      const arr = [
        undefined,
        undefined,
        0,
        1,
        '',
        {a: 1, b: '2', c: undefined, d: null, e: 0, f: ''},
      ];

      const actualResult = removeEmptyValues(arr);
      const expectedResult = [0, 1, {a: 1, b: '2', e: 0}];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - obj', () => {
      const obj = {a: 1, b: '2', c: undefined, d: null, e: 0, f: ''};

      const actualResult = removeEmptyValues(obj);
      const expectedResult = {a: 1, b: '2', e: 0};

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - with string array', () => {
      const obj = {
        connectedContactId: '554840',
        relationships: ['', 'Macanta User', ''],
        test: [{a: [undefined, 'Test']}],
      };

      const actualResult = removeEmptyValues(obj);
      const expectedResult = {
        connectedContactId: '554840',
        relationships: ['Macanta User'],
        test: [{a: ['Test']}],
      };

      expect(actualResult).toEqual(expectedResult);
    });
  });

  describe('removeFunctions', () => {
    it('happy - array', () => {
      const arr = [
        undefined,
        () => 'test',
        0,
        '',
        {a: 1, b: '2', c: undefined, d: null, e: () => 'test'},
      ];

      const actualResult = removeFunctions(arr);
      const expectedResult = [
        undefined,
        0,
        '',
        {a: 1, b: '2', c: undefined, d: null},
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - obj', () => {
      const obj = {a: 1, b: '2', c: undefined, d: null, e: () => 'test'};

      const actualResult = removeFunctions(obj);
      const expectedResult = {a: 1, b: '2', c: undefined, d: null};

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - with string array', () => {
      const obj = {
        connectedContactId: '554840',
        relationships: [() => 'test', 'Macanta User', ''],
        test: [{a: [undefined, 'Test']}],
      };

      const actualResult = removeFunctions(obj);
      const expectedResult = {
        connectedContactId: '554840',
        relationships: ['Macanta User', ''],
        test: [{a: [undefined, 'Test']}],
      };

      expect(actualResult).toEqual(expectedResult);
    });
  });

  describe('replaceKeys', () => {
    it('transform keys', () => {
      const arr = [
        {
          a: 1,
          b: '2',
          c: undefined,
          d: null,
          e: {a: 1, b: '2', c: undefined, d: null},
        },
      ];

      const valueCallback = (val) => `Edited ${val}`;

      const keyMapping = {
        a: 'apple',
        b: 'bapple',
        c: 'capple',
        d: 'dapple',
        e: 'epple',
      };

      const actualResult = replaceKeys(arr, keyMapping);
      const expectedResult = [
        {
          apple: 1,
          bapple: '2',
          capple: undefined,
          dapple: null,
          epple: {
            apple: 1,
            bapple: '2',
            capple: undefined,
            dapple: null,
          },
        },
      ];

      const keyMappingWithMissingProperty = {
        a: 'apple',
        d: 'dapple',
        e: 'epple',
      };

      const actualResult2 = replaceKeys(arr, keyMappingWithMissingProperty);
      const expectedResult2 = [
        {
          apple: 1,
          b: '2',
          c: undefined,
          dapple: null,
          epple: {
            apple: 1,
            b: '2',
            c: undefined,
            dapple: null,
          },
        },
      ];

      const actualResult3 = replaceKeys(arr, keyMapping, valueCallback);
      const expectedResult3 = [
        {
          apple: 'Edited 1',
          bapple: 'Edited 2',
          capple: 'Edited undefined',
          dapple: 'Edited null',
          epple: {
            apple: 'Edited 1',
            bapple: 'Edited 2',
            capple: 'Edited undefined',
            dapple: 'Edited null',
          },
        },
      ];

      expect(actualResult).toEqual(expectedResult);
      expect(actualResult2).toEqual(expectedResult2);
      expect(actualResult3).toEqual(expectedResult3);
    });

    it('sad - null / undefined', () => {
      const actualResult1 = replaceKeys(null);
      const actualResult2 = replaceKeys(undefined);
      const expectedResult1 = {};
      const expectedResult2 = {};

      expect(actualResult1).toEqual(expectedResult1);
      expect(actualResult2).toEqual(expectedResult2);
    });
  });
});
