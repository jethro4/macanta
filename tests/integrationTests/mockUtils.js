import flatten from 'lodash/flatten';
import doTypeFactory from '@tests/jestSettings/mockFactory/doTypeFactory';
import doFieldFactory from '@tests/jestSettings/mockFactory/doFieldFactory';
import doItemFactory from '@tests/jestSettings/mockFactory/doItemFactory';
import relationshipFactory from '@tests/jestSettings/mockFactory/relationshipFactory';
import userFactory from '@tests/jestSettings/mockFactory/userFactory';
import {overrideHandlers} from '@tests/jestSettings/utils/renderUtils';
import {graphql} from 'msw';
import doRelationshipFactory from '@tests/jestSettings/mockFactory/doRelationshipFactory';
import appSettingsFactory from '@tests/jestSettings/mockFactory/appSettingsFactory';
import accessPermissionsFactory from '@tests/jestSettings/mockFactory/accessPermissionsFactory';
import {mockData} from '@tests/jestSettings/mockFactory/factory';

export const handleMockAppSettings = () => {
  const mockedResult = appSettingsFactory();

  overrideHandlers(
    graphql.query('getAppSettings', (req, res, ctx) => {
      return res(
        ctx.data({
          getAppSettings: mockedResult,
        }),
      );
    }),
  );

  return mockedResult;
};

export const handleMockAccessPermissions = () => {
  const mockedResult = accessPermissionsFactory();

  overrideHandlers(
    graphql.query('getAccessPermissions', (req, res, ctx) => {
      return res(
        ctx.data({
          getAccessPermissions: mockedResult,
        }),
      );
    }),
  );

  return mockedResult;
};

export const handleMockUsers = () => {
  const mockedResult = [userFactory(), userFactory()];

  overrideHandlers(
    graphql.query('listUsers', (req, res, ctx) => {
      return res(
        ctx.data({
          listUsers: {
            items: mockedResult,
            count: mockedResult.length,
            total: mockedResult.length,
          },
        }),
      );
    }),
  );

  return mockedResult;
};

export const handleMockDOTypes = (data) => {
  const mockedResult =
    data ||
    mockData([
      doTypeFactory({
        id: 'DataObject1',
      }),
      doTypeFactory({
        id: 'DataObject2',
      }),
      doTypeFactory({
        id: 'DataObject3',
      }),
      doTypeFactory({
        id: 'DataObject4',
      }),
    ]);

  overrideHandlers(
    graphql.query('listDataObjectTypes', (req, res, ctx) => {
      return res(
        ctx.data({
          listDataObjectTypes: mockedResult,
        }),
      );
    }),
  );

  return mockedResult;
};

export const handleMockDOFields = (data) => {
  const defaultMockedFields = [
    doFieldFactory({
      type: 'Text',
      name: 'Text Field',
      showInTable: true,
    }),
    doFieldFactory({
      type: 'TextArea',
      name: 'TextArea Field',
      showInTable: true,
    }),
    doFieldFactory({
      type: 'Select',
      name: 'Select Field',
      showInTable: true,
    }),
    doFieldFactory({
      type: 'Radio',
      name: 'Radio Field',
      showInTable: false,
    }),
    doFieldFactory({
      type: 'Checkbox',
      name: 'Checkbox Field',
      sectionTag: 'Multi Value',
      showInTable: false,
    }),
  ];
  const mockedResult = data || {
    DataObject1: [...defaultMockedFields],
    DataObject2: [
      defaultMockedFields[0],
      {...defaultMockedFields[1], showInTable: false},
      defaultMockedFields[2],
    ],
    DataObject3: [
      defaultMockedFields[0],
      defaultMockedFields[1],
      {...defaultMockedFields[2], showInTable: false},
    ],
    DataObject4: [defaultMockedFields[0]],
  };

  overrideHandlers(
    graphql.query('listDataObjectFields', (req, res, ctx) => {
      return res(
        ctx.data({
          listDataObjectFields: mockedResult[req.variables.groupId],
        }),
      );
    }),
  );

  return mockedResult;
};

export const handleMockRelationships = (data) => {
  const mockedResult = data || {
    noGroupId: mockData([
      relationshipFactory(),
      relationshipFactory(),
      relationshipFactory(),
      relationshipFactory(),
    ]),
    DataObject1: mockData([
      doRelationshipFactory({
        id: 'Relationship1',
      }),
      doRelationshipFactory({
        id: 'Relationship2',
      }),
    ]),
    DataObject2: mockData([
      doRelationshipFactory({
        id: 'Relationship1',
      }),
      doRelationshipFactory({
        id: 'Relationship2',
      }),
    ]),
    DataObject3: mockData([
      doRelationshipFactory({
        id: 'Relationship3',
        role: 'Relationship 3',
      }),
    ]),
    DataObject4: mockData([
      doRelationshipFactory({
        id: 'Relationship4',
        role: 'Relationship 4',
      }),
    ]),
  };

  overrideHandlers(
    graphql.query('listRelationships', (req, res, ctx) => {
      return res(
        ctx.data({
          listRelationships: mockedResult[req.variables.groupId || 'noGroupId'],
        }),
      );
    }),
  );
};

export const handleMockDOSearchItems = (data) => {
  const mockedResult = data || {
    DataObject1: [
      doItemFactory({
        id: 'ItemDataObject1',
        isSearch: true,
      }),
      doItemFactory({
        id: 'ItemDataObject4',
        isSearch: true,
      }),
    ],
    DataObject2: [
      doItemFactory({
        id: 'ItemDataObject2',
        isSearch: true,
      }),
      doItemFactory({
        id: 'ItemDataObject3',
        isSearch: true,
      }),
      doItemFactory({
        id: 'ItemDataObject5',
        isSearch: true,
      }),
    ],
  };

  const doTypes = handleMockDOTypes();

  const getMockedGroups = (doTypeId) => {
    if (doTypeId) {
      return [
        {
          groupId: doTypeId,
          type: doTypes.find((d) => d.id === doTypeId),
          items: mockedResult[doTypeId],
          __typename: 'DataObjectSearch',
        },
      ];
    }

    return doTypes.map((doType) => ({
      groupId: doType.id,
      type: doType.title,
      items: mockedResult[doType.id],
      __typename: 'DataObjectSearch',
    }));
  };

  overrideHandlers(
    graphql.query('listDataObjects', (req, res, ctx) => {
      const mockedItems = getMockedGroups(req.variables.groupId);

      return res(
        ctx.data({
          listDataObjects: {
            items: mockedItems,
            total: flatten(mockedItems.map(({items}) => items)).length,
            __typename: 'DataObjectSearch',
          },
        }),
      );
    }),
  );

  return mockedResult;
};
