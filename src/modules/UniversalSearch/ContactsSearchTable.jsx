import React, {useState, useLayoutEffect, useMemo, useEffect} from 'react';
import isEmpty from 'lodash/isEmpty';
import ContactsQuery from '@macanta/modules/hoc/ContactsQuery';
import Box from '@mui/material/Box';
import Button from '@macanta/components/Button';
import Pagination from '@macanta/components/Pagination';
import AddIcon from '@mui/icons-material/Add';
import DataTable from '@macanta/containers/DataTable';
import Search from '@macanta/containers/Search';
import MarkDeduplicateMergeBtn from '@macanta/modules/ContactForms/MarkDeduplicateMergeBtn';
import DeduplicateMergeBtn from '@macanta/modules/ContactForms/DeduplicateMergeBtn';
import ContactFormsIconBtn from '@macanta/modules/ContactForms/ContactFormsIconBtn';
import useContactFields from '@macanta/modules/ContactDetails/useContactFields';
import {filterFieldsForTable} from '@macanta/selectors/dataObject.selector';
import Checkbox from '@mui/material/Checkbox';
import Radio from '@mui/material/Radio';

const DEFAULT_SORTABLE_COLUMNS = ['FullName'];

const ContactsSearchTableContainer = ({
  searchQuery: searchQueryProp,
  hideSearchAndAdd,
  SearchProps,
  ...props
}) => {
  const [searchQuery, setSearchQuery] = useState(searchQueryProp);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(25);
  const [sort, setSort] = useState({});
  const [markContactsMerge, setMarkContactsMerge] = useState(false);

  const handleSearch = (newVal) => {
    setSearchQuery({search: newVal});
    setPage(0);
  };

  const handleChangePage = (event, value) => {
    setPage(value);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value));
    setPage(0);
  };

  const handleSort = (property, sortDirection) => {
    setSort({
      order: sortDirection,
      orderBy: property,
    });
  };

  const handleTriggerDeduplicate = (isTriggered) => {
    setMarkContactsMerge(isTriggered);
  };

  useLayoutEffect(() => {
    setSearchQuery(searchQueryProp);
  }, [searchQueryProp]);

  return (
    <>
      {!hideSearchAndAdd && (
        <Box
          style={{
            padding: '1rem',
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            height: '3.75rem',
          }}>
          <Search
            autoFocus
            style={{
              width: 300,
            }}
            defaultValue={searchQuery?.search}
            autoSearch
            // defaultSearchQuery={{search: '', page: 0, limit: 10}}
            // value={value}
            onSearch={handleSearch}
            // onSelectItem={(contact) => setFieldValue('contact', contact)}
            disableCategories
            disableSuggestions
            {...SearchProps}
          />

          <Box>
            <MarkDeduplicateMergeBtn
              style={{
                marginRight: '1rem',
              }}
              onTrigger={handleTriggerDeduplicate}
            />
            <ContactFormsIconBtn
              onSaveContact={(contact) => {
                handleSearch(contact.id);
              }}
              renderButton={(handleAddContact) => {
                return (
                  <Button
                    onClick={handleAddContact}
                    size="small"
                    variant="contained"
                    startIcon={<AddIcon />}>
                    Add Contact
                  </Button>
                );
              }}
            />
          </Box>
        </Box>
      )}
      <ContactsQuery
        {...searchQuery}
        page={page}
        limit={rowsPerPage}
        order={sort?.order}
        orderBy={sort?.orderBy}>
        {({data = {}, loading}) => {
          return (
            <>
              <ContactsSearchTable
                rowsPerPage={rowsPerPage}
                contacts={data?.listContacts?.items}
                loading={loading}
                onSort={handleSort}
                order={sort?.order}
                orderBy={sort?.orderBy}
                markContactsMerge={markContactsMerge}
                {...props}
              />
              <Box
                style={{
                  borderTop: '1px solid #eee',
                  boxShadow: '0px -4px 10px -2px #eee',
                  zIndex: 1,
                }}>
                <Pagination
                  table
                  count={data?.listContacts?.total}
                  page={page}
                  onPageChange={handleChangePage}
                  rowsPerPage={rowsPerPage}
                  rowsPerPageOptions={[25, 50, 100]}
                  onRowsPerPageChange={handleChangeRowsPerPage}
                />
              </Box>
            </>
          );
        }}
      </ContactsQuery>
    </>
  );
};

const ContactsSearchTable = ({
  onSelectItem,
  columns: columnsProp,
  onSort,
  order,
  orderBy,
  contacts,
  loading,
  markContactsMerge,
  ...props
}) => {
  const [tableData, setTableData] = useState([]);
  const [markedContacts, setMarkedContacts] = useState({});

  const contactDefaultFieldsQuery = useContactFields(
    null,
    'contactDefaultFields',
    {
      removeEmail: true,
    },
  );
  const contactCustomFieldsQuery = useContactFields(
    null,
    'contactCustomFields',
    {
      removeEmail: true,
    },
  );

  const contactDefaultFields =
    contactDefaultFieldsQuery.data?.listDataObjectFields;
  const customFields = contactCustomFieldsQuery.data?.listDataObjectFields;
  const sortableColumns = [
    ...DEFAULT_SORTABLE_COLUMNS,
    ...(contactDefaultFields?.map((field) => field.name) || []),
  ];

  const handleMarkContact = (contact) => {
    let updatedMarkedContacts = {...markedContacts};
    const isMarked = !!updatedMarkedContacts[contact.id];

    if (isMarked) {
      delete updatedMarkedContacts[contact.id];
    } else {
      updatedMarkedContacts = {...updatedMarkedContacts, [contact.id]: true};
    }

    setMarkedContacts(updatedMarkedContacts);
  };

  const columns = useMemo(() => {
    if (columnsProp) {
      return columnsProp;
    }

    if (contactDefaultFields && customFields) {
      const columnsWithId = filterFieldsForTable({
        fields: [...contactDefaultFields, ...customFields],
        hideContacts: true,
      });

      return columnsWithId;
    }

    return [];
  }, [contactDefaultFields, customFields]);

  useLayoutEffect(() => {
    if (columns.length && contacts) {
      const formattedContacts = contacts.map((item) => ({
        FullName: `${item.firstName || ''} ${item.lastName || ''}`.trim(),
        FirstName: item.firstName,
        LastName: item.lastName,
        Email: item.email,
        ...item,
        ...item.customFields?.reduce((acc, field) => {
          const value = field.value || field.default;

          if (value) {
            acc[field.name] = value;
          }

          return acc;
        }, {}),
      }));

      setTableData(formattedContacts);
    }
  }, [columns, contacts]);

  useEffect(() => {
    if (!markContactsMerge && !isEmpty(markedContacts)) {
      setMarkedContacts({});
    }
  }, [markContactsMerge]);

  const markedContactItems = useMemo(() => {
    if (tableData) {
      return tableData.filter((contact) => !!markedContacts[contact.id]);
    }

    return [];
  }, [markedContacts, tableData]);

  return (
    <>
      <DataTable
        fullHeight
        order={order}
        orderBy={orderBy}
        loading={loading}
        hideEmptyMessage={loading}
        hidePagination
        columns={columns}
        selectable
        data={tableData}
        onSelectItem={!markContactsMerge ? onSelectItem : handleMarkContact}
        onSort={onSort}
        sortableColumns={sortableColumns}
        {...(markContactsMerge && {
          multiSelect: true,
          actionColumn: {
            label: 'Duplicate?',
            show: true,
          },
          renderActionButtons: ({item}) => (
            <Checkbox color="primary" checked={!!markedContacts[item.id]} />
          ),
        })}
        {...props}
      />
      {!isEmpty(markedContacts) && (
        <Box
          style={{
            position: 'absolute',
            bottom: '10px',
            right: '1rem',
            zIndex: 2,
          }}>
          <DeduplicateMergeBtn
            contacts={markedContactItems}
            renderTable={({onSelectPrimary, selectedPrimaryContact}) => {
              return (
                <DataTable
                  noOddRowStripes
                  fullHeight
                  order={order}
                  orderBy={orderBy}
                  hidePagination
                  columns={columns}
                  selectable
                  data={markedContactItems}
                  onSelectItem={onSelectPrimary}
                  sortableColumns={sortableColumns}
                  actionColumn={{
                    label: 'Primary?',
                    show: true,
                  }}
                  renderActionButtons={({item}) => (
                    <Radio
                      color="light"
                      checked={item.id === selectedPrimaryContact.id}
                    />
                  )}
                  highlight
                />
              );
            }}
          />
        </Box>
      )}
    </>
  );
};

export default ContactsSearchTableContainer;
