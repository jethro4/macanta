import {experimentalStyled as styled} from '@mui/material/styles';

import IconButton from '@mui/material/IconButton';
import InputBase from '@mui/material/InputBase';
import Box from '@mui/material/Box';
import MUIAppBar from '@mui/material/AppBar';

export const Root = styled(Box)`
  width: 100%;
`;

export const FlexGrow = styled(Box)`
  flex: 1;
`;

export const SearchIconContainer = styled(Box)`
  width: ${({theme}) => theme.spacing(9)};
  height: 100%;
  position: absolute;
  pointer-events: none;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const SearchInput = styled(InputBase)`
  padding-top: 0.25rem;
  padding-right: 0.25rem;
  padding-bottom: 0.25rem;
  padding-left: ${({theme}) => theme.spacing(9)};
  transition: ${({theme}) => theme.transitions.create('width')};
  width: 100%;
`;

export const SectionDesktop = styled(Box)`
  display: none;
  ${({theme}) => theme.breakpoints.up('md')} {
    display: flex;
  }
`;

export const SectionMobile = styled(Box)`
  display: flex;
  ${({theme}) => theme.breakpoints.up('md')} {
    display: none;
  }
`;

export const AppBar = styled(MUIAppBar)``;

export const MenuButton = styled(IconButton)`
  margin-right: ${({theme}) => theme.spacing(2)};
  ${({theme}) => theme.breakpoints.up('sm')} {
    margin-right: ${({theme}) => theme.spacing(4)};
  }
`;
