import React, {useState} from 'react';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import Tooltip from '@macanta/components/Tooltip';
import Popover from '@macanta/components/Popover';
import EmailDOItemOption from './EmailDOItemOption';
import NotesDOItemOption from './NotesDOItemOption';
import RelationshipsDOItemOption from './RelationshipsDOItemOption';
import AttachmentsDOItemOption from './AttachmentsDOItemOption';
import HistoryDOItemOption from './HistoryDOItemOption';
import DeleteDOItemOption from './DeleteDOItemOption';
import EmailIcon from '@mui/icons-material/Email';
import EventNoteIcon from '@mui/icons-material/EventNote';
import GroupsIcon from '@mui/icons-material/Groups';
import AccountTreeIcon from '@mui/icons-material/AccountTree';
import AttachmentIcon from '@mui/icons-material/Attachment';
import HistoryIcon from '@mui/icons-material/History';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import PermissionMetaQuery from '@macanta/modules/hoc/PermissionMetaQuery';
import useDOItemMutation from '@macanta/modules/DataObjectItems/DOItemForms/useDOItemMutation';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import * as Styled from './styles';
import DOToDORelationshipsOption from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/DOToDORelationshipsOption';

const OptionView = ({
  selectedType,
  itemId,
  groupId,
  fields,
  relationships,
  connectedContacts,
  refetch,
  onClose,
  loading,
  doTitle,
}) => {
  const {saving, save} = useDOItemMutation(itemId, groupId, null, {
    onCompleted: () => {
      refetch && refetch();
    },
  });

  const handleSave = async (values) => {
    await save(values);
  };

  let optionView = null;

  switch (selectedType) {
    case 'email': {
      optionView = (
        <EmailDOItemOption
          modal
          showModal
          itemId={itemId}
          onClose={onClose}
          connectedContacts={connectedContacts}
          doTitle={doTitle}
        />
      );
      break;
    }
    case 'notes': {
      optionView = (
        <NotesDOItemOption
          modal
          showModal
          itemId={itemId}
          onClose={onClose}
          onSave={handleSave}
        />
      );
      break;
    }
    case 'contactRelationships': {
      optionView = (
        <RelationshipsDOItemOption
          modal
          showModal
          itemId={itemId}
          relationships={relationships}
          onClose={onClose}
          onSave={handleSave}
          loading={loading}
        />
      );
      break;
    }
    case 'doToDORelationships': {
      optionView = (
        <DOToDORelationshipsOption
          modal
          showModal
          doItemId={itemId}
          groupId={groupId}
          onClose={onClose}
          onSave={handleSave}
        />
      );
      break;
    }
    case 'attachments': {
      optionView = (
        <AttachmentsDOItemOption
          modal
          showModal
          itemId={itemId}
          fields={fields}
          relationships={relationships}
          onClose={onClose}
          onSave={handleSave}
        />
      );
      break;
    }
    case 'history': {
      optionView = (
        <HistoryDOItemOption
          modal
          showModal
          itemId={itemId}
          onClose={onClose}
        />
      );
      break;
    }
    case 'delete': {
      optionView = (
        <DeleteDOItemOption showDialog itemId={itemId} onClose={onClose} />
      );
      break;
    }
  }

  return (
    <>
      {optionView}
      <LoadingIndicator modal loading={saving} />
    </>
  );
};

const MoreOptions = ({
  itemId,
  groupId,
  fields,
  relationships,
  connectedContacts,
  refetch,
  loading,
  PopoverProps,
  doTitle,
  ...props
}) => {
  const [anchorEl, setAnchorEl] = useState(false);
  const [selectedType, setSelectedType] = useState('');

  const openMoreOptions = Boolean(anchorEl);

  const handleMoreOptions = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleCloseMoreOptions = () => {
    setAnchorEl(null);
  };

  const handleSelectType = (type) => () => {
    setSelectedType(type);
    handleCloseMoreOptions();
  };

  const handleClose = () => {
    setSelectedType('');
  };

  return (
    <>
      <Tooltip title="More">
        <Styled.ActionButton
          show={openMoreOptions}
          onClick={handleMoreOptions}
          {...props}>
          <MoreVertIcon />
        </Styled.ActionButton>
      </Tooltip>
      <Popover
        open={openMoreOptions}
        anchorEl={anchorEl}
        onClose={handleCloseMoreOptions}
        anchorOrigin={{
          vertical: 'center',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'center',
          horizontal: 'right',
        }}
        {...PopoverProps}>
        <List
          style={{
            padding: 0,
            minWidth: 220,
          }}>
          <NoteTaskHistoryStyled.SelectionItem
            button
            onClick={handleSelectType('email')}>
            <EmailIcon />
            <NoteTaskHistoryStyled.SelectionItemText primary="Email" />
          </NoteTaskHistoryStyled.SelectionItem>
          <Divider />
          <NoteTaskHistoryStyled.SelectionItem
            button
            onClick={handleSelectType('notes')}>
            <EventNoteIcon />
            <NoteTaskHistoryStyled.SelectionItemText primary="Notes" />
          </NoteTaskHistoryStyled.SelectionItem>
          <Divider />
          <NoteTaskHistoryStyled.SelectionItem
            button
            onClick={handleSelectType('contactRelationships')}>
            <GroupsIcon />
            <NoteTaskHistoryStyled.SelectionItemText primary="Contact Relationships" />
          </NoteTaskHistoryStyled.SelectionItem>
          <Divider />
          <NoteTaskHistoryStyled.SelectionItem
            button
            onClick={handleSelectType('doToDORelationships')}>
            <AccountTreeIcon />
            <NoteTaskHistoryStyled.SelectionItemText primary="DO Direct Relationships" />
          </NoteTaskHistoryStyled.SelectionItem>
          <Divider />
          <NoteTaskHistoryStyled.SelectionItem
            button
            onClick={handleSelectType('attachments')}>
            <AttachmentIcon />
            <NoteTaskHistoryStyled.SelectionItemText primary="Attachments" />
          </NoteTaskHistoryStyled.SelectionItem>
          <Divider />
          <NoteTaskHistoryStyled.SelectionItem
            button
            onClick={handleSelectType('history')}>
            <HistoryIcon />
            <NoteTaskHistoryStyled.SelectionItemText primary="History" />
          </NoteTaskHistoryStyled.SelectionItem>
          <Divider />
          <PermissionMetaQuery validate="deleteDO">
            <NoteTaskHistoryStyled.SelectionItem
              button
              onClick={handleSelectType('delete')}>
              <DeleteForeverIcon />
              <NoteTaskHistoryStyled.SelectionItemText primary="Delete" />
            </NoteTaskHistoryStyled.SelectionItem>
          </PermissionMetaQuery>
        </List>
      </Popover>
      <OptionView
        selectedType={selectedType}
        itemId={itemId}
        groupId={groupId}
        fields={fields}
        relationships={relationships}
        connectedContacts={connectedContacts}
        refetch={refetch}
        onClose={handleClose}
        loading={loading}
        doTitle={doTitle}
      />
    </>
  );
};

MoreOptions.defaultProps = {
  relationships: [],
  connectedContacts: [],
  refetch: () => {},
};

export default MoreOptions;
