import {experimentalStyled as styled} from '@mui/material/styles';
import Box from '@mui/material/Box';

export const Root = styled(Box)`
  display: inline-block;

  #field-wrapper {
    & > :nth-child(2) {
      flex: 1;
    }

    & > :nth-child(3) {
      flex: 0.4;
    }

    & > :nth-child(4) {
      flex: 0.2;
    }
  }
`;
