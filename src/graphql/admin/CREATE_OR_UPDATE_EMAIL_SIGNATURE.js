import {gql} from '@apollo/client';

export default gql`
  mutation createOrUpdateEmailSignature(
    $createOrUpdateEmailSignatureInput: CreateOrUpdateEmailSignatureInput!
  ) {
    createOrUpdateEmailSignature(input: $createOrUpdateEmailSignatureInput) {
      success
    }
  }
`;
