import {useEffect, useRef, useMemo} from 'react';
import isString from 'lodash/isString';

const useHTML = (elmHtml, options = {}) => {
  const scriptsRef = useRef([]);
  const createAndAppendScript = (scriptElm, parentElm) => {
    const script = document.createElement('script');

    const attributes = [];

    for (let attr of scriptElm.attributes) {
      attributes.push({
        name: attr.name,
        value: attr.value,
      });
    }

    attributes.forEach((attr) => {
      script[attr.name] = attr.value;
    });

    script.innerHTML = scriptElm.innerHTML;

    if (!options.parentSelector) {
      parentElm.removeChild(scriptElm);
      parentElm.appendChild(script);
    } else {
      document.querySelector(options.parentSelector).appendChild(script);

      scriptsRef.current.push(script);
    }
  };

  const generatedElm = useMemo(() => {
    if (elmHtml && isString(elmHtml)) {
      const containerElm = document.createElement('div');
      containerElm.innerHTML = elmHtml.trim();

      const scriptSrcs = [];

      const scriptElms = containerElm.getElementsByTagName('script');

      for (let i = 0; i < scriptElms.length; i++) {
        const scriptElm = scriptElms[i];
        const scriptSrc = scriptElm.src || scriptElm.innerHTML.trim();

        if (scriptSrc && !scriptSrcs.includes(scriptSrc)) {
          scriptSrcs.push(scriptSrc);

          createAndAppendScript(scriptElm, scriptElm.parentElement);
        }
      }

      return containerElm.childNodes;
    }

    return null;
  }, [elmHtml]);

  useEffect(() => {
    return () => {
      if (options?.cleanUpScripts && scriptsRef.current.length) {
        scriptsRef.current.forEach((scriptElm) => scriptElm.remove());
      }
    };
  }, []);

  return generatedElm;
};

export default useHTML;
