import React from 'react';

const ScrollableAreaContext = React.createContext({
  x: 0,
  y: 0,
  highestY: 0,
});

export default ScrollableAreaContext;
