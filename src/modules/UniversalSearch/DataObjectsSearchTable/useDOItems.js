import useRetryQuery from '@macanta/hooks/apollo/useRetryQuery';
import {LIST_DATA_OBJECTS} from '@macanta/graphql/dataObjects';

export const DEFAULT_PAGE = 0;
export const DEFAULT_PAGE_LIMIT = 25;

const useDOItems = (
  {
    q,
    filters,
    groupId,
    contactId,
    relationship,
    page = DEFAULT_PAGE,
    limit = DEFAULT_PAGE_LIMIT,
    order,
    orderBy,
    contactField,
  },
  options,
) => {
  const doItemsQuery = useRetryQuery(LIST_DATA_OBJECTS, {
    variables: {
      removeAppInfo: true,
      q,
      ...(filters?.length && {
        filter: JSON.stringify(
          filters.map((f, index) => {
            let operator = f.operator;

            switch (f.operator) {
              case '~>~': {
                operator = 'is greater than';
                break;
              }
              case '~<~': {
                operator = 'is less than';
                break;
              }
            }

            return {
              queryCDFieldLogic: index === 0 ? 'AND' : f.logic.toUpperCase(),
              queryCDFieldName: f.name,
              queryCDFieldOperator: operator,
              queryCDFieldValue: f.value,
              queryCDFieldValues: f.values,
            };
          }),
        ),
      }),
      groupId,
      contactId,
      relationship,
      page,
      limit,
      order,
      orderBy,
      contactField,
    },
    ...options,
  });

  const dataObjects = doItemsQuery.data?.listDataObjects;

  const selectedDO = dataObjects?.items?.find((d) => d.groupId === groupId);

  return {...doItemsQuery, data: dataObjects, selectedDO};
};

export default useDOItems;
