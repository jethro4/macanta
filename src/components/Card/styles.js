import {experimentalStyled as styled} from '@mui/material/styles';
import MUICard from '@mui/material/Card';
import MUICardHeader from '@mui/material/CardHeader';

export const Root = styled(MUICard)`
  display: flex;
  flex-direction: column;
`;

export const CardHeader = styled(MUICardHeader)`
  .MuiCardHeader-title {
    font-size: 1rem;
  }

  .MuiCardHeader-subheader {
    font-size: 0.875rem;
  }
`;
