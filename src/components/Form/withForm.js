import {withFormik} from 'formik';

const withForm = (WrappedComponent, options) => {
  return withFormik(
    Object.assign(
      {
        enableReinitialize: true,
        validateOnBlur: false,
        validateOnChange: false,
        handleSubmit: async (values, {props, ...formikHelpers}) => {
          await props.onSubmit(values, formikHelpers);
        },
        ...options,
        mapPropsToValues: (props) => ({
          ...props.initialValues,
          ...options?.mapPropsToValues?.(props),
        }),
      },
      options?.displayName && {
        displayName: options.displayName,
      },
    ),
  )(WrappedComponent);
};

export default withForm;
