const fs = require('fs');
const path = require('path');

const basename = path.basename(__filename);
const handlersMap = {};

fs.readdirSync(__dirname)
  .filter(
    (file) =>
      file.indexOf('.') !== 0 && file !== basename && file.slice(-3) === '.js',
  )
  .forEach((file) => {
    // eslint-disable-next-line import/no-dynamic-require, global-require
    const handlerObj = require(path.join(__dirname, file));

    Object.assign(handlersMap, handlerObj);
  });

const handlers = [];

for (const key of Object.keys(handlersMap)) {
  const handler = handlersMap[key];
  if (typeof handler === 'object') {
    handlers.push(handler);
  }
}

export default handlers;
