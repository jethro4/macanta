/* eslint-disable react/jsx-filename-extension */
import React, {useState, useEffect} from 'react';
import GridItem from './GridItem';

const useGridItems = ({
  data,
  rowHeight,
  renderGridItem,
  autoResizeItem,
  marginY,
  isItemDraggable,
}) => {
  const [itemDimensions, setItemDimensions] = useState({});
  const [initialized, setInitialized] = useState(false);

  const loadedAllItems =
    !!data.length &&
    !data.some((item) => {
      return !itemDimensions[item.id];
    });

  const handleLoadDimensions = ({id, dimensions}) => {
    setItemDimensions((state) => ({
      ...state,
      [id]: {
        h: dimensions.height / rowHeight,
        minH: dimensions.height / rowHeight,
      },
    }));
  };

  const handleDeleteItems = (ids) => {
    setItemDimensions((state) => {
      const stateObj = {...state};

      ids.forEach((id) => {
        delete stateObj[id];
      });

      return stateObj;
    });
  };

  useEffect(() => {
    if (!initialized && loadedAllItems) {
      setInitialized(true);
    }
  }, [loadedAllItems]);

  useEffect(() => {
    const deletedItemIds = Object.keys(itemDimensions).filter(
      (key) => !data.some((d) => d.id === key),
    );
    if (deletedItemIds.length) {
      handleDeleteItems(deletedItemIds);
    }
  }, [data]);

  const columnsWithMaxHeights = (Object.values(itemDimensions).length
    ? Object.values(itemDimensions)
    : data
  ).reduce((acc, itemDimension) => {
    let accObj = {...acc};

    const accVal =
      (accObj[itemDimension.x] || 0) +
      (itemDimension?.h || 1) * rowHeight +
      marginY * 2;
    accObj[itemDimension.x] = accVal;

    return accObj;
  }, {});
  const largestHeight = Math.max(...Object.values(columnsWithMaxHeights)) || 0;

  return {
    initialized,
    loadedAllItems,
    largestHeight,
    gridItems: data.map(({dataGrid, ...d}, index) => {
      const transformedDataGrid = {...dataGrid, ...itemDimensions[d.id]};

      return (
        <GridItem
          key={dataGrid.i}
          draggable={isItemDraggable}
          data-grid={transformedDataGrid}
          data-item-grid={JSON.stringify(transformedDataGrid)}
          item={d}
          renderGridItem={renderGridItem.bind(null, {item: d, index})}
          autoResizeItem={autoResizeItem}
          onLoadDimensions={handleLoadDimensions}
        />
      );
    }),
  };
};

export default useGridItems;
