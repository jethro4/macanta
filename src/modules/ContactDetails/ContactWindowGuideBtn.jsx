import React from 'react';
import HelperTextBtn from '@macanta/containers/FormField/HelperTextBtn';
import useContactWindowGuide from '@macanta/hooks/useContactWindowGuide';

const ContactWindowGuideBtn = () => {
  const contactWindowGuide = useContactWindowGuide();

  return <HelperTextBtn>{contactWindowGuide?.value}</HelperTextBtn>;
};

export default ContactWindowGuideBtn;
