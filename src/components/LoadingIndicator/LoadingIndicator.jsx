import React from 'react';
import clsx from 'clsx';
import {makeStyles} from '@mui/material/styles';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';
import * as Styled from './styles';

const useStyles = makeStyles(() => ({
  defaultLoadingContainer: {
    position: 'absolute',
    left: '50%',
    top: '50%',
    '-ms-transform': 'translate(-50%, -50%)',
    transform: 'translate(-50%, -50%)',
    zIndex: 1000,
  },
  flex: {
    display: 'flex',
  },
  alignTop: {
    top: 16,
    transform: 'translateX(-50%)',
  },
  alignCenter: {
    justifyContent: 'center',
  },
  backdrop: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(0, 0, 0, 0.4)',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1300,
  },
  backdropLight: {
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
  },
  marginAuto: {
    margin: '0 auto',
  },
}));

const LoadingIndicator = ({
  loading,
  transparent,
  fill,
  backdrop,
  align,
  color,
  style,
  sx,
  backdropStyle,
  marginAuto,
  className,
  ...props
}) => {
  const classes = useStyles();

  if (!loading) {
    return null;
  }

  let RootComp = (
    <Styled.Container
      transparent={transparent || backdrop}
      className={clsx(className, {
        [classes.defaultLoadingContainer]: fill,
        [classes.flex]: !fill,
        [classes.alignTop]: align === 'top',
        [classes.alignCenter]: align === 'center',
        [classes.marginAuto]: marginAuto,
      })}
      style={style}
      sx={sx}>
      <CircularProgress
        style={{
          color: !backdrop || backdrop === 'light' ? color : 'white',
        }}
        {...props}
      />
    </Styled.Container>
  );

  if (backdrop) {
    RootComp = (
      <Box
        className={clsx({
          [classes.backdrop]: true,
          [classes.backdropLight]: backdrop === 'light',
        })}
        style={backdropStyle}>
        {RootComp}
      </Box>
    );
  }

  return RootComp;
};

LoadingIndicator.defaultProps = {
  loading: true,
  color: '#aaa',
  size: 24,
};

export default LoadingIndicator;
