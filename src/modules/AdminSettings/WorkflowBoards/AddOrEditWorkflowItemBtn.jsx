import React, {useState} from 'react';
import AddIcon from '@mui/icons-material/Add';
import VisibilityIcon from '@mui/icons-material/Visibility';
import Modal from '@macanta/components/Modal';
import Button from '@macanta/components/Button';
import Tooltip from '@macanta/components/Tooltip';
import WorkflowBoardsForms from './WorkflowBoardsForms';
import * as DataObjectsSearchTableStyled from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/styles';

const AddOrEditWorkflowItemBtn = ({item, onSuccess}) => {
  const [showModal, setShowModal] = useState(false);

  const handleShowModal = () => {
    setShowModal(true);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  const handleSuccess = () => {
    handleCloseModal();

    onSuccess && onSuccess();
  };

  return (
    <>
      {!item ? (
        <Button
          onClick={handleShowModal}
          size="small"
          variant="contained"
          startIcon={<AddIcon />}>
          Add Workflow Board
        </Button>
      ) : (
        <Tooltip title="Show Details">
          <DataObjectsSearchTableStyled.ActionButton
            show={showModal}
            onClick={handleShowModal}
            size="small">
            <VisibilityIcon />
          </DataObjectsSearchTableStyled.ActionButton>
        </Tooltip>
      )}
      <Modal
        headerTitle={!item ? 'Add Workflow Board' : item.id}
        open={showModal}
        onClose={handleCloseModal}
        contentWidth={1400}
        contentHeight={800}>
        <WorkflowBoardsForms item={item} onSuccess={handleSuccess} />
      </Modal>
    </>
  );
};

export default AddOrEditWorkflowItemBtn;
