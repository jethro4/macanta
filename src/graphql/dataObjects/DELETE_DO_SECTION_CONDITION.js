import {gql} from '@apollo/client';

export default gql`
  mutation deleteDOSectionCondition($input: DeleteDOSectionConditionInput!) {
    deleteDOSectionCondition(input: $input) {
      id
      success
    }
  }
`;
