import {gql} from '@apollo/client';

export default gql`
  query listBlastProgress(
    $appName: String!
    $apiKey: String!
    $userId: String!
  ) {
    listBlastProgress(appName: $appName, apiKey: $apiKey, userId: $userId) {
      items {
        type
        queryId
        subject
        senderEmail
        groupName
        widgetName
        status
        progress
        created
        updated
        runtime
      }
    }
  }
`;
