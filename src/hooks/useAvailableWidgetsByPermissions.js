import {useState, useEffect} from 'react';
import useQuery from '@macanta/hooks/apollo/useQuery';
import {GET_ACCESS_PERMISSIONS} from '@macanta/graphql/permissions';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {ACCESS_LEVELS} from '@macanta/constants/accessLevels';
import {getAvailableWidgetId} from '@macanta/modules/pages/AppContainer/Widgets/helpers';
import * as Storage from '@macanta/utils/storage';

const useAvailableWidgetsByPermissions = ({type, widgets, disabledWidgets}) => {
  const {userId: loggedInUserId, email: userEmail} = Storage.getItem(
    'userDetails',
  );
  const [filteredWidgets, setFilteredWidgets] = useState([]);
  const [availableWidgets, setAvailableWidgets] = useState([]);

  const {data} = useQuery(GET_ACCESS_PERMISSIONS, {
    variables: {
      email: userEmail,
    },
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
    onError() {},
  });

  useEffect(() => {
    if (!widgets) {
      return;
    }

    const isAdmin = data?.getAccessPermissions?.isAdmin;

    if (isAdmin) {
      setFilteredWidgets(widgets);
    } else if (type === 'availableTaskWidgets') {
      const tasks = data?.getAccessPermissions?.tasks;
      let filteredTaskWidgets = [];

      tasks?.forEach((task) => {
        if (task.allow) {
          if (task.taskId === 'OwnTask') {
            const ownWidget = widgets.find((w) => w.userId === loggedInUserId);

            if (ownWidget) {
              filteredTaskWidgets = filteredTaskWidgets.concat(ownWidget);
            }
          }
          if (task.taskId === 'AllUserTask') {
            filteredTaskWidgets = filteredTaskWidgets.concat(
              widgets.filter((w) => {
                return w.userId !== loggedInUserId;
              }),
            );
          } else if (task.taskId === 'SpecificUserTask') {
            filteredTaskWidgets = filteredTaskWidgets.concat(
              widgets.filter((w) => {
                return task.specificIds.includes(w.userId);
              }),
            );
          }
        }
      });

      setFilteredWidgets(filteredTaskWidgets);
    } else if (type === 'availableQueryWidgets') {
      const mqbs = data?.getAccessPermissions?.mqbs;

      setFilteredWidgets(
        widgets.filter((w) => {
          const queryAccess = mqbs?.find(
            (access) => access.queryId === w.queryId,
          );

          return [ACCESS_LEVELS.READ_WRITE, ACCESS_LEVELS.READ_ONLY].includes(
            queryAccess?.permission,
          );
        }),
      );
    }
  }, [widgets]);

  useEffect(() => {
    setAvailableWidgets(
      filteredWidgets.filter((w) => {
        const isDisabledWidget = disabledWidgets?.some((disabledWidget) => {
          const availableWidgetId = getAvailableWidgetId(disabledWidget);

          return w.id === availableWidgetId;
        });

        return !isDisabledWidget;
      }),
    );
  }, [filteredWidgets]);

  return availableWidgets;
};

export default useAvailableWidgetsByPermissions;
