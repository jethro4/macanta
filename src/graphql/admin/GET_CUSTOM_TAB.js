import {gql} from '@apollo/client';
import {CUSTOM_TAB_FRAGMENT} from './fragments';

export default gql`
  query getCustomTab($appName: String!, $apiKey: String!, $id: ID!) {
    getCustomTab(appName: $appName, apiKey: $apiKey, id: $id) {
      ...customTabFragment
    }
  }
  ${CUSTOM_TAB_FRAGMENT}
`;
