import useValue from '@macanta/hooks/base/useValue';
import {ACCESS_LEVELS} from '@macanta/constants/accessLevels';
import useSections from './useSections';

const useSectionWithPermission = ({groupId, permission, ...args}, options) => {
  const sectionsQuery = useSections({groupId, ...args}, options);

  const sections = sectionsQuery.data;

  const [data] = useValue(() => {
    return sections.map((section) => {
      const sectionAccess = permission?.sections?.find(
        (p) => p.groupId === groupId,
      )?.access;
      const permissionItem = sectionAccess?.find(
        (a) => section.sectionName === a.name,
      );
      const sectionPermission =
        permissionItem?.permission || ACCESS_LEVELS.NO_ACCESS;

      return {
        ...section,
        ...permissionItem,
        hasAccess: [ACCESS_LEVELS.READ_ONLY, ACCESS_LEVELS.READ_WRITE].includes(
          sectionPermission,
        ),
        permission: sectionPermission,
      };
    });
  }, [groupId, permission, sections]);

  return {...sectionsQuery, data};
};

export default useSectionWithPermission;
