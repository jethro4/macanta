import {overrideHandlers} from '@tests/jestSettings/utils/renderUtils';
import {graphql} from 'msw';

export const handleMockDOToDORelationships = ({direct, indirect} = {}) => {
  const mockedDirect = direct;
  const mockedIndirect = indirect || {};

  overrideHandlers(
    graphql.query('listDOToDORelationships', (req, res, ctx) => {
      if (req.variables.type === 'direct') {
        return res(
          ctx.data({
            listDOToDORelationships: mockedDirect,
          }),
        );
      } else if (req.variables.type === 'indirect') {
        if (
          req.variables.doItemId === 'ItemDataObject1' &&
          mockedIndirect['ItemDataObject1']
        ) {
          return res(
            ctx.data({
              listDOToDORelationships: mockedIndirect['ItemDataObject1'],
            }),
          );
        } else if (
          req.variables.doItemId === 'ItemDataObject3' &&
          mockedIndirect['ItemDataObject3']
        ) {
          return res(
            ctx.data({
              listDOToDORelationships: mockedIndirect['ItemDataObject3'],
            }),
          );
        }
      }

      return res(
        ctx.data({
          listDOToDORelationships: [],
        }),
      );
    }),
  );

  return mockedDirect;
};
