import React from 'react';
import ContactFormsIconBtn from '@macanta/modules/ContactForms/ContactFormsIconBtn';
import MoreOptions from './MoreOptions';
import * as DataObjectsSearchTableStyled from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/styles';

const ActionButtons = ({contact}) => {
  return (
    <DataObjectsSearchTableStyled.ActionButtonContainer>
      <ContactFormsIconBtn item={contact} />
      <MoreOptions contact={contact} />
    </DataObjectsSearchTableStyled.ActionButtonContainer>
  );
};

export default ActionButtons;
