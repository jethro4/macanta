import {
  CREATE_NOTE,
  UPDATE_NOTE,
  LIST_NOTES,
} from '@macanta/graphql/notesTasks';
import {useMutation} from '@apollo/client';

const useNoteMutation = ({onSuccess}) => {
  const [callCreateNoteMutation, createNoteMutation] = useMutation(
    CREATE_NOTE,
    {
      update(cache, {data: {createNote}}) {
        cache.modify({
          fields: {
            listNotes(listNotesRef) {
              cache.modify({
                id: listNotesRef.__ref,
                fields: {
                  items(existingItems = []) {
                    const newItemRef = cache.writeQuery({
                      data: createNote,
                      query: LIST_NOTES,
                    });

                    return [newItemRef, ...existingItems];
                  },
                },
              });
            },
          },
        });
      },
      onCompleted(data) {
        if (data?.createNote) {
          onSuccess();
        }
      },
      onError() {},
    },
  );
  const [callUpdateNoteMutation, updateNoteMutation] = useMutation(
    UPDATE_NOTE,
    {
      onCompleted(data) {
        if (data?.updateNote) {
          onSuccess();
        }
      },
      onError() {},
    },
  );
  const loading = Boolean(
    createNoteMutation.loading || updateNoteMutation.loading,
  );
  const error = createNoteMutation.error || updateNoteMutation.error;

  return {callCreateNoteMutation, callUpdateNoteMutation, loading, error};
};

export default useNoteMutation;
