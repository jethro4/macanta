import {gql} from '@apollo/client';

export default gql`
  query getAppSubscription {
    getAppSubscription {
      planId
      status
      options {
        id
        name
        price
        currencyCode
        period
        periodUnit
        pricingModel
        subscriptionUrl
      }
    }
  }
`;
