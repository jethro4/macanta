<h3>Preview:</h3>

```jsx
import useMutation from '@macanta/hooks/apollo/useMutation';
import {CREATE_OR_UPDATE_DO_SCHEDULE} from '@macanta/graphql/scheduler';
import DOScheduleForm from '@macanta/modules/AdminSettings/DOScheduler/DOScheduleForm';
import LoadingIndicator from '@macanta/components/LoadingIndicator';

const [callMutation, mutationState] = useMutation(
  CREATE_OR_UPDATE_DO_SCHEDULE,
  {
    alertOnComplete: true,
    alertOnError: true,
    successMessage: 'Sample Success Message',
    errorMessage: 'Sample Error Message',
    onCompleted: async ({result}) => {
      console.info(result);
    },
    onError(err) {
      console.error(err);
    },
  },
);

const handleSave = async (values) => {
  console.info('Values to save', values);

  callMutation({
    variables: {
      id: values.id,
      groupId: values.groupId,
      name: values.name,
      description: values.description,
      userRelationshipId: values.userRelationshipId,
      contactRelationshipId: values.contactRelationshipId,
      startDateFieldId: values.startDateFieldId,
      endDateFieldId: values.endDateFieldId || '',
      schedulerType: values.schedulerType,
    },
  });
};

<>
  <DOScheduleForm
    item={{
      id: 'sampleId',
      groupId: 'sampleId',
      name: 'Test',
      description: 'Test Description',
      userRelationshipId: '',
      contactRelationshipId: '',
      startDateFieldId: '',
      endDateFieldId: '',
      schedulerType: 'DateTime',
    }}
    onSave={handleSave}
  />
  <LoadingIndicator modal loading={mutationState.loading} />
</>;
```
