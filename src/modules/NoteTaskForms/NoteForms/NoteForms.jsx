import React from 'react';
import {NOTE_TAG} from '@macanta/constants/noteTaskTypes';
import Form from '@macanta/components/Form';
import Typography from '@mui/material/Typography';
import TurnedInIcon from '@mui/icons-material/TurnedIn';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import FormField from '@macanta/containers/FormField';
import * as Styled from '@macanta/modules/NoteTaskForms/styles';

export const filterNoteTags = (tags) => {
  return tags.filter((v) => !NOTE_TAG.TYPES.includes(v.toLowerCase()));
};

const NoteForms = ({
  initValues,
  onSubmit,
  error,
  validationSchema,
  loading,
  saveBtnIcon,
  saveBtnText,
  size,
  hideFooter,
  onChange,
  ...props
}) => {
  const handleSubmitFilterTags = (values) => {
    onSubmit({...values, tags: filterNoteTags(values.tags)});
  };

  const handleChange = (...args) => {
    onChange?.(...args);
  };

  return (
    <>
      <Styled.Root {...props}>
        <Form
          initialValues={initValues}
          validationSchema={validationSchema}
          onSubmit={handleSubmitFilterTags}>
          {({values, errors, handleSubmit, setFieldValue, dirty}) => (
            <>
              <Styled.TextField
                labelPosition="normal"
                error={errors.title}
                onChange={(val) => {
                  setFieldValue('title', val);

                  handleChange(val, 'title');
                }}
                label="Title"
                variant="outlined"
                value={values.title}
                fullWidth
                size={size}
              />

              <FormField
                type="TextArea"
                labelPosition="normal"
                error={errors.note}
                onChange={(val) => {
                  setFieldValue('note', val);

                  handleChange(val, 'note');
                }}
                label="Description"
                variant="outlined"
                value={values.note}
                fullWidth
                size={size}
              />

              <Styled.AddTagsField
                error={errors.tags}
                value={values.tags}
                onChange={(val) => {
                  setFieldValue('tags', val);

                  handleChange(val, 'tags');
                }}
                label="Tags"
                variant="outlined"
                defaultTags={filterNoteTags(values.tags)}
                fullWidth
                size={size}
              />

              {!!error && (
                <Typography
                  color="error"
                  variant="subtitle2"
                  align="center"
                  style={{
                    marginTop: '1rem',
                  }}>
                  {error.message}
                </Typography>
              )}

              {!hideFooter && (
                <Styled.Footer>
                  <Styled.FooterButton
                    disabled={!dirty}
                    variant="contained"
                    startIcon={saveBtnIcon || <TurnedInIcon />}
                    onClick={handleSubmit}
                    size={size}>
                    {saveBtnText || 'Save'}
                  </Styled.FooterButton>
                </Styled.Footer>
              )}
            </>
          )}
        </Form>
      </Styled.Root>
      <LoadingIndicator modal loading={loading} />
    </>
  );
};

NoteForms.defaultProps = {
  size: 'medium',
};

export default NoteForms;
