import React, {useState} from 'react';
import Box from '@mui/material/Box';
import {SchedulesList, SchedulesCalendar} from '.';
import Section from '@macanta/containers/Section';
import {buildDate, getMonthOfYear, getNow, getYear} from '@macanta/utils/time';
import useSchedules from '@macanta/hooks/scheduler/useSchedules';
import {getFirstAndLastDates} from '@macanta/modules/UserSchedules/helpers';
import useFixedValue from '@macanta/hooks/base/useFixedValue';
import useValue from '@macanta/hooks/base/useValue';
import MonthPickerField from '@macanta/modules/UserSchedules/MonthPickerField';
import FlexGrid from '@macanta/components/Base/FlexGrid';
import Time from '@macanta/components/Base/Time';
import * as Styled from './styles';
import useAppSettings from '@macanta/hooks/admin/useAppSettings';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import Colors from '@macanta/utils/colors';

const UserSchedules = () => {
  const nowDate = useFixedValue(getNow());

  const [searchOptions, setSearchOptions] = useState(() => ({
    month: getMonthOfYear(nowDate),
    year: getYear(nowDate),
  }));

  const [dateRange] = useValue(() => {
    const {month, year} = searchOptions;

    const date = buildDate({month, year});

    const [dateStart, dateEnd] = getFirstAndLastDates(date);

    return {dateStart, dateEnd};
  }, [searchOptions]);

  const schedulesQuery = useSchedules(dateRange);
  const appSettingsQuery = useAppSettings({
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const appTimeZone = appSettingsQuery.data?.appTimeZone;

  return (
    <Section
      loading={schedulesQuery.loading}
      fullHeight
      title="Schedules"
      bodyStyle={{
        backgroundColor: '#f2f4f5',
      }}
      headerStyle={{
        borderBottom: `1px solid ${Colors.borderLight}`,
        boxShadow: 'none',
      }}
      HeaderMidComp={
        <Box
          sx={{
            display: 'flex',
            alignItems: 'center',
          }}>
          <MonthPickerField value={searchOptions} onChange={setSearchOptions} />
        </Box>
      }
      HeaderRightComp={
        <Styled.HighlightedInfoContainer>
          <Styled.InfoBox>
            <Styled.InfoLabel>Date & Time:</Styled.InfoLabel>
            <Styled.InfoValue>
              <Time intervalType="minute" format="MMM DD YYYY, hh:mma" />
            </Styled.InfoValue>
          </Styled.InfoBox>
          <Styled.InfoBox>
            <Styled.InfoLabel>Timezone:</Styled.InfoLabel>
            <Styled.InfoValue>{appTimeZone}</Styled.InfoValue>
          </Styled.InfoBox>
        </Styled.HighlightedInfoContainer>
      }>
      <FlexGrid
        cols={2}
        // margin={[0, '8px']}
        border={[0, 1]}
        sx={{
          height: '100%',
        }}>
        <SchedulesCalendar
          colWidth={1.35}
          searchOptions={searchOptions}
          data={schedulesQuery.allData}
        />
        <SchedulesList
          loading={schedulesQuery.loading}
          data={schedulesQuery.monthlyData}
          searchOptions={searchOptions}
        />
      </FlexGrid>
    </Section>
  );
};

export default UserSchedules;
