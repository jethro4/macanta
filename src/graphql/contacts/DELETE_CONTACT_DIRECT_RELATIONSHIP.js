import {gql} from '@apollo/client';

export default gql`
  mutation deleteContactDirectRelationship(
    $deleteContactDirectRelationshipInput: DeleteContactDirectRelationshipInput!
  ) {
    deleteContactDirectRelationship(
      input: $deleteContactDirectRelationshipInput
    ) {
      success
    }
  }
`;
