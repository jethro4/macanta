import {gql} from '@apollo/client';
import {CONTACT_ATTRIBUTES} from './fragments';

export default gql`
  query listContacts(
    $appName: String!
    $apiKey: String!
    $q: String
    $page: Int
    $limit: Int
    $order: String
    $orderBy: String
  ) {
    listContacts(
      appName: $appName
      apiKey: $apiKey
      q: $q
      page: $page
      limit: $limit
      order: $order
      orderBy: $orderBy
    ) {
      items {
        ...contactResults
      }
      count
      total
    }
  }
  ${CONTACT_ATTRIBUTES}
`;
