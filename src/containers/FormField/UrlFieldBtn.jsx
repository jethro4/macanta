import React from 'react';
import OpenInNewIcon from '@mui/icons-material/OpenInNew';
import * as Styled from './styles';

const UrlFieldBtn = ({value}) => {
  const handleOpenLink = () => {
    let url = value;

    if (!/(http(s?)):\/\//i.test(url)) {
      url = 'https://' + url;
    }

    window.open(encodeURI(url));
  };

  return (
    !!value && (
      <Styled.UrlFieldBtn onClick={handleOpenLink}>
        <OpenInNewIcon />
      </Styled.UrlFieldBtn>
    )
  );
};

UrlFieldBtn.defaultProps = {
  value: '',
};

export default UrlFieldBtn;
