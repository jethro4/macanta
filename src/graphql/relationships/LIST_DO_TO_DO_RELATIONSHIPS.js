import {gql} from '@apollo/client';
import {DO_TO_DO_RELATIONSHIP_FRAGMENT} from '@macanta/graphql/relationships/fragments';

export default gql`
  query listDOToDORelationships($doItemId: ID, $groupId: ID, $type: String) {
    listDOToDORelationships(
      doItemId: $doItemId
      groupId: $groupId
      type: $type
    ) {
      ...doToDORelationshipFragment
    }
  }
  ${DO_TO_DO_RELATIONSHIP_FRAGMENT}
`;
