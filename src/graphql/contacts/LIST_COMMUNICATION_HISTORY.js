import {gql} from '@apollo/client';

export default gql`
  query listCommunicationHistory(
    $appName: String!
    $apiKey: String!
    $contactId: ID
    $emailId: ID
    $stats: Boolean
    $type: String
    $page: Int
    $limit: Int
  ) {
    listCommunicationHistory(
      appName: $appName
      apiKey: $apiKey
      contactId: $contactId
      emailId: $emailId
      stats: $stats
      type: $type
      page: $page
      limit: $limit
    ) {
      ... on CommunicationEmailMessage {
        html
      }
      ... on CommunicationEmailStats {
        opens
        clicks
      }
      ... on CommunicationHistory {
        items {
          ... on CommunicationEmailItem {
            emailId
            contactId
            contactName
            email
            status
            message
            subject
            sentDateTime
            lastEventReceived
            attachments {
              thumbnail
              fileName
              fileExt
              isUrl
            }
            timezone
          }
          ... on CommunicationSMSItem {
            contactId
            contactName
            phoneNumber
            status
            message
            lastEventSent
            sentByNumber
            sentByFrom
            timezone
          }
        }
        total
      }
    }
  }
`;
