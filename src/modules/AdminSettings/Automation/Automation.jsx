import React from 'react';
import Section from '@macanta/containers/Section';
import {
  ROOT_PATH_ADMIN,
  SettingsItemCard,
} from '@macanta/modules/AdminSettings/AdminSettings';
import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import FactCheckIcon from '@material-ui/icons/FactCheck';
import EmailIcon from '@material-ui/icons/Email';
import * as Styled from '@macanta/modules/AdminSettings/styles';
import {useMatch} from '@reach/router';
import Breadcrumbs from '@macanta/components/Breadcrumbs';

export const ROOT_PATH_AUTOMATION = '/app/admin-settings/automation';

const AUTOMATION_ROUTES = [
  // {
  //   path: 'automation/new-automation-workflow',
  //   icon: SettingsApplicationsIcon,
  //   title: 'Create a New Workflow',
  //   description: '',
  // },
  {
    path: 'automation/edit-automation-workflow',
    icon: FactCheckIcon,
    title: 'Edit Automation Workflow',
    description: '',
  },
  {
    path: 'automation/advanced-automation-editor',
    icon: EmailIcon,
    title: 'Workflow Advanced Editor',
    description: '',
  },
];

const Automation = ({children}) => {
  const isRootPath = useMatch(encodeURI(ROOT_PATH_AUTOMATION));

  return (
    <NoteTaskHistoryStyled.FullHeightGrid container>
      <NoteTaskHistoryStyled.FullHeightFlexGridColumn item xs={12} md={12}>
        {!isRootPath ? (
          children
        ) : (
          <Section
            fullHeight
            bodyStyle={{
              backgroundColor: '#f5f6fa',
            }}
            HeaderLeftComp={
              <Breadcrumbs
                title="Automation Settings"
                backTitle="Admin Settings"
                backPath={ROOT_PATH_ADMIN}
              />
            }>
            <Styled.GridContainer container>
              {AUTOMATION_ROUTES.map((itemProps) => (
                <SettingsItemCard key={itemProps.path} {...itemProps} />
              ))}
            </Styled.GridContainer>
          </Section>
        )}
      </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
    </NoteTaskHistoryStyled.FullHeightGrid>
  );
};

export default Automation;
