import {gql} from '@apollo/client';

export const EMAIL_ACTIONS_FRAGMENT = gql`
  fragment emailActionsFragment on EmailActionsItems {
    queryId
    queryType
    queryFromAddress
    queryFromName
    querySubject
    queryAttachment
    queryDescription
    queryPreviewText
    querySenderSigRelationship
    queryName
    cc_email {
      MergedField
      BCC
    }
  }
`;

export default gql`
  query getEmailAction($id: ID) {
    getEmailAction(id: $id) {
      ...emailActionsFragment
      BCCEmail
      emailTemplate {
        EmailId
        CompiledEmailHTML
      }
    }
  }
  ${EMAIL_ACTIONS_FRAGMENT}
`;
