import React from 'react';
import AddCustomTabBtn from './AddCustomTabBtn';
import DeleteCustomTabBtn from './DeleteCustomTabBtn';
import * as DataObjectsSearchTableStyled from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/styles';

const ItemButtons = ({item}) => {
  return (
    <DataObjectsSearchTableStyled.ActionButtonContainer>
      <AddCustomTabBtn item={item} />
      <DeleteCustomTabBtn id={item.id} name={item.title} />
    </DataObjectsSearchTableStyled.ActionButtonContainer>
  );
};

export default ItemButtons;
