import React, {useState, useEffect, useMemo} from 'react';
import useQuery from '@macanta/hooks/apollo/useQuery';
import Modal from '@macanta/components/Modal';
import Button from '@macanta/components/Button';
import TurnedInIcon from '@mui/icons-material/TurnedIn';
import useContact from '@macanta/hooks/contact/useContact';
import {GET_DATA_OBJECT} from '@macanta/graphql/dataObjects';
import Attachments from '@macanta/modules/DataObjectItems/DOItemForms/Attachments';
import {
  transformAttachments,
  getDefaultConnectedContacts as getDefaultConnectedContactsHelper,
} from '@macanta/selectors/dataObject.selector';
import useContactId from '@macanta/hooks/useContactId';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import * as FormStyled from '@macanta/modules/NoteTaskForms/styles';

const AttachmentsDOItemOption = ({
  itemId,
  modal,
  showModal: showModalProp = false,
  fields,
  relationships,
  onClose,
  onSave,
}) => {
  const contactId = useContactId();

  const [changes, setChanges] = useState([]);
  const [showModal, setShowModal] = useState(showModalProp);

  const {data: defaultConnectedContact} = useContact(contactId, {
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });
  const {loading, data} = useQuery(GET_DATA_OBJECT, {
    variables: {
      itemId,
    },
  });

  const attachments = useMemo(() => {
    if (data?.getDataObject?.attachments?.length) {
      return transformAttachments({
        attachments: data.getDataObject.attachments,
        connectedContacts: getDefaultConnectedContactsHelper({
          connectedContacts: data.getDataObject.connectedContacts,
          relationships,
          contactId,
          defaultConnectedContact,
        }),
        fields,
        contactId,
      });
    }
    return [];
  }, [data?.getDataObject?.attachments]);

  const handleCloseDOHistory = () => {
    setShowModal(false);

    onClose && onClose();
  };

  const handleValueChange = (formData, options = {}) => {
    const attachment = changes.find((a) => a.id === formData.id);
    let updatedAttachments = [];

    if (attachment) {
      if (options.type === 'delete') {
        updatedAttachments = changes.filter((a) => a.id !== formData.id);
      } else {
        updatedAttachments = changes.map((a) => {
          if (a.id === formData.id) {
            return formData;
          }

          return a;
        });
      }
    } else {
      if (options.type === 'delete') {
        updatedAttachments = changes.concat({
          id: formData.id,
          deleted: true,
        });
      } else {
        updatedAttachments = changes.concat(formData);
      }
    }

    setChanges(updatedAttachments);
  };

  const handleSubmit = async () => {
    await onSave({attachments: changes});

    setChanges([]);
  };

  useEffect(() => {
    setShowModal(showModalProp);
  }, [showModalProp]);

  let doAttachments = (
    <Attachments
      style={{
        margin: 0,
      }}
      loading={loading}
      data={attachments}
      doItemId={itemId}
      onChange={handleValueChange}
      FooterComp={
        <FormStyled.Footer
          style={{
            padding: '1rem',
            margin: 0,
            borderTop: '1px solid #eee',
            width: '100%',
            backgroundColor: '#f5f6fa',
          }}>
          <Button
            disabled={!changes.length}
            variant="contained"
            startIcon={<TurnedInIcon />}
            onClick={handleSubmit}
            size="medium">
            Save Changes
          </Button>
        </FormStyled.Footer>
      }
    />
  );

  if (modal) {
    doAttachments = (
      <Modal
        headerTitle={itemId}
        open={showModal}
        onClose={handleCloseDOHistory}
        contentWidth={600}
        contentHeight={'90%'}>
        {doAttachments}
      </Modal>
    );
  }

  return doAttachments;
};

export default AttachmentsDOItemOption;
