import React, {useMemo} from 'react';
import {useFormikContext} from 'formik';
import AddIcon from '@mui/icons-material/Add';
import {useTheme} from '@mui/material/styles';
import Typography from '@mui/material/Typography';
import MenuList from '@mui/material/MenuList';
import Form from '@macanta/components/Form';
import {PopoverButton} from '@macanta/components/Button';
import {SelectMenuItems} from '@macanta/components/Forms/ChoiceFields/SelectField';
import Section from '@macanta/containers/Section';
import CriteriaForms from '@macanta/modules/AdminSettings/QueryBuilder/CriteriaForms';
import useContactFields from '@macanta/modules/ContactDetails/useContactFields';
import useDeepCompareMemo from '@macanta/hooks/base/useDeepCompareMemo';
import {CONTACT_DEFAULT_FIELDS} from '@macanta/constants/contactFields';
import {DEFAULT_OPERATOR} from '@macanta/constants/operators';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {
  sortArrayByObjectKey,
  sortArrayByPriority,
  arrayConcatOrUpdateByKey,
  remove,
} from '@macanta/utils/array';
import * as TableStyled from '@macanta/components/Table/styles';

const defaultFields = CONTACT_DEFAULT_FIELDS.map((f) => ({
  id: f.name,
  label: f.label,
  value: f.name,
}));

const getNewItem = (type, logic) => {
  return {
    id: Date.now(),
    name: '',
    logic,
    operator: DEFAULT_OPERATOR,
    values: null,
    value: '',
  };
};

const DOSectionConditionsSection = (props) => {
  const {values, setValues} = useFormikContext();
  const theme = useTheme();

  const contactCustomFieldsQuery = useContactFields(
    null,
    'contactCustomFields',
    {
      removeEmail: true,
      fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
    },
  );

  const contactCustomFields =
    contactCustomFieldsQuery.data?.listDataObjectFields;

  const handleAddSection = (sectionId) => {
    const section = sectionNames.find((s) => s.value === sectionId);

    const newItem = {
      id: section.value,
      sectionName: section.label,
      conditions: [getNewItem(section.value, '')],
    };

    setValues((state) => {
      const sortedSectionConditions = sortArrayByPriority(
        arrayConcatOrUpdateByKey({
          arr: state.sectionConditions,
          item: newItem,
        }),
        'id',
        sectionNames.map((s) => s.value),
      );

      return {
        ...state,
        sectionConditions: sortedSectionConditions,
        updatedSectionConditions: sortedSectionConditions,
      };
    });
  };

  const handleChange = (value) => {
    setValues((state) => {
      const [
        updatedSectionConditions,
        deletedSectionConditions,
      ] = state.sectionConditions.reduce(
        (acc, sectionCondition) => {
          const conditions = value[sectionCondition.id];

          if (conditions) {
            let updatedSectionConditionsArr = [...acc[0]],
              deletedSectionConditionsArr = [...acc[1]];

            if (conditions.length) {
              updatedSectionConditionsArr = arrayConcatOrUpdateByKey({
                arr: updatedSectionConditionsArr,
                item: {
                  ...sectionCondition,
                  conditions: conditions.map((condition) => {
                    const field = allFields.find(
                      (f) => f.value === condition.name,
                    );

                    return {
                      ...condition,
                      fieldId: field?.id,
                    };
                  }),
                },
              });

              deletedSectionConditionsArr = deletedSectionConditionsArr.filter(
                (sectionName) => sectionName !== sectionCondition.sectionName,
              );
            } else {
              updatedSectionConditionsArr = remove({
                arr: updatedSectionConditionsArr,
                key: 'id',
                value: sectionCondition.id,
              });

              deletedSectionConditionsArr = deletedSectionConditionsArr.concat(
                sectionCondition.sectionName,
              );
            }

            return [updatedSectionConditionsArr, deletedSectionConditionsArr];
          }

          return acc;
        },
        [state.sectionConditions, state.deletedSectionConditions],
      );

      return {
        ...state,
        sectionConditions: updatedSectionConditions,
        updatedSectionConditions,
        deletedSectionConditions,
      };
    });
  };

  const sectionNames = useMemo(() => {
    if (values.sectionsData.length) {
      return values.sectionsData.map((section) => ({
        label: section.sectionName,
        value: section.id,
        IconComp: (
          <AddIcon
            color="primary"
            style={{
              marginLeft: -10,
              marginRight: 8,
            }}
          />
        ),
      }));
    }

    return [];
  }, [values.sectionsData]);

  const fieldNames = useMemo(() => {
    if (values.sectionsData.length) {
      return values.sectionsData.reduce(
        (acc, section) => {
          const fieldsArr = [...acc];

          section.subGroups.forEach((subGroup) => {
            subGroup.data.forEach((d) => {
              fieldsArr.push({
                id: d.id,
                label: d.name,
                value: d.name,
                type: d.type,
                choices: d.choices,
              });
            });
          });

          return fieldsArr;
        },
        [
          {
            isSubheader: true,
            label: `${values.name} Fields`,
          },
        ],
      );
    }

    return [];
  }, [values.name, values.sectionsData]);

  const allFields = useMemo(() => {
    if (contactCustomFields && fieldNames) {
      return [
        ...fieldNames,
        ...[
          {
            isSubheader: true,
            label: 'Contact Basic Fields',
          },
        ].concat(defaultFields),
        ...[
          {
            isSubheader: true,
            label: 'Contact Custom Fields',
          },
        ].concat(
          sortArrayByObjectKey(
            contactCustomFields.map((f) => ({
              label: f.name,
              value: f.name,
              type: f.type,
              choices: f.choices,
            })),
            'label',
          ),
        ),
      ];
    }

    return [];
  }, [fieldNames, contactCustomFields]);

  const initValues = useDeepCompareMemo(() => {
    return sectionNames.reduce((acc, sectionName) => {
      const accObj = {...acc};

      const condtionItem = values.sectionConditions.find(
        (sectionCondition) => sectionCondition.id === sectionName.value,
      );
      const section = values.sectionsData.find(
        (s) => s.id === sectionName.value,
      );

      if (condtionItem?.conditions.length) {
        accObj[section.id] =
          condtionItem?.conditions.map((condition) => {
            const field = allFields.find((f) =>
              f.id ? f.id === condition.fieldId : f.value === condition.name,
            );

            return {
              ...condition,
              ...(field && {
                fieldId: field.id,
                name: field.value,
              }),
            };
          }) || [];
      }

      return accObj;
    }, {});
  }, [allFields, sectionNames, values.sectionConditions]);

  const sectionOptions = useDeepCompareMemo(() => {
    return sectionNames.filter((section) => !initValues[section.value]);
  }, [sectionNames, initValues]);

  return (
    <Section
      title="Section Conditions"
      bodyContainerStyle={{
        overflowY: 'auto',
        backgroundColor: theme.palette.grayLight.main,
        borderBottomLeftRadius: 4,
        borderBottomRightRadius: 4,
      }}
      bodyStyle={{
        paddingBottom: 16,
        display: 'flex',
        flexDirection: 'column',
      }}
      HeaderRightComp={
        <PopoverButton
          size="small"
          variant="contained"
          startIcon={<AddIcon />}
          PopoverProps={{
            contentMinWidth: 180,
            // bodyStyle: {
            //   backgroundColor: '#f5f6fA',
            // },
          }}
          renderContent={(handleClose) => (
            <MenuList
              style={{
                height: '100%',
                padding: 0,
              }}>
              <SelectMenuItems
                popover
                withDivider
                // value={value}
                options={sectionOptions}
                allowSearch={sectionOptions.length > 8}
                onChange={(newVal) => {
                  handleAddSection(newVal);
                  handleClose();
                }}
                emptyMessage="All sections added"
              />
            </MenuList>
          )}>
          Add Section
        </PopoverButton>
      }
      {...props}>
      {values.sectionConditions.length === 0 && (
        <TableStyled.EmptyMessageContainer>
          <Typography
            style={{
              paddingBottom: '2rem',
            }}
            align="center"
            color="#888">
            No conditions found
          </Typography>
        </TableStyled.EmptyMessageContainer>
      )}
      <Form initialValues={initValues} enableReinitialize={true}>
        <ConditionForms
          sectionConditions={values.sectionConditions}
          allFields={allFields}
          onChange={handleChange}
        />
      </Form>
    </Section>
  );
};

const ConditionForms = ({sectionConditions, allFields, onChange}) => {
  const theme = useTheme();
  const {values} = useFormikContext();

  return sectionConditions.map((section) => {
    const conditions = values[section.id];

    return (
      <CriteriaForms
        key={section.id}
        headerStyle={{
          boxShadow: 'none',
          borderBottom: 'none',
          minHeight: 'auto',
          paddingTop: 12,
          paddingBottom: 8,
        }}
        titleStyle={{
          color: theme.palette.primary.main,
          fontWeight: 'bold',
        }}
        style={{
          borderRadius: 0,
          boxShadow: 'none',
          borderBottom: '1px solid #e4e4e4',
          margin: '16px 0 0',
        }}
        bodyStyle={{
          padding: '8px 1rem 1rem',
        }}
        title={section.sectionName}
        data={conditions}
        type={section.id}
        choices={allFields}
        getNewItem={getNewItem}
        onChange={onChange}
      />
    );
  });
};

export default DOSectionConditionsSection;
