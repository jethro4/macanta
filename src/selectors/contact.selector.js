export const getFullName = (contact) => {
  return `${contact?.firstName || ''} ${contact?.lastName || ''}`.trim();
};

export const transformContact = (contact = {}) => {
  return {
    id: contact.id || '',
    email: contact.email || '',
    email2: contact.email2 || '',
    email3: contact.email3 || '',
    firstName: contact.firstName || '',
    middleName: contact.middleName || '',
    lastName: contact.lastName || '',
    birthday: contact.birthday || '',
    title: contact.title || '',
    company: contact.company || '',
    website: contact.website || '',
    jobTitle: contact.jobTitle || '',
    streetAddress1: contact.streetAddress1 || '',
    streetAddress2: contact.streetAddress2 || '',
    city: contact.city || '',
    state: contact.state || '',
    postalCode: contact.postalCode || '',
    country: contact.country || '',
    address2Street1: contact.address2Street1 || '',
    address2Street2: contact.address2Street2 || '',
    city2: contact.city2 || '',
    state2: contact.state2 || '',
    postalCode2: contact.postalCode2 || '',
    country2: contact.country2 || '',
    loggedInUserPermission: contact.loggedInUserPermission || '',
    createdDate: contact.createdDate || '',
    phoneNumbers: contact.phoneNumbers || [],
    phoneNumberExts: contact.phoneNumberExts || [],
    customFields: contact.customFields || [],
  };
};
