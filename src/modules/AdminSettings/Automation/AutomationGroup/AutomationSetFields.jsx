import React from 'react';
import Box from '@mui/material/Box';
import FlexGrid from '@macanta/components/Base/FlexGrid';
import FormField from '@macanta/containers/FormField';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {
  useTriggerActions,
  useTriggerConditions,
} from '@macanta/hooks/automation';
import DeleteButton from '@macanta/containers/buttons/DeleteButton';

const AutomationSetFields = ({
  item,
  onChange,
  onDelete,
  WaitActionFieldsComp,
  ...props
}) => {
  const triggerConditionsQuery = useTriggerConditions({
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });
  const triggerActionsQuery = useTriggerActions({
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const handleEdit = ({key, value}) => {
    const updatedItem = {
      ...item,
      [key]: value,
    };

    onChange?.(updatedItem);
  };

  return (
    <FlexGrid
      {...props}
      hideOverflow
      margin={[0, '1.5rem']}
      containerPadding={[1, 0]}
      style={{
        alignItems: 'center',
        ...props.style,
      }}>
      <FormField
        dividerRight={1}
        colWidth="66px"
        labelPosition="normal"
        type="Switch"
        value={item.active}
        onChange={(value) => handleEdit({item, key: 'active', value})}
      />

      <FormField
        labelPosition="normal"
        loading={triggerConditionsQuery.initLoading}
        value={item.conditionId}
        onChange={(val) => handleEdit({item, key: 'conditionId', value: val})}
        type="Select"
        options={triggerConditionsQuery.result}
        labelKey="name"
        valueKey="id"
        size="small"
        variant="outlined"
      />

      <Box colWidth={1.2}>{WaitActionFieldsComp}</Box>

      <FormField
        dividerRight={1}
        labelPosition="normal"
        loading={triggerActionsQuery.initLoading}
        value={item.actionId}
        onChange={(val) => handleEdit({item, key: 'actionId', value: val})}
        type="Select"
        options={triggerActionsQuery.result}
        labelKey="name"
        valueKey="id"
        size="small"
        variant="outlined"
      />

      <DeleteButton colWidth="32px" id={item.id} onDelete={onDelete} />
    </FlexGrid>
  );
};

export default AutomationSetFields;
