import React, {useState, useEffect} from 'react';
import useQuery from '@macanta/hooks/apollo/useQuery';
import {GET_NOTIFICATIONS_STATUS} from '@macanta/graphql/notifications';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import MenuItem from '@mui/material/MenuItem';
import isNil from 'lodash/isNil';
import * as Storage from '@macanta/utils/storage';
import * as Styled from './styles';

const DEEPWORK_DURATIONS = [
  {
    value: 0,
    label: 'None',
  },
  {
    value: 30,
    label: '30 minutes',
  },
  {
    value: 60,
    label: '60 minutes',
  },
  {
    value: 90,
    label: '90 minutes',
  },
  {
    value: 120,
    label: '120 minutes',
  },
];

const NotifDeepworkSelect = ({onSelectDuration}) => {
  const {userId} = Storage.getItem('userDetails');

  const notificationsStatusQuery = useQuery(GET_NOTIFICATIONS_STATUS, {
    variables: {
      userId,
    },
  });

  const durationData =
    notificationsStatusQuery.data?.getNotificationsStatus?.duration;

  const [duration, setDuration] = useState(durationData);

  const handleDurationChange = (newVal) => {
    const newDuration = newVal;

    setDuration(newDuration);
    onSelectDuration(Number(newDuration));
  };

  const handleDeepworkRenderValue = (value) => {
    const selectedCategory =
      DEEPWORK_DURATIONS.find((c) => String(c.value) === value) ||
      DEEPWORK_DURATIONS[0];

    return selectedCategory.label;
  };

  useEffect(() => {
    if (
      isNil(duration) &&
      !isNil(durationData) &&
      !notificationsStatusQuery.loading
    ) {
      setDuration(durationData);
    }
  }, [durationData, notificationsStatusQuery.loading]);

  return (
    <Styled.DeepworkContainer>
      <Styled.DeepworkLabel>Set Deepwork Timer:</Styled.DeepworkLabel>
      {isNil(duration) && notificationsStatusQuery.loading ? (
        <LoadingIndicator transparent size={20} />
      ) : (
        <Styled.DeepworkSelect
          select
          size="small"
          value={String(duration)}
          onChange={handleDurationChange}
          variant="filled"
          InputProps={{
            disableUnderline: true,
          }}
          renderValue={handleDeepworkRenderValue}>
          {DEEPWORK_DURATIONS.map((d) => {
            const val = String(d.value);

            return (
              <MenuItem key={val} value={val}>
                {d.label}
              </MenuItem>
            );
          })}
        </Styled.DeepworkSelect>
      )}
    </Styled.DeepworkContainer>
  );
};

export default NotifDeepworkSelect;
