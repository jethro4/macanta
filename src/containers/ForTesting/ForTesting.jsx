import {isForTesting} from './index';

const ForTesting = ({children}) => {
  return isForTesting ? children : null;
};

export default ForTesting;
