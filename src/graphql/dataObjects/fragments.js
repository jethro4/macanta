import {gql} from '@apollo/client';

export const DO_ITEM_ATTRIBUTES = gql`
  fragment doItemResults on ItemDataObject {
    id
    groupId
    data {
      id
      fieldId
      value
      contactSpecificValues {
        contactId
        value
      }
    }
    attachments {
      id
      fileName
      thumbnail
      downloadUrl
      meta {
        contactId
        referenceFieldId
      }
    }
    notes {
      id
      accepted
      userId
      creationDate
      completionDate
      lastUpdated
      lastUpdatedBy
      endDate
      type
      actionDate
      title
      noteType
      note
      tags
      createdBy
    }
    meta {
      editable
      searchable
      multiple_link
    }
    connectedContacts {
      contactId
      firstName
      lastName
      email
      relationships {
        id
      }
    }
  }
`;

export const DO_CONDITION_FRAGMENT = gql`
  fragment doSectionConditionItemResults on DOSectionConditionItem {
    id
    sectionName
    conditions {
      logic
      name
      operator
      values
      value
    }
  }
`;
