import {gql} from '@apollo/client';

export default gql`
  mutation updateAppSettings($updateAppSettingsInput: UpdateAppSettingsInput!) {
    updateAppSettings(input: $updateAppSettingsInput) {
      success
      type
      value
    }
  }
`;
