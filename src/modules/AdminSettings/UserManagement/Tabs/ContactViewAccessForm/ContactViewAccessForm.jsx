import React, {useState, useEffect} from 'react';
import ButtonGroup from '@macanta/components/ButtonGroup';
import Button from '@macanta/components/Button';
import Switch from '@macanta/components/Forms/ChoiceFields/Switch';
import BodySection from '@macanta/modules/AdminSettings/UserManagement/BodySection';
import PermissionsTable from '@macanta/modules/AdminSettings/UserManagement/Tabs/PermissionsTable';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import useContactViewPermissions from '@macanta/hooks/contact/useContactViewPermissions';
import useValue from '@macanta/hooks/base/useValue';
import {
  traverseRead,
  arrayConcatOrUpdateByKey,
  insert,
} from '@macanta/utils/array';
import {ACCESS_LEVELS} from '@macanta/constants/accessLevels';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {
  AccessRadioGroup,
  AccessTableHeadCell,
} from '@macanta/modules/AdminSettings/UserManagement/Tabs/AccessPermissions';
import SpecificRelationshipsSelect from './SpecificRelationshipsSelect';
import * as DataObjectsSearchTableStyled from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/styles';

const SECTION_ACCESS_COLUMNS = [
  {
    id: 'hasAccess',
    label: '',
    disableSort: true,
    disableResize: true,
    minWidth: 80,
    maxWidth: 80,
    renderBodyCellValue: ({item, value, handlers}) => {
      return (
        <Switch
          color="success"
          checked={value}
          onChange={(event) => {
            handlers.onChangeAccess({
              item,
              value: event.target.checked
                ? ACCESS_LEVELS.READ_ONLY
                : ACCESS_LEVELS.NO_ACCESS,
            });
          }}
        />
      );
    },
  },
  {
    id: 'label',
    label: 'Access Conditions',
    maxWidth: '100%',
    getBodyCellStyle: ({item}) =>
      !item.hasAccess && {
        color: 'grayDarkest.main',
      },
  },
  {
    id: 'others',
    label: '',
    disableSort: true,
    disableResize: true,
    minWidth: 250,
    maxWidth: 250,
    renderBodyCellValue: ({item, handlers}) => {
      return (
        item.hasAccess &&
        item.groupId !== 'Global' &&
        item.id === 'TypeB' && (
          <DataObjectsSearchTableStyled.ActionButtonContainer
            style={{
              width: '100%',
              padding: 0,
            }}>
            <SpecificRelationshipsSelect
              type="Select"
              groupId={item.groupId}
              value={item.relationship}
              onChange={(values) => {
                handlers.onChangeAccess({
                  item,
                  value: values,
                  key: 'relationship',
                });
              }}
            />
          </DataObjectsSearchTableStyled.ActionButtonContainer>
        )
      );
    },
  },
  {
    id: 'permission',
    label: '',
    disableSort: true,
    disableResize: true,
    minWidth: 200,
    maxWidth: 200,
    renderHeadCellValue: () => {
      return <AccessTableHeadCell />;
    },
    renderBodyCellValue: ({item, value, handlers}) => {
      return (
        item.hasAccess && (
          <AccessRadioGroup
            value={value}
            onChange={(val) => {
              handlers.onChangeAccess({item, value: val});
            }}
          />
        )
      );
    },
  },
];

const ContactViewAccessForm = ({permission, onChange, ...props}) => {
  const [selectedDOType, setSelectedDOType] = useState(null);

  const doTypesQuery = useDOTypes({
    removeEmail: true,
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });
  const contactViewPermissionsQuery = useContactViewPermissions(
    {
      groupId: selectedDOType?.id,
      permission,
      isDataObjectType: true,
    },
    {
      fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
    },
  );

  const doTypes = doTypesQuery.data?.listDataObjectTypes;

  const [doTypesWithGlobal] = useValue(
    () =>
      insert({
        arr: doTypes,
        item: {id: 'Global', title: 'Global'},
        index: 0,
      }),
    [doTypes],
  );

  const handleSelectSectionData = (doType) => () => {
    setSelectedDOType(doType);
  };

  const handleChangeAccess = ({item, value, key}) => {
    const currentAccess = traverseRead(permission, [
      'contacts',
      `groupId:${selectedDOType?.id}`,
      'access',
    ]);

    const updatedAccess = arrayConcatOrUpdateByKey({
      arr: currentAccess,
      item: {
        name: item.id,
        [key || 'permission']: value,
      },
      key: 'name',
      includeAll: true,
    });

    const updatedPermission = {
      ...permission,
      contacts: arrayConcatOrUpdateByKey({
        arr: permission.contacts,
        item: {
          groupId: selectedDOType?.id,
          access: updatedAccess,
        },
        key: 'groupId',
        includeAll: true,
      }),
    };

    onChange(updatedPermission);
  };

  useEffect(() => {
    if (!selectedDOType) {
      setSelectedDOType(doTypesWithGlobal[0]);
    }
  }, [doTypesWithGlobal]);

  return (
    <BodySection {...props}>
      <ButtonGroup
        increaseBtnMinWidth
        sx={{
          '& .MuiButton-root': {
            position: 'relative',

            '&:after': {
              content: '""',
              position: 'absolute',
              width: '0.5px',
              right: '-0.5px',
              top: 0,
              bottom: 0,
              backgroundColor: 'primary.main',
              borderRadius: '4px',
            },
          },
        }}
        style={{
          flexWrap: 'wrap',
          margin: '1rem',
        }}
        size="small"
        variant="outlined"
        aria-label="primary button group">
        {doTypesWithGlobal?.map((doType) => {
          return (
            <Button key={doType.id} onClick={handleSelectSectionData(doType)}>
              {doType.title}
            </Button>
          );
        })}
      </ButtonGroup>

      <PermissionsTable
        style={{
          flex: 1,
        }}
        columns={SECTION_ACCESS_COLUMNS}
        data={contactViewPermissionsQuery.data}
        handlers={{
          onChangeAccess: handleChangeAccess,
        }}
      />
    </BodySection>
  );
};

export default ContactViewAccessForm;
