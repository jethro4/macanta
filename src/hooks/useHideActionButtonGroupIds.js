import {useMemo} from 'react';
import useQuery from '@macanta/hooks/apollo/useQuery';
import {GET_ACCESS_PERMISSIONS} from '@macanta/graphql/permissions';
import * as Storage from '@macanta/utils/storage';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';

const useHideActionButtonGroupIds = () => {
  const {email: userEmail} = Storage.getItem('userDetails');

  const {data} = useQuery(GET_ACCESS_PERMISSIONS, {
    variables: {
      email: userEmail,
    },
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
    onError() {},
  });

  const isAdmin = data?.getAccessPermissions?.isAdmin;

  const hideActionButtonsForGroups =
    data?.getAccessPermissions?.permissionMeta?.hideActionButtonsForGroups;

  const groupIds = useMemo(
    () =>
      !isAdmin && hideActionButtonsForGroups?.length
        ? hideActionButtonsForGroups
        : [],
    [],
  );

  return groupIds;
};

export default useHideActionButtonGroupIds;
