# mkdir "./scripts/downloaded_schemas"

### SalesFerry AppSync ###

# dev - jbp2nq7janhwpcasd5iy6wawom
touch "./scripts/downloaded_schemas/schema.dev.graphql"
aws --profile "macantaProfile" --region "us-west-1" appsync get-introspection-schema --api-id "jbp2nq7janhwpcasd5iy6wawom" --format SDL "./scripts/downloaded_schemas/schema.dev.graphql"

# staging - 3is3ys3ekjbfhj4ldhpgfpho2i
touch "./scripts/downloaded_schemas/schema.staging.graphql"
aws --profile "macantaProfile" --region "us-west-1" appsync get-introspection-schema --api-id "3is3ys3ekjbfhj4ldhpgfpho2i" --format SDL "./scripts/downloaded_schemas/schema.staging.graphql"

# prod - khmo2katsfdm5m33x6yvdvmtrm
touch "./scripts/downloaded_schemas/schema.prod.graphql"
aws --profile "macantaProfile" --region "us-west-1" appsync get-introspection-schema --api-id "khmo2katsfdm5m33x6yvdvmtrm" --format SDL "./scripts/downloaded_schemas/schema.prod.graphql"
