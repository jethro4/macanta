import React, {useState} from 'react';
import Modal from '@macanta/components/Modal';
import FullscreenIcon from '@mui/icons-material/Fullscreen';
import Button from '@macanta/components/Button';

const ExpandBtn = ({title, content, ...props}) => {
  const [showModal, setShowModal] = useState(false);

  const handleShowModal = () => {
    setShowModal(true);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  return (
    <>
      <Button
        onClick={handleShowModal}
        size="small"
        variant="contained"
        startIcon={<FullscreenIcon />}
        {...props}>
        Expand
      </Button>
      <Modal
        headerTitle={title}
        open={showModal}
        onClose={handleCloseModal}
        contentWidth={1400}
        contentHeight={'96%'}
        bodyStyle={{
          backgroundColor: '#f5f6fa',
          alignItems: 'center',
        }}>
        {content}
      </Modal>
    </>
  );
};

export default ExpandBtn;
