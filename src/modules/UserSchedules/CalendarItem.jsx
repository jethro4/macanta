import React from 'react';
import Box from '@mui/material/Box';
import {
  getStatusColor,
  isWeekend,
  transformDateLabel,
} from '@macanta/modules/UserSchedules/helpers';
import useValue from '@macanta/hooks/base/useValue';
import * as Styled from './styles';
import {generateColorsByThemeBaseColor} from '@macanta/utils/colors';
import {isToday} from '@macanta/utils/time';

const CalendarItem = ({value, dateOfMonth, data, day, ...props}) => {
  const [{label}] = useValue(() => {
    return {
      label: transformDateLabel({
        value,
        dateOfMonth,
      }),
      today: isToday(value),
    };
  }, [value]);

  return (
    <Styled.CalendarItem
      {...props}
      sx={Object.assign(
        {},
        isWeekend(day) && {
          backgroundColor: 'grayLighter.main',
        },
        props.sx,
      )}>
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'column',
        }}>
        <Styled.CalendarItemHeader
          sx={Object.assign(
            {
              alignSelf: 'flex-start',
            },
            // today && {
            //   backgroundColor: 'primary.main',
            //   padding: '1px 4px',
            //   marginTop: '-2px',
            //   marginLeft: '-4px',
            //   borderRadius: '16px',
            //   color: 'common.white',
            // },
          )}>
          {label}
        </Styled.CalendarItemHeader>

        <Styled.CalendarData>
          {data.map((d) => {
            const statusColor = getStatusColor(d);
            const generatedColors = generateColorsByThemeBaseColor(statusColor);

            return (
              <Styled.CalendarDataItem
                key={d.id}
                sx={{
                  backgroundColor: generatedColors.tint2,
                }}>
                <Styled.CalendarDataTitle
                  sx={{
                    color: generatedColors.primary,
                  }}>
                  {d.title}
                </Styled.CalendarDataTitle>
              </Styled.CalendarDataItem>
            );
          })}
        </Styled.CalendarData>
      </Box>
    </Styled.CalendarItem>
  );
};

export default CalendarItem;
