import {experimentalStyled as styled} from '@mui/material/styles';

export const Root = styled('img')`
  text-align: center;
  max-width: 100%;
  margin: 0 auto;
`;
