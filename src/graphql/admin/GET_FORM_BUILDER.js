import {gql} from '@apollo/client';
import {FORM_BUILDER_FRAGMENT} from './fragments';

export default gql`
  query getFormBuilder($id: ID!) {
    getFormBuilder(id: $id) {
      ...formBuilderFragment
    }
  }
  ${FORM_BUILDER_FRAGMENT}
`;
