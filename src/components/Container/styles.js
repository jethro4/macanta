import {experimentalStyled as styled} from '@mui/material/styles';

import MUIContainer from '@mui/material/Container';

export const Root = styled(MUIContainer)`
  padding-left: 0px;
  padding-right: 0px;

  ${({theme}) => theme.breakpoints.up('sm')} {
    padding-left: ${({theme}) => theme.spacing(2)}!important;
    padding-right: ${({theme}) => theme.spacing(2)}!important;
  }
`;
