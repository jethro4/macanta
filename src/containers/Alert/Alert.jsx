import React, {useState, useEffect} from 'react';
import isString from 'lodash/isString';
import Portal from '@mui/material/Portal';
import * as Styled from './styles';

const Alert = ({
  open: openProp,
  message,
  onClose,
  snackbarProps,
  style,
  setRef,
  ...props
}) => {
  const [open, setOpen] = useState(openProp);

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen(false);
    onClose && onClose();
  };

  useEffect(() => {
    setOpen(openProp);
  }, [openProp]);

  return (
    <Portal>
      <Styled.Snackbar
        ref={setRef}
        open={open}
        autoHideDuration={4000}
        onClose={handleClose}
        {...snackbarProps}
        style={{
          ...snackbarProps?.style,
          ...(snackbarProps?.anchorOrigin?.horizontal === 'right' && {
            left: 'unset',
          }),
          ...((!snackbarProps?.anchorOrigin?.horizontal ||
            snackbarProps?.anchorOrigin?.horizontal === 'left') && {
            right: 'unset',
          }),
        }}>
        <Styled.Alert style={style} onClose={handleClose} {...props}>
          {isString(message) ? (
            <Styled.MessageContainer
              dangerouslySetInnerHTML={{__html: message}}
            />
          ) : (
            <Styled.MessageContainer>{message}</Styled.MessageContainer>
          )}
        </Styled.Alert>
      </Styled.Snackbar>
    </Portal>
  );
};

Alert.defaultProps = {
  open: false,
  severity: 'success',
};

export default Alert;
