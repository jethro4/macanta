import React from 'react';
import clsx from 'clsx';
import Box from '@mui/material/Box';
import EnhancedTableBodyCell from './EnhancedTableBodyCell';
import * as Styled from './styles';

const EnhancedTableBodyRow = ({
  item,
  columns,
  hasFrontActionButtons,
  renderFrontActionButtons,
  hasActionButtons,
  renderActionButtons,
  numOfTextLines,
  disableLineClamp,
  handlers,
  className,
  actionColumn,
  stickyFrontActionButtons,
  stickyActionButtons,
  ...props
}) => {
  return (
    <Styled.TableBodyRow
      className={clsx(className, 'item-data')}
      {...props}
      style={{
        ...props.style,
        ...item.style,
      }}>
      {hasFrontActionButtons && (
        <Styled.TableActionBodyCell
          style={Object.assign(
            {
              padding: 0,
              ...(stickyFrontActionButtons && {
                position: 'sticky',
                zIndex: '1',
                left: '-1px',
              }),
            },
            actionColumn.frontWidth && {
              width: actionColumn.frontWidth,
            },
          )}
          align="center"
          className="actionButtonCol">
          <Box
            style={{
              display: 'flex',
              justifyContent: 'center',
            }}>
            {renderFrontActionButtons({item, columns})}
          </Box>
        </Styled.TableActionBodyCell>
      )}
      {columns.map((column) => {
        return (
          <EnhancedTableBodyCell
            key={column.id}
            column={column}
            item={item}
            numOfTextLines={numOfTextLines}
            disableLineClamp={disableLineClamp}
            handlers={handlers}
          />
        );
      })}
      {hasActionButtons ? (
        <Styled.TableActionBodyCell
          style={Object.assign(
            {
              padding: 0,
              ...(stickyActionButtons && {
                position: 'sticky',
                zIndex: '1',
                right: '-1px',
              }),
            },
            actionColumn.backWidth && {
              width: actionColumn.backWidth,
            },
          )}
          align="center"
          className="actionButtonCol">
          <Box
            style={{
              display: 'flex',
              justifyContent: 'center',
            }}>
            {renderActionButtons({item, columns})}
          </Box>
        </Styled.TableActionBodyCell>
      ) : (
        <Styled.TableActionBodyCell
          style={{
            padding: 0,
          }}
        />
      )}
    </Styled.TableBodyRow>
  );
};

export default EnhancedTableBodyRow;
