import {experimentalStyled as styled} from '@mui/material/styles';

import * as CheckboxGroupStyled from '@macanta/components/Forms/ChoiceFields/CheckboxGroup/styles';

export const Checkbox = styled(CheckboxGroupStyled.Checkbox)`
  &.MuiCheckbox-root {
    margin-right: 10px;
  }

  .MuiSvgIcon-root {
    font-size: 18px;
  }
`;
