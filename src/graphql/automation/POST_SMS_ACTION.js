import {gql} from '@apollo/client';
import {SMS_ACTIONS_FRAGMENT} from '@macanta/graphql/automation/GET_SMS_ACTIONS';

export default gql`
  mutation postSmsAction($input: SmsActionInput!) {
    postSmsAction(input: $input) {
      ...smsActionsFragment
    }
  }
  ${SMS_ACTIONS_FRAGMENT}
`;
