<h3>Preview:</h3>

```jsx
import useQuery from '@macanta/hooks/apollo/useQuery';
import {GET_EMAIL_ACTIONS} from '@macanta/graphql/automation';
import useColumns from '@macanta/modules/AdminSettings/Automation/TriggerActions/useColumns';
import DataTable from '@macanta/containers/DataTable';

const query = useQuery(GET_EMAIL_ACTIONS, {
  variables: {
    id: '',
  },
});

const columns = useColumns();

<DataTable
  loading={query.loading}
  data={query.data}
  rowsPerPage={25}
  rowsPerPageOptions={[25, 50, 100]}
  columns={columns}
/>;
```
