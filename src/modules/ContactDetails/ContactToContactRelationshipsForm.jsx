import React, {useState, useMemo} from 'react';
import {useFormikContext} from 'formik';
import TurnedInIcon from '@mui/icons-material/TurnedIn';
import PersonRemoveIcon from '@mui/icons-material/PersonRemove';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import Button, {ButtonLink} from '@macanta/components/Button';
import Tooltip, {OverflowTip} from '@macanta/components/Tooltip';
import {SubSection} from '@macanta/containers/Section';
import FormField from '@macanta/containers/FormField';
import Form from '@macanta/components/Form';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import {useQuery, useMutation} from '@apollo/client';
import useDORelationships from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDORelationships';
import {
  LIST_CONTACT_RELATIONSHIPS,
  LIST_DIRECT_CONTACT_RELATIONSHIPS,
  CONNECT_CONTACT_DIRECT_RELATIONSHIP,
  DELETE_CONTACT_DIRECT_RELATIONSHIP,
} from '@macanta/graphql/contacts';
import {sortArrayByObjectKey} from '@macanta/utils/array';
import ConnectAnotherContactFormsBtn from '@macanta/modules/DataObjectItems/DOItemForms/Relationships/ConnectAnotherContactFormsBtn';
import Permission from '@macanta/modules/hoc/Permission';
import addDirectRelationshipValidationSchema from '@macanta/validations/addDirectRelationship';
import * as NoteTaskFormsStyled from '@macanta/modules/NoteTaskForms/styles';
import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import * as QueryBuilderStyled from '@macanta/modules/AdminSettings/QueryBuilder/styles';
import * as ListStyled from '@macanta/containers/List/styles';

const ColumnDividerContainer = ({width}) => (
  <Box
    style={{
      width: width || '1rem',
    }}
  />
);

const ContactToContactRelationshipsFormContainer = ({
  id: contactId,
  ...props
}) => {
  const {displayMessage} = useProgressAlert();

  const [saving, setSaving] = useState(false);

  const directContactRelationshipsQuery = useQuery(LIST_CONTACT_RELATIONSHIPS, {
    variables: {
      id: contactId,
      type: 'direct',
    },
  });
  const [callConnectContactDirectRelationship] = useMutation(
    CONNECT_CONTACT_DIRECT_RELATIONSHIP,
    {
      onError() {},
    },
  );
  const [callDeleteContactDirectRelationship] = useMutation(
    DELETE_CONTACT_DIRECT_RELATIONSHIP,
  );

  const handleFormSubmit = async (values, actions) => {
    const relationshipsObj = sortArrayByObjectKey(
      Object.values(values),
      'createdAt',
    ).reduce(
      (acc, item) => {
        const connectedRels = acc.connected[item.directRelationId] || [];
        const isDisconnect = !!values[item.contactId].disconnect;

        if (!isDisconnect) {
          acc.connected[item.directRelationId] = connectedRels.concat({
            contactId: item.contactId,
            relationId: item.relationships[0].relId,
          });
        } else {
          acc.disconnected = acc.disconnected.concat(item.contactId);
        }

        return acc;
      },
      {
        connected: {},
        disconnected: [],
      },
    );

    const itemsToAddOrEdit = Object.entries(relationshipsObj.connected);
    const itemsToDisconnect = relationshipsObj.disconnected;

    setSaving(true);

    try {
      await Promise.all(
        itemsToAddOrEdit.map(async ([directRelationId, relationshipItems]) => {
          await callConnectContactDirectRelationship({
            variables: {
              connectContactDirectRelationshipInput: {
                contactId,
                relationId: directRelationId,
                relationships: relationshipItems,
              },
              __mutationkey: 'connectContactDirectRelationshipInput',
              removeAppInfo: true,
            },
          });
        }),
      );

      await Promise.all(
        itemsToDisconnect.map(async (relatedContactId) => {
          await callDeleteContactDirectRelationship({
            variables: {
              deleteContactDirectRelationshipInput: {
                contactId,
                relatedContactId,
              },
              __mutationkey: 'deleteContactDirectRelationshipInput',
              removeAppInfo: true,
            },
          });
        }),
      );

      await directContactRelationshipsQuery.refetch();

      displayMessage('Saved successfully!');

      actions.resetForm({});
    } finally {
      setSaving(false);
    }
  };

  return (
    <>
      <Form initialValues={{}} onSubmit={handleFormSubmit}>
        <ContactToContactRelationshipsForm
          directContactRelationshipsQuery={directContactRelationshipsQuery}
          contactId={contactId}
          {...props}
        />
      </Form>
      <LoadingIndicator modal loading={saving} />
    </>
  );
};

const ContactToContactRelationshipsForm = ({
  directContactRelationshipsQuery,
  contactId,
  primaryContactName,
  ...props
}) => {
  const {
    values,
    errors,
    setFieldValue,
    dirty,
    handleSubmit,
  } = useFormikContext();

  const allRelationshipsQuery = useDORelationships(null, {
    fetchAll: true,
  });
  const allDirectContactRelationshipsQuery = useQuery(
    LIST_DIRECT_CONTACT_RELATIONSHIPS,
  );
  const indirectContactRelationshipsQuery = useQuery(
    LIST_CONTACT_RELATIONSHIPS,
    {
      variables: {
        id: contactId,
        type: 'indirect',
      },
    },
  );

  const listDirectContactRelationships =
    allDirectContactRelationshipsQuery.data?.listDirectContactRelationships;
  const listRelationships = allRelationshipsQuery.data?.listRelationships;
  const directContactRelationships =
    directContactRelationshipsQuery.data?.listContactRelationships;
  const indirectContactRelationships =
    indirectContactRelationshipsQuery.data?.listContactRelationships;

  const handleDirectRelationshipRenderValue = (val) => {
    const selected = allDirectContactRelationships?.find(
      ({value}) => value === val,
    );

    return selected?.label;
  };

  const handleIndirectRelationshipRenderValue = (val) => {
    const selected = val.reduce((acc, item) => {
      const relItem = allRelationships?.find(({value}) => value === item);

      if (relItem?.label) {
        acc.push(relItem.label);
      }

      return acc;
    }, []);

    return selected.join(', ');
  };

  const handleDisconnect = (item) => () => {
    if (
      directContactRelationships.some(
        (directItem) => directItem.contactId === item.contactId,
      )
    ) {
      setFieldValue(item.contactId, {
        ...item,
        disconnect: true,
      });
    }
  };

  const renderRelationshipForms = ({
    values: values2,
    errors: errors2,
    setRelationships,
    setCustomData,
  }) => {
    return (
      <Box>
        <FormField
          type="Select"
          options={allDirectContactRelationships}
          value={values2.directRelationId}
          error={errors2.directRelationId}
          onChange={(val) => {
            setCustomData('directRelationId', val);
          }}
          label={`${primaryContactName}'s Relationship`}
          fullWidth
          size="small"
          variant="outlined"
          renderValue={handleDirectRelationshipRenderValue}
        />
        <ColumnDividerContainer />
        <FormField
          type="Select"
          options={allDirectContactRelationships}
          value={values2.relationships[0]}
          error={errors2.relationships}
          onChange={(val) => {
            setRelationships([val]);
          }}
          label="Selected Contact's Relationship"
          size="small"
          variant="outlined"
          renderValue={handleDirectRelationshipRenderValue}
        />
      </Box>
    );
  };

  const allRelationships = useMemo(() => {
    if (listRelationships) {
      return listRelationships.map((r) => ({
        label: r.role,
        value: r.id,
      }));
    }

    return [];
  }, [listRelationships]);

  const allDirectContactRelationships = useMemo(() => {
    if (listDirectContactRelationships) {
      return sortArrayByObjectKey(
        listDirectContactRelationships.map((r) => ({
          label: r.name,
          value: r.id,
        })),
        'label',
      );
    }

    return [];
  }, [listDirectContactRelationships]);

  const transformedIndirectContactRelationships = useMemo(() => {
    if (indirectContactRelationships) {
      return indirectContactRelationships.map((r) => ({
        ...r,
        transformedRelationships: r.relationships.map((rel) => rel.relId),
      }));
    }

    return [];
  }, [indirectContactRelationships]);

  const directContactValueItems = sortArrayByObjectKey(
    Object.values(values),
    'createdAt',
  ).reduce((acc, item) => {
    if (!acc.some((directItem) => directItem.contactId === item.contactId)) {
      return acc.concat(item);
    }

    if (item.disconnect) {
      return acc.filter(
        (directItem) => directItem.contactId !== item.contactId,
      );
    }

    return acc.map((directItem) => {
      if (directItem.contactId === item.contactId) {
        return item;
      }

      return directItem;
    });
  }, directContactRelationships || []);

  const directContactValueKeys = [contactId].concat(
    directContactValueItems.map((item) => item.contactId),
  );

  return (
    <QueryBuilderStyled.FormRoot {...props}>
      <NoteTaskHistoryStyled.FullHeightGrid container>
        <NoteTaskHistoryStyled.FullHeightFlexGridColumn item xs={12} md={7}>
          <QueryBuilderStyled.SubBody
            style={{
              flex: 1,
            }}>
            <SubSection
              fullHeight
              loading={directContactRelationshipsQuery.loading}
              headerStyle={{
                backgroundColor: 'white',
                minHeight: '3rem',
              }}
              bodyStyle={{
                padding: '1rem',
              }}
              style={{
                marginTop: 0,
              }}
              title="Direct Relationships"
              HeaderRightComp={
                <Permission keys={['allowAddDirectRelationship']}>
                  <ConnectAnotherContactFormsBtn
                    loading={directContactRelationshipsQuery.loading}
                    headerTitle="Add Direct Relationship"
                    btnLabel="Add Direct Relationship"
                    submitBtnLabel="Add Relationship"
                    hideRelationshipsError
                    schema={addDirectRelationshipValidationSchema}
                    selectedContactIds={directContactValueKeys}
                    renderRelationshipForms={renderRelationshipForms}
                    onSuccess={(contact, relationships, {directRelationId}) => {
                      const valItem = allDirectContactRelationships.find(
                        (directRel) => directRel.value === relationships[0],
                      );

                      setFieldValue(contact.id, {
                        contactId: contact.id,
                        name: `${contact.firstName} ${contact.lastName}`.trim(),
                        email: contact.email,
                        relationships: [
                          {
                            relId: valItem.value,
                            name: valItem.label,
                          },
                        ],
                        directRelationId,
                        createdAt: Date.now(),
                      });
                    }}
                  />
                </Permission>
              }
              FooterComp={
                <NoteTaskFormsStyled.Footer
                  style={{
                    width: '100%',
                    padding: '1rem',
                    margin: 0,
                    borderTop: '1px solid #eee',
                  }}>
                  <Button
                    disabled={!dirty}
                    variant="contained"
                    startIcon={<TurnedInIcon />}
                    onClick={handleSubmit}
                    size="medium">
                    Save Changes
                  </Button>
                </NoteTaskFormsStyled.Footer>
              }>
              <>
                {!directContactRelationshipsQuery.loading &&
                  directContactValueItems?.length === 0 && (
                    <ListStyled.EmptyMessage align="center" color="#888">
                      No direct contacts
                    </ListStyled.EmptyMessage>
                  )}
                {directContactValueItems?.map((item) => {
                  return (
                    <Box
                      key={item.id}
                      style={{
                        display: 'flex',
                        alignItems: 'center',
                        marginBottom: '1.5rem',
                      }}>
                      <Box
                        style={{
                          width: '46%',
                          paddingRight: '1rem',
                        }}>
                        <ButtonLink
                          to={`/app/contact/${item.contactId}/notes`}
                          style={{
                            color: '#2196f3',
                            padding: 0,
                          }}>
                          <OverflowTip>{item.name}</OverflowTip>
                        </ButtonLink>
                        <OverflowTip
                          variant="caption"
                          style={{display: 'block', color: '#aaa'}}>
                          {item.email}
                        </OverflowTip>
                      </Box>
                      <FormField
                        style={{
                          width: '54%',
                          marginBottom: 0,
                        }}
                        type="Select"
                        options={allDirectContactRelationships}
                        value={item.relationships[0].relId}
                        error={errors[item.contactId]}
                        onChange={(val) => {
                          const valItem = allDirectContactRelationships.find(
                            (directRel) => directRel.value === val,
                          );

                          setFieldValue(item.contactId, {
                            ...item,
                            relationships: [
                              {
                                relId: valItem.value,
                                name: valItem.label,
                              },
                            ],
                            createdAt: Date.now(),
                          });
                        }}
                        placeholder="Select Relationship..."
                        fullWidth
                        size="small"
                        variant="outlined"
                        renderValue={handleDirectRelationshipRenderValue}
                      />
                      <ColumnDividerContainer width="0.5rem" />
                      <Tooltip title="Disconnect">
                        <div>
                          <QueryBuilderStyled.ConditionIconButton
                            onClick={handleDisconnect(item)}>
                            <PersonRemoveIcon />
                          </QueryBuilderStyled.ConditionIconButton>
                        </div>
                      </Tooltip>
                    </Box>
                  );
                })}
              </>
            </SubSection>
          </QueryBuilderStyled.SubBody>
        </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
        <NoteTaskHistoryStyled.FullHeightFlexGridColumn item xs={12} md={5}>
          <QueryBuilderStyled.SubBody
            style={{
              height: '100%',
            }}>
            <SubSection
              loading={
                allRelationshipsQuery.loading ||
                indirectContactRelationshipsQuery.loading
              }
              headerStyle={{
                backgroundColor: 'white',
                minHeight: '3rem',
              }}
              bodyContainerStyle={{
                overflowY: 'auto',
              }}
              bodyStyle={{
                padding: '1rem',
                minHeight: '8rem',
              }}
              style={{
                marginTop: 0,
                maxHeight: '100%',
              }}
              title="Indirect Relationships">
              {!indirectContactRelationshipsQuery.loading &&
                transformedIndirectContactRelationships.length === 0 && (
                  <ListStyled.EmptyMessage align="center" color="#888">
                    No indirect contacts
                  </ListStyled.EmptyMessage>
                )}
              {transformedIndirectContactRelationships.map((item) => {
                return (
                  <FormField
                    key={item.id}
                    type="MultiSelect"
                    options={allRelationships}
                    value={item.transformedRelationships}
                    label={
                      <>
                        <ButtonLink
                          to={`/app/contact/${item.contactId}/notes`}
                          style={{
                            color: '#2196f3',
                            padding: 0,
                          }}>
                          {item.name}
                        </ButtonLink>
                        <Typography
                          variant="caption"
                          style={{display: 'block', color: '#aaa'}}>
                          {item.email}
                        </Typography>
                      </>
                    }
                    fullWidth
                    size="small"
                    readOnly
                    renderValue={handleIndirectRelationshipRenderValue}
                  />
                );
              })}
            </SubSection>
          </QueryBuilderStyled.SubBody>
        </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
      </NoteTaskHistoryStyled.FullHeightGrid>
    </QueryBuilderStyled.FormRoot>
  );
};

export default ContactToContactRelationshipsFormContainer;
