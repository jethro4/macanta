Preview:

```jsx
import React from 'react';
import FormField from '@macanta/containers/FormField';

<FormField
  type="Number"
  variant="outlined"
  label="Sample label"
  value="100"
  onChange={(value) => {
    console.info('Value has changed:', value);
  }}
  size="small"
/>;
```
