import React from 'react';
import Divider from '@mui/material/Divider';
import FormField from '@macanta/containers/FormField';
import useValue from '@macanta/hooks/base/useValue';
import useUsers from '@macanta/hooks/admin/useUsers';
import {
  sortArrayByObjectKey,
  arrayConcatOrUpdateByKey,
} from '@macanta/utils/array';
import BodySection from '@macanta/modules/AdminSettings/UserManagement/BodySection';
import {transformUser} from '@macanta/selectors/user.selector';
import {getFullName} from '@macanta/selectors/contact.selector';

const MAX_COLUMNS = 3;
const DEFAULT_MIN_WIDTH = `${100 / MAX_COLUMNS - 1}%`;
const DEFAULT_TASKS = [
  {
    taskId: 'OwnTask',
    allow: true,
  },
  {
    taskId: 'AllUserTask',
    allow: false,
  },
  {
    taskId: 'SpecificUserTask',
    allow: false,
    specificIds: [],
  },
];
const DEFAULT_OPTIONS = [{value: 'own', label: 'Own Tasks'}];
const OTHER_OPTIONS = [
  {value: 'none', label: 'None'},
  {value: 'everyone', label: 'Everyone'},
  {value: 'specific', label: 'Specific Users'},
];

const getOptionLabel = (option) => {
  const userInstance = transformUser(option);

  return getFullName(userInstance);
};

const TaskAccessForm = ({permission, onChange, ...props}) => {
  const usersQuery = useUsers();

  const users = usersQuery.data;
  const tasks = permission?.tasks?.length ? permission.tasks : DEFAULT_TASKS;

  const [sortedUsers] = useValue(
    () => sortArrayByObjectKey(users, 'firstName'),
    [users],
  );
  const [{otherStatus, specificIds}] = useValue(() => {
    const taskPermission = tasks?.find(
      (item) => item.taskId !== 'OwnTask' && item.allow,
    );
    const obj = {
      otherStatus: 'none',
      specificIds: [],
    };

    if (taskPermission?.taskId === 'AllUserTask') {
      obj.otherStatus = 'everyone';
    } else if (taskPermission?.taskId === 'SpecificUserTask') {
      obj.otherStatus = 'specific';
      obj.specificIds = taskPermission.specificIds;
    }

    return obj;
  }, [permission]);

  const handleChangeAccess = (value) => {
    let selectedTaskId;

    if (value === 'everyone') {
      selectedTaskId = 'AllUserTask';
    } else if (value === 'specific') {
      selectedTaskId = 'SpecificUserTask';
    }

    const updatedPermission = {
      ...permission,
      tasks: tasks.map((task) => {
        return {
          ...task,
          allow: task.taskId === 'OwnTask' || selectedTaskId === task.taskId,
        };
      }),
    };

    onChange(updatedPermission);
  };

  const handleSpecificUsersAccess = (values) => {
    const updatedPermission = {
      ...permission,
      tasks: arrayConcatOrUpdateByKey({
        arr: tasks,
        item: {
          taskId: 'SpecificUserTask',
          specificIds: values,
        },
        key: 'taskId',
        includeAll: true,
      }),
    };

    onChange(updatedPermission);
  };

  return (
    <BodySection
      loading={usersQuery.loading}
      title="Tasks"
      bodyContainerStyle={{
        overflowY: 'auto',
      }}
      bodyStyle={{
        padding: '1rem',
      }}
      {...props}>
      <FormField
        style={{
          marginBottom: 0,
        }}
        disabled
        sx={{
          '& .MuiFormControlLabel-root': {
            minWidth: DEFAULT_MIN_WIDTH,
          },
        }}
        type="Checkbox"
        options={DEFAULT_OPTIONS}
        value={[DEFAULT_OPTIONS[0].value]}
      />

      <Divider
        style={{
          margin: '1rem 0',
          borderBottom: '1px dashed #eee',
        }}
      />

      <FormField
        style={{
          marginBottom: 0,
        }}
        sx={{
          '& .MuiFormControlLabel-root': {
            minWidth: DEFAULT_MIN_WIDTH,
          },
        }}
        type="Radio"
        options={OTHER_OPTIONS}
        label="Others' Tasks"
        value={otherStatus}
        onChange={handleChangeAccess}
      />

      {otherStatus === 'specific' && (
        <>
          <Divider
            style={{
              margin: '1rem 0',
              borderBottom: '1px dashed #eee',
            }}
          />

          <FormField
            style={{
              marginBottom: 0,
            }}
            sx={{
              '& .MuiFormControlLabel-root': {
                minWidth: DEFAULT_MIN_WIDTH,
                height: '40px',
              },
            }}
            type="Checkbox"
            row
            options={sortedUsers}
            value={specificIds}
            getOptionLabel={getOptionLabel}
            valueKey="id"
            // label="Specific User's Task"
            onChange={handleSpecificUsersAccess}
          />
        </>
      )}
    </BodySection>
  );
};

export default TaskAccessForm;
