import {experimentalStyled as styled} from '@mui/material/styles';

import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Paper from '@macanta/components/Paper';
import {OverflowTip} from '@macanta/components/Tooltip';

export const Root = styled(Box)`
  display: flex;
  flex-wrap: wrap;
`;

export const Item = styled(Box)`
  display: flex;
  flex-direction: column;
`;

export const EmptyMessage = styled(Typography)`
  padding-top: ${({theme}) => theme.spacing(4)};
`;

export const ItemContainer = styled(Paper)`
  margin: 8px 0px 0px;
  padding: 0;
  border-radius: 0px;
  box-shadow: none;
  border-bottom: 1px solid #e4e4e4;
`;

export const ItemHeader = styled(Typography)`
  padding: 0.75rem 1rem 0.875rem;
  color: ${({theme}) => theme.palette.text.gray};
  font-size: 0.875rem;
  font-weight: bold;
`;

export const ItemBody = styled(Box)`
  padding: 0 1rem 1.25rem;

  & > * {
    &:not(:last-child) {
      padding-bottom: 1rem;
      border-bottom: 1px solid #e4e4e4;
    }

    &:not(:first-child) {
      margin-top: 1rem;
    }
  }
`;

export const ItemBodyContainer = styled(Box)`
  display: flex;
`;

export const ItemBodyDataContainer = styled(Box)`
  display: flex;

  & > *:not(:last-child) {
    margin-right: 1.5rem;
  }
`;

export const ItemId = styled(Typography)`
  margin-bottom: 10px;
  text-align: left;
  color: ${({theme}) => theme.palette.text.secondary};
  font-size: 0.875rem;
  line-height: 0.875rem;
`;

export const FieldContainer = styled(Box)`
  min-width: 100px;
  flex-basis: 100%;
  overflow-x: hidden;
  display: flex;
  flex-direction: column;
`;

export const FieldName = styled(OverflowTip)`
  margin-bottom: 4px;
  font-size: 0.8125rem;
  font-weight: 700;
  color: ${({theme}) => `${theme.palette.text.blue}`};
`;

export const FieldValue = styled(OverflowTip)`
  font-size: 0.8125rem;
  line-height: 0.875rem;
`;
