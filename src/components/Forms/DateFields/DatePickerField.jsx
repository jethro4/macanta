import React, {useState} from 'react';
import AdapterDateFns from '@mui/x-date-pickers/AdapterDateFns';
import LocalizationProvider from '@mui/x-date-pickers/LocalizationProvider';
import DatePicker from '@mui/x-date-pickers/DatePicker';
import {useTheme} from '@mui/material/styles';
import useValue from '@macanta/hooks/base/useValue';
import TextField from '@macanta/components/Forms/InputFields/TextField';
import {
  convertToDate,
  formatDateToLocaleString,
} from '@macanta/components/Forms/InputFields/helpers';

const DATE_INPUT_FORMAT = 'yyyy-MM-dd';
const DATE_INPUT_MASK = '____-__-__';
const DATE_OUTPUT_FORMAT = 'YYYY-MM-DD';

const DatePickerField = ({
  DatePickerProps,
  value,
  onChange,
  onClick,
  ...props
}) => {
  const theme = useTheme();

  const [selectedDate, setSelectedDate] = useValue(
    () => convertToDate(value, DATE_OUTPUT_FORMAT),
    [value],
  );
  const [openPicker, setOpenPicker] = useState(false);

  const handleDateChange = (newDate) => {
    setSelectedDate(newDate);
    onChange(formatDateToLocaleString(newDate, DATE_OUTPUT_FORMAT));
  };

  const handleOpenPicker = () => {
    setOpenPicker(true);
  };

  const handleClosePicker = () => {
    setOpenPicker(false);
  };

  const handleClick = (event) => {
    onClick && onClick(event);
  };

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <DatePicker
        value={selectedDate}
        open={openPicker}
        onChange={handleDateChange}
        onOpen={handleOpenPicker}
        onClose={handleClosePicker}
        renderInput={(params) => (
          <TextField
            {...params}
            {...props}
            onClick={handleClick}
            helperText=""
            value={selectedDate}
          />
        )}
        OpenPickerButtonProps={{
          style: {
            color: theme.palette.secondary.main,
            padding: 6,
            marginRight: -6,
          },
        }}
        {...DatePickerProps}
        inputFormat={DATE_INPUT_FORMAT}
        mask={DATE_INPUT_MASK}
      />
    </LocalizationProvider>
  );
};

export default DatePickerField;
