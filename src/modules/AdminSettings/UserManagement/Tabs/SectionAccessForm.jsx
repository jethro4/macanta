import React, {useState, useEffect} from 'react';
import ButtonGroup from '@macanta/components/ButtonGroup';
import Button from '@macanta/components/Button';
import Switch from '@macanta/components/Forms/ChoiceFields/Switch';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import useSectionWithPermission from '@macanta/hooks/dataObject/useSectionsWithPermission';
import BodySection from '@macanta/modules/AdminSettings/UserManagement/BodySection';
import PermissionsTable from '@macanta/modules/AdminSettings/UserManagement/Tabs/PermissionsTable';
import {traverseRead, arrayConcatOrUpdateByKey} from '@macanta/utils/array';
import {ACCESS_LEVELS} from '@macanta/constants/accessLevels';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {
  AccessRadioGroup,
  AccessTableHeadCell,
} from '@macanta/modules/AdminSettings/UserManagement/Tabs/AccessPermissions';

const SECTION_ACCESS_COLUMNS = [
  {
    id: 'hasAccess',
    label: '',
    disableSort: true,
    disableResize: true,
    minWidth: 80,
    maxWidth: 80,
    renderBodyCellValue: ({item, value, handlers}) => {
      return (
        <Switch
          color="success"
          checked={value}
          onChange={(event) => {
            handlers.onChangeAccess({
              item,
              value: event.target.checked
                ? ACCESS_LEVELS.READ_ONLY
                : ACCESS_LEVELS.NO_ACCESS,
            });
          }}
        />
      );
    },
  },
  {
    id: 'sectionName',
    label: 'Sections',
    maxWidth: '100%',
    getBodyCellStyle: ({item}) =>
      !item.hasAccess && {
        color: 'grayDarkest.main',
      },
  },
  {
    id: 'permission',
    label: '',
    disableSort: true,
    disableResize: true,
    minWidth: 200,
    maxWidth: 200,
    renderHeadCellValue: () => {
      return <AccessTableHeadCell />;
    },
    renderBodyCellValue: ({item, value, handlers}) => {
      return (
        item.hasAccess && (
          <AccessRadioGroup
            value={value}
            onChange={(val) => {
              handlers.onChangeAccess({item, value: val});
            }}
          />
        )
      );
    },
  },
];

const getKeyExtractor = (item) => {
  return item.sectionName;
};

const SectionAccessForm = ({permission, onChange, ...props}) => {
  const [selectedDOType, setSelectedDOType] = useState(null);

  const doTypesQuery = useDOTypes({
    removeEmail: true,
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });
  const sectionsQuery = useSectionWithPermission(
    {
      groupId: selectedDOType?.id,
      permission,
      isDataObjectType: true,
    },
    {
      fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
    },
  );

  const doTypes = doTypesQuery.data?.listDataObjectTypes;

  const handleSelectSectionData = (doType) => () => {
    setSelectedDOType(doType);
  };

  const handleChangeAccess = ({item, value}) => {
    const currentAccess = traverseRead(permission, [
      'sections',
      `groupId:${selectedDOType?.id}`,
      'access',
    ]);

    const updatedAccess = arrayConcatOrUpdateByKey({
      arr: currentAccess,
      item: {
        name: item.sectionName,
        permission: value,
      },
      key: 'name',
      includeAll: true,
    });

    const updatedPermission = {
      ...permission,
      sections: arrayConcatOrUpdateByKey({
        arr: permission.sections,
        item: {
          groupId: selectedDOType?.id,
          access: updatedAccess,
        },
        key: 'groupId',
        includeAll: true,
      }),
    };

    onChange(updatedPermission);
  };

  useEffect(() => {
    if (!selectedDOType && doTypes?.length) {
      setSelectedDOType(doTypes[0]);
    }
  }, [doTypes]);

  return (
    <BodySection {...props}>
      <ButtonGroup
        increaseBtnMinWidth
        sx={{
          '& .MuiButton-root': {
            position: 'relative',

            '&:after': {
              content: '""',
              position: 'absolute',
              width: '0.5px',
              right: '-0.5px',
              top: 0,
              bottom: 0,
              backgroundColor: 'primary.main',
              borderRadius: '4px',
            },
          },
        }}
        style={{
          flexWrap: 'wrap',
          margin: '1rem',
        }}
        size="small"
        variant="outlined"
        aria-label="primary button group">
        {doTypes?.map((doType) => {
          return (
            <Button key={doType.id} onClick={handleSelectSectionData(doType)}>
              {doType.title}
            </Button>
          );
        })}
      </ButtonGroup>

      <PermissionsTable
        style={{
          flex: 1,
        }}
        keyExtractor={getKeyExtractor}
        loading={sectionsQuery.loading}
        columns={SECTION_ACCESS_COLUMNS}
        data={sectionsQuery.data}
        handlers={{
          onChangeAccess: handleChangeAccess,
        }}
      />
    </BodySection>
  );
};

export default SectionAccessForm;
