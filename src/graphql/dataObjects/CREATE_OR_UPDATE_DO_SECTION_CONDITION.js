import {gql} from '@apollo/client';
import {DO_CONDITION_FRAGMENT} from './fragments';

export default gql`
  mutation createOrUpdateDOSectionCondition(
    $input: CreateOrUpdateDOSectionConditionInput!
  ) {
    createOrUpdateDOSectionCondition(input: $input) {
      ...doSectionConditionItemResults
    }
  }
  ${DO_CONDITION_FRAGMENT}
`;
