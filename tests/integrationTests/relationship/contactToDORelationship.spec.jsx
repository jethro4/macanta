import React from 'react';
import {Router, navigate} from '@reach/router';
import {graphql} from 'msw';
import {
  renderApp,
  overrideHandlers,
} from '@tests/jestSettings/utils/renderUtils';
import {click, toggleSelect} from '@tests/jestSettings/utils/eventUtils';
import {
  selectSelect,
  selectButton,
  waitUntil,
  waitApis,
} from '@tests/jestSettings/utils/selectorUtils';
import RelationshipsDOItemOption from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/RelationshipsDOItemOption';
import doItemFactory from '@tests/jestSettings/mockFactory/doItemFactory';

import * as Storage from '@macanta/utils/storage';
import {
  handleMockDOFields,
  handleMockUsers,
} from '@tests/integrationTests/mockUtils';
import doRelationshipFactory from '@tests/jestSettings/mockFactory/doRelationshipFactory';

describe('Contact To DO Relationship', () => {
  let doItem;

  beforeAll(() => {
    Storage.setItem('userDetails', {
      userId: 'Contact1',
      email: 'test1@macantacrm.com',
    });
  });

  beforeEach(() => {
    // jest.mock('@reach/router', () => ({
    //   ...jest.requireActual('@reach/router'),
    //   useLocation: jest.fn().mockImplementation(() => {
    //     return {
    //       pathname: encodeURI(
    //         `/app/contact/Contact1/data-object/DataObject1/DO Type 1`,
    //       ),
    //     };
    //   }),
    // }));

    handleMockUsers();
    handleMockDOFields();

    doItem = doItemFactory();

    overrideHandlers([
      graphql.query('getDataObject', (req, res, ctx) => {
        return res(
          ctx.data({
            getDataObject: doItem,
          }),
        );
      }),
    ]);
  });

  it('display connected contact relationships', async () => {
    navigate(
      encodeURI(`/app/contact/Contact1/data-object/DataObject1/DO Type 1`),
    );

    renderApp(
      <Router>
        <RelationshipsDOItemOption
          path={`/app/contact/:contactId/data-object/:groupId/:doTitle`}
          itemId="ItemDataObject1"
          relationships={[doRelationshipFactory(), doRelationshipFactory()]}
        />
      </Router>,
    );

    await waitApis('getDataObject');

    const primaryRelationshipSelect = selectSelect({
      order: 1,
      testId: 'DORelationshipsForm',
    });
    const otherRelationshipSelect = selectSelect({
      order: 2,
      testId: 'DORelationshipsForm',
    });

    await waitUntil(() =>
      expect(primaryRelationshipSelect).toHaveValue(
        'Relationship 1,Relationship 2',
      ),
    );

    await waitUntil(() =>
      expect(otherRelationshipSelect).toHaveValue('Relationship 1'),
    );
  });

  it('Connect contact', async () => {
    const submitInterceptor = jest.fn();

    renderApp(
      <Router>
        <RelationshipsDOItemOption
          path={`/app/contact/:contactId/data-object/:groupId/:doTitle`}
          itemId="ItemDataObject1"
          relationships={[doRelationshipFactory(), doRelationshipFactory()]}
          onSave={submitInterceptor}
        />
      </Router>,
    );

    const submit = selectButton('Save Changes');

    expect(submit).toBeDisabled();

    await waitApis('getDataObject');

    const primaryRelationshipSelect = selectSelect({
      order: 1,
      testId: 'DORelationshipsForm',
    });
    const otherRelationshipSelect = selectSelect({
      order: 2,
      testId: 'DORelationshipsForm',
    });

    await toggleSelect(primaryRelationshipSelect, ['Relationship 1']);
    await toggleSelect(otherRelationshipSelect, ['Relationship 2']);

    await waitUntil(() => expect(submit).toBeEnabled());

    await waitUntil(() =>
      expect(primaryRelationshipSelect).toHaveValue('Relationship 2'),
    );
    await waitUntil(() =>
      expect(otherRelationshipSelect).toHaveValue(
        'Relationship 1,Relationship 2',
      ),
    );

    click(submit);

    await waitUntil(() => expect(submitInterceptor).toHaveBeenCalledTimes(1));

    await waitUntil(() =>
      expect(submitInterceptor).toHaveBeenCalledWith({
        relationships: [
          {
            connectedContactId: 'Contact1',
            email: 'test1@macantacrm.com',
            firstName: 'FirstName1',
            lastName: 'LastName1',
            relationships: ['Relationship 2'],
          },
          {
            connectedContactId: 'Contact2',
            email: 'test2@macantacrm.com',
            firstName: 'FirstName2',
            lastName: 'LastName2',
            relationships: ['Relationship 1', 'Relationship 2'],
          },
        ],
      }),
    );
  });
});
