import {gql} from '@apollo/client';

export const WEBHOOK_ACTIONS_FRAGMENT = gql`
  fragment webhookActionsFragment on WebhookActionItems {
    queryId
    queryType
    queryName
    queryDescription
    queryConnectedDataType
    queryPostURL
    queryContact {
      queryContactRelationshipFieldLogic
      queryContactRelationship
    }
    queryCDField {
      queryCDFieldLogic
      queryCDFieldName
      queryCDFieldOperator
      queryCDFieldValue
      queryCDFieldValues
    }
    queryCustomField {
      queryCDFieldName
      queryCDFieldValue
    }
  }
`;

export default gql`
  query getWebhookActions($id: ID) {
    getWebhookActions(id: $id) {
      action
      status
      count
      items {
        ...webhookActionsFragment
      }
    }
  }
  ${WEBHOOK_ACTIONS_FRAGMENT}
`;
