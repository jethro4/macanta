import {gql} from '@apollo/client';
import {DO_CONDITION_FRAGMENT} from './fragments';

export default gql`
  query listDOSectionConditions(
    $appName: String!
    $apiKey: String!
    $groupId: ID!
  ) {
    listDOSectionConditions(
      appName: $appName
      apiKey: $apiKey
      groupId: $groupId
    ) {
      ...doSectionConditionItemResults
    }
  }
  ${DO_CONDITION_FRAGMENT}
`;
