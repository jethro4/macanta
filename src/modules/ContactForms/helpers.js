export const getAddressComponents = (addressComponents) => {
  const getComp = (type) => {
    const component = addressComponents?.find((item) => {
      return item.types.includes(type);
    });

    return component?.long_name || '';
  };

  let country = getComp('country');

  const address1 = `${getComp('street_number')} ${getComp('route')}`.trim();
  const address2 =
    country === 'United Kingdom'
      ? getComp('locality')
      : getComp('neighborhood');
  const city =
    country === 'United Kingdom' ? getComp('postal_town') : getComp('locality');
  const state =
    country === 'United Kingdom'
      ? getComp('administrative_area_level_2')
      : getComp('administrative_area_level_1');
  const postalCode = getComp('postal_code');
  country =
    country === 'United Kingdom'
      ? getComp('administrative_area_level_1')
      : country;

  return [address1, address2, city, state, postalCode, country];
};
