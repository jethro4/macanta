import {experimentalStyled as styled} from '@mui/material/styles';

import MUIList from '@mui/material/List';

export const Root = styled(MUIList)`
  display: flex;
  flex-direction: column;
`;
