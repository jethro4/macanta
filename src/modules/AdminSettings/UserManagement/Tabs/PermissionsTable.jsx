import React from 'react';
import DataTable from '@macanta/containers/DataTable';
import {ACCESS_LEVELS} from '@macanta/constants/accessLevels';

const PermissionsTable = (props) => {
  const accessToggle = props.columns?.find(
    (column) => column.id === 'hasAccess',
  );

  return (
    <DataTable
      fullHeight
      highlightSelected={false}
      headRowHeight={50}
      bodyRowHeight={50}
      noOddRowStripes
      noLimit
      hidePagination
      rowsPerPage={25}
      rowsPerPageOptions={[25, 50, 100]}
      {...(accessToggle && {
        selectable: true,
        isSelectable: (item) =>
          accessToggle.isSelectable
            ? accessToggle.isSelectable(item)
            : !item.hasAccess,
        onSelectItem: (item) => {
          props.handlers.onChangeAccess({item, value: ACCESS_LEVELS.READ_ONLY});
        },
      })}
      {...props}
    />
  );
};

export default PermissionsTable;
