import * as yup from 'yup';

const schema = yup.object().shape({
  name: yup.string().required('Required'),
  relationships: yup
    .array()
    .required('Must select at least one relationship')
    .min(1, 'Must select at least one relationship'),
  sectionsData: yup
    .array()
    .required('Must have at least one section')
    .min(1, 'Must have at least one section'),
});

export default schema;
