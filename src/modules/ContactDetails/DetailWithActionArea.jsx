import React from 'react';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import DetailInfo from './DetailInfo';
import * as Styled from './styles';

const DetailWithActionArea = ({
  label,
  value,
  noMargin,
  ButtonComp,
  leftItemCol,
  rightItemCol,
  disableGrid,
}) =>
  !disableGrid ? (
    <Grid container>
      <Grid item xs={leftItemCol}>
        <DetailInfo label={label} value={value} noMargin={noMargin} />
      </Grid>
      <Grid
        item
        xs={rightItemCol}
        style={{
          position: 'relative',
        }}>
        <Box
          style={{
            position: 'absolute',
            top: '50%',
            transform: 'translateY(-50%)',
            ...(!noMargin && {paddingBottom: '0.5rem'}),
          }}>
          {ButtonComp}
        </Box>
      </Grid>
    </Grid>
  ) : (
    <Styled.ContactHeaderInfoContainer>
      <DetailInfo
        style={{
          marginRight: '1.5rem',
        }}
        label={label}
        value={value}
        noMargin={noMargin}
      />
      {ButtonComp}
    </Styled.ContactHeaderInfoContainer>
  );

DetailWithActionArea.defaultProps = {
  leftItemCol: 6.5,
  rightItemCol: 5.5,
};

export default DetailWithActionArea;
