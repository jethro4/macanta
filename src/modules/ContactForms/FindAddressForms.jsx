import React, {useState} from 'react';
import {useFormikContext} from 'formik';
import Box from '@mui/material/Box';
import TextField from '@macanta/components/Forms';
import List from '@mui/material/List';
import Checkbox from '@mui/material/Checkbox';
import InputAdornment from '@mui/material/InputAdornment';
import SearchIcon from '@mui/icons-material/Search';
import RoomIcon from '@mui/icons-material/Room';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import {getAppInfo} from '@macanta/utils/app';
import {billingAddressKeys, shippingAddressKeys} from './FormsGeneral';
import {getAddressComponents} from './helpers';
import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import * as FormStyled from '@macanta/modules/NoteTaskForms/styles';
import * as ListStyled from '@macanta/containers/List/styles';
import * as Styled from './styles';
import envConfig from '@macanta/config/envConfig';

const FindAddressForms = ({type, onSelect, ...props}) => {
  const {values, setValues} = useFormikContext();

  const [loadingPredictions, setLoadingPredictions] = useState(false);
  const [loadingPlace, setLoadingPlace] = useState(false);
  const [searchValue, setSearchValue] = useState('');
  const [selectedPlaceId, setSelectedPlaceId] = useState('');
  const [isCopyTo, setIsCopyTo] = useState(false);
  const [predictions, setPredictions] = useState([]);

  const handleSearchAddress = async (val) => {
    const [appName, apiKey] = getAppInfo();

    setSearchValue(val);

    if (val.trim().length > 1) {
      setLoadingPredictions(true);

      const predictionsUrl = `https://${appName}.${envConfig.apiDomain}/rest/v1/autocomplete_address?q=${val}&api_key=${apiKey}`;

      const res = await fetch(predictionsUrl);
      const response = await res.json();

      setPredictions(response?.predictions || []);
      setLoadingPredictions(false);
    } else {
      setPredictions([]);
    }
  };

  const handleSelectAddress = (placeId) => () => {
    setSelectedPlaceId(placeId);
  };

  const handleUse = async () => {
    setLoadingPlace(true);

    const [appName, apiKey] = getAppInfo();

    const placeDetailsUrl = `https://${appName}.${envConfig.apiDomain}/rest/v1/autocomplete_address?placeId=${selectedPlaceId}&api_key=${apiKey}`;

    const res = await fetch(placeDetailsUrl);
    const response = await res.json();

    const addressComponents = getAddressComponents(
      response?.result?.address_components,
    );

    const addressValues = {};
    const keys = type === 'billing' ? billingAddressKeys : shippingAddressKeys;
    const copyToKeys =
      type === 'billing' ? shippingAddressKeys : billingAddressKeys;

    addressComponents.forEach((value, index) => {
      const key = keys[index];
      addressValues[key] = value;

      if (isCopyTo) {
        const copyToKey = copyToKeys[index];
        addressValues[copyToKey] = value;
      }
    });

    setValues({...values, ...addressValues});

    onSelect({type, isCopyTo});
  };

  const handleCopyTo = (e) => {
    setIsCopyTo(e.target.checked);
  };

  return (
    <Box
      style={{
        position: 'relative',
        display: 'flex',
        flexDirection: 'column',
        flex: 1,
      }}>
      <TextField
        autoFocus
        style={{
          width: '100%',
          backgroundColor: 'white',
        }}
        timeout={400}
        onChange={handleSearchAddress}
        placeholder="Search..."
        value={searchValue}
        InputProps={{
          style: {
            borderRadius: 0,
          },
          endAdornment: (
            <InputAdornment position="end">
              <SearchIcon
                style={{
                  color: '#ccc',
                }}
                size={20}
              />
            </InputAdornment>
          ),
        }}
        {...props}
      />
      {!loadingPredictions && !!searchValue && predictions.length === 0 && (
        <ListStyled.EmptyMessage align="center" color="#888">
          No addresses found
        </ListStyled.EmptyMessage>
      )}
      <Box
        style={{
          flex: 1,
          position: 'relative',
        }}>
        <List
          style={{
            padding: 0,
            position: 'absolute',
            height: '100%',
            width: '100%',
            overflow: 'auto',
          }}>
          {predictions.map((option) => (
            <Styled.AddressRow
              key={option.place_id}
              disabled={selectedPlaceId === option.place_id}
              highlight={selectedPlaceId === option.place_id}
              style={{
                borderBottom: '1px solid #eee',
              }}
              button
              onClick={handleSelectAddress(option.place_id)}>
              <RoomIcon />
              <NoteTaskHistoryStyled.SelectionItemText
                secondary={option.description}
              />
            </Styled.AddressRow>
          ))}
          {loadingPredictions && (
            <LoadingIndicator fill transparent align="top" />
          )}
        </List>
      </Box>
      <FormStyled.Footer
        style={{
          padding: '1rem',
          margin: 0,
          borderTop: '1px solid #eee',
          justifyContent: 'space-between',
        }}>
        <Styled.CheckboxContainer
          label={type === 'billing' ? 'Copy to Shipping' : 'Copy to Billing'}
          control={<Checkbox checked={isCopyTo} onChange={handleCopyTo} />}
        />
        <Box
          style={{
            display: 'flex',
            alignItems: 'center',
          }}>
          {loadingPlace && <LoadingIndicator transparent size={20} />}

          <FormStyled.FooterButton
            disabled={!selectedPlaceId || loadingPlace}
            variant="contained"
            startIcon={<RoomIcon />}
            onClick={handleUse}
            size="medium">
            Use
          </FormStyled.FooterButton>
        </Box>
      </FormStyled.Footer>
    </Box>
  );
};

export default FindAddressForms;
