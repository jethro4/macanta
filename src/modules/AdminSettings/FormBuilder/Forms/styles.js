import {experimentalStyled as styled} from '@mui/material/styles';

import Box from '@mui/material/Box';
import {Accordion} from '@macanta/components/Layout';
import FormField from '@macanta/containers/FormField';

export const FormSidebarRoot = styled(Box)`
  height: 100%;
  width: 320px;
  padding: 1rem;
  display: flex;
  flex-direction: column;
  border-radius: 0;
  background-color: ${({theme}) => theme.palette.common.white};
  border-right: 1px solid #eee;
`;

export const SidebarAccordion = styled(Accordion)``;

export const SidebarField = styled(FormField)`
  margin-bottom: ${({theme}) => theme.spacing(2)};
`;

export const SettingsField = styled(FormField)`
  margin-bottom: ${({theme}) => theme.spacing(2.5)};
`;

export const FormBodyContainer = styled(Box)`
  height: 100%;
  padding: 1rem 1.5rem 1.5rem;
`;

export const FormUIContainer = styled(Box)`
  position: relative;
  border: 1px dashed #ccc;

  &:hover {
    border: 1px dashed #888;
  }

  background-color: ${({theme}) => theme.palette.grayLightest.main};
  border-radius: 4px;
  padding: 16px 12px 12px;
`;

export const FormActionButtonsContainer = styled(Box)`
  position: absolute;
  top: 4px;
  right: 12px;
`;

export const FormsArea = styled(Box)`
  #MacantaWebform {
    width: 100%;

    #MacantaForm .fields-group-title {
      width: auto;
    }

    fieldset.fields-group {
      display: block;
    }
  }
`;
