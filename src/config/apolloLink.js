import fetch from 'isomorphic-fetch';
import {ApolloLink, HttpLink} from '@apollo/client';
import isObject from 'lodash/isObject';
import {createAuthLink} from 'aws-appsync-auth-link';
import {RetryLink} from '@apollo/client/link/retry';
import {onError} from '@apollo/client/link/error';
import sentryConfig from '@macanta/config/sentryConfig';
import {isLoggedIn, getAppInfo} from '@macanta/utils/app';
import {triggerCheck} from '@macanta/hooks/admin/useSessionExpiry';
import {sessionExpiredVar} from '@macanta/graphql/cache/vars';
import * as Storage from '@macanta/utils/storage';
import {findItem} from '@macanta/utils/array';

class ApolloServerException extends Error {
  constructor(...params) {
    super(...params);
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, ApolloServerException);
    }
    this.name = this.errorType = 'ApolloServerException';
  }
}

class ApolloClientException extends Error {
  constructor(...params) {
    super(...params);
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, ApolloClientException);
    }
    this.name = this.errorType = 'ApolloClientException';
  }
}

const SENTRY_MAX_CHAR_LENGTH = 220;

const logByParts = (msg = '') => {
  for (let i = 0; i < Math.ceil(msg.length / SENTRY_MAX_CHAR_LENGTH); i++) {
    const message =
      `Error Log ${i + 1} - ` +
      msg.substring(
        i * SENTRY_MAX_CHAR_LENGTH,
        SENTRY_MAX_CHAR_LENGTH * (i + 1),
      );

    console.error(message);
  }
};

const logGraphQLError = (message, variables, options) => {
  if (variables) {
    logByParts(
      JSON.stringify({
        variables: {
          ...variables,
          ...(variables.password && {
            password: '********',
          }),
        },
        message: isObject(message) ? JSON.stringify(message) : message,
      }),
    );
  }

  const errTitle = [options?.operationName, options?.errorType, message]
    .filter((t) => !!t)
    .join(' - ');

  if (options?.client) {
    sentryConfig.captureException({
      err: new ApolloClientException(errTitle),
      operationName: options?.operationName,
      errorType: options?.errorType,
    });
  } else {
    sentryConfig.captureException({
      err: new ApolloServerException(errTitle),
      operationName: options?.operationName,
      errorType: options?.errorType,
    });
  }
};

const validLoggedInOperation = (operationName) => {
  return [
    'appCheck',
    'getSessionStatus',
    'getAppSettings',
    'login',
    'forgotPassword',
  ].includes(operationName);
};

const AppSyncConfig = {
  graphqlEndpoint: process.env.GATSBY_API_URL,
  region: process.env.AWS_REGION,
  authenticationType: process.env.AWS_AUTHENTICATION_TYPE,
  apiKey: process.env.GATSBY_API_KEY,
};

const url = AppSyncConfig.graphqlEndpoint;
const region = AppSyncConfig.region;
const auth = {
  type: AppSyncConfig.authenticationType,
  apiKey: AppSyncConfig.apiKey,
};

const handleForward = (operation, forward) => {
  return forward(operation).map((response) => {
    return response;
  });
};

export const retryLink = new RetryLink();

export const appLink = new ApolloLink((operation, forward) => {
  const session = Storage.getItem('session');
  const [appName, apiKey] = getAppInfo();

  const {removeAppInfo, ...operationVars} = operation.variables;

  if (!removeAppInfo && !operation.getContext().removeAppInfo) {
    const operationType = findItem(operation.query.definitions, {
      kind: 'OperationDefinition',
    })?.operation;
    const isMutation = operationType === 'mutation';

    if (isMutation) {
      const mutationKey = operationVars.__mutationkey || 'input';
      const mutationVariables = operationVars[mutationKey];

      operation.variables[mutationKey] = {
        appName,
        apiKey,
        ...mutationVariables,
      };
    } else {
      operation.variables = {appName, apiKey, ...operationVars};
    }
  }

  const customHeaders = {
    'app-name': appName,
  };

  if (isLoggedIn() && !sessionExpiredVar()) {
    Object.assign(customHeaders, {
      'app-api-key': apiKey,
      'session-id': session?.sessionId,
    });
  } else if (!validLoggedInOperation(operation.operationName)) {
    return null;
  }

  operation.setContext(({headers}) => {
    return {
      headers: {
        ...customHeaders,
        ...headers,
      },
    };
  });

  return handleForward(operation, forward);
});

export const errorLink = onError(({graphQLErrors, networkError, operation}) => {
  if (networkError) {
    const errMessage = `Network error - ${operation.operationName}`;

    console.error(errMessage);
  } else if (graphQLErrors) {
    graphQLErrors.forEach((err) => {
      const errMessage = `GraphQL error - ${operation.operationName} - ${err.errorType}`;

      console.error(errMessage);

      if (err.errorType) {
        if (err.errorType === 'SessionExpiredException') {
          triggerCheck();
        } else {
          logGraphQLError(err.message, operation.variables, {
            operationName: operation.operationName,
            errorType: err.errorType,
          });
        }
      } else {
        logGraphQLError(err.message, operation.variables, {
          operationName: operation.operationName,
          errorType: 'GraphQLValidation',
          client: true,
        });
      }
    });
  }
});

const link = ApolloLink.from([
  errorLink,
  retryLink,
  createAuthLink({url, region, auth}),
  appLink,
  new HttpLink({uri: url, fetch}),
]);

export default link;
