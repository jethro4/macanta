import React, {useState} from 'react';
import HelpIcon from '@mui/icons-material/Help';
import Popover from '@macanta/components/Popover';
import Markdown from '@macanta/components/Markdown';
import * as Styled from './styles';

const HelperTextBtn = ({children, PopoverProps, ...props}) => {
  const [anchorEl, setAnchorEl] = useState(false);
  const openHelperText = Boolean(anchorEl);

  const handlePopoverMouseEvent = (event) => {
    event.stopPropagation();
  };

  const handleViewHelperText = (event) => {
    handlePopoverMouseEvent(event);

    setAnchorEl(event.currentTarget);
  };

  const handleCloseHelperText = () => {
    setAnchorEl(null);
  };

  return !children ? null : (
    <>
      <Styled.HelperTextBtn
        aria-haspopup="true"
        onClick={handleViewHelperText}
        {...props}>
        <HelpIcon />
      </Styled.HelperTextBtn>
      <Popover
        open={openHelperText}
        anchorEl={anchorEl}
        onClick={handlePopoverMouseEvent}
        onMouseUp={handlePopoverMouseEvent}
        onMouseDown={handlePopoverMouseEvent}
        onClose={handleCloseHelperText}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        {...PopoverProps}>
        <div
          style={{
            minWidth: 250,
            maxWidth: 600,
            margin: '1rem',
          }}>
          <Markdown>{children}</Markdown>
        </div>
      </Popover>
    </>
  );
};

export default HelperTextBtn;
