import React from 'react';
import * as Styled from './styles';

const StepLabel = (props) => {
  return <Styled.StepLabel {...props} />;
};

export default StepLabel;
