import {hasLabelTexts} from '@tests/jestSettings/utils/assertUtils';
import {selectText, wait} from '@tests/jestSettings/utils/selectorUtils';
import {fireEvent, screen} from '@testing-library/react';
import isArray from 'lodash/isArray';
import isString from 'lodash/isString';

export const click = (elm, options) => {
  let element = elm;

  if (isString(elm)) {
    element = selectText(elm, options);
  }

  fireEvent.click(element);
};

export const changeInput = (elm, value) => {
  fireEvent.change(elm, {target: {value}});
};

export const toggleSelect = async (elm, toggleValue) => {
  click(elm);

  await wait();

  if (isArray(toggleValue)) {
    await hasLabelTexts(toggleValue);

    toggleValue.forEach((val) => {
      click(screen.getByLabelText(val));
    });
  } else {
    await hasLabelTexts(toggleValue);

    click(screen.getByLabelText(toggleValue));
  }

  click(elm);

  await wait();
};
