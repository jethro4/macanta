let nextFactoryIds = {};

export const resetFactoryIds = () => {
  nextFactoryIds = {};
};

export const nextFactoryId = (typename, {reset} = {reset: false}) => {
  const currentIndex = (!reset && nextFactoryIds[typename]) || 1;
  nextFactoryIds[typename] = currentIndex + 1;
  return {
    id: `${typename}${String(currentIndex)}`,
    currentIndex: String(currentIndex),
  };
};

export const mockData = (data) => {
  resetFactoryIds();

  return data;
};
