import React from 'react';
import RestartAltIcon from '@mui/icons-material/RestartAlt';
import Box from '@mui/material/Box';
import Button from '@macanta/components/Button';

const ActionButtons = () => {
  return (
    <Box
      style={{
        display: 'flex',
        alignItems: 'center',
      }}>
      <Button
        // disabled={!dirty}
        size="small"
        variant="outlined"
        startIcon={<RestartAltIcon />}
        onClick={() => {
          // setValues(initialValues);
          // setErrors({});
        }}>
        Reset
      </Button>
    </Box>
  );
};

export default ActionButtons;
