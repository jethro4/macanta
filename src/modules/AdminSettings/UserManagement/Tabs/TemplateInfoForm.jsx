import React from 'react';
import {useFormikContext} from 'formik';
import FormField from '@macanta/containers/FormField';
import BodySection from '@macanta/modules/AdminSettings/UserManagement/BodySection';

const TemplateInfoForm = (props) => {
  const {values, errors, handleChange} = useFormikContext();

  return (
    <BodySection
      style={{
        height: 'auto',
      }}
      bodyStyle={{
        padding: '1rem',
      }}
      {...props}>
      <FormField
        required
        value={values.name}
        error={errors.name}
        onChange={handleChange('name')}
        label="Template Name"
        fullWidth
        size="small"
        variant="outlined"
      />

      <FormField
        type="TextArea"
        hideExpand
        value={values.description}
        onChange={handleChange('description')}
        label="Description"
        fullWidth
        size="small"
        variant="outlined"
      />
    </BodySection>
  );
};

export default TemplateInfoForm;
