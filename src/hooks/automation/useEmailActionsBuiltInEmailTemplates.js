import useQuery from '@macanta/hooks/apollo/useQuery';
import {GET_EMAIL_ACTIONS_BUILT_IN_EMAIL_TEMPLATES} from '@macanta/graphql/automation';

const useEmailActionsBuiltInEmailTemplates = (options) => {
  const query = useQuery(GET_EMAIL_ACTIONS_BUILT_IN_EMAIL_TEMPLATES, options);

  return query;
};

export default useEmailActionsBuiltInEmailTemplates;
