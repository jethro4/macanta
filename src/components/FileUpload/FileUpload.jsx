import React from 'react';
import Typography from '@mui/material/Typography';
import Dropzone from '@macanta/components/Dropzone';
import * as Styled from './styles';

const FileUpload = ({
  value,
  readOnly,
  attachments,
  style,
  className,
  ...props
}) => {
  return !readOnly ? (
    <Styled.Root style={style} className={className}>
      <Dropzone
        initialFiles={attachments}
        //       onChange={handleChange}
        {...props}
      />
    </Styled.Root>
  ) : (
    <Typography
      sx={{
        color: 'primary.main',
      }}
      style={{
        fontSize: '0.875rem',
      }}>
      {value}
    </Typography>
  );
};

FileUpload.defaultProps = {
  attachments: [],
};

export default FileUpload;
