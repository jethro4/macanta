import React, {useState} from 'react';
import clsx from 'clsx';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Menu from '@mui/material/Menu';
import MoreIcon from '@mui/icons-material/MoreVert';
import {makeStyles} from '@mui/material/styles';
import Drawer, {
  drawerWidth,
  getDrawerClosedWidth,
} from '@macanta/containers/Drawer';
import isNil from 'lodash/isNil';
import * as Styled from './styles';

//TODO: remove !important overrides if fixed on next MUI version
const useStyles = makeStyles((theme) => ({
  appBar: {
    // zIndex: `${theme.zIndex.drawer + 1}!important`,
    marginLeft: getDrawerClosedWidth(theme),
    width: `calc(100% - ${getDrawerClosedWidth(theme)})!important`,
    transition: `${theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    })}!important`,
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)!important`,
    transition: `${theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    })}!important`,
  },
  toolbar: {
    padding: '0 1rem!important',
  },
}));

const Header = ({children, renderDesktopMenu, renderMobileMenu}) => {
  const classes = useStyles();

  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = useState(null);
  const [drawerOpen, setDrawerOpen] = useState(false);

  const handleDrawerToggle = (toggleState) => {
    setDrawerOpen(!isNil(toggleState) ? toggleState : !drawerOpen);
  };

  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  const handleMobileMenuOpen = (event) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  return (
    <>
      <Styled.AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: drawerOpen,
        })}>
        <Toolbar className={classes.toolbar}>
          {children}
          <Styled.FlexGrow />
          <Styled.SectionDesktop>{renderDesktopMenu}</Styled.SectionDesktop>
          <Styled.SectionMobile>
            <IconButton aria-haspopup="true" onClick={handleMobileMenuOpen}>
              <MoreIcon />
            </IconButton>
          </Styled.SectionMobile>
        </Toolbar>
      </Styled.AppBar>
      <Drawer drawerOpen={drawerOpen} onDrawerToggle={handleDrawerToggle} />
      <Menu
        anchorEl={mobileMoreAnchorEl}
        anchorOrigin={{vertical: 'top', horizontal: 'right'}}
        transformOrigin={{vertical: 'top', horizontal: 'right'}}
        open={isMobileMenuOpen}
        onClose={handleMobileMenuClose}>
        {renderMobileMenu}
      </Menu>
    </>
  );
};

export default Header;
