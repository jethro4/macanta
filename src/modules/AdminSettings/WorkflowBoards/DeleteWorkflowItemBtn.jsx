import React, {useState} from 'react';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import Tooltip from '@macanta/components/Tooltip';
import ConfirmationDialog from '@macanta/containers/ConfirmationDialog';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import {DELETE_WORKFLOW_BOARD} from '@macanta/graphql/admin';
import {useMutation} from '@apollo/client';
import * as DataObjectsSearchTableStyled from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/styles';

const DeleteWorkflowItemBtn = ({id, name}) => {
  const {displayMessage} = useProgressAlert();

  const [showDeleteDialog, setShowDeleteDialog] = useState(false);

  const [
    callDeleteWorkflowBoardMutation,
    deleteWorkflowBoardMutation,
  ] = useMutation(DELETE_WORKFLOW_BOARD, {
    update(cache, {data: {deleteWorkflowBoard}}) {
      const normalizedId = cache.identify({
        id: deleteWorkflowBoard.id,
        __typename: 'WorkflowBoard',
      });
      cache.evict({id: normalizedId});
      cache.gc();
    },
    onCompleted(data) {
      if (data.deleteWorkflowBoard?.success) {
        displayMessage('Deleted board successfully');

        handleClose();
      }
    },
    onError() {},
  });

  const handleClose = () => {
    setShowDeleteDialog(false);
  };

  const handleShowDialog = () => {
    setShowDeleteDialog(true);
  };

  const handleDelete = () => {
    callDeleteWorkflowBoardMutation({
      variables: {
        deleteWorkflowBoardInput: {
          id,
        },
        __mutationkey: 'deleteWorkflowBoardInput',
        removeAppInfo: true,
      },
    });
  };

  return (
    <>
      <Tooltip title="Delete">
        <DataObjectsSearchTableStyled.ActionButton
          style={{
            color: '#aaa',
          }}
          show={showDeleteDialog}
          onClick={handleShowDialog}>
          <DeleteForeverIcon />
        </DataObjectsSearchTableStyled.ActionButton>
      </Tooltip>
      <ConfirmationDialog
        open={!!showDeleteDialog}
        onClose={handleClose}
        title="Are you sure?"
        description={[
          `You are about to delete "${name}" from Workflow Boards.`,
          'This cannot be undone. Do you want to proceed?',
        ]}
        actions={[
          {
            label: 'Delete',
            onClick: handleDelete,
            startIcon: <DeleteForeverIcon />,
          },
        ]}
      />
      <LoadingIndicator modal loading={deleteWorkflowBoardMutation.loading} />
    </>
  );
};

export default DeleteWorkflowItemBtn;
