import React, {useRef, useState} from 'react';

import Typography from '@mui/material/Typography';
import InfoIcon from '@mui/icons-material/Info';
import ExitToAppIcon from '@mui/icons-material/ExitToApp';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import {useLazyQuery, useReactiveVar} from '@apollo/client';
import {LOGIN} from '@macanta/graphql/auth';
import Form from '@macanta/components/Form';
import {GET_ACCESS_PERMISSIONS} from '@macanta/graphql/permissions';
import loginValidationSchema from '@macanta/validations/login';
import * as Storage from '@macanta/utils/storage';
import {navigate} from 'gatsby';
import FormHeader from './FormHeader';
import * as Styled from './styles';
import {newVersionAvailableVar} from '@macanta/graphql/cache/vars';
import {setIsLoggedIn} from '@macanta/utils/app';
import {clearSessionVars} from '@macanta/graphql/cache/sessionVars';

const FormsLogin = ({onForgotPassword, onLogin}) => {
  const newVersionAvailable = useReactiveVar(newVersionAvailableVar);

  const [loading, setLoading] = useState(false);

  const userRef = useRef({});

  const [callLoginQuery, loginQuery] = useLazyQuery(LOGIN, {
    fetchPolicy: FETCH_POLICIES.NO_CACHE,
    onCompleted(data = {}) {
      if (data.login) {
        const {email} = userRef.current;
        const {
          sessionId,
          apiKey,
          autoLoginKey,
          firstName,
          lastName,
          userId,
          signature,
          permission,
        } = data.login;

        const session = Storage.getItem('session');
        const lastUrl = session?.lastUrl;
        const initUrl = lastUrl && lastUrl !== '/' ? lastUrl : '/app/dashboard';

        if (permission) {
          const variables = {
            email,
          };

          loginQuery.client.writeQuery({
            query: GET_ACCESS_PERMISSIONS,
            data: {
              getAccessPermissions: {
                __typename: 'AccessPermissions',
                ...permission,
              },
            },
            variables,
          });
        }

        Storage.setItem('session', {sessionId, apiKey, autoLoginKey});
        Storage.setItem('userDetails', {
          email,
          firstName,
          lastName,
          userId,
          signature,
        });

        setIsLoggedIn(true);

        clearSessionVars();

        if (!onLogin) {
          if (newVersionAvailable) {
            history.pushState(null, '', lastUrl);
            window.location.reload();
          } else {
            navigate(initUrl, {replace: true});
          }
        } else {
          onLogin();
        }
      }
    },
    onError() {
      setLoading(false);
    },
  });

  const handleFormSubmit = (values) => {
    setLoading(true);

    userRef.current = values;

    callLoginQuery({
      variables: {
        email: values.email,
        password: values.password,
      },
    });
  };

  return (
    <>
      <FormHeader title="Login" description="Please fill in the form below." />

      <Form
        initialValues={{email: '', password: ''}}
        validationSchema={loginValidationSchema}
        onSubmit={handleFormSubmit}>
        {({
          values,
          errors,
          handleChange,
          handleSubmit,
          /* and other goodies */
        }) => (
          <>
            <Styled.TextField
              error={errors.email}
              autoComplete="email"
              onChange={handleChange('email')}
              label="Email"
              variant="filled"
              value={values.email}
              fullWidth
              onEnterPress={handleSubmit}
            />
            <Styled.TextField
              error={errors.password}
              autoComplete="current-password"
              password
              onChange={handleChange('password')}
              label="Password"
              variant="filled"
              value={values.password}
              fullWidth
              onEnterPress={handleSubmit}
            />

            {!!loginQuery.error && (
              <Typography color="error" variant="subtitle2" align="center">
                {loginQuery.error.message}
              </Typography>
            )}

            <Styled.GridItem container>
              <Styled.GridItem item xs={12} sm={8}>
                <Styled.ButtonLeft
                  onClick={onForgotPassword}
                  startIcon={<InfoIcon />}>
                  Forgot password?
                </Styled.ButtonLeft>
              </Styled.GridItem>
              <Styled.GridItem item xs={12} sm={4}>
                <Styled.ButtonRight
                  onClick={handleSubmit}
                  color="primary"
                  variant="contained"
                  startIcon={<ExitToAppIcon />}>
                  Log me in
                </Styled.ButtonRight>
              </Styled.GridItem>
            </Styled.GridItem>
          </>
        )}
      </Form>
      <LoadingIndicator modal loading={loading} />
    </>
  );
};

export default FormsLogin;
