import {gql} from '@apollo/client';

export default gql`
  mutation deleteDataObjectField($input: DeleteDataObjectFieldInput!) {
    deleteDataObjectField(input: $input) {
      success
      fieldId
    }
  }
`;
