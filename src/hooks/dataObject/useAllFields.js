import {useMemo} from 'react';
import useDOFields from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOFields';
import useContactFields from '@macanta/modules/ContactDetails/useContactFields';
import {CONTACT_DEFAULT_FIELDS} from '@macanta/constants/contactFields';
import {sortArrayByObjectKey} from '@macanta/utils/array';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import useSelectorQuery from '@macanta/hooks/apollo/useSelectorQuery';

const getType = (type = 'Text') => {
  if (['Date', 'DateTime', 'TextArea'].includes(type)) {
    return 'Text';
  }

  return type;
};

const useAllFields = ({
  doTitle,
  groupId,
  includeNone,
  noneLabel = 'None',
  noneValue = '',
  isContactObjectType: isContactObjectTypeProp,
  isDataObjectType,
  includeAllTypes,
  options,
  subheaders = true,
}) => {
  const isContactObjectType =
    groupId === 'co_customfields' || isContactObjectTypeProp;

  const {result: doType} = useSelectorQuery({
    selector: useDOTypes,
    selectorArgs: {
      removeEmail: true,
      includeContactObject: true,
      fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
      skip: !groupId || !!isContactObjectType || !!doTitle,
    },
    itemKey: 'id',
    itemValue: groupId,
  });
  const doFieldsQuery = useDOFields(groupId, {
    removeEmail: true,
    notifyOnNetworkStatusChange: true,
    skip: !groupId || !!isContactObjectType,
    ...options,
  });
  const contactDefaultFieldsQuery = useContactFields(
    null,
    'contactDefaultFields',
    {
      removeEmail: true,
      notifyOnNetworkStatusChange: true,
      skip: !!isDataObjectType,
      ...options,
    },
  );
  const contactCustomFieldsQuery = useContactFields(
    null,
    'contactCustomFields',
    {
      removeEmail: true,
      notifyOnNetworkStatusChange: true,
      skip: !!isDataObjectType,
      ...options,
    },
  );

  const contactDefaultFields =
    contactDefaultFieldsQuery.data?.listDataObjectFields;
  const contactCustomFields =
    contactCustomFieldsQuery.data?.listDataObjectFields;
  const doFields = doFieldsQuery.data?.listDataObjectFields;

  const data = useMemo(() => {
    if (
      ((contactDefaultFields && contactCustomFields) || isDataObjectType) &&
      (doFields || isContactObjectType)
    ) {
      let allFields = [
        ...(isContactObjectType
          ? []
          : (!subheaders
              ? []
              : [
                  {
                    isSubheader: true,
                    label: `${doTitle || doType?.title || ''} Fields`,
                  },
                ]
            ).concat(
              sortArrayByObjectKey(
                doFields.map((f) => ({
                  ...f,
                  label: f.name,
                  value: f.name,
                  id: f.id,
                  choices: f.choices,
                  type: !includeAllTypes ? getType(f.type) : f.type,
                })),
                'label',
              ),
            )),
        ...(isDataObjectType
          ? []
          : (!subheaders
              ? []
              : [
                  {
                    isSubheader: true,
                    label: 'Contact Basic Fields',
                  },
                ]
            ).concat(
              CONTACT_DEFAULT_FIELDS.map((f) => {
                const contactField =
                  contactDefaultFields.find((cField) => cField.id === f.id) ||
                  f;

                return {
                  ...contactField,
                  label: f.label,
                  value: f.name,
                  id: f.id,
                  choices: contactField?.choices,
                  type: !includeAllTypes
                    ? getType(contactField?.type)
                    : contactField?.type,
                };
              }),
            )),
        ...(isDataObjectType
          ? []
          : (!subheaders
              ? []
              : [
                  {
                    isSubheader: true,
                    label: 'Contact Custom Fields',
                  },
                ]
            ).concat(
              sortArrayByObjectKey(
                contactCustomFields.map((f) => ({
                  ...f,
                  label: f.name,
                  value: f.name,
                  id: f.id,
                  choices: f.choices,
                  type: !includeAllTypes ? getType(f.type) : f.type,
                })),
                'label',
              ),
            )),
      ];

      if (includeNone) {
        allFields = [
          {
            label: noneLabel,
            value: noneValue,
          },
        ].concat(allFields);
      }

      return allFields;
    }

    return [];
  }, [
    includeNone,
    isContactObjectType,
    isDataObjectType,
    doFields,
    contactDefaultFields,
    contactCustomFields,
    doType?.title,
  ]);

  const loading = Boolean(
    doFieldsQuery.loading ||
      contactDefaultFieldsQuery.loading ||
      contactCustomFieldsQuery.loading,
  );
  const initLoading =
    ((!isContactObjectType && !doFields) ||
      (!isDataObjectType && (!contactDefaultFields || !contactCustomFields))) &&
    loading;

  return {
    loading,
    initLoading,
    data,
  };
};

export default useAllFields;
