import {useState, useEffect, useCallback} from 'react';
import debounce from 'lodash/debounce';

const getWindowDimensions = () => {
  const {innerWidth: width, innerHeight: height} = window;

  return {
    width,
    height,
  };
};

const useWindowDimensions = (onResize, debounceDelay = 400) => {
  const [windowDimensions, setWindowDimensions] = useState(() =>
    getWindowDimensions(),
  );

  const handleResize = useCallback(
    debounce(
      () => {
        setWindowDimensions(getWindowDimensions());

        onResize?.();
      },
      debounceDelay,
      {
        leading: false,
        trailing: true,
      },
    ),
    [],
  );

  useEffect(() => {
    window.addEventListener('resize', handleResize);

    return () => window.removeEventListener('resize', handleResize);
  }, []);

  return windowDimensions;
};

export default useWindowDimensions;
