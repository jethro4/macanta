import {sortArrayByObjectKey} from '@macanta/utils/array';

export const DEFAULT_REGEX = /~(?=\w)([^~]+)\w~/g;

export const matchAndTransformStrings = ({newVal, oldVal, match}) => {
  let metaArr = getStringsMetadataByMatch({newVal, oldVal, match});
  metaArr = sortArrayByObjectKey(metaArr, 'endIndex', 'desc');

  const transformedString = !metaArr.length
    ? newVal
    : metaArr.reduce((acc, item) => {
        return transformStringMetadata({
          input: acc,
          search: item.str,
          replace: '',
          start: item.startIndex,
          end: item.endIndex,
        });
      }, oldVal);

  return transformedString;
};

export const matchAndTransformStringsWithMetadata = ({
  newVal,
  oldVal,
  match,
}) => {
  let metaArr = getStringsMetadataByMatch({newVal, oldVal, match});
  metaArr = sortArrayByObjectKey(metaArr, 'endIndex', 'desc');

  const transformedString = !metaArr.length
    ? {value: newVal}
    : metaArr.reduce(
        (acc, item) => {
          const value = transformStringMetadata({
            input: acc.value,
            search: item.str,
            replace: '',
            start: item.startIndex,
            end: item.endIndex,
          });

          return {value, currentIndex: item.startIndex};
        },
        {value: oldVal},
      );

  return transformedString;
};

export const getStringsMetadataByMatch = ({newVal, oldVal, match}) => {
  const newValMatches = Array.from(newVal.matchAll(match));
  const oldValMatches = oldVal.matchAll
    ? Array.from(oldVal.matchAll(match))
    : [];
  const filteredMatches = oldValMatches.filter(
    (oldMatch) =>
      !newValMatches.some((newMatch) => newMatch[0] === oldMatch[0]),
  );
  const transformedMatches = filteredMatches.map((filteredMatch) => {
    const str = filteredMatch[0];
    const startIndex = filteredMatch.index;
    const endIndex = startIndex + str.length;

    return {
      str,
      startIndex,
      endIndex,
    };
  });

  return transformedMatches;
};

export const transformStringMetadata = ({
  input,
  search,
  replace,
  start,
  end,
}) => {
  return (
    input.slice(0, start) +
    input.slice(start, end).replace(search, replace) +
    input.slice(end)
  );
};
