import useQuery from '@macanta/hooks/apollo/useQuery';
import {LIST_PERMISSION_TEMPLATES} from '@macanta/graphql/permissions';

const usePermissionTemplates = (id, options = {}) => {
  const permissionTemplatesQuery = useQuery(LIST_PERMISSION_TEMPLATES, options);

  const permissionTemplates =
    permissionTemplatesQuery.data?.listPermissionTemplates;
  let selectedTemplate;

  if (permissionTemplates && id) {
    selectedTemplate = permissionTemplates.find((t) => t.id === id);
  }

  return {
    ...permissionTemplatesQuery,
    data: permissionTemplates,
    selectedTemplate,
  };
};

export default usePermissionTemplates;
