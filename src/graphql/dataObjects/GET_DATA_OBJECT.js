import {gql} from '@apollo/client';
import {DO_ITEM_ATTRIBUTES} from './fragments';

export default gql`
  query getDataObject($appName: String!, $apiKey: String!, $itemId: ID!) {
    getDataObject(appName: $appName, apiKey: $apiKey, itemId: $itemId) {
      ...doItemResults
    }
  }
  ${DO_ITEM_ATTRIBUTES}
`;
