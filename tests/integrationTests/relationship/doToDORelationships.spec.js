import React from 'react';
import {renderApp} from '@tests/jestSettings/utils/renderUtils';
import * as Storage from '@macanta/utils/storage';
import {waitApis} from '@tests/jestSettings/utils/selectorUtils';
import {screen} from '@testing-library/react';
import {
  handleMockDOFields,
  handleMockDOSearchItems,
  handleMockRelationships,
  handleMockDOTypes,
} from '@tests/integrationTests/mockUtils';
import {handleMockDOToDORelationships} from '@tests/integrationTests/relationship/mockUtils';
import {click} from '@tests/jestSettings/utils/eventUtils';
import {triggerConnectDODirectSteps} from '@tests/integrationTests/relationship/renderUtils';
import doToDORelationshipFactory from '@tests/jestSettings/mockFactory/doToDORelationshipFactory';
import DORelationshipsForm from '@macanta/modules/DataObjectItems/DOItemForms/Relationships/DORelationshipsForm';
import {
  hasLength,
  hasTexts,
  isNotInDocument,
} from '@tests/jestSettings/utils/assertUtils';
import doItemFactory from '@tests/jestSettings/mockFactory/doItemFactory';

describe('Display DO To DO Relationships', () => {
  beforeAll(() => {
    Storage.setItem('userDetails', {
      userId: 'Contact1',
      email: 'test1@macantacrm.com',
    });
  });

  beforeEach(() => {
    handleMockDOTypes();
    handleMockDOFields();
  });

  it('display DO direct relationships with sorted fields data', async () => {
    renderApp(
      <DORelationshipsForm doItemId="ItemDataObject4" groupId="DataObject1" />,
    );

    handleMockDOToDORelationships({
      direct: [
        doToDORelationshipFactory({
          doItemId: `ItemDataObject1`,
          groupId: 'DataObject1',
          type: 'direct',
          data: [
            {
              fieldName: 'Text Field',
              value: 'Test Text 1',
            },
            {
              fieldName: 'TextArea Field',
              value: 'Test TextArea 1',
            },
            {
              fieldName: 'Select Field',
              value: 'Test Select 1',
            },
          ],
        }),
        doToDORelationshipFactory({
          doItemId: `ItemDataObject2`,
          groupId: 'DataObject2',
          type: 'direct',
          data: [
            {
              fieldName: 'Text Field',
              value: 'Test Text 2',
            },
            {
              fieldName: 'Select Field',
              value: 'Test Select 2',
            },
          ],
        }),
        doToDORelationshipFactory({
          doItemId: `ItemDataObject3`,
          groupId: 'DataObject2',
          type: 'direct',
          data: [
            {
              fieldName: 'Text Field',
              value: 'Test Text 3',
            },
            {
              fieldName: 'TextArea Field',
              value: 'Test TextArea 3',
            },
          ],
        }),
      ],
    });

    await waitApis('listDOToDORelationships');

    await hasLength(['.item-data'], 3, {
      testId: 'DODirectRelationshipsForm',
    });
    await hasTexts(
      ['.item-data', '.item-data-field'],
      [
        [
          ['Text Field', 'Test Text 1'],
          ['TextArea Field', 'Test TextArea 1'],
          ['Select Field', 'Test Select 1'],
        ],
        [
          ['Text Field', 'Test Text 2'],
          ['Select Field', 'Test Select 2'],
        ],
        [
          ['Text Field', 'Test Text 3'],
          ['TextArea Field', 'Test TextArea 3'],
        ],
      ],
      {
        testId: 'DODirectRelationshipsForm',
      },
    );

    await isNotInDocument('ItemDataObject6', {
      testId: 'DOIndirectRelationshipsForm',
    });
    await isNotInDocument('ItemDataObject7', {
      testId: 'DOIndirectRelationshipsForm',
    });
  });

  it('display DO indirect relationships for each direct relationships', async () => {
    renderApp(
      <DORelationshipsForm
        onChange={jest.fn()}
        doItemId="ItemDataObject5"
        groupId="DataObject2"
      />,
    );

    handleMockDOToDORelationships({
      direct: [
        doToDORelationshipFactory({
          doItemId: `ItemDataObject1`,
          groupId: 'DataObject1',
          type: 'direct',
        }),
        doToDORelationshipFactory({
          doItemId: `ItemDataObject3`,
          groupId: 'DataObject2',
          type: 'direct',
        }),
      ],
      indirect: {
        ItemDataObject1: [
          doToDORelationshipFactory({
            doItemId: `ItemDataObject6`,
            groupId: 'DataObject2',
            type: 'indirect',
          }),
        ],
        ItemDataObject3: [
          doToDORelationshipFactory({
            doItemId: `ItemDataObject7`,
            groupId: 'DataObject2',
            type: 'indirect',
          }),
        ],
      },
    });

    await waitApis();

    await hasLength(['.item-data'], 2, {
      testId: 'DODirectRelationshipsForm',
    });
    await hasTexts(['.item-data'], ['ItemDataObject1', 'ItemDataObject3'], {
      testId: 'DODirectRelationshipsForm',
    });

    await hasLength(['.item-data'], 2, {
      testId: 'DOIndirectRelationshipsForm',
    });
    await hasTexts(['.item-data'], ['ItemDataObject6', 'ItemDataObject7'], {
      selector: '.indirect-DataObject2',
    });
  });
});

describe('Connect DO To DO Relationships', () => {
  beforeAll(() => {
    Storage.setItem('userDetails', {
      userId: 'Contact1',
      email: 'test1@macantacrm.com',
    });
  });

  beforeEach(() => {
    handleMockDOTypes();
    handleMockDOFields();
    handleMockRelationships();
  });

  it('search and connect DO To DO relationships', async () => {
    renderApp(
      <DORelationshipsForm
        onChange={jest.fn()}
        doItemId="ItemDataObject5"
        groupId="DataObject2"
      />,
    );

    handleMockDOSearchItems({
      DataObject1: [
        doItemFactory({
          id: 'ItemDataObject1',
          isSearch: true,
        }),
        doItemFactory({
          id: 'ItemDataObject4',
          isSearch: true,
        }),
      ],
    });
    handleMockDOToDORelationships({
      direct: [
        doToDORelationshipFactory({
          doItemId: `ItemDataObject2`,
          groupId: 'DataObject2',
          type: 'direct',
        }),
        doToDORelationshipFactory({
          doItemId: `ItemDataObject3`,
          groupId: 'DataObject2',
          type: 'direct',
        }),
      ],
    });

    await waitApis();

    await triggerConnectDODirectSteps();

    await hasLength(['.item-data'], 4, {
      testId: 'DODirectRelationshipsForm',
    });
    await hasTexts(['.item-data'], ['ItemDataObject1', 'ItemDataObject4'], {
      selector: '.direct-DataObject1',
    });
    await hasTexts(['.item-data'], ['ItemDataObject2', 'ItemDataObject3'], {
      selector: '.direct-DataObject2',
    });
  });

  it('delete DO To DO relationships', async () => {
    renderApp(
      <DORelationshipsForm
        onChange={jest.fn()}
        doItemId="ItemDataObject5"
        groupId="DataObject2"
      />,
    );

    handleMockDOToDORelationships({
      direct: [
        doToDORelationshipFactory({
          doItemId: `ItemDataObject1`,
          groupId: 'DataObject1',
          type: 'direct',
        }),
        doToDORelationshipFactory({
          doItemId: `ItemDataObject2`,
          groupId: 'DataObject2',
          type: 'direct',
        }),
        doToDORelationshipFactory({
          doItemId: `ItemDataObject3`,
          groupId: 'DataObject2',
          type: 'direct',
        }),
      ],
    });

    await waitApis('listDOToDORelationships');

    const deleteButtons = screen.getAllByLabelText('Delete');

    click(deleteButtons[0]);
    click(deleteButtons[2]);

    await hasLength(['.item-data'], 1, {
      testId: 'DODirectRelationshipsForm',
    });
    await hasTexts(['.item-data'], ['ItemDataObject2'], {
      testId: 'DODirectRelationshipsForm',
    });
  });
});
