import {
  ACTION_FIELDS_MAPPING,
  TRIGGER_ACTION_TYPES,
} from '@macanta/constants/automation';

export const getActions = (actionItem) => {
  return TRIGGER_ACTION_TYPES.map((item) => {
    const valueId = actionItem?.[ACTION_FIELDS_MAPPING[item.type]] || '';
    const hasAccess = !!valueId;

    return {
      label: item.label,
      type: item.type,
      name: ACTION_FIELDS_MAPPING[item.type],
      value: valueId,
      hasAccess,
    };
  });
};

export const addIdToItems = (arr, getId) =>
  arr.map((item, index) => ({
    ...item,
    id: getId?.({item, index}) || `${JSON.stringify(item)}-${index}`,
  }));
