import useMutation from '@macanta/hooks/apollo/useMutation';
import {POST_SMS_ACTION} from '@macanta/graphql/automation';

const usePostSmsActionMutation = (options) => {
  const mutation = useMutation(POST_SMS_ACTION, options);

  return mutation;
};

export default usePostSmsActionMutation;
