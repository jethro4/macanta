import * as yup from 'yup';
import trim from 'lodash/trim';
import zipObject from 'lodash/zipObject';
import XLSX from 'xlsx';

const parseExcel = function (file) {
  var workbook = XLSX.read(file, {
    type: 'base64',
  });

  const sheetName = workbook.SheetNames[0];

  var XL_row_object = XLSX.utils.sheet_to_row_object_array(
    workbook.Sheets[sheetName],
  );

  if (XL_row_object[0]?.__EMPTY) {
    return XL_row_object.slice(1).map((item) =>
      Object.entries(item).reduce((acc, [key, value]) => {
        acc[XL_row_object[0][key]] = value;

        return acc;
      }, {}),
    );
  }

  return XL_row_object;
};

function csvToObjectArray(csvString) {
  var csvRowArray = csvString.split(/\r\n/);
  var headerCellArray = trimQuotes(csvRowArray.shift().split(','));
  var objectArray = [];

  while (csvRowArray.length) {
    var rowCellArray = trimQuotes(csvRowArray.shift().split(','));
    var rowObject = zipObject(headerCellArray, rowCellArray);
    objectArray.push(rowObject);
  }

  return objectArray;
}

function trimQuotes(stringArray) {
  for (var i = 0; i < stringArray.length; i++) {
    stringArray[i] = trim(stringArray[i], '"');
  }

  return stringArray;
}

const checkEmptyData = function (values) {
  const attachment = values[0];

  if (attachment) {
    let results;

    if (attachment.fileExt.startsWith('xls')) {
      results = parseExcel(attachment.thumbnail.split(',')[1]);
    } else {
      const csvString = window.atob(attachment.thumbnail.split(',')[1]);
      results = csvToObjectArray(csvString);
    }

    if (!results.length) {
      return false;
    }

    return true;
  }

  return false;
};

const checkRequiredFields = function (values) {
  const dataType = this.parent?.dataType;

  const attachment = values[0];

  if (dataType && attachment) {
    let results;

    if (attachment.fileExt.startsWith('xls')) {
      results = parseExcel(attachment.thumbnail.split(',')[1]);
    } else {
      const csvString = window.atob(attachment.thumbnail.split(',')[1]);
      results = csvToObjectArray(csvString);
    }

    if (results[0]) {
      const fileFields = Object.keys(results[0]);

      const nonSupportedFields = fileFields.reduce((acc, fieldName) => {
        if (
          !(
            dataType !== 'Contacts' &&
            ['Contact Email', 'Contact Relationship'].includes(fieldName)
          ) &&
          !this.parent?.fieldNames.includes(fieldName)
        ) {
          return acc.concat(fieldName);
        }

        return acc;
      }, []);

      this.parent.nonSupportedFieldsWarning = nonSupportedFields
        .map((fieldName) => `"${fieldName}"`)
        .join(' | ');

      if (
        dataType === 'Contacts' &&
        (!fileFields.includes('FirstName') || !fileFields.includes('Email'))
      ) {
        return this.createError({
          message: 'Must have required fields: "FirstName" | "Email"',
        });
      } else if (dataType !== 'Contacts') {
        const requiredFields = this.parent?.requiredFields;

        if (
          requiredFields?.length &&
          requiredFields.some((fieldName) => !fileFields.includes(fieldName))
        ) {
          return this.createError({
            message: `Must have required fields: ${requiredFields
              .map((fieldName) => `"${fieldName}"`)
              .join(' | ')}`,
          });
        }
      }

      if (
        !this.parent?.fieldNames.some((fieldName) =>
          fileFields.includes(fieldName),
        )
      ) {
        return this.createError({
          message: 'Must have at least one matching field',
        });
      }
    }
  }

  return true;
};

const checkRequiredFieldId = function (value) {
  const dataType = this.parent?.dataType;

  const fieldId = value;

  if (dataType && dataType !== 'Contacts' && !fieldId) {
    return false;
  }

  return true;
};

const schema = yup.object().shape({
  dataType: yup.string().required('Required'),
  attachments: yup
    .array()
    .min(1, 'Please attach a file to import')
    .test('checkEmptyData', 'File has empty data', checkEmptyData)
    .test(
      'checkRequiredFields',
      'Required Validation Error',
      checkRequiredFields,
    ),
  fieldName: yup
    .string()
    .test('checkRequiredFieldId', 'Required', checkRequiredFieldId),
});

export default schema;
