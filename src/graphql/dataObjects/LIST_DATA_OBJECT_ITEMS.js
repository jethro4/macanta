import {gql} from '@apollo/client';
import {DO_ITEM_ATTRIBUTES} from './fragments';

export default gql`
  query listDataObjectItems(
    $appName: String!
    $apiKey: String!
    $email: String
    $groupId: ID!
    $contactId: ID!
  ) {
    listDataObjectItems(
      appName: $appName
      apiKey: $apiKey
      email: $email
      groupId: $groupId
      contactId: $contactId
    ) {
      items {
        ...doItemResults
      }
      count
      next
      previous
    }
  }
  ${DO_ITEM_ATTRIBUTES}
`;
