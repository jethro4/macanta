import useAccessPermissions from '@macanta/hooks/admin/useAccessPermissions';
import useFilterUserContacts from '@macanta/hooks/admin/useFilterUserContacts';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';

const useDOHasConnectedUser = (connectedContacts = []) => {
  const accessPermissionsQuery = useAccessPermissions({
    key: 'permissionMeta.userDOEditPermission',
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });
  const connectedUsers = useFilterUserContacts(connectedContacts);

  const userDOEditPermission = accessPermissionsQuery.data;
  const hasConnectedUser = !!connectedUsers.length;
  const checkRelationships = !!userDOEditPermission?.otherUserRelationship
    ?.length;
  const hasConnectedRelationships =
    hasConnectedUser &&
    checkRelationships &&
    connectedUsers.some((contact) =>
      contact.relationships.some((r) =>
        userDOEditPermission.otherUserRelationship.includes(r.id),
      ),
    );

  return {
    hasConnectedUser,
    hasConnectedRelationships,
  };
};

export default useDOHasConnectedUser;
