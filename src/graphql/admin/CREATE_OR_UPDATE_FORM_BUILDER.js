import {gql} from '@apollo/client';
import {FORM_BUILDER_FRAGMENT} from './fragments';

export default gql`
  mutation createOrUpdateFormBuilder(
    $createOrUpdateFormBuilderInput: CreateOrUpdateFormBuilderInput!
  ) {
    createOrUpdateFormBuilder(input: $createOrUpdateFormBuilderInput) {
      ...formBuilderFragment
    }
  }
  ${FORM_BUILDER_FRAGMENT}
`;
