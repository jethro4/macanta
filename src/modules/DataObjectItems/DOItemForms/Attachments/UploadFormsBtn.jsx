import React, {useState} from 'react';
import Modal from '@macanta/components/Modal';
import UploadForms from './UploadForms';
import Button from '@macanta/components/Button';
import CloudUploadIcon from '@mui/icons-material/CloudUpload';

const UploadFormsBtn = ({onSuccess, ...props}) => {
  const [showModal, setShowModal] = useState(false);

  const handleShowModal = () => {
    setShowModal(true);
  };
  const handleCloseModal = () => {
    setShowModal(false);
  };
  const handleSuccess = async (attachment, type) => {
    onSuccess && (await onSuccess(attachment, type));
    setShowModal(false);
  };

  return (
    <>
      <Button
        onClick={handleShowModal}
        size="small"
        variant="contained"
        startIcon={<CloudUploadIcon />}
        {...props}>
        Upload
      </Button>
      <Modal
        headerTitle="Attach Files"
        open={showModal}
        onClose={handleCloseModal}
        contentWidth={800}
        contentHeight={600}>
        <UploadForms onSubmit={handleSuccess} />
      </Modal>
    </>
  );
};

export default UploadFormsBtn;
