import useMutation from '@macanta/hooks/apollo/useMutation';
import {POST_EMAIL_ACTION} from '@macanta/graphql/automation';

const usePostEmailActionMutation = (options) => {
  const mutation = useMutation(POST_EMAIL_ACTION, options);

  return mutation;
};

export default usePostEmailActionMutation;
