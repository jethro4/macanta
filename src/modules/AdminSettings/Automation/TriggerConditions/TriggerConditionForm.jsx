import React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import FlexGrid from '@macanta/components/Base/FlexGrid';
import BasicInfoForm from '@macanta/modules/AdminSettings/Automation/TriggerActions/SubForms/BasicInfoForm';
import withForm from '@macanta/components/Form/withForm';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import BodySection from '@macanta/modules/AdminSettings/UserManagement/BodySection';
import FormField from '@macanta/containers/FormField';
import CriteriaFiltersForm from '@macanta/modules/AdminSettings/Automation/CriteriaFiltersForm';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import ActionForm from '@macanta/modules/AdminSettings/Automation/TriggerActions/SubForms/ActionForm';
import withPropHandler from '@macanta/hoc/withPropHandler';
import {dispatchResize} from '@macanta/utils/react';
import {addIdToItems} from '@macanta/modules/AdminSettings/Automation/TriggerActions/helpers';

let TriggerConditionForm = (props) => {
  const {values, setValues, setFieldValue} = props;

  const doTypesQuery = useDOTypes({
    removeEmail: true,
    includeContactObject: true,
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const handleChangeFieldValue = (val) => {
    setValues((state) => ({...state, ...val}));
  };

  return (
    <ActionForm>
      <FlexGrid
        {...props}
        cols={2}
        margin={[0, '2rem']}
        // border={[0, 1]}
        sx={{
          padding: '1rem',
          height: '100%',
          ...props.sx,
        }}>
        <Box colWidth={0.7}>
          <BasicInfoForm
            labelPosition="normal"
            actionLabel="Trigger Condition Name"
            renderOtherForms={() => (
              <FormField
                name="doTitle"
                labelPosition="normal"
                loading={doTypesQuery.initLoading}
                type="Select"
                options={doTypesQuery.result}
                onChange={(val, item) => setFieldValue('groupId', item.id)}
                labelKey="title"
                valueKey="title"
                label="Data Object Type"
                fullWidth
                size="small"
                variant="outlined"
              />
            )}
          />
        </Box>

        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
          }}>
          <BodySection
            title="Settings"
            bodyStyle={{
              padding: '1rem',
            }}>
            <FlexGrid cols={2} margin={[0, '3rem']}>
              <FormField
                colWidth="250px"
                name="restartAfter"
                // labelPosition="normal"
                label="Restart After"
                placeholder="e.g. 1 day or 1 hour or never"
                size="small"
                variant="outlined"
              />

              {/* <FormField
                colWidth="200px"
                name="unit"
                labelPosition="normal"
                type="Select"
                options={AUTOMATION_SET_TIME_UNITS}
                label="Unit"
                size="small"
                variant="outlined"
              /> */}

              <Box>
                <Typography
                  sx={{
                    fontSize: '13px',
                    marginBottom: '4px',
                  }}>
                  Trigger When File Attached
                </Typography>

                <FlexGrid fitContent margin={[0, '1.5rem']}>
                  <FormField
                    name="fileAttachedByUser"
                    labelPosition="normal"
                    type="Switch"
                    label="By User"
                  />
                  <FormField
                    name="fileAttachedByContact"
                    labelPosition="normal"
                    type="Switch"
                    label="By Contact"
                  />
                </FlexGrid>
              </Box>
            </FlexGrid>

            {/* <Divider
              sx={{
                margin: '1rem 0',
              }}
            /> */}
          </BodySection>

          <CriteriaFiltersForm
            hideContactConditions={values.groupId === 'co_customfields'}
            style={{
              margin: '1.5rem 1rem 0',
            }}
            filters={{
              doConditions: values.doConditions,
              contactConditions: values.contactConditions,
            }}
            groupId={values.groupId}
            onChange={handleChangeFieldValue}
          />
        </Box>
      </FlexGrid>
    </ActionForm>
  );
};

TriggerConditionForm = withPropHandler(TriggerConditionForm, {
  propHandlers: {
    doConditions: dispatchResize,
    contactConditions: dispatchResize,
  },
  parentPath: 'values',
  conditionCallback: (currValue, prevValue) =>
    currValue?.length !== prevValue?.length,
});

TriggerConditionForm = withForm(TriggerConditionForm, {
  mapPropsToValues: (props) => {
    const initialValues = {
      ...props.initialValues,
    };

    return {
      ...initialValues,
      groupId: initialValues.groupId || 'co_customfields',
      doTitle: initialValues.doTitle || 'Contact Object',
      fileAttachedByUser: !!initialValues.fileAttachedByUser,
      fileAttachedByContact: !!initialValues.fileAttachedByContact,
      doConditions: initialValues.doConditions
        ? addIdToItems(initialValues.doConditions)
        : [],
      contactConditions: initialValues.contactConditions
        ? addIdToItems(initialValues.contactConditions)
        : [],
    };
  },
});

export default TriggerConditionForm;
