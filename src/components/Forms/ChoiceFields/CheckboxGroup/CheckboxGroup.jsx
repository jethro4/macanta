import React from 'react';
import Box from '@mui/material/Box';
import {SelectSubHeader} from '@macanta/components/Forms/ChoiceFields/SelectField';
import {OverflowTip} from '@macanta/components/Tooltip';
import {transformValue} from '@macanta/components/Forms/InputFields/helpers';
import useTransformChoices from '@macanta/hooks/base/useTransformChoices';
import useValue from '@macanta/hooks/base/useValue';
import {sortArrayByPriority} from '@macanta/utils/array';
import * as Styled from './styles';

const CheckboxGroup = ({
  value: valueProp,
  options: optionsProp,
  getOptionLabel,
  labelKey,
  valueKey,
  readOnly,
  onChange,
  renderRightComp,
  renderItemRightComp,
  itemContainerStyle,
  sort,
  ...props
}) => {
  const [value, setValue] = useValue(() => transformValue(valueProp, true), [
    valueProp,
  ]);

  const options = useTransformChoices({
    options: optionsProp,
    sort,
    getOptionLabel,
    labelKey,
    valueKey,
  });

  const handleChange = (newVal) => {
    const sortedValue = !options.length
      ? newVal
      : sortArrayByPriority(
          options.filter((item) => newVal.includes(item.value)),
          'label',
          options.map((item) => item.label),
        ).map((item) => item.value);

    setValue(sortedValue);
    onChange && onChange(sortedValue);
  };

  return (
    <Styled.Container {...props}>
      {options.map((option, index) => {
        const checked = value.includes(option.value);

        if (readOnly && !checked) {
          return null;
        }

        const renderFormLabel = (style) => (
          <Styled.FormControlLabel
            key={option.value}
            disabled={readOnly || option.disabled}
            {...(option.label && {
              label: (
                <OverflowTip
                  style={{
                    width: '100%',
                  }}>
                  {option.label}
                </OverflowTip>
              ),
            })}
            value={option.value}
            sx={{
              ...(option.disabled && {
                '&': {
                  opacity: 0.6,
                },
              }),
            }}
            style={style}
            control={
              <Styled.Checkbox
                color="primary"
                checked={checked}
                onChange={(event) => {
                  let updatedValue;

                  if (event.target.checked) {
                    updatedValue = value.concat(option.value);
                  } else {
                    updatedValue = value.filter((v) => v !== option.value);
                  }

                  handleChange(updatedValue);
                }}
              />
            }
            {...props}
          />
        );
        let checkboxComp = null;

        if (renderItemRightComp) {
          checkboxComp = (
            <Box
              sx={{
                '.MuiFormControlLabel-root': {
                  marginRight: 0,
                },
              }}
              style={{
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'center',
                ...itemContainerStyle,
              }}
              {...(checked && {
                className: 'checkbox-selected',
              })}>
              {renderFormLabel()}
              {renderItemRightComp(option, index)}
            </Box>
          );
        } else if (!option.isSubheader) {
          checkboxComp = renderFormLabel(itemContainerStyle);
        }

        if (
          option.isSubheader &&
          !(index === options.length - 1 || options[index + 1]?.isSubheader)
        ) {
          checkboxComp = <SelectSubHeader label={option.label} />;
        }

        return checkboxComp;
      })}

      {renderRightComp && renderRightComp(value)}
    </Styled.Container>
  );
};

export default CheckboxGroup;
