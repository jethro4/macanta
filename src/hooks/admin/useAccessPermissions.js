import useQuery from '@macanta/hooks/apollo/useQuery';
import {GET_ACCESS_PERMISSIONS} from '@macanta/graphql/permissions';
import {isLoggedIn} from '@macanta/utils/app';
import get from 'lodash/get';
import * as Storage from '@macanta/utils/storage';

const useAccessPermissions = (optionsProp = {}) => {
  const userDetails = Storage.getItem('userDetails');

  const {key, ...options} = optionsProp;

  const accessPermissionsQuery = useQuery(GET_ACCESS_PERMISSIONS, {
    variables: {
      email: userDetails?.email,
    },
    skip: !isLoggedIn(),
    onError() {},
    ...options,
  });

  let accessPermissions = accessPermissionsQuery.data?.getAccessPermissions;

  if (accessPermissions && key) {
    accessPermissions = get(accessPermissions, key);
  }

  return {...accessPermissionsQuery, data: accessPermissions};
};

export default useAccessPermissions;
