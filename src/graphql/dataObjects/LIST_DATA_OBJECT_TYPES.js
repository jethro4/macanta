import {gql} from '@apollo/client';

export default gql`
  query listDataObjectTypes(
    $appName: String!
    $apiKey: String!
    $includeContactObject: Boolean
    $userEmail: String
  ) {
    listDataObjectTypes(
      appName: $appName
      apiKey: $apiKey
      includeContactObject: $includeContactObject
      userEmail: $userEmail
    ) {
      id
      title
    }
  }
`;
