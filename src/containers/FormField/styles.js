import {experimentalStyled as styled} from '@mui/material/styles';

import FormsComp from '@macanta/components/Forms';
import TextAreaFieldComp from '@macanta/components/Forms/InputFields/TextAreaField';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Button, {IconButton} from '@macanta/components/Button';
import * as applicationStyles from '@macanta/themes/applicationStyles';

export const Root = styled(Box)`
  display: flex;
  flex-direction: column;
  margin-bottom: ${({theme}) => theme.spacing(3)};
`;

export const LabelContainer = styled(Box)`
  display: flex;
  align-items: flex-end;
  margin-bottom: ${({theme}) => theme.spacing(0.5)};
`;

export const Label = styled(Typography)``;

export const HeadingTextContainer = styled(Box)`
  padding: ${({theme}) => theme.spacing(0.5)} 0 ${({theme}) => theme.spacing(1)};
  margin-bottom: ${({theme}) => theme.spacing(1)};
  border-bottom: 1px solid ${({theme}) => theme.palette.secondary.main};
`;

export const HeadingText = styled(Typography)`
  font-weight: bold;
  color: ${({theme}) => theme.palette.primary.main};
`;

export const Value = styled(Typography)`
  font-size: 0.875rem;
  color: ${({theme}) => theme.palette.primary.main};
  ${({type}) => (type === 'URL' ? applicationStyles.oneLineText : '')}
`;

export const TextField = styled(FormsComp)``;

export const TextAreaField = styled(TextAreaFieldComp)`
  min-height: 400px;
  background-color: ${({theme}) => theme.palette.common.white};
`;

export const TopComp = styled(Box)`
  display: flex;
  justify-content: space-between;
  align-items: flex-end;
`;

export const HelperTextBtn = styled(IconButton)`
  margin-left: ${({theme}) => theme.spacing(1)};
  padding: 0;

  & .MuiSvgIcon-root {
    color: ${({theme}) => theme.palette.info.main};
    font-size: 1.3rem;
  }
`;

export const HelperText = styled(Typography)`
  white-space: pre-line;
`;

export const ExpandButton = styled(Button)`
  align-self: flex-end;
  padding: 0;
  margin-top: ${({theme}) => theme.spacing(0.5)};
`;

export const ExpandDoneButton = styled(Button)`
  align-self: flex-end;
  margin: ${({theme}) => theme.spacing(2)} 4px 4px;
`;

export const Divider = styled(Box)`
  border-bottom: 2px dotted #ccc;
  margin: 0 ${({theme}) => theme.spacing(1)} ${({theme}) => theme.spacing(2)};
`;

export const UrlFieldBtn = styled(IconButton)`
  color: ${({theme}) => theme.palette.info.main};
  padding: 0;
  margin-bottom: 0.25rem;
  margin-left: 0.5rem;
`;
