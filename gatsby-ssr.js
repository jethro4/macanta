/* eslint-disable react/jsx-filename-extension */

/**
 * Implement Gatsby's SSR (Server Side Rendering) APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/ssr-apis/
 */

import React from 'react';
import ApolloProviderWrapper from '@macanta/containers/ApolloProviderWrapper';
// react-grid-layout stylesheets
import 'react-grid-layout/css/styles.css';
import 'react-resizable/css/styles.css';
// END react-grid-layout stylesheets

const RootElementWrapper = ({element}) => {
  return <ApolloProviderWrapper>{element}</ApolloProviderWrapper>;
};

export const wrapRootElement = RootElementWrapper;

export const onRenderBody = ({setPostBodyComponents}) => {
  setPostBodyComponents([
    <link
      type="text/css"
      rel="stylesheet"
      key="/helpSupportStyle.css"
      href="/helpSupportStyle.css"
    />,
    <script key="/helpSupportScript.js" src="/helpSupportScript.js" />,
    <script
      key="https://euc-widget.freshworks.com/widgets/77000003397.js"
      src="https://euc-widget.freshworks.com/widgets/77000003397.js"
      async
      defer
    />,
  ]);
};
