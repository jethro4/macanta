import React, {useState, useEffect} from 'react';
import Typography from '@mui/material/Typography';
import FormField from '@macanta/containers/FormField';
import {sortArrayByPriority} from '@macanta/utils/array';
import {
  getContactRelationships,
  getFilteredRelationshipsByLimit,
  getRelationshipOptions,
} from '@macanta/selectors/relationship.selector';

const MultiDORelationshipSelect = ({
  connectedContacts,
  relationships,
  onChange,
}) => {
  const [filteredRelationships, setFilteredRelationships] = useState([]);
  const [selectedRoles, setSelectedRoles] = useState([]);

  const allRelationshipRoles = relationships.map((r) => r.role);
  const relationshipOptions = getRelationshipOptions(
    allRelationshipRoles,
    [],
    filteredRelationships,
  );

  const handleChange = (newVal) => {
    onChange(newVal);
    setSelectedRoles(newVal);
  };

  const handleRenderValue = (selected) => {
    const sortedRenderedValue = sortArrayByPriority(
      selected,
      null,
      allRelationshipRoles,
    );

    return sortedRenderedValue.length > 2
      ? `${sortedRenderedValue.length} selected`
      : sortedRenderedValue.join(', ');
  };

  useEffect(() => {
    if (connectedContacts && relationships.length) {
      const contactRelationships = getContactRelationships(connectedContacts);
      const filteredRelationshipsByLimit = getFilteredRelationshipsByLimit(
        contactRelationships,
        relationships,
      );

      setFilteredRelationships(filteredRelationshipsByLimit);
    }
  }, [connectedContacts, relationships]);

  return relationshipOptions.length ? (
    <FormField
      style={{
        width: 220,
        textAlign: 'left',
        marginBottom: 0,
      }}
      type="MultiSelect"
      options={relationshipOptions}
      renderValue={handleRenderValue}
      // error={errors[field.name]}
      onChange={handleChange}
      variant="outlined"
      placeholder="None selected"
      value={selectedRoles}
      fullWidth
      size="small"
    />
  ) : (
    <Typography align="center" color="#aaa">
      Relationships limit reached
    </Typography>
  );
};

MultiDORelationshipSelect.defaultProps = {
  connectedContacts: [],
  relationships: [],
};

export default MultiDORelationshipSelect;
