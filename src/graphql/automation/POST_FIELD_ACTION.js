import {gql} from '@apollo/client';
import {FIELD_ACTIONS_FRAGMENT} from '@macanta/graphql/automation/GET_FIELD_ACTIONS';

export default gql`
  mutation postFieldAction($input: FieldActionInput!) {
    postFieldAction(input: $input) {
      ...fieldActionsFragment
    }
  }
  ${FIELD_ACTIONS_FRAGMENT}
`;
