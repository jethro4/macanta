import React from 'react';

const withShow = (WrappedComponent, conditionCallback) => {
  const EnhancedComponent = (props) => {
    const shouldShow = conditionCallback(props);

    return !shouldShow ? null : <WrappedComponent {...props} />;
  };

  return EnhancedComponent;
};

export default withShow;
