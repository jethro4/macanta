import React, {useState, useMemo} from 'react';
import {useFormikContext} from 'formik';
import DraftsIcon from '@mui/icons-material/Drafts';
import SearchIcon from '@mui/icons-material/Search';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import Button from '@macanta/components/Button';
import Form from '@macanta/components/Form';
import Modal from '@macanta/components/Modal';
import Section from '@macanta/containers/Section';
import FormField from '@macanta/containers/FormField';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import useLazyFetch from '@macanta/hooks/base/useLazyFetch';
import useNextEffect from '@macanta/hooks/base/useNextEffect';
import useAccessPermissions from '@macanta/hooks/admin/useAccessPermissions';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
// import dataImportsValidationSchema from '@macanta/validations/dataImports';
import {uncapitalizeFirstLetter} from '@macanta/utils/string';
import {getAppInfo} from '@macanta/utils/app';
import SingleContactSearch from './SingleContactSearch';
import SingleQuerySearch from './SingleQuerySearch';
import * as NoteTaskFormsStyled from '@macanta/modules/NoteTaskForms/styles';
import * as FormFieldStyled from '@macanta/containers/FormField/styles';
import * as Storage from '@macanta/utils/storage';
import envConfig from '@macanta/config/envConfig';

const EXPORT_OPTIONS = [
  {
    label: 'All Contacts & Queries',
    value: 'All',
  },
  {
    label: 'Single Contact',
    value: 'SingleContact',
  },
  {
    label: 'Single Query',
    value: 'Query',
  },
];

const EXPORT_TYPES = [
  {
    label: 'Contacts',
    value: 'Contact',
  },
  {
    label: 'Notes & Tasks',
    value: 'NoteTask',
  },
  {
    label: 'Data Objects',
    value: 'DataObject',
  },
];

const ExportSectionContainer = ({initialValues, onSuccess, ...props}) => {
  const {displayMessage} = useProgressAlert();

  const [callFetch, fetchState] = useLazyFetch();

  const handleFormSubmit = async (values, actions) => {
    const session = Storage.getItem('session');
    const [appName, apiKey] = getAppInfo();

    const {data} = await callFetch(
      `https://${appName}.${envConfig.apiDomain}/rest/v1/data_tools/export`,
      {
        method: 'POST',
        body: {
          api_key: apiKey,
          sessionName: session?.sessionId,
          exportType: values.option,
          includes: values.exportTypes,
          typeId: values.selectedData?.id,
        },
      },
    );

    if (data?.status === 'success') {
      displayMessage(
        'Your Data Export has been created.<br>' +
          "You'll receive an email once the export has been processed,<br>" +
          'with links to download the exported data.',
        8000,
      );

      onSuccess && onSuccess();

      actions.resetForm({
        values: {
          option: 'All',
          exportTypes: [],
          selectedData: null,
        },
      });
    }
  };

  return (
    <>
      <Form
        initialValues={{
          option: initialValues?.option || 'All',
          exportTypes: initialValues?.exportTypes || [],
          selectedData: initialValues?.selectedData || null,
        }}
        // validationSchema={dataImportsValidationSchema}
        onSubmit={handleFormSubmit}>
        <ExportSection {...props} />
      </Form>
      <LoadingIndicator modal loading={fetchState.loading} />
    </>
  );
};

const ExportSection = (props) => {
  const {
    initialValues,
    values,
    errors,
    handleChange,
    setValues,
    setFieldValue,
    setErrors,
    handleSubmit,
    dirty,
  } = useFormikContext();

  const [showModal, setShowModal] = useState(false);

  const accessPermissionsQuery = useAccessPermissions({
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const permissionMeta = accessPermissionsQuery.data?.permissionMeta;

  const hasInitialValues = initialValues.option !== 'All';
  const isSingleContact = values.option === 'SingleContact';
  const fromSingleType = ['SingleContact', 'Query'].includes(values.option);
  const singleType = fromSingleType
    ? isSingleContact
      ? 'Contact'
      : 'Query'
    : '';
  const selectedDataText =
    values.selectedData &&
    (isSingleContact
      ? `${values.selectedData.email} (${values.selectedData.id})`
      : `${values.selectedData.name} (${values.selectedData.id})`);
  const selectedIds = values.selectedData ? [values.selectedData.id] : [];

  const handleShowModal = () => {
    setShowModal(true);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  const handleSelectData = (item) => {
    setFieldValue('selectedData', item);
    handleCloseModal();
  };

  const handleReset = () => {
    setValues({option: 'All', exportTypes: [], selectedData: null});
    setErrors({});
  };

  useNextEffect(() => {
    if (values.selectedData) {
      setFieldValue('selectedData', null);
    }
  }, [singleType]);

  const exportTypeOptions = useMemo(() => {
    return EXPORT_TYPES.map((item) => {
      const enableOption =
        (item.value === 'Contact' &&
          permissionMeta.allowContactExport === true) ||
        (item.value === 'DataObject' &&
          permissionMeta.allowDataObjectExport === true) ||
        (item.value === 'NoteTask' &&
          permissionMeta.allowNoteTaskExport === true);

      return {
        ...item,
        disabled: !enableOption,
      };
    });
  }, [permissionMeta]);

  const allOptionsDisabled =
    exportTypeOptions.filter((o) => !!o.disabled).length ===
    exportTypeOptions.length;
  const enableExport =
    !!values.exportTypes && (!fromSingleType || !!values.selectedData);

  return (
    <Section
      fullHeight
      style={{
        marginTop: 0,
      }}
      headerStyle={{
        backgroundColor: 'white',
      }}
      bodyStyle={{
        padding: '1rem',
      }}
      title="Export Data"
      FooterComp={
        <NoteTaskFormsStyled.Footer
          style={{
            padding: '1rem',
            margin: 0,
            width: '100%',
            justifyContent: 'space-between',
          }}>
          <Box>
            {!hasInitialValues && (
              <Button
                disabled={!dirty}
                variant="outlined"
                onClick={handleReset}
                size="medium">
                Reset
              </Button>
            )}
          </Box>

          <Button
            disabled={!dirty || !enableExport}
            variant="contained"
            startIcon={<DraftsIcon />}
            onClick={handleSubmit}
            size="medium">
            Export
          </Button>
        </NoteTaskFormsStyled.Footer>
      }
      {...props}>
      <FormField
        style={{
          marginBottom: 0,
        }}
        required
        type="Checkbox"
        options={exportTypeOptions}
        value={values.exportTypes}
        error={errors.exportTypes}
        onChange={(val) => setFieldValue('exportTypes', val)}
        label="Data Export Types"
        fullWidth
        size="small"
        variant="outlined"
      />

      {!hasInitialValues && (
        <>
          <FormFieldStyled.Divider
            style={{
              margin: '1rem 0 1.5rem',
            }}
          />

          <FormField
            disabled={allOptionsDisabled}
            style={{
              marginBottom: 0,
            }}
            required
            type="Select"
            options={EXPORT_OPTIONS}
            value={values.option}
            error={errors.option}
            onChange={handleChange('option')}
            label="Include From"
            fullWidth
            size="small"
            variant="outlined"
          />

          {fromSingleType && (
            <>
              <FormFieldStyled.Divider
                style={{
                  margin: '1.9rem 0 1.4rem',
                }}
              />
              <Box
                style={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                <Box>
                  <Box
                    style={{
                      display: 'flex',
                    }}>
                    <FormFieldStyled.Label variant="caption">
                      {singleType}
                    </FormFieldStyled.Label>
                    <Typography
                      style={{
                        marginLeft: '4px',
                        lineHeight: '1.180625rem',
                      }}
                      component="span"
                      color="error">
                      *
                    </Typography>
                  </Box>
                  <Typography
                    sx={{
                      color: selectedDataText ? 'success.main' : '#999',
                    }}
                    style={{
                      fontSize: '0.75rem',
                    }}>
                    {selectedDataText || (
                      <>No selected {uncapitalizeFirstLetter(singleType)}</>
                    )}
                  </Typography>
                </Box>
                <Button
                  variant="contained"
                  color="info"
                  startIcon={<SearchIcon />}
                  onClick={handleShowModal}
                  size="small">
                  Select
                </Button>
              </Box>
            </>
          )}

          <Modal
            disableScroll
            headerTitle={`Select ${singleType}`}
            open={showModal}
            onClose={handleCloseModal}
            contentWidth={650}
            contentHeight={700}>
            {isSingleContact ? (
              <SingleContactSearch
                selectedContactIds={selectedIds}
                onSuccess={handleSelectData}
              />
            ) : (
              <SingleQuerySearch
                selectedQueryIds={selectedIds}
                onSuccess={handleSelectData}
              />
            )}
          </Modal>
        </>
      )}
    </Section>
  );
};

export default ExportSectionContainer;
