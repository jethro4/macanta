import {experimentalStyled as styled} from '@mui/material/styles';

import MUIPaper from '@mui/material/Paper';

export const Root = styled(MUIPaper)`
  display: flex;
  flex-direction: column;
  padding: ${({theme}) => theme.spacing(2)};
`;
