import {
  ContactActionsForm,
  ContactActionsSection,
  ContactActionsSelect,
} from '@macanta/modules/AdminSettings/Automation/TriggerActions/ContactActions';
import {
  EmailActionsForm,
  EmailActionsSection,
  EmailActionsSelect,
} from '@macanta/modules/AdminSettings/Automation/TriggerActions/EmailActions';
import {
  FieldActionsForm,
  FieldActionsSection,
  FieldActionsSelect,
} from '@macanta/modules/AdminSettings/Automation/TriggerActions/FieldActions';
import {
  SmsActionsForm,
  SmsActionsSection,
  SmsActionsSelect,
} from '@macanta/modules/AdminSettings/Automation/TriggerActions/SmsActions';
import {
  UserActionsForm,
  UserActionsSection,
  UserActionsSelect,
} from '@macanta/modules/AdminSettings/Automation/TriggerActions/UserActions';
import {
  WebhookActionsForm,
  WebhookActionsSection,
  WebhookActionsSelect,
} from '@macanta/modules/AdminSettings/Automation/TriggerActions/WebhookActions';

export const TRIGGER_ACTION_TYPES = Object.freeze([
  {
    label: 'Email',
    type: 'email',
    component: EmailActionsSection,
    formComponent: EmailActionsForm,
    selectComponent: EmailActionsSelect,
  },
  {
    label: 'SMS',
    type: 'sms',
    component: SmsActionsSection,
    formComponent: SmsActionsForm,
    selectComponent: SmsActionsSelect,
  },
  {
    label: 'Field',
    type: 'field',
    component: FieldActionsSection,
    formComponent: FieldActionsForm,
    selectComponent: FieldActionsSelect,
  },
  {
    label: 'Contact',
    type: 'contact',
    component: ContactActionsSection,
    formComponent: ContactActionsForm,
    selectComponent: ContactActionsSelect,
  },
  {
    label: 'User',
    type: 'user',
    component: UserActionsSection,
    formComponent: UserActionsForm,
    selectComponent: UserActionsSelect,
  },
  {
    label: 'Webhook',
    type: 'webhook',
    component: WebhookActionsSection,
    formComponent: WebhookActionsForm,
    selectComponent: WebhookActionsSelect,
  },
]);

export const DERIVATION_FIELDS_MAPPING = Object.freeze({
  Addition: {
    variable1: 'Field 1',
    variable2: 'Field 2',
    result: 'Result Field',
  },
  Substraction: {
    variable1: 'Field 1',
    variable2: 'Field 2',
    result: 'Result Field',
  },
  Multiplication: {
    variable1: 'Field 1',
    variable2: 'Field 2',
    result: 'Result Field',
  },
  Division: {
    variable1: 'Field 1',
    variable2: 'Field 2',
    result: 'Result Field',
  },
  TicketNumber: {
    ticketNumber: 'Ticket Number',
    ticketNumberPrefix: 'Prefix',
    result: 'Result Field',
  },
  ActionCounter: {
    result: 'Result Field',
  },
  'HOWOLD/HOWLONG': {
    variable1: 'Input Field',
    timeUnitFormat: 'Formatting',
    result: 'Result Field',
  },
  NextBirthday: {
    variable1: 'Date Field',
    birthdayResultFormat: 'Date Format',
    result: 'Result Field',
  },
  TimeDifferenceFields: {
    variable1: 'Date/Time Field 1',
    variable2: 'Date/Time Field 2',
    result: 'Result Field',
  },
  HumaniseDates: {
    variable1: 'Date Field',
    fieldNameFormat: 'Date Format',
    result: 'Result Field',
  },
  ZeroData: {
    fieldName: 'Field',
  },
  NumberDate: {
    dateNumber: 'Date',
    dateDay: 'Day',
    dateMonth: 'Month',
    requestDateField: 'Date Field',
    result: 'Result Field',
  },
  AddToDate: {
    dayCount: 'Days',
    weekCount: 'Weeks',
    monthCount: 'Months',
    yearCount: 'Years',
    requestDateField: 'Date Field',
    fieldNameFormat: 'Date Format',
    result: 'Result Field',
  },
  NextAvailableNumber: {
    result: 'Result Field',
  },
  RoundRobin: {
    contactListId: 'Contact List ID',
  },
});

export const DERIVATION_FIELD_KEY_TYPES_MAPPING = Object.freeze({
  ticketNumber: 'text',
  ticketNumberPrefix: 'text',
  contactListId: 'text',
  dayCount: 'number',
  weekCount: 'number',
  monthCount: 'number',
  yearCount: 'number',
  dateNumber: 'dateNumbers',
  dateDay: 'dateDays',
  dateMonth: 'dateMonths',
  requestDateField: 'dateFields',
  fieldNameFormat: 'dateFormats',
  birthdayResultFormat: 'dateFormats',
  timeUnitFormat: [
    'Years',
    'Years and Months',
    'Years, Months and Days',
    'Days',
    'Days Number Only',
  ],
  fieldName: 'fields',
  variable1: 'fields',
  variable2: 'fields',
  result: 'fields',
});

export const ACTION_FIELDS_MAPPING = {
  email: 'emailActionId',
  sms: 'smsActionId',
  field: 'fieldActionId',
  contact: 'contactActionId',
  user: 'userActionId',
  webhook: 'webhookActionId',
};

export const FIELD_ACTION_TYPES = [
  {
    isSubheader: true,
    label: 'Arithmetic',
  },
  {
    label: 'Addition',
    value: 'Addition',
    fieldKeys: ['variable1', 'variable2', 'result'],
  },
  {
    label: 'Substraction',
    value: 'Substraction',
    fieldKeys: ['variable1', 'variable2', 'result'],
  },
  {
    label: 'Multiplication',
    value: 'Multiplication',
    fieldKeys: ['variable1', 'variable2', 'result'],
  },
  {
    label: 'Division',
    value: 'Division',
    fieldKeys: ['variable1', 'variable2', 'result'],
  },
  {
    isSubheader: true,
    label: 'Other Transforms',
  },
  {
    label: 'Ticket Number',
    value: 'TicketNumber',
    fieldKeys: ['ticketNumber', 'ticketNumberPrefix', 'result'],
  },
  {
    label: 'Action Counter',
    value: 'ActionCounter',
    fieldKeys: ['result'],
  },
  {
    label: 'How Old / How Long',
    value: 'HOWOLD/HOWLONG',
    fieldKeys: ['variable1', 'timeUnitFormat', 'result'],
  },
  {
    label: 'Next Birthday',
    value: 'NextBirthday',
    fieldKeys: ['variable1', 'birthdayResultFormat', 'result'],
  },
  {
    label: 'Time Difference Fields',
    value: 'TimeDifferenceFields',
    fieldKeys: ['variable1', 'variable2', 'result'],
  },
  {
    label: 'Humanise Dates',
    value: 'HumaniseDates',
    fieldKeys: ['variable1', 'fieldNameFormat', 'result'],
  },
  {label: 'Zero Data', value: 'ZeroData', fieldKeys: ['fieldName']},
  {
    label: 'Number Date',
    value: 'NumberDate',
    fieldKeys: [
      'requestDateField',
      'dateMonth',
      'dateNumber',
      // 'dateDay',
      'result',
    ],
  },
  {
    label: 'Add To Date',
    value: 'AddToDate',
    fieldKeys: [
      'dayCount',
      'weekCount',
      'monthCount',
      'yearCount',
      'requestDateField',
      'fieldNameFormat',
      'result',
    ],
  },
  {
    label: 'Next Available Number',
    value: 'NextAvailableNumber',
    fieldKeys: ['result'],
  },
  {
    label: 'Round Robin',
    value: 'RoundRobin',
    fieldKeys: ['contactListId'],
  },
];

export const AUTOMATION_SET_TIME_UNITS = [
  'Minutes',
  'Hours',
  'Days',
  'Weeks',
  'Months',
];
