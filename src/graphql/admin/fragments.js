import {gql} from '@apollo/client';

export const WIDGET_ATTRIBUTES = gql`
  fragment widgetResults on Widget {
    id
    userId
    queryId
    type
    title
    access
    addedBy
    savedSearchID
    groupName
  }
`;

export const WIDGET_COUNT_FRAGMENT = gql`
  fragment widgetCountResults on Widget {
    currentCount
    last7DayCount
    last30DayCount
    last90DayCount
    allCount
  }
`;

export const QUERY_BUILDER_ITEM_ATTRIBUTES = gql`
  fragment queryBuilderItem on QueryBuilder {
    id
    doType
    name
    description
    doConditions {
      name
      logic
      operator
      values
      value
    }
    contactConditions {
      relationship
      logic
    }
    userConditions {
      relationship
      logic
      operator
      userId
    }
    chosenFields
    criteriaMessageA
    criteriaMessageB
  }
`;

export const TRIGGER_CONDITION_ITEM_ATTRIBUTES = gql`
  fragment triggerConditionItem on TriggerCondition {
    id
    doType
    name
    description
    restartAfter
    doConditions {
      name
      logic
      operator
      values
      value
    }
    contactConditions {
      relationship
      logic
    }
  }
`;
export const WIDGET_OPTION_ATTRIBUTES = gql`
  fragment widgetOptionResults on WidgetOption {
    id
    userId
    queryId
    type
    title
    access
  }
`;

export const WIDGET_DATA_FRAGMENT = gql`
  fragment WidgetDataFragment on WidgetData {
    value
  }
`;

export const LIST_ASSIGNMENT_FRAGMENT = gql`
  fragment listAssignmentFragment on ListAssignments {
    id
    assignmentType
    unassignmentBehavior
    selectedRoundRobin
    selectedUsers
    assignedRelationship
    status
  }
`;

export const CUSTOM_TAB_FRAGMENT = gql`
  fragment customTabFragment on CustomTab {
    id
    title
    content
    hasLink
  }
`;

export const CUSTOM_TAB_ITEM_FRAGMENT = gql`
  fragment customTabItemFragment on CustomTabItem {
    id
    title
  }
`;

export const WORKFLOW_BOARD_FRAGMENT = gql`
  fragment workflowBoardFragment on WorkflowBoard {
    id
    title
    description
    type
    displayFields
    keyField
    relationship
    activeColumnsColor
  }
`;

export const FORM_BUILDER_GROUP_FRAGMENT = gql`
  fragment formBuilderGroupFragment on FormBuilderGroup {
    subGroupName
    fields {
      ... on FormBuilderField {
        fieldId
        label
        tooltip
        required
        column
      }
      ... on UploadFormBuilderField {
        fieldId
        label
        tooltip
        required
        column
        type
        referenceFieldId
        multiple
        mimeTypes
      }
    }
    columns
  }
`;

export const FORM_BUILDER_FRAGMENT = gql`
  fragment formBuilderFragment on FormBuilder {
    id
    title
    description
    thankMessage
    type
    contactGroups {
      ...formBuilderGroupFragment
    }
    dataObject {
      dataObjectId
      dataObjectGroups {
        ...formBuilderGroupFragment
      }
      relationships
      integrationField {
        limit
        displayFields
        conditions {
          name
          logic
          operator
          values
          value
        }
        type
        order
        orderBy
      }
    }
    customCSS
    redirectUrl
    submitLabel
    jsCode
    iFrameCode
    htmlCode
    link
  }
  ${FORM_BUILDER_GROUP_FRAGMENT}
`;
