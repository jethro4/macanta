import React from 'react';
import {navigate} from 'gatsby';
import EmailIcon from '@mui/icons-material/Email';
import SettingCard from '@macanta/modules/AdminSettings/SettingCard';

const BlastProgressAdminCard = () => {
  /* const navigate = useNavigate();*/

  const handleBlastProgress = () => {
    navigate('/app/admin-settings/blast-progress');
  };

  return (
    <SettingCard
      IconComp={EmailIcon}
      title="Blast Progress"
      description="Keep track of progress details from all user emails and sms in queue"
      onClick={handleBlastProgress}
    />
  );
};

export default BlastProgressAdminCard;
