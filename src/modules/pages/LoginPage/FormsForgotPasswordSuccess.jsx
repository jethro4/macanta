import React from 'react';

import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import Button from '@macanta/components/Button';
import FormHeader from './FormHeader';

const FormsForgotPasswordSuccess = ({onLogin}) => {
  return (
    <>
      <FormHeader
        title="Reset Password Success!"
        description="You may now login with your new password."
      />
      <Button
        style={{
          marginTop: '1rem',
        }}
        onClick={onLogin}
        startIcon={
          <ChevronLeftIcon
            style={{
              fontSize: '1.5rem',
            }}
          />
        }>
        Log in now
      </Button>
    </>
  );
};

export default FormsForgotPasswordSuccess;
