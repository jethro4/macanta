import {gql} from '@apollo/client';

export default gql`
  query getNotificationsStatus($userId: ID, $type: String) {
    getNotificationsStatus(userId: $userId, type: $type) {
      status
      startTime
      duration
    }
  }
`;
