import React, {useState, useLayoutEffect} from 'react';
import * as Styled from './styles';
import ShowContext from './ShowContext';

const Show = ({children, removeHidden, style, in: inProp, ...props}) => {
  const [visible, setVisible] = useState(false);

  useLayoutEffect(() => {
    setVisible(!!inProp);
  }, [inProp]);

  if (removeHidden && !visible) {
    return null;
  }

  return (
    <ShowContext.Provider
      value={{
        visible,
      }}>
      <Styled.Root visible={visible} style={style} {...props}>
        {children}
      </Styled.Root>
    </ShowContext.Provider>
  );
};

Show.defaultProps = {
  in: false,
};

export default Show;
