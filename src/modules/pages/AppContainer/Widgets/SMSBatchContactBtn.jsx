import React, {useState} from 'react';
import Modal from '@macanta/components/Modal';
import SMSBatchContactForms from './SMSBatchContactForms';
import TextsmsIcon from '@mui/icons-material/Textsms';
import Button from '@macanta/components/Button';
import useProgressAlert from '@macanta/hooks/useProgressAlert';

const SMSBatchContactBtn = ({addedWidgets, ...props}) => {
  const [showModal, setShowModal] = useState(false);
  const {displayMessage} = useProgressAlert();

  const handleAddContact = () => {
    setShowModal(true);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  const onSuccess = () => {
    displayMessage('Batch SMS sent!');
    setShowModal(false);
  };

  return (
    <>
      <Button
        disabled={!addedWidgets.length}
        onClick={handleAddContact}
        size="small"
        variant="contained"
        startIcon={<TextsmsIcon />}
        {...props}>
        Send SMS
      </Button>
      <Modal
        headerTitle={`Send SMS`}
        open={showModal}
        onClose={handleCloseModal}
        contentWidth={500}>
        <SMSBatchContactForms widgets={addedWidgets} onSuccess={onSuccess} />
      </Modal>
    </>
  );
};

SMSBatchContactBtn.defaultProps = {
  addedWidgets: [],
};

export default SMSBatchContactBtn;
