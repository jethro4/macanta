import React, {useState, useEffect} from 'react';
import {useQuery, useMutation} from '@apollo/client';
import EditIcon from '@mui/icons-material/Edit';
import PreviewIcon from '@mui/icons-material/Preview';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import Form from '@macanta/components/Form';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import useNextEffect from '@macanta/hooks/base/useNextEffect';
import {FORM_TYPES} from '@macanta/constants/form';
import {
  CREATE_OR_UPDATE_FORM_BUILDER,
  GET_FORM_BUILDER,
  LIST_FORM_BUILDERS,
} from '@macanta/graphql/admin';
import formBuilderValidationSchema from '@macanta/validations/formBuilder';
import BuilderForms from './BuilderForms';

export const BUILDER_MODES = [
  {label: 'Edit', iconComp: <EditIcon />},
  {label: 'Preview', iconComp: <PreviewIcon />},
];

const BuilderRootContainer = ({item, ...props}) => {
  const formBuildersQuery = useQuery(GET_FORM_BUILDER, {
    variables: {
      id: item?.id,
    },
    skip: !item?.id,
  });

  const formBuilder = formBuildersQuery.data?.getFormBuilder;

  const initLoading = !formBuilder && formBuildersQuery.loading;

  return !initLoading ? (
    <BuilderRoot initLoading={initLoading} item={formBuilder} {...props} />
  ) : (
    <LoadingIndicator
      fill
      align="top"
      style={{
        top: '2.5rem',
      }}
      loading={initLoading}
    />
  );
};

const BuilderRoot = ({initLoading, item, onClose, ...props}) => {
  const {displayMessage} = useProgressAlert();

  const [defaultCustomCSS, setDefaultCustomCSS] = useState('');

  const [
    callCreateOrUpdateFormBuilder,
    {loading: savingFormBuilder},
  ] = useMutation(CREATE_OR_UPDATE_FORM_BUILDER, {
    update(cache, {data: {createOrUpdateFormBuilder}}) {
      cache.modify({
        fields: {
          listFormBuilders(listFormBuilderRef) {
            cache.modify({
              id: listFormBuilderRef.__ref,
              fields: {
                items(existingItems = []) {
                  const newItemRef = cache.writeQuery({
                    data: createOrUpdateFormBuilder,
                    query: LIST_FORM_BUILDERS,
                  });

                  return [...existingItems, newItemRef];
                },
              },
            });
          },
        },
      });
    },
    onError() {},
    onCompleted: (data) => {
      if (data?.createOrUpdateFormBuilder) {
        displayMessage('Saved successfully!');

        if (!item) {
          onClose();
        }
      }
    },
  });

  const getInitValues = () => {
    let contactGroups = item?.contactGroups || [
      {
        subGroupName: 'General',
        fields: [
          {
            fieldId: 'field_email',
            label: 'Email',
            tooltip: 'This will be used as your ID',
            required: true,
            column: 1,
          },
        ],
        columns: 3,
      },
    ];
    let doGroups = item?.dataObject?.dataObjectGroups || [];

    contactGroups = contactGroups.map((group) => ({
      id: `subGroup-${group.subGroupName}`,
      ...group,
    }));
    doGroups = doGroups.map((group) => ({
      id: `subGroup-${group.subGroupName}`,
      ...group,
    }));

    return {
      id: item?.id,
      title: item?.title || '',
      description: item?.description || '',
      redirectUrl: item?.redirectUrl || '',
      thankMessage: item?.thankMessage || '',
      type: item?.type || FORM_TYPES[0].value,
      doType: item?.dataObject?.dataObjectId || 'contactsOnly',
      contactGroups,
      doGroups: doGroups.map((group) => ({
        ...group,
        fields: group.fields.map((field, index) => {
          if (field.fieldId === 'field_integration') {
            return {
              ...field,
              id: field.fieldId,
              type: item?.dataObject?.integrationField?.type,
              choices: ['Sample 1', 'Sample 2'],
            };
          }

          const newId = String(Date.now()) + index;
          const fieldId = field.fieldId || newId;

          return {
            ...field,
            id: fieldId,
            fieldId,
          };
        }),
      })),
      relationships: item?.dataObject?.relationships || [],
      integrationField: item?.dataObject?.integrationField
        ? {
            ...item?.dataObject?.integrationField,
            conditions: item?.dataObject?.integrationField.conditions.map(
              (condition, index) => ({
                id: condition.name + index,
                ...condition,
              }),
            ),
          }
        : {
            limit: 5,
            displayFields: '',
            conditions: [],
            type: 'Select',
            order: '',
            orderBy: '',
          },
      customCSS: defaultCustomCSS, //item?.customCSS || defaultCustomCSS,
      selectedMode: BUILDER_MODES[0].label,
      submitLabel: item?.submitLabel || 'Submit',
      jsCode: item?.jsCode || '',
      iFrameCode: item?.iFrameCode || '',
      htmlCode: item?.htmlCode || '',
      link: item?.link || '',
    };
  };

  const [initValues, setInitValues] = useState(() => getInitValues());

  const handleFormSubmit = async (values) => {
    callCreateOrUpdateFormBuilder({
      variables: {
        createOrUpdateFormBuilderInput: {
          id: values.id,
          title: values.title,
          description: values.description,
          redirectUrl: values.redirectUrl,
          thankMessage: values.thankMessage,
          type: values.type,
          contactGroups: values.contactGroups.map((group) => ({
            subGroupName: group.subGroupName,
            fields: group.fields.map((field) => ({
              fieldId: field.fieldId,
              label: field.label,
              tooltip: field.tooltip,
              required: field.required,
              column: field.column,
            })),
            columns: group.columns,
          })),
          customCSS: values.customCSS,
          submitLabel: values.submitLabel,
          ...(values.doType !== 'contactsOnly' && {
            dataObject: {
              dataObjectId: values.doType,
              dataObjectGroups: values.doGroups.map((group) => ({
                subGroupName: group.subGroupName,
                fields: group.fields.map((field) => ({
                  fieldId: field.fieldId,
                  label: field.label,
                  tooltip: field.tooltip,
                  required: field.required,
                  column: field.column,
                  ...(field.type === 'file' && {
                    type: field.type,
                    ...(field.referenceFieldId
                      ? {
                          referenceFieldId: field.referenceFieldId,
                        }
                      : {
                          multiple: field.multiple,
                          mimeTypes: field.mimeTypes,
                        }),
                  }),
                })),
                columns: group.columns,
              })),
              relationships: values.relationships,
              ...(values.type === 'Integrated' && {
                integrationField: {
                  limit: values.integrationField.limit,
                  type: values.integrationField.type,
                  order: values.integrationField.order,
                  orderBy: values.integrationField.orderBy,
                  displayFields: values.integrationField.displayFields,
                  conditions: values.integrationField.conditions.map(
                    (condition) => ({
                      name: condition.name,
                      logic: condition.logic,
                      operator: condition.operator,
                      values: condition.values,
                      value: condition.value,
                    }),
                  ),
                },
              }),
            },
          }),
        },
        __mutationkey: 'createOrUpdateFormBuilderInput',
        removeAppInfo: true,
      },
    });
  };

  useNextEffect(() => {
    if (item || defaultCustomCSS) {
      setInitValues(getInitValues());
    }
  }, [item, defaultCustomCSS]);

  useEffect(() => {
    if (!initLoading) {
      fetch(`/formStyles.css?date=${Date.now()}`)
        .then((res) => res.text())
        .then((res) => {
          setDefaultCustomCSS(res);
        })
        .catch((err) => {
          console.error(err);
        })
        .finally(() => {});
    }
  }, [initLoading]);

  return (
    <>
      <Form
        initialValues={initValues}
        enableReinitialize
        validationSchema={formBuilderValidationSchema}
        onSubmit={handleFormSubmit}>
        {({values}) => {
          return <BuilderForms item={values} {...props} />;
        }}
      </Form>
      <LoadingIndicator modal loading={savingFormBuilder} />
    </>
  );
};

export default BuilderRootContainer;
