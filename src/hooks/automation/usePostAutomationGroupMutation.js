import useMutation from '@macanta/hooks/apollo/useMutation';
import {POST_AUTOMATION_GROUP} from '@macanta/graphql/automation';

const usePostAutomationGroupMutation = (options) => {
  const mutation = useMutation(POST_AUTOMATION_GROUP, options);

  return mutation;
};

export default usePostAutomationGroupMutation;
