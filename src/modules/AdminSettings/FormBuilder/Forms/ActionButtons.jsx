import React from 'react';
import EditIcon from '@mui/icons-material/Edit';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import Box from '@mui/material/Box';
import {
  PopoverButton,
  IconButton,
  DialogButton,
} from '@macanta/components/Button';

const ActionButtons = ({
  renderActionButtons,
  renderForm,
  onDelete,
  DialogProps,
  editIcon,
  editTitle,
  hideDelete,
}) => {
  return (
    <Box
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      {renderActionButtons && renderActionButtons()}
      {renderForm && (
        <PopoverButton
          style={{
            padding: 0,
          }}
          icon
          sx={{
            padding: '0.25rem',
            color: 'info.main',
          }}
          // onClick={handleAdd}
          size="small"
          TooltipProps={{
            title: editTitle,
          }}
          PopoverProps={{
            title: editTitle,
            contentWidth: 380,
            anchorOrigin: {
              vertical: 'top',
              horizontal: 'center',
            },
            transformOrigin: {
              vertical: 'bottom',
              horizontal: 'center',
            },
          }}
          renderContent={renderForm}>
          {editIcon ? (
            editIcon
          ) : (
            <EditIcon
              style={{
                fontSize: 20,
              }}
            />
          )}
        </PopoverButton>
      )}
      {!hideDelete && (
        <>
          {DialogProps ? (
            <DialogButton
              style={{
                padding: 0,
              }}
              icon
              size="small"
              TooltipProps={{title: 'Delete'}}
              DialogProps={{
                title: 'Are you sure?',
                onActions: (handleClose) => {
                  return [
                    {
                      label: 'Delete',
                      onClick: () => {
                        onDelete();
                        handleClose();
                      },
                      startIcon: <DeleteForeverIcon />,
                    },
                  ];
                },
                ...DialogProps,
              }}>
              <DeleteForeverIcon
                style={{
                  fontSize: 20,
                }}
                sx={{
                  color: 'grayDarker.main',
                }}
              />
            </DialogButton>
          ) : (
            <IconButton
              style={{
                padding: 0,
              }}
              size="small"
              TooltipProps={{title: 'Delete'}}
              onClick={onDelete}>
              <DeleteForeverIcon
                style={{
                  fontSize: 20,
                }}
                sx={{
                  color: 'grayDarker.main',
                }}
              />
            </IconButton>
          )}
        </>
      )}
    </Box>
  );
};

ActionButtons.defaultProps = {
  editTitle: 'Edit',
};

export default ActionButtons;
