import React from 'react';
import {useFormikContext} from 'formik';
import TurnedInIcon from '@mui/icons-material/TurnedIn';
import Typography from '@mui/material/Typography';
import {useTheme} from '@mui/material/styles';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import useValue from '@macanta/hooks/base/useValue';
import ConfirmationDialog from '@macanta/containers/ConfirmationDialog';
import FormField from '@macanta/containers/FormField';
import {
  getContactRelationships,
  getFilteredRelationshipsByLimit,
  getRelationshipOptions,
} from '@macanta/selectors/relationship.selector';
import useContact from '@macanta/hooks/contact/useContact';
import useDORelationships from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDORelationships';
import {update, mergeAndAddArraysBySameKeys} from '@macanta/utils/array';

const DORequiredFieldsDialog = ({onSave, ...props}) => {
  const {errors, setErrors, setValues} = useFormikContext();
  const [requiredFields, setRequiredFields] = useValue(
    errors.requiredFieldsMap,
  );
  const [details, setDetails] = useValue(requiredFields?.details);
  const [relationships, setRelationships] = useValue(
    requiredFields?.relationships,
  );

  const handleClose = () => {
    setRequiredFields(null);
    setErrors({});
  };

  const handleSave = () => {
    setValues((state) => ({
      ...state,
      details: {
        ...state['details'],
        ...details.reduce((acc, detail) => {
          const accObj = {...acc};

          accObj[detail.name] = detail.value;

          return accObj;
        }, {}),
      },
      relationships: update({
        arr: state.relationships,
        item: relationships,
        key: 'connectedContactId',
        includeAll: true,
      }),
    }));

    setTimeout(() => {
      onSave();
    }, 0);
  };

  return (
    <ConfirmationDialog
      PaperProps={{
        style: {
          maxWidth: 500,
          width: '100%',
        },
      }}
      open={!!errors.requiredFieldsMap}
      onClose={handleClose}
      title="Please fill in required fields"
      // description={requiredFields?.text}
      BodyComp={
        <RequiredFieldsForm
          details={details}
          setDetails={setDetails}
          relationships={relationships}
          setRelationships={setRelationships}
        />
      }
      actions={[
        {
          label: 'Save',
          onClick: handleSave,
          startIcon: <TurnedInIcon />,
        },
      ]}
      {...props}
    />
  );
};

const RequiredFieldsForm = ({
  details,
  relationships,
  setDetails,
  setRelationships,
}) => {
  const theme = useTheme();
  const {values} = useFormikContext();

  const relationshipsQuery = useDORelationships(values.groupId, {
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const doRelationships = relationshipsQuery.data?.listRelationships;
  const doRelationshipsRoles = doRelationships.map((r) => r.role);

  const handleDetails = (key) => (value) => {
    setDetails(
      update({
        arr: details,
        item: {name: key, value},
        key: 'name',
        includeAll: true,
      }),
    );
  };

  const handleRelationships = (key) => (value) => {
    setRelationships(
      update({
        arr: relationships,
        item: {connectedContactId: key, relationships: value},
        key: 'connectedContactId',
        includeAll: true,
      }),
    );
  };

  const getRelationshipsByLimit = () => {
    const mergedRelationships = mergeAndAddArraysBySameKeys(
      values.context.connectedContacts.map((c) => ({
        connectedContactId: c.contactId,
        relationships: c.relationships.map(({role}) => role),
      })),
      values.relationships,
      {
        keysToCheck: ['connectedContactId'],
        includeAll: true,
      },
    ).filter(({deleted}) => !deleted);

    const mergedConnectedContacts = mergeAndAddArraysBySameKeys(
      mergedRelationships,
      relationships,
      {
        keysToCheck: ['connectedContactId'],
        includeAll: true,
      },
    ).filter(({deleted}) => !deleted);

    const contactRelationships = getContactRelationships(
      mergedConnectedContacts.map((c) => ({
        ...c,
        relationships: c.relationships.map((role) => ({role})),
      })),
    );
    const filteredRelationshipsByLimit = getFilteredRelationshipsByLimit(
      contactRelationships,
      doRelationships,
    );

    return filteredRelationshipsByLimit;
  };

  return (
    <>
      {!!details?.length && (
        <Typography
          style={{
            marginBottom: '0.5rem',
            color: theme.palette.primary.main,
            fontWeight: 'bold',
          }}>
          Details
        </Typography>
      )}
      {details?.map((field) => {
        const fieldValue = details.find((d) => d.id === field.id)?.value;

        return (
          <FormField
            key={field.id}
            type={field.type}
            required={field.required}
            options={field.choices}
            // error={errors[field.name]}
            onChange={handleDetails(field.name)}
            label={field.name}
            placeholder={field.placeholder}
            headingText={field.headingText}
            helperText={field.helpText}
            addDivider={field.addDivider}
            value={fieldValue}
            // readOnly={isReadOnly}
            // {...(field.type === 'FileUpload' && {
            //   readOnly: true, //TODO: Temporarily set as static true until FileUpload type works
            //   attachments: fieldDetails.attachments,
            // })}
            fullWidth
            size="small"
            variant="outlined"
            // TopRightComp={
            //   field.contactSpecificField && (
            //     <ContactSpecificFields values={fieldDetails.contactSpecificValues} />
            //   )
            // }
          />
        );
      })}
      {!!relationships?.length && (
        <Typography
          style={{
            ...(!!details?.length && {
              marginTop: '1rem',
            }),
            marginBottom: '0.5rem',
            color: theme.palette.primary.main,
            fontWeight: 'bold',
          }}>
          Relationships
        </Typography>
      )}
      {relationships?.map((rel) => {
        const relationshipOptions = getRelationshipOptions(
          doRelationshipsRoles,
          rel.relationships,
          getRelationshipsByLimit(),
        );

        return (
          <React.Fragment key={rel.connectedContactId}>
            <RelationshipField
              contactId={rel.connectedContactId}
              type="MultiSelect"
              options={relationshipOptions}
              // renderValue={handleRenderValue}
              // error={errors[field.name]}
              onChange={handleRelationships(rel.connectedContactId)}
              variant="outlined"
              // placeholder={field.placeholder}
              // value={values[field.name]}
              value={rel.relationships}
              // readOnly={isReadOnly}
              fullWidth
              size="small"
              // TopRightComp={TopRightComp}
              // {...fieldAttrs}
            />

            {/* {isPrimaryContact && connectedContacts.length > 1 && (
              <Styled.SubHeader>Other Related Contacts</Styled.SubHeader>
            )} */}
          </React.Fragment>
        );
      })}
    </>
  );
};

const RelationshipField = ({contactId, ...props}) => {
  const {data: connectedContact = {}} = useContact(contactId, {
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const {firstName, lastName, email} = connectedContact;

  const name = `${firstName} ${lastName}`.trim();

  const label = (
    <>
      {name}

      <Typography variant="caption" style={{display: 'block', color: '#aaa'}}>
        {email}
      </Typography>
    </>
  );

  return <FormField label={label} {...props} />;
};

export default DORequiredFieldsDialog;
