import React from 'react';
import Breadcrumbs from '@mui/material/Breadcrumbs';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import Typography from '@mui/material/Typography';
import Section from '@macanta/containers/Section';
import {ROOT_PATH_ADMIN} from '@macanta/modules/AdminSettings/AdminSettings';
import useAppSettings from '@macanta/hooks/admin/useAppSettings';
import BrandLogoPicker from './BrandLogoPicker';
import BrandColorPicker from './BrandColorPicker';
import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import * as AdminSettingsStyled from '@macanta/modules/AdminSettings/styles';
import {navigate} from 'gatsby';

const LookAndFeel = () => {
  /* const navigate = useNavigate();*/

  const appSettingsQuery = useAppSettings();

  const handleBack = () => {
    navigate(ROOT_PATH_ADMIN);
  };

  return (
    <NoteTaskHistoryStyled.FullHeightGrid container>
      <NoteTaskHistoryStyled.FullHeightFlexGridColumn item xs={12}>
        <Section
          loading={appSettingsQuery.loading}
          fullHeight
          bodyStyle={{
            backgroundColor: '#f5f6fa',
            padding: '1rem 0',
          }}
          HeaderLeftComp={
            <Breadcrumbs aria-label="breadcrumb">
              <AdminSettingsStyled.TitleCrumbBtn
                startIcon={
                  <ArrowBackIcon
                    style={{
                      color: '#333',
                    }}
                  />
                }
                onClick={handleBack}
                disabled={false}>
                <Typography color="textPrimary">Admin Settings</Typography>
              </AdminSettingsStyled.TitleCrumbBtn>
              <Typography>Look & Feel</Typography>
            </Breadcrumbs>
          }>
          <NoteTaskHistoryStyled.FullHeightGrid container>
            <NoteTaskHistoryStyled.FullHeightFlexGridColumn item xs={12} md={4}>
              <BrandLogoPicker />
            </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
            <NoteTaskHistoryStyled.FullHeightFlexGridColumn item xs={12} md={4}>
              <BrandColorPicker />
            </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
          </NoteTaskHistoryStyled.FullHeightGrid>
        </Section>
      </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
    </NoteTaskHistoryStyled.FullHeightGrid>
  );
};

export default LookAndFeel;
