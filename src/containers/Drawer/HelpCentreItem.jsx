import React from 'react';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import ContactSupportIcon from '@mui/icons-material/ContactSupport';
import Tooltip from '@macanta/components/Tooltip';

const HelpCentreItem = () => {
  const handleToggle = async () => {
    window.FreshworksWidget('open');
  };

  return (
    <Tooltip title="Help Centre" placement="right">
      <ListItem id={`help-centre-button`} button onClick={handleToggle}>
        <ListItemIcon>
          <ContactSupportIcon />
        </ListItemIcon>
        <ListItemText primary={'Help Centre'} />
      </ListItem>
    </Tooltip>
  );
};

export default HelpCentreItem;
