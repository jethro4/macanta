import React, {useState} from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import AssignmentIndIcon from '@mui/icons-material/AssignmentInd';
import Popover from '@macanta/components/Popover';
import {ButtonLink} from '@macanta/components/Button';
import {OverflowTip} from '@macanta/components/Tooltip';
import * as Styled from './styles';

const ContactSpecificFields = ({values}) => {
  const [anchorEl, setAnchorEl] = useState(false);
  const openContactSpecificValues = Boolean(anchorEl);

  const handleViewContactSpecificValues = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleCloseContactSpecificValues = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <Styled.ContactSpecificFieldsBtn
        aria-haspopup="true"
        onClick={handleViewContactSpecificValues}>
        <AssignmentIndIcon />
      </Styled.ContactSpecificFieldsBtn>
      <Popover
        title="Contact Specific Values"
        open={openContactSpecificValues}
        anchorEl={anchorEl}
        onClose={handleCloseContactSpecificValues}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}>
        <div
          style={{
            maxWidth: 500,
            minWidth: 250,
            maxHeight: 500,
            overflowY: 'auto',
            padding: '1rem',
          }}>
          {!values.length ? (
            <Typography
              style={{
                margin: '1rem 0',
              }}
              align="center"
              color="#888">
              No values found
            </Typography>
          ) : (
            values.map((contact) => {
              return (
                <Box
                  key={contact.contactId}
                  style={{
                    display: 'flex',
                    marginBottom: 4,
                    // alignItems: 'center',
                    // whiteSpace: 'noWrap',
                  }}>
                  <Box
                    style={{
                      flexGrow: 1,
                      flexBasis: 0,
                      overflowX: 'hidden',
                      minWidth: 125,
                    }}>
                    <ButtonLink
                      to={`/app/contact/${contact.contactId}/notes`}
                      style={{
                        color: '#2196f3',
                        padding: 0,
                        textAlign: 'right',
                      }}>
                      <OverflowTip
                        style={{
                          fontSize: '0.875rem',
                          // textOverflow: 'ellipsis',
                          // overflow: 'hidden',
                        }}>
                        {`${contact.firstName} ${contact.lastName}`.trim()}
                      </OverflowTip>
                    </ButtonLink>
                  </Box>
                  <Typography
                    style={{
                      fontSize: '0.875rem',
                    }}>
                    &nbsp;:&nbsp;&nbsp;&nbsp;
                  </Typography>
                  <Box
                    style={{
                      flexGrow: 1,
                      flexBasis: 0,
                      overflowX: 'hidden',
                    }}>
                    <OverflowTip
                      style={{
                        fontSize: '0.875rem',
                        ...(!contact.value && {
                          color: '#aaa',
                        }),
                      }}>
                      {contact.value || 'N/A'}
                    </OverflowTip>
                  </Box>
                </Box>
              );
            })
          )}
        </div>
      </Popover>
    </>
  );
};

ContactSpecificFields.defaultProps = {
  values: [],
};

export default ContactSpecificFields;
