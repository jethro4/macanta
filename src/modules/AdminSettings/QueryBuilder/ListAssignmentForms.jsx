import React, {useState, useEffect, useLayoutEffect, useMemo} from 'react';
import {useFormikContext} from 'formik';
import TurnedInIcon from '@mui/icons-material/TurnedIn';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import Button from '@macanta/components/Button';
import {SubSection} from '@macanta/containers/Section';
import FormField from '@macanta/containers/FormField';
import Show from '@macanta/containers/Show';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import useDORelationships from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDORelationships';
import Form from '@macanta/components/Form';
import listAssignmentValidationSchema from '@macanta/validations/listAssignment';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import usePrevious from '@macanta/hooks/usePrevious';
import useUsers from '@macanta/hooks/admin/useUsers';
import {useQuery, useMutation} from '@apollo/client';
import {
  GET_LIST_ASSIGNMENTS,
  LIST_ROUND_ROBINS,
  CREATE_OR_UPDATE_LIST_ASSIGNMENT,
} from '@macanta/graphql/admin';
import isEqual from 'lodash/isEqual';
import {sortArrayByObjectKey} from '@macanta/utils/array';
import * as NoteTaskFormsStyled from '@macanta/modules/NoteTaskForms/styles';
import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import * as applicationStyles from '@macanta/themes/applicationStyles';
import * as Styled from './styles';

const ASSIGNMENT_TYPES = [
  {
    label: 'Assign via Existing Round Robin',
    value: 'roundRobin',
  },
  {
    label: 'Assign via Selected Users',
    value: 'selectedUsers',
  },
];

const UNASSIGNMENT_BEHAVIORS = [
  {
    label: 'No Action',
    value: 'noAction',
  },
  {
    label: 'Un-Assign when Data Object Item removed from list',
    value: 'unAssignWhenDORemoved',
  },
];

const ListAssignmentFormsContainer = ({
  id,
  doType: doTypeProp,
  onSuccess,
  ...props
}) => {
  const {displayMessage} = useProgressAlert();

  const listAssignmentQuery = useQuery(GET_LIST_ASSIGNMENTS, {
    variables: {
      queryId: id,
    },
    skip: !id,
  });

  const listAssignment = listAssignmentQuery.data?.getListAssignments;

  const getInitValues = () => {
    return {
      queryId: id,
      assignmentType:
        listAssignment?.assignmentType || ASSIGNMENT_TYPES[0].value,
      unassignmentBehavior:
        listAssignment?.unassignmentBehavior || UNASSIGNMENT_BEHAVIORS[0].value,
      selectedRoundRobin: listAssignment?.selectedRoundRobin || '',
      selectedUsers: listAssignment?.selectedUsers || [],
      assignedRelationship: listAssignment?.assignedRelationship || '',
      status: listAssignment?.status,
    };
  };

  const [initValues, setInitValues] = useState(() => getInitValues());
  const prevInitValues = usePrevious(initValues);

  const doTypes = useDOTypes({
    removeEmail: true,
  });

  const doType = doTypes?.data?.listDataObjectTypes?.find(
    (type) => type.title === doTypeProp,
  );
  const groupId = doType?.id;

  const relationshipsQuery = useDORelationships(groupId);

  const relationships = relationshipsQuery.data?.listRelationships;

  const [
    callCreateOrUpdateListAssignment,
    createOrUpdateListAssignmentMutation,
  ] = useMutation(CREATE_OR_UPDATE_LIST_ASSIGNMENT, {
    onError() {},
    onCompleted: (data) => {
      if (data?.createOrUpdateListAssignment) {
        displayMessage('Saved successfully!');

        onSuccess && onSuccess();
      }
    },
  });

  const handleFormSubmit = async (values) => {
    const isDirty = !isEqual(values, initValues);
    const status = isDirty ? 'active' : initValues.status;

    callCreateOrUpdateListAssignment({
      variables: {
        createOrUpdateListAssignmentInput: {
          ...values,
          status,
        },
        __mutationkey: 'createOrUpdateListAssignmentInput',
      },
    });
  };

  useEffect(() => {
    if (listAssignment && prevInitValues === initValues) {
      setInitValues(getInitValues());
    }
  }, [listAssignment]);

  const initLoading =
    (!doType || !relationships || !listAssignment) &&
    Boolean(
      listAssignmentQuery.loading ||
        relationshipsQuery.loading ||
        doTypes.loading,
    );

  return (
    <>
      <Form
        initialValues={initValues}
        enableReinitialize={true}
        validationSchema={listAssignmentValidationSchema}
        onSubmit={handleFormSubmit}>
        {({values}) => {
          return (
            <ListAssignmentForms
              item={values}
              groupId={groupId}
              hideLoading={initLoading}
              loadingListAssignment={listAssignmentQuery.loading}
              errorSaveAssignment={createOrUpdateListAssignmentMutation.error}
              relationships={relationships}
              {...props}
            />
          );
        }}
      </Form>
      <LoadingIndicator fill backdrop loading={initLoading} />
      <LoadingIndicator
        modal
        loading={createOrUpdateListAssignmentMutation.loading}
      />
    </>
  );
};

const ListAssignmentForms = ({
  item,
  groupId,
  hideLoading,
  loadingListAssignment,
  errorSaveAssignment,
  relationships,
  ...props
}) => {
  const {
    errors,
    handleChange,
    setFieldValue,
    handleSubmit,
    dirty,
  } = useFormikContext();

  const usersQuery = useUsers();
  const roundRobinsQuery = useQuery(LIST_ROUND_ROBINS, {
    variables: {
      groupId,
    },
    skip: !groupId,
  });

  const updatedUsers = usersQuery.data;
  const roundRobins = roundRobinsQuery.data?.listRoundRobins;

  const handleRenderValue = (choices) => (val) => {
    const selected = choices?.find(({value}) => value === val);

    return selected?.label;
  };

  const handleUserRenderValue = (selectedUserIds) => {
    return selectedUserIds.length > 3
      ? `${selectedUserIds.length} selected`
      : selectedUserIds
          .map((selectedUserId) => {
            const selected = updatedUsers?.find(
              (user) => user.id === selectedUserId,
            );

            return selected?.email;
          })
          .join(', ');
  };

  const allRelationships = useMemo(() => {
    if (relationships) {
      return relationships.map((r) => ({
        label: r.role,
        value: r.role,
      }));
    }

    return [];
  }, [relationships]);

  const users = useMemo(() => {
    if (updatedUsers) {
      return sortArrayByObjectKey(
        updatedUsers.map((r) => ({
          label: r.email,
          value: r.id,
        })),
        'label',
      );
    }

    return [];
  }, [updatedUsers]);

  const allRoundRobins = useMemo(() => {
    if (
      !roundRobins?.length &&
      item?.selectedRoundRobin &&
      item?.assignedRelationship
    ) {
      return [
        {
          label: item.selectedRoundRobin,
          value: item.selectedRoundRobin,
          relationship: item.assignedRelationship,
        },
      ];
    }

    if (roundRobins) {
      return roundRobins.map((r) => ({
        label: r.name,
        value: r.name,
        relationship: r.relationship,
      }));
    }

    return [];
  }, [roundRobins]);

  useLayoutEffect(() => {
    if (
      allRoundRobins.length &&
      item?.assignmentType === 'roundRobin' &&
      item?.selectedRoundRobin
    ) {
      const selectedRoundRobin = allRoundRobins.find(
        (r) => r.value === item.selectedRoundRobin,
      );

      setFieldValue('assignedRelationship', selectedRoundRobin.relationship);
    }
  }, [allRoundRobins, item?.assignmentType, item?.selectedRoundRobin]);

  const loading =
    !hideLoading &&
    (loadingListAssignment || roundRobinsQuery.loading || usersQuery.loading);

  return (
    <Styled.FormRoot {...props}>
      <Styled.Body>
        <NoteTaskHistoryStyled.FullHeightGrid
          style={{
            flexDirection: 'row',
          }}
          sx={applicationStyles.fillScroll}
          container>
          <NoteTaskHistoryStyled.FullHeightFlexGridColumn item sm={12} md={6}>
            <Styled.SubBody>
              <SubSection
                headerStyle={{
                  backgroundColor: 'white',
                  minHeight: '3rem',
                }}
                bodyStyle={{
                  padding: '1rem 0',
                }}
                style={{
                  marginTop: 0,
                }}
                title="Assignment Settings">
                <FormField
                  style={{
                    padding: '0 1rem',
                  }}
                  type="Select"
                  options={ASSIGNMENT_TYPES}
                  value={item?.assignmentType}
                  error={errors.assignmentType}
                  onChange={(val) => {
                    setFieldValue('assignmentType', val);

                    if (val === 'roundRobin') {
                      setFieldValue('selectedUsers', []);
                    } else {
                      setFieldValue('selectedRoundRobin', '');
                    }

                    setFieldValue('assignedRelationship', '');
                  }}
                  label="Choose Assignment Type"
                  fullWidth
                  size="small"
                  variant="outlined"
                  renderValue={handleRenderValue(ASSIGNMENT_TYPES)}
                />

                <Show in={item?.assignmentType === 'roundRobin'}>
                  <FormField
                    style={{
                      padding: '0 1rem',
                    }}
                    disabled={!allRoundRobins.length}
                    {...(!hideLoading &&
                      !allRoundRobins.length && {
                        placeholder: 'No round robins found',
                      })}
                    required
                    addDivider
                    type="Select"
                    options={allRoundRobins}
                    value={item?.selectedRoundRobin}
                    error={errors.selectedRoundRobin}
                    onChange={handleChange('selectedRoundRobin')}
                    label="Select Round Robin"
                    fullWidth
                    size="small"
                    variant="outlined"
                    renderValue={handleRenderValue(allRoundRobins)}
                  />
                </Show>
                <Show in={item?.assignmentType === 'selectedUsers'}>
                  <FormField
                    style={{
                      padding: '0 1rem',
                    }}
                    required
                    addDivider
                    type="MultiSelect"
                    options={users}
                    value={item?.selectedUsers}
                    error={errors.selectedUsers}
                    onChange={(val) => setFieldValue('selectedUsers', val)}
                    label="Select Users"
                    fullWidth
                    size="small"
                    variant="outlined"
                    renderValue={handleUserRenderValue}
                  />
                </Show>

                <Show in={item?.assignmentType === 'roundRobin'}>
                  <FormField
                    disabled
                    style={{
                      padding: '0 1rem',
                    }}
                    value={item?.assignedRelationship || 'N/A'}
                    error={errors.assignedRelationship}
                    label="Assigned Relationship"
                    fullWidth
                    size="small"
                    variant="outlined"
                  />
                </Show>
                <Show in={item?.assignmentType === 'selectedUsers'}>
                  <FormField
                    style={{
                      padding: '0 1rem',
                    }}
                    required
                    type="Select"
                    options={allRelationships}
                    value={item?.assignedRelationship}
                    error={errors.assignedRelationship}
                    onChange={handleChange('assignedRelationship')}
                    label="Assigned Relationship"
                    fullWidth
                    size="small"
                    variant="outlined"
                    renderValue={handleRenderValue(allRelationships)}
                  />
                </Show>
              </SubSection>
            </Styled.SubBody>
          </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
          <NoteTaskHistoryStyled.FullHeightFlexGridColumn item xs={12} md={6}>
            <Styled.SubBody>
              <SubSection
                headerStyle={{
                  backgroundColor: 'white',
                  minHeight: '3rem',
                }}
                bodyStyle={{
                  padding: '1rem 0',
                }}
                style={{
                  marginTop: 0,
                }}
                title="Un-Assignment Settings">
                <FormField
                  style={{
                    padding: '0 1rem',
                  }}
                  type="Select"
                  options={UNASSIGNMENT_BEHAVIORS}
                  value={item?.unassignmentBehavior}
                  error={errors.unassignmentBehavior}
                  onChange={handleChange('unassignmentBehavior')}
                  label="Select Un-Assignment Behavior"
                  fullWidth
                  size="small"
                  variant="outlined"
                  renderValue={handleRenderValue(UNASSIGNMENT_BEHAVIORS)}
                />
              </SubSection>
            </Styled.SubBody>
          </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
        </NoteTaskHistoryStyled.FullHeightGrid>
        <LoadingIndicator
          fill
          align="top"
          style={{
            top: '2.5rem',
          }}
          loading={loading}
        />
      </Styled.Body>
      <NoteTaskFormsStyled.Footer
        sx={applicationStyles.footer}
        style={{
          padding: '1rem',
          margin: 0,
          borderTop: '1px solid #eee',
          width: '100%',
          backgroundColor: '#f5f6fa',
        }}>
        <Box
          style={{
            flex: 1,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          {!!errorSaveAssignment && (
            <Typography
              color="error"
              style={{
                fontSize: '0.75rem',
                textAlign: 'center',
                marginRight: '1rem',
              }}>
              {errorSaveAssignment.message}
            </Typography>
          )}
        </Box>
        <Button
          disabled={!dirty && item?.status !== 'inactive'}
          variant="contained"
          startIcon={<TurnedInIcon />}
          onClick={handleSubmit}
          size="medium">
          {dirty || item?.status !== 'inactive' ? 'Save' : 'Re-Run'} Assignment
        </Button>
      </NoteTaskFormsStyled.Footer>
    </Styled.FormRoot>
  );
};

export default ListAssignmentFormsContainer;
