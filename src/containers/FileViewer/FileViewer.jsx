import React, {useEffect, useState} from 'react';
import Modal from '@macanta/components/Modal';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import Box from '@mui/material/Box';
import PDFViewer from './PDFViewer';

const FileViewer = ({
  ModalProps,
  headerTitle,
  isPDF,
  src,
  download,
  onClose,
}) => {
  const [hideLoading, setHideLoading] = useState(false);
  const hasSrc = !!src;

  const hideLoadingIndicator = () => {
    setHideLoading(true);
  };

  const handleClose = () => {
    setHideLoading(false);
    onClose();
  };

  useEffect(() => {
    if (download) {
      setTimeout(() => {
        window.location.href = src;
        handleClose();
      }, 400);
    }
  });

  return (
    <Modal
      open={hasSrc}
      headerTitle={headerTitle}
      onClose={handleClose}
      innerStyle={{
        position: 'relative',
      }}
      {...ModalProps}>
      {!hideLoading && (
        <Box
          style={{
            position: 'absolute',
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <LoadingIndicator />
        </Box>
      )}
      {!download && (
        <>
          {isPDF ? (
            <PDFViewer pdf={src} onLoadSuccess={hideLoadingIndicator} />
          ) : (
            <iframe
              title={headerTitle}
              style={{
                display: 'block',
                width: '100%',
                height: '100%',
              }}
              src={src}
              onLoad={hideLoadingIndicator}
            />
          )}
        </>
      )}
    </Modal>
  );
};

FileViewer.defaultProps = {
  headerTitle: '',
};

export default FileViewer;
