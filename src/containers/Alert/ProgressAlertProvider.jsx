import React, {useState} from 'react';
import ProgressAlertContext from './ProgressAlertContext';
import Alert from './Alert';
import {generateUniqueID} from '@macanta/utils/string';
import {filterItems, insert, remove} from '@macanta/utils/array';
import useDimensions from '@macanta/hooks/base/useDimensions';

const DEFAULT_MARGIN = 12;

const ProgressAlert = ({children}) => {
  const [options, setOptions] = useState([]);

  const addOptions = (option) => {
    setOptions((state) => {
      const filteredState = filterItems(
        state,
        (item) => item.message !== option.message,
      );

      return insert({
        arr: filteredState,
        item: option,
        index: 0,
      });
    });
  };

  const removeOption = (id) => {
    setOptions((state) =>
      remove({
        arr: state,
        key: 'id',
        value: id,
      }),
    );
  };

  const displayMessage = (message, timeout) => {
    addOptions({
      id: generateUniqueID(),
      message: message?.message || message,
      severity: message?.severity || 'success',
      timeout: message?.timeout || timeout,
    });
  };

  const handleClose = (id) => {
    removeOption(id);
  };

  return (
    <>
      <ProgressAlertContext.Provider
        value={{
          displayMessage,
        }}>
        {children}
      </ProgressAlertContext.Provider>

      {options.map((option, index) => {
        return (
          <AlertItem
            key={option.id}
            item={option}
            index={index}
            onClose={handleClose}
          />
        );
      })}
    </>
  );
};

const AlertItem = ({item, index, onClose}) => {
  const {dimensions, setRef} = useDimensions();

  const order = index + 1;
  const alertHeight = dimensions.height;
  const marginOffset = alertHeight * order + order * DEFAULT_MARGIN;
  const bottomOffset = marginOffset - DEFAULT_MARGIN - alertHeight / 2;

  const handleClose = (id) => () => {
    onClose(id);
  };

  return (
    <Alert
      key={item.id}
      setRef={setRef}
      open
      message={item.message}
      onClose={handleClose(item.id)}
      severity={item.severity}
      snackbarProps={Object.assign(
        {
          style: {
            bottom: bottomOffset,
          },
        },
        item.timeout && {
          autoHideDuration: item.timeout,
        },
      )}
    />
  );
};

export default ProgressAlert;
