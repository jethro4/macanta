import {POLICIES_WITH_NETWORK_CALL} from '@macanta/constants/fetchPolicies';
import useQuery from './useQuery';
import useQueue from './useQueue';

const useQueuedQuery = (graphqlTag, optionsArg) => {
  const {limit, ...options} = Object.assign({limit: 5}, optionsArg);

  const tagFieldName = graphqlTag.definitions?.[0]?.name.value;
  const callsNetwork =
    !options?.skip &&
    (!options?.fetchPolicy ||
      POLICIES_WITH_NETWORK_CALL.includes(options.fetchPolicy));

  const {pending, completed, handleCompleteQuery} = useQueue({
    tagFieldName,
    variables: options?.variables,
    callsNetwork,
    limit,
  });

  const query = useQuery(graphqlTag, {
    ...options,
    skip: pending || options?.skip,
    onCompleted(response) {
      handleCompleteQuery();

      options?.onCompleted?.(response);
    },
  });

  return {...query, pending, completed};
};

export default useQueuedQuery;
