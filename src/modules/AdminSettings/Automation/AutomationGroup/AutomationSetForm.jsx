import React from 'react';
import AddIcon from '@material-ui/icons/Add';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import {GridLayout} from '@macanta/components/Layout';
import Section from '@macanta/containers/Section';
import Button from '@macanta/components/Button';
import {generateUniqueID} from '@macanta/utils/string';
import {insert, remove, update} from '@macanta/utils/array';
import AutomationSetFields from '@macanta/modules/AdminSettings/Automation/AutomationGroup/AutomationSetFields';
import WaitActionFields from '@macanta/modules/AdminSettings/Automation/AutomationGroup/WaitActionFields';
import ButtonField from '@macanta/containers/buttons/ButtonField';
import {getWaitDescription} from '@macanta/modules/AdminSettings/Automation/AutomationGroup/helpers';
import * as TableStyled from '@macanta/components/Table/styles';
import FlexGrid from '@macanta/components/Base/FlexGrid';
import Divider from '@macanta/components/Base/Divider';

const getNewItem = () => ({
  id: generateUniqueID(),
  active: true,
  actionId: '',
  conditionId: '',
  revalidate: false,
  waitFor: null,
  unit: '',
  time: '',
});

const AutomationSetForm = ({data, onChange, ...props}) => {
  const handleAdd = () => {
    const callback = (items) =>
      insert({
        arr: items,
        item: getNewItem(),
        index: 0,
      });

    onChange?.(callback);
  };

  const handleEdit = (updatedItem) => {
    const callback = (items) =>
      update({
        arr: items,
        item: updatedItem,
        key: 'id',
      });

    onChange?.(callback);
  };

  const handleDelete = (id) => {
    const callback = (items) =>
      remove({
        arr: items,
        key: 'id',
        value: id,
      });

    onChange?.(callback);
  };

  const handleClear = (item) => () => {
    handleEdit({
      ...item,
      revalidate: false,
      time: '',
      unit: '',
      waitFor: null,
    });
  };

  const handleSort = (...args) => {
    console.debug('handleSort', ...args);
  };

  return (
    <Section
      title="Automation Set"
      bodyStyle={{
        minHeight: 250,
        maxHeight: 600,
        overflowX: 'hidden',
        overflowY: 'auto',
      }}
      HeaderRightComp={
        <Button
          size="small"
          variant="contained"
          startIcon={<AddIcon />}
          onClick={handleAdd}>
          Add Set
        </Button>
      }
      {...props}>
      {!!data.length && (
        <FlexGrid
          margin={[0, '1.5rem']}
          sx={{
            marginTop: '8px',
            padding: '4px 1rem',
          }}>
          <SetHeader colWidth="66px" heading="Status" subHeading="On/Off" />

          <SetHeader heading="If" subHeading="Choose your Trigger Condition." />

          <SetHeader
            colWidth={1.2}
            heading="Wait"
            subHeading="Choose your Wait Action."
          />

          <SetHeader heading="Then" subHeading="Choose your Trigger Action." />

          <Box colWidth="32px" />
        </FlexGrid>
      )}

      <Divider
        sx={{
          margin: '12px 1rem 4px',
        }}
      />

      <GridLayout
        responsive
        autoResizeItem
        // autoHeight
        useCSSTransforms={false}
        isDraggable={false}
        transparent
        maxColumn={1}
        margin={[0, 0]}
        containerPadding={[0, 0]}
        data={data}
        rowHeight={50}
        onSort={handleSort}
        renderGridItem={({item}) => {
          const waitDescription = getWaitDescription(item);

          return (
            <Box
              style={{
                backgroundColor: 'white',
                padding: '12px 1rem',
              }}>
              <AutomationSetFields
                item={item}
                onChange={handleEdit}
                onDelete={handleDelete}
                WaitActionFieldsComp={
                  <ButtonField
                    id={item.id}
                    value={waitDescription}
                    onClear={handleClear(item)}
                    PopoverProps={{
                      contentWidth: 500,
                      bodyStyle: {
                        padding: '1rem',
                      },
                    }}
                    MenuProps={{
                      anchorOrigin: {
                        vertical: 'bottom',
                        horizontal: 'center',
                      },
                      transformOrigin: {
                        vertical: -8,
                        horizontal: 'center',
                      },
                    }}>
                    <WaitActionFields item={item} onChange={handleEdit} />
                  </ButtonField>
                }
              />
            </Box>
          );
        }}
      />

      {data.length === 0 && (
        <TableStyled.EmptyMessageContainer>
          <Typography
            style={{
              paddingBottom: '4rem',
            }}
            align="center"
            color="#888">
            No items found
          </Typography>
        </TableStyled.EmptyMessageContainer>
      )}
    </Section>
  );
};

const SetHeader = ({heading, subHeading}) => {
  return (
    <Box>
      <Typography
        sx={{
          fontSize: '15px',
          fontWeight: 'bold',
          letterSpacing: '0.5px',
          color: 'secondary.main',
        }}>
        {heading}
      </Typography>

      {subHeading && (
        <Typography
          sx={{
            fontSize: '12px',
            letterSpacing: '0.5px',
            color: 'text.grayLight',
          }}>
          {subHeading}
        </Typography>
      )}
    </Box>
  );
};

export default AutomationSetForm;
