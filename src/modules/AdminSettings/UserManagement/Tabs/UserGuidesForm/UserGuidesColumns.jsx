import React from 'react';
import VisibilityIcon from '@mui/icons-material/Visibility';
import {ModalButton} from '@macanta/components/Button';
import {OverflowTip} from '@macanta/components/Tooltip';
import MarkdownForm from './MarkdownForm';

const UserGuidesColumns = [
  {
    id: 'subType',
    label: 'Type',
    minWidth: 180,
  },
  {
    id: 'value',
    label: 'Guide',
    maxWidth: '100%',
    renderBodyCellValue: ({value}) => {
      return (
        <OverflowTip
          sx={{
            color: 'grayDarkest.main',
          }}>
          {value}
        </OverflowTip>
      );
    },
  },
  {
    id: 'actions',
    label: '',
    disableSort: true,
    disableResize: true,
    minWidth: 130,
    maxWidth: 130,
    renderBodyCellValue: ({item, handlers}) => {
      return (
        <ModalButton
          size="small"
          variant="contained"
          startIcon={<VisibilityIcon />}
          ModalProps={{
            headerTitle: `User Guide (${item.subType})`,
            contentWidth: 1050,
            contentMinHeight: 450,
            contentMaxHeight: 600,
          }}
          renderContent={(handleClose) => (
            <MarkdownForm
              value={item.value}
              onChange={(val) => {
                handlers.onChange({item, value: val});

                handleClose();
              }}
            />
          )}>
          Preview
        </ModalButton>
      );
    },
  },
];

export default UserGuidesColumns;
