import React, {useState, useCallback} from 'react';
import PropTypes from 'prop-types';
import {Editor} from '@tinymce/tinymce-react';
import debounce from 'lodash/debounce';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import useValue from '@macanta/hooks/base/useValue';
import * as Styled from './styles';

const DEFAULT_MENU_BAR = 'file edit view insert format tools table ';

const RichTextField = ({
  customMenu,
  timeout,
  placeholder,
  onChange,
  value: valueProp,
  loading,
  contentStyleBodyCSS,
  ...props
}) => {
  const [editorLoading, setEditorLoading] = useState(true);
  const [value, setValue] = useValue(valueProp);

  const handleDebouncedChange = useCallback(
    debounce(
      (html) => {
        handleChange(html);
      },
      timeout,
      {leading: false, trailing: true},
    ),
    [],
  );

  const handleChange = (html) => {
    onChange(html);
  };

  const handleEditorChange = (html) => {
    setValue(html);

    if (onChange) {
      if (timeout) {
        handleDebouncedChange(html);
      } else {
        handleChange(html);
      }
    }
  };

  const handleInit = () => {
    setEditorLoading(false);
  };

  const menu = customMenu.reduce((acc, cm) => {
    return {
      ...acc,
      [cm.id]: {
        title: cm.title,
        items: cm.menuItems.map((menuItem) => menuItem.id),
      },
    };
  }, {});
  const menubar = DEFAULT_MENU_BAR.concat(
    customMenu.map((cm) => cm.id).join(' '),
  );
  const setup = (editor) => {
    customMenu.forEach((cm) => {
      if (cm.menuItems) {
        cm.menuItems.forEach((menuItem) => {
          if (menuItem.items) {
            editor.ui.registry.addNestedMenuItem(menuItem.id, {
              text: menuItem.title,
              getSubmenuItems: () => {
                return menuItem.items.map((subMenu) => {
                  return {
                    type: 'menuitem',
                    text: subMenu.title,
                    onAction: () => {
                      if (!subMenu.onAction) {
                        editor.insertContent(subMenu.content);
                      } else {
                        subMenu.onAction(editor);
                      }
                    },
                  };
                });
              },
            });
          } else {
            editor.ui.registry.addMenuItem(menuItem.id, {
              text: menuItem.title,
              onAction: () => {
                if (!menuItem.onAction) {
                  editor.insertContent(menuItem.content);
                } else {
                  menuItem.onAction(editor);
                }
              },
            });
          }
        });
      }
    });
  };

  return (
    <Styled.RichTextEditorContainer {...props}>
      <Editor
        value={value || ''}
        onEditorChange={handleEditorChange}
        init={{
          height: '100%',
          menu,
          menubar,
          setup,
          placeholder,
          plugins: [
            'advlist autolink lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table paste code wordcount',
          ],
          toolbar:
            'undo redo | formatselect | ' +
            'bold italic backcolor | alignleft aligncenter ' +
            'alignright alignjustify | bullist numlist outdent indent | ' +
            'removeformat | help',
          content_style: `
              body {
                font-family: Arial,Helvetica,sans-serif;
                font-size: 14px;
                ${contentStyleBodyCSS}
              }
              p {
                margin: 0;
              }
            `,
          extended_valid_elements: 'script[*],button[*],svg[*],circle[*]',
          valid_children: '+body[style]',
        }}
        onInit={handleInit}
      />
      {(editorLoading || loading) && <LoadingIndicator fill />}
    </Styled.RichTextEditorContainer>
  );
};

RichTextField.propTypes = {
  customMenu: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      title: PropTypes.string,
      menuItems: PropTypes.arrayOf(
        PropTypes.shape({
          id: PropTypes.string,
          title: PropTypes.string,
          items: PropTypes.arrayOf(
            PropTypes.shape({
              title: PropTypes.string,
              content: PropTypes.string,
            }),
          ),
          disabled: PropTypes.bool,
          onAction: PropTypes.func,
        }),
      ),
    }),
  ),
  timeout: PropTypes.number,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.string,
  loading: PropTypes.bool,
  contentStyleBodyCSS: PropTypes.string,
};

RichTextField.defaultProps = {
  customMenu: [],
  timeout: 0,
  loading: false,
};

export default RichTextField;
