import isEqual from 'lodash/isEqual';
import usePrevious from '@macanta/hooks/usePrevious';
import useNextEffect from './useNextEffect';

const useDeepCompareNextEffect = (callback, dependencies) => {
  const prevDependencies = usePrevious(dependencies);

  useNextEffect(() => {
    if (isEqual(dependencies, prevDependencies)) {
      return;
    }

    callback();
  }, dependencies);
};

export default useDeepCompareNextEffect;
