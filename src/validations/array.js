export const exists = (arr = [], value = '') => {
  return arr.some(
    (str) => str.toLowerCase().trim() === value.toLowerCase().trim(),
  );
};
