import {gql} from '@apollo/client';

export default gql`
  mutation createOrUpdateDataObjectType(
    $input: CreateOrUpdateDataObjectTypeInput!
  ) {
    createOrUpdateDataObjectType(input: $input) {
      id
      title
    }
  }
`;
