import React, {useState, useEffect} from 'react';
import {useFormikContext} from 'formik';
import InputAdornment from '@mui/material/InputAdornment';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import useNextEffect from '@macanta/hooks/base/useNextEffect';
import * as Styled from './styles';

const AsyncValidationField = ({onChange, name, ...props}) => {
  const [loading, setLoading] = useState(false);
  const [validated, setValidated] = useState(false);

  const {
    initialValues,
    values,
    errors,
    setErrors,
    validateField,
    isValidating,
  } = useFormikContext();

  const isSame = initialValues[name] && initialValues[name] === values[name];
  const isValid = validated && !errors[name] && !loading;

  const handleChange = async (val) => {
    onChange(val);

    setLoading(true);
  };

  const handleValidate = async () => {
    await validateField(name);

    if (!validated) {
      setValidated(true);
    }

    setLoading(false);
  };

  useNextEffect(() => {
    handleValidate();
  }, [props.value]);

  useEffect(() => {
    if (isSame) {
      delete errors[name];
      delete errors.context;

      setErrors({...errors});
    }
  }, [errors[name], isSame]);

  return (
    <Styled.TextField
      timeout={400}
      onChange={handleChange}
      InputProps={{
        ...(!props.disabled && {
          endAdornment: (
            <InputAdornment position="end">
              {!isSame && loading && <LoadingIndicator transparent size={20} />}
              {(isSame || isValid) && !!values[name] && (
                <CheckCircleIcon
                  style={{
                    color: '#4caf50',
                  }}
                  size={20}
                />
              )}
            </InputAdornment>
          ),
        }),
      }}
      {...props}
      {...(isValidating && {
        error: null,
      })}
    />
  );
};

export default AsyncValidationField;
