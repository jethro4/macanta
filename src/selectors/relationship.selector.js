export const getContactRelationships = (contacts) => {
  const allContactRelationships = [];

  contacts.forEach((contact) => {
    const contactRelationships = contact.relationships.map((r) => r.role);

    contactRelationships.forEach((r) => {
      if (r) {
        allContactRelationships.push(r);
      }
    });
  });

  return allContactRelationships;
};

export const getFilteredRelationshipsByLimit = (
  contactRelationships,
  doRelationships,
) => {
  const filteredRelationships = doRelationships.filter((r) => {
    if (!r.limit) {
      return true;
    } else if (r.limit) {
      const roleLength = contactRelationships.filter((rel) => rel === r.role)
        .length;

      if (roleLength < r.limit) {
        return true;
      }
    }

    return !contactRelationships.includes(r.role);
  });

  return filteredRelationships.map((r) => r.role);
};

export const getRelationshipOptions = (
  doRelationshipsRoles,
  selectedValues,
  filteredRelationships,
) => {
  return doRelationshipsRoles
    .filter(
      (role) =>
        selectedValues.includes(role) || filteredRelationships.includes(role),
    )
    .map((role) => ({label: role, value: role}));
};

export const getOptimizedRelationshipOptions = (
  doRelationships,
  selectedValues,
  filteredRelationships,
) => {
  return doRelationships.filter(
    ({role}) =>
      selectedValues.some((r) => r.role === role) ||
      filteredRelationships.includes(role),
  );
};
