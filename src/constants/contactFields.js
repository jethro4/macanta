export const CONTACT_DEFAULT_FIELDS = Object.freeze([
  {
    label: 'First Name',
    name: 'FirstName',
    key: 'firstName',
    id: 'field_firstname',
  },
  {
    label: 'Middle Name',
    name: 'MiddleName',
    key: 'middleName',
    id: 'field_middlename',
  },
  {
    label: 'Last Name',
    name: 'LastName',
    key: 'lastName',
    id: 'field_lastname',
  },
  {
    label: 'Email',
    name: 'Email',
    key: 'email',
    id: 'field_email',
  },
  {
    label: 'Phone No.',
    name: 'Phone1',
    key: 'phone1',
    id: 'field_phone1',
  },
  {
    label: 'Billing Address',
    name: 'StreetAddress1',
    key: 'streetAddress1',
    id: 'field_streetaddress1',
  },
  {
    label: 'Billing Address 2',
    name: 'StreetAddress2',
    key: 'streetAddress2',
    id: 'field_streetaddress2',
  },
  {
    label: 'City',
    name: 'City',
    key: 'city',
    id: 'field_city',
  },
  {
    label: 'State',
    name: 'State',
    key: 'state',
    id: 'field_state',
  },
  {
    label: 'Postal Code',
    name: 'PostalCode',
    key: 'postalCode',
    id: 'field_postalcode',
  },
  {
    label: 'Country',
    name: 'Country',
    key: 'country',
    id: 'field_country',
  },
  {
    label: 'Birthday',
    name: 'Birthday',
    key: 'birthday',
    type: 'Date',
    id: 'field_birthday',
  },
  {
    label: 'Title',
    name: 'Title',
    key: 'title',
    id: 'field_title',
  },
  {
    label: 'Job Title',
    name: 'JobTitle',
    key: 'jobTitle',
    id: 'field_jobtitle',
  },
  {
    label: 'Company',
    name: 'Company',
    key: 'company',
    id: 'field_company',
  },
  {
    label: 'Website',
    name: 'Website',
    key: 'website',
    type: 'URL',
    id: 'field_website',
  },
]);

export const CONTACT_DEFAULT_ADDITIONAL_FIELDS = Object.freeze([
  {
    label: 'Email 2',
    name: 'EmailAddress2',
    key: 'email2',
    id: 'field_emailaddress2',
  },
  {
    label: 'Email 3',
    name: 'EmailAddress3',
    key: 'email3',
    id: 'field_emailaddress3',
  },
  {
    label: 'Phone 2',
    name: 'Phone2',
    key: 'phone2',
    id: 'field_phone2',
  },
  {
    label: 'Phone 3',
    name: 'Phone3',
    key: 'phone3',
    id: 'field_phone3',
  },
  {
    label: 'Phone 1 Ext',
    name: 'Phone1Ext',
    key: 'phone1Ext',
    id: 'field_phone1ext',
  },
  {
    label: 'Phone 2 Ext',
    name: 'Phone2Ext',
    key: 'phone2Ext',
    id: 'field_phone2ext',
  },
  {
    label: 'Phone 3 Ext',
    name: 'Phone3Ext',
    key: 'phone3Ext',
    id: 'field_phone3ext',
  },
  {
    label: 'Shipping Address',
    name: 'Address2Street1',
    key: 'address2Street1',
    id: 'field_address2street1',
  },
  {
    label: 'Shipping Address 2',
    name: 'Address2Street2',
    key: 'address2Street2',
    id: 'field_address2street2',
  },
  {
    label: 'City 2',
    name: 'City2',
    key: 'city2',
    id: 'field_city2',
  },
  {
    label: 'State 2',
    name: 'State2',
    key: 'state2',
    id: 'field_state2',
  },
  {
    label: 'Postal Code 2',
    name: 'PostalCode2',
    key: 'postalCode2',
    id: 'field_postalcode2',
  },
  {
    label: 'Country 2',
    name: 'Country2',
    key: 'country2',
    id: 'field_country2',
  },
]);

export const CONTACT_FIELDS = Object.freeze([
  ...CONTACT_DEFAULT_FIELDS,
  ...CONTACT_DEFAULT_ADDITIONAL_FIELDS,
]);

export const FIELD_APPLICABLE_TO = Object.freeze([
  {
    label: 'All',
    value: 'All',
  },
  {
    label: 'User',
    value: 'User',
  },
  {
    label: 'Contact',
    value: 'Contact',
  },
  {
    label: 'Both User & Contact',
    value: 'Both',
  },
  {
    label: 'Admin',
    value: 'Admin',
  },
]);
