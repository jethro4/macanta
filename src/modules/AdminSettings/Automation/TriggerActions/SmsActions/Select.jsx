import React from 'react';
import SelectAction from '@macanta/modules/AdminSettings/Automation/TriggerActions/SubForms/SelectAction';
import {useSmsActions} from '@macanta/hooks/automation';

const Select = ({...props}) => {
  const smsActionsQuery = useSmsActions({
    variables: {
      id: '',
    },
  });

  return (
    <SelectAction
      loading={smsActionsQuery.initLoading}
      type="Select"
      options={smsActionsQuery.result}
      labelKey="name"
      valueKey="id"
      {...props}
    />
  );
};

export default Select;
