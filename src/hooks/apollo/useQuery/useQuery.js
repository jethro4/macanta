import * as apollo from '@apollo/client';
import {getTagFieldName} from '@macanta/utils/graphql';
import isObject from 'lodash/isObject';
import isEmpty from 'lodash/isEmpty';
import useValueShallow from '@macanta/hooks/base/useValueShallow';
import {replaceKeys} from '@macanta/utils/object';

const useQuery = (graphqlTag, optionsArg) => {
  const {
    onCompleted,
    onError,
    keyMapping,
    transformResult,
    dependencies,
    ...options
  } = Object.assign({dependencies: []}, optionsArg);

  const tagFieldName = getTagFieldName(graphqlTag);

  const getResult = ({data}) => {
    let res = data?.[tagFieldName];

    if (res) {
      if (keyMapping) {
        res = replaceKeys(res, keyMapping);
      }

      if (transformResult) {
        res = transformResult(res);
      }
    }

    return res;
  };

  const query = apollo.useQuery(graphqlTag, {
    notifyOnNetworkStatusChange: true,
    ...options,
    onCompleted(data) {
      const result = getResult({data});

      onCompleted?.({...data, result});
    },
    onError(err) {
      console.error('Error on useQuery', err);

      onError?.(err);
    },
  });

  const [result] = useValueShallow(() => getResult({data: query.data}), [
    query.data,
    ...dependencies,
  ]);
  const initLoaded = isObject(result) ? !isEmpty(result) : !!result;
  const initLoading = !initLoaded && query.loading;

  return {...query, result, initLoading, initLoaded};
};

export default useQuery;
