import useNylasInfo from '@macanta/hooks/admin/useNylasInfo';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';

const LoadNylasInfo = () => {
  useNylasInfo({
    fetchPolicy: FETCH_POLICIES.CACHE_AND_NETWORK,
  });

  return null;
};

export default LoadNylasInfo;
