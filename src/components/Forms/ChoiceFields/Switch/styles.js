import {experimentalStyled as styled} from '@mui/material/styles';
import Box from '@mui/material/Box';
import MUIFormControlLabel from '@mui/material/FormControlLabel';
import MUISwitch from '@mui/material/Switch';

export const Root = styled(Box)`
  display: flex;
  justify-content: flex-start;
  align-items: center;

  .MuiSwitch-thumb {
    color: ${({theme}) => theme.palette.common.white};
  }

  .Mui-checked .MuiSwitch-thumb {
    color: ${({theme}) => theme.palette.common.white};
  }

  .MuiSwitch-track {
    background-color: ${({theme, color}) =>
      theme.palette[color]?.main || color} !important;
  }

  ${({size}) =>
    (!size || size === 'medium') &&
    `
      .MuiSwitch-root:not(.MuiButtonBase-root) {
        align-items: center;
        padding: 0;
        width: 42px;
      }

      .MuiSwitch-root.MuiButtonBase-root {
        top: unset;
        padding: 9px;
        left: -6px;

        &.Mui-checked {
          transform: translateX(20px);
        }
      }
    
      .MuiSwitch-thumb {
        width: 16px;
        height: 16px;
        border-radius: 16px;
      }

      .MuiSwitch-track {
        border-radius: 22px;
        height: 22px;
      }
  `}

  ${({size}) =>
    size === 'small' &&
    `
      .MuiSwitch-root:not(.MuiButtonBase-root) {
        align-items: center;
        padding: 0;
        width: 36px;
      }

      .MuiSwitch-root.MuiButtonBase-root {
        top: unset;
        padding: 0;
        left: 2px;

        &.Mui-checked {
          transform: translateX(18px);
        }
      }
    
      .MuiSwitch-thumb {
        width: 14px;
        height: 14px;
        border-radius: 14px;
      }

      .MuiSwitch-track {
        border-radius: 20px;
        height: 20px;
      }
  `}
`;

export const FormControlLabel = styled(MUIFormControlLabel)`
  margin: 0;
  padding: 0 !important;
`;

export const Switch = styled(MUISwitch)`
  .Mui-disabled + .MuiSwitch-track {
    cursor: not-allowed;
    opacity: 0.3;
  }
`;
