import React from 'react';
import {graphql} from 'msw';
import {screen, within} from '@testing-library/react';
import UserPermissionsTemplateForm, {
  PERMISSION_TEMPLATE_FORM_TABS,
} from '@macanta/modules/AdminSettings/UserManagement/UserPermissionsTemplateForm';
import {formatVariables} from '@macanta/modules/AdminSettings/UserManagement/UserPermissionsTemplateForm/helpers';
import {
  renderApp,
  testRenderer,
  overrideHandlers,
} from '@tests/jestSettings/utils/renderUtils';
import {
  click,
  changeInput,
  toggleSelect,
} from '@tests/jestSettings/utils/eventUtils';
import {
  selectInput,
  selectSelect,
  selectSwitch,
  selectRadio,
  selectButtonGroup,
  selectRows,
  selectColumn,
  selectButton,
  selectListItem,
  waitUntil,
  waitApis,
  wait,
} from '@tests/jestSettings/utils/selectorUtils';
import doTypeFactory from '@tests/jestSettings/mockFactory/doTypeFactory';
import doFieldFactory from '@tests/jestSettings/mockFactory/doFieldFactory';
import permissionTemplateFactory from '@tests/jestSettings/mockFactory/permissionTemplateFactory';
import queryFactory from '@tests/jestSettings/mockFactory/queryFactory';
import * as Storage from '@macanta/utils/storage';
import {
  handleMockDOFields,
  handleMockRelationships,
} from '@tests/integrationTests/mockUtils';
import {
  isInDocument,
  isNotInDocument,
} from '@tests/jestSettings/utils/assertUtils';
import customTabFactory from '@tests/jestSettings/mockFactory/customTabFactory';
import appSettingsFactory from '@tests/jestSettings/mockFactory/appSettingsFactory';
import {syncPromises} from '@macanta/utils/promise';

describe('Permission Templates', () => {
  let allDOTypes, allPermissionTemplates, item;

  beforeAll(() => {
    Storage.setItem('userDetails', {
      userId: 'Contact1',
      email: 'test1@macantacrm.com',
    });
  });

  beforeEach(() => {
    allDOTypes = [
      doTypeFactory(),
      doTypeFactory(),
      doTypeFactory(),
      doTypeFactory(),
    ];

    allPermissionTemplates = [
      permissionTemplateFactory(),
      permissionTemplateFactory(),
    ];

    item = {
      id: 'PermissionTemplate1',
    };

    overrideHandlers([
      graphql.query('listDataObjectTypes', (req, res, ctx) => {
        return res(
          ctx.data({
            listDataObjectTypes: allDOTypes,
          }),
        );
      }),
      graphql.query('listPermissionTemplates', (req, res, ctx) => {
        return res(
          ctx.data({
            listPermissionTemplates: allPermissionTemplates,
          }),
        );
      }),
    ]);
  });

  describe('Forms', () => {
    it('TemplateInfo - should pre-populate forms on edit', async () => {
      renderApp(<UserPermissionsTemplateForm item={item} />);

      await isInDocument('input', {
        order: 1,
      });
      await isInDocument('input', {
        order: 2,
      });
    });

    it('SidebarTabAccessForm - should pre-populate forms on edit with respected tab order', async () => {
      const renderPage = testRenderer(
        <UserPermissionsTemplateForm item={item} />,
      );

      renderPage([
        graphql.query('getAppSettings', (req, res, ctx) => {
          return res(
            ctx.data({
              getAppSettings: appSettingsFactory({
                macantaTabOrder: [
                  'Communication History',
                  'Notes',
                  'DO Type 3',
                  'DO Type 2',
                  'DO Type 1',
                  'DO Type 4',
                ],
              }),
            }),
          );
        }),
        graphql.query('listCustomTabs', (req, res, ctx) => {
          return res(
            ctx.data({
              listCustomTabs: [customTabFactory()],
            }),
          );
        }),
      ]);

      await waitApis();

      const menuTab = selectListItem(2, {
        testId: 'UserPermissionsTemplateForm',
      });
      click(menuTab);

      await waitApis();

      await syncPromises([
        async () => {
          const switchAccessColumnElm = selectColumn([1, 1]);
          const radioAccessColumnElm = selectColumn([1, 3]);

          await waitUntil(() =>
            expect(
              selectSwitch({
                parent: switchAccessColumnElm,
              }),
            ).toBeChecked(),
          );
          await waitUntil(() =>
            expect(
              selectRadio({
                value: 'READ_ONLY',
                parent: radioAccessColumnElm,
              }),
            ).toBeChecked(),
          );
        },
        async () => {
          const switchAccessColumnElm = selectColumn([2, 1]);
          const radioAccessColumnElm = selectColumn([2, 3]);

          await waitUntil(() =>
            expect(
              selectSwitch({
                parent: switchAccessColumnElm,
              }),
            ).toBeChecked(),
          );
          await waitUntil(() =>
            expect(
              selectRadio({
                value: 'READ_WRITE',
                parent: radioAccessColumnElm,
              }),
            ).toBeChecked(),
          );
        },
        async () => {
          const switchAccessColumnElm = selectColumn([3, 1]);
          const radioAccessColumnElm = selectColumn([3, 3]);

          await waitUntil(() =>
            expect(
              selectSwitch({
                parent: switchAccessColumnElm,
              }),
            ).not.toBeChecked(),
          );
          await waitUntil(() =>
            expect(
              within(radioAccessColumnElm).queryByDisplayValue('READ_ONLY'),
            ).not.toBeInTheDocument(),
          );
        },
        async () => {
          const switchAccessColumnElm = selectColumn([4, 1]);
          const radioAccessColumnElm = selectColumn([4, 3]);

          await waitUntil(() =>
            expect(
              selectSwitch({
                parent: switchAccessColumnElm,
              }),
            ).toBeChecked(),
          );
          await waitUntil(() =>
            expect(
              selectRadio({
                value: 'READ_WRITE',
                parent: radioAccessColumnElm,
              }),
            ).toBeChecked(),
          );
        },
        async () => {
          const switchAccessColumnElm = selectColumn([5, 1]);
          const radioAccessColumnElm = selectColumn([5, 3]);

          await waitUntil(() =>
            expect(
              selectSwitch({
                parent: switchAccessColumnElm,
              }),
            ).toBeChecked(),
          );
          await waitUntil(() =>
            expect(
              selectRadio({
                value: 'READ_ONLY',
                parent: radioAccessColumnElm,
              }),
            ).toBeChecked(),
          );
        },
        async () => {
          const switchAccessColumnElm = selectColumn([6, 1]);
          const radioAccessColumnElm = selectColumn([6, 3]);

          await waitUntil(() =>
            expect(
              selectSwitch({
                parent: switchAccessColumnElm,
              }),
            ).not.toBeChecked(),
          );
          await waitUntil(() =>
            expect(
              within(radioAccessColumnElm).queryByDisplayValue('READ_ONLY'),
            ).not.toBeInTheDocument(),
          );
        },
        async () => {
          const switchAccessColumnElm = selectColumn([7, 1]);
          const radioAccessColumnElm = selectColumn([7, 3]);

          await waitUntil(() =>
            expect(
              selectSwitch({
                parent: switchAccessColumnElm,
              }),
            ).toBeChecked(),
          );
          await waitUntil(() =>
            expect(
              selectRadio({
                value: 'READ_WRITE',
                parent: radioAccessColumnElm,
              }),
            ).toBeChecked(),
          );
        },
      ]);
    });

    it('SectionAccessForm - should pre-populate forms on edit', async () => {
      renderApp(<UserPermissionsTemplateForm item={item} />);

      handleMockDOFields();

      await waitApis('listDataObjectTypes');

      const menuTab = selectListItem(3, {
        testId: 'UserPermissionsTemplateForm',
      });
      click(menuTab);

      await waitApis('listDataObjectFields');

      await syncPromises([
        async () => {
          const switchAccessColumnElm = selectColumn([1, 1]);
          const radioAccessColumnElm = selectColumn([1, 3]);

          await waitUntil(() =>
            expect(
              selectSwitch({
                parent: switchAccessColumnElm,
              }),
            ).not.toBeChecked(),
          );
          await isNotInDocument('radio', {
            parent: radioAccessColumnElm,
          });
        },
        async () => {
          click(selectButtonGroup(2));

          await wait();

          const switchAccessColumnElm = selectColumn([1, 1]);
          const radioAccessColumnElm = selectColumn([1, 3]);

          await waitUntil(() =>
            expect(
              selectSwitch({
                parent: switchAccessColumnElm,
              }),
            ).toBeChecked(),
          );
          await waitUntil(() =>
            expect(
              selectRadio({
                value: 'READ_ONLY',
                parent: radioAccessColumnElm,
              }),
            ).toBeChecked(),
          );
        },
      ]);
    });

    it('MQBAccessForm - should pre-populate forms on edit', async () => {
      const renderPage = testRenderer(
        <UserPermissionsTemplateForm item={item} />,
      );

      // const doFields = [doFieldFactory(), doFieldFactory()];
      const allQueries = [queryFactory()];

      renderPage(
        graphql.query('listQueryBuilders', (req, res, ctx) => {
          return res(
            ctx.data({
              listQueryBuilders: allQueries,
            }),
          );
        }),
      );

      const menuTab = selectListItem(4, {
        testId: 'UserPermissionsTemplateForm',
      });
      click(menuTab);

      await waitApis();

      const rows = selectRows();

      expect(rows).toHaveLength(1);

      const switchAccessColumnElm = selectColumn([1, 1]);
      await waitUntil(() =>
        expect(
          selectSwitch({
            parent: switchAccessColumnElm,
          }),
        ).toBeChecked(),
      );

      const switchWidgetColumnElm = selectColumn([1, 3]);
      await waitUntil(() =>
        expect(
          selectSwitch({
            order: 1,
            parent: switchWidgetColumnElm,
          }),
        ).toBeChecked(),
      );
      await waitUntil(() =>
        expect(
          selectSwitch({
            order: 2,
            parent: switchWidgetColumnElm,
          }),
        ).toBeChecked(),
      );

      const radioAccessColumnElm = selectColumn([1, 4]);
      await waitUntil(() =>
        expect(
          selectRadio({
            value: 'READ_WRITE',
            parent: radioAccessColumnElm,
          }),
        ).toBeChecked(),
      );
    });

    it('TaskAccessForm - should pre-populate forms on edit', async () => {
      const renderPage = testRenderer(
        <UserPermissionsTemplateForm item={item} />,
      );

      const doFields = [doFieldFactory(), doFieldFactory()];

      renderPage(
        graphql.query('listDataObjectFields', (req, res, ctx) => {
          return res(
            ctx.data({
              listDataObjectFields: doFields,
            }),
          );
        }),
      );

      const menuTab = selectListItem(5, {
        testId: 'UserPermissionsTemplateForm',
      });
      click(menuTab);
    });

    it('EmailTemplateAccessForm - should pre-populate forms on edit', async () => {
      const renderPage = testRenderer(
        <UserPermissionsTemplateForm item={item} />,
      );

      const doFields = [doFieldFactory(), doFieldFactory()];

      renderPage(
        graphql.query('listDataObjectFields', (req, res, ctx) => {
          return res(
            ctx.data({
              listDataObjectFields: doFields,
            }),
          );
        }),
      );

      const menuTab = selectListItem(6, {
        testId: 'UserPermissionsTemplateForm',
      });
      click(menuTab);
    });

    it('ContactViewAccessForm - should pre-populate forms on edit', async () => {
      const renderPage = testRenderer(
        <UserPermissionsTemplateForm item={item} />,
      );

      const doFields = [doFieldFactory(), doFieldFactory()];

      renderPage(
        graphql.query('listDataObjectFields', (req, res, ctx) => {
          return res(
            ctx.data({
              listDataObjectFields: doFields,
            }),
          );
        }),
      );

      const menuTab = selectListItem(7, {
        testId: 'UserPermissionsTemplateForm',
      });
      click(menuTab);
    });

    it('WorkflowBoardAccessForm - should pre-populate forms on edit', async () => {
      const renderPage = testRenderer(
        <UserPermissionsTemplateForm item={item} />,
      );

      const doFields = [doFieldFactory(), doFieldFactory()];

      renderPage(
        graphql.query('listDataObjectFields', (req, res, ctx) => {
          return res(
            ctx.data({
              listDataObjectFields: doFields,
            }),
          );
        }),
      );

      const menuTab = selectListItem(8, {
        testId: 'UserPermissionsTemplateForm',
      });
      click(menuTab);
    });

    it('OtherSettingsAccessForm - should pre-populate forms on edit', async () => {
      renderApp(<UserPermissionsTemplateForm item={item} />);

      handleMockRelationships();

      await waitApis('listPermissionTemplates');

      const menuTab = selectListItem(9, {
        testId: 'UserPermissionsTemplateForm',
      });
      click(menuTab);

      await waitApis(['listDataObjectTypes', 'listRelationships']);

      const otherSelect = selectSelect({
        value: 'Relationship1,Relationship2,Relationship3,Relationship4',
      });

      await toggleSelect(otherSelect, [
        'Relationship 2',
        'Relationship 3',
        'Relationship 4',
      ]);

      await waitUntil(() => expect(otherSelect).toHaveValue('Relationship1'));
    });

    it('Create - should submit data', async () => {
      const renderPage = testRenderer(<UserPermissionsTemplateForm />);

      const mutationInterceptor = jest.fn();
      renderPage(
        graphql.mutation(
          'createOrUpdatePermissionTemplate',
          (req, res, ctx) => {
            mutationInterceptor(req.variables); // pass the variables here
            return res.once(
              ctx.data({
                createOrUpdatePermissionTemplate: {
                  __typename: 'PermissionTemplate',
                  id: 'PermissionTemplate1',
                },
              }),
            );
          },
        ),
      );

      const submit = selectButton('Save Template');

      expect(submit).toBeDisabled();

      const inputName = selectInput({order: 1});
      const inputDescription = selectInput({order: 2});

      changeInput(inputName, 'New Name');
      changeInput(inputDescription, 'New Description');

      await waitUntil(() => expect(submit).toBeEnabled());

      click(submit);

      await waitUntil(() =>
        expect(mutationInterceptor).toHaveBeenCalledTimes(1),
      );

      await waitUntil(() =>
        expect(mutationInterceptor).toHaveBeenCalledWith({
          input: {name: 'New Name', description: 'New Description', data: []},
        }),
      );
    });

    it('Edit - should submit data', async () => {
      const renderPage = testRenderer(
        <UserPermissionsTemplateForm item={item} />,
      );

      const mutationInterceptor = jest.fn();
      renderPage(
        graphql.mutation(
          'createOrUpdatePermissionTemplate',
          (req, res, ctx) => {
            mutationInterceptor(req.variables); // pass the variables here
            return res.once(
              ctx.data({
                createOrUpdatePermissionTemplate: {
                  __typename: 'PermissionTemplate',
                  id: 'PermissionTemplate1',
                },
              }),
            );
          },
        ),
      );

      const submit = selectButton('Save Template');

      expect(submit).toBeDisabled();

      const inputName = selectInput({order: 1});
      const inputDescription = selectInput({order: 2});

      changeInput(inputName, 'Edited Name');
      changeInput(inputDescription, 'Edited Description');

      await waitUntil(() => expect(submit).toBeEnabled());

      click(submit);

      await waitUntil(() =>
        expect(mutationInterceptor).toHaveBeenCalledTimes(1),
      );

      await waitUntil(() =>
        expect(mutationInterceptor).toHaveBeenCalledWith({
          input: formatVariables({
            ...allPermissionTemplates.find((p) => p.id === item.id),
            doTypes: allDOTypes,
            name: 'Edited Name',
            description: 'Edited Description',
          }),
        }),
      );
    });
  });

  describe('<UserPermissionsTemplateForm>', () => {
    it('display Tabs selection of permission types', () => {
      renderApp(<UserPermissionsTemplateForm />);

      PERMISSION_TEMPLATE_FORM_TABS.forEach((tab) => {
        const tabScope = screen.getByTestId(`listitem-${tab.value}`);
        const withinItemScope = within(tabScope);

        expect(tabScope).toBeInTheDocument();
        expect(withinItemScope.getByText(tab.label)).toBeInTheDocument();
      });
    });

    it('select first Tab by default', () => {
      renderApp(<UserPermissionsTemplateForm />);

      const firstTab = PERMISSION_TEMPLATE_FORM_TABS[0];
      const firstBodyScope = screen.getByTestId(`itembody-${firstTab.value}`);

      expect(firstBodyScope).toBeInTheDocument();

      PERMISSION_TEMPLATE_FORM_TABS.slice(1).forEach((tab) => {
        const otherBodyScope = screen.queryByTestId(`itembody-${tab.value}`);

        expect(otherBodyScope).not.toBeInTheDocument();
      });
    });
  });
});
