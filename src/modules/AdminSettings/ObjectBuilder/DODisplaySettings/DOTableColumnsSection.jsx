import React, {useMemo} from 'react';
import {useFormikContext} from 'formik';
import Section from '@macanta/containers/Section';
import TransferList from '@macanta/containers/TransferList';
import useDeepCompareMemo from '@macanta/hooks/base/useDeepCompareMemo';
import {sortArrayByObjectKey} from '@macanta/utils/array';

const DOTableColumnsSection = (props) => {
  const {values, setValues} = useFormikContext();

  const handleChangeFields = (newFields) => {
    setValues((state) => {
      return {
        ...state,
        sectionsData: state.sectionsData.map((section) => ({
          ...section,
          subGroups: section.subGroups.map((subGroup) => ({
            ...subGroup,
            data: subGroup.data.map((d) => {
              if (newFields.includes(d.id)) {
                return {
                  ...d,
                  showInTable: true,
                  showOrder: newFields.indexOf(d.id) + 1,
                };
              } else if (d.showInTable) {
                return {...d, showInTable: false, showOrder: 0};
              }

              return d;
            }),
          })),
        })),
      };
    });
  };

  const fieldNamesWithOrder = useMemo(() => {
    if (values.sectionsData.length) {
      return values.sectionsData.reduce((acc, section) => {
        const fieldsArr = [...acc];

        fieldsArr.push({
          isSubheader: true,
          label: section.sectionName,
        });

        section.subGroups.forEach((subGroup) => {
          subGroup.data.forEach((d) => {
            fieldsArr.push({
              label: d.label || d.name,
              value: d.id,
              showInTable: d.showInTable,
              showOrder: d.showOrder,
            });
          });
        });

        return fieldsArr;
      }, []);
    }

    return [];
  }, [values.sectionsData]);

  const options = useMemo(() => {
    return fieldNamesWithOrder.map((d) => {
      if (!d.isSubheader) {
        return {
          label: d.label,
          value: d.value,
        };
      }

      return d;
    });
  }, [fieldNamesWithOrder]);

  const chosenFields = useDeepCompareMemo(() => {
    return sortArrayByObjectKey(
      fieldNamesWithOrder.filter((f) => !!f.showInTable),
      'showOrder',
    ).map((f) => f.value);
  }, [fieldNamesWithOrder]);

  return (
    <Section
      title="Table Columns"
      bodyContainerStyle={{
        overflowY: 'auto',
      }}
      bodyStyle={{
        width: '100%',
        height: '100%',
      }}
      {...props}>
      <TransferList
        options={options}
        selectedItems={chosenFields}
        onChangeFields={handleChangeFields}
        emptyMessage="No columns selected"
      />
    </Section>
  );
};

export default DOTableColumnsSection;
