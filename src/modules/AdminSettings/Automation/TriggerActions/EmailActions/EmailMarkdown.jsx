import React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Markdown from '@macanta/components/Markdown';
import EmailAttachments from '@macanta/modules/ContactDetails/EmailAttachments';

const EmailMarkdown = ({values, attachments, ...props}) => {
  return (
    <Box {...props}>
      <Typography
        style={{
          fontWeight: 'bold',
          marginBottom: 20,
        }}>
        {values.subject || 'No subject'}
      </Typography>
      <Markdown>{values.message?.replace(/[\n\r]/g, '')}</Markdown>
      {values.signature && !values.message?.includes('~Sender.Signature~') && (
        <Markdown>
          {`<div style="margin-top: 30px;">${values.signature}</div>`}
        </Markdown>
      )}
      {attachments && <EmailAttachments files={attachments} disableDelete />}
    </Box>
  );
};

export default EmailMarkdown;
