import React from 'react';
import HelperTextBtn from '@macanta/containers/FormField/HelperTextBtn';
import useNotesTasksGuide from '@macanta/hooks/useNotesTasksGuide';

const NotesTasksGuideBtn = () => {
  const notesTasksGuide = useNotesTasksGuide();

  return <HelperTextBtn>{notesTasksGuide?.value}</HelperTextBtn>;
};

export default NotesTasksGuideBtn;
