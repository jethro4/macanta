import fetch from 'isomorphic-fetch';
import {ApolloLink, HttpLink, InMemoryCache} from '@apollo/client';
import {errorLink, retryLink} from '@macanta/config/apolloLink';
// import {createAuthLink} from 'aws-appsync-auth-link';

const AppSyncConfig = {
  graphqlEndpoint: process.env.GATSBY_API_URL,
  region: process.env.AWS_REGION,
  authenticationType: process.env.AWS_AUTHENTICATION_TYPE,
  apiKey: process.env.GATSBY_API_KEY,
};

const url = AppSyncConfig.graphqlEndpoint;
// const region = AppSyncConfig.region;
// const auth = {
//   type: AppSyncConfig.authenticationType,
//   apiKey: AppSyncConfig.apiKey,
// };

export const cache = new InMemoryCache();

export const link = ApolloLink.from([
  errorLink,
  retryLink,
  // createAuthLink({url, region, auth}),
  // appLink,
  new HttpLink({uri: url, fetch}),
]);
