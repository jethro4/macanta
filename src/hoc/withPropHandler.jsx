import React from 'react';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import pick from 'lodash/pick';
import isEqual from 'lodash/isEqual';
import useDeepCompareNextEffect from '@macanta/hooks/base/useDeepCompareNextEffect';
import usePrevious from '@macanta/hooks/usePrevious';

const withPropHandler = (WrappedComponent, optionsArg) => {
  const {propHandlers, parentPath, conditionCallback} = Object.assign(
    {},
    optionsArg,
  );

  if (isEmpty(propHandlers)) {
    throw `Missing "propHandlers" is required`;
  }

  const EnhancedComponent = (props) => {
    const keys = Object.keys(propHandlers);
    const keyPropValues = pick(
      !parentPath ? props : get(props, parentPath),
      keys,
    );
    const prevPropValues = usePrevious(keyPropValues);

    useDeepCompareNextEffect(() => {
      keys.forEach((key) => {
        const currValue = keyPropValues[key];
        const prevValue = prevPropValues[key];

        if (
          conditionCallback
            ? conditionCallback(currValue, prevValue, {
                key,
                props,
              })
            : !isEqual(currValue, prevValue)
        ) {
          const handler = propHandlers[key];

          handler(props[key]);
        }
      });
    }, [keyPropValues]);

    return <WrappedComponent {...props} />;
  };

  return EnhancedComponent;
};

export default withPropHandler;
