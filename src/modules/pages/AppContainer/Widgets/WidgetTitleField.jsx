import React, {useEffect} from 'react';
import * as Styled from '@macanta/modules/NoteTaskForms/styles';

const WidgetTitleField = ({getSelectedWidget, widgetId, ...props}) => {
  useEffect(() => {
    if (widgetId) {
      const selectedWidget = getSelectedWidget(widgetId);

      props.onChange(selectedWidget.label);
    }
  }, [widgetId]);

  return <Styled.TextField {...props} />;
};

export default WidgetTitleField;
