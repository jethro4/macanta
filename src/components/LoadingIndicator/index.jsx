import React from 'react';
import LoadingIndicatorComp from './LoadingIndicator';
import LoadingIndicatorModal from './LoadingIndicatorModal';

const LoadingIndicator = ({modal, ModalProps, ...props}) => {
  if (modal) {
    return <LoadingIndicatorModal ModalProps={ModalProps} {...props} />;
  }

  return <LoadingIndicatorComp {...props} />;
};

export default LoadingIndicator;

export {LoadingIndicatorModal};
