import {gql} from '@apollo/client';

export default gql`
  mutation deleteRelationship(
    $deleteRelationshipInput: DeleteRelationshipInput!
  ) {
    deleteRelationship(input: $deleteRelationshipInput) {
      id
      success
    }
  }
`;
