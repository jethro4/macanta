import {
  capitalizeFirstLetter,
  insertString,
  ordinalNumberOf,
  replaceAll,
  replaceStrings,
  replaceWords,
  uncapitalizeFirstLetter,
} from '@macanta/utils/string';

describe('URI Utils', () => {
  describe('capitalizeFirstLetter', () => {
    it('basic usage', () => {
      const text = 'sample';

      const actualResult = capitalizeFirstLetter(text);
      const expectedResult = 'Sample';

      expect(actualResult).toEqual(expectedResult);
    });
  });

  describe('uncapitalizeFirstLetter', () => {
    it('basic usage', () => {
      const text = 'Sample';

      const actualResult = uncapitalizeFirstLetter(text);
      const expectedResult = 'sample';

      expect(actualResult).toEqual(expectedResult);
    });
  });

  describe('ordinalNumberOf', () => {
    it('single digits', () => {
      const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9];

      const actualResult = arr.map(ordinalNumberOf);
      const expectedResult = [
        '1st',
        '2nd',
        '3rd',
        '4th',
        '5th',
        '6th',
        '7th',
        '8th',
        '9th',
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('double digits', () => {
      const arr = [10, 11, 12, 13, 14, 15, 16, 17, 18, 19];

      const actualResult = arr.map(ordinalNumberOf);
      const expectedResult = [
        '10th',
        '11th',
        '12th',
        '13th',
        '14th',
        '15th',
        '16th',
        '17th',
        '18th',
        '19th',
      ];

      expect(actualResult).toEqual(expectedResult);
    });
  });
});

describe('insertString', () => {
  it('insert string on specified index', () => {
    const text = 'sample';
    const stringToInsert = 'X0X';
    const index = 0;

    const insertOnBeginningResult = insertString({
      text,
      stringToInsert,
      index,
    });
    const insertOn3rdResult = insertString({text, stringToInsert, index: 3});

    expect(insertOnBeginningResult).toEqual('X0Xsample');
    expect(insertOn3rdResult).toEqual('samX0Xple');
  });

  it('insert at end by if no index', () => {
    const text = 'sample';
    const stringToInsert = 'X0X';

    const actualResult = insertString({text, stringToInsert});
    const expectedResult = 'sampleX0X';

    expect(actualResult).toEqual(expectedResult);
  });
});

describe('replaceAll', () => {
  it('replace all matched strings with a new string', () => {
    const text = `Test here, and there`;
    const stringToReplace = 'ere';
    const replaceWith = '1';

    const actualResult = replaceAll(text, stringToReplace, replaceWith);

    expect(actualResult).toEqual(`Test h1, and th1`);
  });
});

describe('replaceStrings', () => {
  it('replace all matched strings based on mapping', () => {
    const text = `Test here, and there`;
    const stringsToReplaceMapping = {
      Te: 'AA',
      ere: 'BBB',
      n: 'C',
    };

    const actualResult = replaceStrings(text, stringsToReplaceMapping);

    expect(actualResult).toEqual(`AAst hBBB, aCd thBBB`);
  });
});

describe('replaceWords', () => {
  it('replace all matched words based on mapping', () => {
    const text = `
      Test
      Test1
      Test2
    `;
    const stringsToReplaceMapping = {
      Test: 'A',
      Test1: 'BB',
      Test2: 'CCC',
    };

    const actualResult = replaceWords(text, stringsToReplaceMapping);

    expect(actualResult).toEqual(`
      A
      BB
      CCC
    `);
  });
});
