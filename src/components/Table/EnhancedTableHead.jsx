import React from 'react';

import ResizableTableCell from './ResizableTableCell';
import * as Styled from './styles';

const EnhancedTableHead = ({
  columns,
  order,
  orderBy,
  onRequestSort,
  hasFrontActionColumn,
  hasActionColumn,
  actionColumn,
  sortableColumns,
  headRowHeight,
}) => {
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };
  const disableDragging = (e) => {
    e.preventDefault();
  };

  return (
    <Styled.TableHead onMouseDown={disableDragging}>
      <Styled.TableHeadRow
        style={{
          height: headRowHeight,
        }}>
        {hasFrontActionColumn && (
          <Styled.TableHeadCell
            style={Object.assign(
              {
                fontWeight: 'bold',
                width: 100,
              },
              actionColumn.style,
              actionColumn.frontWidth && {
                maxWidth: actionColumn.frontWidth,
              },
            )}
            align="center">
            {actionColumn.frontLabel}
          </Styled.TableHeadCell>
        )}
        {columns.map((column, headCellIndex) => (
          <ResizableTableCell
            key={column.id}
            style={{
              zIndex: columns.length + 1 - headCellIndex,
            }}
            column={column}
            order={order}
            orderBy={orderBy}
            onSort={createSortHandler(column.id)}
            sortable={
              !column.disableSort &&
              (!sortableColumns || sortableColumns.includes(column.id))
            }
          />
        ))}
        {hasActionColumn ? (
          <Styled.TableHeadCell
            style={Object.assign(
              {
                zIndex: 1,
                fontWeight: 'bold',
                width: 100,
              },
              actionColumn.style,
              actionColumn.backWidth && {
                maxWidth: actionColumn.backWidth,
              },
            )}
            align="center">
            {actionColumn.label}
          </Styled.TableHeadCell>
        ) : (
          <Styled.TableHeadCell
            style={{
              zIndex: 1,
              padding: 0,
              minWidth: '0.625rem',
            }}
          />
        )}
      </Styled.TableHeadRow>
    </Styled.TableHead>
  );
};

export default EnhancedTableHead;
