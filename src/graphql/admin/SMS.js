import {gql} from '@apollo/client';

export default gql`
  mutation sms($smsInput: SMSInput!) {
    sms(input: $smsInput) {
      success
    }
  }
`;
