import React, {useState} from 'react';
// import range from 'lodash/range';
import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';
import Box from '@mui/material/Box';
import Button from '@macanta/components/Button';
// import {ordinalNumberOf} from '@macanta/utils/string';
import * as Styled from './styles';

const AddGroupForm = ({subGroups, item: itemProp, onAdd, onClose}) => {
  const isEdit = !!itemProp?.id;

  const [item, setItem] = useState({
    ...itemProp,
    position: subGroups.length + (!isEdit ? 1 : 0),
  });

  const handleChange = (key) => (value) => {
    setItem((state) => ({...state, [key]: value}));
  };

  const handleAdd = () => {
    onAdd(item, item.position);
    onClose();
  };

  // const ordinalPositions = useMemo(() => {
  //   return range(1, subGroups.length + 2).map((pos) => ({
  //     label: ordinalNumberOf(pos),
  //     value: String(pos),
  //   }));
  // }, []);

  return (
    <>
      <Box
        style={{
          padding: '1rem',
        }}>
        {/* <Styled.SettingsField
          labelPosition="normal"
          type="Select"
          size="small"
          options={ordinalPositions}
          value={String(item.position)}
          onChange={handleChange('position')}
          label="Position"
          variant="outlined"
        /> */}
        <Styled.SettingsField
          labelPosition="normal"
          type="Number"
          size="small"
          value={item.columns}
          onChange={handleChange('columns')}
          label="Number Of Columns"
          variant="outlined"
        />
        <Styled.SettingsField
          labelPosition="normal"
          autoFocus
          size="small"
          options={subGroups}
          value={item.subGroupName}
          onChange={handleChange('subGroupName')}
          label="Group Name"
          variant="outlined"
        />
      </Box>
      <Box
        style={{
          display: 'flex',
          justifyContent: 'flex-end',
          padding: '12px 1rem',
          borderTop: '1px solid #eee',
        }}>
        <Button
          disabled={!item.subGroupName}
          variant="contained"
          startIcon={!isEdit ? <AddIcon /> : <EditIcon />}
          onClick={handleAdd}
          size="small">
          {!isEdit ? 'Add' : 'Edit'} Group
        </Button>
      </Box>
    </>
  );
};

AddGroupForm.defaultProps = {
  subGroups: [],
  item: {
    fields: [],
    columns: 3,
  },
};

export default AddGroupForm;
