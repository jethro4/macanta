import {gql} from '@apollo/client';

export default gql`
  mutation deletePermissionTemplate($input: DeletePermissionTemplateInput!) {
    deletePermissionTemplate(input: $input) {
      id
      success
    }
  }
`;
