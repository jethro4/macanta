import useMutation from '@macanta/hooks/apollo/useMutation';
import {POST_FIELD_ACTION} from '@macanta/graphql/automation';

const usePostFieldActionMutation = (options) => {
  const mutation = useMutation(POST_FIELD_ACTION, options);

  return mutation;
};

export default usePostFieldActionMutation;
