import React from 'react';
import Button from '@macanta/components/Button';
import Markdown from '@macanta/components/Markdown';
import BorderColorIcon from '@mui/icons-material/BorderColor';
import EmailSignatureBtn from '@macanta/modules/ContactDetails/EmailSignatureBtn';
import useSignature from '@macanta/hooks/admin/useSignature';
import SettingCard from './SettingCard';
import * as Styled from './styles';

const SignatureCard = () => {
  const signatureQuery = useSignature();

  return (
    <>
      <SettingCard
        title="Email Signature"
        bodyStyle={{
          marginTop: 8,
          backgroundColor: '#f5f6fa',
          padding: '8px',
          borderRadius: '4px',
        }}
        BodyComp={
          <Styled.SignaturePreviewContainer>
            <Markdown>{signatureQuery.data}</Markdown>
          </Styled.SignaturePreviewContainer>
        }
        FooterComp={
          <EmailSignatureBtn
            value={signatureQuery.data}
            renderOpenButton={(handleOpen) => {
              return (
                <Button
                  style={{
                    width: '75%',
                    maxWidth: 160,
                  }}
                  variant="contained"
                  onClick={handleOpen}
                  startIcon={<BorderColorIcon />}
                  size="medium">
                  Edit Signature
                </Button>
              );
            }}
            title="Edit Signature"
            anchorOrigin={{
              vertical: 'center',
              horizontal: 'center',
            }}
            transformOrigin={{
              vertical: 'center',
              horizontal: 'center',
            }}
          />
        }
      />
    </>
  );
};

export default SignatureCard;
