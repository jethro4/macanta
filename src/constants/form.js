import {getCategoryExtensions} from '@macanta/modules/AdminSettings/FormBuilder/Forms/helpers';

export const FORM_TYPES = Object.freeze([
  {
    label: 'Standard',
    value: 'Standard',
    disabled: false,
  },
  {
    label: 'Integrated Form',
    value: 'Integrated',
    disabled: false,
  },
  {
    label: '2-Way Synced Form',
    value: '2-Way Synced',
    disabled: false,
  },
]);

export const FIELD_TYPES = Object.freeze([
  {
    label: 'Text',
    value: 'Text',
  },
  {
    label: 'Text Area',
    value: 'TextArea',
  },
  {
    label: 'Whole Number',
    value: 'Number',
  },
  {
    label: 'Decimal',
    value: 'Currency',
  },
  {
    label: 'Email',
    value: 'Email',
  },
  {
    label: 'Password',
    value: 'Password',
  },
  {
    label: 'Select',
    value: 'Select',
    hasChoices: true,
  },
  {
    label: 'Radio',
    value: 'Radio',
    hasChoices: true,
  },
  {
    label: 'Checkbox',
    value: 'Checkbox',
    hasChoices: true,
  },
  {
    label: 'Date',
    value: 'Date',
  },
  {
    label: 'Date & Time',
    value: 'DateTime',
  },
  {
    label: 'File Upload',
    value: 'FileUpload',
  },
  {
    label: 'URL',
    value: 'URL',
  },
  {
    label: 'Data Reference',
    value: 'DataReference',
  },
]);

export const FIELDS_WITH_CHOICES = Object.freeze(
  FIELD_TYPES.filter((field) => !!field.hasChoices).map((field) => field.value),
);

export const NON_TEXT_FIELDS = Object.freeze(
  FIELD_TYPES.filter((field) =>
    ['Radio', 'Checkbox'].includes(field.value),
  ).map((field) => field.value),
);

export const DATE_FIELD_TYPES = Object.freeze(['Date', 'DateTime']);

export const UPLOAD_TYPES = [
  {
    label: 'Single',
    value: 'single',
  },
  {
    label: 'Multiple',
    value: 'multiple',
  },
];

export const UPLOAD_FIELD_TYPES = [
  {
    label: 'Regular',
    value: 'regular',
  },
  {
    label: 'Field Specific',
    value: 'fieldSpecific',
  },
];

export const MIME_TYPES_WITH_EXTENSION = Object.freeze({
  image: {
    'image/bmp': ['bmp'],
    'image/gif': ['gif'],
    'image/vnd.microsoft.icon': ['ico'],
    'image/jpeg': ['jpeg', 'jpg'],
    'image/png': ['png'],
    'image/svg+xml': ['svg'],
    'image/tiff': ['tif', 'tiff'],
    'image/webp': ['webp'],
  },
  text: {
    'text/css': ['css'],
    'text/csv': ['csv'],
    'text/html': ['html', 'htm'],
    'text/calendar': ['ics'],
    'text/javascript': ['js', 'mjs'],
    'text/plain': ['txt'],
    'text/xml': ['xml'],
  },
  video: {
    'video/x-msvideo': ['avi'],
    'video/mpeg': ['mpeg'],
    'video/ogg': ['ogv'],
    'video/mp2t': ['ts'],
    'video/webm': ['webm'],
    'video/3gpp': ['3gp'],
    'video/3gpp2': ['3g2'],
  },
  audio: {
    'audio/aac': ['aac'],
    'audio/midi': ['mid', 'midi'],
    'audio/x-midi': ['mid', 'midi'],
    'audio/mpeg': ['mp3'],
    'audio/ogg': ['oga'],
    'audio/opus': ['opus'],
    'audio/wav': ['wav'],
    'audio/webm': ['weba'],
    'audio/3gpp': ['3gp'],
    'audio/3gpp2': ['3g2'],
  },
  application: {
    'application/x-abiword': ['abw'],
    'application/x-freearc': ['arc'],
    'application/vnd.amazon.ebook': ['azw'],
    'application/octet-stream': ['bin'],
    'application/x-bzip': ['bz'],
    'application/x-bzip2': ['bz2'],
    'application/x-csh': ['csh'],
    'application/msword': ['doc'],
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document': [
      'docx',
    ],
    'application/vnd.ms-fontobject': ['eot'],
    'application/epub+zip': ['epub'],
    'application/gzip': ['gz'],
    'application/java-archive': ['jar'],
    'application/json': ['json'],
    'application/ld+json': ['jsonld'],
    'application/vnd.apple.installer+xml': ['mpkg'],
    'application/vnd.oasis.opendocument.presentation': ['odp'],
    'application/vnd.oasis.opendocument.spreadsheet': ['ods'],
    'application/vnd.oasis.opendocument.text': ['odt'],
    'application/ogg': ['ogx'],
    'application/pdf': ['pdf'],
    'application/x-httpd-php': ['php'],
    'application/vnd.ms-powerpoint': ['ppt'],
    'application/vnd.openxmlformats-officedocument.presentationml.presentation': [
      'pptx',
    ],
    'application/vnd.rar': ['rar'],
    'application/rtf': ['rtf'],
    'application/x-sh': ['sh'],
    'application/x-shockwave-flash': ['swf'],
    'application/x-tar': ['tar'],
    'application/vnd.visio': ['vsd'],
    'application/xhtml+xml': ['xhtml'],
    'application/vnd.ms-excel': ['xls'],
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': [
      'xlsx',
    ],
    'application/xml': ['xml'],
    'application/vnd.mozilla.xul+xml': ['xul'],
    'application/zip': ['zip'],
    'application/x-7z-compressed': ['7z'],
  },
  font: {
    'font/otf': ['otf'],
    'font/ttf': ['ttf'],
    'font/woff': ['woff'],
    'font/woff2': ['woff2'],
  },
});

export const ALL_MIME_TYPE_CATEGORIES = Object.freeze([
  {
    label: 'Image',
    value: 'image',
  },
  {
    label: 'Document',
    value: 'document',
  },
  {
    label: 'Audio / Video',
    value: 'media',
  },
]);

export const ALL_MIME_TYPE_CATEGORIES_WITH_ALL = Object.freeze([
  {
    label: 'All',
    value: 'all',
  },
  ...ALL_MIME_TYPE_CATEGORIES,
]);

export const ALL_MIME_TYPES = Object.freeze(
  Object.keys(MIME_TYPES_WITH_EXTENSION).reduce((acc, category) => {
    const mimeTypes = Object.keys(MIME_TYPES_WITH_EXTENSION[category]);
    return acc.concat(mimeTypes);
  }, []),
);

export const ALL_EXTENSIONS = Object.freeze(
  Object.keys(MIME_TYPES_WITH_EXTENSION).reduce((acc, category) => {
    const accArr = [...acc];
    const categoryExtensions = getCategoryExtensions(category);

    categoryExtensions.forEach((ext) => {
      if (!accArr.includes(ext)) {
        accArr.push(ext);
      }
    });

    return accArr;
  }, []),
);

export const ALL_CATEGORIES = Object.freeze(
  Object.keys(MIME_TYPES_WITH_EXTENSION),
);
