import {
  handleMockDOFields,
  handleMockDOTypes,
} from '@tests/integrationTests/mockUtils';

import {mockDOSchedules} from '@tests/integrationTests/mockHandlers/scheduler';
import doFieldFactory from '@tests/jestSettings/mockFactory/doFieldFactory';
import doSchedulesFactory from '@tests/jestSettings/mockFactory/doSchedulesFactory';
import doTypeFactory from '@tests/jestSettings/mockFactory/doTypeFactory';

export const handleDOTypes = () => {
  handleMockDOTypes([
    doTypeFactory({
      id: 'DataObject4',
      title: `DO Type 4`,
    }),
    doTypeFactory({
      id: 'DataObject2',
      title: `DO Type 2`,
    }),
    doTypeFactory({
      id: 'DataObject1',
      title: `DO Type 1`,
    }),
    doTypeFactory({
      id: 'DataObject3',
      title: `DO Type 3`,
    }),
  ]);
};

export const handleDOFields = () => {
  handleMockDOFields({
    DataObject1: [
      doFieldFactory({
        id: 'Field1',
        type: 'Date',
        name: 'Last Login',
      }),
      doFieldFactory({
        id: 'Field2',
        type: 'DateTime',
        name: 'Event Start',
      }),
      doFieldFactory({
        id: 'Field3',
        type: 'DateTime',
        name: 'Event End',
      }),
    ],
    DataObject2: [
      doFieldFactory({
        id: 'Field4',
        type: 'Date',
        name: 'Sample Date Field',
      }),
    ],
  });
};

export const handleDOSchedules = () => {
  mockDOSchedules([
    doSchedulesFactory({
      schedulerType: 'Date',
      name: 'Sample Date Scheduler',
      description: 'Sample Description',
      groupId: 'DataObject2',
      userRelationshipId: 'Relationship1',
      contactRelationshipId: 'Relationship2',
      startDateFieldId: 'Field4',
      endDateFieldId: '',
    }),
    doSchedulesFactory({
      schedulerType: 'Date',
      name: 'Last Login Scheduler',
      description: 'This Shows User When Their Last Login',
      groupId: 'DataObject1',
      userRelationshipId: 'Relationship1',
      contactRelationshipId: 'Relationship2',
      startDateFieldId: 'Field1',
      endDateFieldId: '',
    }),
    doSchedulesFactory({
      schedulerType: 'DateTime',
      name: 'Events Scheduler',
      description: 'Schedules for events',
      groupId: 'DataObject1',
      userRelationshipId: 'Relationship1',
      contactRelationshipId: 'Relationship2',
      startDateFieldId: 'Field2',
      endDateFieldId: 'Field3',
    }),
  ]);
};
