import React from 'react';
import Tooltip from '@macanta/components/Tooltip';
import {textContent} from '@macanta/utils/react';
import * as Styled from './styles';

const Button = ({TooltipProps, ...props}) => {
  const ariaLabel = textContent(props.children);

  let buttonComp = <Styled.Button {...props} aria-label={ariaLabel} />;

  if (TooltipProps?.title) {
    buttonComp = <Tooltip {...TooltipProps}>{buttonComp}</Tooltip>;
  }

  return buttonComp;
};

export default Button;
