import React from 'react';
import SearchFieldComp from './SearchField';
import SearchCustomField from './SearchCustomField';

let SearchField = ({custom, ...props}) => {
  if (custom) {
    return <SearchCustomField {...props} />;
  }

  return <SearchFieldComp {...props} />;
};

export default SearchField;

export {SearchCustomField};
