import React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Theme from '@macanta/containers/Theme';
import ProgressAlertProvider from '@macanta/containers/Alert/ProgressAlertProvider';

const BaseTheme = ({children, ...props}) => {
  return (
    <Theme {...props}>
      <CssBaseline />
      <ProgressAlertProvider>{children}</ProgressAlertProvider>
    </Theme>
  );
};

export default BaseTheme;
