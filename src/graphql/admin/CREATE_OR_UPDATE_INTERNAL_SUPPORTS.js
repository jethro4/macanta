import {gql} from '@apollo/client';

export default gql`
  mutation createOrUpdateInternalSupports(
    $createOrUpdateInternalSupportsInput: CreateOrUpdateInternalSupportsInput!
  ) {
    createOrUpdateInternalSupports(
      input: $createOrUpdateInternalSupportsInput
    ) {
      success
    }
  }
`;
