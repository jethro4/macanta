/* eslint-disable react/jsx-filename-extension */
import React, {useState, useMemo, useLayoutEffect} from 'react';
import {GET_APP_SETTINGS} from '@macanta/graphql/app';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import ThemeColorContext from '@macanta/modules/BaseTheme/ThemeColorContext';
import useQuery from '@macanta/hooks/apollo/useQuery';
import * as Storage from '@macanta/utils/storage';
import {
  DEFAULT_MACANTA_PRIMARY_COLOR,
  generateColorsByThemeBaseColor,
} from '@macanta/utils/colors';

const useThemeColors = () => {
  const cachedPrimaryColor = Storage.getItem('primaryColor');

  const {data} = useQuery(GET_APP_SETTINGS, {
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const uiColour = data?.getAppSettings?.uiColour;

  const primaryColor =
    uiColour && uiColour !== 'false'
      ? uiColour
      : cachedPrimaryColor || DEFAULT_MACANTA_PRIMARY_COLOR;

  const [themeColor, setThemeColor] = useState(primaryColor);

  const renderThemeColorProvider = (children) => {
    return (
      <ThemeColorContext.Provider
        value={{
          themeColor,
          setThemeColor,
        }}>
        {children}
      </ThemeColorContext.Provider>
    );
  };

  useLayoutEffect(() => {
    if (primaryColor) {
      Storage.setItem('primaryColor', primaryColor);

      if (primaryColor !== themeColor) {
        setThemeColor(primaryColor);
      }
    }
  }, [primaryColor]);

  const themeColors = useMemo(() => {
    const generatedColors = generateColorsByThemeBaseColor(themeColor);

    return generatedColors;
  }, [themeColor]);

  return {
    themeColors,
    renderThemeColorProvider,
  };
};

export default useThemeColors;
