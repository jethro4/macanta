import {gql} from '@apollo/client';
import {PERMISSIONS_FRAGMENT} from './fragments';

export default gql`
  query getAccessPermissions(
    $appName: String!
    $apiKey: String!
    $email: String!
  ) {
    getAccessPermissions(appName: $appName, apiKey: $apiKey, email: $email) {
      isAdmin
      isUser
      ...permissionsFragment
    }
  }
  ${PERMISSIONS_FRAGMENT}
`;
