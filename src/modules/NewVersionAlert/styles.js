import {experimentalStyled as styled} from '@mui/material/styles';
import AlertComp from '@macanta/containers/Alert';

export const Alert = styled(AlertComp)`
  align-items: center;
  padding-top: 0;
  padding-bottom: 0;
  background-color: ${({theme}) => theme.palette.info.main};
`;
