import useQuery from '@macanta/hooks/apollo/useQuery';
import {GET_ACCESS_PERMISSIONS} from '@macanta/graphql/permissions';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {ACCESS_LEVELS} from '@macanta/constants/accessLevels';
import * as Storage from '@macanta/utils/storage';

const useNotesPermissions = () => {
  const {email: userEmail} = Storage.getItem('userDetails');

  const {data} = useQuery(GET_ACCESS_PERMISSIONS, {
    variables: {
      email: userEmail,
    },
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
    onError() {},
  });

  const isAdmin = data?.getAccessPermissions?.isAdmin;

  const tabsAccess = data?.getAccessPermissions?.tabs;
  const notesTabAccess = tabsAccess?.find((tab) => tab.groupId === 'notes_tab');

  const isVisible =
    isAdmin ||
    [ACCESS_LEVELS.READ_WRITE, ACCESS_LEVELS.READ_ONLY].includes(
      notesTabAccess?.permission,
    );
  const isReadOnly =
    !isAdmin && notesTabAccess?.permission !== ACCESS_LEVELS.READ_WRITE;

  return {isVisible, isReadOnly};
};

export default useNotesPermissions;
