import React, {useState, useLayoutEffect, useMemo} from 'react';
import ButtonGroup from '@macanta/components/ButtonGroup';
import Button from '@macanta/components/Button';
import AddDOItemBtn from '@macanta/modules/DataObjectItems/AddDOItemBtn';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import DOTypeWithFields from './DOTypeWithFields';
import * as Styled from './styles';

const DOTypeSelectTable = ({
  loading: doLoading,
  dataObjects,
  onCreate,
  children,
  renderSearch,
  ...props
}) => {
  const [selectedGroupId, setSelectedGroupId] = useState(null);
  const [filteredDOTypes, setFilteredDOTypes] = useState(dataObjects || []);

  const doTypesQuery = useDOTypes({
    removeEmail: true,
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const doTypes = doTypesQuery.data?.listDataObjectTypes;

  useLayoutEffect(() => {
    if (doTypes && dataObjects) {
      setFilteredDOTypes(
        doTypes
          .filter((type) => {
            const doObj = dataObjects.find((d) => d.groupId === type.id);

            return type.id === selectedGroupId || !!doObj?.items?.length;
          })
          .map((type) => {
            const doObj = dataObjects.find((d) => d.groupId === type.id);

            return {
              groupId: type.id,
              type: type.title,
              items: doObj?.items?.length ? doObj.items : [],
            };
          }),
      );
    }
  }, [doTypes, dataObjects]);

  useLayoutEffect(() => {
    const filteredDOTypesArr = filteredDOTypes.filter((item) =>
      selectedGroupId ? item.groupId === selectedGroupId : item.items.length,
    );

    if (filteredDOTypesArr.length) {
      setSelectedGroupId(filteredDOTypesArr[0].groupId);
    }
  }, [filteredDOTypes]);

  const selectedDO = useMemo(() => {
    if (selectedGroupId) {
      return filteredDOTypes.find((d) => d.groupId === selectedGroupId);
    }

    return null;
  }, [selectedGroupId, filteredDOTypes]);

  return (
    <DOTypeWithFields
      loading={doLoading}
      groupId={selectedDO?.groupId}
      items={selectedDO?.items}
      {...props}>
      {(doResultsState) => {
        return (
          <>
            <Styled.DOTypeButtonGroupContainer
              style={{
                ...(filteredDOTypes.length === 0 && {
                  padding: 0,
                  marginBottom: '1rem',
                }),
              }}>
              {filteredDOTypes.length > 0 && (
                <>
                  <ButtonGroup
                    sx={{
                      '& .MuiButton-root': {
                        position: 'relative',

                        '&:after': {
                          content: '""',
                          position: 'absolute',
                          width: '0.5px',
                          right: '-0.5px',
                          top: 0,
                          bottom: 0,
                          backgroundColor: 'primary.main',
                          borderRadius: '4px',
                        },
                      },
                    }}
                    style={{
                      flexWrap: 'wrap',
                    }}
                    selectedButtonIndex={filteredDOTypes.findIndex(
                      (item) => item.groupId === selectedDO?.groupId,
                    )}
                    size="medium"
                    variant="outlined"
                    aria-label="primary button group">
                    {filteredDOTypes.map(({groupId, type, items}, index) => {
                      return (
                        <Button
                          key={index}
                          onClick={() => setSelectedGroupId(groupId)}>
                          {type} ({items.length})
                        </Button>
                      );
                    })}
                  </ButtonGroup>

                  <AddDOItemBtn
                    doTitle={selectedDO?.type}
                    groupId={selectedDO?.groupId}
                    doFields={doResultsState.doFields}
                    relationships={doResultsState.relationships}
                    onSave={onCreate}
                  />
                </>
              )}
            </Styled.DOTypeButtonGroupContainer>

            {renderSearch &&
              renderSearch(
                doResultsState.doFields,
                selectedDO?.type,
                selectedDO?.groupId,
              )}

            {children({...doResultsState, doTitle: selectedDO?.type})}
          </>
        );
      }}
    </DOTypeWithFields>
  );
};

export default DOTypeSelectTable;
