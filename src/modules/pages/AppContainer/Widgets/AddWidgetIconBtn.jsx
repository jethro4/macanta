import React, {useState} from 'react';
import Modal from '@macanta/components/Modal';
import WidgetForms from './WidgetForms';
import AddIcon from '@mui/icons-material/Add';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import Button from '@macanta/components/Button';

const AddWidgetIconBtn = ({taskWidgets, queryWidgets, onSuccess, ...props}) => {
  const [showModal, setShowModal] = useState(false);
  const {displayMessage} = useProgressAlert();

  const handleShowModal = () => {
    setShowModal(true);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  const handleSuccess = (newWidget) => {
    displayMessage('Successfully added widget');

    onSuccess(newWidget);
    handleCloseModal();
  };

  return (
    <>
      <Button
        disabled={!taskWidgets?.length && !queryWidgets?.length}
        onClick={handleShowModal}
        size="small"
        variant="contained"
        startIcon={<AddIcon />}
        {...props}>
        Add Widget
      </Button>
      <Modal
        headerTitle={`Add Widget`}
        open={showModal}
        onClose={handleCloseModal}>
        <WidgetForms
          taskWidgets={taskWidgets}
          queryWidgets={queryWidgets}
          onSuccess={handleSuccess}
        />
      </Modal>
    </>
  );
};

export default AddWidgetIconBtn;
