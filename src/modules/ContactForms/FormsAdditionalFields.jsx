import React from 'react';
import {useFormikContext} from 'formik';
import Box from '@mui/material/Box';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import {SubSection} from '@macanta/containers/Section';
import FormField from '@macanta/containers/FormField';

const FormsAdditionalFields = ({readOnly, subGroups, loading}) => {
  const {values, errors, setValues} = useFormikContext();

  const handleCustomFieldChange = (fieldName) => (value) => {
    setValues((state) => {
      let customFields = state.customFields || [];

      const customField = customFields.find((cf) => cf.name === fieldName);

      if (customField) {
        customFields = customFields.map((cf) => {
          if (cf.name === customField.name) {
            return {...cf, value};
          }

          return cf;
        });
      } else {
        customFields = customFields.concat({
          name: fieldName,
          value,
        });
      }

      return {...state, customFields};
    });
  };

  return (
    <>
      <LoadingIndicator fill loading={loading} />
      <Box
        style={{
          display: 'flex',
          paddingBottom: '1rem',
          flexWrap: 'wrap',
        }}>
        {subGroups.map(({subGroupName, data}) => {
          const finalSubGroupName =
            subGroupName !== 'Overview' ? subGroupName : '';

          return (
            <SubSection
              key={finalSubGroupName}
              style={{
                width: 350,
                minWidth: 350,
                alignSelf: 'flex-start',
              }}
              title={finalSubGroupName}>
              {data.map((field) => {
                const customField = values.customFields?.find(
                  (cf) => cf.name === field.name,
                );

                return (
                  <FormField
                    key={field.id}
                    readOnly={readOnly}
                    type={field.type}
                    required={field.required}
                    options={field.choices}
                    error={errors[field.name]}
                    onChange={handleCustomFieldChange(field.name)}
                    label={field.name}
                    placeholder={field.placeholder}
                    headingText={field.headingText}
                    helperText={field.helpText}
                    addDivider={field.addDivider}
                    value={customField?.value}
                    fullWidth
                    size="small"
                    variant="outlined"
                  />
                );
              })}
            </SubSection>
          );
        })}
      </Box>
    </>
  );
};

export default FormsAdditionalFields;
