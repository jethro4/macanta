import React from 'react';
import Box from '@mui/material/Box';
import SelectUserField from '@macanta/modules/NoteTaskForms/SelectUserField';

const SelectUserFilter = ({userId, onChange}) => {
  return (
    <Box
      style={{
        minWidth: 270,
      }}>
      <SelectUserField
        labelPosition="normal"
        size="xsmall"
        fieldValueKey="id"
        style={{
          margin: 0,
        }}
        onChange={onChange}
        label="Select User"
        variant="outlined"
        value={userId}
        fullWidth
      />
    </Box>
  );
};

export default SelectUserFilter;
