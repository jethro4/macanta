import React, {useState} from 'react';
import Button from '@macanta/components/Button';
import LinkIcon from '@mui/icons-material/Link';
import Modal from '@macanta/components/Modal';
import useHideConnectButtonGroupIds from '@macanta/hooks/useHideConnectButtonGroupIds';
import MultiDOConnectContactsForms from './MultiDOConnectContactsForms';

const MultiDOConnectContactsBtnContainer = (props) => {
  const hiddenGroupIds = useHideConnectButtonGroupIds();

  return !hiddenGroupIds.includes(props.groupId) ? (
    <MultiDOConnectContactsBtn {...props} />
  ) : null;
};

const MultiDOConnectContactsBtn = ({doTitle, groupId, onSave}) => {
  const [showForms, setShowForms] = useState(false);

  const handleMultiDOConnect = () => {
    setShowForms(true);
  };

  const handleCloseConnectModal = () => {
    setShowForms(false);
  };

  const handleSuccess = () => {
    handleCloseConnectModal();

    onSave();
  };

  return (
    <>
      <Button
        onClick={handleMultiDOConnect}
        size="small"
        variant="contained"
        startIcon={<LinkIcon />}>
        Connect Contact
      </Button>
      <Modal
        headerTitle={`Connect Selected Contact to Existing ${doTitle}`}
        open={showForms}
        onClose={handleCloseConnectModal}
        contentWidth={'100%'}
        contentHeight={'94%'}>
        <MultiDOConnectContactsForms
          doTitle={doTitle}
          groupId={groupId}
          onSuccess={handleSuccess}
        />
      </Modal>
    </>
  );
};

export default MultiDOConnectContactsBtnContainer;
