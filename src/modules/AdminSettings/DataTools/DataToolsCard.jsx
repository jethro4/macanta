import React from 'react';
import {navigate} from 'gatsby';
import GroupIcon from '@mui/icons-material/Group';
import SettingCard from '@macanta/modules/AdminSettings/SettingCard';

const DataToolsCard = () => {
  /* const navigate = useNavigate();*/

  const handleCustomTabs = () => {
    navigate('/app/admin-settings/data-import-export');
  };

  return (
    <SettingCard
      IconComp={GroupIcon}
      title="Data Tools"
      description="Import Contacts and Data Objects"
      onClick={handleCustomTabs}
    />
  );
};

export default DataToolsCard;
