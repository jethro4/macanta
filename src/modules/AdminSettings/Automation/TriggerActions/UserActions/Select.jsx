import React from 'react';
import SelectAction from '@macanta/modules/AdminSettings/Automation/TriggerActions/SubForms/SelectAction';
import {useUserActions} from '@macanta/hooks/automation';

const Select = ({...props}) => {
  const userActionsQuery = useUserActions({
    variables: {
      id: '',
    },
  });

  return (
    <SelectAction
      loading={userActionsQuery.initLoading}
      type="Select"
      options={userActionsQuery.result}
      labelKey="name"
      valueKey="id"
      {...props}
    />
  );
};

export default Select;
