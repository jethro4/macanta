import React from 'react';
import range from 'lodash/range';
import Box from '@mui/material/Box';
import FlexGrid from '@macanta/components/Base/FlexGrid';
import BasicInfoForm from '@macanta/modules/AdminSettings/Automation/TriggerActions/SubForms/BasicInfoForm';
import FormField from '@macanta/containers/FormField';
import withForm from '@macanta/components/Form/withForm';
import {findItem} from '@macanta/utils/array';
import {CONTACT_FIELDS} from '@macanta/constants/contactFields';
import BodySection from '@macanta/modules/AdminSettings/UserManagement/BodySection';
import ActionForm from '@macanta/modules/AdminSettings/Automation/TriggerActions/SubForms/ActionForm';

const phoneFields = range(1, 4).map((num) => ({
  ...findItem(CONTACT_FIELDS, {id: `field_phone${num}`}),
  label: `Phone ${num}`,
}));

let Form = (props) => {
  // const handleChange = (val) => {};

  // const handleSave = () => {
  //   onSave();
  // };

  return (
    <ActionForm>
      <FlexGrid
        {...props}
        cols={2}
        margin={[0, '2rem']}
        // border={[0, 1]}
        sx={{
          padding: '1rem',
          height: '100%',
          ...props.sx,
        }}>
        <Box colWidth={0.7}>
          <BasicInfoForm labelPosition="normal" actionLabel="SMS Action Name" />
        </Box>

        <BodySection
          title="SMS Settings"
          bodyStyle={{
            padding: '1rem',
          }}>
          <FormField
            name="phoneField"
            labelPosition="normal"
            type="Select"
            options={phoneFields}
            valueKey="name"
            label="Contact Phone Field"
            size="small"
            variant="outlined"
          />

          <FormField
            name="message"
            labelPosition="normal"
            label="SMS Message"
            type="TextArea"
          />
        </BodySection>
      </FlexGrid>
    </ActionForm>
  );
};

Form = withForm(Form);

export default Form;
