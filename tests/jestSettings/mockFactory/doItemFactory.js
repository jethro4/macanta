import {nextFactoryId} from './factory';

export default ({isSearch, reset, ...overwrites} = {}) => {
  const typename = !isSearch
    ? 'ItemDataObject'
    : 'DataObjectSearchResponseItemDataObject';

  const {id, currentIndex} = nextFactoryId(typename, {reset});

  return Object.assign(
    {
      __typename: typename,
      id,
      data: [
        {
          id: 'ItemDataObject-Field1',
          fieldId: 'Field1',
          value: `Test Text ${currentIndex}`,
          __typename: 'ItemData',
        },
        {
          id: 'ItemDataObject-Field2',
          fieldId: 'Field2',
          value: 'Test TextArea',
          __typename: 'ItemData',
        },
        {
          id: 'ItemDataObject-Field3',
          fieldId: 'Field3',
          value: 'Choice 1',
          __typename: 'ItemData',
        },
        {
          id: 'ItemDataObject-Field4',
          fieldId: 'Field4',
          value: 'Choice 1',
          __typename: 'ItemData',
        },
        {
          id: 'ItemDataObject-Field5',
          fieldId: 'Field5',
          value: ['Choice 1', 'Choice 2'].join(','),
          __typename: 'ItemData',
        },
      ],
      connectedContacts: [
        connectedContactFactory({
          relationships: [
            connectedContactRelationshipFactory(),
            connectedContactRelationshipFactory(),
          ],
        }),
        connectedContactFactory({
          relationships: [connectedContactRelationshipFactory({reset: true})],
        }),
      ],
    },
    !isSearch && {
      groupId: `DataObject${currentIndex}`,
      notes: [
        {
          id: '1',
          accepted: false,
          userId: 'Contact1',
          creationDate: '2022-08-17 12:45:04',
          completionDate: '',
          lastUpdated: '2022-08-17 12:45:04',
          lastUpdatedBy: '0',
          endDate: '',
          type: 'General',
          actionDate: '',
          title: 'Test 3',
          noteType: null,
          note: '',
          tags: ['general'],
          createdBy: 'FirstName1 LastName1',
          __typename: 'ItemNote',
        },
      ],
      attachments: [
        {
          id: '1',
          fileName: 'Test.csv',
          thumbnail:
            'iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAIAAAAlC+aJAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABnRSTlMAAAAAAABupgeRAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAH6MAAB+jAH2GftsAAAAB3RJTUUH5gkCFAEPOVvfuQAACW5JREFUaN7VmltvXFcVx/9r7X3OmbvHju3YuTRNmzaFXqVSAX1AQAVqKyGQeIBX3pBAiFe+AZ+hCKkfABCgCgGlgiIojUBRmrahl6SJL4kzvoznPuey9148zNix4xl7zjhNzZZfPOecdf6/vddt7xnCjlGp3MB9HERkTNJq1jzPFxFrDURE4Pn+2YeeGtEI30/FA4YIROIoDLttZy2IiJDE8fVPLv+fABCJCADt+QDGYPisAQQAARDniHgMhs8aYAsCgMg4DEcBYAdKeoajBTAGw5EDGMpwbTDDUQS4mwFEhCQZzHBEAXYxuP0Yji7AFgP1GWiL4ZN3xwKQkQZE0mkk9CT2qsHAwcwiorVvksTz/Vy+uPOqHvFFrBSIDr5PxDmXbpKdgwiItgvC7usASMQlcah9v1mv5vKlcQA6zZa1lvZlEBGlVCafG10/MftBZgtgqF0i1r7vrNXal92LfDAAETnnNlYq3VaHmUXckNvYOZct5E48/GBv0UeYfWFW+WL5AAFAzznF6+tJB3CHg6A8PWwRRETiuDeR6eLgIFTZFgDQVvOXGoAAcVKenipNTzljd4WcgLVqrFdXl1e24zFlLI8Oe7fhkVcAEAhr7QeBVWaXy4oorVnrfm9JRKOEe3qtA0cKAGZubGx2W629pokoiWKldBInlcWbh5TvnPMzwdTxGTAf6GCjAvTmNmy323U7IGULWCntaWtMfb16GA/qJYBsPj95fIZHMJQuBgrlop/Nyp5MT8xRp9tptjzfn5ie2htqo76FOe6G7XqD1agVNk0MiCuUy1PzszYxO71cRJSnqyurrc26V/BmT58kJojsU1yH2O/baW7WRn8qBQCB4ihq1xvOOmyp680zK47DiJisdZ1mk4hFJJX8O3aiiNKQp4gBUtxYr9bXNobdo7ROoujWtYU0yvdMExEpHt3/jnQ3OspIE8TWlY7PFCcnBrpQs1qv3q5k8vmZU3OHcaHmZr16qzL6s+kKmR8E+YnSwCCOOqE4UYpzxeJhgjjqhJImC6cJYuJWrRZH0bA0ylolcbK6dPNwabTb24jdYwABiKldbzY2avsUMpMkGyur4xcygdKKmEZ/PkUMQCSTz3uBN6yViDqh8nS+VBy2Ozn4LURJlITt9qcSA8650rHJqeMz1gxo5qqVtdvNxUwhN3/2AeKeC6XsibbsdJrN0R9KAQCQNSaOor3ttLXWGkMgcS6OYuaxYmDLTiryFDHAzPX1amNjc/ANzilPJ1F869qN1NJ32hFhTlHI0qwAwRojbtiWkohZnEuMOQwAMadyvVTdqJuYnc6XSs7Zne2KQJhVu9GorW5kctnJuRmiserAHTvrn04hE8nmc+XZYwMLmbNm060prSaOTfWCOFVPtstOmvhJtyNrN5rOOufc7iQEZu6226yVMWZjpUJEgnStxC47/CkUMuecc9Ks1urr1YE39GIg7oarSzdTKr97mpxLcTp2MEBvOYNspmd9/zvvwWYecM71XjeKJ42wAiJENH1y/l5oSzG2MsHhAXrmmO/J7I44BAcfeKUDGPWY5r6P3QDuaIocGUDy/met53AA3lsL1ElEM9TBR2L3exDBOjJOct5wgLeXvL/fsKdKUs5QbO9ZXjzkEIAgvqJaqJYbyVceHAogvpJSgKwnGQ3mAzI74c6uQPbsY7ev9i8JiNDLjHe1STtPB4YTiM+U9aQUiK+GAvStC0i2s9gQw0xwgjgRJ6QInu5/si09tmIsCKQUNAOAsXACzeDdCT6xAKDUvqstJIBsKRwK0Bd8kPczIUwA0HSR84G0IrfeABEFHkTgIFHMpSxPFWCdq3cQGRAoH0CxhAmM3bmho0IWiqQbw8pBHjvgvCLVjqyvXrqJOjHpf/sL+vFTUAxjzQe34tcuuuUqPAVj/K897r/0DJWyAKTe6b7yhr2y7H39ueBbz8Z/fCf6/X8o44Mg3Vidnc3+8BtutRG+8hdJYmiVNnmkBGBCmKj5cuZH3+Spgrl43V5fVedP6PMnkn9+iMV1Ca333Lnge1+WZjf+82UAVMpKswsmd2sTRPqJ0/Hrl2EsPA3r1Lk5KmXdlWXXDCnQY6S+lABOBPBefJqnCtGv3o7/cAkE+tsVKuel1oavERv97FkA4S//ai7dQOCBiAJNucBeq7i1Bp8+xjMlu7hBSsjX6vw8APPOAqwF+0j1FW1vStNNf2x4pqSfedBtNJO3PqKcT5MFMEm11Y9UAIoBSDsCERUzlA/ABM3SDO1HKwDUmRk4B2NpqqAenpNG1y2skTfO9KcEAIl1PFMiX7vFDQkTeBrGggmBhgBMsM5cvA4g84Ov6qfPSDuSbgwQiOGc/e9NAOqROdJKYqMemKaMZz646Tbb8FN7/xgAgAjlfADSCmG3llu2migRyvrmwtXo1xd4ppj96cvZH7+ozkxLmIBAnrbX16Qbq3NzVMjAOHX+BAD73hKsQ5rjxEMAAH3dmgd8t94rUJ6KX7/c+fnvkgtX9eOnsj95ST92QjoxAu02W/bDFZrM00wRgVafOynd2F5bJU+PfRqZCkDA7GodADxdpJ7TM/X/+nUXYKJC1t2uh794I3z1Tcr63gtPkCIAMM5cWQKgTk/z7ARPF+0Ht1y1CU+N3QinARCQp1yl5m7X1CPzPFeWegfWSTeWVojE9MqQdGNpduEpaJVc+Fg22zxdhFawQprt1QoAdW5Of/4kAPPeIqxDml38YQAEnpJmmLx5BUDw/efVQ7MQ6MdOBt/9Ik0VYByc6KfO6CcfgFbkKe9Lj9Jk3i6sS5SACZ6WtYa9eludn/eef1Q6kb1aIa0O87VsyjogQhkv+ceHNF3yX3gi97PvSCeiQgZEbqWWvP0xecp7/lH99BlpdOGEyjl3uxb/6R0iAgGKpJuY95aCc3NUyJhLN2S9CX/MBDoQoOfK+/zyBWCGuPg3F+z7y/qZM1TOyUbLXFqwC2uU8eAk+u2/7fVVdeoYFNuFNXPhqtQ7CDw4ARFpZd9fTubKlPGSf30sTkbcvA+Tt6cb7f38aB+LIlAMJvP+knl3sd+EMlHGAxGYpFKPX7sI7sc0BRq9Jq/3bKBdpRa++iYA0kyZkae/p6onbxiATGR62UB8TUm8ny2A8sGdRh879tOBRxmv79W01QPvXkPK8p1/Rxvia0IEJzKRGQqQPHlSfbQueU/KWYoNkn07k23Fd03hzs3NMH1pfd5jKWedc5LRyZMnd175H+Kad1L7JhXRAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDIyLTA5LTAyVDIwOjAxOjE1KzAwOjAw6xGXZgAAACV0RVh0ZGF0ZTptb2RpZnkAMjAyMi0wOS0wMlQyMDowMToxNSswMDowMJpML9oAAAAZdEVYdFNvZnR3YXJlAHd3dy5pbmtzY2FwZS5vcmeb7jwaAAAAAElFTkSuQmCC',
          downloadUrl:
            'https://f000.backblazeb2.com/file/macantacrm-connected-data-attachments/staging/item_a298557499a4d6a7/Test.csv',
          meta: {
            contactId: null,
            referenceFieldId: null,
            __typename: 'ItemAttachmentMeta',
          },
          __typename: 'ItemAttachment',
        },
      ],
    },
    overwrites,
  );
};

export const connectedContactFactory = ({reset, ...overwrites} = {}) => {
  const typename = 'ItemConnectedContacts';

  const {currentIndex} = nextFactoryId(typename, {reset});

  return {
    __typename: typename,
    contactId: `Contact${currentIndex}`,
    email: `test${currentIndex}@macantacrm.com`,
    firstName: `FirstName${currentIndex}`,
    lastName: `LastName${currentIndex}`,
    relationships: [connectedContactRelationshipFactory()],
    ...overwrites,
  };
};

export const connectedContactRelationshipFactory = ({
  reset,
  ...overwrites
} = {}) => {
  const typename = 'ItemConnectedContactRelationship';

  const {currentIndex} = nextFactoryId(typename, {reset});

  return {
    __typename: typename,
    id: `Relationship${currentIndex}`,
    ...overwrites,
  };
};
