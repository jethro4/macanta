import React, {useState, useLayoutEffect} from 'react';
import Box from '@mui/material/Box';
import VisibilityIcon from '@mui/icons-material/Visibility';
import * as Styled from './styles';
import Popover from '@macanta/components/Popover';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import useQuery from '@macanta/hooks/apollo/useQuery';
import {LIST_INTERFACE_ALERTS} from '@macanta/graphql/contacts';

const INTERFACE_ALERTS_MAX_LENGTH = 4;

const InterfaceAlerts = ({contactId}) => {
  const [data, setData] = useState([]);
  const [anchorEl, setAnchorEl] = useState(false);

  const {loading: interfaceAlertsLoading, data: interfaceAlertsData} = useQuery(
    LIST_INTERFACE_ALERTS,
    {
      variables: {
        userId: contactId,
        contactId,
      },
    },
  );

  const interfaceAlerts = interfaceAlertsData?.listInterfaceAlerts?.messages;
  const openShowMore = Boolean(anchorEl);

  const handleShowMore = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleCloseShowMore = () => {
    setAnchorEl(null);
  };

  useLayoutEffect(() => {
    if (interfaceAlerts) {
      setData(interfaceAlerts.slice(0, INTERFACE_ALERTS_MAX_LENGTH));
    }
  }, [interfaceAlerts]);

  return (
    <>
      <Styled.InterfaceAlertsContainer>
        {data.length > 0 && (
          <Styled.InterfaceAlertsBox>
            <Styled.InterfaceAlert
              dangerouslySetInnerHTML={{
                __html: data.join('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'),
              }}
            />
            {interfaceAlerts?.length > INTERFACE_ALERTS_MAX_LENGTH && (
              <Styled.ShowMoreBtn
                style={{
                  marginLeft: '1.1rem',
                }}
                onClick={handleShowMore}
                size="small"
                startIcon={<VisibilityIcon />}>
                More
              </Styled.ShowMoreBtn>
            )}
          </Styled.InterfaceAlertsBox>
        )}
        {interfaceAlertsLoading && (
          <LoadingIndicator fill transparent size={20} />
        )}
      </Styled.InterfaceAlertsContainer>
      <Popover
        open={openShowMore}
        anchorEl={anchorEl}
        onClose={handleCloseShowMore}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}>
        <div
          style={{
            maxWidth: 500,
            minWidth: 250,
            maxHeight: 500,
            overflowY: 'auto',
            backgroundColor: '#f5f6fa',
          }}>
          <Box
            style={{
              padding: '1rem',
            }}>
            {interfaceAlerts?.map((alertMsg, index) => {
              return (
                <Styled.InterfaceAlert key={index}>
                  {alertMsg}
                </Styled.InterfaceAlert>
              );
            })}
          </Box>
        </div>
      </Popover>
    </>
  );
};

export default InterfaceAlerts;
