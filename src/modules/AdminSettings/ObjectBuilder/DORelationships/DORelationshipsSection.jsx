import React from 'react';
import {useFormikContext} from 'formik';
import Section from '@macanta/containers/Section';
import useDORelationships from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDORelationships';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import DORelationshipsList from './DORelationshipsList';

const DORelationshipsSection = (props) => {
  const {values, setValues} = useFormikContext();

  const relationshipsQuery = useDORelationships(values.id, {
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const handleSort = (sortedData) => {
    setValues((state) => ({
      ...state,
      relationships: sortedData,
    }));
  };

  const handleDelete = (id) => {
    setValues((state) => ({
      ...state,
      relationships: state.relationships.filter((d) => d.id !== id),
    }));
  };

  return (
    <Section
      loading={relationshipsQuery.loading}
      title="Selected Relationships"
      bodyStyle={{
        width: '100%',
        height: '100%',
      }}
      {...props}>
      <DORelationshipsList
        loading={relationshipsQuery.loading}
        data={values.relationships}
        onSort={handleSort}
        onDelete={handleDelete}
      />
    </Section>
  );
};

export default DORelationshipsSection;
