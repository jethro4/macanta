export const getTitleAndEmailFromTaskTitle = (taskTitle) => {
  const regExp = /\(([^)]+)\)/;
  const matches = regExp.exec(taskTitle);
  const title = taskTitle.split('<small>')[0].trim();
  const email = matches[1];

  return [title, email];
};

export const getAvailableWidgetId = ({userId, queryId, type}) => {
  return type === 'MacantaTaskSearch'
    ? `${queryId}-${type}`
    : `${userId}-${queryId}-${type}`;
};
