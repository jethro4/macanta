import React from 'react';
import GroupIcon from '@mui/icons-material/Group';
import SettingCard from '@macanta/modules/AdminSettings/SettingCard';
import {navigate} from 'gatsby';

const RelationshipsCard = () => {
  /* const navigate = useNavigate();*/

  const handleCustomTabs = () => {
    navigate('/app/admin-settings/relationships');
  };

  return (
    <SettingCard
      IconComp={GroupIcon}
      title="Contact to Contact Relationships"
      description="Manage Direct and Indirect Contact Relationships"
      onClick={handleCustomTabs}
    />
  );
};

export default RelationshipsCard;
