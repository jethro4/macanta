import Subject from '@macanta/helpers/observers/Subject';
import Observer from '@macanta/helpers/observers/Observer';
import {spyOnConsole} from '@tests/jestSettings/utils/apiUtils';

describe('Observers', () => {
  beforeEach(() => {
    Subject.clearAll();
  });

  describe('Base Observer', () => {
    let subject, observer1, observer2, observer3, observer4;

    beforeEach(() => {
      subject = new Subject('Sample key 1');
      observer1 = new Observer((data) => console.debug(`Observer 1 - ${data}`));
      observer2 = new Observer((data) => console.debug(`Observer 2 - ${data}`));
      observer3 = new Observer((data) => console.debug(`Observer 3 - ${data}`));
      observer4 = new Observer((data) => console.debug(`Observer 4 - ${data}`));
    });

    it('subscribe and notify observers', () => {
      const spy = spyOnConsole();

      subject.subscribe(observer1);
      subject.subscribe(observer2);
      subject.subscribe(observer3);
      subject.subscribe(observer4);

      subject.updateAndNotify('Subject 1');

      expect(spy).toHaveBeenNthCalledWith(1, 'Observer 1 - Subject 1');
      expect(spy).toHaveBeenNthCalledWith(2, 'Observer 2 - Subject 1');
      expect(spy).toHaveBeenNthCalledWith(3, 'Observer 3 - Subject 1');
      expect(spy).toHaveBeenNthCalledWith(4, 'Observer 4 - Subject 1');
    });

    it('unsubscribe and notify remaining observers', () => {
      const spy = spyOnConsole();

      const unsubscribe1 = subject.subscribe(observer1);
      const unsubscribe2 = subject.subscribe(observer2);
      const unsubscribe3 = subject.subscribe(observer3);

      subject.subscribe(observer4);

      unsubscribe1();
      unsubscribe2();
      unsubscribe3();

      subject.updateAndNotify('Subject 1');

      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenNthCalledWith(1, 'Observer 4 - Subject 1');
    });
  });
});
