import {useCallback, useRef} from 'react';
import throttle from 'lodash/throttle';
import isNumber from 'lodash/isNumber';

const DRAGGING_ACTIVE_PX_THRESHOLD = 8;

const useDragHelper = ({onSwitch}) => {
  const draggingRef = useRef(false);
  const baseOffsetRef = useRef(null);
  const largestOffsetDiffRef = useRef(0);

  const preventClickHandler = useCallback((event) => {
    if (draggingRef.current) {
      event.preventDefault();
    }

    draggingRef.current = false;
  }, []);

  const handleDragging = useCallback(
    throttle(
      (layout, oldItem, newItem, p, e) => {
        const newOffset = e.pageX + e.pageY;

        if (!isNumber(baseOffsetRef.current)) {
          baseOffsetRef.current = newOffset;
        } else {
          const offsetDiff = Math.abs(newOffset - baseOffsetRef.current);

          if (offsetDiff > largestOffsetDiffRef.current) {
            largestOffsetDiffRef.current = offsetDiff;

            e.target.addEventListener('click', preventClickHandler, true);
          }
        }

        draggingRef.current = false;
      },
      30,
      {leading: true, trailing: true},
    ),
    [],
  );

  const handleDragStop = (layout, oldItem, newItem, p, e) => {
    const hasSwitched = oldItem.x !== newItem.x || oldItem.y !== newItem.y;
    if (
      largestOffsetDiffRef.current >= DRAGGING_ACTIVE_PX_THRESHOLD ||
      hasSwitched
    ) {
      draggingRef.current = true;
    }

    if (hasSwitched) {
      onSwitch && onSwitch(layout, oldItem, newItem);
    }

    baseOffsetRef.current = null;
    largestOffsetDiffRef.current = 0;

    e.target.removeEventListener('click', preventClickHandler);
  };

  return {
    handleDragging,
    handleDragStop,
  };
};

export default useDragHelper;
