import React, {useState, useMemo} from 'react';
import {useFormikContext} from 'formik';
import range from 'lodash/range';
import AddIcon from '@mui/icons-material/Add';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import Button from '@macanta/components/Button';
import Switch from '@macanta/components/Forms/ChoiceFields/Switch';
import useAllFields from '@macanta/hooks/dataObject/useAllFields';
import useEventResize from '@macanta/hooks/base/useEventResize';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {UPLOAD_FIELD_TYPES} from '@macanta/constants/form';
import {ordinalNumberOf} from '@macanta/utils/string';
import {validateLabelExists, getCategories} from './helpers';
import * as Styled from './styles';

const BuilderFieldsForm = ({
  allowUploadField,
  maxColumn,
  column,
  selectedFields,
  isContactsOnly,
  isDataObjectsOnly,
  doTypeId,
  onAdd,
  onClose,
  shouldAddEmail,
  shouldAddIntegrated,
}) => {
  const {values} = useFormikContext();

  const [item, setItem] = useState({
    column,
    fields: [],
  });
  const [uploadDetails, setUploadDetails] = useState({
    isUpload: false,
    uploadFieldType: 'regular',
    label: '',
    referenceFieldId: null,
  });

  const labelAlreadyUsed = validateLabelExists({
    label: uploadDetails.label,
    groups: [...values.contactGroups, ...values.doGroups],
  });

  const allFieldsQuery = useAllFields({
    groupId: doTypeId,
    isContactObjectType: isContactsOnly,
    isDataObjectType: isDataObjectsOnly,
    includeAllTypes: true,
    options: {
      fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
    },
  });

  const allFields = useMemo(() => {
    let requiredFieldsArr = [];
    let allFieldsArr = allFieldsQuery.data ? [...allFieldsQuery.data] : [];

    if (shouldAddEmail) {
      requiredFieldsArr = requiredFieldsArr.concat({
        id: 'field_email',
        label: 'Email',
        value: 'Email',
        required: true,
        type: 'Email',
      });
      allFieldsArr = allFieldsArr.filter(
        (field) => !['field_email'].includes(field.id),
      );
    }
    if (shouldAddIntegrated) {
      requiredFieldsArr = requiredFieldsArr.concat({
        id: 'field_integration',
        label: 'Integration Field',
        value: 'Integration Field',
        required: true,
        type: 'Select',
        choices: ['Sample 1', 'Sample 2'],
      });
    }

    if (requiredFieldsArr.length) {
      requiredFieldsArr = [
        {
          isSubheader: true,
          label: 'Required Fields',
        },
      ].concat(requiredFieldsArr);
    }

    return requiredFieldsArr.concat(allFieldsArr);
  }, [allFieldsQuery.data]);

  const handleChange = (key) => (value) => {
    setItem((state) => ({...state, [key]: value}));
  };

  const handleChangeUpload = (key) => (value) => {
    setUploadDetails((state) => ({...state, [key]: value}));
  };

  const handleAdd = () => {
    const addedFields = allFields.filter((field) =>
      item.fields.includes(field.value),
    );

    if (!uploadDetails.isUpload) {
      onAdd(
        addedFields.map((field) => ({
          ...field,
          fieldId: field.id,
          column: parseInt(item.column),
        })),
      );
    } else {
      const newId = uploadDetails.referenceFieldId || String(Date.now());

      onAdd({
        column: parseInt(item.column),
        label: uploadDetails.label,
        value: newId,
        id: newId,
        fieldId: newId,
        type: 'file',
        referenceFieldId: uploadDetails.referenceFieldId,
        multiple: false,
        mimeTypes: getCategories('all'),
        required: false,
      });
    }
    onClose();
  };

  useEventResize([
    uploadDetails.isUpload,
    uploadDetails.uploadFieldType,
    labelAlreadyUsed,
  ]);

  const ordinalPositions = useMemo(() => {
    return range(1, maxColumn + 1).map((pos) => ({
      label: ordinalNumberOf(pos),
      value: String(pos),
    }));
  }, []);

  const filteredFields = useMemo(() => {
    if (allFields) {
      let allFieldsArr = [...allFields];
      if (uploadDetails.isUpload) {
        allFieldsArr = allFields.filter((field) => field.type === 'FileUpload');
      }

      return allFieldsArr.filter(
        (field) =>
          !selectedFields.some(
            (selectedField) => selectedField.id === field.id,
          ),
      );
    }

    return [];
  }, [allFields, uploadDetails.isUpload]);

  return (
    <>
      <Box
        style={{
          padding: !allowUploadField ? '1rem' : '0.5rem 1rem 1rem',
          display: 'flex',
          flexDirection: 'column',
          overflowY: 'auto',
        }}>
        {allowUploadField && (
          <>
            <Box
              style={{
                display: 'flex',
                alignItems: 'center',
              }}>
              <Switch
                style={{
                  marginRight: 12,
                }}
                checked={uploadDetails.isUpload}
                onChange={(event) =>
                  handleChangeUpload('isUpload')(event.target.checked)
                }
              />
              <Typography
                sx={{
                  color: 'grayDarker.main',
                }}
                style={{
                  fontSize: 14,
                }}>
                Upload Field
              </Typography>
            </Box>
            <Divider
              style={{
                marginTop: 2,
                marginBottom: 20,
                borderBottom: '1px solid #f2f2f2',
              }}
            />
          </>
        )}
        <Styled.SettingsField
          labelPosition="normal"
          type="Select"
          size="small"
          options={ordinalPositions}
          value={String(item.column)}
          onChange={handleChange('column')}
          label="Column"
          variant="outlined"
        />
        {!uploadDetails.isUpload ? (
          <Styled.SettingsField
            labelPosition="normal"
            type="MultiSelect"
            size="small"
            options={filteredFields}
            value={item.fields}
            onChange={handleChange('fields')}
            label="Fields"
            fullWidth
            variant="outlined"
          />
        ) : (
          <>
            <Styled.SettingsField
              row
              style={{
                width: '100%',
              }}
              type="Radio"
              options={UPLOAD_FIELD_TYPES}
              labelPosition="normal"
              size="small"
              value={uploadDetails.uploadFieldType}
              onChange={handleChangeUpload('uploadFieldType')}
              variant="outlined"
            />
            <Styled.SettingsField
              required
              autoFocus
              style={{
                width: '100%',
              }}
              labelPosition="normal"
              size="small"
              value={uploadDetails.label}
              error={labelAlreadyUsed}
              onChange={handleChangeUpload('label')}
              label="Label"
              variant="outlined"
            />
            {uploadDetails.uploadFieldType === 'fieldSpecific' && (
              <Styled.SettingsField
                required
                type="Select"
                options={filteredFields}
                valueKey="id"
                labelPosition="normal"
                size="small"
                value={uploadDetails.referenceFieldId}
                onChange={handleChangeUpload('referenceFieldId')}
                label="Reference Field"
                variant="outlined"
              />
            )}
          </>
        )}
      </Box>
      <Box
        style={{
          display: 'flex',
          justifyContent: 'flex-end',
          padding: '12px 1rem',
          borderTop: '1px solid #eee',
        }}>
        <Button
          disabled={
            !uploadDetails.isUpload
              ? !item.fields.length
              : labelAlreadyUsed ||
                (uploadDetails.uploadFieldType === 'fieldSpecific'
                  ? !uploadDetails.label || !uploadDetails.referenceFieldId
                  : !uploadDetails.label)
          }
          variant="contained"
          startIcon={<AddIcon />}
          onClick={handleAdd}
          size="small">
          Add Field
        </Button>
      </Box>
    </>
  );
};

BuilderFieldsForm.defaultProps = {
  maxColumn: 3,
  column: 1,
  selectedFields: [],
};

export default BuilderFieldsForm;
