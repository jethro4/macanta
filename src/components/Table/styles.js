import {experimentalStyled as styled} from '@mui/material/styles';

import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import MUITable from '@mui/material/Table';
import MUITableContainer from '@mui/material/TableContainer';
import MUITableRow from '@mui/material/TableRow';
import MUITableHead from '@mui/material/TableHead';
import MUITableCell from '@mui/material/TableCell';
import MUITableSortLabel from '@mui/material/TableSortLabel';
import {OverflowTip} from '@macanta/components/Tooltip';
import SortIconComp from '@macanta/themes/icons/Sort';
import * as applicationStyles from '@macanta/themes/applicationStyles';

export const EmptyMessageContainer = styled(Box)`
  position: absolute;
  top: ${applicationStyles.DEFAULT_HEAD_ROW_HEIGHT}px;
  left: 0;
  right: 0;
  bottom: 0;
  pointer-events: none;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const EmptyMessage = styled(Typography)``;

export const Root = styled(Box)`
  background-color: ${({theme}) => theme.palette.grayLighter.main};
  display: flex;
  flex-direction: column;
  position: relative;
  border-bottom-left-radius: 4px;
  border-bottom-right-radius: 4px;
`;

export const RootContainer = styled(Box)`
  flex: 1;
  position: relative;
`;

export const TableContainer = styled(MUITableContainer)``;

export const Table = styled(MUITable)`
  transform: translate3d(0, 0, 0);

  thead th {
    left: unset;
  }
`;

export const TableHead = styled(MUITableHead)`
  & > * {
    user-select: none;
  }
`;

export const TableHeadRow = styled(MUITableRow)``;

export const TableBodyRow = styled(MUITableRow)`
  ${({disabled, selectable}) =>
    !disabled
      ? selectable && 'cursor: pointer;'
      : `
      cursor: not-allowed; 
      background-color: #bbb !important;

      .MuiTypography-root {
        color: white;
      }
    `}

  & .actionButtonCol .MuiButtonBase-root {
    opacity: ${({show}) => (!show ? '0' : '1')};
  }

  &:hover {
    ${({theme, highlight}) =>
      highlight && `background-color: ${theme.palette.gray.main} !important;`}

    .actionButtonCol .MuiButtonBase-root {
      opacity: 1;
    }
  }
`;

export const BodySubheaderRow = styled(MUITableRow)`
  background-color: ${({theme}) => theme.palette.common.white};

  ${({theme, nested}) =>
    !nested
      ? `
        .MuiTableCell-root {
          border-bottom: 0;

          .MuiTypography-root {
            color: ${theme.palette.primary.main};
            font-size: 1rem;
            font-weight: bold;
          }
        }
      `
      : `
      .MuiTableCell-root {
        border-bottom: 0;
  
        .MuiTypography-root {
          color: ${theme.palette.grayDarkest.main};
          font-weight: bold;
        }
      }
  `}
`;

export const TableHeadCell = styled(MUITableCell)`
  padding: 0 1rem;
  vertical-align: middle;
  background-color: ${({theme}) => theme.palette.table.headRow};
`;

export const TableBodyCell = styled(MUITableCell)`
  padding: 0 1rem;
  vertical-align: middle;
  position: relative;

  ${({align}) =>
    align === 'center' &&
    `
    .cellbody-container {
      left: 0;
    }
  `}
`;

export const TableActionBodyCell = styled(TableBodyCell)`
  vertical-align: middle;
`;

export const TableCellText = styled(Typography)`
  font-size: 14px;
`;

export const TableHeadCellText = styled(OverflowTip)``;

export const TableHeadCellLabelContainer = styled(Box)`
  padding: 0 1rem;
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  height: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const TableBodyCellLabelContainer = styled(Box)`
  position: absolute;
  top: 0;
  right: ${({theme}) => theme.spacing(2)};
  bottom: 0;
  left: ${({theme}) => theme.spacing(2)};
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

export const TableBodyCellLabel = styled(OverflowTip)``;

export const Subtext = styled(OverflowTip)`
  font-size: 12px;
  color: #aaa;
`;

export const TableSortLabel = styled(MUITableSortLabel)`
  width: 100%;
  justify-content: flex-start;
`;

export const TableFooter = styled(Box)`
  ${applicationStyles.footer}
`;

export const SortIcon = styled(SortIconComp)`
  opacity: 0.1;
  font-size: 12px;
  margin-left: 0.5rem;
  margin-right: 0;
  fill: currentColor;

  ${({active}) => active && `fill: unset; color: #2196f3!important;`}
`;

export const ColumnResizerContainer = styled(Box)`
  cursor: col-resize;
  padding: 0 0.5rem;
  height: 100%;
  position: absolute;
  right: 0;
  top: 0;
  z-index: 1;
  ${'' /* prevents from scrolling while dragging on touch devices */}
  touch-action:none;
  transform: translateX(50%);
  display: flex;
  align-items: center;

  &:hover > * {
    background-color: ${({theme}) => theme.palette.primary.main};
    width: 2px;
  }

  ${({theme, disabled}) =>
    disabled &&
    `
      cursor: not-allowed;

      &:hover > * {
        background-color: ${theme.palette.border.main};
      }
  `}
`;

export const ColumnResizerChild = styled(Box)`
  background-color: ${({theme}) => theme.palette.border.main};
  width: 1px;
  height: 28px;
`;
