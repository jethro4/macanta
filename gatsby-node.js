const SentryWebpackPlugin = require('@sentry/webpack-plugin');
var pjson = require('./package.json');

const pathsToIgnore = ['/develop/'];

exports.onCreatePage = async ({page, actions}) => {
  const {createPage} = actions;

  // page.matchPath is a special key that's used for matching pages
  // only on the client.
  console.info('onCreatePage: matches pathname', page.path);

  if (page.path.match(/^\/app/)) {
    page.matchPath = `/app/*`;

    // Update the page.
    createPage(page);
  }

  if (
    process.env.NODE_ENV === 'production' &&
    pathsToIgnore.includes(page.path)
  ) {
    console.info('Delete page', page.path);
    actions.deletePage(page);
  }
};

exports.onCreateWebpackConfig = ({stage, loaders, actions}) => {
  console.info('Stage: ' + stage);

  if (stage === 'develop') {
    actions.setWebpackConfig({
      resolve: {
        fallback: {
          url: false,
        },
      },
    });
  } else if (stage === 'build-javascript') {
    console.info('....WEBPACK CONFIG....');
    console.info('NODE_ENV: ' + process.env.NODE_ENV);
    console.info('STAGING: ' + !!process.env.STAGING);
    console.info('Version: ' + pjson.version);
    console.info('Build Number: ' + pjson.buildNumber);

    actions.setWebpackConfig({
      devtool: 'source-map',
      resolve: {
        fallback: {
          url: false,
        },
      },
      ...(!process.env.STAGING &&
        process.env.NODE_ENV === 'production' && {
          plugins: [
            new SentryWebpackPlugin({
              // sentry-cli configuration - can also be done directly through sentry-cli
              // see https://docs.sentry.io/product/cli/configuration/ for details
              authToken: process.env.SENTRY_AUTH_TOKEN,
              org: 'macanta',
              project: 'macanta-react',
              release: process.env.SENTRY_RELEASE,
              dist: pjson.buildNumber,
              cleanArtifacts: true,

              // other SentryWebpackPlugin configuration
              include: './public',
              ignore: ['node_modules', 'webpack.config.js'],
            }),
          ],
        }),
    });
  } else if (stage === 'build-html') {
    actions.setWebpackConfig({
      module: {
        rules: [
          {
            test: /pdfjs-dist/,
            use: loaders.null(),
          },
          {
            test: /react-pdf/,
            use: loaders.null(),
          },
        ],
      },
    });
  }
};
