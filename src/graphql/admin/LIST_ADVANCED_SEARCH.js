import {gql} from '@apollo/client';

export default gql`
  query listAdvancedSearch(
    $shouldSave: Boolean
    $queryId: ID
    $name: String
    $description: String
    $page: Int
    $limit: Int
    $order: String
    $orderBy: String
    $doType: String
    $chosenFields: String
    $doConditions: String
    $contactConditions: String
    $userConditions: String
  ) {
    listAdvancedSearch(
      shouldSave: $shouldSave
      queryId: $queryId
      name: $name
      description: $description
      page: $page
      limit: $limit
      order: $order
      orderBy: $orderBy
      doType: $doType
      chosenFields: $chosenFields
      doConditions: $doConditions
      contactConditions: $contactConditions
      userConditions: $userConditions
    ) {
      queryId
      results {
        items {
          data {
            id
            isContactField
            fieldKey
            fieldType
            value
            choices
          }
        }
        fields
        total
      }
    }
  }
`;
