import React from 'react';
import AddIcon from '@material-ui/icons/Add';
import VisibilityIcon from '@mui/icons-material/Visibility';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import Box from '@material-ui/core/Box';
import {ModalButton, DialogButton} from '@macanta/components/Button';
import Section from '@macanta/containers/Section';
import DataTable from '@macanta/containers/DataTable';
import TriggerActionForm from './TriggerActionForm';
import {
  useDeleteAutomationsMutation,
  usePostTriggerActionMutation,
  useTriggerActions,
} from '@macanta/hooks/automation';
import {findItem} from '@macanta/utils/array';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import {writeItemInListFragment} from '@macanta/graphql/helpers';
import {TRIGGER_ACTIONS_FRAGMENT} from '@macanta/graphql/automation/GET_TRIGGER_ACTIONS';
import {ACTION_FIELDS_MAPPING} from '@macanta/constants/automation';
import {capitalizeFirstLetter} from '@macanta/utils/string';

const TRIGGER_ACTIONS_COLUMNS = [
  {
    id: 'name',
    label: 'Name',
  },
  {
    id: 'description',
    label: 'Description',
  },
  {id: 'doTitle', label: 'Data Object Type', minWidth: 170},
  // {
  //   id: 'contactConditions',
  //   label: 'Contact Relationships',
  //   emptyMessage: 'N/A',
  //   transform: ({value}) =>
  //     removeEmptyValues(map(value, 'relationship')).join(', '),
  // },
  {
    id: 'actions',
    label: 'Actions',
    maxWidth: 150,
    transform: ({value}) =>
      Object.keys(ACTION_FIELDS_MAPPING)
        .reduce((acc, key) => {
          const accArr = [...acc];

          const actionItem = findItem(
            value,
            (item) => item.type === key && !!item.value,
          );

          if (actionItem) {
            return accArr.concat(
              key === 'sms' ? 'SMS' : capitalizeFirstLetter(key),
            );
          }

          return accArr;
        }, [])
        .join(', '),
  },
];

const MODAL_WIDTH = 1250;
const MODAL_MIN_HEIGHT = 750;
const MODAL_MAX_HEIGHT = '92%';

const getKeyExtractor = (item, index) => item?.queryId || String(index);

const TriggerActionsSection = (props) => {
  const triggerActionsQuery = useTriggerActions();

  const [
    callPostTriggerActionMutation,
    postTriggerActionMutation,
  ] = usePostTriggerActionMutation({
    update(cache, {result}, options) {
      writeItemInListFragment({
        isCreate: !options.variables.input?.id,
        keyField: 'queryId',
        item: result,
        typeName: 'TriggerActionItems',
        fragment: TRIGGER_ACTIONS_FRAGMENT,
        parentKey: 'getTriggerActions',
        listKey: 'items',
        position: 'first',
      });
    },
  });

  const [
    callDeleteItemMutation,
    deleteItemMutation,
  ] = useDeleteAutomationsMutation({
    typeName: 'TriggerActionItems',
  });

  const handleSave = (values) => {
    const variables = {
      id: values.id,
      doType: values.doTitle,
      emailActionId: findItem(values.actions, {name: 'emailActionId'}).value,
      smsActionId: findItem(values.actions, {name: 'smsActionId'}).value,
      fieldActionId: findItem(values.actions, {name: 'fieldActionId'}).value,
      contactActionId: findItem(values.actions, {name: 'contactActionId'})
        .value,
      userActionId: findItem(values.actions, {name: 'userActionId'}).value,
      webhookActionId: findItem(values.actions, {name: 'webhookActionId'})
        .value,
      name: values.name,
      description: values.description,
      doConditions: values.doConditions.map((item) => ({
        fieldName: item.name,
        value: item.value,
        values: item.values,
      })),
      contactConditions: values.contactConditions.map((item) => ({
        relationship: item.relationship,
        logic: item.logic || 'and',
      })),
    };

    return callPostTriggerActionMutation({
      variables,
    });
  };

  const handleDelete = (id) => {
    return callDeleteItemMutation({
      variables: {
        id,
      },
    });
  };

  return (
    <>
      <Section
        fullHeight
        title="Trigger Actions"
        HeaderRightComp={
          <ModalButton
            size="small"
            variant="contained"
            startIcon={<AddIcon />}
            ModalProps={{
              headerTitle: 'Add Trigger Action',
              contentWidth: MODAL_WIDTH,
              contentMinHeight: MODAL_MIN_HEIGHT,
              contentMaxHeight: MODAL_MAX_HEIGHT,
              bodyStyle: {
                backgroundColor: '#f5f6fa',
              },
            }}
            renderContent={(handleClose) => {
              return (
                <TriggerActionForm
                  onSubmit={async (values) => {
                    const {result} = await handleSave(values);

                    if (result) {
                      handleClose();
                    }
                  }}
                />
              );
            }}>
            Add Trigger Action
          </ModalButton>
        }
        {...props}>
        <DataTable
          keyExtractor={getKeyExtractor}
          fullHeight
          loading={triggerActionsQuery.loading}
          columns={TRIGGER_ACTIONS_COLUMNS}
          data={triggerActionsQuery.result}
          actionColumn={{
            show: true,
            style: {
              minWidth: 100,
            },
          }}
          renderActionButtons={({item}) => {
            return (
              <Box
                style={{
                  marginRight: -10,
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <ModalButton
                  icon
                  sx={{
                    padding: '0.25rem',
                    color: 'info.main',
                  }}
                  size="small"
                  TooltipProps={{
                    title: 'Show Details',
                  }}
                  ModalProps={{
                    headerTitle: item.id,
                    contentWidth: MODAL_WIDTH,
                    contentMinHeight: MODAL_MIN_HEIGHT,
                    contentMaxHeight: MODAL_MAX_HEIGHT,
                    bodyStyle: {
                      backgroundColor: '#f5f6fa',
                    },
                  }}
                  renderContent={(handleClose) => {
                    return (
                      <TriggerActionForm
                        initialValues={item}
                        onSubmit={async (values) => {
                          const {result} = await handleSave(values);

                          if (result) {
                            handleClose();
                          }
                        }}
                      />
                    );
                  }}>
                  <VisibilityIcon />
                </ModalButton>
                <DialogButton
                  icon
                  size="small"
                  TooltipProps={{
                    title: 'Delete',
                  }}
                  DialogProps={{
                    title: 'Are you sure?',
                    description: [
                      `You are about to delete "${item.name}" from Trigger Actions.`,
                      'This cannot be undone. Do you want to proceed?',
                    ],
                    onActions: (handleClose) => {
                      return [
                        {
                          label: 'Delete',
                          onClick: async () => {
                            await handleDelete(item.id);

                            handleClose();
                          },
                          startIcon: <DeleteForeverIcon />,
                        },
                      ];
                    },
                  }}>
                  <DeleteForeverIcon
                    sx={{
                      color: 'grayDarker.main',
                    }}
                  />
                </DialogButton>
              </Box>
            );
          }}
        />
      </Section>
      <LoadingIndicator
        modal
        loading={
          postTriggerActionMutation.loading || deleteItemMutation.loading
        }
      />
    </>
  );
};

export default TriggerActionsSection;
