import {gql} from '@apollo/client';

export default gql`
  mutation deleteCustomTab($deleteCustomTabInput: DeleteCustomTabInput!) {
    deleteCustomTab(input: $deleteCustomTabInput) {
      success
    }
  }
`;
