import React, {useRef} from 'react';

import Typography from '@mui/material/Typography';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import SendIcon from '@mui/icons-material/Send';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import TextField from '@macanta/components/Forms';
import Form from '@macanta/components/Form';
import {useMutation} from '@apollo/client';
import {FORGOT_PASSWORD} from '@macanta/graphql/auth';
import {forgotPasswordValidationSchema} from '@macanta/validations/forgotPassword';
import FormHeader from './FormHeader';
import * as Styled from './styles';

const FormsForgotPassword = ({onLogin, onVerificationCode}) => {
  const emailRef = useRef({});

  const [callForgotPwStep1, forgotPwStep1Mutation] = useMutation(
    FORGOT_PASSWORD,
    {
      onCompleted(data) {
        if (data.forgotPassword?.success) {
          onVerificationCode(emailRef.current);
        }
      },
      onError() {},
    },
  );

  const handleFormSubmit = (values) => {
    callForgotPwStep1({
      variables: {
        forgotPasswordInput: {
          step: 1,
          email: values.email,
        },
        __mutationkey: 'forgotPasswordInput',
      },
    });

    emailRef.current = {email: values.email};
  };

  return (
    <>
      <FormHeader
        title="Forgot Password"
        description="Please let us help you retrieve your account."
      />

      <Form
        initialValues={{email: ''}}
        validationSchema={forgotPasswordValidationSchema}
        onSubmit={handleFormSubmit}>
        {({
          values,
          errors,
          handleChange,
          handleSubmit,
          /* and other goodies */
        }) => (
          <>
            <TextField
              autoFocus
              error={errors.email}
              autoComplete="email"
              onChange={handleChange('email')}
              label="Email"
              variant="filled"
              value={values.email}
              fullWidth
              onEnterPress={handleSubmit}
            />

            {!!forgotPwStep1Mutation.error && (
              <Typography
                color="error"
                variant="subtitle2"
                align="center"
                dangerouslySetInnerHTML={{
                  __html: forgotPwStep1Mutation.error.message,
                }}
              />
            )}

            <Styled.GridItem container>
              <Styled.GridItem item xs={12} sm={8}>
                <Styled.ButtonLeft
                  onClick={onLogin}
                  startIcon={
                    <ChevronLeftIcon
                      style={{
                        fontSize: '1.5rem',
                      }}
                    />
                  }>
                  Back to login
                </Styled.ButtonLeft>
              </Styled.GridItem>
              <Styled.GridItem item xs={12} sm={4}>
                <Styled.ButtonRight
                  onClick={handleSubmit}
                  color="primary"
                  variant="contained"
                  startIcon={<SendIcon />}>
                  Proceed
                </Styled.ButtonRight>
              </Styled.GridItem>
            </Styled.GridItem>
          </>
        )}
      </Form>
      <LoadingIndicator fill loading={forgotPwStep1Mutation.loading} />
    </>
  );
};

export default FormsForgotPassword;
