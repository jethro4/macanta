import React, {useEffect, useState} from 'react';
import isNil from 'lodash/isNil';
import Box from '@mui/material/Box';
import ButtonGroup from '@macanta/components/ButtonGroup';
import Button from '@macanta/components/Button';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import {SubSection} from '@macanta/containers/Section';
import FormField from '@macanta/containers/FormField';
import useContactId from '@macanta/hooks/useContactId';
import useDOSectionConditions from '@macanta/hooks/useDOSectionConditions';
import usePermissionsWhitelistedDOSections from '@macanta/hooks/usePermissionsWhitelistedDOSections';
import useIsAdmin from '@macanta/hooks/useIsAdmin';
import {ACCESS_LEVELS} from '@macanta/constants/accessLevels';
import {useFormikContext} from 'formik';
import ContactSpecificFields from './ContactSpecificFields';
import DOSectionGuideBtn from './DOSectionGuideBtn';
import {getSectionsData} from '@macanta/selectors/field.selector';
import * as Styled from './styles';
import * as Storage from '@macanta/utils/storage';

const FieldForms = ({
  isEdit,
  groupId,
  fields,
  data,
  loading,
  hasConnectedUser,
  hasOtherUserDOPermission,
}) => {
  const {userId: loggedInUserId} = Storage.getItem('userDetails');

  const contactId = useContactId();
  const isAdmin = useIsAdmin();
  const {values, errors, setValues} = useFormikContext();

  const [sectionsData, setSectionsData] = useState([]);
  const [sectionNames, setSectionNames] = useState([]);
  const [filteredSectionsData, setFilteredSectionsData] = useState(null);

  const {additionalSections, blacklistedSections} = useDOSectionConditions({
    groupId,
    details: data,
  });
  const whitelistedDOSections = usePermissionsWhitelistedDOSections(
    groupId,
    additionalSections,
    blacklistedSections,
  );

  const isLoggedInUser = loggedInUserId === contactId;

  const handleSelectSectionData = (sectionName) => () => {
    const filteredData = sectionsData.find(
      (section) => section.sectionName === sectionName,
    );

    setFilteredSectionsData(filteredData);
  };

  const handleChange = (fieldName) => (value) => {
    setValues((state) => ({
      ...state,
      details: {
        ...state['details'],
        [fieldName]: value,
      },
    }));
  };

  useEffect(() => {
    if (fields) {
      const allSections = getSectionsData(
        fields,
        whitelistedDOSections,
        blacklistedSections,
      );
      const allSectionNames = allSections.map((section) => section.sectionName);

      setSectionsData(allSections);
      setSectionNames(allSectionNames);
    }
  }, [fields, whitelistedDOSections, blacklistedSections]);

  useEffect(() => {
    if (!filteredSectionsData && sectionNames.length) {
      handleSelectSectionData(sectionNames[0])();
    }
  }, [filteredSectionsData, sectionNames]);

  if (!filteredSectionsData) {
    return null;
  }

  return (
    <Styled.Root>
      <Styled.SectionsBtnContainer>
        <Styled.BodyHeader>
          <ButtonGroup
            sx={{
              '& .MuiButton-root': {
                position: 'relative',

                '&:after': {
                  content: '""',
                  position: 'absolute',
                  width: '0.5px',
                  right: '-0.5px',
                  top: 0,
                  bottom: 0,
                  backgroundColor: 'primary.main',
                  borderRadius: '4px',
                },
              },
            }}
            style={{
              flexWrap: 'wrap',
            }}
            size="small"
            variant="outlined"
            aria-label="primary button group">
            {sectionNames.map((sectionName, index) => {
              return (
                <Button
                  key={index}
                  onClick={handleSelectSectionData(sectionName)}
                  endIcon={<DOSectionGuideBtn sectionName={sectionName} />}>
                  {sectionName}
                </Button>
              );
            })}
          </ButtonGroup>
        </Styled.BodyHeader>
      </Styled.SectionsBtnContainer>
      <Box
        style={{
          display: 'flex',
          paddingBottom: '1rem',
          flexWrap: 'wrap',
        }}>
        {filteredSectionsData &&
          filteredSectionsData.subGroups.map((subGroup) => {
            const subGroupName =
              subGroup.subGroupName !== 'Overview' ? subGroup.subGroupName : '';

            return (
              <SubSection
                key={subGroupName}
                style={{
                  width: 350,
                  minWidth: 350,
                  alignSelf: 'flex-start',
                }}
                bodyStyle={{
                  padding: '1rem 0',
                }}
                title={subGroupName}>
                {fields.length > 0 &&
                  subGroup.data.map((field) => {
                    const fieldDetails = data[field.name];
                    const value = !isNil(values.details[field.name])
                      ? values.details[field.name]
                      : fieldDetails?.value;
                    const isReadOnly =
                      isEdit &&
                      !isAdmin &&
                      !isLoggedInUser &&
                      (loading ||
                        (!hasConnectedUser &&
                          field.permission === ACCESS_LEVELS.READ_ONLY) ||
                        (hasConnectedUser && !hasOtherUserDOPermission));

                    return (
                      <FormField
                        key={field.id}
                        style={{
                          padding: '0 1rem',
                        }}
                        type={field.type}
                        required={field.required}
                        options={field.choices}
                        error={errors[field.name]}
                        onChange={handleChange(field.name)}
                        label={field.name}
                        placeholder={field.placeholder}
                        headingText={field.headingText}
                        helperText={field.helpText}
                        addDivider={field.addDivider}
                        value={value}
                        readOnly={isReadOnly}
                        {...(field.type === 'FileUpload' && {
                          readOnly: true, //TODO: Temporarily set as static true until FileUpload type works
                          attachments: fieldDetails.attachments,
                        })}
                        fullWidth
                        size="small"
                        variant="outlined"
                        TopRightComp={
                          field.contactSpecificField && (
                            <ContactSpecificFields
                              values={fieldDetails.contactSpecificValues}
                            />
                          )
                        }
                      />
                    );
                  })}
              </SubSection>
            );
          })}
      </Box>

      <LoadingIndicator fill align="top" loading={loading} />
    </Styled.Root>
  );
};

FieldForms.defaultProps = {
  loading: false,
  fields: [],
  data: {},
};

export default FieldForms;
