import React from 'react';
import PermissionsTable from '@macanta/modules/AdminSettings/UserManagement/Tabs/PermissionsTable';
import useGeneralUserGuides from '@macanta/hooks/app/useGeneralUserGuides';
import UserGuidesColumns from './UserGuidesColumns';
import {getKeyExtractor} from './helpers';

const GeneralInterfaceGuideForm = ({permission, onChange}) => {
  const {data} = useGeneralUserGuides({permission});

  return (
    <PermissionsTable
      keyExtractor={getKeyExtractor}
      columns={UserGuidesColumns}
      data={data}
      handlers={{
        onChange,
      }}
    />
  );
};

export default GeneralInterfaceGuideForm;
