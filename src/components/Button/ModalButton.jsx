import React, {useState} from 'react';
import ButtonIndex from './index';
import Modal from '@macanta/components/Modal';

const ModalButton = ({ModalProps, renderButton, renderContent, ...props}) => {
  const {onOpen, onClose, ...otherModalProps} = ModalProps;

  const [open, setOpen] = useState(false);

  const handleOpen = (...args) => {
    setOpen(true);

    onOpen && onOpen(...args);
  };

  const handleClose = (...args) => {
    setOpen(false);

    onClose && onClose(...args);
  };

  return (
    <>
      {renderButton ? (
        renderButton(handleOpen)
      ) : (
        <ButtonIndex onClick={handleOpen} {...props} />
      )}

      <Modal open={open} onClose={handleClose} {...otherModalProps}>
        {open && renderContent && renderContent(handleClose)}
      </Modal>
    </>
  );
};

export default ModalButton;
