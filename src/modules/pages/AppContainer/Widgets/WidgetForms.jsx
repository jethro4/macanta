import React, {useState, useLayoutEffect} from 'react';
import widgetValidationSchema from '@macanta/validations/widget';
import TurnedInIcon from '@mui/icons-material/TurnedIn';
import Form from '@macanta/components/Form';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import {useMutation} from '@apollo/client';
import {CREATE_OR_UPDATE_WIDGET} from '@macanta/graphql/admin';
import WidgetTitleField from './WidgetTitleField';
import {getTitleAndEmailFromTaskTitle} from './helpers';
import * as Styled from '@macanta/modules/NoteTaskForms/styles';
import * as Storage from '@macanta/utils/storage';

const WidgetForms = ({
  taskWidgets,
  queryWidgets,
  isEdit,
  widget: widgetProp,
  onSuccess,
  ...props
}) => {
  const {userId: loggedInUserId} = Storage.getItem('userDetails');

  const [allWidgets, setAllWidgets] = useState([]);

  const initValues = isEdit
    ? {
        widgetId: widgetProp.id,
        title: widgetProp.title,
        ...widgetProp,
        label: widgetProp.title,
      }
    : {};

  const [callWidgetMutation, widgetMutation] = useMutation(
    CREATE_OR_UPDATE_WIDGET,
    {
      onCompleted(data) {
        if (data.createOrUpdateWidget) {
          onSuccess(data.createOrUpdateWidget);
        }
      },
      onError() {},
    },
  );

  const handleSuccess = (values) => {
    const newWidget = {
      userId: loggedInUserId,
      queryId: values.queryId || values.userId,
      type: values.type,
      title: values.title,
      access: values.access,
      isEdit: !!isEdit,
    };

    callWidgetMutation({
      variables: {
        createOrUpdateWidgetInput: newWidget,
        __mutationkey: 'createOrUpdateWidgetInput',
      },
    });
  };

  const getSelectedWidget = (widgetId) => {
    if (isEdit) {
      return initValues;
    }

    const selectedWidget = allWidgets.find((widget) => widget.id === widgetId);

    return selectedWidget;
  };

  useLayoutEffect(() => {
    const allUpdatedWidgets = [
      ...(taskWidgets.length
        ? [
            {
              isSubheader: true,
              label: 'Tasks',
            },
          ]
        : []),
      ...taskWidgets.map((w) => {
        const [title, email] = getTitleAndEmailFromTaskTitle(w.title);

        return {
          ...w,
          label: `${title} (${email})`,
          value: w.id,
        };
      }),
      ...(taskWidgets.length
        ? [
            {
              isSubheader: true,
              label: 'Queries',
            },
          ]
        : []),
      ...queryWidgets.map((w) => {
        return {
          ...w,
          label: w.title,
          value: w.id,
        };
      }),
    ];
    setAllWidgets(allUpdatedWidgets);
  }, [taskWidgets, queryWidgets]);

  return (
    <Styled.Root {...props}>
      <Styled.Container>
        <Form
          initialValues={initValues}
          validationSchema={widgetValidationSchema}
          onSubmit={handleSuccess}>
          {({values, errors, handleChange, setValues, handleSubmit}) => (
            <>
              <Styled.FormGroup>
                {!isEdit && (
                  <Styled.TextField
                    select
                    // placeholder="Select Widget..."
                    label="Widget"
                    options={allWidgets}
                    error={errors.widgetId}
                    onChange={(val) => {
                      const selectedWidget = getSelectedWidget(val);

                      setValues((vals) => ({
                        ...vals,
                        ...selectedWidget,
                        widgetId: selectedWidget.id,
                      }));
                    }}
                    variant="outlined"
                    value={values.widgetId}
                    fullWidth
                    size="medium"
                  />
                )}

                <WidgetTitleField
                  getSelectedWidget={getSelectedWidget}
                  widgetId={values.widgetId}
                  error={errors.title}
                  onChange={handleChange('title')}
                  label="Title"
                  variant="outlined"
                  value={values.title}
                  fullWidth
                  size="medium"
                />
              </Styled.FormGroup>

              {/* {!!error && (
                <Typography color="error" variant="subtitle2" align="center">
                  {error.message}
                </Typography>
              )} */}

              <Styled.Footer>
                <Styled.FooterButton
                  disabled={!values.widgetId}
                  variant="contained"
                  startIcon={<TurnedInIcon />}
                  onClick={handleSubmit}
                  size="medium">
                  Save
                </Styled.FooterButton>
              </Styled.Footer>
            </>
          )}
        </Form>
      </Styled.Container>
      <LoadingIndicator modal loading={widgetMutation.loading} />
    </Styled.Root>
  );
};

WidgetForms.defaultProps = {
  taskWidgets: [],
  queryWidgets: [],
};

export default WidgetForms;
