/* eslint-disable react/jsx-filename-extension */
import React from 'react';
import {render} from '@testing-library/react';
import isArray from 'lodash/isArray';
import isFunction from 'lodash/isFunction';
// import {MockedProvider} from '@apollo/client/testing';
// import ApolloProviderContainer from '@macanta/containers/ApolloProviderWrapper/ApolloProviderContainer';

import {ApolloProvider} from '@apollo/client';
import {client} from '@macanta/containers/ApolloProviderWrapper';
import {server} from '@tests/jestSettings/mockServer';
import BaseTheme from '@macanta/modules/BaseTheme';

export const renderApp = (children) =>
  render(
    <ApolloProvider client={client}>
      <BaseTheme>{children}</BaseTheme>
    </ApolloProvider>,
  );

export const testRenderer = (children) => (handlersArg) => {
  overrideHandlers(handlersArg);

  return renderApp(children);
};

export const overrideHandlers = (handlersArg, args) => {
  if (handlersArg) {
    const handlers = isArray(handlersArg) ? handlersArg : [handlersArg];

    handlers.forEach((handler) => {
      server.use(isFunction(handler) ? handler(args) : handler);
    });
  }
};
