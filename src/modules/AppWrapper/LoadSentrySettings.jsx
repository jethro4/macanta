import {useLayoutEffect} from 'react';
import {useReactiveVar} from '@apollo/client';
import {loggedInVar, sessionExpiredVar} from '@macanta/graphql/cache/vars';
import * as Storage from '@macanta/utils/storage';

const Sentry = require('@sentry/browser');

const LoadSentrySettings = () => {
  const loggedIn = useReactiveVar(loggedInVar);
  const sessionExpired = useReactiveVar(sessionExpiredVar);

  useLayoutEffect(() => {
    if (!loggedIn) {
      Sentry.setTag('session_info', 'Not logged in');
    } else if (sessionExpired) {
      Sentry.setTag('session_info', 'Session expired');
    } else {
      const session = Storage.getItem('session');

      Sentry.setTag('session_info', session?.sessionId);
    }
  }, [loggedIn, sessionExpired]);

  useLayoutEffect(() => {
    if (!loggedIn) {
      Sentry.setTag('user_id', 'Not logged in');
    } else {
      const userDetails = Storage.getItem('userDetails');

      Sentry.setTag('user_id', userDetails?.userId);
    }
  }, [loggedIn]);

  return null;
};

export default LoadSentrySettings;
