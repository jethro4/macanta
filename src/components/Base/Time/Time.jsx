import React from 'react';
import Typography from '@mui/material/Typography';
import AutoUpdate from '@macanta/components/Base/AutoUpdate';
import useValue from '@macanta/hooks/base/useValue';
import {createDate} from '@macanta/utils/time';
import {getIntervalByUnit} from '@macanta/components/Base/Time/helpers';

const Time = ({intervalType, format, sx, ...props}) => {
  const [interval] = useValue(() => {
    const intervalVal = getIntervalByUnit(intervalType);

    return intervalVal;
  }, [intervalType, format]);

  const key = `${interval}-${format}`;

  return (
    <AutoUpdate
      key={key}
      interval={interval}
      renderComponent={() => {
        const dateToFormat = createDate();

        return (
          <Typography
            sx={{
              fontSize: '1em',
              ...sx,
            }}
            {...props}>
            {dateToFormat.format(format)}
          </Typography>
        );
      }}
    />
  );
};

Time.defaultProps = {
  intervalType: 'minute',
  format: 'lll',
};

export default Time;
