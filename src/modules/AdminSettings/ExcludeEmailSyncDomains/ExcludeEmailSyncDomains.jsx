import React, {useMemo} from 'react';
import Breadcrumbs from '@mui/material/Breadcrumbs';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import Typography from '@mui/material/Typography';
import Section from '@macanta/containers/Section';
import DataTable from '@macanta/containers/DataTable';
import {ROOT_PATH_ADMIN} from '@macanta/modules/AdminSettings/AdminSettings';
import useAppSettings from '@macanta/hooks/admin/useAppSettings';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import AddDomainBtn from './AddDomainBtn';
import DeleteDomainBtn from './DeleteDomainBtn';
import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import * as AdminSettingsStyled from '@macanta/modules/AdminSettings/styles';
import {navigate} from 'gatsby';

const CUSTOM_TABS_COLUMNS = [
  {
    id: 'domain',
    label: 'Domain',
    maxWidth: '100%',
  },
];

const ExcludeEmailSyncDomains = () => {
  /* const navigate = useNavigate();*/

  const appSettingsQuery = useAppSettings({
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const filteredEmailDomain = appSettingsQuery.data?.filteredEmailDomain;

  const handleBack = () => {
    navigate(ROOT_PATH_ADMIN);
  };

  const domains = useMemo(() => {
    if (filteredEmailDomain) {
      return filteredEmailDomain.map((domain) => ({
        id: domain,
        domain,
      }));
    }

    return [];
  }, [filteredEmailDomain]);

  return (
    <NoteTaskHistoryStyled.FullHeightGrid container>
      <NoteTaskHistoryStyled.FullHeightFlexGridColumn item xs={12} lg={6}>
        <Section
          fullHeight
          bodyStyle={{
            backgroundColor: 'white',
            padding: 0,
          }}
          HeaderLeftComp={
            <Breadcrumbs aria-label="breadcrumb">
              <AdminSettingsStyled.TitleCrumbBtn
                startIcon={
                  <ArrowBackIcon
                    style={{
                      color: '#333',
                    }}
                  />
                }
                onClick={handleBack}
                disabled={false}>
                <Typography color="textPrimary">Admin Settings</Typography>
              </AdminSettingsStyled.TitleCrumbBtn>
              <Typography>Exclude Domains</Typography>
            </Breadcrumbs>
          }
          HeaderRightComp={<AddDomainBtn onSave={appSettingsQuery.refetch} />}>
          <DataTable
            noOddRowStripes
            fullHeight
            columns={CUSTOM_TABS_COLUMNS}
            data={domains}
            order="desc"
            orderBy="created"
            rowsPerPage={25}
            rowsPerPageOptions={[25, 50, 100]}
            renderActionButtons={({item}) => (
              <DeleteDomainBtn name={item.domain} />
            )}
            actionColumn={{
              show: true,
              style: {
                minWidth: 100,
              },
            }}
          />
        </Section>
      </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
    </NoteTaskHistoryStyled.FullHeightGrid>
  );
};

export default ExcludeEmailSyncDomains;
