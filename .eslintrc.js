module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
    'jest/globals': true,
    es6: true,
  },
  plugins: [
    'react',
    'import',
    'unused-imports',
    'react-hooks',
    'jsx-a11y',
    'jest',
    'jest-dom',
    'testing-library',
  ],
  extends: [
    'eslint:recommended',
    'plugin:react/recommended',
    'plugin:import/recommended',
    'plugin:jsx-a11y/recommended',
    'plugin:jest/recommended',
    'plugin:jest-dom/recommended',
    'plugin:testing-library/react',
  ],
  parserOptions: {
    ecmaVersion: 13,
    parser: '@babel/eslint-parser',
    sourceType: 'module',
    requireConfigFile: false,
    babelOptions: {
      babelrc: false,
      configFile: false,
    },
  },
  settings: {
    react: {
      version: 'detect',
    },
    'import/resolver': {
      alias: {
        extensions: ['.js', '.jsx', '.ts', '.tsx', '.json'],
        map: [
          ['@macanta', './src'],
          ['@mui/material', './node_modules/@material-ui/core'],
          ['@mui/icons-material', './node_modules/@material-ui/icons'],
          ['@mui/x-date-pickers', './node_modules/@material-ui/lab'],
          ['@tests', './tests'],
        ],
      },
    },
    'import/extensions': ['.js', '.jsx', '.ts', '.tsx'],
    'import/order': [
      'warn',
      {
        groups: ['builtin', 'external', 'internal'],
        pathGroups: [
          {
            pattern: 'react',
            group: 'external',
            position: 'before',
          },
          {
            pattern: 'src/**',
            group: 'internal',
            position: 'after',
          },
        ],
        pathGroupsExcludedImportTypes: ['react'],
        'newlines-between': 'always',
        alphabetize: {
          order: 'asc',
          caseInsensitive: true,
        },
      },
    ],
  },
  rules: {
    'react/prop-types': 'off',
    'react/no-unescaped-entities': 'off',
    'react-hooks/rules-of-hooks': 'error',
    'react-hooks/exhaustive-deps': 'off',
    'react/display-name': 'off',
    'react/jsx-filename-extension': 'warn',
    'import/no-unresolved': 'error',
    'unused-imports/no-unused-imports': 'warn',
    'no-shadow': 'warn',
    'no-prototype-builtins': 'off',
    'no-param-reassign': 'warn',
    'no-empty': 'warn',
    'no-use-before-define': [
      'error',
      {
        functions: false,
        classes: false,
        variables: true,
        allowNamedExports: false,
      },
    ],
    'jsx-a11y/no-autofocus': 'warn',
    'testing-library/no-node-access': 'warn',
    'testing-library/render-result-naming-convention': 'warn',
    'testing-library/await-async-utils': 'warn',
    'jest/expect-expect': [
      'warn',
      {
        assertFunctionNames: ['is*', 'has*', 'expect'],
        additionalTestBlockFunctions: [],
      },
    ],
    'no-restricted-imports': [
      'error',
      {
        patterns: [
          {
            group: ['../*'],
            message: 'Usage of relative parent imports is not allowed.',
          },
        ],
      },
    ],
  },
};
