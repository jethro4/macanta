import {gql} from '@apollo/client';

export default gql`
  query getSetupDetails {
    setupDetails @client {
      appName
      apiKey
    }
  }
`;
