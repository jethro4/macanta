import isNil from 'lodash/isNil';

const getStorage = (isSession) =>
  !isSession ? window.localStorage : window.sessionStorage;

export const setItem = (storageKey, value, isSession) => {
  const serializedValue =
    typeof value === 'object' ? JSON.stringify(value) : value;

  if (!storageKey || isNil(value)) {
    throw 'Empty storageKey or value';
  }

  try {
    getStorage(isSession)[storageKey] = serializedValue;
  } catch (err) {}
};

export const getItem = (storageKey, isSession) => {
  let deserializedValue = null;
  let value = null;

  try {
    value = getStorage(isSession)[storageKey];
    deserializedValue = JSON.parse(value);
  } catch (err) {
    deserializedValue = value; // getItem value is of type String
  }

  return deserializedValue;
};

export const clear = (isSession) => {
  try {
    getStorage(isSession).clear();
  } catch (err) {}
};
