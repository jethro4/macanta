import {DELETE_AUTOMATIONS} from '@macanta/graphql/automation';
import useDeleteMutation from '@macanta/hooks/apollo/useDeleteMutation';

const useDeleteAutomationsMutation = (options) => {
  const mutation = useDeleteMutation(DELETE_AUTOMATIONS, options);

  return mutation;
};

export default useDeleteAutomationsMutation;
