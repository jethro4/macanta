import {gql} from '@apollo/client';

export const TRIGGER_ACTIONS_FRAGMENT = gql`
  fragment triggerActionsFragment on TriggerActionItems {
    queryId
    queryType
    queryDescription
    queryConnectedDataType
    queryName
    queryConnectedDataEmail
    queryConnectedDataSMS
    queryConnectedDataFieldAction
    queryConnectedDataContactAction
    queryConnectedDataUserAction
    queryConnectedDataHTTPPost
    queryContact {
      queryContactRelationshipFieldLogic
      queryContactRelationship
    }
    queryCDField {
      queryCDFieldLogic
      queryCDFieldName
      queryCDFieldOperator
      queryCDFieldValue
      queryCDFieldValues
    }
  }
`;

export default gql`
  query getTriggerActions($id: ID) {
    getTriggerActions(id: $id) {
      action
      status
      count
      items {
        ...triggerActionsFragment
      }
    }
  }
  ${TRIGGER_ACTIONS_FRAGMENT}
`;
