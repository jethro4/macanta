import React, {useState, useRef} from 'react';
import BorderColorIcon from '@mui/icons-material/BorderColor';
import EditIcon from '@mui/icons-material/Edit';
import Tooltip from '@macanta/components/Tooltip';
import Popover from '@macanta/components/Popover';
import Button from '@macanta/components/Button';
import {useMutation} from '@apollo/client';
import {
  CREATE_OR_UPDATE_EMAIL_SIGNATURE,
  GET_EMAIL_SIGNATURE,
} from '@macanta/graphql/admin';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import * as FormStyled from '@macanta/modules/NoteTaskForms/styles';
import * as Styled from './styles';
import * as Storage from '@macanta/utils/storage';

const EmailSignatureBtn = ({
  value,
  onEdit,
  renderOpenButton,
  title,
  anchorOrigin,
  transformOrigin,
}) => {
  const loggedInUserDetails = Storage.getItem('userDetails');

  const [anchorEl, setAnchorEl] = useState(false);
  const htmlRef = useRef();

  const openOtherDetails = Boolean(anchorEl);

  const [
    callCreateOrUpdateEmailSignatureMutation,
    {loading, client},
  ] = useMutation(CREATE_OR_UPDATE_EMAIL_SIGNATURE, {
    onError() {},
  });

  const handleOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleCloseOtherDetails = () => {
    setAnchorEl(null);
  };

  const handleChangeHTML = (newHTML) => {
    htmlRef.current = newHTML;
  };

  const handleEdit = async () => {
    await callCreateOrUpdateEmailSignatureMutation({
      variables: {
        createOrUpdateEmailSignatureInput: {
          userId: loggedInUserDetails.userId,
          signature: htmlRef.current,
        },
        __mutationkey: 'createOrUpdateEmailSignatureInput',
      },
    });

    client.writeQuery({
      query: GET_EMAIL_SIGNATURE,
      data: {
        getEmailSignature: {
          __typename: 'EmailSignature',
          html: htmlRef.current,
        },
      },
      variables: {
        userId: loggedInUserDetails.userId,
      },
    });

    onEdit && onEdit(htmlRef.current);
    handleCloseOtherDetails();
  };

  return (
    <>
      {!renderOpenButton ? (
        <Tooltip title={title} placement="top">
          <div>
            <Styled.EmailFooterIconButton
              color="secondary"
              onClick={handleOpen}
              size="small">
              <BorderColorIcon />
            </Styled.EmailFooterIconButton>
          </div>
        </Tooltip>
      ) : (
        renderOpenButton(handleOpen)
      )}
      <Popover
        disableEnforceFocus
        title={title}
        open={openOtherDetails}
        anchorEl={anchorEl}
        onClose={handleCloseOtherDetails}
        anchorOrigin={anchorOrigin}
        transformOrigin={transformOrigin}>
        <div
          style={{
            width: 800,
            height: 600,
            display: 'flex',
            flexDirection: 'column',
          }}>
          <Styled.EmailRichTextField
            value={value}
            style={{
              flex: 1,
              borderBottom: '1px solid #f1f1f1',
            }}
            richText
            // error={errors.message}
            // onChange={handleChange('message')}
            onChange={handleChangeHTML}
            placeholder="Insert Your Signature Here"
            variant="outlined"
            // value={values.message}
            fullWidth
            size="medium"
            contentStyleBodyCSS="line-height: 1.7;"
          />
          <FormStyled.Footer
            style={{
              margin: '8px 0',
              padding: '0 1rem',
            }}>
            <Button
              // disabled={!values.message}
              variant="contained"
              startIcon={<EditIcon />}
              onClick={handleEdit}
              size="medium">
              Save Signature
            </Button>
          </FormStyled.Footer>
        </div>
        <LoadingIndicator fill backdrop loading={loading} />
      </Popover>
    </>
  );
};

EmailSignatureBtn.defaultProps = {
  title: 'Signature',
  anchorOrigin: {
    vertical: 'top',
    horizontal: 'right',
  },
  transformOrigin: {
    vertical: 'bottom',
    horizontal: 'right',
  },
};

export default EmailSignatureBtn;
