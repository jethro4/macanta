import React, {useEffect, useState} from 'react';
import useQuery from '@macanta/hooks/apollo/useQuery';
import FactCheckIcon from '@mui/icons-material/FactCheck';
import Box from '@mui/material/Box';
import {PopoverButton} from '@macanta/components/Button';
import DOItemQueries from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/DOItemQueries';
import MoreOptions from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/MoreOptions';
import {GET_DATA_OBJECT} from '@macanta/graphql/dataObjects';
import WidgetDOEditPopover from './WidgetDOEditPopover';
import * as Styled from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/styles';

const WidgetQueryDOItemButtons = ({
  doTitle,
  itemId,
  onSave,
  fields: doFields,
  relationships,
  loading: loadingProp,
  setGroupId,
  groupId,
  hideQueriesBtn,
}) => {
  const [selectedItemId, setSelectedItemId] = useState(null);

  const handleClickActionButton = () => {
    setSelectedItemId(itemId);
  };

  return (
    <WidgetDOItemWithItemId
      itemId={selectedItemId}
      setGroupId={setGroupId}
      groupId={groupId}>
      {({loading: doItemLoading, doItem}) => {
        const loading = Boolean(doItemLoading || loadingProp);

        return (
          <Box onClick={handleClickActionButton}>
            <Styled.ActionButtonContainer>
              <WidgetDOEditPopover
                loading={loading}
                doItem={doItem}
                doFields={doFields}
                relationships={relationships}
                onSave={onSave}
              />
              {!hideQueriesBtn && (
                <PopoverButton
                  icon
                  sx={{
                    color: '#aaa',
                    marginRight: '-0.25rem',
                    padding: '0.25rem',
                  }}
                  // onClick={handleAdd}
                  size="small"
                  TooltipProps={{
                    title: 'Queries',
                  }}
                  PopoverProps={{
                    title: 'Queries',
                    contentWidth: 300,
                    anchorOrigin: {
                      vertical: 'center',
                      horizontal: 'left',
                    },
                    transformOrigin: {
                      vertical: 'center',
                      horizontal: 'right',
                    },
                  }}
                  renderContent={(handleClose) => (
                    <DOItemQueries
                      item={doItem}
                      doTitle={doTitle}
                      onClose={handleClose}
                      onSave={onSave}
                    />
                  )}>
                  <FactCheckIcon />
                </PopoverButton>
              )}
              <MoreOptions
                style={{
                  color: '#aaa',
                }}
                itemId={itemId}
                groupId={doItem?.groupId}
                fields={doFields}
                relationships={relationships}
                connectedContacts={doItem?.connectedContacts}
                // refetch={refetch}
                loading={loading}
                doTitle={doTitle}
              />
            </Styled.ActionButtonContainer>
          </Box>
        );
      }}
    </WidgetDOItemWithItemId>
  );
};

const WidgetDOItemWithItemId = ({itemId, setGroupId, groupId, ...props}) => {
  return (
    <WidgetDOItemWithData
      setGroupId={setGroupId}
      groupId={groupId}
      itemId={itemId}
      {...props}
    />
  );
};

const WidgetDOItemWithData = ({itemId, setGroupId, groupId, children}) => {
  const {data, loading} = useQuery(GET_DATA_OBJECT, {
    variables: {
      itemId,
    },
    skip: !itemId,
  });

  const doItem = data?.getDataObject;

  useEffect(() => {
    if ((!groupId || doItem?.groupId !== groupId) && doItem?.groupId) {
      setGroupId(doItem?.groupId);
    }
  }, [doItem?.groupId]);

  return children({doItem, loading: !doItem && loading});
};

export default WidgetQueryDOItemButtons;
