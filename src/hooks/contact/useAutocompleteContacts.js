import {useState, useEffect} from 'react';
import {useApolloClient, useReactiveVar} from '@apollo/client';
import isNumber from 'lodash/isNumber';
import {
  DEFAULT_PAGE,
  DEFAULT_PAGE_LIMIT,
  DEFAULT_ORDER,
  DEFAULT_ORDER_BY,
} from '@macanta/modules/hoc/ContactsQuery';
import usePrevious from '@macanta/hooks/usePrevious';
import {
  getVariables as getItemVariables,
  writeQueryItem,
} from '@macanta/hooks/contact/useContact';
import {LIST_CONTACTS} from '@macanta/graphql/contacts';
import {recentContactsVar} from '@macanta/graphql/cache/vars';
import {getAppInfo} from '@macanta/utils/app';
import * as Storage from '@macanta/utils/storage';
import envConfig from '@macanta/config/envConfig';

export const AUTOCOMPLETE_MINIMUM_SEARCH_LENGTH = 1;

const DEFAULT_AUTOCOMPLETE_LIMIT = 10;

const useAutocompleteContacts = (searchQuery, options) => {
  const client = useApolloClient();
  const recentContacts = useReactiveVar(recentContactsVar);

  const search = searchQuery?.search || '';

  const {userId: loggedInUserId} = Storage.getItem('userDetails');

  const [loading, setLoading] = useState(false);
  const [data, setData] = useState(null);
  const [error, setError] = useState();

  const prevSearch = usePrevious(search);

  const getAutocompleteContacts = (q) => {
    setLoading(true);

    const [appName] = getAppInfo();

    fetch(
      `https://${appName}.${envConfig.apiDomain}/search.php?q=${q}&limit=${
        options?.limit || DEFAULT_AUTOCOMPLETE_LIMIT
      }&userId=${loggedInUserId}`,
    )
      .then((res) => res.json())
      .then((res) => {
        const items = res || [];

        const transformedItems = transformContactItems(items);

        if (transformedItems.length) {
          const listVariables = getVariables(q);

          const listCachedData = client.readQuery({
            query: LIST_CONTACTS,
            variables: listVariables,
          });

          if (!listCachedData?.listContacts) {
            client.writeQuery({
              query: LIST_CONTACTS,
              data: {
                listContacts: {
                  __typename: 'ContactItems',
                  items: transformedItems,
                  count: transformedItems.length,
                  total: DEFAULT_AUTOCOMPLETE_LIMIT,
                },
              },
              variables: listVariables,
            });
          }

          transformedItems.forEach((item) => {
            if (item.id) {
              const itemVariables = getItemVariables(item.id);

              const itemCachedData = client.readQuery({
                query: LIST_CONTACTS,
                variables: itemVariables,
              });

              if (!itemCachedData?.listContacts) {
                writeQueryItem(item);
              }
            }
          });
        }

        setData(transformedItems);
      })
      .catch((err) => {
        console.error(err);

        setData(null);

        setError(err);
      })
      .finally(() => {
        options?.onComplete && options.onComplete();

        setLoading(false);
      });
  };

  const getVariables = (q) => ({
    q,
    page: DEFAULT_PAGE,
    limit: DEFAULT_PAGE_LIMIT,
    order: DEFAULT_ORDER,
    orderBy: DEFAULT_ORDER_BY,
  });

  const transformContactItems = (items = []) => {
    return items.map((item) => ({
      id: item.Id,
      firstName: item.FirstName,
      lastName: item.LastName,
      email: item.Email,
      phoneNumbers: [item.Phone1, item.Phone2, item.Phone3].filter(
        (phone) => !!phone,
      ),
      company: item.Company,
      streetAddress1: item.StreetAddress1,
      city: item.City,
      country: item.Country,
      postalCode: item.PostalCode,

      email2: '',
      email3: '',
      middleName: '',
      birthday: '',
      title: '',
      website: '',
      jobTitle: '',
      streetAddress2: '',
      state: '',
      address2Street1: '',
      address2Street2: '',
      city2: '',
      state2: '',
      postalCode2: '',
      country2: '',
      loggedInUserPermission: '',
      phoneNumberExts: [],
      createdDate: '',
      customFields: [],
      __typename: 'Contact',
    }));
  };

  useEffect(() => {
    if (
      search.length >=
        (isNumber(options?.minSearchLength)
          ? options?.minSearchLength
          : AUTOCOMPLETE_MINIMUM_SEARCH_LENGTH) &&
      !options?.skip
    ) {
      if (prevSearch !== search) {
        getAutocompleteContacts(search);
      } else {
        options?.onComplete && options.onComplete();
      }
    } else {
      setData(options?.defaultValue || recentContacts || null);
    }
  }, [searchQuery]);

  return {loading, data, error};
};

export default useAutocompleteContacts;
