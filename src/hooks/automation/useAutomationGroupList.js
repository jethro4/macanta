import useQuery from '@macanta/hooks/apollo/useQuery';
import {GET_AUTOMATION_GROUPS} from '@macanta/graphql/automation';

const useAutomationGroupList = (options) => {
  const query = useQuery(GET_AUTOMATION_GROUPS, options);

  return query;
};

export default useAutomationGroupList;
