import useSignature from '@macanta/hooks/admin/useSignature';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';

const LoadSignature = () => {
  useSignature({
    fetchPolicy: FETCH_POLICIES.CACHE_AND_NETWORK,
  });

  return null;
};

export default LoadSignature;
