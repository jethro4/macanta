import React from 'react';
import SettingsIcon from '@mui/icons-material/Settings';
import FormField from '@macanta/containers/FormField';
import ActionAreaContainer from './ActionAreaContainer';
import FieldSettingsForm from './FieldSettingsForm';

const FormUI = ({
  isEdit,
  hideDelete,
  onDelete,
  item,
  renderActionButtons,
  ...props
}) => {
  const handleDelete = () => {
    onDelete('field', item.fieldId);
  };

  const handleRenderActionButtons = () => {
    return renderActionButtons(item);
  };

  return (
    <ActionAreaContainer
      hideBorder={!isEdit}
      hideActionButtons={!isEdit}
      {...(renderActionButtons && {
        renderActionButtons: handleRenderActionButtons,
      })}
      renderForm={(handleClose) => (
        <FieldSettingsForm item={item} onClose={handleClose} />
      )}
      editIcon={
        <SettingsIcon
          sx={{
            color: '#2196f3bb',
          }}
          style={{
            fontSize: 20,
          }}
        />
      }
      editTitle="Field Settings"
      hideDelete={hideDelete}
      onDelete={handleDelete}>
      <FormField
        raw
        style={{
          pointerEvents: isEdit ? 'none' : 'auto',
        }}
        // labelPosition="normal"
        id={item.id}
        type={item.type}
        options={item.choices}
        required={item.required}
        defaultValue={item.default}
        tooltip={item.tooltip}
        label={item.label}
        {...(item.type === 'file' &&
          !item.multiple && {
            filesLimit: 1,
          })}
        size="small"
        InputProps={{
          style: {
            // backgroundColor: '#fbfbfb',
            backgroundColor: 'white',
          },
        }}
        {...(item.type === 'TextArea' && {
          hideExpand: true,
        })}
        {...props}
      />
    </ActionAreaContainer>
  );
};

export default FormUI;
