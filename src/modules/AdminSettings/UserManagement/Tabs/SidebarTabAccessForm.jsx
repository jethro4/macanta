import React from 'react';
import Switch from '@macanta/components/Forms/ChoiceFields/Switch';
import useSidebarTabsWithPermission from '@macanta/hooks/contact/useSidebarTabsWithPermission';
import BodySection from '@macanta/modules/AdminSettings/UserManagement/BodySection';
import PermissionsTable from '@macanta/modules/AdminSettings/UserManagement/Tabs/PermissionsTable';
import {arrayConcatOrUpdateByKey} from '@macanta/utils/array';
import {ACCESS_LEVELS} from '@macanta/constants/accessLevels';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {
  AccessRadioGroup,
  AccessTableHeadCell,
} from '@macanta/modules/AdminSettings/UserManagement/Tabs/AccessPermissions';

const TAB_ACCESS_COLUMNS = [
  {
    id: 'hasAccess',
    label: '',
    disableSort: true,
    disableResize: true,
    minWidth: 80,
    maxWidth: 80,
    renderBodyCellValue: ({item, value, handlers}) => {
      return (
        <Switch
          color="success"
          checked={value}
          onChange={(event) => {
            handlers.onChangeAccess({
              item,
              value: event.target.checked
                ? ACCESS_LEVELS.READ_ONLY
                : ACCESS_LEVELS.NO_ACCESS,
            });
          }}
        />
      );
    },
  },
  {
    id: 'title',
    label: 'Tab Name',
    maxWidth: '100%',
    getBodyCellStyle: ({item}) =>
      !item.hasAccess && {
        color: 'grayDarkest.main',
      },
  },
  {
    id: 'permission',
    label: '',
    disableSort: true,
    disableResize: true,
    minWidth: 200,
    maxWidth: 200,
    renderHeadCellValue: () => {
      return <AccessTableHeadCell />;
    },
    renderBodyCellValue: ({item, value, handlers}) => {
      return (
        item.hasAccess && (
          <AccessRadioGroup
            value={value}
            onChange={(val) => {
              handlers.onChangeAccess({item, value: val});
            }}
          />
        )
      );
    },
  },
];

const SidebarTabAccessForm = ({permission, onChange, ...props}) => {
  const sidebarTabsQuery = useSidebarTabsWithPermission(permission, {
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const handleChangeAccess = ({item, value}) => {
    const updatedPermission = {
      ...permission,
      tabs: arrayConcatOrUpdateByKey({
        arr: permission?.tabs,
        item: {
          groupId: item.groupId === 'co_customfields' ? item.id : item.groupId,
          permission: value,
        },
        key: 'groupId',
        includeAll: true,
      }),
    };

    onChange(updatedPermission);
  };

  return (
    <BodySection {...props}>
      <PermissionsTable
        loading={sidebarTabsQuery.loading}
        columns={TAB_ACCESS_COLUMNS}
        data={sidebarTabsQuery.data}
        handlers={{
          onChangeAccess: handleChangeAccess,
        }}
      />
    </BodySection>
  );
};

export default SidebarTabAccessForm;
