import {getColumnSpanOptions} from '@macanta/components/Table/helpers';

describe('Table Helpers', () => {
  describe('getColumnSpanOptions', () => {
    let columns;

    beforeEach(() => {
      columns = [
        {id: 'col1', label: 'Column 1'},
        {id: 'col2', label: 'Column 2'},
      ];
    });

    it('happy - 2 columns - should remove succeeding column if within colSpan range', () => {
      const item = {
        col1: 'Test Col 1',
        col2: 'Test Col 2',
        options: {colSpans: [['col1', 2]]},
      };
      const props = {
        colSpans: item.options.colSpans,
        columns,
        hasActionButtons: true,
      };

      const actualResult = getColumnSpanOptions(props);
      const expectedResult = {
        columns: [{id: 'col1', label: 'Column 1', colSpan: 2}],
        hasActionButtons: true,
      };

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - 3 columns - should remove succeeding column if within colSpan range', () => {
      columns = columns.concat({id: 'col3', label: 'Column 3'});

      const item = {
        col1: 'Test Col 1',
        col2: 'Test Col 2',
        col3: 'Test Col 3',
        options: {colSpans: [['col1', 2]]},
      };
      const props = {
        colSpans: item.options.colSpans,
        columns,
        hasActionButtons: true,
      };

      const actualResult = getColumnSpanOptions(props);
      const expectedResult = {
        columns: [
          {id: 'col1', label: 'Column 1', colSpan: 2},
          {id: 'col3', label: 'Column 3'},
        ],
        hasActionButtons: true,
      };

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - should set hasActionButtons=false if colSpan reach end of columns array', () => {
      const item = {
        col1: 'Test Col 1',
        col2: 'Test Col 2',
        options: {colSpans: [['col2', 2]]},
      };
      const props = {
        colSpans: item.options.colSpans,
        columns,
        hasActionButtons: true,
      };

      const actualResult = getColumnSpanOptions(props);
      const expectedResult = {
        columns: [
          {id: 'col1', label: 'Column 1'},
          {id: 'col2', label: 'Column 2', colSpan: 2},
        ],
        hasActionButtons: false,
      };

      expect(actualResult).toEqual(expectedResult);
    });

    it('sad - return original data if no colSpans found', () => {
      const item = {
        col1: 'Test Col 1',
        col2: 'Test Col 2',
      };
      const props = {
        colSpans: item.options?.colSpans,
        columns,
        hasActionButtons: true,
      };

      const actualResult = getColumnSpanOptions(props);
      const expectedResult = {
        columns: [
          {id: 'col1', label: 'Column 1'},
          {id: 'col2', label: 'Column 2'},
        ],
        hasActionButtons: true,
      };

      expect(actualResult).toEqual(expectedResult);
    });
  });
});
