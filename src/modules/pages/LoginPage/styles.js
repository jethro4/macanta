import {experimentalStyled as styled} from '@mui/material/styles';
import TextFieldComp from '@macanta/components/Forms';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Button from '@macanta/components/Button';
import Paper from '@macanta/components/Paper';
import Page from '@macanta/components/Page';

export const TextField = styled(TextFieldComp)`
  margin-bottom: ${({theme}) => theme.spacing(2)};
`;

export const VerificationTextField = styled(TextFieldComp)`
  margin-bottom: ${({theme}) => theme.spacing(3)};
  height: 64px;
  padding-right: 0;
  width: 180px;

  .MuiInputBase-root {
    letter-spacing: 8px;
  }
`;

export const Root = styled(Page)`
  background-color: white;
`;

export const Content = styled(Box)`
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding-bottom: 5rem;
`;

export const FormPaper = styled(Paper)`
  margin-top: ${({theme}) => theme.spacing(4)};
  width: 90%;
  ${({theme}) => theme.breakpoints.up('sm')} {
    width: 68%;
    max-width: 31.25rem;
  }
`;

export const Description = styled(Typography)`
  margin-bottom: ${({theme}) => theme.spacing(2)};
`;

export const GridItem = styled(Grid)`
  margin: ${({theme}) => theme.spacing(1)} 0;
`;

export const ButtonLeft = styled(Button)`
  width: 100%;
  ${({theme}) => theme.breakpoints.up('sm')} {
    width: auto;
  }
`;

export const ButtonRight = styled(Button)`
  float: right;
  width: 100%;
  ${({theme}) => theme.breakpoints.up('sm')} {
    width: auto;
  }
`;
