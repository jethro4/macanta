import {gql} from '@apollo/client';
import {QUERY_BUILDER_ITEM_ATTRIBUTES} from './fragments';

export default gql`
  query listQueryBuilders {
    listQueryBuilders {
      ...queryBuilderItem
    }
  }
  ${QUERY_BUILDER_ITEM_ATTRIBUTES}
`;
