import {useMemo} from 'react';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import useQuery from '@macanta/hooks/apollo/useQuery';
import {LIST_EMAIL_TEMPLATES} from '@macanta/graphql/admin';
import {GET_ACCESS_PERMISSIONS} from '@macanta/graphql/permissions';
import useIsAdmin from '@macanta/hooks/useIsAdmin';
import * as Storage from '@macanta/utils/storage';

const useEmailTemplates = () => {
  const {email: userEmail} = Storage.getItem('userDetails');
  const emailTemplatesQuery = useQuery(LIST_EMAIL_TEMPLATES);

  const accessPermissionsQuery = useQuery(GET_ACCESS_PERMISSIONS, {
    variables: {
      email: userEmail,
    },
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
    onError() {},
  });

  const isAdmin = useIsAdmin();

  const templates = emailTemplatesQuery.data?.listEmailTemplates.items;
  const permittedEmailTemplates =
    accessPermissionsQuery.data?.getAccessPermissions?.emailTemplates;

  const memoizedTemplates = useMemo(() => {
    if (isAdmin && templates) {
      return templates;
    }
    if (templates?.length && permittedEmailTemplates?.length) {
      const filteredTemplates = templates.filter((item) =>
        permittedEmailTemplates.some(
          (pItem) => pItem.templateGroupId === item.templateGroupId,
        ),
      );

      return filteredTemplates;
    }

    return [];
  }, [templates, permittedEmailTemplates]);

  return {...emailTemplatesQuery, templates: memoizedTemplates};
};

export default useEmailTemplates;
