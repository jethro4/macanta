import {useRef, useState} from 'react';
import {
  addActiveQuerySession,
  getBatchCache,
  getBatchResult,
  getTagFieldName,
} from '@macanta/utils/graphql';
import useDeepCompareEffect from '@macanta/hooks/base/useDeepCompareEffect';
import useValue from '@macanta/hooks/base/useValue';
import useCleanUp from '@macanta/hooks/base/useCleanUp';
import useFixedValue from '@macanta/hooks/base/useFixedValue';

const useBatchQuery = (graphqlTag, optionsArg) => {
  const tagFieldName = getTagFieldName(graphqlTag);

  const pathname = useFixedValue(window.location.pathname);
  const options = Object.assign({}, optionsArg, {
    dependencies: optionsArg?.dependencies || [pathname],
  });

  const [loading, setLoading] = useState(false);
  const abortAllFuncRef = useRef();

  const [data, setData] = useValue(() => getBatchCache(graphqlTag, options), [
    tagFieldName,
    options,
  ]);

  const callBatchQuery = async () => {
    setLoading(true);

    try {
      const response = getBatchResult(graphqlTag, options);

      abortAllFuncRef.current = response.abortAll;

      const batchResult = await response.promise;

      options?.variablesArr?.forEach((variables) => {
        addActiveQuerySession({
          graphqlTag,
          options: {
            ...options,
            variables,
          },
        });
      });

      setData(batchResult);
    } finally {
      setLoading(false);
    }
  };

  const abortBatchQuery = () => {
    abortAllFuncRef.current?.();
  };

  useDeepCompareEffect(() => {
    abortBatchQuery();

    if (!options?.skip) {
      callBatchQuery();
    }
  }, [tagFieldName, options?.skip, options?.variablesArr]);

  useCleanUp(() => {
    abortBatchQuery();
  });

  const initLoading = !data && loading;
  const initLoaded = !!data;

  return {
    loading,
    data,
    initLoading,
    initLoaded,
    variablesArr: options?.variablesArr,
  };
};

export default useBatchQuery;
