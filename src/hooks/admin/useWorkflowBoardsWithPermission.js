import useValue from '@macanta/hooks/base/useValue';
import useWorkflowBoards from './useWorkflowBoards';

const useWorkflowBoardsWithPermission = ({permission, ...args}, options) => {
  const workflowBoardsQuery = useWorkflowBoards(args, options);

  const workflowBoards = workflowBoardsQuery.data;

  const [data] = useValue(() => {
    return workflowBoards?.map((board) => {
      const permissionItem = permission?.workflowBoards?.find(
        (p) => p.boardId === board.id,
      );
      const boardScope = permissionItem?.scope || 'CurrentUser';

      return {
        ...board,
        scope: boardScope,
      };
    });
  }, [workflowBoards, permission]);

  return {...workflowBoardsQuery, data};
};

export default useWorkflowBoardsWithPermission;
