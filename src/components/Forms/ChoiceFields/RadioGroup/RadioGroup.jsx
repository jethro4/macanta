import React from 'react';
import Box from '@mui/material/Box';
import {OverflowTip} from '@macanta/components/Tooltip';
import useTransformChoices from '@macanta/hooks/base/useTransformChoices';
import useValue from '@macanta/hooks/base/useValue';
import * as Styled from './styles';

const RadioGroup = ({
  value: valueProp,
  options: optionsProp,
  labelKey,
  valueKey,
  readOnly,
  onChange,
  renderRightComp,
  renderItemRightComp,
  itemContainerStyle,
  ...props
}) => {
  const [value, setValue] = useValue(valueProp);

  const options = useTransformChoices({
    options: optionsProp,
    labelKey,
    valueKey,
  });

  const handleChange = (newVal) => {
    setValue(newVal);
    onChange && onChange(newVal);
  };

  return (
    <Styled.Root {...props}>
      {options.map((option, index) => {
        const checked = value === option.value;

        if (readOnly && !checked) {
          return null;
        }

        const renderFormLabel = (style) => (
          <Styled.FormControlLabel
            key={option.value}
            disabled={readOnly || option.disabled}
            noLabel={!option.label}
            {...(option.label && {
              label: (
                <OverflowTip
                  style={{
                    width: '100%',
                  }}>
                  {option.label}
                </OverflowTip>
              ),
            })}
            value={option.value}
            sx={{
              '& .MuiFormControlLabel-label': {
                width: '100%',
              },
              ...(option.disabled && {
                '&': {
                  opacity: 0.6,
                },
              }),
            }}
            style={style}
            control={
              <Styled.Radio
                checked={checked}
                readOnly={readOnly}
                onChange={(event) => {
                  if (event.target.checked) {
                    handleChange(option.value);
                  }
                }}
              />
            }
            {...props}
          />
        );
        let radioComp = null;

        if (renderItemRightComp) {
          radioComp = (
            <Box
              sx={{
                '.MuiFormControlLabel-root': {
                  marginRight: 0,
                },
              }}
              style={{
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'center',
                ...itemContainerStyle,
              }}
              {...(checked && {
                className: 'radio-selected',
              })}>
              {renderFormLabel()}
              {renderItemRightComp(option, index)}
            </Box>
          );
        } else {
          radioComp = renderFormLabel(itemContainerStyle);
        }

        return radioComp;
      })}

      {renderRightComp && renderRightComp(value)}
    </Styled.Root>
  );
};

RadioGroup.defaultProps = {
  onChange: () => {},
};

export default RadioGroup;
