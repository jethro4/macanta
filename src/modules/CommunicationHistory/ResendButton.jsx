import React from 'react';
import SendIcon from '@mui/icons-material/Send';
import Tooltip from '@macanta/components/Tooltip';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import {useMutation} from '@apollo/client';
import {EMAIL, SMS} from '@macanta/graphql/admin';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import useContactId from '@macanta/hooks/useContactId';
import {toDataURL} from '@macanta/utils/file';
import * as ContactDetailsStyled from '@macanta/modules/ContactDetails/styles';
import * as Storage from '@macanta/utils/storage';

const ResendButton = ({
  type,
  emailId,
  message,
  subject,
  phoneNumber,
  attachments,
  style,
}) => {
  const loggedInUserDetails = Storage.getItem('userDetails');

  const {displayMessage} = useProgressAlert();
  const contactId = useContactId();

  const [callEmailMutation, emailMutation] = useMutation(EMAIL, {
    onCompleted(data) {
      if (data.email?.success) {
        displayMessage(`Email resent successfully!`);
      }
    },
    onError() {},
  });

  const [callSMSMutation, smsMutation] = useMutation(SMS, {
    onCompleted(data) {
      if (data.sms?.success) {
        displayMessage(`SMS resent successfully!`);
      }
    },
    onError() {},
  });

  const loading = Boolean(emailMutation.loading || smsMutation.loading);

  const handleResendButton = async () => {
    if (type === 'email') {
      callEmailMutation({
        variables: {
          emailInput: {
            userId: loggedInUserDetails.userId,
            sendMethod: 'single',
            mode: 'Live',
            toContacts: contactId,
            subject,
            message,
            uploadedFileAttachments: await Promise.all(
              attachments.map(async (f) => {
                let thumbnail = f.thumbnail;

                if (f.isUrl) {
                  thumbnail = await toDataURL(f.thumbnail);
                }

                return {
                  fileName: f.fileName,
                  content: thumbnail,
                };
              }),
            ),
            resend: true,
            emailId,
          },
          __mutationkey: 'emailInput',
        },
      });
    } else {
      callSMSMutation({
        variables: {
          smsInput: {
            userId: loggedInUserDetails.userId,
            sendMethod: 'single',
            contactId,
            toPhone: phoneNumber,
            txtMessage: message,
          },
          __mutationkey: 'smsInput',
        },
      });
    }
  };

  return (
    <Tooltip title="Resend" placement="top">
      <div>
        <ContactDetailsStyled.EmailFooterIconButton
          color="secondary"
          onClick={handleResendButton}
          size="small"
          style={style}>
          {loading ? <LoadingIndicator transparent size={20} /> : <SendIcon />}
        </ContactDetailsStyled.EmailFooterIconButton>
      </div>
    </Tooltip>
  );
};

ResendButton.defaultProps = {
  attachments: [],
};

export default ResendButton;
