import useQuery from '@macanta/hooks/apollo/useQuery';
import {GET_FIELD_ACTIONS} from '@macanta/graphql/automation';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {findItem} from '@macanta/utils/array';

export const FIELD_KEY_MAPPING = {
  queryId: 'id',
  queryType: 'type',
  queryName: 'name',
  queryDescription: 'description',
  queryConnectedDataType: 'doTitle',
  queryContactRelationship: 'contactRelationship',
  actionType: 'actionType',
  queryCDFieldName: 'fieldName',
  queryCDFieldName1: 'variable1',
  queryCDFieldName2: 'variable2',
  queryCDFieldNameResult: 'result',
  birthdayResultFormat: 'birthdayResultFormat',
  queryCDFieldNameFormat: 'fieldNameFormat',
  dateNumber: 'dateNumber',
  dateDay: 'dateDay',
  dateMonth: 'dateMonth',
  queryCDRequestdate: 'requestDateField',
  dayCount: 'dayCount',
  weekCount: 'weekCount',
  monthCount: 'monthCount',
  yearCount: 'yearCount',
  queryContactListId: 'contactListId',
};

const useFieldActions = (options) => {
  const doTypesQuery = useDOTypes({
    removeEmail: true,
    includeContactObject: true,
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const query = useQuery(GET_FIELD_ACTIONS, {
    keyMapping: FIELD_KEY_MAPPING,
    transformResult: (result) =>
      result?.items.map((item) => {
        const doType = findItem(
          doTypesQuery.result,
          (d) => d.title === item.doTitle,
        );

        return {
          ...item,
          groupId: doType?.id,
        };
      }),
    dependencies: [doTypesQuery.result],
    ...options,
  });

  return query;
};

export default useFieldActions;
