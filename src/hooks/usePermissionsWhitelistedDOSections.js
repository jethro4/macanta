import {useMemo} from 'react';
import useQuery from '@macanta/hooks/apollo/useQuery';
import {GET_ACCESS_PERMISSIONS} from '@macanta/graphql/permissions';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {ACCESS_LEVELS} from '@macanta/constants/accessLevels';
import * as Storage from '@macanta/utils/storage';

const usePermissionsWhitelistedDOSections = (groupId, additionalSections) => {
  const {email: userEmail} = Storage.getItem('userDetails');

  const {data} = useQuery(GET_ACCESS_PERMISSIONS, {
    variables: {
      email: userEmail,
    },
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
    onError() {},
  });

  const isAdmin = data?.getAccessPermissions?.isAdmin;

  const sections = data?.getAccessPermissions?.sections;

  const whitelistedDOSections = useMemo(() => {
    if (!isAdmin) {
      let whitelistedArr = additionalSections
        ? ['General', ...additionalSections]
        : ['General'];

      const section = sections?.find((s) => s.groupId === groupId);

      if (section && section.access?.length) {
        const permittedSections = section.access
          .filter((a) =>
            [
              ACCESS_LEVELS.GLOBAL_ACCESS,
              ACCESS_LEVELS.READ_WRITE,
              ACCESS_LEVELS.READ_ONLY,
            ].includes(a.permission),
          )
          .map((a) => a.name);

        whitelistedArr = whitelistedArr.concat(permittedSections);
      }

      return whitelistedArr;
    }

    return [];
  }, [isAdmin, sections, additionalSections]);

  return whitelistedDOSections;
};

export default usePermissionsWhitelistedDOSections;
