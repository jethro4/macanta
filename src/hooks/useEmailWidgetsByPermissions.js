import {useState, useEffect} from 'react';
import useQuery from '@macanta/hooks/apollo/useQuery';
import {GET_ACCESS_PERMISSIONS} from '@macanta/graphql/permissions';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import * as Storage from '@macanta/utils/storage';

const useEmailWidgetsByPermissions = (widgets) => {
  const {email: userEmail} = Storage.getItem('userDetails');
  const [emailWidgets, setEmailWidgets] = useState([]);

  const {data} = useQuery(GET_ACCESS_PERMISSIONS, {
    variables: {
      email: userEmail,
    },
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
    onError() {},
  });

  useEffect(() => {
    if (!widgets) {
      return;
    }

    const isAdmin = data?.getAccessPermissions?.isAdmin;
    const mqbWidgetEmail = data?.getAccessPermissions?.mqbWidgetEmail;
    const filteredWidgets = widgets.filter((w) => w.type === 'MacantaQuery');

    setEmailWidgets(
      isAdmin
        ? filteredWidgets
        : filteredWidgets.filter((w) => {
            const queryAccess = mqbWidgetEmail?.find(
              (access) => access.queryId === w.queryId,
            );

            return queryAccess?.show;
          }),
    );
  }, [widgets]);

  return emailWidgets;
};

export default useEmailWidgetsByPermissions;
