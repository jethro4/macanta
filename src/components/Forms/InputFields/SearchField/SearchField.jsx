import React, {useState, useEffect, useRef} from 'react';
import Autocomplete, {createFilterOptions} from '@mui/material/Autocomplete';
import ListItemText from '@mui/material/ListItemText';
import TextField from '@macanta/components/Forms/InputFields/TextField';
import * as Styled from '@macanta/components/Forms/InputFields/styles';

export const DEFAULT_OPTIONS_LIMIT = 5;

const defaultFilterOptions = createFilterOptions({
  limit: DEFAULT_OPTIONS_LIMIT,
});

let SearchField = ({
  AutoCompleteProps,
  value: valueProp,
  onChange,
  onSelectOption,
  onEnterPress,
  clearOnSelect,
  disableSuggestions,
  style,
  ...props
}) => {
  const {
    getOptionValue,
    filterOptions = defaultFilterOptions,
    style: autoCompleteStyle,
    ...autoCompleteProps
  } = AutoCompleteProps;

  const [selectedSuggestion, setSelectedSuggestion] = useState(valueProp);
  const [inputValue, setInputValue] = useState(valueProp);

  const focusedValueRef = useRef(null);

  const handleSuggestionChange = (e, sValue, reason) => {
    if (clearOnSelect) {
      setInputValue('');
    }

    if (reason === 'select-option') {
      onSelectOption(sValue);
    }
  };

  const handleInputValue = (e, newInputValue) => {
    setInputValue(newInputValue);

    onChange(newInputValue);
  };

  const handleHighlightChange = (e, highlightedValue, reason) => {
    if (reason === 'keyboard') {
      const optionValue = getOptionValue
        ? getOptionValue(highlightedValue)
        : highlightedValue;

      focusedValueRef.current = optionValue;
    } else {
      focusedValueRef.current = null;
    }
  };

  const handleEnterPress = (e) => {
    onEnterPress && onEnterPress(e, focusedValueRef.current);
  };

  useEffect(() => {
    if (disableSuggestions) {
      setSelectedSuggestion(valueProp);
    }

    if (valueProp !== inputValue) {
      setInputValue(valueProp);
    }
  }, [disableSuggestions, valueProp]);

  return (
    <Autocomplete
      openOnFocus
      blurOnSelect
      disableClearable
      freeSolo
      value={selectedSuggestion}
      onChange={handleSuggestionChange}
      onInputChange={handleInputValue}
      onHighlightChange={handleHighlightChange}
      inputValue={inputValue}
      renderInput={(params) => {
        return (
          <TextField
            {...params}
            {...props}
            style={{
              width: '100%',
              height: '100%',
              ...style,
            }}
            onEnterPress={handleEnterPress}
            InputProps={{
              ...params.InputProps,
              ...props.InputProps,
              style: {
                ...props.InputProps?.style,
              },
            }}
            inputProps={{
              ...params.inputProps,
              ...props.inputProps,
              style: {
                ...props.inputProps?.style,
              },
            }}
          />
        );
      }}
      filterOptions={filterOptions}
      style={{
        width: '100%',
        height: '100%',
        ...autoCompleteStyle,
      }}
      renderOption={(liProps, liOption) => {
        const option = autoCompleteProps.getOptionLabel
          ? autoCompleteProps.getOptionLabel(liOption)
          : liOption;

        return (
          <Styled.SelectListItem {...liProps}>
            <ListItemText primary={option} />
          </Styled.SelectListItem>
        );
      }}
      {...autoCompleteProps}
    />
  );
};

export default SearchField;
