import React from 'react';
import MUIDrawer from '@mui/material/Drawer';

const Drawer = (props) => <MUIDrawer {...props} />;

export default Drawer;
