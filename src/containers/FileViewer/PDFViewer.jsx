import React, {useState} from 'react';
import {Document, Page} from 'react-pdf/dist/esm/entry.webpack';
import * as Styled from './styles';

export default function PDFViewer({pdf, onLoadSuccess}) {
  const [numPages, setNumPages] = useState(null);

  function onDocumentLoadSuccess({numPages}) {
    setNumPages(numPages);
    onLoadSuccess();
  }

  return (
    <Styled.PDFDocumentWrapper>
      <Document
        file={pdf}
        options={{workerSrc: '/pdf.worker.js'}}
        onLoadSuccess={onDocumentLoadSuccess}>
        {Array.from(new Array(numPages), (el, index) => (
          <Page key={`page_${index + 1}`} pageNumber={index + 1} />
        ))}
      </Document>
    </Styled.PDFDocumentWrapper>
  );
}
