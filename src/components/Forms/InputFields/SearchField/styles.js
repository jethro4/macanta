import {experimentalStyled as styled} from '@mui/material/styles';
import Box from '@mui/material/Box';

export const CustomRoot = styled(Box)`
  font-size: 14px;
  position: relative;
  z-index: 1000;
`;

export const Listbox = styled('ul')(
  ({theme}) => `
  width: calc(100% - 2rem);
  margin: 2px 0 0;
  padding: 0;
  position: absolute;
  list-style: none;
  background-color: ${theme.palette.mode === 'dark' ? '#141414' : '#fff'};
  overflow: auto;
  border-radius: 4px;
  box-shadow: 0 2px 8px rgba(0, 0, 0, 0.15);
  z-index: 1000;
`,
);
