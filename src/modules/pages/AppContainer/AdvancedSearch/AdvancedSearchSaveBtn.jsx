import React, {useState, useMemo} from 'react';
import {useFormikContext} from 'formik';
import TurnedInIcon from '@mui/icons-material/TurnedIn';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Button from '@macanta/components/Button';
import Popover from '@macanta/components/Popover';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import EmailBatchWidgetsIconBtn from '@macanta/modules/pages/AppContainer/Widgets/EmailBatchWidgetsIconBtn';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import useAdvancedSearch from '@macanta/hooks/admin/useAdvancedSearch';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import * as QueryBuilderStyled from '@macanta/modules/AdminSettings/QueryBuilder/styles';
import * as Storage from '@macanta/utils/storage';

const AdvancedSearchSaveBtn = () => {
  const userDetails = Storage.getItem('userDetails');

  const {values} = useFormikContext();

  const {displayMessage} = useProgressAlert();

  const [anchorEl, setAnchorEl] = useState(false);
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [loading, setLoading] = useState(false);
  const [saved, setSaved] = useState(false);

  const advancedSearchQuery = useAdvancedSearch(values, {
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const handleOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleSaveQuery = async () => {
    try {
      setLoading(true);

      const {data} = await advancedSearchQuery.refetch({
        shouldSave: true,
        name: name,
        description: description,
        doType: values.doType,
        chosenFields: JSON.stringify(values.chosenFields),
        doConditions: JSON.stringify(values.doConditions),
        contactConditions: JSON.stringify(values.contactConditions),
        userConditions: JSON.stringify(values.userConditions),
      });

      if (data?.listAdvancedSearch?.queryId) {
        displayMessage('Successfully saved query');

        handleClose();

        setSaved(true);
      }
    } finally {
      setLoading(false);
    }
  };

  const open = Boolean(anchorEl);

  const tempWidget = useMemo(() => {
    return {
      id: 'tempWidgetId',
      type: 'MacantaQuery',
      title: 'Advanced Search Results',
      groupName: values.doType,
      queryId: advancedSearchQuery.queryId,
      userId: userDetails?.userId,
    };
  }, [advancedSearchQuery.queryId]);

  return (
    <>
      <EmailBatchWidgetsIconBtn
        disableSelectRecipient
        variant="contained"
        size="medium"
        selectedWidgetIds={[tempWidget.id]}
        addedWidgets={tempWidget}
      />
      {saved ? (
        <Typography
          sx={{
            color: 'success.main',
          }}
          style={{
            fontWeight: 'bold',
          }}>
          Saved
        </Typography>
      ) : (
        <Button
          style={{
            marginLeft: '1rem',
          }}
          variant="contained"
          startIcon={<TurnedInIcon />}
          onClick={handleOpen}
          size="medium">
          Save As New Query
        </Button>
      )}
      <Popover
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}>
        <div
          style={{
            width: 500,
            padding: '1rem',
          }}>
          <QueryBuilderStyled.TextField
            labelPosition="normal"
            autoFocus
            required
            value={name}
            onChange={setName}
            label="Name"
            fullWidth
            size="medium"
            variant="outlined"
          />
          <QueryBuilderStyled.TextField
            labelPosition="normal"
            type="TextArea"
            hideExpand
            value={description}
            onChange={setDescription}
            label="Description"
            fullWidth
            size="medium"
            variant="outlined"
          />
          <Box
            style={{
              marginTop: '2rem',
              display: 'flex',
              justifyContent: 'flex-end',
            }}>
            <Button
              disabled={!name}
              variant="contained"
              startIcon={<TurnedInIcon />}
              onClick={handleSaveQuery}
              size="medium">
              Save Query
            </Button>
          </Box>
        </div>
      </Popover>
      <LoadingIndicator modal loading={loading} />
    </>
  );
};

export default AdvancedSearchSaveBtn;
