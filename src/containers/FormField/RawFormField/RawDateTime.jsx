import React from 'react';

const RawDateTime = (props) => {
  return <input type="datetime-local" {...props} />;
};

export default RawDateTime;
