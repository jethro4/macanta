import React from 'react';
import Typography from '@mui/material/Typography';
import * as Styled from './styles';

const SettingInfo = ({label, value, subtext, style, IconComp, RightComp}) => (
  <Styled.DetailContainer style={style}>
    {IconComp}
    {!!label && <Styled.DetailLabel>{label}:</Styled.DetailLabel>}
    <Styled.DetailValue>
      {value}
      {subtext && (
        <Typography
          component="span"
          style={{
            color: '#888',
            marginLeft: 8,
            fontSize: 'inherit',
          }}>
          {subtext}
        </Typography>
      )}
    </Styled.DetailValue>
    {RightComp && (
      <Styled.InfoRightCompContainer>{RightComp}</Styled.InfoRightCompContainer>
    )}
  </Styled.DetailContainer>
);

export default SettingInfo;
