import {gql} from '@apollo/client';

export const CONDITIONS_ACTIONS_ITEM_FRAGMENT = gql`
  fragment conditionsActionsItemFragment on ConditionsActionsItem {
    actionId
    active
    conditionId
    revalidate
    time
    unit
    waitFor
  }
`;

export const AUTOMATION_GROUP_FRAGMENT = gql`
  fragment automationGroupFragment on AutomationGroup {
    active
    conditionsActions {
      ...conditionsActionsItemFragment
    }
    description
    id
    name
  }
  ${CONDITIONS_ACTIONS_ITEM_FRAGMENT}
`;

export default gql`
  query getAutomationGroups {
    getAutomationGroups {
      ...automationGroupFragment
    }
  }
  ${AUTOMATION_GROUP_FRAGMENT}
`;
