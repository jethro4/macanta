module.exports = {
  verbose: true,
  testEnvironment: 'jsdom',
  testEnvironmentOptions: {url: 'http://staging.localhost:8000'},
  transform: {
    '^.+\\.jsx?$': '<rootDir>/jest-preprocess.js',
  },
  moduleNameMapper: {
    '.+\\.(css|styl|less|sass|scss)$': 'identity-obj-proxy',
    '.+\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/__mocks__/file-mock.js',
    '^@macanta/(.*)$': '<rootDir>/src/$1',
    '^@mui/material/(.*)$': '<rootDir>/node_modules/@material-ui/core/$1',
    '^@mui/icons-material/(.*)$':
      '<rootDir>/node_modules/@material-ui/icons/$1',
    '^@mui/x-date-pickers/(.*)$': '<rootDir>/node_modules/@material-ui/lab/$1',
    '^@tests/(.*)$': '<rootDir>/tests/$1',
  },
  testPathIgnorePatterns: [
    'node_modules',
    '\\.cache',
    '<rootDir>.*/public',
    '<rootDir>/backup',
  ],
  transformIgnorePatterns: ['node_modules/(?!(gatsby)/)'],
  globals: {
    __PATH_PREFIX__: '',
  },
  setupFiles: [
    '<rootDir>/loadershim.js',
    '<rootDir>/tests/jestSettings/setEnvVars.js',
  ],
  setupFilesAfterEnv: [
    '<rootDir>/tests/jestSettings/setupMocks.js',
    '<rootDir>/tests/jestSettings/setupTests.js',
  ],
};
