import React, {useState} from 'react';
import map from 'lodash/map';
import AddIcon from '@mui/icons-material/Add';
import {useTheme} from '@mui/material/styles';
import {PopoverButton} from '@macanta/components/Button';
import Section from '@macanta/containers/Section';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import * as ListStyled from '@macanta/containers/List/styles';
import * as Styled from './styles';
import Modal from '@macanta/components/Modal';
import ConnectDORelationshipForm from '@macanta/modules/DataObjectItems/DOItemForms/Relationships/ConnectDORelationshipForm';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import useValue from '@macanta/hooks/base/useValue';
import DORelationshipItems from '@macanta/modules/DataObjectItems/DOItemForms/Relationships/DORelationshipItems';
import Delay from '@macanta/components/Delay';

const DODirectRelationshipsForm = ({
  loading,
  filteredDirectItems,
  groupedData,
  doItemId,
  onConnect,
  onDelete,
  ...props
}) => {
  const theme = useTheme();

  const [selectedDOType, setSelectedDOType] = useState(null);

  const doTypesQuery = useDOTypes({
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const showModal = !!selectedDOType;
  const doTypes = doTypesQuery.data?.listDataObjectTypes;

  const [hiddenIds] = useValue(() => {
    return [doItemId].concat(map(filteredDirectItems, 'doItemId'));
  }, [filteredDirectItems]);

  const handleOpenDOSearch = (doType) => {
    setSelectedDOType(doType);
  };

  const handleCloseDOSearch = () => {
    setSelectedDOType(null);
  };

  const handleConnect = (items) => {
    handleCloseDOSearch();

    onConnect(items);
  };

  const handleDelete = (deletedItem) => {
    onDelete(deletedItem);
  };

  return (
    <Section
      data-testid="DODirectRelationshipsForm"
      loading={loading}
      fullHeight
      title="Direct DO Relationships"
      HeaderRightComp={
        <PopoverButton
          size="small"
          variant="contained"
          startIcon={<AddIcon />}
          PopoverProps={{
            contentMinWidth: 180,
            // bodyStyle: {
            //   backgroundColor: '#f5f6fA',
            // },
          }}
          renderContent={(handleClose) => (
            <List
              data-testid="DOTypesList"
              style={{
                padding: 0,
              }}>
              {doTypes?.map((d) => (
                <>
                  <NoteTaskHistoryStyled.SelectionItem
                    button
                    onClick={() => {
                      handleOpenDOSearch(d);
                      handleClose();
                    }}>
                    <AddIcon />
                    <NoteTaskHistoryStyled.SelectionItemText
                      primary={d.title}
                    />
                  </NoteTaskHistoryStyled.SelectionItem>
                  <Divider />
                </>
              ))}
            </List>
          )}>
          Connect DO
        </PopoverButton>
      }
      {...props}
      bodyStyle={{
        padding: 0,
        backgroundColor: theme.palette.grayLight.main,
        ...props.bodyStyle,
      }}
      style={{
        marginTop: 0,
        ...props.style,
      }}>
      {!loading && filteredDirectItems.length === 0 && (
        <Delay>
          <ListStyled.EmptyMessage align="center" color="#888">
            No direct relationships
          </ListStyled.EmptyMessage>
        </Delay>
      )}
      {groupedData?.map((gd) => {
        const doType = doTypes?.find((d) => d.id == gd.groupId);

        return (
          <Styled.ItemContainer key={gd.groupId}>
            <Styled.ItemHeader>{doType?.title}</Styled.ItemHeader>
            <Styled.ItemBody className={`direct-${gd.groupId}`}>
              <DORelationshipItems
                items={gd.items}
                doTitle={doType?.title}
                onDelete={handleDelete}
              />
            </Styled.ItemBody>
          </Styled.ItemContainer>
        );
      })}

      <Modal
        headerTitle={`Search and Connect Objects (${selectedDOType?.title})`}
        open={showModal}
        onClose={handleCloseDOSearch}
        contentWidth={'100%'}
        contentHeight={'94%'}>
        <ConnectDORelationshipForm
          disabledIds={hiddenIds}
          groupId={selectedDOType?.id}
          doTitle={selectedDOType?.title}
          onConnect={handleConnect}
        />
      </Modal>
    </Section>
  );
};

DODirectRelationshipsForm.defaultProps = {
  data: [],
};

export default DODirectRelationshipsForm;
