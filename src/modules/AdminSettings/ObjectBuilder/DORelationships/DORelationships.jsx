import React, {useMemo} from 'react';
import {useFormikContext} from 'formik';
import Box from '@mui/material/Box';
import useNextEffect from '@macanta/hooks/base/useNextEffect';
import useDORelationships from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDORelationships';
import {mergeAndAddArraysBySameKeys} from '@macanta/utils/array';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {AUTO_ASSIGN_OPTIONS} from './DORelationshipForms';
import DOAllRelationshipsSection from './DOAllRelationshipsSection';
import DORelationshipsSection from './DORelationshipsSection';

const DORelationships = () => {
  const {values, setValues, validateField} = useFormikContext();

  const relationshipsQuery = useDORelationships(values.id, {
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const relationships = relationshipsQuery.data?.listRelationships;

  const handleChange = (val, allRelationships) => {
    setValues((state) => ({
      ...state,
      relationships: mergeAndAddArraysBySameKeys(
        relationships?.map((d) => ({
          ...d,
          autoAssignOptions: AUTO_ASSIGN_OPTIONS.filter(
            (o) => !!d[o.value],
          ).map((r) => r.value),
        })),
        allRelationships,
        {
          keysToCheck: ['id'],
          keysToMerge: ['role'],
        },
      ).filter((d) => val.includes(d.id)),
    }));
  };

  useNextEffect(() => {
    validateField('relationships');
  }, [values.relationships]);

  const selectedRelationships = useMemo(() => {
    return values.relationships.map(({id}) => id);
  }, [values.relationships]);

  return (
    <Box
      style={{
        display: 'flex',
        height: '100%',
      }}>
      <DOAllRelationshipsSection
        selectedRelationships={selectedRelationships}
        onChange={handleChange}
        style={{
          marginTop: '1rem',
          marginBottom: '1rem',
          width: 300,
          overflowY: 'hidden',
        }}
      />
      <DORelationshipsSection
        style={{
          marginTop: '1rem',
          marginBottom: '1rem',
          flex: 1,
        }}
      />
    </Box>
  );
};

export default DORelationships;
