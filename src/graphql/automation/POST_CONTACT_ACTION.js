import {gql} from '@apollo/client';
import {CONTACT_ACTION_FRAGMENT} from '@macanta/graphql/automation/GET_CONTACT_ACTIONS';

export default gql`
  mutation postContactAction($input: ContactActionInput!) {
    postContactAction(input: $input) {
      ...contactActionFragment
    }
  }
  ${CONTACT_ACTION_FRAGMENT}
`;
