import {
  getSectionsData,
  convertWithSections,
  transformSectionsData,
} from '@macanta/selectors/field.selector';

describe('DO Item Details Data Transformer', () => {
  let itemData;

  const initialValues = [
    {
      id: 'field_kcylb25x',
      name: 'Project',
      type: 'Text',
      value: 'Macanta Apple Watch',
      subGroup: '',
      sectionTag: '',
    },
    {
      id: 'field_keljzr6m',
      name: 'Logged Hours',
      type: 'Number',
      value: '36',
      subGroup: 'Section details 1',
      sectionTag: 'Test Section 1',
    },
    {
      id: 'field_kejv78th',
      name: 'Level',
      type: 'Text',
      value: 'Senior',
      subGroup: 'Section details 1',
      sectionTag: '',
    },
    {
      id: 'field_kelg4y6m',
      name: 'Preferred Libraries',
      type: 'Text',
      value: 'Redux Toolkit',
      subGroup: '',
      sectionTag: '',
    },
    {
      id: 'field_kewffahy',
      name: 'Is Contact App Owner?',
      type: 'Text',
      value: 'Nope',
      subGroup: 'Section details 1',
      sectionTag: 'Test Section 1',
    },
    {
      id: 'field_kg22lshb',
      name: 'Test Field with Section',
      type: 'Text',
      value: 'test',
      subGroup: 'Section details 1',
      sectionTag: 'Test Section 2',
    },
    {
      id: 'field_kg22y8yk',
      name: 'Test Field 2',
      type: 'Text',
      value: '',
      subGroup: 'Section details 2',
      sectionTag: 'Test Section 2',
    },
  ];

  beforeEach(() => {
    itemData = initialValues;
  });

  it('happy - convert DO item data with sections and subGroups as object', () => {
    const actualResult = convertWithSections(itemData);
    const expectedResult = {
      General: {
        subGroups: {
          Overview: initialValues.filter(
            (item) => !item.sectionTag && !item.subGroup,
          ),
          'Section details 1': initialValues.filter(
            (item) => !item.sectionTag && item.subGroup === 'Section details 1',
          ),
        },
      },
      'Test Section 1': {
        subGroups: {
          'Section details 1': initialValues.filter(
            (item) =>
              item.sectionTag === 'Test Section 1' &&
              item.subGroup === 'Section details 1',
          ),
        },
      },
      'Test Section 2': {
        subGroups: {
          'Section details 1': initialValues.filter(
            (item) =>
              item.sectionTag === 'Test Section 2' &&
              item.subGroup === 'Section details 1',
          ),
          'Section details 2': initialValues.filter(
            (item) =>
              item.sectionTag === 'Test Section 2' &&
              item.subGroup === 'Section details 2',
          ),
        },
      },
    };

    expect(actualResult).toEqual(expectedResult);
  });

  it('happy - transform DO item data as sections and subGroups as array', () => {
    const sectionsObj = convertWithSections(itemData);
    const actualResult = transformSectionsData(sectionsObj);
    const expectedResult = [
      {
        sectionName: 'General',
        subGroups: [
          {
            subGroupName: 'Overview',
            data: [
              {
                id: 'field_kcylb25x',
                name: 'Project',
                type: 'Text',
                value: 'Macanta Apple Watch',
                subGroup: '',
                sectionTag: '',
              },
              {
                id: 'field_kelg4y6m',
                name: 'Preferred Libraries',
                type: 'Text',
                value: 'Redux Toolkit',
                subGroup: '',
                sectionTag: '',
              },
            ],
          },
          {
            subGroupName: 'Section details 1',
            data: [
              {
                id: 'field_kejv78th',
                name: 'Level',
                type: 'Text',
                value: 'Senior',
                subGroup: 'Section details 1',
                sectionTag: '',
              },
            ],
          },
        ],
      },
      {
        sectionName: 'Test Section 1',
        subGroups: [
          {
            subGroupName: 'Section details 1',
            data: [
              {
                id: 'field_keljzr6m',
                name: 'Logged Hours',
                type: 'Number',
                value: '36',
                subGroup: 'Section details 1',
                sectionTag: 'Test Section 1',
              },
              {
                id: 'field_kewffahy',
                name: 'Is Contact App Owner?',
                type: 'Text',
                value: 'Nope',
                subGroup: 'Section details 1',
                sectionTag: 'Test Section 1',
              },
            ],
          },
        ],
      },
      {
        sectionName: 'Test Section 2',
        subGroups: [
          {
            subGroupName: 'Section details 1',
            data: [
              {
                id: 'field_kg22lshb',
                name: 'Test Field with Section',
                type: 'Text',
                value: 'test',
                subGroup: 'Section details 1',
                sectionTag: 'Test Section 2',
              },
            ],
          },
          {
            subGroupName: 'Section details 2',
            data: [
              {
                id: 'field_kg22y8yk',
                name: 'Test Field 2',
                type: 'Text',
                value: '',
                subGroup: 'Section details 2',
                sectionTag: 'Test Section 2',
              },
            ],
          },
        ],
      },
    ];

    expect(actualResult).toEqual(expectedResult);
  });

  it('should display default value if no value is present', () => {
    itemData = [
      {
        id: 'field_keljzr6m',
        name: 'Logged Hours',
        type: 'Number',
        default: '8',
        subGroup: 'Section details 1',
        sectionTag: 'Test Section 1',
      },
    ];
    const sectionsObj = convertWithSections(itemData);
    const actualResult = transformSectionsData(sectionsObj);
    const expectedResult = [
      {
        sectionName: 'Test Section 1',
        subGroups: [
          {
            subGroupName: 'Section details 1',
            data: [
              {
                id: 'field_keljzr6m',
                name: 'Logged Hours',
                type: 'Number',
                default: '8',
                value: '8',
                subGroup: 'Section details 1',
                sectionTag: 'Test Section 1',
              },
            ],
          },
        ],
      },
    ];

    expect(actualResult).toEqual(expectedResult);
  });

  it('happy - should whitelist sections', () => {
    const actualResult = getSectionsData(itemData, [
      'General',
      'Test Section 1',
    ]);
    const expectedResult = [
      {
        sectionName: 'General',
        subGroups: [
          {
            subGroupName: 'Overview',
            data: [
              {
                id: 'field_kcylb25x',
                name: 'Project',
                type: 'Text',
                value: 'Macanta Apple Watch',
                subGroup: '',
                sectionTag: '',
              },
              {
                id: 'field_kelg4y6m',
                name: 'Preferred Libraries',
                type: 'Text',
                value: 'Redux Toolkit',
                subGroup: '',
                sectionTag: '',
              },
            ],
          },
          {
            subGroupName: 'Section details 1',
            data: [
              {
                id: 'field_kejv78th',
                name: 'Level',
                type: 'Text',
                value: 'Senior',
                subGroup: 'Section details 1',
                sectionTag: '',
              },
            ],
          },
        ],
      },
      {
        sectionName: 'Test Section 1',
        subGroups: [
          {
            subGroupName: 'Section details 1',
            data: [
              {
                id: 'field_keljzr6m',
                name: 'Logged Hours',
                type: 'Number',
                value: '36',
                subGroup: 'Section details 1',
                sectionTag: 'Test Section 1',
              },
              {
                id: 'field_kewffahy',
                name: 'Is Contact App Owner?',
                type: 'Text',
                value: 'Nope',
                subGroup: 'Section details 1',
                sectionTag: 'Test Section 1',
              },
            ],
          },
        ],
      },
    ];

    expect(actualResult).toEqual(expectedResult);
  });

  it('happy - should whitelist all sections if whitelistedSections array is empty', () => {
    const actualResult = getSectionsData(itemData, []);
    const expectedResult = [
      {
        sectionName: 'General',
        subGroups: [
          {
            subGroupName: 'Overview',
            data: [
              {
                id: 'field_kcylb25x',
                name: 'Project',
                type: 'Text',
                value: 'Macanta Apple Watch',
                subGroup: '',
                sectionTag: '',
              },
              {
                id: 'field_kelg4y6m',
                name: 'Preferred Libraries',
                type: 'Text',
                value: 'Redux Toolkit',
                subGroup: '',
                sectionTag: '',
              },
            ],
          },
          {
            subGroupName: 'Section details 1',
            data: [
              {
                id: 'field_kejv78th',
                name: 'Level',
                type: 'Text',
                value: 'Senior',
                subGroup: 'Section details 1',
                sectionTag: '',
              },
            ],
          },
        ],
      },
      {
        sectionName: 'Test Section 1',
        subGroups: [
          {
            subGroupName: 'Section details 1',
            data: [
              {
                id: 'field_keljzr6m',
                name: 'Logged Hours',
                type: 'Number',
                value: '36',
                subGroup: 'Section details 1',
                sectionTag: 'Test Section 1',
              },
              {
                id: 'field_kewffahy',
                name: 'Is Contact App Owner?',
                type: 'Text',
                value: 'Nope',
                subGroup: 'Section details 1',
                sectionTag: 'Test Section 1',
              },
            ],
          },
        ],
      },
      {
        sectionName: 'Test Section 2',
        subGroups: [
          {
            subGroupName: 'Section details 1',
            data: [
              {
                id: 'field_kg22lshb',
                name: 'Test Field with Section',
                type: 'Text',
                value: 'test',
                subGroup: 'Section details 1',
                sectionTag: 'Test Section 2',
              },
            ],
          },
          {
            subGroupName: 'Section details 2',
            data: [
              {
                id: 'field_kg22y8yk',
                name: 'Test Field 2',
                type: 'Text',
                value: '',
                subGroup: 'Section details 2',
                sectionTag: 'Test Section 2',
              },
            ],
          },
        ],
      },
    ];

    expect(actualResult).toEqual(expectedResult);
  });
});
