import React, {useState, useEffect, useLayoutEffect} from 'react';
import contactValidationSchema from '@macanta/validations/contact';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import TurnedInIcon from '@mui/icons-material/TurnedIn';
import ButtonGroup from '@macanta/components/ButtonGroup';
import Form from '@macanta/components/Form';
import Button from '@macanta/components/Button';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import Show from '@macanta/containers/Show';
import {getSectionsData} from '@macanta/selectors/field.selector';
import FormsGeneral from './FormsGeneral';
import FormsAdditionalFields from './FormsAdditionalFields';
import useContactFields from '@macanta/modules/ContactDetails/useContactFields';
import DOSectionGuideBtn from '@macanta/modules/DataObjectItems/DOItemForms/FieldForms/DOSectionGuideBtn';
import usePrevious from '@macanta/hooks/usePrevious';
import {filterAndTransformFieldValues} from '@macanta/selectors/dataObject.selector';
import useIsAdmin from '@macanta/hooks/useIsAdmin';
import {writeQueryItem} from '@macanta/hooks/contact/useContact';
import {ACCESS_LEVELS} from '@macanta/constants/accessLevels';
import {useMutation} from '@apollo/client';
import {
  CREATE_OR_UPDATE_CONTACT,
  LIST_CONTACTS,
} from '@macanta/graphql/contacts';
import * as Styled from './styles';
import * as FormStyled from '@macanta/modules/NoteTaskForms/styles';
import * as Storage from '@macanta/utils/storage';

const ContactForms = (props) => {
  const {onSuccess, contact = {}} = props;

  const isEdit = !!contact?.id;

  const [selectedTab, setSelectedTab] = useState('Basic Info');
  const [sectionsData, setSectionsData] = useState([]);
  const [sectionNames, setSectionNames] = useState(['Basic Info']);

  const isAdmin = useIsAdmin();

  const getHasDefaultAccess = (id) => {
    const {userId: loggedInUserId} = Storage.getItem('userDetails');

    const isLoggedInUser = loggedInUserId === id;

    return isAdmin || isLoggedInUser;
  };

  const contactFieldsQuery = useContactFields(
    contact.email,
    'contactCustomFields',
    {
      removeEmail: !contact.id,
    },
  );

  const contactFields = contactFieldsQuery.data?.listDataObjectFields;
  const hasDefaultAccess = getHasDefaultAccess(contact.id);
  const isReadOnly =
    !!contact.id &&
    !hasDefaultAccess &&
    ACCESS_LEVELS.READ_WRITE !== contact.loggedInUserPermission;

  const getInitValues = () => {
    return {
      ...contact,
      ...contact.phoneNumbers?.reduce((acc, num = '', index) => {
        const obj = {...acc};

        obj[`phone${index}`] = num;

        return obj;
      }, {}),
      isTempEmail: false,
    };
  };

  const [initValues, setInitValues] = useState(() => getInitValues());
  const prevInitValues = usePrevious(initValues);

  const [callContactMutation, contactMutation] = useMutation(
    CREATE_OR_UPDATE_CONTACT,
    {
      update(cache, {data: {createOrUpdateContact}}) {
        cache.modify({
          fields: {
            listContacts(listContactsRef) {
              cache.modify({
                id: listContactsRef.__ref,
                fields: {
                  items(existingItems = []) {
                    const newItemRef = cache.writeQuery({
                      data: createOrUpdateContact,
                      query: LIST_CONTACTS,
                    });

                    return [newItemRef, ...existingItems];
                  },
                },
              });
            },
          },
        });

        writeQueryItem(createOrUpdateContact);
      },
      onCompleted(data) {
        const userDetails = Storage.getItem('userDetails');

        if (data.createOrUpdateContact?.id === userDetails.userId) {
          Storage.setItem('userDetails', {
            ...userDetails,
            firstName: data.createOrUpdateContact.firstName,
            lastName: data.createOrUpdateContact.lastName,
          });
        }
      },
      onError() {},
    },
  );

  const handleSelectTab = (tab) => () => {
    setSelectedTab(tab);
  };

  const handleFormSubmit = (values) => {
    const customFieldsObj = values.customFields?.reduce((acc, item) => {
      acc[item.name] = item.value;

      return acc;
    }, {});

    const transformedCustomFields = filterAndTransformFieldValues(
      customFieldsObj,
      contactFields,
    );

    const newContact = {
      id: values.id,
      autoGenerateEmail: values.isTempEmail,
      email: !values.isTempEmail && values.email ? values.email.trim() : '',
      email2: !values.isTempEmail ? values.email2 : '',
      email3: !values.isTempEmail ? values.email3 : '',
      firstName: values.firstName,
      middleName: values.middleName,
      lastName: values.lastName,
      birthday: values.birthday,
      title: values.title,
      company: values.company,
      website: values.website,
      jobTitle: values.jobTitle,
      phoneNumbers: [],
      streetAddress1: values.streetAddress1,
      streetAddress2: values.streetAddress2,
      city: values.city,
      state: values.state,
      postalCode: values.postalCode,
      country: values.country,
      address2Street1: values.address2Street1,
      address2Street2: values.address2Street2,
      city2: values.city2,
      state2: values.state2,
      postalCode2: values.postalCode2,
      country2: values.country2,
      fax1: values.fax1,
      fax2: values.fax2,
      leadSource: values.leadSource,
      timeZone: values.timeZone,
      phoneNumberExts: values.phoneNumberExts,
      pdfDownloadLink: values.pdfDownloadLink,
      userKeyId: values.userKeyId,
      primaryOffice: values.primaryOffice,
      customFields: Object.entries(transformedCustomFields)
        ?.filter(([name]) => name !== 'LastUpdated' && name !== 'LastUpdatedBy')
        .map(([name, value]) => ({
          name,
          value,
        })),
    };

    for (let i = 0; i < 3; i++) {
      if (values[`phone${i}`]) {
        newContact.phoneNumbers.push(values[`phone${i}`]);
      }
    }

    callContactMutation({
      variables: {
        createOrUpdateContactInput: newContact,
        __mutationkey: 'createOrUpdateContactInput',
      },
    });
  };

  useEffect(() => {
    if (contactMutation.data?.createOrUpdateContact) {
      onSuccess(contactMutation.data?.createOrUpdateContact);
    }
  }, [contactMutation.data]);

  useEffect(() => {
    if (contact && prevInitValues === initValues) {
      setInitValues(getInitValues());
    }
  }, [contact]);

  useLayoutEffect(() => {
    if (contactFields) {
      const filteredFields = contactFields.filter(
        (field) =>
          field.name !== 'LastUpdated' && field.name !== 'LastUpdatedBy',
      );

      const allSections = getSectionsData(filteredFields);
      const allSectionNames = allSections.map((section) => section.sectionName);

      setSectionsData(allSections);
      setSectionNames(['Basic Info', ...allSectionNames]);
    }
  }, [contactFields]);

  const loading = !contactFields && contactFieldsQuery.loading;

  return (
    <Styled.Root {...props}>
      {!!contactMutation.error && (
        <Typography
          color="error"
          style={{
            fontSize: '0.75rem',
            textAlign: 'center',
            marginTop: '1rem',
          }}>
          {contactMutation.error.message}
        </Typography>
      )}
      <Form
        initialValues={initValues}
        enableReinitialize
        validationSchema={contactValidationSchema({initValues, isEdit})}
        onSubmit={handleFormSubmit}>
        {({dirty, isValidating, handleSubmit}) => {
          return (
            <>
              <Styled.FormsContainer>
                <Box
                  style={{
                    margin: '1rem 1rem 0',
                    display: 'flex',
                    alignItems: 'center',
                  }}>
                  <ButtonGroup
                    increaseBtnMinWidth
                    size="small"
                    variant="outlined"
                    aria-label="primary button group">
                    {sectionNames.map((sectionName, index) => {
                      return (
                        <Button
                          key={index}
                          onClick={handleSelectTab(sectionName)}
                          endIcon={
                            <DOSectionGuideBtn sectionName={sectionName} />
                          }>
                          {sectionName}
                        </Button>
                      );
                    })}
                  </ButtonGroup>
                  <LoadingIndicator
                    style={{
                      marginLeft: '1rem',
                    }}
                    transparent
                    size={20}
                    loading={loading}
                  />
                </Box>

                <Show removeHidden in={selectedTab === 'Basic Info'}>
                  <FormsGeneral readOnly={isReadOnly} contact={contact} />
                </Show>
                {sectionsData.map((section) => {
                  return (
                    <Show
                      key={section.sectionName}
                      removeHidden
                      in={selectedTab === section.sectionName}>
                      <FormsAdditionalFields
                        readOnly={isReadOnly}
                        subGroups={section.subGroups}
                        loading={contactFieldsQuery.loading}
                      />
                    </Show>
                  );
                })}
              </Styled.FormsContainer>

              {!isReadOnly && (
                <FormStyled.Footer
                  style={{
                    padding: '1rem',
                    margin: 0,
                    borderTop: '1px solid #eee',
                    boxShadow: '0px -4px 10px -2px #eee',
                  }}>
                  <FormStyled.FooterButton
                    disabled={!dirty || isValidating}
                    variant="contained"
                    startIcon={<TurnedInIcon />}
                    onClick={handleSubmit}
                    size="medium">
                    Save Contact
                  </FormStyled.FooterButton>
                </FormStyled.Footer>
              )}
            </>
          );
        }}
      </Form>
      <LoadingIndicator modal loading={contactMutation.loading} />
    </Styled.Root>
  );
};

export default ContactForms;
