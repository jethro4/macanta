import React, {useEffect, useMemo} from 'react';
import {useFormikContext} from 'formik';
import DownloadIcon from '@mui/icons-material/Download';
import FileUploadIcon from '@mui/icons-material/FileUpload';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import Button from '@macanta/components/Button';
import Form from '@macanta/components/Form';
import Section from '@macanta/containers/Section';
import FormField from '@macanta/containers/FormField';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import useDOFields from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOFields';
import useDORelationships from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDORelationships';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import useAppSettings from '@macanta/hooks/admin/useAppSettings';
import {useMutation} from '@apollo/client';
import {IMPORT_DATA} from '@macanta/graphql/admin';
import dataImportsValidationSchema from '@macanta/validations/dataImports';
import UploadForms from '@macanta/modules/DataObjectItems/DOItemForms/Attachments/UploadForms';
import {getMIMEType} from '@macanta/utils/file';
import {convertSpaceToUnderscore} from '@macanta/modules/NoteTaskForms/AddTagsField';
import {GET_APP_SETTINGS, UPDATE_APP_SETTINGS} from '@macanta/graphql/app';
import {
  CONTACT_DEFAULT_FIELDS,
  CONTACT_DEFAULT_ADDITIONAL_FIELDS,
} from '@macanta/constants/contactFields';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {sortArrayByKeyValuesIncludeAll} from '@macanta/utils/array';
import * as NoteTaskFormsStyled from '@macanta/modules/NoteTaskForms/styles';
import * as FormFieldStyled from '@macanta/containers/FormField/styles';

const AUTOMATION_STATUS = [
  {
    label: 'Active',
    value: 'active',
  },
  {
    label: 'Inactive',
    value: 'inactive',
  },
];

const generateAndDownloadFileUrl = async (content = '', fileName, fileExt) => {
  const element = document.createElement('a');
  const formattedFileName = `${convertSpaceToUnderscore(fileName)}.${fileExt}`;
  const mimeType = getMIMEType(fileExt);
  const file = new File([content], formattedFileName, {
    type: mimeType,
  });
  const objectUrl = URL.createObjectURL(file);

  element.href = objectUrl;
  element.download = formattedFileName;

  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);

  URL.revokeObjectURL(objectUrl);
};

const transformCSVDefaultValue = (field, is2ndData) => {
  let value = '';

  if (is2ndData) {
    switch (field.type) {
      case 'Select':
      case 'Radio':
      case 'Checkbox': {
        value = field.choices[0];

        break;
      }
      case 'Date': {
        value = '2021-11-28';

        break;
      }
      case 'DateTime': {
        value = '2021-11-28 02:05PM';

        break;
      }
      case 'URL': {
        value = `samplelink.org`;

        break;
      }
      case 'Number':
      case 'Currency': {
        value = '0';

        break;
      }
    }

    value = field.default || value;
  }

  return value ? `"${value}"` : '';
};

const ImportSectionContainer = () => {
  const {displayMessage} = useProgressAlert();

  const doTypesQuery = useDOTypes({
    removeEmail: true,
  });
  const appSettingsQuery = useAppSettings({
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const doTypes = doTypesQuery.data?.listDataObjectTypes;
  const enableCDImport = appSettingsQuery.data?.enableCDImport;

  const [callImportData, importDataMutation] = useMutation(IMPORT_DATA, {
    onError() {},
  });
  const [callUpdateAppSettings] = useMutation(UPDATE_APP_SETTINGS, {
    onError() {},
    onCompleted: async (data) => {
      if (data?.updateAppSettings?.success) {
        displayMessage('Saved successfully!');
      }
    },
  });

  const handleFormSubmit = async (values, actions) => {
    const {dataType, attachments, fieldName, automationStatus} = values;
    const {fileName, thumbnail} = attachments[0];

    const {data} = await callImportData({
      variables: {
        importDataInput: {
          dataType,
          fileName,
          attachment: thumbnail.split(',')[1],
          ...(dataType !== 'Contacts' && {
            fieldName,
            automationStatus,
          }),
        },
        __mutationkey: 'importDataInput',
        removeAppInfo: true,
      },
    });

    if (data?.importData?.success) {
      displayMessage(
        'Your file was processed successfully.\nWe will send you an email containing the result details.',
      );

      actions.resetForm({
        values: {attachments: [], automationStatus: 'active'},
      });

      const {data: updateData} = await callUpdateAppSettings({
        variables: {
          updateAppSettingsInput: {
            type: 'EnableCDImport',
            value: 'yes',
          },
          __mutationkey: 'updateAppSettingsInput',
        },
      });

      if (updateData?.updateAppSettings?.success) {
        appSettingsQuery.client.writeQuery({
          query: GET_APP_SETTINGS,
          data: {
            getAppSettings: {
              __typename: 'AppSettings',
              ...appSettingsQuery.data,
              enableCDImport: true,
            },
          },
        });
      }
    }
  };

  const types = useMemo(() => {
    if (doTypes) {
      return [
        {
          label: 'Contacts',
          value: 'Contacts',
        },
        {
          isSubheader: true,
          label: 'Data Object Types',
        },
      ].concat(
        doTypes.map((doType) => ({
          label: doType.title,
          value: doType.title,
          id: doType.id,
          disabled: !enableCDImport,
        })),
      );
    }

    return [];
  }, [doTypes, enableCDImport]);

  const initLoading = !doTypes && doTypesQuery.loading;

  return (
    <>
      <Form
        initialValues={{attachments: [], automationStatus: 'active'}}
        validationSchema={dataImportsValidationSchema}
        onSubmit={handleFormSubmit}>
        <ImportSection types={types} initLoading={initLoading} />
      </Form>
      <LoadingIndicator modal loading={importDataMutation.loading} />
    </>
  );
};

const ImportSection = ({types, initLoading}) => {
  const {
    values,
    errors,
    handleChange,
    setFieldValue,
    setFieldError,
    setValues,
    setErrors,
    handleSubmit,
    validateField,
    dirty,
  } = useFormikContext();

  const isContactsDataType = values.dataType === 'Contacts';
  const doType = types.find(
    (type) => type.value === values.dataType && !isContactsDataType,
  );
  const groupId = !isContactsDataType ? doType?.id : 'contactCustomFields';

  const allFieldsQuery = useDOFields(groupId, {
    removeEmail: true,
  });
  const relationshipsQuery = useDORelationships(groupId);

  const allFields = allFieldsQuery.data?.listDataObjectFields;
  const relationships = relationshipsQuery.data?.listRelationships;

  const handleDownloadTemplateFile = () => {
    let dataType;
    let csvHeader;
    let csvData;

    if (isContactsDataType) {
      const columnsOrder = [
        'Email',
        'EmailAddress2',
        'EmailAddress3',
        'FirstName',
        'LastName',
        'Phone1',
        'Phone2',
        'Phone3',
        'Company',
        'StreetAddress1',
        'StreetAddress2',
        'City',
        'State',
        'PostalCode',
        'Country',
        'Birthday',
        'Title',
        'JobTitle',
        'Website',
        'Phone1Ext',
        'Phone2Ext',
        'Phone3Ext',
        'Address2Street1',
        'Address2Street2',
        'City2',
        'State2',
        'Country2',
        'PostalCode2',
      ];
      const sortedContactFields = sortArrayByKeyValuesIncludeAll(
        [
          ...CONTACT_DEFAULT_FIELDS,
          ...CONTACT_DEFAULT_ADDITIONAL_FIELDS,
          ...allFields,
        ],
        'name',
        columnsOrder,
      );

      dataType = 'Contacts';
      csvHeader = [sortedContactFields.map((field) => `"${field.name}"`)];
      const sampleContactValues = [
        'sample2@macanta.org',
        '',
        '',
        'Pieter',
        'de Villiers',
        '01932977975',
        '',
        '',
        'Blue Peg',
        '5 Cobham Road',
        'Fetcham',
        'Leatherhead',
        'Surrey',
        'KT22 9AU',
        'England',
        '1985-01-20',
      ];
      csvData = [
        [
          'sample1@macanta.org',
          ...sortedContactFields
            .slice(1)
            .map((field) => transformCSVDefaultValue(field)),
        ],
        [
          ...sampleContactValues,
          ...sortedContactFields
            .slice(sampleContactValues.length)
            .map((field) => transformCSVDefaultValue(field, true)),
        ],
      ];
    } else {
      dataType = values.dataType;
      csvHeader = [
        'Contact Email',
        'Contact Relationship',
        ...allFields.map((field) => `"${field.name}"`),
      ];
      csvData = [
        [
          'sample1@macanta.org',
          relationships
            .map((rel) => rel.role)
            .slice(0, 2)
            .join('; '),
          ...allFields.map((field) => transformCSVDefaultValue(field)),
        ],
        [
          'sample2@macanta.org',
          relationships
            .map((rel) => rel.role)
            .slice(0, 2)
            .join('; '),
          ...allFields.map((field) => transformCSVDefaultValue(field, true)),
        ],
      ];
    }

    const csvArr = [csvHeader, ...csvData];
    const csvString = csvArr.map((item) => item.join(',')).join('\r\n');

    generateAndDownloadFileUrl(
      csvString,
      `Sample_SpreadSheet_${dataType}`,
      'csv',
    );
  };

  const handleChangeAttachment = (attachments) => {
    setFieldValue('attachments', attachments);

    validateField('attachments');
  };

  const handleRenderValue = (choices) => (val) => {
    const selected = choices?.find(({value}) => value === val);

    return selected?.label;
  };

  const handleReset = () => {
    setValues({attachments: [], automationStatus: 'active'});
    setErrors({});
  };

  const handleResetWarnings = () => {
    setFieldValue('nonSupportedFieldsWarning', '');
  };

  const handleDeleteAttachment = () => () => {
    handleResetWarnings();

    setFieldValue('attachments', []);

    setFieldError('attachments', '');
  };

  useEffect(() => {
    if (values.dataType) {
      setFieldValue('fieldName', '');
    }
  }, [values.dataType]);

  useEffect(() => {
    if (allFields) {
      if (isContactsDataType) {
        setFieldValue('requiredFields', ['FirstName', 'Email']);
        setFieldValue(
          'fieldNames',
          [
            ...CONTACT_DEFAULT_FIELDS,
            ...CONTACT_DEFAULT_ADDITIONAL_FIELDS,
            ...allFields,
          ].map((field) => field.name),
        );
      } else {
        const requiredFields = allFields.reduce((acc, item) => {
          if (item.required) {
            return acc.concat(item.name);
          }

          return acc;
        }, []);

        setFieldValue('requiredFields', requiredFields);
        setFieldValue(
          'fieldNames',
          allFields.map((field) => field.name),
        );
      }
    }

    if (values.dataType && values.attachments.length) {
      handleResetWarnings();

      setTimeout(() => validateField('attachments'), 0);
    }
  }, [values.dataType, allFields]);

  const fields = useMemo(() => {
    if (allFields) {
      return allFields.map((field) => ({
        label: field.name,
        value: field.name,
      }));
    }

    return [];
  }, [allFields]);

  const loading =
    initLoading ||
    ((!allFields || !relationships) &&
      (allFieldsQuery.loading || relationshipsQuery.loading));

  return (
    <Section
      loading={loading}
      fullHeight
      style={{
        marginTop: 0,
      }}
      headerStyle={{
        backgroundColor: 'white',
      }}
      title="Import Data"
      FooterComp={
        <NoteTaskFormsStyled.Footer
          style={{
            padding: '1rem',
            margin: 0,
            width: '100%',
            justifyContent: 'space-between',
          }}>
          <Button
            disabled={!dirty}
            variant="outlined"
            onClick={handleReset}
            size="medium">
            Reset
          </Button>

          <Button
            disabled={!dirty || loading}
            variant="contained"
            startIcon={<FileUploadIcon />}
            onClick={handleSubmit}
            size="medium">
            Import
          </Button>
        </NoteTaskFormsStyled.Footer>
      }>
      <Box
        style={{
          display: 'flex',
          flexDirection: 'column',
          padding: '1rem 1rem 0',
        }}>
        <FormField
          required
          style={{
            marginBottom: 8,
          }}
          type="Select"
          options={types}
          value={values.dataType}
          error={errors.dataType}
          onChange={handleChange('dataType')}
          label="Data Import Type"
          fullWidth
          size="small"
          variant="outlined"
          renderValue={handleRenderValue(types)}
        />

        <Button
          style={{
            alignSelf: 'flex-end',
          }}
          variant="text"
          disabled={!values.dataType || loading}
          color="info"
          startIcon={<DownloadIcon />}
          onClick={handleDownloadTemplateFile}
          size="small">
          Download Template File
          {values.dataType ? ` (${values.dataType})` : ''}
        </Button>

        {values.dataType && !isContactsDataType && (
          <>
            <FormFieldStyled.Divider
              style={{
                margin: '1rem 0',
              }}
            />

            <FormField
              style={{
                marginBottom: '1rem',
              }}
              type="Select"
              options={fields}
              value={values.fieldName}
              error={errors.fieldName}
              onChange={handleChange('fieldName')}
              label="Data Object Identifying Field"
              fullWidth
              size="small"
              variant="outlined"
              renderValue={handleRenderValue(fields)}
              helperText="This field will identify individual data object to prevent duplicated item."
            />

            <FormField
              style={{
                marginBottom: 0,
              }}
              type="Radio"
              options={AUTOMATION_STATUS}
              value={values.automationStatus}
              error={errors.automationStatus}
              onChange={handleChange('automationStatus')}
              label="Automation Status"
              fullWidth
              size="small"
              variant="outlined"
              renderValue={handleRenderValue(AUTOMATION_STATUS)}
              helperText={`<b>Active</b> - Any Data Object Imported, WILL trigger any automation, matching the Trigger Conditions of the Data.<br><br><b>Inactive</b> - Any Data Objects Imported, WILL NOT trigger any automation, until at least one field value on the Data Object is amended.`}
            />
          </>
        )}
      </Box>

      <FormFieldStyled.Divider
        style={{
          margin: '1rem',
        }}
      />

      <Box
        style={{
          padding: '0 1rem 1rem',
        }}>
        <Box
          style={{
            display: 'flex',
            flexDirection: 'column',
          }}>
          <Box
            style={{
              display: 'flex',
            }}>
            <FormFieldStyled.Label variant="caption">
              Attach File (CSV or Excel)
            </FormFieldStyled.Label>
            <Typography
              style={{
                marginLeft: '4px',
                lineHeight: '1.180625rem',
              }}
              component="span"
              color="error">
              *
            </Typography>
          </Box>
          {!values.attachments.length && !!values.requiredFields?.length && (
            <Typography
              sx={{
                color: '#999',
              }}
              style={{
                fontSize: '0.75rem',
              }}>
              {`Must have required fields: ${values.requiredFields
                .map((fieldName) => `"${fieldName}"`)
                .join(' | ')}`}
            </Typography>
          )}
          {!errors.attachments && !!values.nonSupportedFieldsWarning && (
            <Typography
              sx={{
                color: (theme) => theme.palette.warning.main,
              }}
              style={{
                fontSize: '0.75rem',
              }}>
              Unsupported fields: {values.nonSupportedFieldsWarning}
            </Typography>
          )}
          {!loading && values.dataType && (
            <FormFieldStyled.Label
              sx={{
                color: errors.attachments ? 'error.main' : 'success.main',
              }}
              variant="caption">
              {errors.attachments ||
                (!!values.attachments.length && <>Attachment is valid</>)}
            </FormFieldStyled.Label>
          )}
        </Box>
        <UploadForms
          attachments={values.attachments}
          hideSubmitBtn
          acceptedFiles={['.csv', '.xls', '.xlsx']}
          onChange={handleChangeAttachment}
          style={{
            padding: 0,
            marginTop: '0.5rem',
          }}
          containerStyle={{
            minHeight: 120,
          }}
          imageContainerStyle={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            padding: '0 8px',
          }}
          footerStyle={{
            padding: '1rem 1rem 0',
            borderTop: '1px solid #eee',
            boxShadow: '0px -4px 10px -2px #eee',
          }}
          filesLimit={1}
          gridProps={{
            disablePreview: false,
            onDelete: handleDeleteAttachment,
            style: {
              display: 'flex',
              justifyContent: 'center',
              marginTop: '1rem',
            },
            actionContainerStyle: {
              paddingTop: 0,
            },
          }}
        />
      </Box>
    </Section>
  );
};

export default ImportSectionContainer;
