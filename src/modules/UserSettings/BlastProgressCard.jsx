import React, {useState} from 'react';
import EmailIcon from '@mui/icons-material/Email';
import Button from '@macanta/components/Button';
import BlastProgressModal from '@macanta/modules/BlastProgress/BlastProgressModal';
import useBlastProgress from '@macanta/hooks/admin/useBlastProgress';
import {timeDiff} from '@macanta/utils/time';
import SettingCard from './SettingCard';
import SettingInfo from './SettingInfo';
import * as Storage from '@macanta/utils/storage';

const BlastProgressCard = () => {
  const userDetails = Storage.getItem('userDetails');

  const [showModal, setShowModal] = useState(false);

  const {
    data,
    loading,
    pendingItems,
    pendingProgress,
    doneItems,
    updatedSince,
  } = useBlastProgress();

  const handleViewBlastProgress = () => {
    setShowModal(true);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  return (
    <>
      <SettingCard
        loading={!data && loading}
        title="My Blast Progress"
        BodyComp={
          <>
            <SettingInfo
              label="Pending"
              value={pendingItems.length}
              subtext={`(${pendingProgress})`}
            />
            <SettingInfo label="Done" value={doneItems.length} />
            <SettingInfo
              label="Updated"
              value={updatedSince ? timeDiff(updatedSince) : 'N/A'}
            />
          </>
        }
        FooterComp={
          <Button
            style={{
              width: '75%',
              maxWidth: 160,
            }}
            variant="contained"
            onClick={handleViewBlastProgress}
            startIcon={<EmailIcon />}
            size="medium">
            View All
          </Button>
        }
      />
      {showModal && (
        <BlastProgressModal
          showModal
          onClose={handleCloseModal}
          data={data}
          headerTitle={`My Blast Progress (${userDetails.email})`}
        />
      )}
    </>
  );
};

export default BlastProgressCard;
