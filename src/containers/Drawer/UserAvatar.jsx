import React from 'react';
import * as Styled from './styles';
import * as Storage from '@macanta/utils/storage';

const UserAvatar = ({expanded}) => {
  const {firstName} = Storage.getItem('userDetails');

  const initial = firstName.charAt(0).toUpperCase();

  return <Styled.UserAvatar expanded={expanded}>{initial}</Styled.UserAvatar>;
};

export default UserAvatar;
