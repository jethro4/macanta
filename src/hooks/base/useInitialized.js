import {useState, useEffect} from 'react';

const useInitialized = (delay = 0) => {
  const [initialized, setInitialized] = useState(false);

  useEffect(() => {
    setTimeout(() => {
      setInitialized(true);
    }, delay);
  }, []);

  return initialized;
};

export default useInitialized;
