import {experimentalStyled as styled} from '@mui/material/styles';

import Box from '@mui/material/Box';

export const RichTextEditorContainer = styled(Box)`
  & .tox-sidebar-wrap {
    min-height: 200px !important;
  }
`;
