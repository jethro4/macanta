import React, {useEffect, forwardRef} from 'react';

import * as Styled from './styles';

let DataTable = (
  {data, selectable, multiSelect, autoSelect, onSelectItem, ...props},
  ref,
) => {
  useEffect(() => {
    if (!multiSelect) {
      const isSingleData = data?.length === 1;

      if (isSingleData && selectable && autoSelect) {
        onSelectItem(data[0]);
      }
    }
  }, [data, selectable, multiSelect, autoSelect]);

  return (
    <Styled.Root
      ref={ref}
      {...props}
      data={data}
      selectable={selectable}
      multiSelect={multiSelect}
      onSelectItem={onSelectItem}
    />
  );
};

DataTable = forwardRef(DataTable);

DataTable.defaultProps = {
  data: [],
};

export default DataTable;
