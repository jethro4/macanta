import React, {useState} from 'react';
import Popover from '@macanta/components/Popover';
import DeduplicateMergeForms from '@macanta/modules/ContactForms/DeduplicateMergeForms';
import MergeTypeIcon from '@mui/icons-material/MergeType';
import Button from '@macanta/components/Button';

import Badge from '@mui/material/Badge';

const DeduplicateMergeBtn = ({contacts, renderTable, style}) => {
  const [anchorEl, setAnchorEl] = useState(false);
  // const {displayMessage} = useProgressAlert();

  const handleOpen = (event) => {
    if (contacts.length < 2) {
      alert('Must mark more than 1 contact for de-duplication');
    } else {
      setAnchorEl(event.currentTarget);
    }
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const openOtherDetails = Boolean(anchorEl);

  return (
    <>
      <Button
        style={style}
        onClick={handleOpen}
        color="primary"
        variant="contained"
        startIcon={<MergeTypeIcon />}>
        De-Duplicate Marked Contacts{' '}
        {contacts.length > 1 && (
          <Badge
            badgeContent={contacts.length}
            color="light"
            sx={{
              color: (theme) => theme.palette.primary.main,
              paddingTop: '2px',
              marginLeft: '1.25rem',

              '.MuiBadge-badge': {
                fontSize: '0.875rem!important',
                fontWeight: 'bold!important',
              },
            }}
          />
        )}
      </Button>
      <Popover
        title="De-Duplicate Merge Contacts"
        open={openOtherDetails}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}>
        <div
          style={{
            width: '90vw',
            height: '88vh',
            maxWidth: 1200,
            maxHeight: 600,
            display: 'flex',
            flexDirection: 'column',
          }}>
          <DeduplicateMergeForms
            contacts={contacts}
            renderTable={renderTable}
          />
        </div>
      </Popover>
    </>
  );
};

DeduplicateMergeBtn.defaultProps = {
  contacts: [],
};

export default DeduplicateMergeBtn;
