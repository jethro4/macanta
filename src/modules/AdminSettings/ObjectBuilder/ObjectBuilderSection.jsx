import React, {useMemo} from 'react';
import {useMutation} from '@apollo/client';
import Box from '@mui/material/Box';
import AddIcon from '@mui/icons-material/Add';
import VisibilityIcon from '@mui/icons-material/Visibility';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import {ModalButton, DialogButton} from '@macanta/components/Button';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import Section from '@macanta/containers/Section';
import DataTable from '@macanta/containers/DataTable';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import {DELETE_DO_TYPE} from '@macanta/graphql/dataObjects';
import ObjectBuilderForms from './ObjectBuilderForms';
import ContactObjectBuilderForms from './ContactObjectBuilderForms';

const DEFAULT_OBJECT_BUILDER_COLUMNS = [
  {
    id: 'title',
    label: 'Name',
    maxWidth: '100%',
  },
];

const ObjectBuilderSection = (props) => {
  const {displayMessage} = useProgressAlert();

  const doTypesQuery = useDOTypes({
    removeEmail: true,
  });

  const [callDeleteDOTypeMutation, deleteDOTypeMutation] = useMutation(
    DELETE_DO_TYPE,
    {
      update(cache, {data: {deleteDataObjectType}}) {
        const normalizedId = cache.identify({
          id: deleteDataObjectType.groupId,
          __typename: 'DataObject',
        });
        cache.evict({id: normalizedId});
        cache.gc();
      },
      onCompleted() {
        displayMessage(`Deleted data object successfully!`);
      },
    },
  );

  const doTypes = doTypesQuery?.data?.listDataObjectTypes;

  const handleDelete = async (id) => {
    await callDeleteDOTypeMutation({
      variables: {
        deleteDataObjectTypeInput: {
          id,
        },
        __mutationkey: 'deleteDataObjectTypeInput',
        removeAppInfo: true,
      },
    });
  };

  const types = useMemo(() => {
    if (doTypes) {
      return [
        {
          id: 'co_customfields',
          title: 'Contact Object',
        },
      ].concat(doTypes);
    }

    return [];
  }, [doTypes]);

  return (
    <>
      <Section
        fullHeight
        title="Data Object Types"
        style={{
          marginTop: 0,
        }}
        bodyStyle={{
          padding: 0,
        }}
        // loading={loading}
        // style={
        //   data.length && {
        //     backgroundColor: '#f5f6fa',
        //   }
        // }
        HeaderRightComp={
          <ModalButton
            size="small"
            variant="contained"
            startIcon={<AddIcon />}
            ModalProps={{
              headerTitle: 'Create Data Object Type',
              contentWidth: 1400,
              contentHeight: '96%',
              bodyStyle: {
                backgroundColor: '#f5f6fa',
              },
            }}
            renderContent={(handleClose) => (
              <ObjectBuilderForms onSave={handleClose} />
            )}>
            Create Data Object Type
          </ModalButton>
        }
        {...props}>
        <DataTable
          fullHeight
          loading={doTypesQuery.loading}
          columns={DEFAULT_OBJECT_BUILDER_COLUMNS}
          data={types}
          actionColumn={{
            show: true,
            style: {
              minWidth: 100,
            },
          }}
          renderActionButtons={({item}) => (
            <Box
              style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <ModalButton
                icon
                size="small"
                // onClick={handleAdd}
                TooltipProps={{
                  title: 'Show Details',
                }}
                ModalProps={{
                  headerTitle: `Data Object Type (${item.id})`,
                  contentWidth: 1400,
                  contentHeight: '96%',
                  bodyStyle: {
                    backgroundColor: '#f5f6fa',
                  },
                }}
                renderContent={(handleClose) =>
                  item.id !== 'co_customfields' ? (
                    <ObjectBuilderForms onSave={handleClose} item={item} />
                  ) : (
                    <ContactObjectBuilderForms
                      onSave={handleClose}
                      item={item}
                    />
                  )
                }>
                <VisibilityIcon
                  sx={{
                    color: 'info.main',
                  }}
                />
              </ModalButton>
              <DialogButton
                {...(item.id === 'co_customfields' && {
                  style: {
                    visibility: 'hidden',
                  },
                })}
                icon
                size="small"
                TooltipProps={{
                  title: 'Delete',
                }}
                DialogProps={{
                  title: 'Are you sure?',
                  description: [
                    `You are about to delete "${item.title}" from Data Objects.`,
                    'This cannot be undone. Do you want to proceed?',
                  ],
                  onActions: (handleClose) => {
                    return [
                      {
                        label: 'Delete',
                        disabledDelay: 3,
                        onClick: async () => {
                          await handleDelete(item.id);
                          handleClose();
                        },
                        startIcon: <DeleteForeverIcon />,
                      },
                    ];
                  },
                }}>
                <DeleteForeverIcon
                  sx={{
                    color: 'grayDarker.main',
                  }}
                />
              </DialogButton>
            </Box>
          )}
        />
      </Section>
      <LoadingIndicator modal loading={deleteDOTypeMutation.loading} />
    </>
  );
};

export default ObjectBuilderSection;
