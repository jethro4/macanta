import isArray from 'lodash/isArray';
import isFunction from 'lodash/isFunction';

export const runPromise = (callback) => {
  return new Promise((resolve, reject) => {
    callback(resolve, reject);
  });
};

export const delay = (timeout, val) =>
  runPromise(function (resolve) {
    setTimeout(() => resolve(val), timeout);
  });

export const syncPromises = async (arr = [], promiseCallback) => {
  return await arr.reduce(async (acc, valueOrCallback, currentIndex) => {
    const accArr = await acc;

    let result;

    if (promiseCallback) {
      result = await promiseCallback(valueOrCallback, currentIndex, accArr);
    } else if (isArray(valueOrCallback)) {
      result = await syncPromises(valueOrCallback, promiseCallback);
    } else if (isFunction(valueOrCallback)) {
      result = await valueOrCallback(currentIndex, accArr);
    } else {
      result = valueOrCallback;
    }

    return [...accArr, result];
  }, Promise.resolve([]));
};

export const asyncPromises = async (arr = [], promiseCallback) => {
  return await Promise.all(
    arr.map(async (valueOrCallback, currentIndex) => {
      let result;

      if (promiseCallback) {
        result = await promiseCallback(valueOrCallback, currentIndex);
      } else if (isArray(valueOrCallback)) {
        result = await asyncPromises(valueOrCallback, promiseCallback);
      } else if (isFunction(valueOrCallback)) {
        result = await valueOrCallback(currentIndex);
      } else {
        result = valueOrCallback;
      }

      return result;
    }),
  );
};
