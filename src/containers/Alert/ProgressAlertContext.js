import React from 'react';

const ProgressAlertContext = React.createContext({
  displayMessage: () => null,
});

export default ProgressAlertContext;
