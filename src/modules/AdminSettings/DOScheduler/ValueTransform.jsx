import React from 'react';
import isArray from 'lodash/isArray';
import {OverflowTip} from '@macanta/components/Tooltip';
import useValue from '@macanta/hooks/base/useValue';
import LoadingIndicator from '@macanta/components/LoadingIndicator';

const ValueTransform = ({
  value,
  labelKey,
  valueKey,
  selector,
  options: optionsProp,
  emptyMessage,
  ...props
}) => {
  const options = !isArray(optionsProp) ? [optionsProp] : optionsProp;
  const selectorQuery = selector(...options);

  const [label] = useValue(() => {
    const item = selectorQuery.result?.find((r) => r[valueKey] === value);

    if (item) {
      return item[labelKey];
    }

    return '';
  }, [value, labelKey, valueKey, selectorQuery.result]);

  return selectorQuery.initLoading ? (
    <LoadingIndicator transparent size={18} />
  ) : (
    <OverflowTip
      sx={{
        ...(!value && {
          color: '#aaa',
        }),
      }}
      {...props}>
      {!value ? emptyMessage : label}
    </OverflowTip>
  );
};

ValueTransform.defaultProps = {
  emptyMessage: 'N/A',
};

export default ValueTransform;
