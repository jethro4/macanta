global.process = {
  env: {
    NODE_ENV: 'development',
    API_DOMAIN: 'localhost:3000',
  },
};

global.process.env.GATSBY_API_URL = 'http://localhost:3000/graphql';
global.process.env.AWS_REGION = 'sample-aws-region';
global.process.env.AWS_AUTHENTICATION_TYPE = 'us-west-1';
global.process.env.GATSBY_API_KEY = 'API_KEY';
