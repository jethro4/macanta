import React from 'react';
import AddIcon from '@material-ui/icons/Add';
import Box from '@mui/material/Box';
import DataTable from '@macanta/containers/DataTable';
import {
  useDeleteAutomationsMutation,
  useEmailActions,
  usePostEmailActionMutation,
} from '@macanta/hooks/automation';
import ItemActionButtons from '@macanta/modules/AdminSettings/Automation/TriggerActions/ItemActionButtons';
import {EmailActionsForm} from '@macanta/modules/AdminSettings/Automation/TriggerActions/EmailActions';
import Section from '@macanta/containers/Section';
import {writeItemInListFragment} from '@macanta/graphql/helpers';
import {EMAIL_ACTIONS_FRAGMENT} from '@macanta/graphql/automation/GET_EMAIL_ACTION';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import {OverflowTip} from '@macanta/components/Tooltip';
import EditButton from '@macanta/containers/buttons/EditButton';

const EMAIL_ACTION_TYPE_COLUMNS = [
  {
    id: 'name',
    label: 'Name',
  },
  {
    id: 'description',
    label: 'Description',
    maxWidth: '25%',
  },
  {
    id: 'from',
    label: 'From',
    renderBodyCellValue: ({item}) => {
      return (
        <Box>
          <OverflowTip
            sx={{
              fontWeight: 'bold',
              fontSize: '14px',
            }}>
            {item.fromName}
          </OverflowTip>
          <OverflowTip
            sx={{
              fontSize: '13px',
              lineHeight: 1.2,
              color: 'text.secondary',
            }}>
            {item.fromAddress}
          </OverflowTip>
        </Box>
      );
    },
  },
  {
    id: 'subject',
    label: 'Subject',
    maxWidth: '30%',
  },
  {
    id: 'action',
    label: '',
    disableSort: true,
    disableResize: true,
    minWidth: 100,
    maxWidth: 100,
    renderBodyCellValue: ({item, handlers}) => {
      return (
        <ItemActionButtons
          modal={handlers.modal}
          type="Email Actions"
          item={item}
          onDelete={handlers.onDelete}
          renderFormComponent={(handleClose) => (
            <EmailActionsForm
              options={{
                variables: {
                  id: item.id,
                },
              }}
              initialValues={item}
              onSubmit={async (values) => {
                const {result} = await handlers.onSave(values);

                if (result) {
                  handleClose();
                }
              }}
            />
          )}
        />
      );
    },
  },
];

const EmailActionsSection = ({modal, ...props}) => {
  const emailActionsQuery = useEmailActions();

  const [callPostMutation, actionMutation] = usePostEmailActionMutation({
    update(cache, {result}, options) {
      writeItemInListFragment({
        isCreate: !options.variables.input?.id,
        keyField: 'queryId',
        item: result,
        typeName: 'EmailActionsItems',
        fragment: EMAIL_ACTIONS_FRAGMENT,
        parentKey: 'getEmailActions',
        listKey: 'items',
        position: 'first',
      });
    },
  });

  const [callDeleteMutation, deleteMutation] = useDeleteAutomationsMutation({
    typeName: 'EmailActionsItems',
  });

  const handleSave = async (values) => {
    const variables = {
      id: values.id,
      name: values.name,
      description: values.description,
      fromAddress: values.fromAddress,
      fromName: values.fromName,
      subject: values.subject,
      previewText: values.previewText,
      senderSigRelationship: values.signature,
      // attachment: values.attachment,
      // ccEmail: values.ccEmail,
    };

    return callPostMutation({
      variables,
    });
  };

  const handleDelete = (id) => {
    return callDeleteMutation({
      variables: {
        id,
      },
    });
  };

  return (
    <>
      <Section
        style={{
          margin: 0,
        }}
        fullHeight
        title={`Email Actions`}
        HeaderRightComp={
          <EditButton
            modal={modal}
            size="small"
            variant="contained"
            startIcon={<AddIcon />}
            PopoverProps={{
              title: `Add Email Action`,
              contentWidth: 1100,
              contentMinHeight: 350,
              bodyStyle: {
                backgroundColor: '#f5f6fA',
              },
            }}
            renderContent={(handleClose) => (
              <EmailActionsForm
                onSubmit={async (values) => {
                  const {result} = await handleSave(values);

                  if (result) {
                    handleClose();
                  }
                }}
              />
            )}>
            Add Email Action
          </EditButton>
        }>
        <DataTable
          loading={emailActionsQuery.loading}
          data={emailActionsQuery.result}
          fullHeight
          rowsPerPage={25}
          rowsPerPageOptions={[25, 50, 100]}
          {...props}
          columns={EMAIL_ACTION_TYPE_COLUMNS}
          handlers={{
            modal,
            onSave: handleSave,
            onDelete: handleDelete,
          }}
        />
      </Section>
      <LoadingIndicator
        modal
        loading={actionMutation.loading || deleteMutation.loading}
      />
    </>
  );
};

export default EmailActionsSection;
