import React from 'react';
import MUICollapse from '@mui/material/Collapse';
import Container from './Container';

const Collapse = ({children, ...props}) => {
  return (
    <MUICollapse {...props}>
      <Container>{children}</Container>
    </MUICollapse>
  );
};

Collapse.defaultProps = {
  orientation: 'horizontal',
};

export default Collapse;
