import React, {useState} from 'react';
import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';
import Popover from '@macanta/components/Popover';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import Tooltip from '@macanta/components/Tooltip';
import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import * as Styled from './styles';

const AddConditionBtn = ({
  onAddQuery,
  disableSelection,
  label,
  title,
  isEdit,
  icon,
  noLogic,
  ...props
}) => {
  const [anchorEl, setAnchorEl] = useState(false);

  const openSelection = Boolean(anchorEl);

  const handleShowSelection = (event) => {
    if (!disableSelection) {
      setAnchorEl(event.currentTarget);
    } else {
      onAddQuery();
    }
  };

  const handleCloseSelection = () => {
    setAnchorEl(null);
  };

  const handleSelectType = (type) => () => {
    onAddQuery(type);

    handleCloseSelection();
  };

  const listIconComp = !isEdit ? <AddIcon /> : <EditIcon />;

  return (
    <>
      <Tooltip title={title}>
        <div>
          {!icon ? (
            <Styled.Button
              variant="contained"
              startIcon={<AddIcon />}
              onClick={noLogic ? handleSelectType('and') : handleShowSelection}
              size="small"
              {...props}>
              {label}
            </Styled.Button>
          ) : (
            <Styled.IconButton onClick={handleShowSelection}>
              <AddIcon />
            </Styled.IconButton>
          )}
        </div>
      </Tooltip>
      <Popover
        hideHeader
        open={openSelection}
        anchorEl={anchorEl}
        onClose={handleCloseSelection}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: -8,
          horizontal: 'right',
        }}>
        <div
          style={{
            width: 100,
          }}>
          <List
            style={{
              padding: 0,
            }}>
            <NoteTaskHistoryStyled.SelectionItem
              button
              onClick={handleSelectType('and')}>
              {listIconComp}
              <NoteTaskHistoryStyled.SelectionItemText primary="AND" />
            </NoteTaskHistoryStyled.SelectionItem>
            <Divider />
            <NoteTaskHistoryStyled.SelectionItem
              button
              onClick={handleSelectType('or')}>
              {listIconComp}
              <NoteTaskHistoryStyled.SelectionItemText primary="OR" />
            </NoteTaskHistoryStyled.SelectionItem>
          </List>
        </div>
      </Popover>
    </>
  );
};

AddConditionBtn.defaultProps = {
  label: 'ADD',
  title: 'Add Query',
};

export default AddConditionBtn;
