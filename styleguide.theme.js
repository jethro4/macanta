module.exports = {
  baseBackground: '#fdfdfc',
  link: '#274e75',
  linkHover: '#90a7bf',
  border: '#e0d2de',
  font: ['Helvetica', 'sans-serif'],
  color: {
    codeComment: '#6d6d6d',
    codePunctuation: '#999',
    codeProperty: '#905',
    codeDeleted: '#905',
    codeString: '#690',
    codeInserted: '#690',
    codeOperator: '#9a6e3a',
    codeKeyword: '#1673b1',
    codeFunction: '#DD4A68',
    codeVariable: '#e90',
  },

  // color: {
  //   link: 'firebrick',
  //   linkHover: 'salmon'
  // },
  // fontFamily: {
  //   base: '"Comic Sans MS", "Comic Sans", cursive'
  // }
};
