import React from 'react';
import {useFormikContext} from 'formik';
import TurnedInIcon from '@mui/icons-material/TurnedIn';
import Button from '@macanta/components/Button';
import * as FormStyled from '@macanta/modules/NoteTaskForms/styles';

const FooterActionButtons = ({isEdit, disabled}) => {
  const {handleSubmit, dirty, setFieldValue} = useFormikContext();

  const handleSave = () => {
    setFieldValue('saveAsNew', false);

    setTimeout(handleSubmit, 0);
  };

  const handleSaveAsNew = () => {
    setFieldValue('saveAsNew', true);

    setTimeout(handleSubmit, 0);
  };

  return (
    <FormStyled.Footer
      // sx={{
      //   backgroundColor: 'grayLighter.main',
      // }}
      style={{
        padding: '1rem',
        margin: 0,
        borderTop: '1px solid #eee',
        width: '100%',
        justifyContent: isEdit ? 'space-between' : 'flex-end',
      }}>
      {/* {!!errorMessage && (
                    <Typography
                      color="error"
                      variant="subtitle2"
                      align="center">
                      {errorMessage}
                    </Typography>
                  )} */}

      {isEdit && (
        <Button
          disabled={disabled}
          size="medium"
          variant="outlined"
          startIcon={<TurnedInIcon />}
          onClick={handleSaveAsNew}>
          Save As New
        </Button>
      )}
      <Button
        style={{
          marginLeft: '1rem',
        }}
        disabled={disabled || !dirty}
        size="medium"
        variant="contained"
        startIcon={<TurnedInIcon />}
        onClick={handleSave}>
        Save Template
      </Button>
    </FormStyled.Footer>
  );
};

export default FooterActionButtons;
