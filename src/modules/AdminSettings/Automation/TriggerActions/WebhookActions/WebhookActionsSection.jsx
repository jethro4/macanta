import React from 'react';
import AddIcon from '@material-ui/icons/Add';
import DataTable from '@macanta/containers/DataTable';
import {
  useDeleteAutomationsMutation,
  useWebhookActions,
  usePostWebhookActionMutation,
} from '@macanta/hooks/automation';
import ItemActionButtons from '@macanta/modules/AdminSettings/Automation/TriggerActions/ItemActionButtons';
import {WebhookActionsForm} from '@macanta/modules/AdminSettings/Automation/TriggerActions/WebhookActions';
import Section from '@macanta/containers/Section';
import {writeItemInListFragment} from '@macanta/graphql/helpers';
import {WEBHOOK_ACTIONS_FRAGMENT} from '@macanta/graphql/automation/GET_WEBHOOK_ACTIONS';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import EditButton from '@macanta/containers/buttons/EditButton';

const WEBHOOK_ACTION_TYPE_COLUMNS = [
  {
    id: 'name',
    label: 'Name',
    maxWidth: '25%',
  },
  {
    id: 'description',
    label: 'Description',
    maxWidth: '30%',
  },
  {
    id: 'postURL',
    label: 'Post URL',
    maxWidth: '30%',
  },
  {
    id: 'doTitle',
    label: 'Data Object Type',
    maxWidth: '20%',
  },
  {
    id: 'action',
    label: '',
    disableSort: true,
    disableResize: true,
    minWidth: 100,
    maxWidth: 100,
    renderBodyCellValue: ({item, handlers}) => {
      return (
        <ItemActionButtons
          modal={handlers.modal}
          type="Webhook Actions"
          item={item}
          onDelete={handlers.onDelete}
          renderFormComponent={(handleClose) => (
            <WebhookActionsForm
              options={{
                variables: {
                  id: item.id,
                },
              }}
              initialValues={item}
              onSubmit={async (values) => {
                const {result} = await handlers.onSave(values);

                if (result) {
                  handleClose();
                }
              }}
            />
          )}
        />
      );
    },
  },
];

const WebhookActionsSection = ({modal, ...props}) => {
  const webhookActionsQuery = useWebhookActions();

  const [callPostMutation, actionMutation] = usePostWebhookActionMutation({
    update(cache, {result}, options) {
      writeItemInListFragment({
        isCreate: !options.variables.input?.id,
        keyField: 'queryId',
        item: result,
        typeName: 'WebhookActionItems',
        fragment: WEBHOOK_ACTIONS_FRAGMENT,
        parentKey: 'getWebhookActions',
        listKey: 'items',
        position: 'first',
      });
    },
  });

  const [callDeleteMutation, deleteMutation] = useDeleteAutomationsMutation({
    typeName: 'WebhookActionItems',
  });

  const handleSave = async (values) => {
    const variables = {
      id: values.id,
      name: values.name,
      description: values.description,
      doType: values.doTitle,
      postURL: values.postURL,
      fieldKeyValue: values.doConditions.map((item) => ({
        fieldName: item.name,
        value: item.value,
      })),
      contactConditions: values.contactConditions.map((item) => ({
        relationship: item.relationship,
        logic: item.logic || 'and',
      })),
      customKeyValue: values.customKeyValues.map((item) => ({
        fieldName: item.name,
        value: item.value,
      })),
    };

    return callPostMutation({
      variables,
    });
  };

  const handleDelete = (id) => {
    return callDeleteMutation({
      variables: {
        id,
      },
    });
  };

  return (
    <>
      <Section
        style={{
          margin: 0,
        }}
        fullHeight
        title={`Webhook Actions`}
        HeaderRightComp={
          <EditButton
            modal={modal}
            size="small"
            variant="contained"
            startIcon={<AddIcon />}
            PopoverProps={{
              title: `Add Webhook Action`,
              contentWidth: 1100,
              contentMinHeight: 350,
              bodyStyle: {
                backgroundColor: '#f5f6fA',
              },
            }}
            renderContent={(handleClose) => (
              <WebhookActionsForm
                onSubmit={async (values) => {
                  const {result} = await handleSave(values);

                  if (result) {
                    handleClose();
                  }
                }}
              />
            )}>
            Add Webhook Action
          </EditButton>
        }>
        <DataTable
          loading={webhookActionsQuery.loading}
          data={webhookActionsQuery.result}
          fullHeight
          rowsPerPage={25}
          rowsPerPageOptions={[25, 50, 100]}
          {...props}
          columns={WEBHOOK_ACTION_TYPE_COLUMNS}
          handlers={{
            modal,
            onSave: handleSave,
            onDelete: handleDelete,
          }}
        />
      </Section>
      <LoadingIndicator
        modal
        loading={actionMutation.loading || deleteMutation.loading}
      />
    </>
  );
};

export default WebhookActionsSection;
