import React from 'react';
import Breadcrumbs from '@mui/material/Breadcrumbs';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import Typography from '@mui/material/Typography';
import Section from '@macanta/containers/Section';
import DataTable from '@macanta/containers/DataTable';
import {ROOT_PATH_ADMIN} from '@macanta/modules/AdminSettings/AdminSettings';
import useQuery from '@macanta/hooks/apollo/useQuery';
import {LIST_INTERNAL_SUPPORTS} from '@macanta/graphql/admin';
import AddEmailBtn from './AddEmailBtn';
import DeleteEmailBtn from './DeleteEmailBtn';
import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import * as AdminSettingsStyled from '@macanta/modules/AdminSettings/styles';
import * as DataObjectsSearchTableStyled from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/styles';
import {navigate} from 'gatsby';

const INTERNAL_SUPPORTS_COLUMNS = [
  {
    id: 'email',
    label: 'Email',
  },
  {
    id: 'fullName',
    label: 'Full Name',
  },
];

const InternalSupportEmails = () => {
  /* const navigate = useNavigate();*/

  const internalSupportsQuery = useQuery(LIST_INTERNAL_SUPPORTS);

  const internalSupports = internalSupportsQuery.data?.listInternalSupports;

  const handleBack = () => {
    navigate(ROOT_PATH_ADMIN);
  };

  return (
    <NoteTaskHistoryStyled.FullHeightGrid container>
      <NoteTaskHistoryStyled.FullHeightFlexGridColumn item xs={12} lg={6}>
        <Section
          fullHeight
          bodyStyle={{
            backgroundColor: 'white',
            padding: 0,
          }}
          HeaderLeftComp={
            <Breadcrumbs aria-label="breadcrumb">
              <AdminSettingsStyled.TitleCrumbBtn
                startIcon={
                  <ArrowBackIcon
                    style={{
                      color: '#333',
                    }}
                  />
                }
                onClick={handleBack}
                disabled={false}>
                <Typography color="textPrimary">Admin Settings</Typography>
              </AdminSettingsStyled.TitleCrumbBtn>
              <Typography>Internal Support Emails</Typography>
            </Breadcrumbs>
          }
          HeaderRightComp={<AddEmailBtn />}>
          <DataTable
            loading={internalSupportsQuery.loading}
            noOddRowStripes
            fullHeight
            columns={INTERNAL_SUPPORTS_COLUMNS}
            data={internalSupports}
            rowsPerPage={25}
            rowsPerPageOptions={[25, 50, 100]}
            renderActionButtons={({item}) => (
              <DataObjectsSearchTableStyled.ActionButtonContainer>
                <AddEmailBtn item={item} />
                <DeleteEmailBtn email={item.email} fullName={item.fullName} />
              </DataObjectsSearchTableStyled.ActionButtonContainer>
            )}
            actionColumn={{
              show: true,
              style: {
                minWidth: 100,
              },
            }}
          />
        </Section>
      </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
    </NoteTaskHistoryStyled.FullHeightGrid>
  );
};

export default InternalSupportEmails;
