import {nextFactoryId} from './factory';

const typename = 'PermissionTemplate';

export default (overwrites) => {
  const {id, currentIndex} = nextFactoryId(typename);

  return {
    __typename: typename,
    id,
    name: `Permission Template ${currentIndex}`,
    description: `Description ${currentIndex}`,
    status: 'Active',
    permission: {
      permissionMeta: {
        allowUserDeleteContact: false,
        allowUserDeleteDOItem: false,
        allowUserAttachment: true,
        allowUserDeleteDOAttachment: true,
        allowUserToMergeContacts: false,
        allowEmailSync: false,
        allowAddRemoveEmailDomainFilter: false,
        allowContactExport: false,
        allowDataObjectExport: false,
        allowNoteTaskExport: false,
        allowAddDirectRelationship: true,
        userDOEditPermission: {
          otherUserRelationship: [
            'Relationship1',
            'Relationship2',
            'Relationship3',
            'Relationship4',
          ],
          withOtherUserDataToggle: true,
          withOtherUserRelationshipToggle: false,
          withOtherUserRelationshipDataToggle: false,
          withOtherUserRelationshipRelationshipToggle: false,
          __typename: 'UserDOEditPermission',
        },
        hideActionButtonsForGroups: ['DataObject4'],
        hideConnectButtonsForGroups: [],
        hidePurchaseButton: true,
        __typename: 'UserPermissionMeta',
      },
      mqbs: [
        {
          queryId: 'QueryBuilder1',
          permission: 'READ_WRITE',
          __typename: 'MQBPermission',
        },
      ],
      mqbWidgetAccess: [
        {
          queryId: 'QueryBuilder1',
          show: true,
          __typename: 'MQBWidgetAccess',
        },
      ],
      mqbWidgetEmail: [
        {
          queryId: 'QueryBuilder1',
          show: true,
          __typename: 'MQBWidgetEmail',
        },
      ],
      tasks: [
        {
          taskId: 'OwnTask',
          allow: true,
          specificIds: [],
          __typename: 'TaskPermission',
        },
        {
          taskId: 'AllUserTask',
          allow: false,
          specificIds: [],
          __typename: 'TaskPermission',
        },
        {
          taskId: 'SpecificUserTask',
          allow: true,
          specificIds: ['554753', '1361'],
          __typename: 'TaskPermission',
        },
      ],
      userGuides: [
        {
          guideId: 'GeneralInterface-Contact Window',
          type: 'GeneralInterface',
          value: 'Test contact',
          subType: 'Contact Window',
          meta: [],
          __typename: 'UserGuide',
        },
        {
          guideId: 'GeneralInterface-Search Function',
          type: 'GeneralInterface',
          value: 'Test Search',
          subType: 'Search Function',
          meta: [],
          __typename: 'UserGuide',
        },
        {
          guideId: 'GeneralInterface-Notes & Tasks',
          type: 'GeneralInterface',
          value: 'Test notes & tasks',
          subType: 'Notes & Tasks',
          meta: [],
          __typename: 'UserGuide',
        },
        {
          guideId: 'SidebarTabs-Notes',
          type: 'SidebarTabs',
          value: 'Test Notes',
          subType: 'Notes',
          meta: [],
          __typename: 'UserGuide',
        },
        {
          guideId: 'SidebarTabs-Admin',
          type: 'SidebarTabs',
          value: 'Test Admin',
          subType: 'Admin',
          meta: [],
          __typename: 'UserGuide',
        },
        {
          guideId: 'SidebarTabs-Macanta Installs',
          type: 'SidebarTabs',
          value: 'Test macanta installs',
          subType: 'Macanta Installs',
          meta: [],
          __typename: 'UserGuide',
        },
        {
          guideId: 'SidebarTabs-Opportunities',
          type: 'SidebarTabs',
          value: 'Test opportunities',
          subType: 'Opportunities',
          meta: [],
          __typename: 'UserGuide',
        },
        {
          guideId: 'DataObject-Macanta Installs-Macanta',
          type: 'DataObject',
          value: 'Test Macanta',
          subType: 'Macanta',
          meta: [
            {
              name: 'doType',
              value: 'Macanta Installs',
              __typename: 'UserGuideMeta',
            },
          ],
          __typename: 'UserGuide',
        },
        {
          guideId: 'DataObject-Macanta Installs-Chargebee',
          type: 'DataObject',
          value: 'Test Chargebee',
          subType: 'Chargebee',
          meta: [
            {
              name: 'doType',
              value: 'Macanta Installs',
              __typename: 'UserGuideMeta',
            },
          ],
          __typename: 'UserGuide',
        },
        {
          guideId: 'DataObject-Macanta Installs-Retention Metrics',
          type: 'DataObject',
          value: 'Test Retention',
          subType: 'Retention Metrics',
          meta: [
            {
              name: 'doType',
              value: 'Macanta Installs',
              __typename: 'UserGuideMeta',
            },
          ],
          __typename: 'UserGuide',
        },
      ],
      tabs: [
        {
          groupId: 'notes_tab',
          permission: 'READ_WRITE',
          __typename: 'TabPermission',
        },
        {
          groupId: 'DataObject1',
          permission: 'READ_ONLY',
          __typename: 'TabPermission',
        },
        {
          groupId: 'DataObject2',
          permission: 'READ_WRITE',
          __typename: 'TabPermission',
        },
        {
          groupId: 'DataObject3',
          permission: 'NO_ACCESS',
          __typename: 'TabPermission',
        },
        {
          groupId: 'DataObject4',
          permission: 'NO_ACCESS',
          __typename: 'TabPermission',
        },
        {
          groupId: 'CustomTabItem1',
          permission: 'READ_WRITE',
          __typename: 'TabPermission',
        },
        {
          groupId: 'com_history',
          permission: 'READ_ONLY',
          __typename: 'TabPermission',
        },
      ],
      sections: [
        {
          groupId: 'DataObject1',
          access: [
            {
              name: 'Single Value',
              permission: 'NO_ACCESS',
              __typename: 'Access',
            },
          ],
          __typename: 'SectionPermission',
        },
        {
          groupId: 'DataObject2',
          access: [
            {
              name: 'Single Value',
              permission: 'READ_ONLY',
              __typename: 'Access',
            },
          ],
          __typename: 'SectionPermission',
        },
        {
          groupId: 'DataObject3',
          access: [
            {
              name: 'Single Value',
              permission: 'NO_ACCESS',
              __typename: 'Access',
            },
          ],
          __typename: 'SectionPermission',
        },
        {
          groupId: 'DataObject4',
          access: [
            {
              name: 'General',
              permission: 'NO_ACCESS',
              __typename: 'Access',
            },
            {
              name: 'Tracking',
              permission: 'READ_WRITE',
              __typename: 'Access',
            },
          ],
          __typename: 'SectionPermission',
        },
      ],
      contacts: [
        {
          groupId: 'Global',
          access: [
            {
              name: 'TypeA',
              permission: 'READ_ONLY',
              relationship: '',
              __typename: 'ContactAccess',
            },
            {
              name: 'TypeB',
              permission: 'READ_ONLY',
              relationship: '',
              __typename: 'ContactAccess',
            },
            {
              name: 'TypeC',
              permission: 'READ_ONLY',
              relationship: '',
              __typename: 'ContactAccess',
            },
          ],
          __typename: 'ContactPermission',
        },
        {
          groupId: 'ci_k30e5zmn',
          access: [
            {
              name: 'TypeB',
              permission: 'READ_WRITE',
              relationship: '',
              __typename: 'ContactAccess',
            },
          ],
          __typename: 'ContactPermission',
        },
      ],
      emailTemplates: [
        {
          templateGroupId: '1',
          name: 'Pieter Emails',
          __typename: 'EmailTemplatePermission',
        },
        {
          templateGroupId: '2',
          name: 'Group B',
          __typename: 'EmailTemplatePermission',
        },
      ],
      workflowBoards: [
        {
          boardId: 'pm_7bfa2f52a0c3ef40',
          scope: 'CurrentUser',
          __typename: 'WorkflowBoardPermission',
        },
        {
          boardId: 'pm_2670ae20c8810c67',
          scope: 'All',
          __typename: 'WorkflowBoardPermission',
        },
        {
          boardId: 'pm_5009488ecd968490',
          scope: 'CurrentUser',
          __typename: 'WorkflowBoardPermission',
        },
        {
          boardId: 'pm_ca09ee32141269d0',
          scope: 'All',
          __typename: 'WorkflowBoardPermission',
        },
      ],
      __typename: 'AccessPermissions',
    },
    ...overwrites,
  };
};
