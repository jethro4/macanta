import React from 'react';
import Markdown from '@macanta/components/Markdown';
import Section from '@macanta/containers/Section';
import Typography from '@mui/material/Typography';
import useCustomTab from '@macanta/hooks/admin/useCustomTab';
import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';

const CustomTabAdmin = ({id, title: titleParam}) => {
  const title = decodeURIComponent(titleParam);

  const customTabQuery = useCustomTab({id});

  const customTab = customTabQuery.data;

  return (
    <NoteTaskHistoryStyled.FullHeightGrid container>
      <NoteTaskHistoryStyled.FullHeightFlexGridColumn item xs={12}>
        <Section
          style={{
            marginTop: 0,
          }}
          fullHeight
          loading={customTabQuery.loading}
          HeaderLeftComp={<Typography color="textPrimary">{title}</Typography>}>
          <Markdown>{customTab?.content}</Markdown>
        </Section>
      </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
    </NoteTaskHistoryStyled.FullHeightGrid>
  );
};

export default CustomTabAdmin;
