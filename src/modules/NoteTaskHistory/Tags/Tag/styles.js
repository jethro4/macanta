import {experimentalStyled as styled} from '@mui/material/styles';

import Chip from '@mui/material/Chip';

export const Root = styled(Chip)`
  margin: ${({theme}) => theme.spacing(1)} 0 ${({theme}) => theme.spacing(1)}
    ${({theme}) => theme.spacing(1)};
  background-color: #e0e0e0;
  border-radius: 16px;
`;
