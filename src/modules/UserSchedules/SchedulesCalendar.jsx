import React from 'react';
import Box from '@mui/material/Box';
import useValue from '@macanta/hooks/base/useValue';
import {
  getCalendarMonthlyItems,
  isWeekend,
  sortedDays,
} from '@macanta/modules/UserSchedules/helpers';
import FlexGrid from '@macanta/components/Base/FlexGrid';
import CalendarItem from '@macanta/modules/UserSchedules/CalendarItem';
import {OverflowTip} from '@macanta/components/Tooltip';
import {buildDate} from '@macanta/utils/time';
import {convertArrayToObjectByKeys} from '@macanta/utils/array';
import {getFontRelativeSize} from '@macanta/themes/applicationStyles';

const SchedulesCalendar = ({searchOptions, data, ...props}) => {
  const [calendarItems] = useValue(() => {
    const mappedData = convertArrayToObjectByKeys(data, {
      labelKey: 'date',
      valueKey: 'items',
    });

    const {month, year} = searchOptions;

    const date = buildDate({month, year});

    const items = getCalendarMonthlyItems(date);

    const itemsWithData = items.map((item) => ({
      ...item,
      items: mappedData[item.value] || [],
    }));

    return itemsWithData;
  }, [searchOptions, data]);

  // const isActiveDate = (value) => {
  //   const dateObj = createDate(value);

  //   return isEqual(searchOptions, {
  //     month: getMonthOfYear(dateObj),
  //     year: getYear(dateObj),
  //   });
  // };

  return (
    <Box
      {...props}
      sx={{
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
        overflowY: 'auto',
        ...props.sx,
      }}>
      <FlexGrid cols={sortedDays.length} border={1}>
        {sortedDays.map((item) => {
          return (
            <OverflowTip
              key={String(item.value)}
              sx={{
                padding: '7px 10px',
                fontSize: getFontRelativeSize(13),
                letterSpacing: '0.5px',
                color: 'text.secondary',
                backgroundColor: isWeekend(item.value)
                  ? 'grayLighter.main'
                  : 'common.white',
              }}>
              {item.label}
            </OverflowTip>
          );
        })}
      </FlexGrid>

      <FlexGrid
        sx={{
          flex: 1,
        }}
        cols={sortedDays.length}
        border={1}>
        {calendarItems.map((item) => {
          return (
            <CalendarItem
              key={item.value}
              sx={{
                padding: '7px 10px',
                height: '100%',
              }}
              minHeight={120}
              value={item.value}
              dateOfMonth={item.dateOfMonth}
              day={item.day}
              data={item.items}
              // isActiveDate={isActiveDate(item.value)}
            />
          );
        })}
      </FlexGrid>
    </Box>
  );
};

export default SchedulesCalendar;
