import React, {useState, useLayoutEffect} from 'react';
import ButtonGroup from '@macanta/components/ButtonGroup';
import Button from '@macanta/components/Button';
import useQuery from '@macanta/hooks/apollo/useQuery';
import {LIST_DO_ITEM_HISTORY} from '@macanta/graphql/dataObjects';
import useContactId from '@macanta/hooks/useContactId';
import DataTable from '@macanta/containers/DataTable';
import DownloadIcon from '@mui/icons-material/Download';
import {getAppInfo} from '@macanta/utils/app';
import * as Styled from './styles';
import envConfig from '@macanta/config/envConfig';

const DO_HISTORY_TYPES = [
  {
    type: 'automation',
    label: 'Automation History',
  },
  {
    type: 'data',
    label: 'Data History',
  },
  {
    type: 'relationship',
    label: 'Relationship History',
  },
];

const DOItemHistory = ({itemId}) => {
  const [selectedHistoryType, setSelectedHistoryType] = useState(
    DO_HISTORY_TYPES[0].type,
  );
  const [columns, setColumns] = useState([]);
  const [tableData, setTableData] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(25);

  const contactId = useContactId();

  const doItemHistoryQuery = useQuery(LIST_DO_ITEM_HISTORY, {
    variables: {
      type: selectedHistoryType,
      doItemId: itemId,
      contactId,
      page,
      limit: rowsPerPage,
    },
  });

  const historyFields =
    doItemHistoryQuery.data?.listDataObjectItemHistory?.fields;
  const historyItems =
    doItemHistoryQuery.data?.listDataObjectItemHistory?.items;

  const handleSelectDOType = (type) => () => {
    const history = DO_HISTORY_TYPES.find((h) => h.type === type);

    setSelectedHistoryType(history.type);
  };

  const handleTableChangePage = (value) => {
    setPage(value);
  };

  const handleTableChangeRowsPerPage = (limit) => {
    setRowsPerPage(limit);
    setPage(0);
  };

  const handleDownloadAll = () => {
    const [appName, apiKey] = getAppInfo();

    const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/history/download/${itemId}?api_key=${apiKey}&limit=100&page=0&contactId=${contactId}`;

    window.location.href = url;
  };

  useLayoutEffect(() => {
    if (historyFields) {
      setColumns(
        historyFields.map((f) => ({
          id: f.label,
          label: f.label,
          minWidth: 170,
        })),
      );
    }
  }, [historyFields]);

  useLayoutEffect(() => {
    if (historyItems && historyFields) {
      const historyData = historyItems.reduce((acc, item) => {
        const itemObj = {};
        item.data.forEach((d) => {
          const field = historyFields.find((f) => f.fieldKey === d.fieldKey);
          itemObj[field.label] = d.value;
        });

        return acc.concat(itemObj);
      }, []);

      setTableData(historyData);
    }
  }, [historyItems, historyFields]);

  return (
    <>
      <Styled.DOTypeButtonGroupContainer
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <ButtonGroup
          size="small"
          variant="outlined"
          aria-label="primary button group">
          {DO_HISTORY_TYPES.map((history) => {
            return (
              <Button
                key={history.type}
                onClick={handleSelectDOType(history.type)}>
                {history.label}
              </Button>
            );
          })}
        </ButtonGroup>

        <Button
          onClick={handleDownloadAll}
          color="gray"
          size="small"
          variant="contained"
          startIcon={<DownloadIcon />}>
          Download History
        </Button>
      </Styled.DOTypeButtonGroupContainer>
      <DataTable
        fullHeight
        loading={doItemHistoryQuery.loading}
        hideEmptyMessage={doItemHistoryQuery.loading}
        columns={columns}
        data={tableData}
        numOfTextLines={2}
        onTablePageChange={handleTableChangePage}
        onTableRowsPerPageChange={handleTableChangeRowsPerPage}
      />
    </>
  );
};

export default DOItemHistory;
