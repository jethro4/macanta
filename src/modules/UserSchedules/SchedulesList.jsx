import React, {useState} from 'react';
import find from 'lodash/find';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import ScheduleItem, {
  VERTICAL_MARGIN,
} from '@macanta/modules/UserSchedules/ScheduleItem';
import useValue from '@macanta/hooks/base/useValue';
import FlexGrid from '@macanta/components/Base/FlexGrid';
import {
  getStatus,
  SCHEDULE_FILTER_TYPES,
} from '@macanta/modules/UserSchedules/helpers';
import Section from '@macanta/containers/Section';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import {MONTHS} from '@macanta/constants/time';
import {uncapitalizeFirstLetter} from '@macanta/utils/string';
import ButtonGroup from '@macanta/components/ButtonGroup';
import * as DOStyled from '@macanta/modules/DataObjectItems/styles';
import Colors from '@macanta/utils/colors';

const SchedulesList = ({searchOptions, data, loading, ...props}) => {
  const [selectedFilterType, setSelectedFilterType] = useState(
    SCHEDULE_FILTER_TYPES[0].value,
  );

  const handleSelectFilterType = (val) => {
    setSelectedFilterType(val);
  };

  const [schedulesMap] = useValue(() => {
    return data.reduce(
      (acc, d) => {
        const accObj = {...acc};

        const scheduleKey = `sched_${getStatus(d)}`;

        const scheduleItems = accObj[scheduleKey];

        d.items.forEach((item) => {
          scheduleItems.push(item);
        });

        accObj[scheduleKey] = scheduleItems;

        return accObj;
      },
      {
        ...SCHEDULE_FILTER_TYPES.reduce((acc, item) => {
          acc[item.value] = [];

          return acc;
        }, {}),
      },
    );
  }, [data]);

  const [{schedulesList, isEmpty, monthInfo, filterTypeInfo}] = useValue(() => {
    const items = schedulesMap[selectedFilterType];

    return {
      schedulesList: items,
      isEmpty: items.length === 0,
      monthInfo: find(MONTHS, ['value', searchOptions.month]),
      filterTypeInfo: find(SCHEDULE_FILTER_TYPES, [
        'value',
        selectedFilterType,
      ]),
    };
  }, [searchOptions, selectedFilterType, schedulesMap]);

  return (
    <Section
      {...props}
      style={{
        alignSelf: 'flex-start',
        margin: '1rem',
        ...props.style,
      }}
      title="Scheduled Items"
      headerStyle={{
        borderBottom: `1px solid ${Colors.borderLight}`,
        boxShadow: 'none',
      }}
      HeaderRightComp={
        <ButtonGroup size="small" variant="text" aria-label="text button group">
          {SCHEDULE_FILTER_TYPES.map((item) => {
            const scheduleCount = schedulesMap[item.value].length;

            return (
              <DOStyled.DOItemNavBtn
                key={item.value}
                sx={{
                  fontSize: '13.4px',

                  // '.MuiButton-label': {
                  //   '&:after': {
                  //     content: '""',
                  //     display: 'none !important',
                  //   },
                  // },
                }}
                onClick={() => handleSelectFilterType(item.value)}>
                {item.label} ({scheduleCount})
              </DOStyled.DOItemNavBtn>
            );
          })}
        </ButtonGroup>
      }>
      <FlexGrid
        cols={1}
        margin={[VERTICAL_MARGIN, 0]}
        sx={{
          padding: '1rem',
        }}>
        {schedulesList.map((item) => {
          const status = getStatus(item);

          return (
            <ScheduleItem
              key={String(item.id)}
              item={item}
              status={status}
              sx={
                ['upcoming', 'overdue'].includes(status) && {
                  '.MuiTypography-root': {
                    opacity: 0.7,
                  },
                }
              }
            />
          );
        })}
      </FlexGrid>
      {isEmpty && (
        <Box
          sx={{
            margin: '2rem 0',
            display: 'flex',
            justifyContent: 'center',
          }}>
          {!loading ? (
            <Typography color="#888">
              No {uncapitalizeFirstLetter(filterTypeInfo?.label)} for{' '}
              {monthInfo?.label}
            </Typography>
          ) : (
            <LoadingIndicator transparent />
          )}
        </Box>
      )}

      {/* <SectionsFlexGrid
        titleKey="date"
        renderData={(item) => <ScheduleItem item={item} />}
        loading={loading}
        data={data}
        cols={4}
        margin={['1.5rem', '1rem']}
      /> */}
    </Section>
  );
};

export default SchedulesList;
