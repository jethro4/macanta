import React from 'react';
import {SelectMenuItems} from '@macanta/components/Forms/ChoiceFields/SelectField';
import {transformValue} from '@macanta/components/Forms/InputFields/helpers';
import useTransformChoices from '@macanta/hooks/base/useTransformChoices';
import useValue from '@macanta/hooks/base/useValue';

const SelectItems = ({
  multiple,
  selectedValue: valueProp,
  options: optionsProp,
  getOptionLabel,
  labelKey,
  valueKey,
  subLabelKey,
  captionKey,
  sort,
  ...props
}) => {
  const [value] = useValue(() => transformValue(valueProp, multiple), [
    valueProp,
  ]);

  const options = useTransformChoices({
    options: optionsProp,
    sort,
    getOptionLabel,
    labelKey,
    valueKey,
    subLabelKey,
    captionKey,
  });

  const withSubLabel = !!subLabelKey;

  return (
    <SelectMenuItems
      multiple={multiple}
      selectedValue={value}
      options={options}
      withSubLabel={withSubLabel}
      {...props}
    />
  );
};

SelectItems.defaultProps = {
  multiple: false,
};

export default SelectItems;
