const path = require('path');
const theme = require('./styleguide.theme');
const styles = require('./styleguide.styles');

module.exports = {
  /** props & methods, preview mode, and view code settings **/
  usageMode: 'expand',
  exampleMode: 'expand',
  getComponentPathLine(componentPath) {
    const name = path.basename(componentPath, '.jsx');
    const dir = path.dirname(componentPath);
    const paths = dir.split('/');
    paths[0] = '@macanta';

    return `import ${name} from '${paths.join('/')}';`;
  },

  /** sidebar, table of contents and meta settings **/
  title: 'Salesferry Components Style Guide',
  pagePerSection: true,
  skipComponentsWithoutExample: true,
  // tocMode: 'collapse',
  sections: [
    {
      name: 'UI Components',
      // components: ['src/{components,containers}/**/[A-Z]*.{jsx,tsx}'],
      components: [
        'src/components/**/Alert.{jsx,tsx}',
        'src/components/**/Delay.{jsx,tsx}',
        'src/components/**/Button.{jsx,tsx}',
      ],
    },
    {
      name: 'Forms',
      sections: [
        {
          name: 'Input Fields',
          content: 'src/components/Forms/InputFields/Readme.md',
          components: ['src/components/Forms/InputFields/**/[A-Z]*.{jsx,tsx}'],
        },
        {
          name: 'Choice Fields',
          components: ['src/components/Forms/ChoiceFields/**/[A-Z]*.{jsx,tsx}'],
        },
        {
          name: 'Date Fields',
          components: ['src/components/Forms/DateFields/**/[A-Z]*.{jsx,tsx}'],
        },
      ],
      sectionDepth: 2,
    },
    {
      name: 'Using GraphQL APIs',
      sections: [
        {
          name: 'Query',
          content: 'src/hooks/apollo/useQuery/Readme.md',
        },
        {
          name: 'Mutation',
          content: 'src/hooks/apollo/useMutation/Readme.md',
        },
      ],
      sectionDepth: 2,
    },
  ],

  /** themes and css settings **/
  theme,
  styles,
  require: [path.join(__dirname, 'styleguide/styles.css')],
  styleguideComponents: {
    Wrapper: path.join(__dirname, 'styleguide/BaseThemeWrapper'),
  },

  /** build settings **/
  printBuildInstructions(config) {
    console.info(
      `Style guide published to ${config.styleguideDir}. Something else interesting.`,
    );
  },

  /** webpack and other settings **/
  ignore: ['**/*.style.{js,jsx,ts,tsx}'],
  webpackConfig: {
    module: {
      rules: [
        {
          test: /\.(js|jsx)?$/,
          exclude: [/node_modules/],
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['babel-preset-gatsby'],
              plugins: ['babel-plugin-transform-imports'],
            },
          },
        },
        {
          test: /\.css$/,
          use: ['style-loader', 'css-loader'],
        },
      ],
    },
    resolve: {
      fallback: {
        url: false,
      },
      extensions: ['.js', '.jsx'],
      alias: {
        '@macanta': path.join(__dirname, 'src'),
        '@mui/material': path.join(__dirname, 'node_modules/@material-ui/core'),
        '@mui/icons-material': path.join(
          __dirname,
          'node_modules/@material-ui/icons',
        ),
        '@mui/x-date-pickers': path.join(
          __dirname,
          'node_modules/@material-ui/lab',
        ),
        '@tests': path.join(__dirname, 'tests'),
      },
    },
  },
};
