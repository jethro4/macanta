import React, {useState} from 'react';
import {useQuery, useMutation} from '@apollo/client';
import VisibilityIcon from '@mui/icons-material/Visibility';
import LinkIcon from '@mui/icons-material/Link';
import CodeIcon from '@mui/icons-material/Code';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import Box from '@mui/material/Box';
import {
  ModalButton,
  PopoverButton,
  DialogButton,
  IconButton,
} from '@macanta/components/Button';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import {GET_FORM_BUILDER, DELETE_FORM_BUILDER} from '@macanta/graphql/admin';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import BuilderRoot from './Forms/BuilderRoot';
import EmbedCode from './Forms/EmbedCode';
import CopyButton from './Forms/CopyButton';

const FormBuilderActionButtons = ({item}) => {
  const {displayMessage} = useProgressAlert();

  const [
    callDeleteFormBuilderMutation,
    deleteFormBuilderMutation,
  ] = useMutation(DELETE_FORM_BUILDER, {
    update(cache, {data: {deleteFormBuilder}}) {
      const normalizedId = cache.identify({
        id: deleteFormBuilder.id,
        __typename: 'FormBuilder',
      });
      cache.evict({id: normalizedId});
      cache.gc();
    },
    onCompleted() {
      displayMessage(`Deleted form successfully!`);
    },
  });

  const handleDelete = async (id) => {
    await callDeleteFormBuilderMutation({
      variables: {
        deleteFormBuilderInput: {
          id,
        },
        __mutationkey: 'deleteFormBuilderInput',
        removeAppInfo: true,
      },
    });
  };

  return (
    <>
      <Box
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <ModalButton
          icon
          size="small"
          // onClick={handleAdd}
          TooltipProps={{
            title: 'Show Details',
          }}
          ModalProps={{
            headerTitle: `Form Builder (${item.id})`,
            // hideHeader: true,
            contentWidth: 1600,
            contentHeight: '100%',
            bodyStyle: {
              backgroundColor: '#f5f6fa',
            },
          }}
          renderContent={(handleClose) => (
            <BuilderRoot onClose={handleClose} item={item} />
          )}>
          <VisibilityIcon
            sx={{
              color: 'info.main',
            }}
          />
        </ModalButton>
        <EmbedCodeOrLinkedPopover item={item} />
        <DialogButton
          {...(item.id === 'co_customfields' && {
            style: {
              visibility: 'hidden',
            },
          })}
          icon
          size="small"
          TooltipProps={{
            title: 'Delete',
          }}
          DialogProps={{
            title: 'Are you sure?',
            description: [
              `You are about to delete "${item.title}" from Form Builders list.`,
              'This cannot be undone. Do you want to proceed?',
            ],
            onActions: (handleClose) => {
              return [
                {
                  label: 'Delete',
                  onClick: async () => {
                    await handleDelete(item.id);
                    handleClose();
                  },
                  startIcon: <DeleteForeverIcon />,
                },
              ];
            },
          }}>
          <DeleteForeverIcon
            sx={{
              color: 'grayDarker.main',
            }}
          />
        </DialogButton>
      </Box>
      <LoadingIndicator modal loading={deleteFormBuilderMutation.loading} />
    </>
  );
};

const EmbedCodeOrLinkedPopover = ({item}) => {
  const [initialized, setInitialized] = useState(false);

  const formBuildersQuery = useQuery(GET_FORM_BUILDER, {
    variables: {
      id: item?.id,
    },
    skip: !initialized || !item?.id,
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const formBuilder = formBuildersQuery.data?.getFormBuilder;

  const initLoading = !formBuilder && formBuildersQuery.loading;

  const handleClick = () => {
    setInitialized(true);
  };

  return !initLoading ? (
    <>
      {item.type === '2-Way Synced' &&
        (initialized ? (
          <CopyButton
            open
            icon
            size="small"
            label=""
            text={formBuilder.link}
            tooltipTitle="Copied link"
            TooltipProps={{
              placement: 'bottom',
            }}>
            <LinkIcon
              sx={{
                color: 'grayDarker.main',
              }}
            />
          </CopyButton>
        ) : (
          <IconButton
            size="small"
            // TooltipProps={{title: 'Delete'}}
            onClick={handleClick}>
            <LinkIcon
              sx={{
                color: 'grayDarker.main',
              }}
            />
          </IconButton>
        ))}
      {item.type !== '2-Way Synced' && (
        <PopoverButton
          icon
          size="small"
          TooltipProps={{
            title: 'Embed Code',
          }}
          PopoverProps={{
            title: 'Embed Code',
            contentWidth: 500,
            bodyStyle: {
              padding: '1rem',
            },
          }}
          renderContent={() => <EmbedCodePopover item={item} />}>
          <CodeIcon
            sx={{
              color: 'grayDarker.main',
            }}
          />
        </PopoverButton>
      )}
    </>
  ) : (
    <LoadingIndicator size={18} transparent marginAuto loading={initLoading} />
  );
};

const EmbedCodePopover = ({item}) => {
  const formBuildersQuery = useQuery(GET_FORM_BUILDER, {
    variables: {
      id: item?.id,
    },
    skip: !item?.id,
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const formBuilder = formBuildersQuery.data?.getFormBuilder;

  const initLoading = !formBuilder && formBuildersQuery.loading;

  return !initLoading && formBuilder?.jsCode ? (
    <EmbedCode
      jsCode={formBuilder.jsCode}
      iFrameCode={formBuilder.iFrameCode}
      htmlCode={formBuilder.htmlCode}
    />
  ) : (
    <LoadingIndicator marginAuto loading={initLoading} />
  );
};

export default FormBuilderActionButtons;
