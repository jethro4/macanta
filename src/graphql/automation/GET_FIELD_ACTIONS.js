import {gql} from '@apollo/client';

export const FIELD_ACTIONS_FRAGMENT = gql`
  fragment fieldActionsFragment on FieldActionItems {
    queryId
    queryType
    queryDescription
    queryConnectedDataType
    queryContactRelationship
    queryName
    actionType
    queryCDFieldName
    queryCDFieldName1
    queryCDFieldName2
    queryCDFieldNameResult
    birthdayResultFormat
    queryCDFieldNameFormat
    dateNumber
    dateDay
    dateMonth
    queryCDRequestdate
    dayCount
    weekCount
    monthCount
    yearCount
    queryContactListId
  }
`;

export default gql`
  query getFieldActions($id: ID) {
    getFieldActions(id: $id) {
      action
      status
      count
      items {
        ...fieldActionsFragment
      }
    }
  }
  ${FIELD_ACTIONS_FRAGMENT}
`;
