import React, {useState} from 'react';
import TurnedInIcon from '@mui/icons-material/TurnedIn';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import Button from '@macanta/components/Button';
import FormField from '@macanta/containers/FormField';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {useQuery, useMutation} from '@apollo/client';
import {
  LIST_INTERNAL_SUPPORTS,
  CREATE_OR_UPDATE_INTERNAL_SUPPORTS,
} from '@macanta/graphql/admin';
import * as NoteTaskFormsStyled from '@macanta/modules/NoteTaskForms/styles';
import * as applicationStyles from '@macanta/themes/applicationStyles';
import * as QueryBuilderStyled from '@macanta/modules/AdminSettings/QueryBuilder/styles';

const AddEmailForms = ({item, onSave, ...props}) => {
  const {displayMessage} = useProgressAlert();

  const [email, setEmail] = useState(item?.email);
  const [fullName, setFullName] = useState(item?.fullName);

  const isEdit = !!item;

  const internalSupportsQuery = useQuery(LIST_INTERNAL_SUPPORTS, {
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const internalSupports = internalSupportsQuery.data?.listInternalSupports;

  const [
    createOrUpdateInternalSupports,
    updateAppSettingsMutation,
  ] = useMutation(CREATE_OR_UPDATE_INTERNAL_SUPPORTS, {
    onError() {},
  });

  const handleSave = async () => {
    const {data} = await createOrUpdateInternalSupports({
      variables: {
        createOrUpdateInternalSupportsInput: {
          internalSupports: [{email, fullName}],
        },
        __mutationkey: 'createOrUpdateInternalSupportsInput',
        removeAppInfo: true,
      },
    });

    if (data?.createOrUpdateInternalSupports?.success) {
      let updatedInternalSupports = [];

      if (internalSupports.some((support) => support.email === email)) {
        updatedInternalSupports = internalSupports.map((support) => {
          if (support.email === email) {
            return {
              email,
              fullName,
            };
          }

          return support;
        });
      } else {
        updatedInternalSupports = [...internalSupports, {email, fullName}];
      }

      internalSupportsQuery.client.writeQuery({
        query: LIST_INTERNAL_SUPPORTS,
        data: {
          listInternalSupports: updatedInternalSupports,
        },
      });

      displayMessage('Saved successfully!');

      onSave();
    }
  };

  return (
    <QueryBuilderStyled.FormRoot
      style={{
        backgroundColor: 'white',
      }}
      {...props}>
      <QueryBuilderStyled.Body
        style={{
          padding: '1rem',
          overflow: 'hidden',
        }}>
        <FormField
          autoFocus
          disabled={isEdit}
          onChange={setEmail}
          label="Email"
          value={email}
          fullWidth
          size="small"
          variant="outlined"
        />

        <FormField
          onChange={setFullName}
          label="Full Name"
          value={fullName}
          fullWidth
          size="small"
          variant="outlined"
        />

        <LoadingIndicator modal loading={updateAppSettingsMutation.loading} />
      </QueryBuilderStyled.Body>
      <NoteTaskFormsStyled.Footer
        sx={applicationStyles.footer}
        style={{
          padding: '1rem',
          margin: 0,
          borderTop: '1px solid #eee',
          width: '100%',
          backgroundColor: 'white',
        }}>
        <Box
          style={{
            flex: 1,
            display: 'flex',
            justifyContent: 'center',
          }}>
          {!!updateAppSettingsMutation.error && (
            <Typography
              color="error"
              style={{
                fontSize: '0.75rem',
                textAlign: 'center',
                marginRight: '1rem',
              }}>
              {updateAppSettingsMutation.error.message}
            </Typography>
          )}
        </Box>
        <Button
          disabled={!email || !fullName || !internalSupports}
          variant="contained"
          startIcon={<TurnedInIcon />}
          onClick={handleSave}
          size="medium">
          Save
        </Button>
      </NoteTaskFormsStyled.Footer>
    </QueryBuilderStyled.FormRoot>
  );
};

export default AddEmailForms;
