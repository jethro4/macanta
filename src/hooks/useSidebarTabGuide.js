import {useState, useLayoutEffect} from 'react';
import useUserGuides from '@macanta/hooks/useUserGuides';

const useSidebarTabGuide = (tab) => {
  const [sidebarTabGuide, setSidebarTab] = useState();
  const {sidebarTabs} = useUserGuides();

  useLayoutEffect(() => {
    setSidebarTab(sidebarTabs.find((guide) => guide.subType === tab));
  }, [sidebarTabs]);

  return sidebarTabGuide;
};

export default useSidebarTabGuide;
