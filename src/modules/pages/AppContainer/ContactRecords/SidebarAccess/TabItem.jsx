import React from 'react';
import {useMatch} from '@reach/router';
import ListItemSecondaryAction from '@mui/material/ListItemSecondaryAction';
import {getTabLink} from './SidebarAccess';
import SidebarTabGuideBtn from './SidebarTabGuideBtn';
import * as Styled from './styles';

const TabItem = ({contactId, id, type, title, ...props}) => {
  const tabLink = getTabLink(contactId, id, type, title);
  const selected = useMatch(tabLink);

  const handleStopMouseEvent = (event) => {
    event.preventDefault();
  };

  return (
    <Styled.ListItem
      onMouseDown={handleStopMouseEvent}
      selected={selected}
      button
      to={tabLink}
      {...props}>
      <Styled.ListItemText primary={title} />
      <ListItemSecondaryAction>
        <SidebarTabGuideBtn tab={title} />
      </ListItemSecondaryAction>
    </Styled.ListItem>
  );
};

export default TabItem;
