import React from 'react';
import HelperTextBtn from '@macanta/containers/FormField/HelperTextBtn';
import useSearchGuide from '@macanta/hooks/useSearchGuide';

const SearchGuideBtn = () => {
  const searchGuide = useSearchGuide();

  return <HelperTextBtn>{searchGuide?.value}</HelperTextBtn>;
};

export default SearchGuideBtn;
