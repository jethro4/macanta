import React from 'react';
import SelectAction from '@macanta/modules/AdminSettings/Automation/TriggerActions/SubForms/SelectAction';
import {useEmailActions} from '@macanta/hooks/automation';

const Select = ({...props}) => {
  const emailActionsQuery = useEmailActions();

  return (
    <SelectAction
      loading={emailActionsQuery.initLoading}
      type="Select"
      options={emailActionsQuery.result}
      labelKey="name"
      valueKey="id"
      {...props}
    />
  );
};

export default Select;
