import useRetryQuery from '@macanta/hooks/apollo/useRetryQuery';
import {LIST_CONTACTS} from '@macanta/graphql/contacts';
import {
  DEFAULT_PAGE,
  DEFAULT_PAGE_LIMIT,
  DEFAULT_ORDER,
  DEFAULT_ORDER_BY,
} from '@macanta/modules/hoc/ContactsQuery';

const useContacts = (
  {
    q,
    page = DEFAULT_PAGE,
    limit = DEFAULT_PAGE_LIMIT,
    order = DEFAULT_ORDER,
    orderBy = DEFAULT_ORDER_BY,
  },
  options,
) => {
  const variables = {
    q,
    page,
    limit,
    order,
    orderBy,
  };

  const contactsQuery = useRetryQuery(LIST_CONTACTS, {
    variables,
    ...options,
  });

  const contacts = contactsQuery.data?.listContacts?.items;

  const total = contactsQuery.data?.listContacts?.total;

  return {...contactsQuery, contacts, total};
};

export default useContacts;
