import React from 'react';
import CheckSessionExpiry from './CheckSessionExpiry';
import LoadLoggedInContact from './LoadLoggedInContact';
import LoadSignature from './LoadSignature';
import LoadUsers from './LoadUsers';
import LoadUsetiful from './LoadUsetiful';
import LoadNylasInfo from './LoadNylasInfo';

const LoggedInBootstrap = () => {
  return (
    <>
      <CheckSessionExpiry />
      <LoadUsetiful />
      <LoadNylasInfo />
      <LoadLoggedInContact />
      <LoadSignature />
      <LoadUsers />
    </>
  );
};

export default LoggedInBootstrap;
