import React, {forwardRef} from 'react';
import {DropzoneArea, DropzoneDialog} from 'material-ui-dropzone';

let Dropzone = ({dialog, ...props}, ref) => {
  const single = props.filesLimit === 1;

  const dropzoneText = `Click here or drag ${
    single ? 'a single file' : 'multiple files'
  }`;

  return !dialog ? (
    <DropzoneArea
      ref={ref}
      classes={{
        root: 'MuiDropzoneArea-root',
      }}
      dropzoneText={dropzoneText}
      {...props}
    />
  ) : (
    <DropzoneDialog ref={ref} dropzoneText={dropzoneText} {...props} />
  );
};

Dropzone = forwardRef(Dropzone);

Dropzone.defaultProps = {
  maxFileSize: 1000000000,
  filesLimit: 50,
  single: false,
  showPreviews: false,
  showPreviewsInDropzone: false,
  //TODO: Remove showAlerts attribute once scrollTop error is resolved on https://github.com/Yuvaleros/material-ui-dropzone/issues/265
  showAlerts: false,
};

export default Dropzone;
