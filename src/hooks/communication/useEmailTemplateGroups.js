import useValue from '@macanta/hooks/base/useValue';
import useQuery from '@macanta/hooks/apollo/useQuery';
import {LIST_EMAIL_TEMPLATE_GROUPS} from '@macanta/graphql/communication';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';

const useEmailTemplateGroups = (permission) => {
  const emailTemplateGroupsQuery = useQuery(LIST_EMAIL_TEMPLATE_GROUPS, {
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const templateGroups =
    emailTemplateGroupsQuery.data?.listEmailTemplateGroups?.items;
  const permittedEmailTemplates = permission?.emailTemplates;

  const [data] = useValue(() => {
    return (
      templateGroups?.map((item) => {
        return {
          ...item,
          hasAccess: permittedEmailTemplates?.some(
            (pItem) => pItem.templateGroupId === item.id,
          ),
        };
      }) || []
    );
  }, [templateGroups, permittedEmailTemplates]);

  return {
    ...emailTemplateGroupsQuery,
    data,
  };
};

export default useEmailTemplateGroups;
