import {gql} from '@apollo/client';
import {EMAIL_ACTIONS_FRAGMENT} from '@macanta/graphql/automation/GET_EMAIL_ACTION';

export default gql`
  query getEmailActions {
    getEmailActions {
      action
      status
      count
      items {
        ...emailActionsFragment
      }
    }
  }
  ${EMAIL_ACTIONS_FRAGMENT}
`;
