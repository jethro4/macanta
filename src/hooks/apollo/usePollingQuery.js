import {useState, useEffect} from 'react';
import moment from 'moment';
import {useReactiveVar, useQuery} from '@apollo/client';
import {sessionExpiredVar} from '@macanta/graphql/cache/vars';
import {isLoggedIn} from '@macanta/utils/app';

const usePollingQuery = (graphqlTag, options, pollInterval = 45000) => {
  const sessionExpired = useReactiveVar(sessionExpiredVar);

  const [skip, setSkip] = useState(sessionExpired);

  const pollingQuery = useQuery(graphqlTag, {
    ...options,
    pollInterval,
    skip: skip || sessionExpired,
  });

  useEffect(() => {
    let timeStart = moment();

    const logData = () => {
      if (document.visibilityState !== 'hidden') {
        setSkip(false);

        const hiddenDuration = moment().diff(timeStart, 'second');

        if (hiddenDuration >= 30) {
          setTimeout(() => {
            pollingQuery.refetch();
          }, 0);
        }
      } else {
        setSkip(true);

        timeStart = moment();
      }
    };

    if (isLoggedIn()) {
      document.addEventListener('visibilitychange', logData);
    }

    return () => {
      document.removeEventListener('visibilitychange', logData);
    };
  }, []);

  return pollingQuery;
};

export default usePollingQuery;
