import {useState, useLayoutEffect} from 'react';
import useUserGuides from '@macanta/hooks/useUserGuides';

const useDOSectionGuide = (sectionName) => {
  const [doSectionGuide, setDOSection] = useState();
  const {doSections} = useUserGuides();

  useLayoutEffect(() => {
    setDOSection(doSections.find((guide) => guide.subType === sectionName));
  }, [doSections]);

  return doSectionGuide;
};

export default useDOSectionGuide;
