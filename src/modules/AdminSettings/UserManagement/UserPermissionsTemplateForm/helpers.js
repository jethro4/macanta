import groupBy from 'lodash/groupBy';
import {ACCESS_LEVELS} from '@macanta/constants/accessLevels';

const transformAccessLevel = (permission) => {
  switch (permission) {
    case ACCESS_LEVELS.READ_WRITE:
      return 'ReadWrite';
    case ACCESS_LEVELS.READ_ONLY:
      return 'ReadOnly';
    default:
      return 'NoAccess';
  }
};

const permissionKeys = [
  {
    key: 'tabs',
    transform: (d) =>
      d.reduce((acc, item) => {
        acc[item.groupId] = transformAccessLevel(item.permission);

        return acc;
      }, {}),
  },
  {
    key: 'sections',
    transform: (d) =>
      d.reduce((acc, item) => {
        acc[item.groupId] = item.access.reduce((acc2, item2) => {
          acc2[item2.name] = transformAccessLevel(item2.permission);

          return acc2;
        }, {});

        return acc;
      }, {}),
  },
  {
    key: 'mqbs',
    transform: (d) =>
      d.reduce((acc, item) => {
        acc[item.queryId] = transformAccessLevel(item.permission);

        return acc;
      }, {}),
  },
  {
    key: 'tasks',
    transform: (d) =>
      d.reduce((acc, item) => {
        acc[item.taskId] = item.allow ? 'yes' : 'no';

        if (item.taskId === 'SpecificUserTask') {
          acc['UserTask'] = item.specificIds;
        }

        return acc;
      }, {}),
  },
  {
    key: 'emailTemplates',
    transform: (d) =>
      d.reduce((acc, item) => {
        acc[item.templateGroupId] = item.name;

        return acc;
      }, {}),
  },
  {
    key: 'mqbWidgetAccess',
    transform: (d) =>
      d.reduce((acc, item) => {
        acc[item.queryId] = item.show ? 'checked' : 'unchecked';

        return acc;
      }, {}),
  },
  {
    key: 'mqbWidgetEmail',
    transform: (d) =>
      d.reduce((acc, item) => {
        acc[item.queryId] = item.show ? 'checked' : 'unchecked';

        return acc;
      }, {}),
  },
  {
    key: 'contacts',
    transform: (d) =>
      d.reduce((acc, item) => {
        if (item.groupId === 'Global') {
          acc[item.groupId] = transformAccessLevel(item.access[0].permission);
        } else {
          acc[item.groupId] = item.access.reduce((acc2, item2) => {
            acc2[item2.name] = transformAccessLevel(item2.permission);

            return acc2;
          }, {});
        }

        return acc;
      }, {}),
  },
  {
    key: 'workflowBoards',
    transform: (d) =>
      d.reduce((acc, item) => {
        acc[item.boardId] = item.scope;

        return acc;
      }, {}),
  },
  {
    key: 'permissionMeta',
    transform: (d, doTypes) => {
      const transformedData = {
        ActionButton:
          doTypes?.reduce((acc, item) => {
            const status = d.hideActionButtonsForGroups?.includes(item.id)
              ? 'Hidden'
              : 'Show';
            acc[item.id] = status;

            return acc;
          }, {}) || {},
        ConnectButton:
          doTypes?.reduce((acc, item) => {
            const status = d.hideConnectButtonsForGroups?.includes(item.id)
              ? 'Hidden'
              : 'Show';
            acc[item.id] = status;

            return acc;
          }, {}) || {},
        PurchaseButton: d.hidePurchaseButton ? 'Hidden' : 'Show',
        AllowEmailSync: d.allowEmailSync ? 'yes' : 'no',
        AllowAddRemoveEmailDomainFilter: d.allowAddRemoveEmailDomainFilter
          ? 'yes'
          : 'no',
        AllowUserToMergeContacts: d.allowUserToMergeContacts ? 'yes' : 'no',
        AllowUserDeleteContact: d.allowUserDeleteContact ? 'yes' : 'no',
        AllowUserDeleteDOItem: d.allowUserDeleteDOItem ? 'yes' : 'no',
        AllowUserAttachment: d.allowUserAttachment ? 'yes' : 'no',
        AllowUserDeleteDOAttachment: d.allowUserDeleteDOAttachment
          ? 'yes'
          : 'no',
        AllowContactExport: d.allowContactExport ? 'yes' : 'no',
        AllowDataObjectExport: d.allowDataObjectExport ? 'yes' : 'no',
        AllowNoteTaskExport: d.allowNoteTaskExport ? 'yes' : 'no',
        AllowAddDirectRelationship: d.allowAddDirectRelationship ? 'yes' : 'no',
        UserDOEditPermission: {
          WithOtherUserDataToggle: d.userDOEditPermission
            ?.withOtherUserDataToggle
            ? 'yes'
            : 'no',
          WithOtherUserRelationshipToggle: d.userDOEditPermission
            ?.withOtherUserRelationshipToggle
            ? 'yes'
            : 'no',
          WithOtherUserRelationshipDataToggle: d.userDOEditPermission
            ?.withOtherUserRelationshipDataToggle
            ? 'yes'
            : 'no',
          WithOtherUserRelationshipRelationshipToggle: d.userDOEditPermission
            ?.withOtherUserRelationshipRelationshipToggle
            ? 'yes'
            : 'no',
          OtherUserRelationship:
            d.userDOEditPermission?.otherUserRelationship || [],
        },
      };

      return transformedData;
    },
  },
  {
    key: 'userGuides',
    transform: (data) => {
      const groupedItemsObj = groupBy(data, 'type');

      return Object.entries(groupedItemsObj).reduce(
        (acc, [key, groupedItems]) => {
          const accObj = {...acc};

          if (key === 'DataObject') {
            const groupedDOItemsObj = groupBy(
              groupedItems.map((group) => ({
                ...group,
                doType: group.meta.find((m) => m.name === 'doType')?.value,
              })),
              'doType',
            );

            accObj[key] = Object.entries(groupedDOItemsObj).reduce(
              (acc2, [doType, doGroupedItems]) => {
                const acc2Obj = {...acc2};

                acc2Obj[doType] = doGroupedItems.reduce((acc3, d) => {
                  const acc3Obj = {...acc3};

                  acc3Obj[d.subType] = d.value;

                  return acc3Obj;
                }, {});

                return acc2Obj;
              },
              {},
            );
          } else {
            accObj[key] = groupedItems.reduce((acc2, d) => {
              const acc2Obj = {...acc2};

              acc2Obj[d.subType] = d.value;

              return acc2Obj;
            }, {});
          }

          return accObj;
        },
        {},
      );
    },
  },
];

export const formatVariables = ({
  id,
  name,
  description,
  permission,
  doTypes,
}) => {
  const varsObj = {
    id,
    name,
    description,
    data: permissionKeys
      .filter(({key}) => permission?.[key])
      .map(({key, transform}) => {
        const data = permission[key];
        const value = transform(data, doTypes);
        let updatedKey;

        switch (key) {
          case 'mqbs': {
            updatedKey = 'mqbAccess';

            break;
          }
          case 'tabs': {
            updatedKey = 'tabAccess';

            break;
          }
          case 'sections': {
            updatedKey = 'sectionAccess';

            break;
          }
          case 'tasks': {
            updatedKey = 'taskAccess';

            break;
          }
          case 'contacts': {
            updatedKey = 'contactViewPermissions';

            break;
          }
          case 'emailTemplates': {
            updatedKey = 'inlineEmailTemplateAccess';

            break;
          }
          default: {
            updatedKey = key;
          }
        }

        return {
          key: updatedKey,
          jsonValue: JSON.stringify(value),
        };
      }),
  };

  return varsObj;
};
