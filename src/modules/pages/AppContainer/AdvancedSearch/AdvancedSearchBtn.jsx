import React from 'react';
import FilterListIcon from '@mui/icons-material/FilterList';
import {PopoverButton} from '@macanta/components/Button';
import AdvancedSearchForms from './AdvancedSearchForms';

const AdvancedSearchBtn = (props) => {
  return (
    <PopoverButton
      id={`advanced-search-button`}
      startIcon={<FilterListIcon />}
      size="small"
      variant="contained"
      PopoverProps={{
        title: 'Advanced Search',
        contentWidth: 1400,
        contentHeight: '88vh',
        anchorOrigin: {
          vertical: 'bottom',
          horizontal: 'center',
        },
        transformOrigin: {
          vertical: -10,
          horizontal: 420,
        },
      }}
      renderContent={() => <AdvancedSearchForms />}
      {...props}>
      Advanced Search
    </PopoverButton>
  );
};

export default AdvancedSearchBtn;
