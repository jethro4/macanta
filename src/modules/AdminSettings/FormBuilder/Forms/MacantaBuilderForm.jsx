import React from 'react';
import {useFormikContext} from 'formik';
import FilterListIcon from '@mui/icons-material/FilterList';
import Divider from '@mui/material/Divider';
import Badge from '@mui/material/Badge';
import Typography from '@mui/material/Typography';
import Button, {PopoverButton} from '@macanta/components/Button';
import DOFiltersForm from '@macanta/modules/DataObjectItems/DOFiltersForm';
import {insertOrUpdate, remove} from '@macanta/utils/array';
import FieldDragSection from './FieldDragSection';
import BuilderFieldsForm from './BuilderFieldsForm';
import AddGroupForm from './AddGroupForm';
import * as Styled from './styles';

const MacantaBuilderForm = ({doTypeTitle, contactGroups, doGroups}) => {
  const {values, errors, setValues} = useFormikContext();

  const isEdit = values.selectedMode === 'Edit';
  const isContactsOnly = values.doType === 'contactsOnly';
  const doTypeId = values.doType;

  const handleSort = (type) => (groupId, sortData) => {
    setValues((state) => {
      return {
        ...state,
        ...(type === 'contact' && {
          contactGroups: state.contactGroups.map((group) => {
            if (group.id === groupId) {
              const {sortedLayout} = sortData;

              return {
                ...group,
                fields: sortedLayout.map((l) => {
                  const field = group.fields.find((f) => f.fieldId === l.i);

                  return {
                    ...field,
                    column: l.x + 1,
                  };
                }),
              };
            }

            return group;
          }),
        }),
        ...(type === 'dataObject' && {
          doGroups: state.doGroups.map((group) => {
            if (group.id === groupId) {
              const {sortedLayout} = sortData;

              return {
                ...group,
                fields: sortedLayout.map((l) => {
                  const field = group.fields.find((f) => f.fieldId === l.i);

                  return {
                    ...field,
                    column: l.x + 1,
                  };
                }),
              };
            }

            return group;
          }),
        }),
      };
    });
  };

  const handleDelete = (type) => (formType, id) => {
    if (formType === 'field') {
      setValues((state) => {
        return {
          ...state,
          ...(type === 'contact' && {
            contactGroups: state.contactGroups.map((group) => ({
              ...group,
              fields: remove({
                arr: group.fields,
                key: 'fieldId',
                value: id,
              }),
            })),
          }),
          ...(type === 'dataObject' && {
            doGroups: state.doGroups.map((group) => ({
              ...group,
              fields: remove({
                arr: group.fields,
                key: 'fieldId',
                value: id,
              }),
            })),
          }),
        };
      });
    } else if (formType === 'group') {
      setValues((state) => {
        return {
          ...state,
          ...(type === 'contact' && {
            contactGroups: remove({
              arr: state.contactGroups,
              key: 'subGroupName',
              value: id,
            }),
          }),
          ...(type === 'dataObject' && {
            doGroups: remove({
              arr: state.doGroups,
              key: 'subGroupName',
              value: id,
            }),
          }),
        };
      });
    }
  };

  const handleAddGroup = (type) => ({id, ...updatedItem}, position) => {
    setValues((state) => {
      let groupsObj = {};

      if (type === 'contact') {
        groupsObj = {
          contactGroups: insertOrUpdate({
            arr: state.contactGroups,
            item: {
              id: id || String(Date.now()),
              ...updatedItem,
            },
            key: 'id',
            index: position - 1,
            includeAll: true,
          }),
        };

        groupsObj.contactGroups = groupsObj.contactGroups.map((group) => ({
          ...group,
          fields: group.fields.map((field) => ({
            ...field,
            column: Math.min(field.column, parseInt(updatedItem.columns)),
          })),
        }));
      } else if (type === 'dataObject') {
        groupsObj = {
          doGroups: insertOrUpdate({
            arr: state.doGroups,
            item: {
              id: id || String(Date.now()),
              ...updatedItem,
            },
            key: 'id',
            index: position - 1,
            includeAll: true,
          }),
        };

        groupsObj.doGroups = groupsObj.doGroups.map((group) => ({
          ...group,
          fields: group.fields.map((field) => ({
            ...field,
            column: Math.min(field.column, parseInt(updatedItem.columns)),
          })),
        }));
      }

      return {
        ...state,
        ...groupsObj,
      };
    });
  };

  const handleAdd = (type, subGroupName) => (fields) => {
    setValues((state) => {
      return {
        ...state,
        ...(type === 'contact' && {
          contactGroups: state.contactGroups.map((group) => {
            if (group.subGroupName === subGroupName) {
              return {
                ...group,
                fields: group.fields.concat(fields),
              };
            }

            return group;
          }),
        }),
        ...(type === 'dataObject' && {
          doGroups: state.doGroups.map((group) => {
            if (group.subGroupName === subGroupName) {
              return {
                ...group,
                fields: group.fields.concat(fields),
              };
            }

            return group;
          }),
        }),
      };
    });
  };

  return (
    <Styled.FormsArea>
      <div id="MacantaWebform">
        <Typography className="form-title">{values.title}</Typography>
        <Divider
          style={{
            margin: '12px 0',
            borderBottom: '1px dashed #eee',
          }}
        />
        <div id="MacantaForm">
          <FieldDragSection
            isEdit={isEdit}
            title="Contact Information"
            groups={contactGroups}
            groupError={errors.contactGroups}
            groupFieldError={errors.contactGroupsEmpty}
            onSort={handleSort('contact')}
            onDelete={handleDelete('contact')}
            renderGroupForm={(handleClose, item) => (
              <AddGroupForm
                subGroups={contactGroups.map((group) => group.subGroupName)}
                item={item}
                onAdd={handleAddGroup('contact')}
                onClose={handleClose}
              />
            )}
            renderFieldsForm={(handleClose, subGroupName, maxColumn) => (
              <BuilderFieldsForm
                maxColumn={maxColumn}
                selectedFields={contactGroups.reduce((acc, group) => {
                  return acc.concat(group.fields);
                }, [])}
                isContactsOnly
                onAdd={handleAdd('contact', subGroupName)}
                onClose={handleClose}
                shouldAddEmail
              />
            )}
          />
          {!isContactsOnly && doTypeId && (
            <>
              <Divider
                style={{
                  margin: '1rem 0',
                  borderBottom: '1px dashed #eee',
                }}
              />
              <FieldDragSection
                isEdit={isEdit}
                title={doTypeTitle}
                groups={doGroups}
                groupError={errors.doGroups}
                groupFieldError={errors.doGroupsEmpty}
                onSort={handleSort('dataObject')}
                onDelete={handleDelete('dataObject')}
                renderGroupForm={(handleClose, item) => (
                  <AddGroupForm
                    subGroups={doGroups.map((group) => group.subGroupName)}
                    item={item}
                    onAdd={handleAddGroup('dataObject')}
                    onClose={handleClose}
                  />
                )}
                renderFieldsForm={(handleClose, subGroupName, maxColumn) => (
                  <BuilderFieldsForm
                    allowUploadField
                    maxColumn={maxColumn}
                    selectedFields={doGroups.reduce((acc, group) => {
                      return acc.concat(group.fields);
                    }, [])}
                    doTypeId={doTypeId}
                    isDataObjectsOnly
                    onAdd={handleAdd('dataObject', subGroupName)}
                    onClose={handleClose}
                    shouldAddIntegrated={values.type === 'Integrated'}
                  />
                )}
                renderActionButtons={(item) =>
                  item.fieldId !== 'field_integration' ? null : (
                    <>
                      {!!errors.integrationField && (
                        <Typography
                          color="error"
                          style={{
                            fontSize: '0.6875rem',
                            lineHeight: '0.6875rem',
                            marginRight: '0.25rem',
                          }}>
                          {errors.integrationField}
                        </Typography>
                      )}
                      <Badge
                        // showZero
                        anchorOrigin={{
                          vertical: 'top',
                          horizontal: 'right',
                        }}
                        sx={{
                          '& .MuiBadge-badge': {
                            backgroundColor: 'info.main',
                            color: 'common.white',
                            right: '8px',
                            top: '-2px',
                            minWidth: '16px',
                            width: '16px',
                            height: '15px',
                            fontSize: '10px',
                            lineHeight: '10px',
                          },
                        }}
                        badgeContent={
                          values.integrationField.conditions.length
                        }>
                        <PopoverButton
                          style={{
                            marginRight: '4px',
                            fontWeight: 'bold',
                            lineHeight: '20px',
                          }}
                          // onClick={handleAdd}
                          startIcon={
                            <FilterListIcon
                              style={{
                                fontSize: 15,
                              }}
                            />
                          }
                          variant="text"
                          size="small"
                          color="info"
                          TooltipProps={{
                            title: `Filters`,
                          }}
                          PopoverProps={{
                            title: `Filters`,
                            contenWidth: 600,
                            contentMinWidth: 250,
                            anchorOrigin: {
                              vertical: 'top',
                              horizontal: 'center',
                            },
                            transformOrigin: {
                              vertical: 'bottom',
                              horizontal: 'center',
                            },
                          }}
                          renderContent={(handleClose) => (
                            <DOFiltersForm
                              filters={values.integrationField.conditions}
                              groupId={values.doType}
                              onSubmit={(val) => {
                                setValues((state) => ({
                                  ...state,
                                  integrationField: {
                                    ...state.integrationField,
                                    conditions: val,
                                  },
                                }));

                                handleClose();
                              }}
                            />
                          )}>
                          Filters
                        </PopoverButton>
                      </Badge>
                    </>
                  )
                }
              />
            </>
          )}
        </div>

        {!isEdit && (
          <Button
            style={{
              marginTop: '32px',
              cursor: 'default',
            }}
            sx={{
              '&:hover': {
                backgroundColor: 'primary.main',
              },
            }}
            id="MacantaFormSubmit"
            fullWidth
            variant="contained"
            disableElevation
            disableFocusRipple
            disableRipple>
            {values.submitLabel}
          </Button>
        )}
      </div>
    </Styled.FormsArea>
  );
};

export default MacantaBuilderForm;
