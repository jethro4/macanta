import React from 'react';
import {SelectMenuItems} from '@macanta/components/Forms/ChoiceFields/SelectField';
import {transformValue} from '@macanta/components/Forms/InputFields/helpers';
import useTransformChoices from '@macanta/hooks/base/useTransformChoices';
import useValue from '@macanta/hooks/base/useValue';

const MultiSelectItems = ({
  selectedValue: valueProp,
  options: optionsProp,
  getOptionLabel,
  labelKey,
  valueKey,
  subLabelKey,
  captionKey,
  sort,
  ...props
}) => {
  const [value] = useValue(() => transformValue(valueProp, true), [valueProp]);

  const options = useTransformChoices({
    options: optionsProp,
    sort,
    getOptionLabel,
    labelKey,
    valueKey,
    subLabelKey,
    captionKey,
  });

  const withSubLabel = !!subLabelKey;

  return (
    <SelectMenuItems
      multiple
      selectedValue={value}
      options={options}
      withSubLabel={withSubLabel}
      {...props}
    />
  );
};

MultiSelectItems.defaultProps = {
  selectedValue: [],
};

export default MultiSelectItems;
