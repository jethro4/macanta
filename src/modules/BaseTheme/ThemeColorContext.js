import React from 'react';

const ThemeColorContext = React.createContext({
  setThemeColor: () => null,
});

export default ThemeColorContext;
