import React, {useState, useLayoutEffect, useCallback, useRef} from 'react';
import PersonIcon from '@mui/icons-material/Person';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import isNil from 'lodash/isNil';
import debounce from 'lodash/debounce';
import Modal from '@macanta/components/Modal';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import useLocationChange from '@macanta/hooks/base/useLocationChange';
import useAutocompleteContacts, {
  AUTOCOMPLETE_MINIMUM_SEARCH_LENGTH,
} from '@macanta/hooks/contact/useAutocompleteContacts';
import ContactsSearchTable from './ContactsSearchTable';
import UsersSearchTable from './UsersSearchTable';
import AllDataObjectsSearchTable from './DataObjectsSearchTable/AllDataObjectsSearchTable';
import DataObjectsSearchTable from './DataObjectsSearchTable';
import * as TextFieldStyled from '@macanta/components/Forms/InputFields/styles';
import * as Styled from './styles';

const DEFAULT_CATEGORY = 'contact';

const DEFAULT_CATEGORIES = [
  {
    value: 'contact',
    label: 'Contact',
  },
];

const getOptionLabel = (option) => {
  return !isNil(option?.firstName)
    ? `${option.firstName || ''} ${option.lastName || ''}`.trim()
    : option;
};

const SearchTable = ({category, ...props}) => {
  let searchTable;

  switch (category) {
    case 'contact': {
      searchTable = <ContactsSearchTable {...props} />;
      break;
    }
    case 'users': {
      searchTable = <UsersSearchTable {...props} />;
      break;
    }
    case 'allDataObjects': {
      searchTable = <AllDataObjectsSearchTable {...props} />;
      break;
    }
    default: {
      searchTable = <DataObjectsSearchTable {...props} />;
      break;
    }
  }

  return searchTable;
};

const UniversalSearch = ({
  defaultCategory = DEFAULT_CATEGORY,
  onSelectItem,
  modal,
  columns,
  SearchProps,
  TableProps,
  defaultSearchQuery,
  autoSelect,
  hideSearchAndAdd,
  disableSuggestions,
  ...props
}) => {
  const [showModal, setShowModal] = useState(false);
  const [categories, setCategories] = useState(DEFAULT_CATEGORIES);
  const [selectedCategory, setSelectedCategory] = useState(defaultCategory);
  const [searchQuery, setSearchQuery] = useState(defaultSearchQuery);
  const [loadingAutocomplete, setLoadingAutocomplete] = useState(false);

  const textFieldRef = useRef(null);

  const enableAutocompleteContacts =
    !disableSuggestions && selectedCategory === 'contact';

  const autocompleteContactsQuery = useAutocompleteContacts(searchQuery, {
    skip: !enableAutocompleteContacts,
    onComplete: () => {
      setLoadingAutocomplete(false);
    },
  });
  const doTypesQuery = useDOTypes();

  const listDOTypes = doTypesQuery.data?.listDataObjectTypes;

  const handleAutocompleteContacts = useCallback(
    debounce((newVal) => {
      setSearchQuery({search: newVal});
    }, 500),
    [],
  );

  const handleChangeText = (newVal) => {
    if (newVal.length < AUTOCOMPLETE_MINIMUM_SEARCH_LENGTH) {
      setLoadingAutocomplete(false);
    } else if (selectedCategory === 'contact') {
      setLoadingAutocomplete(true);
    }

    if (enableAutocompleteContacts) {
      handleAutocompleteContacts(newVal);
    }
  };

  const handleClear = () => {
    if (enableAutocompleteContacts) {
      setSearchQuery({search: ''});
    }
  };

  const handleSearch = (value) => {
    setShowModal(true);
    setLoadingAutocomplete(false);
    setSearchQuery({search: value});
  };

  const handleSelectItem = (item) => {
    setLoadingAutocomplete(false);

    onSelectItem(item);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  const handleChangeCategory = (category) => {
    setSelectedCategory(category);
  };

  const handleEnterValue = (value) => {
    if (selectedCategory === 'contact') {
      onSelectItem(value);
    } else {
      handleSearch(value);
    }
  };

  useLayoutEffect(() => {
    if (listDOTypes?.length && categories.length === 1) {
      setCategories(
        categories.concat([
          {
            label: 'All Data Objects',
            value: 'allDataObjects',
            style: {
              fontWeight: 'bold',
            },
          },
          ...listDOTypes.map((doType) => ({
            label: doType.title,
            value: doType.title,
            id: doType.id,
          })),
        ]),
      );
    }
  }, [listDOTypes]);

  useLocationChange(handleCloseModal);

  const selectedDOType = listDOTypes?.find(
    (doType) => doType.title === selectedCategory,
  );

  const selectedCategoryTitle = categories.find(
    (c) => c.value === selectedCategory,
  )?.label;

  let searchTable = !!searchQuery && (
    <SearchTable
      category={selectedCategory}
      searchQuery={searchQuery}
      columns={columns}
      autoSelect={autoSelect}
      onSelectItem={handleSelectItem}
      hideSearchAndAdd={hideSearchAndAdd}
      groupId={selectedDOType?.id}
      doTitle={selectedDOType?.title}
      SearchProps={SearchProps}
      {...TableProps}
    />
  );

  if (modal) {
    searchTable = (
      <Modal
        headerTitle={`Search ${selectedCategoryTitle}`}
        open={showModal}
        onClose={handleCloseModal}
        contentWidth={'100%'}
        contentHeight={'94%'}>
        {searchTable}
      </Modal>
    );
  }

  return (
    <>
      <Styled.Root
        {...props}
        ref={textFieldRef}
        loading={!showModal && loadingAutocomplete}
        disableSuggestions={disableSuggestions}
        onSearch={handleSearch}
        onChange={handleChangeText}
        onClear={handleClear}
        getOptionLabel={getOptionLabel}
        categories={categories}
        defaultCategory={defaultCategory}
        onChangeCategory={handleChangeCategory}
        onEnterValue={handleEnterValue}
        {...(enableAutocompleteContacts && {
          AutoCompleteProps: {
            getOptionLabel: (option) => {
              return option?.email || '';
            },
            filterOptions: () => {
              let contactOptions = autocompleteContactsQuery.data || [];

              return contactOptions;
            },
            renderOption: (liProps, liOption) => {
              // if (liOption.isSubheader) {
              //   return (
              //     <ListSubheader
              //       disableSticky
              //       style={{
              //         backgroundColor: '#F2F2F2',
              //         height: '36px',
              //         lineHeight: '36px',
              //         padding: '0 1rem',
              //       }}>
              //       {liOption.label}
              //     </ListSubheader>
              //   );
              // }
              return (
                <TextFieldStyled.SelectListItem
                  style={{
                    alignItems: 'flex-start',
                    borderBottom: '1px solid #eee',
                  }}
                  {...liProps}
                  key={liOption.id}
                  onClick={() => {
                    textFieldRef.current.blur();

                    handleSelectItem(liOption);
                  }}>
                  <ListItemIcon
                    sx={{
                      color: 'primary.main',
                    }}>
                    <PersonIcon />
                  </ListItemIcon>
                  <ListItemText
                    primary={`${liOption.firstName || ''} ${
                      liOption.lastName || ''
                    }`.trim()}
                    secondary={liOption.email}
                  />
                </TextFieldStyled.SelectListItem>
              );
            },
          },
        })}
      />
      {searchTable}
    </>
  );
};

UniversalSearch.defaultProps = {
  onSelectItem: () => {},
  autoSelect: false,
  modal: false,
};

export default UniversalSearch;
