import React, {useEffect, useRef} from 'react';
import useValue from '@macanta/hooks/base/useValue';
import useInterval from '@macanta/hooks/base/useInterval';
import {textContent} from '@macanta/utils/react';

const DisabledDelay = ({delay, children, ...props}) => {
  const {count, done} = useInterval({
    maxCount: delay,
  });
  const [disabled, setDisabled] = useValue(props.disabled || !done);

  const delayRef = useRef(null);

  useEffect(() => {
    if (delay) {
      delayRef.current = setTimeout(() => {
        setDisabled(true);
      }, delay);
    }

    return () => {
      clearTimeout(delayRef.current);
    };
  }, []);

  let [childrenComp] = useValue(() => {
    const buttonProps = Object.assign(
      {
        disabled,
      },
      !!count && {
        children: `${textContent(children)} (${count})`,
      },
    );

    return React.cloneElement(children, buttonProps);
  }, [disabled, count]);

  return childrenComp;
};

DisabledDelay.defaultProps = {
  defaultExpanded: '',
};

export default DisabledDelay;
