import {gql} from '@apollo/client';

export default gql`
  query listRelationships($groupId: ID) {
    listRelationships(groupId: $groupId) {
      id
      groupId
      role
      description
      exclusive
      limit
      autoAssignLoggedInUser
      autoAssignContact
    }
  }
`;
