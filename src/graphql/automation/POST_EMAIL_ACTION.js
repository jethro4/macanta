import {gql} from '@apollo/client';
import {EMAIL_ACTIONS_FRAGMENT} from '@macanta/graphql/automation/GET_EMAIL_ACTION';

export default gql`
  mutation postEmailAction($input: EmailActionInput!) {
    postEmailAction(input: $input) {
      ...emailActionsFragment
    }
  }
  ${EMAIL_ACTIONS_FRAGMENT}
`;
