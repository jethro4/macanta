import {useEffect} from 'react';
import useValue from '@macanta/hooks/base/useValue';
import useWindowDimensions from '@macanta/hooks/base/useWindowDimensions';
import useTargetRef from '@macanta/hooks/base/useTargetRef';

const DEFAULT_DIMENSIONS = {
  width: 0,
  height: 0,
  top: 0,
  bottom: 0,
  left: 0,
  right: 0,
  x: 0,
  y: 0,
};

const useDimensions = (onResize) => {
  const {anchorEl: elementRef, setRef} = useTargetRef();

  const hasElementRef = !!elementRef;

  const [dimensions, setDimensions] = useValue(
    () => elementRef?.getBoundingClientRect() || DEFAULT_DIMENSIONS,
    [hasElementRef],
  );

  const handleResize = () => {
    if (elementRef) {
      const updatedDimensions =
        elementRef.getBoundingClientRect() || DEFAULT_DIMENSIONS;

      setDimensions(updatedDimensions);

      onResize?.(updatedDimensions);
    }
  };

  useWindowDimensions(handleResize);

  useEffect(handleResize, [dimensions.width, dimensions.height]);

  return {dimensions, setRef, elementRef};
};

export default useDimensions;
