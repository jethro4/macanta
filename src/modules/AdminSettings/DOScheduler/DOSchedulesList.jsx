import React from 'react';
import Box from '@mui/material/Box';
import EditIcon from '@mui/icons-material/Edit';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import useDOSchedules from '@macanta/hooks/scheduler/useDOSchedules';
import DataTable from '@macanta/containers/DataTable';
import {DialogButton, ModalButton} from '@macanta/components/Button';
import DOScheduleForm from '@macanta/modules/AdminSettings/DOScheduler/DOScheduleForm';
import withSectionsData from '@macanta/components/Table/withSectionsData';
import ValueTransform from '@macanta/modules/AdminSettings/DOScheduler/ValueTransform';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import useDORelationships from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDORelationships';
import useDOFields from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOFields';

const DOSchedulesList = ({onSave, onDelete}) => {
  const doSchedulesQuery = useDOSchedules();

  return (
    <SectionsDataTable
      data-testid="DOSchedulesList"
      renderSubheader={({origValue}) => (
        <ValueTransform
          value={origValue}
          labelKey="title"
          valueKey="id"
          selector={useDOTypes}
          options={{
            removeEmail: true,
          }}
        />
      )}
      fullHeight
      loading={doSchedulesQuery.loading}
      headRowHeight={50}
      bodyRowHeight={50}
      noOddRowStripes
      noLimit
      columns={DO_SCHEDULE_COLUMNS}
      data={doSchedulesQuery.data}
      rowsPerPage={25}
      rowsPerPageOptions={[25, 50, 100]}
      handlers={{
        onSave,
        onDelete,
      }}
    />
  );
};

const SectionsDataTable = withSectionsData(DataTable, {groupBy: 'groupId'});

const DO_SCHEDULE_COLUMNS = [
  {
    id: 'name',
    label: 'Name',
    minWidth: 220,
    maxWidth: 270,
  },
  {
    id: 'description',
    label: 'Description',
    minWidth: 240,
    maxWidth: 290,
  },
  {
    id: 'userRelationshipId',
    label: 'User Relationship',
    renderBodyCellValue: ({origValue, item}) => {
      return (
        <ValueTransform
          value={origValue}
          labelKey="role"
          valueKey="id"
          selector={useDORelationships}
          options={item.groupId}
        />
      );
    },
  },
  {
    id: 'contactRelationshipId',
    label: 'Contact Relationship',
    renderBodyCellValue: ({origValue, item}) => {
      return (
        <ValueTransform
          value={origValue}
          labelKey="role"
          valueKey="id"
          selector={useDORelationships}
          options={item.groupId}
        />
      );
    },
  },
  {
    id: 'startDateFieldId',
    label: 'Start Date Field',
    renderBodyCellValue: ({origValue, item}) => {
      return (
        <ValueTransform
          value={origValue}
          labelKey="name"
          valueKey="id"
          selector={useDOFields}
          options={[
            item.groupId,
            {
              removeEmail: true,
              skip: !origValue,
            },
          ]}
        />
      );
    },
  },
  {
    id: 'endDateFieldId',
    label: 'End Date Field',
    renderBodyCellValue: ({origValue, item}) => {
      return (
        <ValueTransform
          value={origValue}
          labelKey="name"
          valueKey="id"
          selector={useDOFields}
          options={[
            item.groupId,
            {
              removeEmail: true,
              skip: !origValue,
            },
          ]}
        />
      );
    },
  },
  {
    id: 'schedulerType',
    label: 'Type',
    minWidth: 120,
    maxWidth: 120,
    transform: ({value}) => {
      return !value || value === 'DateTime' ? 'Date & Time' : 'Date';
    },
  },
  {
    id: 'action',
    label: '',
    disableSort: true,
    disableResize: true,
    minWidth: 100,
    maxWidth: 100,
    renderBodyCellValue: ({item, handlers}) => {
      return (
        <Box
          style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ModalButton
            style={{
              padding: 0,
            }}
            icon
            sx={{
              padding: '0.25rem',
              color: 'info.main',
            }}
            size="small"
            TooltipProps={{
              title: 'Edit',
            }}
            ModalProps={{
              headerTitle: `Edit Schedule (${item.id})`,
              contentWidth: 500,
            }}
            renderContent={(handleClose) => (
              <DOScheduleForm
                item={item}
                onSave={async (values) => {
                  await handlers.onSave(values);

                  handleClose();
                }}
              />
            )}>
            <EditIcon
              style={{
                fontSize: 20,
              }}
            />
          </ModalButton>

          <DialogButton
            icon
            size="small"
            TooltipProps={{
              title: 'Delete',
            }}
            DialogProps={{
              title: 'Are you sure?',
              description: [
                `You are about to delete "${item.name}" from Data Object schedules.`,
                'This cannot be undone. Do you want to proceed?',
              ],
              onActions: (handleClose) => {
                return [
                  {
                    label: 'Delete',
                    onClick: async () => {
                      await handlers.onDelete(item.id);
                      handleClose();
                    },
                    startIcon: <DeleteForeverIcon />,
                  },
                ];
              },
            }}>
            <DeleteForeverIcon
              sx={{
                color: 'grayDarker.main',
              }}
            />
          </DialogButton>
        </Box>
      );
    },
  },
];

export default DOSchedulesList;
