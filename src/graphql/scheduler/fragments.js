import {gql} from '@apollo/client';

export const DO_SCHEDULE_FRAGMENT = gql`
  fragment doScheduleFragment on DOSchedule {
    id
    groupId
    name
    description
    userRelationshipId
    contactRelationshipId
    startDateFieldId
    endDateFieldId
    schedulerType
    dateCreated
  }
`;

export const DO_SCHEDULE_USER_FRAGMENT = gql`
  fragment doScheduleUserFragment on DOScheduleItem {
    id
    userId
    creationDate
    contactId
    type
    actionDate
    title
    note
  }
`;

export const TASK_SCHEDULE_USER_FRAGMENT = gql`
  fragment taskScheduleUserFragment on TaskScheduleItem {
    id
    userId
    creationDate
    contactId
    type
    actionDate
    title
    note
  }
`;
