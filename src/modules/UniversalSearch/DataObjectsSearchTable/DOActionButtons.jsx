import React, {useState, useEffect} from 'react';
import VisibilityIcon from '@mui/icons-material/Visibility';
import FactCheckIcon from '@mui/icons-material/FactCheck';
import DOItemEditSection from './DOItemEditSection';
import {PopoverButton} from '@macanta/components/Button';
import DOItemQueries from './DOItemQueries';
import MoreOptions from './MoreOptions';
import * as Styled from './styles';

const DOActionButtons = ({
  item,
  groupId,
  doFields,
  relationships,
  onSave,
  doTitle,
  closeAfterSave,
}) => {
  const [selectedItem, setSelectedItem] = useState(null);

  const contact = item.connectedContacts[0];

  const handleSave = () => {
    onSave && onSave();
  };

  useEffect(() => {
    if (item && doFields) {
      const data = doFields.map((field) => {
        const origValue = item[field.name];
        const value = Array.isArray(origValue)
          ? origValue[0]?.value
          : origValue;
        const contactSpecificValues =
          item[`${field.name}-contactSpecificValues`];

        return {
          ...field,
          fieldId: field.id,
          value,
          contactSpecificValues,
        };
      });

      setSelectedItem({
        id: item.id,
        data,
        connectedContacts: item.connectedContacts,
      });
    }
  }, [item, doFields]);

  return (
    <>
      <Styled.ActionButtonContainer className="action-buttons-container">
        <PopoverButton
          icon
          sx={{
            padding: '0.25rem',
            color: 'info.main',
          }}
          // onClick={handleAdd}
          size="small"
          TooltipProps={{
            title: 'Show Details',
          }}
          PopoverProps={{
            title: doTitle,
            contentWidth: 1600,
            contentHeight: '86vh',
            anchorOrigin: {
              vertical: 'center',
              horizontal: 'left',
            },
            transformOrigin: {
              vertical: 'center',
              horizontal: 'right',
            },
          }}
          renderContent={(handleClose) => (
            <DOItemEditSection
              loadInfo
              item={selectedItem}
              contact={contact}
              groupId={groupId}
              fields={doFields}
              relationships={relationships}
              onSave={() => {
                closeAfterSave && handleClose();
                handleSave();
              }}
            />
          )}>
          <VisibilityIcon />
        </PopoverButton>
        <PopoverButton
          icon
          sx={{
            color: '#aaa',
            marginRight: '-0.25rem',
            padding: '0.25rem',
          }}
          // onClick={handleAdd}
          size="small"
          TooltipProps={{
            title: 'Queries',
          }}
          PopoverProps={{
            title: 'Queries',
            contentWidth: 300,
            anchorOrigin: {
              vertical: 'center',
              horizontal: 'left',
            },
            transformOrigin: {
              vertical: 'center',
              horizontal: 'right',
            },
          }}
          renderContent={(handleClose) => (
            <DOItemQueries
              item={selectedItem}
              doTitle={doTitle}
              onClose={handleClose}
            />
          )}>
          <FactCheckIcon />
        </PopoverButton>
        <MoreOptions
          style={{
            color: '#aaa',
          }}
          itemId={item.id}
          groupId={groupId}
          fields={doFields}
          relationships={relationships}
          connectedContacts={item.connectedContacts}
          refetch={handleSave}
          doTitle={doTitle}
        />
      </Styled.ActionButtonContainer>
    </>
  );
};

export default DOActionButtons;
