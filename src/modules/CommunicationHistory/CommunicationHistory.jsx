import React, {useState, useEffect, useLayoutEffect} from 'react';
import Box from '@mui/material/Box';
import Button from '@macanta/components/Button';
import ButtonGroup from '@macanta/components/ButtonGroup';
import Pagination from '@macanta/components/Pagination';
import Section from '@macanta/containers/Section';
import DataTable from '@macanta/containers/DataTable';
import useRetryQuery from '@macanta/hooks/apollo/useRetryQuery';
import {LIST_COMMUNICATION_HISTORY} from '@macanta/graphql/contacts';
import useContactId from '@macanta/hooks/useContactId';
import {getBase64Prefix} from '@macanta/utils/file';
import EmailPreview from './EmailPreview';
import EmailStats from './EmailStats';
import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import * as DataObjectsSearchTableStyled from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/styles';
import * as Styled from './styles';

const DEFAULT_EMAIL_COLUMNS = [
  {id: 'status', label: 'Status', minWidth: 170},
  {id: 'message', label: 'Message', minWidth: 170, maxWidth: 370},
  {id: 'sentDateTime', label: 'Sent Date/Time', minWidth: 170},
  {id: 'lastEventReceived', label: 'Last Event Received', minWidth: 170},
  {
    id: 'opens',
    label: 'Opens',
    minWidth: 100,
    maxWidth: 100,
    renderBodyCellValue: ({item}) => {
      return (
        <EmailStats emailId={item.emailId} type="opens">
          {({data}) => <Styled.Stats>{data}</Styled.Stats>}
        </EmailStats>
      );
    },
  },
  {
    id: 'clicks',
    label: 'Clicks',
    minWidth: 100,
    maxWidth: 100,
    renderBodyCellValue: ({item}) => {
      return (
        <EmailStats emailId={item.emailId} type="clicks">
          {({data}) => <Styled.Stats>{data}</Styled.Stats>}
        </EmailStats>
      );
    },
  },
];

const DEFAULT_SMS_COLUMNS = [
  {id: 'status', label: 'Status', minWidth: 170},
  {id: 'message', label: 'Message', minWidth: 170, maxWidth: 370},
  {id: 'lastEventSent', label: 'Last Event Sent', minWidth: 170},
  {id: 'sentBy', label: 'Sent By', minWidth: 170},
];

const COMMUNICATION_HISTORY_TABS = [
  {
    key: 'email',
    label: 'Email History',
  },
  {
    key: 'sms',
    label: 'SMS History',
  },
];

const CommunicationHistoryContainer = (props) => {
  // const notesPermission = useNotesPermissions();

  // return notesPermission.isVisible ? <CommunicationHistory {...props} /> : null;
  return <CommunicationHistory {...props} />;
};

const CommunicationHistory = () => {
  const contactId = useContactId();

  const [selectedTab, setSelectedTab] = useState(
    COMMUNICATION_HISTORY_TABS[0].key,
  );
  const [columns, setColumns] = useState([]);
  const [tableData, setTableData] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [total, setTotal] = useState(0);

  const {data, loading} = useRetryQuery(LIST_COMMUNICATION_HISTORY, {
    variables: {
      contactId,
      type: selectedTab,
      page,
      limit: rowsPerPage,
    },
    onError() {},
  });

  const communicationHistoryItems = data?.listCommunicationHistory?.items;

  const handleSelectTab = (tab) => () => {
    setSelectedTab(tab);
  };

  const transformEmailItemData = (historyData = [], columns) => {
    return historyData.map((item) => {
      const itemObj = columns.reduce(
        (acc, column) => {
          if (column.id === 'status') {
            acc[column.id] = [
              {
                value: item.status,
                renderLabel: () => {
                  return (
                    <Styled.Status success={item.status === 'Delivered'}>
                      {item.status}
                    </Styled.Status>
                  );
                },
              },
            ];
          } else if (column.id === 'message') {
            const to =
              selectedTab === 'email'
                ? `To: ${item.contactName} (${item.email})`
                : `To: ${item.contactName} (${item.phoneNumber})`;
            const message =
              selectedTab === 'email' ? item.subject : item.message;

            acc[column.id] = [
              {
                value: message,
                renderLabel: () => {
                  return (
                    <Box>
                      <Styled.ToFrom>{to}</Styled.ToFrom>
                      <Styled.Text>{message}</Styled.Text>
                    </Box>
                  );
                },
              },
            ];
          } else if (
            ['sentDateTime', 'lastEventReceived', 'lastEventSent'].includes(
              column.id,
            )
          ) {
            const dateTime =
              selectedTab === 'email'
                ? column.id === 'sentDateTime'
                  ? item.sentDateTime
                  : item.lastEventReceived
                : item.lastEventSent;

            acc[column.id] = [
              {
                value: dateTime,
                renderLabel: () => {
                  return (
                    <Box>
                      <Styled.Text>{dateTime}</Styled.Text>
                      <Styled.Timezone>{item.timezone}</Styled.Timezone>
                    </Box>
                  );
                },
              },
            ];
          } else if (column.id === 'sentBy') {
            const sentByNumber = `From: (${item.sentByNumber})`;
            const sentByName = item.sentByFrom;

            acc[column.id] = [
              {
                value: sentByNumber,
                renderLabel: () => {
                  return (
                    <Box>
                      <Styled.ToFrom>{sentByNumber}</Styled.ToFrom>
                      <Styled.Text>{sentByName}</Styled.Text>
                    </Box>
                  );
                },
              },
            ];
          } else {
            acc[column.id] = item[column.id];
          }

          return acc;
        },
        {
          emailId: item.emailId,
          previewMessage: item.message,
          subject: item.subject,
          phoneNumber: item.phoneNumber,
          attachments: item.attachments?.map((a, index) => {
            return {
              id: `${Date.now()}${index}`,
              thumbnail: !a.isUrl
                ? `${getBase64Prefix(a.fileExt)}${a.thumbnail}`
                : a.thumbnail,
              fileName: a.fileName,
              fileExt: a.fileExt,
              isPDF: a.fileExt === 'pdf',
              isUrl: a.isUrl,
              isNew: true,
            };
          }),
        },
      );

      return itemObj;
    });
  };

  const handleChangePage = (event, value) => {
    setPage(value);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value));
    setPage(0);
  };

  useEffect(() => {
    if (page !== 0) {
      setPage(0);
    }
  }, [selectedTab]);

  useLayoutEffect(() => {
    if (!loading || data?.listCommunicationHistory?.total) {
      setTotal(data?.listCommunicationHistory?.total);
    }
  }, [loading, data?.listCommunicationHistory?.total]);

  useLayoutEffect(() => {
    const updatedColumns =
      selectedTab === 'email' ? DEFAULT_EMAIL_COLUMNS : DEFAULT_SMS_COLUMNS;

    setColumns(updatedColumns);

    if (communicationHistoryItems) {
      setTableData(
        transformEmailItemData(communicationHistoryItems, updatedColumns),
      );
    }
  }, [selectedTab, communicationHistoryItems]);

  return (
    <NoteTaskHistoryStyled.FullHeightGrid container>
      <NoteTaskHistoryStyled.FullHeightFlexGridColumn item xs={12}>
        <Section
          fullHeight
          headerStyle={{
            backgroundColor: 'white',
          }}
          loading={loading}
          title="Communication History"
          HeaderRightComp={
            <ButtonGroup
              increaseBtnMinWidth
              size="small"
              variant="outlined"
              aria-label="primary button group">
              {COMMUNICATION_HISTORY_TABS.map((tab, index) => {
                return (
                  <Button key={index} onClick={handleSelectTab(tab.key)}>
                    {tab.label}
                  </Button>
                );
              })}
            </ButtonGroup>
          }
          FooterComp={
            tableData.length > 0 && (
              <Pagination
                table
                count={total}
                page={page}
                onPageChange={handleChangePage}
                rowsPerPage={rowsPerPage}
                onRowsPerPageChange={handleChangeRowsPerPage}
                labelRowsPerPage={''}
                rowsPerPageOptions={[]}
                labelDisplayedRows={() => `Page ${page + 1}`}
              />
            )
          }>
          <DataTable
            fullHeight
            noLimit
            hideEmptyMessage={loading}
            hidePagination
            columns={columns}
            data={tableData}
            rowsPerPage={rowsPerPage}
            order="desc"
            orderBy="sentDateTime"
            renderActionButtons={({item}) => (
              <DataObjectsSearchTableStyled.ActionButtonContainer>
                {selectedTab === 'email' ? (
                  <EmailPreview
                    emailId={item.emailId}
                    subject={item.subject}
                    attachments={item.attachments}
                  />
                ) : (
                  <Styled.SMSPreview
                    to={item.phoneNumber}
                    message={item.previewMessage}
                  />
                )}
                <Styled.ResendButton
                  type={selectedTab}
                  emailId={item.emailId}
                  message={item.previewMessage}
                  subject={item.subject}
                  phoneNumber={item.phoneNumber}
                  attachments={item.attachments}
                />
              </DataObjectsSearchTableStyled.ActionButtonContainer>
            )}
            numOfTextLines={2}
          />
        </Section>
      </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
    </NoteTaskHistoryStyled.FullHeightGrid>
  );
};

export default CommunicationHistoryContainer;
