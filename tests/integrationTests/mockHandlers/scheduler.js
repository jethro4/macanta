import doSchedulesFactory from '@tests/jestSettings/mockFactory/doSchedulesFactory';
import {overrideHandlers} from '@tests/jestSettings/utils/renderUtils';
import {graphql} from 'msw';

export const mockDOSchedules = (data) => {
  const mockedResult = data || [doSchedulesFactory(), doSchedulesFactory()];

  overrideHandlers(
    graphql.query('listDOSchedules', (req, res, ctx) => {
      return res(
        ctx.data({
          listDOSchedules: mockedResult,
        }),
      );
    }),
  );

  return mockedResult;
};
