import useValue from '@macanta/hooks/base/useValue';
import useUserGuides from '@macanta/hooks/useUserGuides';
import {getDefaultUserGuide} from '@macanta/modules/AdminSettings/UserManagement/Tabs/UserGuidesForm/helpers';

const GENERAL_INTERFACE_SUBTYPES = [
  'Contact Window',
  'Search Function',
  'Notes & Tasks',
];

const useGeneralUserGuides = ({permission}) => {
  const {general} = useUserGuides({permission});

  const [guides] = useValue(() => {
    return GENERAL_INTERFACE_SUBTYPES.map((subType) => {
      return (
        general.find((guide) => guide.subType === subType) ||
        getDefaultUserGuide({
          type: 'GeneralInterface',
          subType,
        })
      );
    });
  }, [general]);

  return {data: guides};
};

export default useGeneralUserGuides;
