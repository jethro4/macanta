import React, {useState, useEffect, useLayoutEffect, useRef} from 'react';
import {Responsive, WidthProvider} from 'react-grid-layout';
import AddIcon from '@mui/icons-material/Add';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import InputAdornment from '@mui/material/InputAdornment';
import FormField from '@macanta/containers/FormField';
import useTransformChoices from '@macanta/hooks/base/useTransformChoices';
import useEventResize from '@macanta/hooks/base/useEventResize';
import {sortArrayByObjectKey, moveItem} from '@macanta/utils/array';
import {exists} from '@macanta/validations/array';
import ChoiceCard from './ChoiceCard';
import * as WidgetStyled from '@macanta/modules/pages/AppContainer/Widgets/styles';
import * as TransferListStyled from '@macanta/containers/TransferList/styles';
import * as QueryBuilderStyled from '@macanta/modules/AdminSettings/QueryBuilder/styles';

const ResponsiveReactGridLayout = WidthProvider(Responsive);

const ChoicesList = ({
  label,
  data: dataProp,
  hideEmptyMessage,
  emptyMessage,
  onChange,
  hideSearch,
  hideDelete,
}) => {
  const data = useTransformChoices({
    options: dataProp,
  });

  const [finalOptions, setFinalOptions] = useState(data);
  const [layouts, setLayouts] = useState({});
  const [value, setValue] = useState('');

  const textFieldRef = useRef(null);

  const choiceExistsError =
    !hideSearch &&
    exists(
      finalOptions.map((option) => option.value),
      value,
    ) &&
    'Choice already exists';

  const handleAdd = () => {
    textFieldRef.current.focus();

    if (choiceExistsError) {
      return;
    }

    const updatedData = finalOptions.concat({label: value, value});

    setFinalOptions(updatedData);
    setValue('');

    onChange(updatedData.map((d) => d.value));
  };

  const handleDelete = (dValue) => {
    const updatedData = finalOptions.filter((d) => d.value !== dValue);

    setFinalOptions(updatedData);

    onChange(updatedData.map((d) => d.value));
  };

  const handleDragStop = (layout, oldItem, newItem) => {
    const hasSwitched = oldItem.x !== newItem.x || oldItem.y !== newItem.y;
    const sortedLayout = sortArrayByObjectKey(layout, 'y');

    if (hasSwitched) {
      const updatedLayouts = generateLayouts(
        sortedLayout.map((l) => ({id: l.i})),
      );

      setLayouts(updatedLayouts);

      handleSortData(oldItem.y, newItem.y);
    }
  };

  const handleSortData = (fromIndex, toIndex) => {
    const sortedData = moveItem({
      arr: data,
      fromIndex,
      toIndex,
    });

    onChange(sortedData.map((d) => d.value));
  };

  useLayoutEffect(() => {
    const updatedLayouts = generateLayouts(data);

    setLayouts(updatedLayouts);
  }, [data]);

  useEffect(() => {
    setFinalOptions(data);
  }, [data]);

  useEventResize([finalOptions, choiceExistsError]);

  return (
    <Box
      style={{
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
      }}>
      {!hideSearch && (
        <FormField
          required
          inputRef={textFieldRef}
          style={{
            marginBottom: 4,
          }}
          value={value}
          error={choiceExistsError}
          onChange={setValue}
          onEnterPress={handleAdd}
          label="Add Choices"
          size="small"
          variant="outlined"
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <QueryBuilderStyled.IconButton
                  TooltipProps={{
                    title: 'Add Choice',
                  }}
                  style={{
                    width: '1.6rem',
                    height: '1.6rem',
                  }}
                  disabled={!value}
                  size="small"
                  onClick={handleAdd}
                  edge="end">
                  <AddIcon />
                </QueryBuilderStyled.IconButton>
              </InputAdornment>
            ),
          }}
        />
      )}

      {!!label && (
        <Typography
          variant="caption"
          style={{
            marginBottom: 4,
          }}>
          {label}
        </Typography>
      )}

      <Paper
        style={{
          flex: 1,
          overflow: 'auto',
          borderTop: '1px solid #F5F5F5',
          backgroundColor: '#e4e4e4',
          borderRadius: 0,
        }}>
        {!!finalOptions.length && (
          <ResponsiveReactGridLayout
            layouts={layouts}
            isBounded
            rowHeight={40}
            margin={[0, 0]}
            cols={{lg: 1, md: 1, sm: 1, xs: 1, xxs: 1}}
            isResizable={false}
            compactType="vertical"
            onDragStop={handleDragStop}>
            {finalOptions.map((item) => {
              return (
                <WidgetStyled.ReactGridItemContainer key={item.value}>
                  <ChoiceCard
                    item={item}
                    onDelete={handleDelete}
                    hideDelete={hideDelete}
                  />
                </WidgetStyled.ReactGridItemContainer>
              );
            })}
          </ResponsiveReactGridLayout>
        )}

        {!hideEmptyMessage && finalOptions.length === 0 && (
          <TransferListStyled.EmptyMessage align="center" color="#888">
            {emptyMessage}
          </TransferListStyled.EmptyMessage>
        )}
      </Paper>
    </Box>
  );
};

const generateLayouts = (items) => {
  return ['lg', 'md', 'sm', 'xs', 'xxs'].reduce((acc, col) => {
    if (items) {
      acc[col] = items.map((item, index) => {
        return {
          x: 0,
          y: index,
          w: 1,
          h: 1,
          i: String(item.id || item.value),
        };
      });
    }

    return acc;
  }, {});
};

ChoicesList.defaultProps = {
  data: [],
  emptyMessage: 'No choices',
};

export default ChoicesList;
