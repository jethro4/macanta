import useQuery from '@macanta/hooks/apollo/useQuery';
import {LIST_ADVANCED_SEARCH} from '@macanta/graphql/admin';

const useAdvancedSearch = (values, options) => {
  const advancedSearchQuery = useQuery(LIST_ADVANCED_SEARCH, {
    variables: {
      doType: values?.doType,
      chosenFields: JSON.stringify(values?.chosenFields),
      doConditions: JSON.stringify(values?.doConditions),
      contactConditions: JSON.stringify(values?.contactConditions),
      userConditions: JSON.stringify(values?.userConditions),
    },
    ...options,
    skip: !values || options?.skip,
  });

  const queryId = advancedSearchQuery.data?.listAdvancedSearch?.queryId;

  const items = advancedSearchQuery.data?.listAdvancedSearch?.results;

  return {...advancedSearchQuery, items, queryId};
};

export default useAdvancedSearch;
