import withFormHandlers from '@macanta/containers/FormField/withFormHandlers';
import React from 'react';
import FormFieldComp from './FormField';
import RawFormField from './RawFormField';

const EnhancedFormField = withFormHandlers(FormFieldComp);

const FormField = ({raw, name, ...props}) => {
  if (raw) {
    return <RawFormField {...props} />;
  } else if (name) {
    return <EnhancedFormField name={name} {...props} />;
  }

  return <FormFieldComp {...props} />;
};

export default FormField;

export {RawFormField};
