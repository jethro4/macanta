import {gql} from '@apollo/client';
import {WIDGET_ATTRIBUTES} from './fragments';

export default gql`
  mutation createOrUpdateWidget(
    $createOrUpdateWidgetInput: CreateOrUpdateWidgetInput!
  ) {
    createOrUpdateWidget(input: $createOrUpdateWidgetInput) {
      ...widgetResults
    }
  }
  ${WIDGET_ATTRIBUTES}
`;
