import React, {useState} from 'react';
import Search from '@macanta/containers/Search';
import {LIST_NOTE_TAGS} from '@macanta/graphql/notesTasks';
import useQuery from '@macanta/hooks/apollo/useQuery';

const HISTORY_TYPES = [
  {
    value: 'terms',
    label: 'Terms',
  },
  {
    value: 'tags',
    label: 'Tags',
  },
];

const HistorySearch = ({onSearch, filterBy, ...props}) => {
  const [suggestions, setSuggestions] = useState([]);
  const [tagsLoaded, setTagsLoaded] = useState(false);

  const isTags = filterBy === 'tags';

  const handleSearch = (value) => {
    onSearch({search: value});
  };

  const handleTagsQuery = (data) => {
    if (data?.listNoteTags.items) {
      const options = data?.listNoteTags.items;

      const tags = options.map((suggestion) => suggestion.name);

      setTagsLoaded(true);
      setSuggestions(tags);
    }
  };

  const handleError = () => {
    setTagsLoaded(true);
  };

  return (
    <>
      {isTags && !tagsLoaded && (
        <TagsQuery onCompleted={handleTagsQuery} onError={handleError} />
      )}
      <Search
        autoSearch
        size="xsmall"
        loading={isTags && !tagsLoaded}
        defaultCategory={HISTORY_TYPES[0].value}
        style={{
          minWidth: 270,
          marginRight: '1rem',
          pointerEvents: isTags && !tagsLoaded ? 'none' : 'auto',
        }}
        CategoryProps={{
          style: {
            minWidth: 80,
          },
        }}
        categories={HISTORY_TYPES}
        suggestions={suggestions}
        onSearch={handleSearch}
        disableSuggestions={isTags ? false : true}
        {...props}
      />
    </>
  );
};

const TagsQuery = ({onCompleted, onError}) => {
  useQuery(LIST_NOTE_TAGS, {
    onCompleted,
    onError,
  });

  return null;
};

HistorySearch.defaultProps = {
  onSelectItem: () => {},
  autoSelect: false,
  modal: false,
};

export default HistorySearch;
