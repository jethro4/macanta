import {gql} from '@apollo/client';
import {CUSTOM_TAB_ITEM_FRAGMENT} from './fragments';

export default gql`
  query listCustomTabs(
    $appName: String!
    $apiKey: String!
    $userEmail: String
  ) {
    listCustomTabs(appName: $appName, apiKey: $apiKey, userEmail: $userEmail) {
      ...customTabItemFragment
    }
  }
  ${CUSTOM_TAB_ITEM_FRAGMENT}
`;
