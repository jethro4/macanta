export default `
<style>
#markdown-guide code {
  padding: 2px 4px;
  font-size: 90%;
  color: #c7254e;
  background-color: #f9f2f4;
  border-radius: 4px;
}
#markdown-guide .modal-body {
  text-align: left;
}
#markdown-guide .modal-body > h3 {
  margin-bottom: 10px;
}
#markdown-guide .modal-body table {
  margin-top: 15px;
  border-top: 1px solid #66339933;
  border-left: 1px solid #66339933;
  border-bottom: 1px solid #66339933;
}
#markdown-guide .modal-body table th {
  font-weight: 700;
  font-size: 16px;
  border-right: 1px solid #66339933;

  color: #353b48;
  background-color: #f5f6fa;
  font-weight: 600;
  height: 43px;
  padding-left: 4px;
  padding-right: 4px;
}
#markdown-guide .modal-body table td {
  vertical-align: middle;
  border-top: 1px solid #66339933;
  border-right: 1px solid #66339933;
  padding-left: 4px;
  padding-right: 4px;
}
#markdown-guide .modal-body .external-link {
  font-weight: 700;
  display: flex;
  align-items: center;
}
#markdown-guide .modal-body .external-link i {
  margin-left: 5px;
  color: blue;
}
</style>

<div id="markdown-guide" class="modal fade" role="dialog">
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-body">
      <h3>Basic Syntax</h3>
      <p>All Markdown applications support these elements.</p>
      <table class="table table-bordered">
        <thead class="thead-light">
          <tr>
            <th>Element</th>
            <th>Markdown Syntax</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <h3>Heading</h3>
            </td>
            <td>
              <code
                ># H1<br />
                ## H2<br />
                ### H3</code
              >
            </td>
          </tr>
          <tr>
            <td><strong>Bold</strong></td>
            <td><code>**bold text**</code></td>
          </tr>
          <tr>
            <td><em>Italic</em></td>
            <td><code>*italicized text*</code></td>
          </tr>
          <tr>
            <td>
              <blockquote>Blockquote</blockquote>
            </td>
            <td><code>&gt; blockquote</code></td>
          </tr>
          <tr>
            <td>Ordered List</td>
            <td>
              <code>
                1. First item<br />
                2. Second item<br />
                3. Third item<br />
              </code>
            </td>
          </tr>
          <tr>
            <td>Unordered List</td>
            <td>
              <code>
                - First item<br />
                - Second item<br />
                - Third item<br />
              </code>
            </td>
          </tr>
          <tr>
            <td><code>Code</code></td>
            <td><code>\`code\`</code></td>
          </tr>
          <tr>
            <td>Horizontal Rule</td>
            <td><code>---</code></td>
          </tr>
          <tr>
            <td><a>Link</a></td>
            <td><code>[title](https://www.example.com)</code></td>
          </tr>
          <tr>
            <td>Image</td>
            <td><code>![alt text](image.jpg)</code></td>
          </tr>
        </tbody>
      </table>
      <h3>Extended Syntax</h3>
      <p>
        These elements extend the basic syntax by adding additional features.
      </p>
      <table class="table table-bordered">
        <thead class="thead-light">
          <tr>
            <th>Element</th>
            <th>Markdown Syntax</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Table</td>
            <td>
              <code>
                | Syntax | Description |<br />
                | ----------- | ----------- |<br />
                | Header | Title |<br />
                | Paragraph | Text |
              </code>
            </td>
          </tr>
          <tr>
            <td>
              <code
                >Fenced<br />
                Code <br />
                Block</code
              >
            </td>
            <td>
              <code
                >\`\`\`<br />
                {<br />
                &nbsp;&nbsp;"firstName": "John",<br />
                &nbsp;&nbsp;"lastName": "Smith",<br />
                &nbsp;&nbsp;"age": 25<br />
                }<br />
                \`\`\`
              </code>
            </td>
          </tr>
          <tr>
            <td>A<sup>Footnote</sup></td>
            <td>
              <code>
                Here's a sentence with a footnote. [^1]<br /><br />

                [^1]: This is the footnote.
              </code>
            </td>
          </tr>
          <tr>
            <td>Heading ID</td>
            <td>
              <code>### My Great Heading {#custom-id}</code>
            </td>
          </tr>
          <tr>
            <td>Definition List</td>
            <td>
              <code>
                term<br />
                : definition
              </code>
            </td>
          </tr>
          <tr>
            <td>
              <del>Strikethrough</del>
            </td>
            <td>
              <code>~~The world is flat.~~</code>
            </td>
          </tr>
          <tr>
            <td>Task List</td>
            <td>
              <code>
                - [x] Write the press release<br />
                - [ ] Update the website<br />
                - [ ] Contact the media
              </code>
            </td>
          </tr>
        </tbody>
      </table>
      <a
        href="https://www.markdownguide.org/cheat-sheet"
        class="external-link"
        target="_blank"
        ><span>Visit the official Markdown Guide website.</span
        ><i class="fa fa-external-link"></i
      ></a>
    </div>
    <div class="modal-footer">
      <button type="button" class="nb-btn nb-secondary" data-dismiss="modal">
        Close
      </button>
    </div>
  </div>
</div>
</div>
`;
