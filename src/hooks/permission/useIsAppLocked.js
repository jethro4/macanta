import useAppSettings from '@macanta/hooks/admin/useAppSettings';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';

const useIsAppLocked = () => {
  const appSettingsQuery = useAppSettings({
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const loginDisabledMessage = appSettingsQuery.data?.loginDisabledMessage;
  const isAppLocked = !!loginDisabledMessage;

  return {isAppLocked, loginDisabledMessage};
};

export default useIsAppLocked;
