import {experimentalStyled as styled} from '@mui/material/styles';

import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import {OverflowTip} from '@macanta/components/Tooltip';
import * as ContactDetailsStyled from '@macanta/modules/ContactDetails/styles';
import * as WidgetStyled from '@macanta/modules/pages/AppContainer/Widgets/styles';

export const BoardDetailsContainer = styled(Box)`
  flex: 1;
  display: flex;
  justify-content: space-between;
`;

export const BoardDetailsInfoContainer = styled(
  ContactDetailsStyled.InterfaceAlertsBox,
)`
  & > *:not(:last-child) {
    margin-right: 2.5rem;
  }
`;

export const BoardDetailsInfo = styled(Box)`
  display: flex;
  align-items: center;
`;

export const BoardDetailsInfoLabel = styled(Typography)`
  white-space: nowrap;
  font-size: 0.875rem;
  font-weight: 700;
  margin-right: 6px;
  color: ${({theme}) => theme.palette.text.blue};
`;

export const BoardDetailsInfoValue = styled(OverflowTip)`
  font-size: 0.875rem;
`;

export const ActionArea = styled(Box)`
  flex: 1;
  display: flex;
  flex-direction: column;
  padding: 0 12px;
`;

export const Header = styled(WidgetStyled.Header)`
  padding: 10px 0 4px;
`;

export const Title = styled(WidgetStyled.Title)`
  font-size: 0.8125rem;
  font-weight: 700;
`;

export const FieldContainer = styled(Box)`
  width: 100%;
  margin-bottom: 8px;
`;

export const ShowMoreContainer = styled(Box)`
  padding: 12px;
`;

export const FieldName = styled(OverflowTip)`
  font-size: 0.8125rem;
  font-weight: 700;
  color: ${({theme}) => `${theme.palette.text.blue}`};
`;

export const FieldValue = styled(OverflowTip)`
  font-size: 0.8125rem;
  line-height: 0.875rem;
`;

export const BoardColumnContainer = styled(Box)`
  flex: 1;
  overflow-x: hidden;
  overflow-y: auto;

  .react-grid-layout {
    height: 100% !important;
  }

  .action-buttons-container {
    padding: 0;
  }

  .board-actions-container {
    margin-bottom: 2px;
    margin-left: 16px;
    margin-right: -8px;

    .MuiButtonBase-root {
      padding: 0;

      .MuiSvgIcon-root {
        font-size: 20px;
      }
    }
  }

  .block-dragging {
    .MuiCard-root {
      background-color: #ccc;
    }
  }
`;
