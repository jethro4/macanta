import React, {useMemo} from 'react';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import Section from '@macanta/containers/Section';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import {Animated} from '@macanta/components/Layout';
import useMediaQuery from '@mui/material/useMediaQuery';
import useAllFields from '@macanta/hooks/dataObject/useAllFields';
import BuilderBody from './BuilderBody';
import BuilderSidebar from './BuilderSidebar';
import * as Styled from './styles';

const BuilderForms = ({item, ...props}) => {
  const isTabletOrMobile = useMediaQuery('(max-width:1399px)');

  const isEdit = item.selectedMode === 'Edit';

  const doTypesQuery = useDOTypes({
    removeEmail: true,
  });

  const doType = doTypesQuery.data?.listDataObjectTypes.find(
    (type) => type.id === item?.doType,
  );
  const groupId = doType?.id;

  const allFieldsQuery = useAllFields({
    groupId,
    isContactObjectType: item.doType === 'contactsOnly',
    includeAllTypes: true,
  });

  const allFields = allFieldsQuery.data;

  const {contactGroups, doGroups} = useMemo(() => {
    if (allFields) {
      return {
        contactGroups: item.contactGroups.map((contactGroup) => {
          return {
            ...contactGroup,
            fields: contactGroup.fields.map((field) => {
              const doField = allFields.find((f) => f.id === field.fieldId);

              return {
                ...doField,
                ...field,
              };
            }),
          };
        }),
        doGroups: item.doGroups.map((doGroup) => {
          return {
            ...doGroup,
            fields: doGroup.fields.map((field) => {
              const doField = allFields.find((f) => f.id === field.fieldId);

              return {
                ...doField,
                ...field,
              };
            }),
          };
        }),
      };
    }

    return {contactGroups: [], doGroups: []};
  }, [allFields, item.contactGroups, item.doGroups]);

  const isMobilePreview = isTabletOrMobile && !isEdit;

  return (
    <Section
      style={{margin: 0}}
      fullHeight
      hideHeader
      // headerStyle={{
      //   // minHeight: '2.55rem',
      //   // boxShadow: 'none',
      //   borderBottom: '1px solid #eee',
      // }}
      bodyStyle={{
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: '#f5f6fa',
      }}
      {...props}>
      {!isMobilePreview && (
        <Animated
          transitions={[
            {
              property: 'opacity',
              duration: '0.1s',
              timingFunction: 'ease-in-out',
            },
            {
              property: 'transform',
              duration: '0.25s',
              timingFunction: 'ease-in-out',
            },
          ]}
          style={{
            opacity: isEdit ? 1 : 0,
            transform: isEdit ? 'translateX(0)' : 'translateX(-320px)',
          }}
          in={isEdit}>
          <BuilderSidebar />
        </Animated>
      )}

      <Animated
        transitions={[
          {
            property: 'width',
            duration: '0.25s',
            timingFunction: 'ease-in-out',
          },
          {
            property: 'transform',
            duration: '0.25s',
            timingFunction: 'ease-in-out',
          },
        ]}
        style={{
          flexBasis: 0,
          flexGrow: 1,
          overflow: 'hidden',
          transform:
            isMobilePreview || isEdit ? 'translateX(0)' : 'translateX(-160px)',
        }}
        in={isEdit}>
        <Styled.FormBodyContainer>
          {!allFieldsQuery.initLoading && (
            <BuilderBody
              doTypeTitle={doType?.title}
              contactGroups={contactGroups}
              doGroups={doGroups}
            />
          )}
          <LoadingIndicator
            fill
            align="top"
            style={{
              top: '2.5rem',
            }}
            loading={allFieldsQuery.loading}
          />
        </Styled.FormBodyContainer>
      </Animated>
    </Section>
  );
};

export default BuilderForms;
