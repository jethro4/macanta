import React, {forwardRef} from 'react';
import TextField from './InputFields/TextField';
import TextAreaField from './InputFields/TextAreaField';
import RichTextField from './InputFields/RichTextField';
import SelectField from './ChoiceFields/SelectField';
import MultiSelectField from './ChoiceFields/MultiSelectField';
import PasswordField from './InputFields/PasswordField';
import SearchField from './InputFields/SearchField';
import DatePickerField from './DateFields/DatePickerField';
import DateTimePickerField from './DateFields/DateTimePickerField';
import DateRangePickerField from './DateFields/DateRangePickerField';
import NumberField from './InputFields/NumberField';
import CreditCardField from './OtherFields/CreditCardField';

let IndexTextField = (
  {
    textArea,
    richText,
    select,
    multiple,
    password,
    search,
    AutoCompleteProps,
    date,
    DatePickerProps,
    datetime,
    DateTimePickerProps,
    daterange,
    DateRangePickerProps,
    number,
    creditCard,
    ...props
  },
  ref,
) => {
  if (textArea) {
    return <TextAreaField {...props} />;
  } else if (richText) {
    return <RichTextField {...props} />;
  } else if (select) {
    if (!multiple) {
      return <SelectField {...props} />;
    }

    return <MultiSelectField {...props} />;
  } else if (password) {
    return <PasswordField {...props} />;
  } else if (search) {
    return (
      <SearchField
        inputRef={ref}
        {...props}
        AutoCompleteProps={AutoCompleteProps}
      />
    );
  } else if (date) {
    return <DatePickerField {...props} DatePickerProps={DatePickerProps} />;
  } else if (datetime) {
    return (
      <DateTimePickerField
        {...props}
        DateTimePickerProps={DateTimePickerProps}
      />
    );
  } else if (daterange) {
    return (
      <DateRangePickerField
        {...props}
        DateRangePickerProps={DateRangePickerProps}
      />
    );
  } else if (number) {
    return <NumberField {...props} />;
  } else if (creditCard) {
    return <CreditCardField {...props} />;
  }

  return <TextField inputRef={ref} {...props} />;
};

IndexTextField = forwardRef(IndexTextField);

export default IndexTextField;

export {TextField, PasswordField};
