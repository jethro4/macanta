import React, {useState} from 'react';
import Box from '@mui/material/Box';
import Button, {IconButton} from '@macanta/components/Button';
import EditIcon from '@mui/icons-material/Edit';
import CloseIcon from '@mui/icons-material/Close';
import TurnedInIcon from '@mui/icons-material/TurnedIn';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import {getFieldOptions} from '@macanta/containers/FormField/helpers';
import {useLazyQuery, useMutation} from '@apollo/client';
import {
  CREATE_OR_UPDATE_CONTACT,
  LIST_CONTACTS,
} from '@macanta/graphql/contacts';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import useDOItemMutation from '@macanta/modules/DataObjectItems/DOItemForms/useDOItemMutation';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {
  DEFAULT_PAGE,
  DEFAULT_PAGE_LIMIT,
} from '@macanta/modules/hoc/ContactsQuery';
import {writeQueryItem} from '@macanta/hooks/contact/useContact';
import useValue from '@macanta/hooks/base/useValue';
import * as TableStyled from '@macanta/components/Table/styles';
import * as Styled from './styles';
import * as Storage from '@macanta/utils/storage';

const InlineEditingForm = ({
  value: valueProp,
  groupId,
  relationships,
  loading,
  isContactField,
  fieldName,
  fieldType,
  itemId,
  contactId,
  hideEdit,
  OtherButtonComp,
  onSave,
  inputStyle,
  options,
}) => {
  const {displayMessage} = useProgressAlert();

  const [showEdit, setShowEdit] = useState(false);
  const [saving, setSaving] = useState(false);

  const [value, setValue] = useValue(valueProp);

  const {save} = useDOItemMutation(itemId, groupId, null, {
    onCompleted: ({values}) => {
      setSaving(false);

      handleClose();

      displayMessage('Saved successfully!');

      onSave({values, itemId, contactId});
    },
  });
  const [callContactsQuery] = useLazyQuery(LIST_CONTACTS, {
    fetchPolicy: FETCH_POLICIES.NETWORK_ONLY,
    onCompleted: (data) => {
      const contact = data?.listContacts?.items[0];
      let updates = {};

      if (fieldName === 'Email') {
        updates = {
          email: value,
        };
      } else if (fieldName === 'FirstName') {
        updates = {
          email: contact.email,
          firstName: value,
        };
      } else if (fieldName === 'LastName') {
        updates = {
          email: contact.email,
          lastName: value,
        };
      } else if (fieldName === 'Phone1') {
        updates = {
          email: contact.email,
          phoneNumbers: [value, ...contact.phoneNumbers.slice(1)],
        };
      } else {
        updates = {
          email: contact.email,
          customFields: [{name: fieldName, value}],
        };
      }

      callContactMutation({
        variables: {
          createOrUpdateContactInput: updates,
          __mutationkey: 'createOrUpdateContactInput',
        },
      });
    },
  });
  const [callContactMutation] = useMutation(CREATE_OR_UPDATE_CONTACT, {
    update(cache, {data: {createOrUpdateContact}}) {
      writeQueryItem(createOrUpdateContact);
    },
    onCompleted(data) {
      const userDetails = Storage.getItem('userDetails');

      if (data.createOrUpdateContact) {
        setSaving(false);
        handleClose();
        displayMessage('Saved successfully!');
      }
      if (data.createOrUpdateContact?.id === userDetails.userId) {
        Storage.setItem('userDetails', {
          ...userDetails,
          firstName: data.createOrUpdateContact.firstName,
          lastName: data.createOrUpdateContact.lastName,
        });
      }
    },
    onError() {},
  });

  const handleEdit = () => {
    setShowEdit(true);
  };

  const handleClose = () => {
    setShowEdit(false);
  };

  const handleCloseAndUndo = () => {
    handleClose();
    setValue(valueProp);
  };

  const handleSave = () => {
    setSaving(true);
    if (isContactField) {
      callContactsQuery({
        variables: {
          q: contactId,
          page: DEFAULT_PAGE,
          limit: DEFAULT_PAGE_LIMIT,
        },
      });
    } else {
      const updatedValue =
        fieldType === 'Checkbox' && Array.isArray(value)
          ? value.filter((v) => v !== '~overwrite~')
          : value;

      save(
        {
          details: {
            [fieldName]:
              fieldType !== 'Checkbox'
                ? updatedValue
                : `~overwrite~${updatedValue ? ',' : ''}` + updatedValue,
          },
          primaryRelationship: {
            connectedContactId: contactId,
            relationships,
          },
        },
        {
          itemId,
          groupId,
        },
      );
    }
  };

  let type;

  switch (fieldType) {
    case 'Radio': {
      type = 'Select';

      break;
    }
    case 'Checkbox': {
      type = 'MultiSelect';

      break;
    }
    default: {
      type = fieldType;
    }
  }

  const fieldOptions = getFieldOptions(type);

  return (
    <Styled.InlineEditingContainer
      style={{
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        ...(showEdit && {
          zIndex: 1,
        }),
      }}>
      {showEdit ? (
        <>
          <Styled.InlineTextField
            autoFocus
            type={type}
            disabled={saving || loading}
            style={{
              marginLeft: type === 'Text' ? -8 : -16,
              ...inputStyle,
            }}
            value={value}
            onChange={setValue}
            size="small"
            options={options}
            {...fieldOptions}
            {...(type === 'Text' && {
              inputProps: {
                style: {
                  paddingLeft: 8,
                  paddingRight: 8,
                },
              },
            })}
          />
          {saving || loading ? (
            <LoadingIndicator transparent size={20} />
          ) : (
            <>
              <IconButton
                style={{
                  padding: '0.5rem',
                  color: '#bbb',
                }}
                onClick={handleCloseAndUndo}>
                <CloseIcon />
              </IconButton>
              <Button
                style={{
                  ...(showEdit && {
                    boxShadow: '1px 1px 10px -2px #777',
                  }),
                }}
                color="info"
                variant="contained"
                startIcon={<TurnedInIcon />}
                onClick={handleSave}
                size="small">
                Save
              </Button>
            </>
          )}
        </>
      ) : (
        <>
          <TableStyled.TableBodyCellLabel>
            {type !== 'MultiSelect'
              ? value
              : (Array.isArray(value) ? value.join(',') : value).replace(
                  '~overwrite~,',
                  '',
                )}
          </TableStyled.TableBodyCellLabel>
          <Box
            className="inline-edit"
            style={{
              position: 'absolute',
              right: 0,
            }}>
            {OtherButtonComp}
            {!hideEdit && (
              <Button
                style={{
                  marginLeft: '0.5rem',
                }}
                color="info"
                variant="contained"
                startIcon={<EditIcon />}
                onClick={handleEdit}
                size="small">
                Edit
              </Button>
            )}
          </Box>
        </>
      )}
    </Styled.InlineEditingContainer>
  );
};

export default InlineEditingForm;
