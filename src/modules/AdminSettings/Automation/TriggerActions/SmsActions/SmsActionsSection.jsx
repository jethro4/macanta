import React from 'react';
import AddIcon from '@material-ui/icons/Add';
import DataTable from '@macanta/containers/DataTable';
import {
  useDeleteAutomationsMutation,
  useSmsActions,
  usePostSmsActionMutation,
} from '@macanta/hooks/automation';
import ItemActionButtons from '@macanta/modules/AdminSettings/Automation/TriggerActions/ItemActionButtons';
import {SmsActionsForm} from '@macanta/modules/AdminSettings/Automation/TriggerActions/SmsActions';
import Section from '@macanta/containers/Section';
import {writeItemInListFragment} from '@macanta/graphql/helpers';
import {SMS_ACTIONS_FRAGMENT} from '@macanta/graphql/automation/GET_SMS_ACTIONS';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import EditButton from '@macanta/containers/buttons/EditButton';

const SMS_ACTION_TYPE_COLUMNS = [
  {
    id: 'name',
    label: 'Name',
  },
  {
    id: 'description',
    label: 'Description',
    maxWidth: '25%',
  },
  {
    id: 'phoneField',
    label: 'Phone Field',
  },
  {
    id: 'message',
    label: 'Message',
    maxWidth: '30%',
  },
  {
    id: 'action',
    label: '',
    disableSort: true,
    disableResize: true,
    minWidth: 100,
    maxWidth: 100,
    renderBodyCellValue: ({item, handlers}) => {
      return (
        <ItemActionButtons
          modal={handlers.modal}
          type="SMS Actions"
          item={item}
          onDelete={handlers.onDelete}
          renderFormComponent={(handleClose) => (
            <SmsActionsForm
              options={{
                variables: {
                  id: item.id,
                },
              }}
              initialValues={item}
              onSubmit={async (values) => {
                const {result} = await handlers.onSave(values);

                if (result) {
                  handleClose();
                }
              }}
            />
          )}
        />
      );
    },
  },
];

const SmsActionsSection = ({modal, ...props}) => {
  const smsActionsQuery = useSmsActions();

  const [callPostMutation, actionMutation] = usePostSmsActionMutation({
    update(cache, {result}, options) {
      writeItemInListFragment({
        isCreate: !options.variables.input?.id,
        keyField: 'queryId',
        item: result,
        typeName: 'SmsActionItems',
        fragment: SMS_ACTIONS_FRAGMENT,
        parentKey: 'getSmsActions',
        listKey: 'items',
        position: 'first',
      });
    },
  });

  const [callDeleteMutation, deleteMutation] = useDeleteAutomationsMutation({
    typeName: 'SmsActionItems',
  });

  const handleSave = async (values) => {
    const variables = {
      id: values.id,
      name: values.name,
      description: values.description,
      phoneField: values.phoneField,
      message: values.message,
    };

    return callPostMutation({
      variables,
    });
  };

  const handleDelete = (id) => {
    return callDeleteMutation({
      variables: {
        id,
      },
    });
  };

  return (
    <>
      <Section
        style={{
          margin: 0,
        }}
        fullHeight
        title={`SMS Actions`}
        HeaderRightComp={
          <EditButton
            modal={modal}
            size="small"
            variant="contained"
            startIcon={<AddIcon />}
            PopoverProps={{
              title: `Add SMS Action`,
              contentWidth: 1100,
              contentMinHeight: 350,
              bodyStyle: {
                backgroundColor: '#f5f6fA',
              },
            }}
            renderContent={(handleClose) => (
              <SmsActionsForm
                onSubmit={async (values) => {
                  const {result} = await handleSave(values);

                  if (result) {
                    handleClose();
                  }
                }}
              />
            )}>
            Add SMS Action
          </EditButton>
        }>
        <DataTable
          loading={smsActionsQuery.loading}
          data={smsActionsQuery.result}
          fullHeight
          rowsPerPage={25}
          rowsPerPageOptions={[25, 50, 100]}
          {...props}
          columns={SMS_ACTION_TYPE_COLUMNS}
          handlers={{
            modal,
            onSave: handleSave,
            onDelete: handleDelete,
          }}
        />
      </Section>
      <LoadingIndicator
        modal
        loading={actionMutation.loading || deleteMutation.loading}
      />
    </>
  );
};

export default SmsActionsSection;
