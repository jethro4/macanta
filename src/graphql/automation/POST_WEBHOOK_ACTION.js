import {gql} from '@apollo/client';
import {WEBHOOK_ACTIONS_FRAGMENT} from '@macanta/graphql/automation/GET_WEBHOOK_ACTIONS';

export default gql`
  mutation postWebhookAction($input: WebhookActionInput!) {
    postWebhookAction(input: $input) {
      ...webhookActionsFragment
    }
  }
  ${WEBHOOK_ACTIONS_FRAGMENT}
`;
