import React from 'react';
import useQuery from '@macanta/hooks/apollo/useQuery';
import {GET_DATA_OBJECT} from '@macanta/graphql/dataObjects';
import DOItemEditSection from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/DOItemEditSection';
import useDOFields from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOFields';
import useDORelationships from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDORelationships';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import Typography from '@mui/material/Typography';

const NotifDOItemEdit = ({itemId, onSave}) => {
  const {data, loading} = useQuery(GET_DATA_OBJECT, {
    variables: {
      itemId,
    },
  });

  const updatedItem = data?.getDataObject;

  if (!updatedItem && loading) {
    return <LoadingIndicator fill />;
  }

  return (
    <NotifDOItemSection
      item={updatedItem}
      contact={updatedItem?.connectedContacts[0]}
      groupId={updatedItem?.groupId}
      onSave={onSave}
    />
  );
};

const NotifDOItemSection = ({item, contact, groupId, onSave}) => {
  const doFieldsQuery = useDOFields(groupId);
  const relationshipsQuery = useDORelationships(groupId);

  const fields = doFieldsQuery.data?.listDataObjectFields;
  const relationships = relationshipsQuery.data?.listRelationships;

  if (!item?.id) {
    return (
      <Typography
        style={{
          paddingTop: '1rem',
        }}
        align="center"
        color="#888">
        Item does not exist or is deleted
      </Typography>
    );
  }

  if (
    (!fields || !relationships) &&
    (doFieldsQuery.loading || relationshipsQuery.loading)
  ) {
    return <LoadingIndicator fill />;
  }

  return (
    <DOItemEditSection
      loadInfo
      item={item}
      contact={contact}
      groupId={groupId}
      fields={fields}
      relationships={relationships}
      onSave={onSave}
    />
  );
};

export default NotifDOItemEdit;
