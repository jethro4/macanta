import React from 'react';
import AddIcon from '@material-ui/icons/Add';
import DataTable from '@macanta/containers/DataTable';
import {
  useDeleteAutomationsMutation,
  useUserActions,
  usePostUserActionMutation,
} from '@macanta/hooks/automation';
import ItemActionButtons from '@macanta/modules/AdminSettings/Automation/TriggerActions/ItemActionButtons';
import {UserActionsForm} from '@macanta/modules/AdminSettings/Automation/TriggerActions/UserActions';
import Section from '@macanta/containers/Section';
import {writeItemInListFragment} from '@macanta/graphql/helpers';
import {USER_ACTIONS_FRAGMENT} from '@macanta/graphql/automation/GET_USER_ACTIONS';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import ValueTransform from '@macanta/modules/AdminSettings/DOScheduler/ValueTransform';
import useDORelationships from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDORelationships';
import useUsers from '@macanta/hooks/admin/useUsers';
import EditButton from '@macanta/containers/buttons/EditButton';

const USER_ACTION_TYPE_COLUMNS = [
  {
    id: 'name',
    label: 'Name',
  },
  {
    id: 'description',
    label: 'Description',
    maxWidth: '25%',
  },
  {
    id: 'doTitle',
    label: 'Data Object Type',
  },
  {
    id: 'contactRelationship',
    label: 'Contact Relationship',
    renderBodyCellValue: ({origValue, item}) => {
      return (
        <ValueTransform
          value={origValue}
          labelKey="role"
          valueKey="id"
          selector={useDORelationships}
          options={item.groupId}
        />
      );
    },
  },
  {
    id: 'appUser',
    label: 'User',
    renderBodyCellValue: ({origValue}) => {
      return (
        <ValueTransform
          value={origValue}
          labelKey="email"
          valueKey="id"
          selector={useUsers}
          options={{
            transformResult: (result) => result?.items,
          }}
        />
      );
    },
  },
  {
    id: 'action',
    label: '',
    disableSort: true,
    disableResize: true,
    minWidth: 100,
    maxWidth: 100,
    renderBodyCellValue: ({item, handlers}) => {
      return (
        <ItemActionButtons
          modal={handlers.modal}
          type="User Actions"
          item={item}
          onDelete={handlers.onDelete}
          renderFormComponent={(handleClose) => (
            <UserActionsForm
              options={{
                variables: {
                  id: item.id,
                },
              }}
              initialValues={item}
              onSubmit={async (values) => {
                const {result} = await handlers.onSave(values);

                if (result) {
                  handleClose();
                }
              }}
            />
          )}
        />
      );
    },
  },
];

const UserActionsSection = ({modal, ...props}) => {
  const userActionsQuery = useUserActions();

  const [callPostMutation, actionMutation] = usePostUserActionMutation({
    update(cache, {result}, options) {
      writeItemInListFragment({
        isCreate: !options.variables.input?.id,
        keyField: 'queryId',
        item: result,
        typeName: 'UserActionItems',
        fragment: USER_ACTIONS_FRAGMENT,
        parentKey: 'getUserActions',
        listKey: 'items',
        position: 'first',
      });
    },
  });

  const [callDeleteMutation, deleteMutation] = useDeleteAutomationsMutation({
    typeName: 'UserActionItems',
  });

  const handleSave = async (values) => {
    const variables = {
      id: values.id,
      name: values.name,
      description: values.description,
      doType: values.doTitle,
      contactRelationship: values.contactRelationship,
      macantaUserId: values.appUser,
      message: values.actionDetails,
    };

    return callPostMutation({
      variables,
    });
  };

  const handleDelete = (id) => {
    return callDeleteMutation({
      variables: {
        id,
      },
    });
  };

  return (
    <>
      <Section
        style={{
          margin: 0,
        }}
        fullHeight
        title={`User Actions`}
        HeaderRightComp={
          <EditButton
            modal={modal}
            size="small"
            variant="contained"
            startIcon={<AddIcon />}
            PopoverProps={{
              title: `Add User Action`,
              contentWidth: 1100,
              contentMinHeight: 350,
              bodyStyle: {
                backgroundColor: '#f5f6fA',
              },
            }}
            renderContent={(handleClose) => (
              <UserActionsForm
                onSubmit={async (values) => {
                  const {result} = await handleSave(values);

                  if (result) {
                    handleClose();
                  }
                }}
              />
            )}>
            Add User Action
          </EditButton>
        }>
        <DataTable
          loading={userActionsQuery.loading}
          data={userActionsQuery.result}
          fullHeight
          rowsPerPage={25}
          rowsPerPageOptions={[25, 50, 100]}
          {...props}
          columns={USER_ACTION_TYPE_COLUMNS}
          handlers={{
            modal,
            onSave: handleSave,
            onDelete: handleDelete,
          }}
        />
      </Section>
      <LoadingIndicator
        modal
        loading={actionMutation.loading || deleteMutation.loading}
      />
    </>
  );
};

export default UserActionsSection;
