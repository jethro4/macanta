import moment from 'moment';

export const createDate = (date, inputFormat) =>
  moment(date || getNow(), inputFormat);

export const buildDate = (options) => {
  const date = getNow();

  date.set(options);

  return date;
};

export const formatDate = (date, format) => moment(date).format(format);

export const add = (date, unit = 'day', val = 1) => moment(date).add(val, unit);
export const subtract = (date, unit = 'day', val = 1) =>
  moment(date).subtract(val, unit);

export const isBefore = (dateTo, dateFrom = getNow(), unit = 'day') =>
  moment(dateTo).isBefore(dateFrom, unit);
export const isAfter = (dateTo, dateFrom = getNow(), unit = 'day') =>
  moment(dateTo).isAfter(dateFrom, unit);
export const isSame = (dateTo, dateFrom = getNow(), unit = 'day') =>
  moment(dateTo).isSame(dateFrom, unit);
export const isSameOrAfter = (dateTo, dateFrom = getNow(), unit = 'day') =>
  moment(dateTo).isSameOrAfter(dateFrom, unit);
export const isSameOrBefore = (dateTo, dateFrom = getNow(), unit = 'day') =>
  moment(dateTo).isSameOrBefore(dateFrom, unit);
export const isBetween = (
  dateTo1,
  dateTo2,
  dateFrom = getNow(),
  unit = 'day',
) => moment(dateFrom).isBetween(dateTo1, dateTo2, unit);

export const isToday = (dateTo, options) => {
  const {format, unit} = Object.assign({unit: 'day'}, options);

  return isSame(createDate(dateTo, format), getNow(), unit);
};
export const isPast = (dateTo, options) => {
  const {format, unit} = Object.assign({unit: 'day'}, options);

  return isBefore(createDate(dateTo, format), getNow(), unit);
};
export const isFuture = (dateTo, options) => {
  const {format, unit} = Object.assign({unit: 'day'}, options);

  return isAfter(createDate(dateTo, format), getNow(), unit);
};
export const isWithin = (dateTo1, dateTo2, options) => {
  const {format, unit} = Object.assign({unit: 'day'}, options);

  return isBetween(
    subtract(createDate(dateTo1, format), unit),
    add(createDate(dateTo2, format), unit),
    getNow(),
    unit,
  );
};

export const getNowISOString = (date) => moment(date).toISOString();
export const getNow = (format) =>
  !format ? moment() : formatDate(moment(), format);
export const getDayOfWeek = (date) => moment(date).days();
export const getDateOfMonth = (date) => moment(date).date();
export const getDaysInMonth = (date) => moment(date).daysInMonth();
export const getMonthOfYear = (date) => moment(date).month();
export const getYear = (date) => moment(date).year();
export const getStartOf = (date, unit = 'month') => moment(date).startOf(unit);
export const getEndOf = (date, unit = 'month') => moment(date).endOf(unit);

export const getSignedValue = (val, signed) => {
  let signedValue = signed ? val : Math.abs(val);

  return Math.round(signedValue);
};

export const getDiffValue = (date1, date2, signed, unit) => {
  let diff = moment(date1).subtract(moment(date2), 'milliseconds') / 1000;

  switch (unit) {
    case 'milliseconds': {
      diff *= 1000;

      break;
    }
    case 'seconds': {
      diff /= 1;

      break;
    }
    case 'minutes': {
      diff /= 60;

      break;
    }
    case 'hours': {
      diff /= 3600;

      break;
    }
    case 'days': {
      diff /= 3600 * 24;

      break;
    }
    case 'weeks': {
      diff /= 3600 * 24 * 7;

      break;
    }
  }

  return getSignedValue(diff, signed);
};

export const diffMilliSeconds = (date1, date2, signed) => {
  const diff = getDiffValue(date1, date2, signed, 'milliseconds');

  return getSignedValue(diff);
};

export const diffSeconds = (date1, date2, signed) => {
  const diff = getDiffValue(date1, date2, signed, 'seconds');

  return diff;
};

export const diffMinutes = (date1, date2, signed) => {
  let diff = getDiffValue(date1, date2, signed, 'minutes');

  return diff;
};

export const diffHours = (date1, date2, signed) => {
  let diff = getDiffValue(date1, date2, signed, 'hours');

  return diff;
};

export const diffDays = (date1, date2, signed) => {
  let diff = getDiffValue(date1, date2, signed, 'days');

  return diff;
};

export const diffWeeks = (date1, date2, signed) => {
  let diff = getDiffValue(date1, date2, signed, 'weeks');

  return diff;
};

export const timeLabel = (date) => {
  if (isToday(date)) {
    return `Today, ${moment(date).format('H:mm')}`;
  } else {
    return moment(date).format('MMM D, H:mm');
  }
};

export const timeDiff = (date1, date2 = getNow()) => {
  if (!date1) {
    return '';
  }

  const millis = diffMilliSeconds(date1, date2, true);
  if (Math.abs(millis) < 1000) {
    return 'Just now';
  }

  const secs = diffSeconds(date1, date2, true);
  if (Math.abs(secs) < 60) {
    return `${Math.abs(secs)}s` + (secs < 0 ? ' ago' : '');
  }

  const mins = diffMinutes(date1, date2, true);
  if (Math.abs(mins) < 60) {
    return `${Math.abs(mins)}m` + (mins < 0 ? ' ago' : '');
  }

  const hours = diffHours(date1, date2, true);
  if (Math.abs(hours) < 24) {
    return `${Math.abs(hours)}h` + (hours < 0 ? ' ago' : '');
  }

  const days = diffDays(date1, date2, true);
  if (Math.abs(days) < 7) {
    return `${Math.abs(days)}d` + (days < 0 ? ' ago' : '');
  }

  const weeks = diffWeeks(date1, date2, true);
  return `${Math.abs(weeks)}w` + (weeks < 0 ? ' ago' : '');
};
