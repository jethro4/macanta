import * as React from 'react';
import Box from '@mui/material/Box';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import Typography from '@mui/material/Typography';
import CloseIcon from '@mui/icons-material/Close';
import ErrorIcon from '@mui/icons-material/Error';
import Button, {IconButton} from '@macanta/components/Button';
import isArray from 'lodash/isArray';
import * as Styled from './styles';

const ConfirmationDialog = ({
  title,
  description,
  BodyComp,
  IconComp,
  hideHeaderIcon,
  actions,
  ...props
}) => {
  const handleStopMouseEvent = (event) => {
    event.stopPropagation();
  };

  return (
    <Styled.Root
      onMouseUp={handleStopMouseEvent}
      onMouseDown={handleStopMouseEvent}
      {...props}>
      {title && (
        <DialogTitle sx={{m: 0, px: 2, py: 1}} id="alert-dialog-title">
          <Box
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Styled.TitleContainer>
              {!hideHeaderIcon && (IconComp || <ErrorIcon color="error" />)}
              <Typography
                style={{
                  marginLeft: !hideHeaderIcon ? '0.75rem' : 0,
                  fontSize: '1rem',
                }}>
                {title}
              </Typography>
            </Styled.TitleContainer>
            {props.onClose ? (
              <IconButton
                size="small"
                aria-label="close"
                onClick={props.onClose}>
                <CloseIcon />
              </IconButton>
            ) : null}
          </Box>
        </DialogTitle>
      )}
      {description && (
        <DialogContent dividers sx={{p: 2}}>
          {isArray(description) ? (
            description.map((desc, index) => (
              <Typography key={index} gutterBottom>
                {desc}
              </Typography>
            ))
          ) : (
            <Typography gutterBottom>{description}</Typography>
          )}
        </DialogContent>
      )}
      {BodyComp && (
        <DialogContent dividers sx={{p: 2}}>
          {BodyComp}
        </DialogContent>
      )}
      <DialogActions sx={{m: 0, px: 2, py: 1.2}}>
        {actions.map(({label, onClick, ...btnProps}, index) => {
          return (
            <Button
              key={index}
              color="primary"
              variant="contained"
              size="medium"
              onClick={onClick}
              {...btnProps}>
              {label}
            </Button>
          );
        })}
      </DialogActions>
    </Styled.Root>
  );
};

export default ConfirmationDialog;
