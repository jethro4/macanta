import React, {useState, useEffect} from 'react';
import EmailDOForms from './EmailDOForms';
import Modal from '@macanta/components/Modal';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import * as Storage from '@macanta/utils/storage';

const EmailDOItemOption = ({
  itemId,
  modal,
  showModal: showModalProp = false,
  onClose,
  connectedContacts,
  doTitle,
}) => {
  const loggedInUserDetails = Storage.getItem('userDetails');

  const {displayMessage} = useProgressAlert();

  const [showModal, setShowModal] = useState(showModalProp);

  const handleCloseDOHistory = () => {
    setShowModal(false);

    onClose && onClose();
  };

  const onSuccess = () => {
    displayMessage('Email sent!');
    handleCloseDOHistory();
  };

  useEffect(() => {
    setShowModal(showModalProp);
  }, [showModalProp]);

  let doNotes = (
    <EmailDOForms
      itemId={itemId}
      doTitle={doTitle}
      connectedContacts={connectedContacts}
      onSuccess={onSuccess}
    />
  );

  if (modal) {
    doNotes = (
      <Modal
        headerTitle={`Compose Email (${loggedInUserDetails.email})`}
        open={showModal}
        onClose={handleCloseDOHistory}
        contentWidth={'86%'}
        contentHeight={'94%'}>
        {doNotes}
      </Modal>
    );
  }

  return doNotes;
};

export default EmailDOItemOption;
