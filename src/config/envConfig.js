import {getAppInfo} from '@macanta/utils/app';

const pjson = require('../../package.json');

const [appName] = getAppInfo();
const isStaging = appName === 'staging';

console.info(`App Name: ${appName} - Version: ${pjson.version}`);
console.info('ENV:', isStaging ? 'staging' : process.env.NODE_ENV);
console.info('API_DOMAIN:', process.env.API_DOMAIN);

export default {
  isDev: process.env.NODE_ENV === 'development',
  isStaging,
  isTest: process.env.NODE_ENV === 'test',
  apiDomain: process.env.API_DOMAIN,
};
