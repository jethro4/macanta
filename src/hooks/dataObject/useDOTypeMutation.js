import {useState} from 'react';
import useMutation from '@macanta/hooks/apollo/useMutation';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import {
  CREATE_OR_UPDATE_DO_TYPE,
  LIST_DATA_OBJECT_TYPES,
  DELETE_DO_FIELD,
  CREATE_OR_UPDATE_DO_SECTION_CONDITION,
  LIST_DO_SECTION_CONDITIONS,
  DELETE_DO_SECTION_CONDITION,
} from '@macanta/graphql/dataObjects';
import {clearSessionQuery} from '@macanta/utils/graphql';

const useDOTypeMutation = (options) => {
  const {displayMessage} = useProgressAlert();

  const [saving, setSaving] = useState(false);

  const [callDOTypeMutation, doTypeMutation] = useMutation(
    CREATE_OR_UPDATE_DO_TYPE,
    {
      alertOnComplete: false,
      alertOnError: false,
      update(cache, {data: {createOrUpdateDataObjectType}}) {
        cache.modify({
          fields: {
            listDataObjectTypes(listDataObjectTypesRef) {
              cache.modify({
                id: listDataObjectTypesRef.__ref,
                fields: {
                  items(existingItems = []) {
                    const newItemRef = cache.writeQuery({
                      data: createOrUpdateDataObjectType,
                      query: LIST_DATA_OBJECT_TYPES,
                    });

                    return [newItemRef, ...existingItems];
                  },
                },
              });
            },
          },
        });
      },
    },
  );
  const [callDeleteDOFieldMutation] = useMutation(DELETE_DO_FIELD, {
    alertOnComplete: false,
    alertOnError: false,
    update(cache, {data: {deleteDataObjectField}}) {
      const normalizedId = cache.identify({
        id: deleteDataObjectField.fieldId,
        __typename: 'Field',
      });
      cache.evict({id: normalizedId});
      cache.gc();
    },
  });
  const [callDOSectionCondition] = useMutation(
    CREATE_OR_UPDATE_DO_SECTION_CONDITION,
    {
      alertOnComplete: false,
      alertOnError: false,
      update(cache, {data: {createOrUpdateDOSectionCondition}}) {
        cache.modify({
          fields: {
            listDOSectionConditions(listDOSectionConditionsRef) {
              cache.modify({
                id: listDOSectionConditionsRef.__ref,
                fields: {
                  items(existingItems = []) {
                    const newItemRef = cache.writeQuery({
                      data: createOrUpdateDOSectionCondition,
                      query: LIST_DO_SECTION_CONDITIONS,
                    });

                    return [newItemRef, ...existingItems];
                  },
                },
              });
            },
          },
        });
      },
    },
  );
  const [callDeleteDOSectionCondition] = useMutation(
    DELETE_DO_SECTION_CONDITION,
    {
      alertOnComplete: false,
      alertOnError: false,
      update(cache, {data: {deleteDOSectionCondition}}) {
        const normalizedId = cache.identify({
          id: deleteDOSectionCondition.id,
          __typename: 'DOSectionConditionItem',
        });
        cache.evict({id: normalizedId});
        cache.gc();
      },
    },
  );

  const deleteField = async (id) => {
    await callDeleteDOFieldMutation({
      variables: {
        id,
      },
    });
  };

  const saveDetails = async ({id, name, relationships, fieldSections}) => {
    const {data = {}, errors} = await callDOTypeMutation({
      variables: {
        id,
        name,
        relationships,
        fieldSections,
      },
    });

    if (errors?.length) {
      throw errors[0];
    }

    return data.createOrUpdateDataObjectType?.id;
  };

  const saveDOSectionCondition = async (
    dataObjectId,
    dataObjectName,
    sections,
  ) => {
    await callDOSectionCondition({
      variables: {
        dataObjectId,
        dataObjectName,
        sections,
      },
    });
  };

  const deleteDOSectionCondition = (dataObjectId, sectionNames) => {
    return Promise.all(
      sectionNames.map(async (sectionName) => {
        await callDeleteDOSectionCondition({
          variables: {
            dataObjectId,
            sectionName,
          },
        });
      }),
    );
  };

  const save = async (
    values,
    fieldsToDelete,
    updatedSectionConditions,
    deletedSectionConditions,
  ) => {
    setSaving(true);

    try {
      if (fieldsToDelete?.length) {
        await Promise.all(
          fieldsToDelete.map(async (fieldId) => {
            await deleteField(fieldId);
          }),
        );
      }

      const doTypeId = await saveDetails(values);

      if (updatedSectionConditions?.length) {
        await saveDOSectionCondition(
          values.id,
          values.name,
          updatedSectionConditions,
        );
      }

      if (deletedSectionConditions?.length) {
        await deleteDOSectionCondition(values.id, deletedSectionConditions);
      }

      options?.onCompleted && options.onCompleted(doTypeId);
    } finally {
      clearSessionQuery();

      displayMessage('Saved successfully!');

      setSaving(false);
    }
  };

  return {
    saving,
    save,
    error: doTypeMutation.error,
  };
};

export default useDOTypeMutation;
