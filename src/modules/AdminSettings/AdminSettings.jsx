import React from 'react';
import {useMatch} from '@reach/router';
import {navigate} from 'gatsby';
import Breadcrumbs from '@mui/material/Breadcrumbs';
import Typography from '@mui/material/Typography';
import Section from '@macanta/containers/Section';
import OpenInNewIcon from '@mui/icons-material/OpenInNew';
import SettingsApplicationsIcon from '@mui/icons-material/SettingsApplications';
import FactCheckIcon from '@mui/icons-material/FactCheck';
import EmailIcon from '@mui/icons-material/Email';
import FormatColorFillIcon from '@mui/icons-material/FormatColorFill';
import TableRowsIcon from '@mui/icons-material/TableRows';
import ConnectWithoutContactIcon from '@mui/icons-material/ConnectWithoutContact';
import SaveIcon from '@mui/icons-material/Save';
import DeveloperBoardIcon from '@mui/icons-material/DeveloperBoard';
import SyncIcon from '@mui/icons-material/Sync';
import BuildIcon from '@mui/icons-material/Build';
import GroupIcon from '@mui/icons-material/Group';
import AutofpsSelectIcon from '@mui/icons-material/AutofpsSelect';
import DynamicFormIcon from '@mui/icons-material/DynamicForm';
import Box from '@mui/material/Box';
import Button, {ModalButton} from '@macanta/components/Button';
import Markdown from '@macanta/components/Markdown';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import useAccessPermissions from '@macanta/hooks/admin/useAccessPermissions';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {getAppInfo} from '@macanta/utils/app';
import AUTOMATION_HTML from '@macanta/fixtures/assets/automationOld';
import envConfig from '@macanta/config/envConfig';
import SettingCard from './SettingCard';
import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import * as Styled from './styles';
import * as Storage from '@macanta/utils/storage';

export const ROOT_PATH_ADMIN = '/app/admin-settings';

export const getAutoLoginLink = () => {
  const session = Storage.getItem('session');
  const autoLoginKey = session?.autoLoginKey;

  if (!autoLoginKey) {
    return '';
  }

  const {userId: loggedInUserId} = Storage.getItem('userDetails');
  const [appName] = getAppInfo();
  const goToLink = `https://${appName}.${envConfig.apiDomain}/autologin/confirmed/${loggedInUserId}/${autoLoginKey}`;

  return goToLink;
};

const ADMIN_ROUTES = [
  {
    path: 'app-details',
    icon: SettingsApplicationsIcon,
    title: 'App Details',
    description: 'General app details and configurations',
  },
  {
    path: 'query-builder',
    icon: FactCheckIcon,
    title: 'Query Builder',
    description: 'Build queries to show specific data in dashboard widgets',
  },
  {
    path: 'blast-progress',
    icon: EmailIcon,
    title: 'Blast Progress',
    description:
      'Keep track of progress details from all user emails and sms in queue',
  },
  {
    path: 'look-and-feel',
    icon: FormatColorFillIcon,
    title: 'Look & Feel',
    description:
      'Customize your brand logo and theme color tailored to your company',
  },
  {
    path: 'custom-tabs',
    icon: TableRowsIcon,
    title: 'Custom Tabs',
    description:
      'Create tabs with customized content to include in contact page sidebar tabs',
  },
  {
    path: 'contact-relationships',
    icon: ConnectWithoutContactIcon,
    title: 'Contact to Contact Relationships',
    description: 'Manage Direct and Indirect Contact Relationships',
  },
  {
    path: 'data-import-export',
    icon: SaveIcon,
    title: 'Data Import & Export',
    description: 'Import New Data or Export Existing Data',
  },
  {
    path: 'workflow-boards',
    icon: DeveloperBoardIcon,
    title: 'Workflow Boards',
    description:
      'Manage Workflow Boards for better overview of state between data objects',
  },
  {
    path: 'internal-support-emails',
    icon: EmailIcon,
    title: 'Internal Support Emails',
    description: 'Add or Remove Internal Support Emails',
  },
  {
    path: 'exclude-email-sync-domains',
    icon: SyncIcon,
    title: 'Email Sync Domain Management',
    description: 'Exclude domains from email sync',
    permission: 'allowAddRemoveEmailDomainFilter',
  },
  {
    path: 'object-builder',
    icon: BuildIcon,
    title: 'Object Builder',
    description:
      'Configure Contact or Data Object Relationships, Types and Fields',
  },
  {
    path: 'user-management',
    icon: GroupIcon,
    title: 'User Management',
    description: 'Manage user accounts and permissions',
  },
  {
    path: 'automation',
    icon: AutofpsSelectIcon,
    title: 'Automation',
    description:
      'Configure automated processes by choosing conditions and actions',
    ...(!envConfig.isStaging && {
      htmlGuide: AUTOMATION_HTML,
    }),
  },
  {
    path: 'form-builder',
    icon: DynamicFormIcon,
    title: 'Form Builder',
    description:
      'Build form templates to embed in your website with custom styles and fields',
  },
  {
    path: 'scheduler',
    icon: GroupIcon,
    title: 'Scheduler',
    description: 'Manage data object schedules',
    debug: true,
  },
];

const AdminSettings = ({children}) => {
  const isAdminSettings = useMatch(encodeURI(ROOT_PATH_ADMIN));

  return (
    <NoteTaskHistoryStyled.FullHeightGrid container>
      <NoteTaskHistoryStyled.FullHeightFlexGridColumn item xs={12}>
        {!isAdminSettings ? (
          children
        ) : (
          <Section
            fullHeight
            bodyStyle={{
              backgroundColor: '#f5f6fa',
              padding: '0.5rem',
            }}
            HeaderLeftComp={
              <Breadcrumbs aria-label="breadcrumb">
                <Styled.TitleCrumbBtn disabled>
                  <Typography color="textPrimary">Admin Settings</Typography>
                </Styled.TitleCrumbBtn>
              </Breadcrumbs>
            }>
            {!!isAdminSettings && (
              <Styled.GridContainer container>
                {ADMIN_ROUTES.map((itemProps) => (
                  <SettingsItemCard key={itemProps.path} {...itemProps} />
                ))}
              </Styled.GridContainer>
            )}
          </Section>
        )}
      </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
    </NoteTaskHistoryStyled.FullHeightGrid>
  );
};

export const SettingsItemCard = ({htmlGuide, ...props}) => {
  return !htmlGuide ? (
    <NavigationCard {...props} />
  ) : (
    <PopupHTMLCard htmlGuide={htmlGuide} {...props} />
  );
};

const NavigationCard = ({
  path,
  icon,
  title,
  description,
  permission,
  debug,
}) => {
  /* const navigate = useNavigate();*/
  const [appName] = getAppInfo();

  const shouldShow = !debug || appName === 'staging';

  const accessPermissionsQuery = useAccessPermissions({
    key: `permissionMeta.${permission}`,
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const handleSettingsItem = () => {
    navigate(`${ROOT_PATH_ADMIN}/${path}`);
  };

  return (!permission || accessPermissionsQuery.data) && shouldShow ? (
    <Styled.GridItem item xl={2.4} lg={3} md={4} sm={6} xs={12}>
      <SettingCard
        id={`${path}-card`}
        IconComp={icon}
        title={title}
        description={description}
        onClick={handleSettingsItem}
      />
    </Styled.GridItem>
  ) : null;
};

const PopupHTMLCard = ({path, icon, title, description, htmlGuide}) => {
  const goToLink = getAutoLoginLink();

  const handleViewAPI = () => {
    window.open(encodeURI(goToLink));
  };

  return (
    <Styled.GridItem item xl={2.4} lg={3} md={4} sm={6} xs={12}>
      <ModalButton
        ModalProps={{
          headerTitle: title,
          contentWidth: 800,
          bodyStyle: {
            backgroundColor: '#eee',
          },
        }}
        renderButton={(handleOpen) => (
          <SettingCard
            id={`${path}-card`}
            IconComp={icon}
            title={title}
            description={description}
            onClick={handleOpen}
          />
        )}
        renderContent={() => (
          <>
            <LoadingIndicator fill transparent />
            <Box
              style={{
                flex: 1,
                zIndex: 1001,
              }}>
              <Markdown>{htmlGuide}</Markdown>
              {!!goToLink && (
                <Box
                  style={{
                    margin: '1rem 0',
                    padding: '0 1rem',
                    display: 'flex',
                    justifyContent: 'flex-end',
                  }}>
                  <Button
                    variant="contained"
                    onClick={handleViewAPI}
                    startIcon={<OpenInNewIcon />}
                    size="medium">
                    Open {title}
                  </Button>
                </Box>
              )}
            </Box>
          </>
        )}
      />
    </Styled.GridItem>
  );
};

export default AdminSettings;
