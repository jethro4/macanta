import React, {useState, useRef} from 'react';
import isNumber from 'lodash/isNumber';
import ColumnResizer from './ColumnResizer';
import {
  DEFAULT_COLUMN_MIN_WIDTH,
  DEFAULT_COLUMN_MAX_WIDTH,
} from '@macanta/themes/applicationStyles';
import * as Styled from './styles';

const ResizableTableCell = ({
  column,
  order,
  orderBy,
  onSort,
  style,
  sortable,
  ...props
}) => {
  const [resizedCellWidth, setResizedCellWidth] = useState(0);

  const cellRef = useRef(null);
  const currentWidthRef = useRef(null);

  const colMaxWidth =
    resizedCellWidth || column.maxWidth || DEFAULT_COLUMN_MAX_WIDTH;
  const colMinWidth =
    resizedCellWidth ||
    Math.min(column.minWidth || DEFAULT_COLUMN_MIN_WIDTH, colMaxWidth);
  const sortActive = orderBy === column.id;
  const direction = sortActive ? order : 'asc';

  const getCurrentWidth = () => {
    let currentWidth;
    if (currentWidthRef.current) {
      currentWidth = currentWidthRef.current;
    } else {
      const {width} = cellRef.current?.getBoundingClientRect() || {};
      currentWidth = width || colMinWidth;
    }

    return Math.min(Math.max(colMinWidth, currentWidth), colMaxWidth);
  };

  const getNewWidth = (deltaX, currentWidth) => {
    const percentageDeltaX = deltaX / currentWidth;

    const newWidth = currentWidth + currentWidth * percentageDeltaX;

    return Math.min(Math.max(colMinWidth, newWidth), colMaxWidth);
  };

  const handleStartSlide = () => {
    const currentWidth = getCurrentWidth();

    currentWidthRef.current = currentWidth;
  };

  const handleSliding = (deltaX) => {
    const currentWidth = getCurrentWidth();

    const newWidth = getNewWidth(deltaX, currentWidth);

    isNumber(currentWidthRef.current) && setResizedCellWidth(newWidth);
  };

  const handleEndSlide = () => {
    currentWidthRef.current = null;
  };

  let tableHeadCellText = (
    <Styled.TableHeadCellText>{column.label}</Styled.TableHeadCellText>
  );

  if (column.renderHeadCellValue) {
    tableHeadCellText = column.renderHeadCellValue({column});
  }

  return (
    <Styled.TableHeadCell
      ref={cellRef}
      key={column.id}
      align={column.align}
      style={{
        minWidth: colMinWidth,
        maxWidth: colMaxWidth,
        ...style,
        width: resizedCellWidth || colMaxWidth,
        ...column.headCellStyle,
      }}
      sortDirection={sortActive ? order : false}
      {...props}>
      <Styled.TableHeadCellLabelContainer sx={column.headCellLabelStyle}>
        {sortable ? (
          <Styled.TableSortLabel
            active={sortActive}
            direction={direction}
            onClick={onSort}
            IconComponent={(iconProps) => {
              return <Styled.SortIcon active={sortActive} {...iconProps} />;
            }}>
            {tableHeadCellText}
          </Styled.TableSortLabel>
        ) : (
          tableHeadCellText
        )}
        <ColumnResizer
          disabled={column.disableResize}
          onStartSlide={handleStartSlide}
          onSliding={handleSliding}
          onEndSlide={handleEndSlide}
        />
      </Styled.TableHeadCellLabelContainer>
    </Styled.TableHeadCell>
  );
};

export default ResizableTableCell;
