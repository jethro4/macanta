import React from 'react';
import DataTable from '@macanta/containers/DataTable';
import AddOrEditDirectRelationshipOptionBtn from './AddOrEditDirectRelationshipOptionBtn';
import DeleteDirectRelationshipOptionBtn from './DeleteDirectRelationshipOptionBtn';
import * as DataObjectsSearchTableStyled from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/styles';

const CUSTOM_TABS_COLUMNS = [
  {id: 'name', label: 'Name', minWidth: 220},
  {
    id: 'description',
    label: 'Description',
    maxWidth: '100%',
  },
];

const ContactRelationshipsTable = ({data, ...props}) => {
  return (
    <DataTable
      fullHeight
      columns={CUSTOM_TABS_COLUMNS}
      data={data}
      order="desc"
      orderBy="created"
      rowsPerPage={25}
      rowsPerPageOptions={[25, 50, 100]}
      renderActionButtons={({item}) => (
        <DataObjectsSearchTableStyled.ActionButtonContainer>
          <AddOrEditDirectRelationshipOptionBtn item={item} />
          <DeleteDirectRelationshipOptionBtn id={item.id} name={item.name} />
        </DataObjectsSearchTableStyled.ActionButtonContainer>
      )}
      actionColumn={{
        show: true,
        style: {
          minWidth: 100,
        },
      }}
      {...props}
    />
  );
};

ContactRelationshipsTable.defaultProps = {
  data: [],
};

export default ContactRelationshipsTable;
