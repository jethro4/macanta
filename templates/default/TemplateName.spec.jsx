import React from 'react';
import {renderApp} from '@tests/jestSettings/utils/renderUtils';
import TemplateName from './TemplateName';
import {isInDocument} from '@tests/jestSettings/utils/assertUtils';

describe('<TemplateName>', () => {
  it('should mount', async () => {
    renderApp(<TemplateName />);

    await isInDocument('TemplateName', {isTestId: true});
  });
});
