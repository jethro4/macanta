import {useEffect} from 'react';

const useCleanUp = (callback, dependencies) => {
  useEffect(() => {
    return callback;
  }, dependencies || []);
};

export default useCleanUp;
