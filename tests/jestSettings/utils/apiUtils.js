import isArray from 'lodash/isArray';
import * as apollo from '@apollo/client';
import {waitUntil} from '@tests/jestSettings/utils/selectorUtils';
import {getTagFieldName} from '@macanta/utils/graphql';
import {asyncPromises} from '@macanta/utils/promise';

export const spyOnQuery = () => {
  return jest.spyOn(apollo, 'useQuery');
};

export const spyOnMutation = () => {
  return jest.spyOn(apollo, 'useMutation');
};

export const spyOnAsync = async (graphqlTagFieldName) => {
  if (isArray(graphqlTagFieldName)) {
    await asyncPromises(graphqlTagFieldName, async (tagFieldName) => {
      await spyOnAsync(tagFieldName);
    });

    return;
  }

  const spy = spyOnQuery();

  await waitUntil(() => expect(spy).toHaveBeenCalled());

  if (!graphqlTagFieldName) {
    await waitUntil(() =>
      expect(spy).toHaveReturnedWith(
        expect.objectContaining({
          loading: false,
        }),
      ),
    );
  } else {
    await waitUntil(() =>
      expect(spy).toHaveReturnedWith(
        expect.objectContaining({
          data: expect.objectContaining({
            [graphqlTagFieldName]: expect.anything(),
          }),
          loading: false,
        }),
      ),
    );
  }
};

export const spyOnOptions = ({graphqlTag, options, data}) => {
  if (options.onCompleted && data) {
    const spy = jest.spyOn(options, 'onCompleted');

    const graphqlTagFieldName = getTagFieldName(graphqlTag);

    expect(spy).toHaveBeenCalledWith({
      [graphqlTagFieldName]: data,
    });
  }
};

export const spyOnConsole = (level = 'debug') => {
  return jest.spyOn(global.console, level);
};
