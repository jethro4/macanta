import React, {useState} from 'react';
import Modal from '@macanta/components/Modal';
import DraftsIcon from '@mui/icons-material/Drafts';
import Button from '@macanta/components/Button';
import ExportSection from '@macanta/modules/AdminSettings/DataTools/ExportSection';

const ExportQueryBtn = ({id, name, ...props}) => {
  const [showModal, setShowModal] = useState(false);

  const handleShowModal = () => {
    setShowModal(true);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  return (
    <>
      <Button
        onClick={handleShowModal}
        size="small"
        variant="contained"
        startIcon={<DraftsIcon />}
        {...props}>
        Export
      </Button>
      <Modal
        headerTitle={`Export Query (${name})`}
        open={showModal}
        onClose={handleCloseModal}
        contentWidth={450}
        contentHeight={350}>
        <ExportSection
          style={{
            margin: 0,
          }}
          headerStyle={{
            display: 'none',
          }}
          initialValues={{
            option: 'Query',
            selectedData: {
              id,
              name,
            },
          }}
          onSuccess={handleCloseModal}
        />
      </Modal>
    </>
  );
};

export default ExportQueryBtn;
