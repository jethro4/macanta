import isArray from 'lodash/isArray';
import isString from 'lodash/isString';
import {screen, waitFor, within} from '@testing-library/react';
import {delay} from '@macanta/utils/promise';
import {spyOnAsync} from '@tests/jestSettings/utils/apiUtils';
import {splice} from '@macanta/utils/array';
import {highlight} from 'pretty-html-log';

export const wait = (timeout = 500) => {
  return delay(timeout);
};

export const waitUntil = (callback, options) => {
  return waitFor(callback, {timeout: 3000, ...options});
};

export const waitApis = async (graphqlTagFieldName) => {
  await spyOnAsync(graphqlTagFieldName);
  await wait();
};

export const selectors = {
  input: '.MuiInputBase-input',
  select: '.MuiInputBase-input ~ input',
  switch: '.MuiSwitch-input[type="checkbox"]',
  radio: 'input[type="radio"]',
  checkbox: 'input[type="checkbox"]',
  listItem: '.MuiList-root .MuiListItem-root',
  selectItem: '.MuiModal-root .MuiList-root .MuiListItem-root',
  rows: 'tbody .item-data',
  subheader: 'tbody .item-subheader',
};

export const getParentScope = (options) =>
  options?.nodeType
    ? options
    : isString(options) && selectors[options]
    ? document.querySelector(selectors[options])
    : options?.testId
    ? screen.getByTestId(options.testId)
    : options?.selector
    ? document.querySelector(options.selector)
    : options?.parent || document;

export const groupByKey = (elmArr = [], key) => {
  const result = elmArr.map((elm) => {
    const groupedValue = selectAll(key, elm);

    return groupedValue;
  });

  return result;
};

export const groupBySelectors = (arr = [], selectorsArr) => {
  let groupedArr = [],
    keysArr = selectorsArr;

  for (let index = 0; index < keysArr.length; index++) {
    const {
      currentItems: currentKeys,
      removedItems: removedKeys,
      isLastItem,
    } = splice(keysArr, 0, 1);
    const currentKey = removedKeys[0];

    groupedArr = groupByKey(arr, currentKey);

    if (!isLastItem) {
      groupedArr = groupedArr.map((groupedItem) => {
        return groupBySelectors(groupedItem, currentKeys);
      });
    }
  }

  return groupedArr;
};

export const selectAll = (selectorsArg, options) => {
  if (isArray(selectorsArg) && selectorsArg.length > 1) {
    const allElements = groupBySelectors(
      selectAll(selectorsArg[0], options),
      selectorsArg.slice(1),
    );

    return allElements;
  }

  const selectorsArr = isArray(selectorsArg) ? selectorsArg : [selectorsArg];
  const parentScope = getParentScope(options);
  const updatedSelectors = selectorsArr.map((s) => selectors[s] || s).join(' ');
  const elms = parentScope.querySelectorAll(updatedSelectors);
  const elmsArr = Array.from(elms);

  if (!elmsArr.length) {
    throw `No elements found for selectors: "${updatedSelectors}"`;
  }

  return options?.order ? elmsArr[options.order - 1] : elmsArr;
};
export const transformOptions = (options) => ({
  order: options?.order || 1,
  ...options,
});

const getFirstElement = (callback) => (options) => {
  return callback(transformOptions(options));
};

// Selects input, textarea & select elements
export const selectInput = getFirstElement((options) => {
  if (options?.value) {
    return screen.getByDisplayValue(options.value);
  } else {
    const elms = selectAll(['input'], options);

    return elms;
  }
});

export const selectSelect = getFirstElement((options) => {
  if (options?.value) {
    return screen.getByDisplayValue(options.value);
  } else {
    const elms = selectAll(['select'], options);

    return elms;
  }
});

export const selectSwitch = getFirstElement((options) => {
  const elms = selectAll(['switch'], options);

  return elms;
});

export const selectRadio = getFirstElement((options) => {
  const elms = selectAll(
    [
      `input[type="radio"]${
        options?.value ? `[value="${options.value}"]` : ''
      }`,
    ],
    options,
  );

  return elms;
});

export const selectButtonGroup = (buttonNumber, options) => {
  const elms = selectAll(['.MuiButtonGroup-root .MuiButton-root'], options);

  return elms[buttonNumber - 1 || 0];
};

export const selectRows = (options) => {
  const elms = selectAll(['tbody tr'], options);

  return elms;
};

export const selectRow = (rowNumber, options) => {
  const elms = selectRows(options);

  return elms[rowNumber - 1 || 0];
};

export const selectColumns = (rowNumber, options) => {
  const elms = selectAll(['td'], {parent: selectRow(rowNumber, options)});

  return elms;
};

export const selectColumn = ([rowNumber, columnNumber], options) => {
  const elms = selectColumns(rowNumber, options);

  return elms[columnNumber - 1 || 0];
};

export const selectButton = (label) => {
  const labelElm = screen.getByText(label);
  const elm = labelElm.closest('button');

  return elm;
};

export const selectLink = (label) => {
  const labelElm = screen.getByText(label);
  const elm = labelElm.closest('a');

  return elm;
};

export const selectListItem = (itemNumber, options) => {
  const elms = selectAll(['listItem'], options);

  return elms[itemNumber - 1 || 0];
};

export const selectText = (text, options) => {
  if (isArray(text) && text.length > 1) {
    return text.map((t) => selectText(t, options));
  }

  const parentScope = getParentScope(options);
  const scope = within(parentScope);

  try {
    const textElm = scope.getByText(text);

    return textElm;
  } catch (e) {
    let message = `Unable to find an element with the text: "${text}". This could be because the text is broken up by multiple elements. In this case, you can provide a function for your text matcher to make your matcher more flexible.`;

    if (!e.message.includes('Unable to find an element')) {
      message = `Found multiple elements with the text: "${text}"`;
    }

    const err = {
      ...e,
      message,
    };

    throw new SelectorException(
      err,
      '"selectText" -> "getByText"',
      parentScope,
    );
  }
};

export const selectLabelText = (text, options) => {
  const parentScope = getParentScope(options);
  const scope = within(parentScope);

  const textElm = scope.getByLabelText(text);

  return textElm;
};

export const selectByText = (text, selector, options) => {
  if (isArray(text) && text.length > 1) {
    return text.map((t) => selectByText(t, selector, options));
  }

  const elms = selectAll(selector, options);

  const filteredElm = elms.find((elm) => elm.textContent === text);

  return filteredElm;
};

export const selectByValue = (text, options) => {
  const parentScope = getParentScope(options);
  const scope = within(parentScope);

  const textElm = scope.getByDisplayValue(text);

  return textElm;
};

class SelectorException extends Error {
  constructor(e, functionName = '', scopeElm) {
    let errorMessages = [`Error: ${e.message}\nFunction: ${functionName}`];

    if (scopeElm.outerHTML) {
      errorMessages = errorMessages.concat(
        `Element Scope:\n${highlight(scopeElm.outerHTML)}`,
      );
    }

    super(errorMessages.join('\n'));

    this.name = 'SelectorException';
    this.stack = e.stack;
  }
}
