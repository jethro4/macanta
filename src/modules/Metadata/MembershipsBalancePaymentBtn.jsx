import React, {useState, useLayoutEffect} from 'react';
import Box from '@mui/material/Box';
import CreditCardIcon from '@mui/icons-material/CreditCard';
import useContactId from '@macanta/hooks/useContactId';
import useContactMetadata from '@macanta/hooks/metadata/useContactMetadata';
import ContactQuery from '@macanta/modules/hoc/ContactQuery';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import Button from '@macanta/components/Button';
import * as TableStyled from '@macanta/components/Table/styles';

const MembershipsBalancePaymentBtn = ({itemId, value}) => {
  const contactId = useContactId();

  return (
    <ContactQuery
      id={contactId}
      options={{fetchPolicy: FETCH_POLICIES.CACHE_FIRST}}>
      {({data: selectedContact}) => {
        return selectedContact ? (
          <MembershipsBalancePaymentBtnWithContactEmail
            itemId={itemId}
            value={value}
            email={selectedContact.email}
          />
        ) : null;
      }}
    </ContactQuery>
  );
};

const MembershipsBalancePaymentBtnWithContactEmail = ({
  itemId,
  value,
  email,
}) => {
  const [paymentURL, setPaymentURL] = useState('');
  const {data: contactMetadata} = useContactMetadata({
    email,
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const membershipsData = contactMetadata?.getContactMetadata?.items.find(
    (item) => item.key === 'memberships',
  );

  useLayoutEffect(() => {
    if (membershipsData?.value) {
      try {
        const membershipItems =
          membershipsData?.value && JSON.parse(membershipsData?.value);
        const balancePaymentURL = membershipItems
          ? membershipItems[itemId]
          : '';

        setPaymentURL(balancePaymentURL);
      } catch (err) {
        console.error(err);
      }
    }
  }, [membershipsData]);

  return (
    <Box
      style={{
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
      }}>
      <TableStyled.TableBodyCellLabel>{value}</TableStyled.TableBodyCellLabel>
      {paymentURL && (
        <Button
          size="small"
          variant="contained"
          onClick={() => {
            window.open(paymentURL);
          }}
          startIcon={<CreditCardIcon />}>
          Payment
        </Button>
      )}
    </Box>
  );
};

export default MembershipsBalancePaymentBtn;
