import React from 'react';

const RawDate = (props) => {
  return <input type="date" {...props} />;
};

export default RawDate;
