import React from 'react';
import Box from '@mui/material/Box';
import Tooltip from '@macanta/components/Tooltip';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import * as Styled from './styles';

const IconButton = ({
  TooltipProps,
  loading,
  disabled,
  style,
  children,
  ...props
}) => {
  const ariaLabel = TooltipProps?.title;

  let buttonComp = (
    <Styled.IconButton
      disabled={loading || disabled}
      style={Object.assign(
        {},
        style,
        loading && {
          pointerEvents: 'none',
        },
      )}
      {...props}
      aria-label={ariaLabel}>
      {loading && (
        <Box
          style={{
            position: 'absolute',
            display: 'flex',
            justifyContent: 'center',
          }}>
          <LoadingIndicator transparent size={16} />
        </Box>
      )}
      <Box
        style={{
          display: 'flex',
          opacity: !loading ? 1 : 0,
        }}>
        {children}
      </Box>
    </Styled.IconButton>
  );

  if (TooltipProps?.title) {
    buttonComp = <Tooltip {...TooltipProps}>{buttonComp}</Tooltip>;
  }

  return buttonComp;
};

export default IconButton;
