import useQuery from '@macanta/hooks/apollo/useQuery';
import {GET_ACCESS_PERMISSIONS} from '@macanta/graphql/permissions';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import * as Storage from '@macanta/utils/storage';

const PermissionMetaQuery = ({children, validate}) => {
  const userDetails = Storage.getItem('userDetails');

  const {data} = useQuery(GET_ACCESS_PERMISSIONS, {
    variables: {
      email: userDetails?.email,
    },
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
    onError() {},
  });

  const isAdmin = data?.getAccessPermissions?.isAdmin;
  const permissionMeta = data?.getAccessPermissions?.permissionMeta;
  let hasAccess = false;

  if (isAdmin) {
    hasAccess = true;
  } else if (validate === 'deleteContact') {
    hasAccess = !!permissionMeta?.allowUserDeleteContact;
  } else if (validate === 'deleteDO') {
    hasAccess = !!permissionMeta?.allowUserDeleteDOItem;
  } else if (validate === 'deleteDOAttachment') {
    hasAccess = !!permissionMeta?.allowUserDeleteDOAttachment;
  }

  return hasAccess ? children : null;
};

export default PermissionMetaQuery;
