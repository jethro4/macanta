import React from 'react';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import * as Styled from './styles';

const Panel = ({removeHidden, expanded, id, item, onChange}) => {
  const handleChange = (event, isExpanded) => {
    onChange(isExpanded ? id : false);
  };

  return (
    <Styled.Panel expanded={expanded} onChange={handleChange}>
      <Styled.PanelHeader
        expandIcon={<ExpandMoreIcon />}
        aria-controls="panel1bh-content"
        id="panel1bh-header">
        <Styled.PanelTitle>{item.title}</Styled.PanelTitle>
      </Styled.PanelHeader>
      <Styled.PanelBody>
        {(!removeHidden || expanded) && item.bodyComp}
      </Styled.PanelBody>
    </Styled.Panel>
  );
};

export default Panel;
