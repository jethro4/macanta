import cloneDeep from 'lodash/cloneDeep';
import * as apollo from '@apollo/client';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import {getTagFieldName} from '@macanta/utils/graphql';

const useMutation = (graphqlTag, optionsArg) => {
  const tagFieldName = getTagFieldName(graphqlTag);

  const {displayMessage} = useProgressAlert();

  const {
    successMessage,
    errorMessage,
    alertOnError,
    alertOnComplete,
    formatVariables,
    update,
    onCompleted,
    onError,
    ...options
  } = Object.assign(
    {
      successMessage: 'Saved successfully',
      alertOnError: true,
      alertOnComplete: true,
    },
    optionsArg,
  );

  const [callMutation, mutationState] = apollo.useMutation(graphqlTag, {
    ...options,
    update(...args) {
      const argsArr = cloneDeep(args);
      const result = argsArr[1].data?.[tagFieldName];

      if (result) {
        argsArr[1].result = result;

        update?.(...argsArr);
      }
    },
    onError() {},
  });

  const handleCallMutation = async ({variables, context, ...opts}) => {
    const variablesInput = formatVariables?.(variables) || variables;

    const response = await callMutation({
      ...opts,
      variables: {
        input: variablesInput,
      },
      context: {
        removeAppInfo: true,
        ...context,
      },
    });

    let error = errorMessage
      ? {message: errorMessage}
      : response.errors?.[0] || response.errors;
    error = error?.message ? error : {message: error || 'Something has failed'};
    const errorMsg = error.message;

    if (response.errors) {
      console.error('Error on useMutation', errorMsg);

      if (alertOnError) {
        displayMessage({
          message: errorMsg,
          severity: 'error',
        });
      }

      onError?.(error);

      return {error};
    } else if (response.data) {
      const result = response.data[tagFieldName];

      if (alertOnComplete) {
        displayMessage({
          message: successMessage,
          severity: 'success',
        });
      }

      onCompleted?.({...response, result});

      return {...response, result};
    }
  };

  return [handleCallMutation, mutationState];
};

export default useMutation;
