import {useState, useLayoutEffect} from 'react';
import useUserGuides from '@macanta/hooks/useUserGuides';

const useContactWindowGuide = () => {
  const [contactWindowGuide, setContactWindow] = useState();
  const {general} = useUserGuides();

  useLayoutEffect(() => {
    setContactWindow(
      general.find((guide) => guide.subType === 'Contact Window'),
    );
  }, [general]);

  return contactWindowGuide;
};

export default useContactWindowGuide;
