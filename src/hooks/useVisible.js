import {useContext} from 'react';
import ShowContext from '@macanta/containers/Show/ShowContext';

const useVisible = () => {
  const {visible} = useContext(ShowContext);

  return visible;
};

export default useVisible;
