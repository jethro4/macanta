import useQuery from '@macanta/hooks/apollo/useQuery';
import {GET_EMAIL_ACTIONS} from '@macanta/graphql/automation';
import {FIELD_KEY_MAPPING} from '@macanta/hooks/automation/useEmailAction';

const useEmailActions = (options) => {
  const query = useQuery(GET_EMAIL_ACTIONS, {
    keyMapping: FIELD_KEY_MAPPING,
    transformResult: (result) => result?.items,
    ...options,
  });

  return query;
};

export default useEmailActions;
