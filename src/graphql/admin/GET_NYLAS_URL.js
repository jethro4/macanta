import {gql} from '@apollo/client';

export default gql`
  query getNylasUrl($appName: String!, $apiKey: String!, $email: String!) {
    getNylasUrl(appName: $appName, apiKey: $apiKey, email: $email) {
      url
    }
  }
`;
