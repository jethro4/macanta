import {experimentalStyled as styled} from '@mui/material/styles';

import MUIButtonGroup from '@mui/material/ButtonGroup';

export const Root = styled(MUIButtonGroup)`
  margin-bottom: -4px;

  & .MuiButtonGroup-grouped {
    margin-bottom: 4px;
    border-radius: 0px;

    ${({increaseBtnMinWidth}) => increaseBtnMinWidth && 'min-width: 100px;'}
  }

  & .MuiButton-text.MuiButtonGroup-grouped {
    color: #999;
    border-color: #eee !important;
    padding: 0 0.8rem;
    min-width: unset;

    &.buttonGroup-selected {
      color: ${({theme}) => theme.palette.primary.main};

      .MuiButton-label {
        position: relative;

        &:after {
          content: '';
          position: absolute;
          bottom: -4px;
          left: 50%;
          transform: translateX(-50%);
          height: 2px;
          display: block;
          width: 100%;
          background-color: ${({theme}) => theme.palette.tertiary.main};
        }
      }
    }
  }

  &.MuiButtonGroup-contained {
    box-shadow: none;

    & .MuiButton-contained.MuiButtonGroup-grouped {
      background-color: transparent;
      border-right: none !important;

      .MuiButton-label {
        color: ${({theme}) => theme.palette.text.secondary};
      }

      &.buttonGroup-selected {
        background-color: ${({theme}) => theme.palette.primary.main};
        border-radius: 20px;

        .MuiButton-label {
          color: ${({theme}) => theme.palette.common.white};
        }
      }
    }
  }
`;
