import useContact from '@macanta/hooks/contact/useContact';
import isFunction from 'lodash/isFunction';
// import * as Storage from '@macanta/utils/storage';

const ContactQuery = ({id, children, onCompleted, options}) => {
  const contactsQuery = useContact(id, {
    ...(onCompleted && {
      onCompleted: (responseData) => {
        onCompleted(responseData.listContacts.items[0]);
      },
    }),
    ...options,
  });

  return isFunction(children) ? children(contactsQuery) : null;
};

export default ContactQuery;
