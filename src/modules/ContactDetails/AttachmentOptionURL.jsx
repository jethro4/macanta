import React, {useState} from 'react';
import UploadUrlForms from '@macanta/modules/DataObjectItems/DOItemForms/Attachments/UploadUrlForms';
import Modal from '@macanta/components/Modal';

const AttachmentOptionURL = ({
  showModal: showModalProp = false,
  onClose,
  onSelect,
}) => {
  const [showModal, setShowModal] = useState(showModalProp);

  const handleCloseModal = () => {
    setShowModal(false);

    onClose && onClose();
  };

  const handleSuccess = (...args) => {
    handleCloseModal();

    onSelect(...args);
  };

  return (
    <Modal
      headerTitle="Attach File URL"
      open={showModal}
      onClose={handleCloseModal}>
      <UploadUrlForms onSubmit={handleSuccess} />
    </Modal>
  );
};

export default AttachmentOptionURL;
