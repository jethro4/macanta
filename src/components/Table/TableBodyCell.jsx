import React from 'react';
import isArray from 'lodash/isArray';
import isObject from 'lodash/isObject';
import Typography from '@mui/material/Typography';
import BodyCellValue from './BodyCellValue';
import * as Styled from './styles';

let TableBodyCell = ({
  item,
  column,
  numOfTextLines,
  disableLineClamp,
  origValue,
  value,
  subtext,
  colMinWidth,
  colMaxWidth,
  handlers,
}) => {
  let tableBodyCellText = (
    <Styled.TableBodyCellLabel>{value}</Styled.TableBodyCellLabel>
  );

  if (column.emptyMessage && !value) {
    tableBodyCellText = (
      <Typography
        sx={{
          fontSize: 'inherit',
          color: '#aaa',
        }}>
        {column.emptyMessage}
      </Typography>
    );
  } else if (column.renderBodyCellValue) {
    tableBodyCellText = column.renderBodyCellValue({
      value,
      origValue,
      item,
      handlers,
    });
  } else if (isArray(value)) {
    tableBodyCellText = value.map((v, vIndex) => (
      <BodyCellValue key={v.id || vIndex} data={v} />
    ));
  } else if (isObject(value)) {
    tableBodyCellText = <BodyCellValue data={value} />;
  }

  return (
    <Styled.TableBodyCell
      align={column.align}
      style={{
        minWidth: colMinWidth,
        maxWidth: colMaxWidth,
        ...column.bodyCellStyle,
      }}
      sx={{
        ...column.getBodyCellStyle?.({value, origValue, item}),
      }}
      {...(column?.colSpan && {
        colSpan: column.colSpan,
      })}>
      <Styled.TableBodyCellLabelContainer
        className="cellbody-container"
        disableLineClamp={disableLineClamp}
        lines={numOfTextLines}
        style={column.textStyle}>
        {tableBodyCellText}
      </Styled.TableBodyCellLabelContainer>
      {!!subtext && <Styled.Subtext>{subtext}</Styled.Subtext>}
    </Styled.TableBodyCell>
  );
};

TableBodyCell = React.memo(TableBodyCell);

export default TableBodyCell;
