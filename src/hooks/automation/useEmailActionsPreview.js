import useQuery from '@macanta/hooks/apollo/useQuery';
import {GET_EMAIL_ACTIONS_PREVIEW} from '@macanta/graphql/automation';

const useEmailActionsPreview = (options) => {
  const query = useQuery(GET_EMAIL_ACTIONS_PREVIEW, options);

  return query;
};

export default useEmailActionsPreview;
