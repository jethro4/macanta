import React from 'react';
import Helmet from 'react-helmet';
import useQuery from '@macanta/hooks/apollo/useQuery';
import AddIcon from '@mui/icons-material/Add';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import Breadcrumbs from '@mui/material/Breadcrumbs';
import Typography from '@mui/material/Typography';
import {ModalButton} from '@macanta/components/Button';
import Section from '@macanta/containers/Section';
import DataTable from '@macanta/containers/DataTable';
import {ROOT_PATH_ADMIN} from '@macanta/modules/AdminSettings/AdminSettings';
import {LIST_FORM_BUILDERS} from '@macanta/graphql/admin';
import FormBuilderActionButtons from './FormBuilderActionButtons';
import BuilderRoot from './Forms/BuilderRoot';
import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import * as AdminSettingsStyled from '@macanta/modules/AdminSettings/styles';
import {navigate} from 'gatsby';

const FORM_BUILDERS_COLUMNS = [
  {id: 'title', label: 'Title', minWidth: 220},
  {
    id: 'type',
    label: 'Type',
    minWidth: 180,
  },
  {
    id: 'description',
    label: 'Description',
    maxWidth: '100%',
  },
  {
    label: '',
    renderBodyCellValue: ({item}) => <FormBuilderActionButtons item={item} />,
    disableSort: true,
    minWidth: 100,
    align: 'center',
  },
];

const FormBuilder = () => {
  /* const navigate = useNavigate();*/

  const formBuildersQuery = useQuery(LIST_FORM_BUILDERS);

  const formBuilders = formBuildersQuery.data?.listFormBuilders;

  const handleBack = () => {
    navigate(ROOT_PATH_ADMIN);
  };

  return (
    <>
      <Helmet>
        <link type="text/css" rel="stylesheet" href="/formStyles.css" />
      </Helmet>
      <NoteTaskHistoryStyled.FullHeightGrid container>
        <NoteTaskHistoryStyled.FullHeightFlexGridColumn item xs={12} lg={6}>
          <Section
            fullHeight
            bodyStyle={{
              backgroundColor: 'white',
              padding: 0,
            }}
            HeaderLeftComp={
              <Breadcrumbs aria-label="breadcrumb">
                <AdminSettingsStyled.TitleCrumbBtn
                  startIcon={
                    <ArrowBackIcon
                      style={{
                        color: '#333',
                      }}
                    />
                  }
                  onClick={handleBack}
                  disabled={false}>
                  <Typography color="textPrimary">Admin Settings</Typography>
                </AdminSettingsStyled.TitleCrumbBtn>
                <Typography>Form Builder</Typography>
              </Breadcrumbs>
            }
            HeaderRightComp={
              <ModalButton
                size="small"
                variant="contained"
                startIcon={<AddIcon />}
                ModalProps={{
                  headerTitle: 'Add Template',
                  contentWidth: 1400,
                  contentHeight: '96%',
                  bodyStyle: {
                    backgroundColor: '#f5f6fa',
                  },
                }}
                renderContent={(handleClose) => (
                  <BuilderRoot onClose={handleClose} />
                )}>
                Create Form
              </ModalButton>
            }>
            <DataTable
              fullHeight
              loading={formBuildersQuery.loading}
              columns={FORM_BUILDERS_COLUMNS}
              data={formBuilders}
            />
          </Section>
        </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
      </NoteTaskHistoryStyled.FullHeightGrid>
    </>
  );
};

export default FormBuilder;
