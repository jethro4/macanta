import {gql} from '@apollo/client';

export default gql`
  mutation deleteWorkflowBoard(
    $deleteWorkflowBoardInput: DeleteWorkflowBoardInput!
  ) {
    deleteWorkflowBoard(input: $deleteWorkflowBoardInput) {
      id
      success
    }
  }
`;
