import React, {useState} from 'react';
import AddIcon from '@mui/icons-material/Add';
import {PopoverButton} from '@macanta/components/Button';
import UserForm from '@macanta/modules/AdminSettings/UserManagement/UserForm';
import SelectPlanForm from '@macanta/modules/AdminSettings/UserManagement/AppSubscriptionForm/SelectPlanForm';
import useAppSubscription from '@macanta/hooks/app/useAppSubscription';
import useValue from '@macanta/hooks/base/useValue';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';

const AppSubscriptionForm = () => {
  const appSubscriptionQuery = useAppSubscription({
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const defaultPlanId = appSubscriptionQuery.planId;
  const options = appSubscriptionQuery.options;

  const defaultOption = useValue(
    () =>
      defaultPlanId
        ? options.find((o) => o.id === defaultPlanId)
        : options?.[0],
    [appSubscriptionQuery.planId, appSubscriptionQuery.options],
  );

  const [openPlanForm, setOpenPlanForm] = useState(false);
  const [selectedOption, setSelectedOption] = useState(defaultOption);

  const handleOpenPlanForm = () => {
    setOpenPlanForm(true);
  };

  const handleClosePlanForm = () => {
    setOpenPlanForm(false);
  };

  const handleOpenOtherForms = () => {
    handleClosePlanForm();
  };

  const isActive = appSubscriptionQuery.isActive;

  return (
    <>
      <PopoverButton
        size="small"
        variant="contained"
        startIcon={<AddIcon />}
        PopoverProps={{
          open: openPlanForm,
          onOpen: handleOpenPlanForm,
          onClose: handleClosePlanForm,
          title: 'Add User',
          contentWidth: isActive ? 380 : 500,
          contentMaxHeight: 600,
        }}
        renderContent={(handleClose) =>
          isActive ? (
            <UserForm
              // onCreate={appSubscriptionQuery.refetch}
              onClose={handleClose}
            />
          ) : (
            <SelectPlanForm
              selectedOption={selectedOption}
              setSelectedOption={setSelectedOption}
              options={options}
              onProceed={handleOpenOtherForms}
            />
          )
        }>
        Add User
      </PopoverButton>
    </>
  );
};

export default AppSubscriptionForm;
