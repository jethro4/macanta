Preview:

```jsx
import React from 'react';
import FormField from '@macanta/containers/FormField';

<FormField
  type="TextArea"
  variant="outlined"
  label="Sample label"
  value="Hello World"
  onChange={(value) => {
    console.info('Value has changed:', value);
  }}
  size="small"
/>;
```
