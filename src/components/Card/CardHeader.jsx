import * as React from 'react';
import * as Styled from './styles';

const CardHeader = (props) => <Styled.CardHeader {...props} />;

export default CardHeader;
