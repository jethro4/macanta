import React from 'react';

const RawTextInput = (props) => {
  return <input type="text" {...props} />;
};

export default RawTextInput;
