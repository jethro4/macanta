import {experimentalStyled as styled} from '@mui/material/styles';

import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';

export const Root = styled(Box)`
  height: 100%;
  display: flex;
  flex-direction: column;
  padding: ${({theme}) => theme.spacing(2)} 0 0;
`;

export const Body = styled(Box)`
  flex: 1;
  display: flex;
`;

export const EmptyMessage = styled(Typography)`
  padding-top: ${({theme}) => theme.spacing(4)};
`;
