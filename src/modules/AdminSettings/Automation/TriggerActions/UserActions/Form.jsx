import React from 'react';
import Box from '@mui/material/Box';
import FlexGrid from '@macanta/components/Base/FlexGrid';
import BasicInfoForm from '@macanta/modules/AdminSettings/Automation/TriggerActions/SubForms/BasicInfoForm';
import withForm from '@macanta/components/Form/withForm';
import useDORelationships from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDORelationships';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import BodySection from '@macanta/modules/AdminSettings/UserManagement/BodySection';
import Divider from '@macanta/components/Base/Divider';
import FormField from '@macanta/containers/FormField';
import SelectUserField from '@macanta/modules/NoteTaskForms/SelectUserField';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import ActionForm from '@macanta/modules/AdminSettings/Automation/TriggerActions/SubForms/ActionForm';

let Form = ({
  // onSave,
  ...props
}) => {
  const {values, setFieldValue} = props;

  const doTypesQuery = useDOTypes({
    removeEmail: true,
    includeContactObject: false,
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });
  const doRelationshipsQuery = useDORelationships(values.groupId, {
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });
  // const [data, setData] = useState({});

  // const handleChange = (val) => {};

  // const handleSave = () => {
  //   onSave();
  // };

  return (
    <ActionForm>
      <FlexGrid
        {...props}
        cols={2}
        margin={[0, '2rem']}
        // border={[0, 1]}
        sx={{
          padding: '1rem',
          height: '100%',
          ...props.sx,
        }}>
        <Box colWidth={0.7}>
          <BasicInfoForm
            labelPosition="normal"
            actionLabel="User Action Name"
            renderOtherForms={() => (
              <FormField
                name="doTitle"
                labelPosition="normal"
                loading={doTypesQuery.initLoading}
                type="Select"
                options={doTypesQuery.result}
                onChange={(val, item) => setFieldValue('groupId', item.id)}
                labelKey="title"
                valueKey="title"
                label="Data Object Type"
                fullWidth
                size="small"
                variant="outlined"
              />
            )}
          />
        </Box>

        <BodySection
          title="Settings"
          bodyStyle={{
            padding: '1rem',
          }}>
          <FlexGrid cols={2} margin={['1rem', '1.5rem']}>
            <SelectUserField
              labelPosition="normal"
              name="appUser"
              fieldValueKey="id"
              label="For Specific Salesferry User"
              variant="outlined"
              size="small"
            />

            {!!values.groupId && (
              <FormField
                name="contactRelationship"
                labelPosition="normal"
                loading={doRelationshipsQuery.initLoading}
                type="Select"
                options={doRelationshipsQuery.result}
                labelKey="role"
                valueKey="id"
                label="Contact Relationship"
                size="small"
                variant="outlined"
              />
            )}
          </FlexGrid>

          <Divider
            style={{
              margin: '1.5rem 0 1rem',
              borderBottom: '1px dashed #eee',
            }}
          />

          <FormField
            name="actionDetails"
            labelPosition="normal"
            type="TextArea"
            label="Action Details"
            fullWidth
            size="small"
            variant="outlined"
          />
        </BodySection>
      </FlexGrid>
    </ActionForm>
  );
};

Form = withForm(Form);

export default Form;
