import React from 'react';

const ShowContext = React.createContext({
  visible: false,
});

export default ShowContext;
