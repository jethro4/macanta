import React from 'react';
import {useFormikContext} from 'formik';
import Typography from '@mui/material/Typography';
import CriteriaConditionForms from './CriteriaConditionForms';
import AddConditionBtn from './AddConditionBtn';
import * as Styled from './styles';

const CriteriaForms = ({
  loading,
  title,
  data,
  required,
  style,
  type,
  choices,
  error,
  getNewItem,
  onChange,
  oneItem,
  noSelection,
  noLogic,
  placeholder,
  ...props
}) => {
  const {setValues} = useFormikContext();

  const handleAddQuery = (logic = '') => {
    setValues((values) => {
      const valueObj = {
        [type]: [...values[type], getNewItem(type, logic)],
      };

      onChange && onChange(valueObj);

      return {
        ...values,
        ...valueObj,
      };
    });
  };

  const handleEditQuery = (id) => (val, key) => {
    setValues((values) => {
      const valueObj = {
        [type]: values[type].map((d, index) => {
          if (
            (key !== 'logic' && d.id === id) ||
            (key === 'logic' && values[type][index - 1]?.id === id)
          ) {
            return {
              ...d,
              [key]: val,
              ...(!['contactConditions', 'userConditions'].includes(type) &&
              key === 'name'
                ? {
                    value: null,
                    values: null,
                  }
                : key === 'value'
                ? {
                    values: null,
                  }
                : key === 'values'
                ? {
                    value: null,
                  }
                : null),
            };
          }

          return d;
        }),
      };

      onChange && onChange(valueObj);

      return {
        ...values,
        ...valueObj,
      };
    });
  };

  const handleDeleteQuery = (id) => () => {
    setValues((values) => {
      const currentItems = values[type].slice();
      const updatedItems = currentItems.filter((d) => d.id !== id);
      const deletedIndex = currentItems.findIndex((d) => d.id === id);

      if (updatedItems[deletedIndex]) {
        const deletedItemLogic = currentItems[deletedIndex].logic;
        updatedItems[deletedIndex].logic = deletedItemLogic;
      }

      const valueObj = {
        [type]: updatedItems,
      };

      onChange && onChange(valueObj);

      return {
        ...values,
        ...valueObj,
      };
    });
  };

  const empty = data.length === 0;

  return (
    <Styled.SubSection
      style={{
        ...style,
        ...(empty && {
          boxShadow: 'none',
        }),
      }}
      headerStyle={{
        backgroundColor: 'white',
        minHeight: '3rem',
        ...(empty && {
          borderBottom: 0,
          borderRadius: 4,
        }),
      }}
      title={title}
      bodyStyle={{
        padding: '1rem 0.4rem 1rem 1rem',
        ...(empty && {
          padding: 0,
        }),
      }}
      HeaderRightComp={
        empty && (
          <AddConditionBtn
            icon
            onAddQuery={handleAddQuery}
            disableSelection={empty}
          />
        )
      }
      {...props}>
      {!empty &&
        data.map((d, index) => {
          const lastItem = index === data.length - 1;

          return (
            <CriteriaConditionForms
              loading={loading}
              key={d.id}
              index={index}
              required={data.length === 1 && required}
              oneItem={oneItem}
              noSelection={noSelection}
              noLogic={noLogic}
              placeholder={placeholder}
              lastItem={lastItem}
              condition={{
                ...d,
                ...(data[index + 1] && {
                  logic: data[index + 1].logic,
                }),
              }}
              type={type}
              choices={choices}
              onAddQuery={handleAddQuery}
              onEditQuery={handleEditQuery(d.id)}
              onDeleteQuery={handleDeleteQuery(d.id)}
            />
          );
        })}
      {!!error && (
        <Typography
          color="error"
          style={{
            fontSize: '0.75rem',
            textAlign: 'center',
            marginTop: '1rem',
          }}>
          {error}
        </Typography>
      )}
    </Styled.SubSection>
  );
};

CriteriaForms.defaultProps = {
  data: [],
};

export default CriteriaForms;
