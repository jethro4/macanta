import React, {useMemo} from 'react';
import useUsers from '@macanta/hooks/admin/useUsers';
import {sortArrayByObjectKey} from '@macanta/utils/array';
import * as Storage from '@macanta/utils/storage';
import FormField from '@macanta/containers/FormField';

const SelectUserField = ({
  fieldLabelKey,
  fieldValueKey,
  withLoggedInUser,
  ...props
}) => {
  const {email: userEmail} = Storage.getItem('userDetails');

  const usersQuery = useUsers();

  const updatedUsers = usersQuery.data;

  const users = useMemo(() => {
    let choices = [];

    if (updatedUsers) {
      choices = sortArrayByObjectKey(
        updatedUsers.map((user) => ({
          ...user,
          label: user[fieldLabelKey], //TODO: check if this should be replaced by full name `${user.firstName} ${user.lastName}`.trim(),
          value: user[fieldValueKey],
        })),
        'label',
      );
    } else {
      choices = [{label: userEmail, value: userEmail}].concat(
        props.value && userEmail !== props.value
          ? {
              label: props.value,
              value: props.value,
            }
          : [],
      );
    }

    if (withLoggedInUser) {
      choices = [
        {
          label: 'Logged In User',
          value: 'Logged In User',
        },
      ].concat(choices);
    }

    return choices;
  }, [updatedUsers]);

  return (
    <FormField
      type="Select"
      loading={usersQuery.initLoading}
      options={users}
      {...props}
    />
  );
};

SelectUserField.defaultProps = {
  fieldLabelKey: 'email',
  fieldValueKey: 'email',
  defaultTags: [],
  onChange: () => {},
};

export default SelectUserField;
