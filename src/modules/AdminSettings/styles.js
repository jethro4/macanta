import {experimentalStyled as styled} from '@mui/material/styles';
import Card from '@mui/material/Card';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Button from '@macanta/components/Button';
import * as AttachmentStyled from '@macanta/modules/DataObjectItems/DOItemForms/Attachments/styles';

export const SettingCard = styled(Card)`
  height: 160px;
  display: flex;
  flex-direction: column;
  position: relative;
  background-color: ${({theme}) => theme.palette.common.white};
  justify-content: center;
  align-items: center;
`;

export const ActionArea = styled(AttachmentStyled.CardActionArea)`
  flex: 1;
  display: flex;
  flex-direction: column;
  padding: 0 ${({theme}) => theme.spacing(2)};
`;

export const IconContainer = styled(Box)`
  flex: 0.8;
  display: flex;
  align-items: flex-end;
`;

export const TextContainer = styled(Box)`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: ${({theme}) => theme.spacing(1.2)};
`;

export const Title = styled(Typography)`
  font-size: 1.12rem;
  font-weight: bold;
  letter-spacing: 0.3px;
  text-align: center;
  color: #888;
  white-space: nowrap;
`;

export const Description = styled(Typography)`
  font-size: 0.8125rem;
  letter-spacing: 0.3px;
  text-align: center;
  color: #aaa;
  padding: 0 ${({theme}) => theme.spacing(1)};
`;

export const GridContainer = styled(Grid)``;

export const GridItem = styled(Grid)`
  padding: ${({theme}) => theme.spacing(1)};
`;

export const TitleCrumbBtn = styled(Button)`
  padding: 0;
`;
