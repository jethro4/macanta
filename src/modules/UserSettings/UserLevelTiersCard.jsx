import React from 'react';
import CheckIcon from '@mui/icons-material/Check';
import Typography from '@mui/material/Typography';
import useAppSettings from '@macanta/hooks/admin/useAppSettings';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import SettingCard from './SettingCard';
import SettingInfo from './SettingInfo';

const USER_TIER_LEVELS_FIXTURE = require('../../fixtures/json/user-tier-levels.json');

const UserLevelTiersCard = () => {
  const appSettingsQuery = useAppSettings({
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const macantaPlanId = appSettingsQuery.data?.macantaPlanId;
  const infoDetails = USER_TIER_LEVELS_FIXTURE[macantaPlanId];

  if (!infoDetails) {
    return null;
  }

  return (
    <>
      <SettingCard
        // loading={!data && loading}
        title={
          <>
            Tier Info
            <Typography
              component="span"
              style={{
                margin: '0 8px',
              }}>
              -
            </Typography>
            <Typography
              component="span"
              sx={{
                color: 'success.main',
              }}
              style={{
                fontWeight: 'bold',
              }}>
              {infoDetails.label}
            </Typography>
          </>
        }
        BodyComp={
          <>
            <SettingInfo
              IconComp={
                <CheckIcon
                  sx={{
                    color: 'success.main',
                  }}
                  style={{
                    fontSize: 16,
                    marginRight: 12,
                  }}
                />
              }
              value={infoDetails.info1}
            />
            <SettingInfo
              IconComp={
                <CheckIcon
                  sx={{
                    color: 'success.main',
                  }}
                  style={{
                    fontSize: 16,
                    marginRight: 12,
                  }}
                />
              }
              value={infoDetails.info2}
            />
            <SettingInfo
              IconComp={
                <CheckIcon
                  sx={{
                    color: 'success.main',
                  }}
                  style={{
                    fontSize: 16,
                    marginRight: 12,
                  }}
                />
              }
              value={infoDetails.info3}
            />
            <SettingInfo
              IconComp={
                <CheckIcon
                  sx={{
                    color: 'success.main',
                  }}
                  style={{
                    fontSize: 16,
                    marginRight: 12,
                  }}
                />
              }
              value={infoDetails.info4}
            />
          </>
        }
        // FooterComp={
        //   <PopoverButton
        //     style={{
        //       width: '75%',
        //       maxWidth: 160,
        //     }}
        //     variant="contained"
        //     startIcon={<VisibilityIcon />}
        //     size="medium"
        //     PopoverProps={{
        //       title: 'Tier Info',
        //       contentWidth: 380,
        //       // bodyStyle: {
        //       //   backgroundColor: '#f5f6fA',
        //       // },
        //     }}
        //     renderContent={() => null}>
        //     Show More
        //   </PopoverButton>
        // }
      />
    </>
  );
};

export default UserLevelTiersCard;
