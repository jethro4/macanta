import {experimentalStyled as styled} from '@mui/material/styles';

import RGL, {WidthProvider, Responsive} from 'react-grid-layout';
import Box from '@mui/material/Box';

const ReactGridLayout = WidthProvider(RGL);
const ResponsiveReactGridLayout = WidthProvider(Responsive);

const rootStyle = ({initialized, autoHeight, transparent}) => {
  return `
    ${
      !initialized
        ? `
          opacity: 0;
        `
        : ''
    }

    ${
      autoHeight
        ? `
          transition: none !important;
        `
        : ''
    }

    ${
      !transparent
        ? `
          background-color: #e4e4e4;
          border-radius: 4px;
        `
        : ''
    }
  `;
};

export const GridLayout = styled(ReactGridLayout)`
  ${(props) => rootStyle(props)}
`;

export const ResponsiveGridLayout = styled(ResponsiveReactGridLayout)`
  ${(props) => rootStyle(props)}
`;

export const GridItem = styled(Box)`
  cursor: grab;

  &.react-draggable-dragging,
  &.react-draggable-dragging .MuiButtonBase-root {
    cursor: grabbing;

    z-index: 1000;
  }

  ${({draggable}) =>
    draggable
      ? `
        & > * {
          pointer-events: none;
        }
      `
      : ''}
`;
