import {gql} from '@apollo/client';

export const VALID_DOMAIN_FRAGMENT = gql`
  fragment validDomainFragment on ValidDomainEmail {
    domain
    username
    valid
    subdomain
    legacy
    dns {
      data
      host
      type
      valid
    }
  }
`;

export const USER_ATTRIBUTES = gql`
  fragment userFragment on User {
    id
    email
    firstName
    lastName
    templateId
    contactLevel
  }
`;
