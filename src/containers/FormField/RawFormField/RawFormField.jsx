import React from 'react';
import Box from '@mui/material/Box';
import RawTextInput from './RawTextInput';
import RawTextArea from './RawTextarea';
import RawSelect from './RawSelect';
import RawDate from './RawDate';
import RawDateTime from './RawDateTime';
import RawRadio from './RawRadio';
import RawCheckbox from './RawCheckbox';
import RawFileUpload from './RawFileUpload';

const RawFormField = ({
  id,
  type,
  tooltip,
  required,
  label,
  options,
  ...props
}) => {
  let FormComponent;
  let fieldProps;

  switch (type) {
    case 'Select': {
      FormComponent = RawSelect;
      fieldProps = {
        options,
      };
      break;
    }
    case 'Date': {
      FormComponent = RawDate;

      break;
    }
    case 'DateTime': {
      FormComponent = RawDateTime;

      break;
    }
    case 'Radio': {
      FormComponent = RawRadio;
      fieldProps = {
        options,
      };
      break;
    }
    case 'Checkbox': {
      FormComponent = RawCheckbox;
      fieldProps = {
        options,
      };
      break;
    }
    case 'TextArea': {
      FormComponent = RawTextArea;
      break;
    }
    case 'file': {
      FormComponent = RawFileUpload;
      fieldProps = {
        filesLimit: props.filesLimit,
      };
      break;
    }
    default: {
      FormComponent = RawTextInput;
    }
  }

  const isBoxInput = !['TextArea', 'Checkbox', 'Radio', 'file'].includes(type);

  return (
    <Box className="field-set" {...props}>
      <label htmlFor={id} className="form-label">
        {label} {required && <span className="form-required">*</span>}
      </label>
      <FormComponent
        {...(isBoxInput && {
          className: 'form-control',
        })}
        id={id}
        name={id}
        required={required}
        {...fieldProps}
      />
      {!!tooltip || isBoxInput ? (
        <p className="form-tooltip">{tooltip}&nbsp;</p>
      ) : (
        <p
          style={{
            fontSize: '1px',
            lineHeight: '1px',
            marginTop: '6px',
          }}>
          &nbsp;
        </p>
      )}
    </Box>
  );
};

RawFormField.defaultProps = {
  labelPosition: 'top',
  type: 'Text',
};

export default RawFormField;
