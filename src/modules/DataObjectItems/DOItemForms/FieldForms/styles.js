import {experimentalStyled as styled} from '@mui/material/styles';
import Box from '@mui/material/Box';
import {IconButton} from '@macanta/components/Button';

export const Root = styled(Box)`
  height: 100%;
`;

export const SectionsBtnContainer = styled(Box)`
  padding: 0px ${({theme}) => theme.spacing(2)};
`;

export const BodyHeader = styled(Box)`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const ContactSpecificFieldsBtn = styled(IconButton)`
  color: #bbb;
  padding: 0;
  margin-bottom: 0.25rem;
  margin-left: 0.5rem;
`;
