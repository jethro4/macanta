import React, {useState} from 'react';
import Modal from '@macanta/components/Modal';
import useLocationChange from '@macanta/hooks/base/useLocationChange';
import ContactToContactRelationshipsForm from './ContactToContactRelationshipsForm';

const ContactRelationshipsQueryOption = ({
  modal,
  showModal: showModalProp = false,
  onClose,
  id,
  name,
  email,
}) => {
  const [showModal, setShowModal] = useState(showModalProp);

  const handleClose = () => {
    setShowModal(false);

    onClose();
  };

  let contactRelationshipsForms = (
    <ContactToContactRelationshipsForm
      id={id}
      primaryContactName={name}
      onSuccess={handleClose}
    />
  );

  useLocationChange(handleClose);

  if (modal) {
    contactRelationshipsForms = (
      <Modal
        headerTitle={`Contact Relationships (${name} - ${email})`}
        open={showModal}
        onClose={handleClose}
        contentWidth={1100}
        contentHeight={'94%'}>
        {contactRelationshipsForms}
      </Modal>
    );
  }

  return contactRelationshipsForms;
};

export default ContactRelationshipsQueryOption;
