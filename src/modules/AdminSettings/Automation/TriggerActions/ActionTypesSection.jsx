import React from 'react';
import {useFormikContext} from 'formik';
import Box from '@mui/material/Box';
import BodySection from '@macanta/modules/AdminSettings/UserManagement/BodySection';
import {useTheme} from '@mui/material/styles';
import {findItem, update} from '@macanta/utils/array';
import ActionTypesMultiSelectList from '@macanta/modules/AdminSettings/Automation/TriggerActions/ActionTypesMultiSelectList';

const ActionTypesSection = () => {
  const {values, setValues} = useFormikContext();

  const theme = useTheme();

  const handleChangeActionType = (type) => {
    const action = findItem(values.actions, {type});
    const hasAccess = !action.hasAccess;

    setValues((state) => ({
      ...state,
      actions: update({
        arr: state.actions,
        item: Object.assign({type, hasAccess}, !hasAccess && {value: ''}),
        key: 'type',
        includeAll: true,
      }),
    }));
  };

  const handleChange = (type, val) => {
    setValues((state) => ({
      ...state,
      actions: update({
        arr: state.actions,
        item: {type, value: val},
        key: 'type',
        includeAll: true,
      }),
    }));
  };

  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
      }}>
      <BodySection
        title="Actions To Trigger"
        style={{
          margin: 0,
          height: 'auto',
        }}
        bodyStyle={{
          backgroundColor: theme.palette.grayLight.main,
        }}>
        <ActionTypesMultiSelectList
          data={values.actions}
          onSelectActionType={handleChangeActionType}
          onChange={handleChange}
        />

        {/* {isEmpty && (
          <TableStyled.EmptyMessageContainer>
            <Typography
              style={{
                paddingBottom: '2rem',
              }}
              align="center"
              color="#888">
              No actions found
            </Typography>
          </TableStyled.EmptyMessageContainer>
        )} */}
      </BodySection>
    </Box>
  );
};

export default ActionTypesSection;
