import React, {useState} from 'react';
import Modal from '@macanta/components/Modal';
import WidgetForms from './WidgetForms';
import EditIcon from '@mui/icons-material/Edit';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import * as Styled from './styles';

const EditWidgetIconBtn = ({widget, onSuccess, ...props}) => {
  const [showModal, setShowModal] = useState(false);
  const {displayMessage} = useProgressAlert();

  const handleStopMouseEvent = (event) => {
    event.stopPropagation();
  };

  const handleShowModal = (event) => {
    handleStopMouseEvent(event);
    setShowModal(true);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  const handleSuccess = (newWidget) => {
    displayMessage('Successfully added widget');

    onSuccess(newWidget);
    handleCloseModal();
  };

  return (
    <>
      <Styled.ActionIconButton
        onClick={handleShowModal}
        onMouseUp={handleStopMouseEvent}
        onMouseDown={handleStopMouseEvent}
        {...props}>
        <EditIcon
          style={{
            fontSize: '1rem',
            color: 'white',
          }}
        />
      </Styled.ActionIconButton>
      <Modal
        onMouseUp={handleStopMouseEvent}
        onMouseDown={handleStopMouseEvent}
        headerTitle={`Edit Widget`}
        open={showModal}
        onClose={handleCloseModal}>
        <WidgetForms isEdit widget={widget} onSuccess={handleSuccess} />
      </Modal>
    </>
  );
};

export default EditWidgetIconBtn;
