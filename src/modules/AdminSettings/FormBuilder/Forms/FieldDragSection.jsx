import React from 'react';
import AddIcon from '@mui/icons-material/Add';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import {PopoverButton} from '@macanta/components/Button';
import {GridLayout} from '@macanta/components/Layout';
import FormUI from './FormUI';
import ActionAreaContainer from './ActionAreaContainer';

const FieldDragSection = ({
  isEdit,
  title,
  groups,
  renderActionButtons,
  renderGroupForm,
  renderFieldsForm,
  onSort,
  onDelete,
  groupError,
  groupFieldError,
}) => {
  const renderGridItem = ({item}) => {
    return (
      <FormUI
        isEdit={isEdit}
        item={item}
        hideDelete={['field_email', 'field_integration'].includes(item.fieldId)}
        onDelete={onDelete}
        renderActionButtons={renderActionButtons}
      />
    );
  };

  const handleSort = (groupId) => (sortData) => {
    onSort(groupId, sortData);
  };

  return (
    <Box>
      <Box
        style={{
          position: 'relative',
          marginBottom: '1rem',
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'flex-end',
        }}>
        <Typography
          className="object-name"
          style={{
            margin: 0,
          }}>
          {title}
        </Typography>

        {isEdit && (
          <Box
            style={{
              display: 'flex',
              alignItems: 'center',
            }}>
            {!!groupError && (
              <Typography
                color="error"
                style={{
                  fontSize: '0.6875rem',
                  lineHeight: '0.6875rem',
                  marginRight: '1rem',
                }}>
                {groupError}
              </Typography>
            )}
            <PopoverButton
              size="small"
              variant="contained"
              startIcon={<AddIcon />}
              PopoverProps={{
                title: 'Add Group',
                contentWidth: 380,
                // bodyStyle: {
                //   backgroundColor: '#f5f6fA',
                // },
              }}
              renderContent={renderGroupForm}>
              Add Group
            </PopoverButton>
          </Box>
        )}
      </Box>

      <Box
        sx={{
          '& > *:not(:last-child)': {
            marginBottom: '1rem',
          },
        }}>
        {groups.map((group, index) => {
          const handleDelete = () => {
            onDelete('group', group.subGroupName);
          };

          return (
            <ActionAreaContainer
              key={group.id}
              sx={{
                ...(!isEdit
                  ? {
                      '.react-grid-item': {
                        cursor: 'default',
                      },
                    }
                  : {
                      paddingTop: '40px !important',
                    }),
              }}
              buttonsContainerStyle={{
                top: -2,
                right: 14,
              }}
              editTitle="Edit Group"
              hideActionButtons={!isEdit}
              component="fieldset"
              className="fields-group"
              renderActionButtons={() => (
                <Box
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                  }}>
                  {groupFieldError && !!groupFieldError.groupsMap[group.id] && (
                    <Typography
                      color="error"
                      style={{
                        fontSize: '0.6875rem',
                        lineHeight: '0.6875rem',
                        marginRight: '1rem',
                      }}>
                      {groupFieldError.text}
                    </Typography>
                  )}
                  <PopoverButton
                    style={{
                      marginRight: '1rem',
                      fontWeight: 'bold',
                    }}
                    size="small"
                    color="info"
                    variant="text"
                    startIcon={<AddIcon />}
                    PopoverProps={{
                      title: `Add Fields (${group.subGroupName})`,
                      contentWidth: 380,
                      anchorOrigin: {
                        vertical: 'top',
                        horizontal: 'center',
                      },
                      transformOrigin: {
                        vertical: 'bottom',
                        horizontal: 'center',
                      },
                    }}
                    renderContent={(handleClose, actions) =>
                      renderFieldsForm(
                        handleClose,
                        group.subGroupName,
                        group.columns,
                        actions,
                      )
                    }>
                    Add Fields
                  </PopoverButton>
                </Box>
              )}
              renderForm={(handleClose) => renderGroupForm(handleClose, group)}
              onDelete={handleDelete}>
              <legend className="fields-group-title">
                {group.subGroupName}
              </legend>

              <GridLayout
                autoResizeItem
                useCSSTransforms={false}
                isDraggable={isEdit}
                key={`${index}-${isEdit}-${group.columns}`}
                transparent
                groupBy="column"
                maxColumn={group.columns}
                margin={[24, 4]}
                containerPadding={[10, 10]}
                data={group.fields}
                renderGridItem={renderGridItem}
                rowHeight={50}
                onSort={handleSort(group.id)}
              />
            </ActionAreaContainer>
          );
        })}
      </Box>
    </Box>
  );
};

export default FieldDragSection;
