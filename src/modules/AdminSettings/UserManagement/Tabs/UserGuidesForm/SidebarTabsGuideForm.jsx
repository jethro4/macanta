import React from 'react';
import PermissionsTable from '@macanta/modules/AdminSettings/UserManagement/Tabs/PermissionsTable';
import useSidebarTabsUserGuides from '@macanta/hooks/app/useSidebarTabsUserGuides';
import UserGuidesColumns from './UserGuidesColumns';
import {getKeyExtractor} from './helpers';

const SidebarTabsGuideForm = ({permission, onChange}) => {
  const {initLoading, data} = useSidebarTabsUserGuides({permission});

  return (
    <PermissionsTable
      loading={initLoading}
      keyExtractor={getKeyExtractor}
      columns={UserGuidesColumns}
      data={data}
      handlers={{
        onChange,
      }}
    />
  );
};

export default SidebarTabsGuideForm;
