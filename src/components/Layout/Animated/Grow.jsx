import React from 'react';
import MUIGrow from '@mui/material/Grow';
import Container from './Container';

const Grow = ({children, ...props}) => {
  return (
    <MUIGrow {...props}>
      <Container>{children}</Container>
    </MUIGrow>
  );
};

export default Grow;
