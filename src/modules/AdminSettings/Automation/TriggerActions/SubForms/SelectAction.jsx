import React from 'react';
import FormField from '@macanta/containers/FormField';

const SelectAction = ({...props}) => {
  return (
    <FormField
      style={{
        marginBottom: 0,
      }}
      type="Select"
      placeholder="Select Action..."
      fullWidth
      size="xsmall"
      variant="outlined"
      {...props}
    />
  );
};

export default SelectAction;
