import useDOItems from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOItems';
import isFunction from 'lodash/isFunction';

const DataObjectsQuery = ({
  search,
  filters,
  groupId,
  contactId,
  onCompleted,
  onError,
  children,
  page,
  limit,
  order,
  orderBy,
  contactField,
}) => {
  const doItemsQuery = useDOItems(
    {
      q: search,
      filters,
      groupId,
      contactId,
      page,
      limit,
      order,
      orderBy,
      contactField,
    },
    {
      onCompleted,
      onError,
    },
  );

  return isFunction(children) ? children(doItemsQuery) : null;
};

export default DataObjectsQuery;
