import React, {useState, useMemo} from 'react';
import {useFormikContext} from 'formik';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import KeyboardArrowLeftIcon from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@mui/icons-material/KeyboardArrowRight';
import SummarizeIcon from '@mui/icons-material/Summarize';
import Stepper from '@macanta/components/Stepper';
import Grid from '@mui/material/Grid';
import Form from '@macanta/components/Form';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import FormField from '@macanta/containers/FormField';
import Button from '@macanta/components/Button';
import Show from '@macanta/containers/Show';
import {SubSection} from '@macanta/containers/Section';
import ConfirmationDialog from '@macanta/containers/ConfirmationDialog';
import DetailInfo from '@macanta/modules/ContactDetails/DetailInfo';
import {DEFAULT_SELECTED_TAB} from '@macanta/modules/pages/AppContainer/ContactRecords/SidebarAccess/SidebarAccess';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import useDORelationships from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDORelationships';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import {MERGE_CONTACT_DUPLICATES} from '@macanta/graphql/contacts';
import {useMutation} from '@apollo/client';
import * as FormStyled from '@macanta/modules/NoteTaskForms/styles';
import {navigate} from 'gatsby';

const DEFAULT_DEDUPLICATION_STEPS = [
  {label: 'Select Primary Contact'},
  {label: 'Data Objects Transfer'},
  {label: 'Notes/Tasks Transfer'},
];

const DEFAULT_DO_ACTIONS = [
  {label: 'No Action', value: 'noAction'},
  {
    label: 'Transfer with same relationship',
    value: 'transferWithExistingRelationship',
  },
  {
    label: 'Transfer with different relationship',
    value: 'transferWithNewRelationship',
  },
];

const DEFAULT_NOTE_TASK_ACTIONS = [
  {label: 'No Action', value: 'noAction'},
  {
    label: 'Transfer all',
    value: 'transferAll',
  },
];

const ColumnDividerContainer = ({width}) => (
  <Box
    style={{
      width: width || '1rem',
    }}
  />
);

const DOSubSection = ({id: groupId, title, action, relationship}) => {
  const {setValues} = useFormikContext();

  const relationshipsQuery = useDORelationships(groupId, {
    skip: action !== 'transferWithNewRelationship',
  });

  const relationships = relationshipsQuery.data?.listRelationships;

  const handleActionRenderValue = (val) => {
    const selected = DEFAULT_DO_ACTIONS.find(({value}) => value === val);

    return selected?.label;
  };

  const handleChangeAction = (val) => {
    const selected = DEFAULT_DO_ACTIONS.find(({value}) => value === val);

    setValues((formValues) => ({
      ...formValues,
      dataObjectActions: {
        ...formValues.dataObjectActions,
        [groupId]: {
          ...formValues.dataObjectActions[groupId],
          label: selected.label,
          action: selected.value,
        },
      },
    }));
  };

  const handleChangeRelationship = (val) => {
    setValues((formValues) => ({
      ...formValues,
      dataObjectActions: {
        ...formValues.dataObjectActions,
        [groupId]: {
          ...formValues.dataObjectActions[groupId],
          relationship: val,
        },
      },
    }));
  };

  const allRelationships = useMemo(() => {
    if (relationships) {
      return relationships.map((r) => ({
        label: r.role,
        value: r.role,
      }));
    }

    return [];
  }, [relationships]);

  return (
    <SubSection
      title={title}
      headerStyle={{
        justifyContent: 'flex-start',
        minHeight: '2.55rem',
      }}>
      <Box
        style={{
          display: 'flex',
          alignItems: 'center',
        }}>
        <FormField
          style={{
            flex: 1,
            marginBottom: 0,
          }}
          type="Select"
          options={DEFAULT_DO_ACTIONS}
          value={action}
          onChange={handleChangeAction}
          fullWidth
          size="small"
          variant="outlined"
          renderValue={handleActionRenderValue}
        />
        <ColumnDividerContainer />
        <Box
          style={{
            flex: 1,
          }}>
          {action === 'transferWithNewRelationship' && (
            <FormField
              style={{
                marginBottom: 0,
              }}
              type="Select"
              options={allRelationships}
              value={relationship}
              onChange={handleChangeRelationship}
              placeholder="Select Relationship..."
              fullWidth
              size="small"
              variant="outlined"
            />
          )}
        </Box>
      </Box>
    </SubSection>
  );
};

const NoteTaskSubSection = ({action}) => {
  const {setValues} = useFormikContext();

  const handleActionRenderValue = (val) => {
    const selected = DEFAULT_NOTE_TASK_ACTIONS.find(({value}) => value === val);

    return selected?.label;
  };

  const handleChangeAction = (val) => {
    const selected = DEFAULT_NOTE_TASK_ACTIONS.find(({value}) => value === val);

    setValues((formValues) => ({
      ...formValues,
      noteTaskAction: {
        ...formValues.noteTaskAction,
        label: selected.label,
        action: selected.value,
      },
    }));
  };

  return (
    <SubSection title="Notes & Tasks">
      <Box
        style={{
          display: 'flex',
          alignItems: 'center',
        }}>
        <FormField
          style={{
            flex: 1,
            marginBottom: 0,
          }}
          type="Select"
          options={DEFAULT_NOTE_TASK_ACTIONS}
          value={action}
          onChange={handleChangeAction}
          fullWidth
          size="small"
          variant="outlined"
          renderValue={handleActionRenderValue}
        />
        <ColumnDividerContainer />
        <Box
          style={{
            flex: 1,
          }}></Box>
      </Box>
    </SubSection>
  );
};

const DeduplicateMergeFormsContainer = ({contacts, ...props}) => {
  const {displayMessage} = useProgressAlert();
  /* const navigate = useNavigate();*/

  const doTypes = useDOTypes({
    removeEmail: true,
  });

  const types = doTypes?.data?.listDataObjectTypes;
  const initialLoading = !types && doTypes.loading;

  const [
    callMergeContactDuplicatesMutation,
    mergeContactDuplicatesMutation,
  ] = useMutation(MERGE_CONTACT_DUPLICATES, {
    onCompleted(data) {
      if (data.mergeContactDuplicates?.success) {
        displayMessage('Merge contacts successfully');

        navigate(
          `/app/contact/${data.mergeContactDuplicates.primaryContactId}/${DEFAULT_SELECTED_TAB}`,
          {
            replace: true,
          },
        );
      }
    },
    onError() {},
  });

  const getInitValues = () => {
    const dataObjectActions =
      types?.reduce((acc, type) => {
        acc[type.id] = {
          id: type.id,
          title: type.title,
          label: DEFAULT_DO_ACTIONS[0].label,
          action: DEFAULT_DO_ACTIONS[0].value,
        };

        return acc;
      }, {}) || {};

    return {
      primaryContact: {},
      duplicateContacts: [],
      dataObjectActions,
      noteTaskAction: {
        label: DEFAULT_NOTE_TASK_ACTIONS[0].label,
        action: DEFAULT_NOTE_TASK_ACTIONS[0].value,
      },
    };
  };

  const handleFormSubmit = async (values) => {
    callMergeContactDuplicatesMutation({
      variables: {
        mergeContactDuplicatesInput: {
          primaryContactId: values.primaryContact.id,
          duplicateContactIds: values.duplicateContacts.map((item) => item.id),
          dataObjectActions: Object.values(values.dataObjectActions).map(
            (item) => ({
              title: item.title,
              action: item.action,
              relationship: item.relationship,
            }),
          ),
          noteTaskAction: values.noteTaskAction.action,
        },
        __mutationkey: 'mergeContactDuplicatesInput',
      },
    });
  };

  return (
    <>
      {!initialLoading && (
        <Form initialValues={getInitValues()} onSubmit={handleFormSubmit}>
          <DeduplicateMergeForms contacts={contacts} {...props} />
        </Form>
      )}

      <LoadingIndicator fill loading={initialLoading} />
      <LoadingIndicator
        modal
        loading={mergeContactDuplicatesMutation.loading}
      />
    </>
  );
};

const DeduplicateMergeForms = ({contacts, renderTable}) => {
  const {values, setFieldValue, handleSubmit} = useFormikContext();

  const [step, setStep] = useState(0);
  const [openSummary, setOpenSummary] = useState(false);
  // const {displayMessage} = useProgressAlert();

  const handleSelectPrimary = (contact) => {
    setFieldValue('primaryContact', contact);
    setFieldValue(
      'duplicateContacts',
      contacts.filter((dupContact) => dupContact.id !== contact.id),
    );
  };

  const handleBack = () => {
    setStep(step - 1);
  };

  const handleProceed = () => {
    setStep(step + 1);
  };

  const handleSummary = () => {
    setOpenSummary(true);
  };

  const handleCloseSummary = () => {
    setOpenSummary(false);
  };

  const dataObjectActions = useMemo(() => {
    return values.dataObjectActions
      ? Object.values(values.dataObjectActions)
      : [];
  }, [values.dataObjectActions]);

  return (
    <>
      <Box
        style={{
          flex: 1,
          backgroundColor: '#f5f6fa',
        }}>
        <Show in={step === 0}>
          {renderTable({
            onSelectPrimary: handleSelectPrimary,
            selectedPrimaryContact: values.primaryContact,
          })}
        </Show>
        <Show
          in={step === 1}
          style={{
            position: 'relative',
          }}>
          <Box
            style={{
              overflowY: 'auto',
              paddingBottom: '1rem',
              position: 'absolute',
              top: 0,
              right: 0,
              bottom: 0,
              left: 0,
            }}>
            <Grid container>
              {dataObjectActions.map((doAction) => {
                return (
                  <Grid key={doAction.id} item xs={12} sm={6}>
                    <DOSubSection
                      id={doAction.id}
                      title={doAction.title}
                      action={doAction.action}
                      relationship={doAction.relationship}
                    />
                  </Grid>
                );
              })}
            </Grid>
          </Box>
        </Show>
        <Show in={step === 2}>
          <Grid container>
            <Grid item xs={12} sm={6}>
              <NoteTaskSubSection action={values.noteTaskAction.action} />
            </Grid>
          </Grid>
        </Show>
      </Box>
      <FormStyled.Footer
        style={{
          margin: 0,
          padding: '1rem',
          justifyContent: 'space-between',
          borderTop: '1px solid #eee',
          boxShadow: '0px -4px 10px -2px #eee',
        }}>
        <Box
          style={{
            width: 150,
          }}>
          {step > 0 && (
            <Button
              variant="outlined"
              startIcon={<KeyboardArrowLeftIcon />}
              onClick={handleBack}
              size="medium">
              Back
            </Button>
          )}
        </Box>
        <Box
          style={{
            flex: 1,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Stepper
            style={{
              minWidth: 600,
            }}
            activeStep={step}
            steps={DEFAULT_DEDUPLICATION_STEPS}
          />
        </Box>
        <Box
          style={{
            width: 150,
            display: 'flex',
            justifyContent: 'flex-end',
          }}>
          {step === 2 ? (
            <Button
              variant="contained"
              startIcon={<SummarizeIcon />}
              onClick={handleSummary}
              size="medium">
              Summary
            </Button>
          ) : (
            <Button
              disabled={!values.primaryContact?.id}
              variant="contained"
              endIcon={<KeyboardArrowRightIcon />}
              onClick={handleProceed}
              size="medium">
              Proceed
            </Button>
          )}
        </Box>
      </FormStyled.Footer>

      <ConfirmationDialog
        sx={{
          '.MuiDialog-paper': {
            width: 600,
            minHeight: 550,
            maxHeight: 700,
          },
        }}
        open={openSummary}
        onClose={handleCloseSummary}
        title="Summary"
        BodyComp={
          <>
            <DetailInfo
              style={{
                marginBottom: '1.5rem',
              }}
              vertical
              disableOneLine
              label="Primary Contact"
              value={values.primaryContact.email}
            />
            <DetailInfo
              style={{
                marginBottom: '1.5rem',
              }}
              vertical
              disableOneLine
              label="Duplicate Contacts">
              <Grid container>
                {values.duplicateContacts.map((contact) => {
                  return (
                    <Grid key={contact.id} item xs={12} sm={6}>
                      <Typography>{contact.email}</Typography>
                    </Grid>
                  );
                })}
              </Grid>
            </DetailInfo>
            <DetailInfo
              style={{
                marginBottom: '1.5rem',
              }}
              vertical
              disableOneLine
              label="Data Objects Actions">
              {dataObjectActions.map((action) => {
                return (
                  <Box
                    key={action.id}
                    style={{
                      display: 'flex',
                    }}>
                    <Typography
                      style={{
                        marginRight: 4,
                        whiteSpace: 'nowrap',
                      }}>
                      {action.title}:
                    </Typography>
                    <Typography
                      style={{
                        color: '#888',
                      }}>
                      {`${action.label}${
                        action.relationship ? ` (${action.relationship})` : ''
                      }`}
                    </Typography>
                  </Box>
                );
              })}
            </DetailInfo>
            <DetailInfo
              style={{
                marginBottom: '1.5rem',
              }}
              vertical
              disableOneLine
              label="Notes & Tasks Action">
              {
                <Typography
                  style={{
                    color: '#888',
                  }}>
                  {values.noteTaskAction.label}
                </Typography>
              }
            </DetailInfo>
          </>
        }
        actions={[
          {
            label: 'Run De-Duplication',
            onClick: handleSubmit,
          },
        ]}
      />
    </>
  );
};

export default DeduplicateMergeFormsContainer;
