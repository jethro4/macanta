import useQuery from '@macanta/hooks/apollo/useQuery';
import {GET_APP_SUBSCRIPTION} from '@macanta/graphql/app';

const useAppSubscription = (options = {}) => {
  const appSubscriptionQuery = useQuery(GET_APP_SUBSCRIPTION, options);

  const appSubscription = appSubscriptionQuery.data?.getAppSubscription;

  return {
    ...appSubscriptionQuery,
    planId: appSubscription?.planId,
    options: appSubscription?.options,
    isActive: appSubscription?.status === 'active',
  };
};

export default useAppSubscription;
