import {gql} from '@apollo/client';
import {TASK_SCHEDULE_USER_FRAGMENT} from '@macanta/graphql/scheduler/fragments';

export default gql`
  query listTaskSchedulesUser(
    $contactId: ID
    $dateStart: String
    $dateEnd: String
  ) {
    listTaskSchedulesUser(
      contactId: $contactId
      dateStart: $dateStart
      dateEnd: $dateEnd
    ) {
      total
      items {
        date
        items {
          ...taskScheduleUserFragment
        }
      }
    }
  }
  ${TASK_SCHEDULE_USER_FRAGMENT}
`;
