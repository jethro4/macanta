export const NOTE_TAG = Object.freeze({
  DEFAULT: 'general',
  TYPES: ['general', 'call note', 'alert', 'update', 'comment'],
});

export const TASK_TAG = Object.freeze({
  DEFAULT: 'task',
  TYPES: ['task'],
});
