import {gql} from '@apollo/client';

export default gql`
  query listDataObjectItemHistory(
    $appName: String!
    $apiKey: String!
    $type: String
    $doItemId: ID!
    $contactId: ID
    $page: Int
    $limit: Int
  ) {
    listDataObjectItemHistory(
      appName: $appName
      apiKey: $apiKey
      type: $type
      doItemId: $doItemId
      contactId: $contactId
      page: $page
      limit: $limit
    ) {
      fields {
        fieldKey
        label
        isSharedField
      }
      items {
        data {
          fieldKey
          value
          sharedFieldValue
        }
      }
    }
  }
`;
