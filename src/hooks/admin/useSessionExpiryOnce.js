import {useState, useEffect} from 'react';
import {useReactiveVar} from '@apollo/client';
import {sessionExpiredVar} from '@macanta/graphql/cache/vars';
import {triggerCheck} from './useSessionExpiry';

const useSessionExpiryOnce = () => {
  const [loading, setLoading] = useState(true);
  const sessionExpired = useReactiveVar(sessionExpiredVar);

  const handleCheckSession = async () => {
    try {
      await triggerCheck();
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    handleCheckSession();
  }, []);

  return {loading, expired: sessionExpired};
};

export default useSessionExpiryOnce;
