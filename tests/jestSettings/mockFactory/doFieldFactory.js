import {FIELDS_WITH_CHOICES} from '@macanta/constants/form';
import {nextFactoryId} from './factory';

const typename = 'Field';

export default (overwrites) => {
  const {id, currentIndex} = nextFactoryId(typename);

  const hasChoices = FIELDS_WITH_CHOICES.includes(overwrites?.type);

  return Object.assign(
    {
      __typename: typename,
      id,
      name: `DO Field ${currentIndex}`,
      type: 'Text',
      helpText: '',
      placeholder: '',
      default: '',
      required: false,
      subGroup: 'Inputs',
      sectionTag: 'Single Value',
      showInTable: true,
      showOrder: currentIndex,
      contactSpecificField: false,
      permission: 'READ_WRITE',
      headingText: '',
      addDivider: false,
      dataReference: null,
      fieldApplicableTo: null,
    },
    overwrites?.type === 'Checkbox' && {
      sectionTag: 'Multi Value',
    },
    hasChoices && {
      subGroup: 'With Choices',
      choices: ['Choice 1', 'Choice 2', 'Choice 3'],
    },
    overwrites?.type === 'FileUpload' && {
      fileUpload: {
        multiple: null,
        mimeTypes: null,
        __typename: 'FieldFileUpload',
      },
    },
    overwrites,
  );
};
