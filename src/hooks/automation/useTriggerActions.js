import useQuery from '@macanta/hooks/apollo/useQuery';
import {GET_TRIGGER_ACTIONS} from '@macanta/graphql/automation';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {findItem} from '@macanta/utils/array';
import {getActions} from '@macanta/modules/AdminSettings/Automation/TriggerActions/helpers';

const FIELD_KEY_MAPPING = {
  queryId: 'id',
  queryName: 'name',
  queryDescription: 'description',
  queryConnectedDataType: 'doTitle',

  queryCDField: 'doConditions',
  queryCDFieldName: 'name',
  queryCDFieldValue: 'value',
  queryCDFieldValues: 'values',
  // queryCDFieldLogic: 'logic',
  // queryCDFieldOperator: 'operator',

  queryContact: 'contactConditions',
  queryContactRelationship: 'relationship',
  queryContactRelationshipFieldLogic: 'logic',

  queryConnectedDataEmail: 'emailActionId',
  queryConnectedDataSMS: 'smsActionId',
  queryConnectedDataFieldAction: 'fieldActionId',
  queryConnectedDataContactAction: 'contactActionId',
  queryConnectedDataUserAction: 'userActionId',
  queryConnectedDataHTTPPost: 'webhookActionId',
};

const useTriggerActions = (options) => {
  const doTypesQuery = useDOTypes({
    removeEmail: true,
    includeContactObject: true,
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });
  const query = useQuery(GET_TRIGGER_ACTIONS, {
    keyMapping: FIELD_KEY_MAPPING,
    transformResult: (result) =>
      result?.items.map((item) => {
        const doType = findItem(doTypesQuery.result, {
          title: item.doTitle,
        });
        const actions = getActions(item);
        const itemWithActions = {
          ...item,
          groupId: doType?.id,
          actions,
        };

        return itemWithActions;
      }),
    dependencies: doTypesQuery.result,
    ...options,
  });

  return query;
};

export default useTriggerActions;
