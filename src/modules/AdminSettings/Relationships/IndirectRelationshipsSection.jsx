import React, {useMemo} from 'react';
import TurnedInIcon from '@mui/icons-material/TurnedIn';
import Button from '@macanta/components/Button';
import Switch from '@macanta/components/Forms/ChoiceFields/Switch';
import Form from '@macanta/components/Form';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import Section from '@macanta/containers/Section';
import DataTable from '@macanta/containers/DataTable';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import useAppSettings from '@macanta/hooks/admin/useAppSettings';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {useMutation} from '@apollo/client';
import {GET_APP_SETTINGS, UPDATE_APP_SETTINGS} from '@macanta/graphql/app';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import * as FormStyled from '@macanta/modules/NoteTaskForms/styles';

const INDIRECT_RELATIONSHIPS_COLUMNS = [
  {
    id: 'title',
    label: 'Type',
    maxWidth: '100%',
  },
];

const IndirectRelationshipsSection = () => {
  const {displayMessage} = useProgressAlert();

  const appSettingsQuery = useAppSettings({
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const doTypesQuery = useDOTypes({
    removeEmail: true,
  });

  const [callUpdateAppSettings, updateAppSettingsMutation] = useMutation(
    UPDATE_APP_SETTINGS,
    {
      onError() {},
      onCompleted: async (data) => {
        if (data?.updateAppSettings?.success) {
          displayMessage('Saved successfully!');
        }
      },
    },
  );

  const indirectRelationshipGroups =
    appSettingsQuery.data?.indirectRelationshipGroups;
  const doTypes = doTypesQuery.data?.listDataObjectTypes;

  const handleFormSubmit = async (values, actions) => {
    const {data} = await callUpdateAppSettings({
      variables: {
        updateAppSettingsInput: {
          type: 'IndirectRelationshipGroups',
          value: values.indirectRelationshipGroups.join(','),
        },
        __mutationkey: 'updateAppSettingsInput',
      },
    });

    if (data?.updateAppSettings?.success) {
      appSettingsQuery.client.writeQuery({
        query: GET_APP_SETTINGS,
        data: {
          getAppSettings: {
            __typename: 'AppSettings',
            ...appSettingsQuery.data,
            indirectRelationshipGroups: values.indirectRelationshipGroups,
          },
        },
      });

      actions.resetForm({
        values,
      });
    }
  };

  const initValues = useMemo(() => {
    return {
      indirectRelationshipGroups: indirectRelationshipGroups || [],
    };
  }, [indirectRelationshipGroups]);

  return (
    <>
      <Form
        enableReinitialize
        initialValues={initValues}
        onSubmit={handleFormSubmit}>
        {({values, setFieldValue, handleSubmit, dirty}) => {
          return (
            <Section
              loading={doTypesQuery.loading}
              fullHeight
              headerStyle={{
                backgroundColor: 'white',
              }}
              style={{
                marginTop: 0,
              }}
              title="Indirect Relationships"
              FooterComp={
                <FormStyled.Footer
                  style={{
                    padding: '1rem',
                    margin: 0,
                    width: '100%',
                  }}>
                  <Button
                    disabled={!dirty}
                    variant="contained"
                    startIcon={<TurnedInIcon />}
                    onClick={handleSubmit}
                    size="medium">
                    Save Changes
                  </Button>
                </FormStyled.Footer>
              }>
              <DataTable
                noOddRowStripes
                full
                noLimit
                hidePagination
                fullHeight
                columns={INDIRECT_RELATIONSHIPS_COLUMNS}
                data={doTypes}
                order="desc"
                orderBy="created"
                rowsPerPage={25}
                rowsPerPageOptions={[25, 50, 100]}
                renderActionButtons={({item}) => (
                  <Switch
                    color="success"
                    checked={values.indirectRelationshipGroups.includes(
                      item.id,
                    )}
                    onChange={(event) => {
                      const isEnabled = event.target.checked;
                      let newArr = values.indirectRelationshipGroups;

                      if (
                        isEnabled &&
                        !values.indirectRelationshipGroups.includes(item.id)
                      ) {
                        newArr = values.indirectRelationshipGroups.concat(
                          item.id,
                        );
                      } else if (
                        !isEnabled &&
                        values.indirectRelationshipGroups.includes(item.id)
                      ) {
                        newArr = values.indirectRelationshipGroups.filter(
                          (doTypeId) => doTypeId !== item.id,
                        );
                      }

                      setFieldValue('indirectRelationshipGroups', newArr);
                    }}
                  />
                )}
                actionColumn={{
                  label: 'Include?',
                  show: true,
                  style: {
                    minWidth: 100,
                  },
                }}
              />
            </Section>
          );
        }}
      </Form>
      <LoadingIndicator modal loading={updateAppSettingsMutation.loading} />
    </>
  );
};

export default IndirectRelationshipsSection;
