import React from 'react';
import Form from '@macanta/components/Form';
import Typography from '@mui/material/Typography';
import TurnedInIcon from '@mui/icons-material/TurnedIn';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import useMutation from '@macanta/hooks/apollo/useMutation';
import {CREATE_OR_UPDATE_CONTACT_RELATIONSHIP} from '@macanta/graphql/contacts';
import * as NoteTaskFormsStyled from '@macanta/modules/NoteTaskForms/styles';
import {writeItems} from '@macanta/graphql/helpers';
import {CONTACT_RELATIONSHIP_DIRECT_FRAGMENT} from '@macanta/graphql/contacts/fragments';

const DirectRelationshipOptionForms = ({initValues, onSuccess}) => {
  const [
    callCreateOrUpdateContactRelationship,
    createOrUpdateContactRelationshipMutation,
  ] = useMutation(CREATE_OR_UPDATE_CONTACT_RELATIONSHIP, {
    successMessage: 'Successfully saved option',
    update(cache, {data: {createOrUpdateContactRelationship}}, options) {
      writeItems({
        isCreate: !options.variables.input?.id,
        item: createOrUpdateContactRelationship,
        typeName: 'ContactRelationshipDirect',
        fragment: CONTACT_RELATIONSHIP_DIRECT_FRAGMENT,
        listKey: 'listDirectContactRelationships',
        position: 'last',
      });
    },
    onError() {},
    onCompleted() {
      onSuccess?.();
    },
  });

  const handleFormSubmit = (values) => {
    callCreateOrUpdateContactRelationship({
      variables: {
        ...values,
      },
    });
  };

  return (
    <NoteTaskFormsStyled.Container
      style={{
        display: 'flex',
        flexDirection: 'column',
        padding: 0,
      }}>
      <Form
        initialValues={initValues}
        // validationSchema={validationSchema}
        onSubmit={handleFormSubmit}>
        {({values, errors, handleChange, handleSubmit}) => (
          <>
            <NoteTaskFormsStyled.FormGroup
              style={{
                margin: 0,
                padding: '1rem',
              }}>
              <NoteTaskFormsStyled.TextField
                labelPosition="normal"
                required
                error={errors.name}
                onChange={handleChange('name')}
                label="Name"
                variant="outlined"
                value={values.name}
                fullWidth
                size="medium"
              />

              <NoteTaskFormsStyled.TextField
                labelPosition="normal"
                error={errors.description}
                onChange={handleChange('description')}
                label="Description"
                variant="outlined"
                value={values.description}
                fullWidth
                size="medium"
                type="TextArea"
                hideExpand
              />
            </NoteTaskFormsStyled.FormGroup>

            {!!createOrUpdateContactRelationshipMutation.error && (
              <Typography color="error" variant="subtitle2" align="center">
                {createOrUpdateContactRelationshipMutation.error.message}
              </Typography>
            )}

            <NoteTaskFormsStyled.Footer
              style={{
                padding: '1rem',
              }}>
              <NoteTaskFormsStyled.FooterButton
                disabled={!values.name}
                variant="contained"
                startIcon={<TurnedInIcon />}
                onClick={handleSubmit}
                size="medium">
                {'Save'}
              </NoteTaskFormsStyled.FooterButton>
            </NoteTaskFormsStyled.Footer>
          </>
        )}
      </Form>
      <LoadingIndicator
        modal
        loading={createOrUpdateContactRelationshipMutation.loading}
      />
    </NoteTaskFormsStyled.Container>
  );
};

export default DirectRelationshipOptionForms;
