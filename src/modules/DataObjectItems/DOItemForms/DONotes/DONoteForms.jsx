import React from 'react';
import noteValidationSchema from '@macanta/validations/note';
import NoteForms from '@macanta/modules/NoteTaskForms/NoteForms';

const DONoteForms = ({
  id,
  title = '',
  note = '',
  tags,
  onSuccess,
  tempId,
  ...props
}) => {
  const initValues = {
    id,
    title,
    note,
    tags,
    tempId,
  };

  return (
    <NoteForms
      initValues={initValues}
      validationSchema={noteValidationSchema}
      onSubmit={onSuccess}
      saveBtnText={id || tempId ? 'Edit' : 'Add'}
      {...props}
    />
  );
};

DONoteForms.defaultProps = {
  tags: [],
};

export default DONoteForms;
