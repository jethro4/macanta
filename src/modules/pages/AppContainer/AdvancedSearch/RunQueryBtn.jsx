import React, {useState, useMemo} from 'react';
import {useFormikContext} from 'formik';
import SearchIcon from '@mui/icons-material/Search';
import Typography from '@mui/material/Typography';
import Button from '@macanta/components/Button';
import Modal from '@macanta/components/Modal';
import WidgetData from '@macanta/modules/pages/AppContainer/Widgets/WidgetData';
import useAdvancedSearch from '@macanta/hooks/admin/useAdvancedSearch';
import AdvancedSearchSaveBtn from './AdvancedSearchSaveBtn';
import * as Storage from '@macanta/utils/storage';

const RunQueryBtn = ({label, headerTitle, ...props}) => {
  const userDetails = Storage.getItem('userDetails');

  const {values, dirty, validateForm} = useFormikContext();

  const [showModal, setShowModal] = useState(false);

  const advancedSearchQuery = useAdvancedSearch(values, {skip: !showModal});

  const validAdvancedSearch = (errors) => {
    return !Object.keys(errors).filter((key) => key !== 'name').length;
  };

  const handleShowModal = () => {
    setShowModal(true);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  const handleRunQuery = () => {
    validateForm().then(async (errors) => {
      if (validAdvancedSearch(errors)) {
        handleShowModal();
      }
    });
  };

  const tempWidget = useMemo(() => {
    return {
      id: 'tempWidgetId',
      type: 'MacantaQuery',
      title: 'Advanced Search Results',
      groupName: values.doType,
      queryId: advancedSearchQuery.queryId,
      userId: userDetails?.userId,
    };
  }, [advancedSearchQuery.queryId]);

  return (
    <>
      {!!advancedSearchQuery.error && (
        <Typography
          color="error"
          style={{
            fontSize: '0.75rem',
            textAlign: 'center',
            marginRight: '1rem',
          }}>
          {advancedSearchQuery.error.message}
        </Typography>
      )}
      <Button
        disabled={!dirty}
        variant="contained"
        startIcon={<SearchIcon />}
        onClick={handleRunQuery}
        size="medium"
        {...props}>
        {label}
      </Button>
      <Modal
        headerTitle={
          headerTitle || `Advanced Search Results (${values.doType})`
        }
        open={showModal}
        onClose={handleCloseModal}
        contentWidth={'100%'}
        contentHeight={'96%'}>
        <WidgetData
          loading={!advancedSearchQuery.queryId && advancedSearchQuery.loading}
          // HeaderLeftComp={<AdvancedSearchHeaderLeft />}
          // hideDateRange
          // hideSearch
          widget={tempWidget}
          data={advancedSearchQuery.items}
          range="main"
          // options={{
          //   skip: true,
          // }}
          // onChangePage={setPage}
          // onChangeRowsPerPage={setRowsPerPage}
          // onSort={setSort}
          PaginationRightComp={<AdvancedSearchSaveBtn />}
        />
      </Modal>
    </>
  );
};

RunQueryBtn.defaultProps = {
  label: 'Run Query',
};

export default RunQueryBtn;
