import React from 'react';
import * as Styled from './styles';

const SettingCard = ({IconComp, title, description, onClick, ...props}) => {
  return (
    <Styled.SettingCard {...props}>
      <Styled.ActionArea onClick={onClick}>
        <Styled.IconContainer>
          <IconComp sx={{fontSize: 40}} color="primary" />
        </Styled.IconContainer>
        <Styled.TextContainer>
          <Styled.Title>{title}</Styled.Title>
          <Styled.Description>{description}</Styled.Description>
        </Styled.TextContainer>
      </Styled.ActionArea>
    </Styled.SettingCard>
  );
};

SettingCard.defaultProps = {
  info: [],
};

export default SettingCard;
