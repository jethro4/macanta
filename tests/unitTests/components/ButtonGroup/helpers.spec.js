import {getSelectedVariantAttrs} from '@macanta/components/ButtonGroup/helpers';

describe('[components] - ButtonGroup', () => {
  describe('get correct attributes based on variant', () => {
    it('happy - default variant', () => {
      const actualResult = getSelectedVariantAttrs();
      const expectedResult = {
        variant: 'contained',
        style: {
          fontWeight: 'bold',
          color: 'white',
        },
      };

      expect(actualResult).toEqual(expectedResult);
    });
  });
});
