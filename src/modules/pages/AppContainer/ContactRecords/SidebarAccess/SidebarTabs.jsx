import React, {useState, useEffect, useLayoutEffect} from 'react';
import {Responsive, WidthProvider} from 'react-grid-layout';
import Box from '@mui/material/Box';
import useIsAdmin from '@macanta/hooks/useIsAdmin';
import {sortArrayByObjectKey, moveItem} from '@macanta/utils/array';
import TabItem from './TabItem';
import useDragHelper from '@macanta/hooks/base/useDragHelper';

import * as WidgetStyled from '@macanta/modules/pages/AppContainer/Widgets/styles';

const ResponsiveReactGridLayout = WidthProvider(Responsive);

const SidebarTabs = ({data, contactId, onSort}) => {
  const [layouts, setLayouts] = useState({});

  const isAdmin = useIsAdmin();

  const {handleDragging, handleDragStop} = useDragHelper({
    onSwitch: (layout, oldItem, newItem) => {
      const sortedLayout = sortArrayByObjectKey(layout, 'y');

      const updatedLayouts = generateLayouts(
        sortedLayout.map((l) => ({id: l.i})),
      );

      setLayouts(updatedLayouts);

      handleSortData(oldItem.y, newItem.y);
    },
  });

  const handleSortData = async (fromIndex, toIndex) => {
    const sortedData = moveItem({
      arr: data,
      fromIndex,
      toIndex,
    });

    const updatedTabTitles = sortedData.map((d) => d.title);

    onSort(updatedTabTitles);
  };

  useLayoutEffect(() => {
    const updatedLayouts = generateLayouts(data);

    setLayouts(updatedLayouts);
  }, [data]);

  useEffect(() => {
    if (data?.length) {
      setTimeout(function () {
        window.dispatchEvent(new Event('resize'));
      }, 0);
    }
  }, [data]);

  return data.length ? (
    <Box
      style={{
        padding: '0.5rem 0',
      }}>
      <ResponsiveReactGridLayout
        isDraggable={!!isAdmin}
        layouts={layouts}
        rowHeight={40}
        margin={[0, 0]}
        cols={{lg: 1, md: 1, sm: 1, xs: 1, xxs: 1}}
        isBounded
        isResizable={false}
        compactType="vertical"
        onDrag={handleDragging}
        onDragStop={handleDragStop}>
        {data.map((item) => {
          return (
            <WidgetStyled.ReactGridItemContainer
              sx={{
                '& .MuiListItem-container': {
                  listStyleType: 'none',
                },
              }}
              key={item.id}>
              <TabItem
                key={item.id}
                id={item.id}
                type={item.type}
                title={item.title}
                contactId={contactId}
              />
            </WidgetStyled.ReactGridItemContainer>
          );
        })}
      </ResponsiveReactGridLayout>
    </Box>
  ) : null;
};

const generateLayouts = (items) => {
  return ['lg', 'md', 'sm', 'xs', 'xxs'].reduce((acc, col) => {
    if (items) {
      acc[col] = items.map((item, index) => {
        return {
          x: 0,
          y: index,
          w: 1,
          h: 1,
          i: String(item.id),
        };
      });
    }

    return acc;
  }, {});
};

export default SidebarTabs;
