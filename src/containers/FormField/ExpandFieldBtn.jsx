import React, {useState} from 'react';
import Popover from '@macanta/components/Popover';
import VisibilityIcon from '@mui/icons-material/Visibility';
import * as Styled from './styles';

const ExpandFieldBtn = ({children, timeout, ...props}) => {
  const [anchorEl, setAnchorEl] = useState(false);

  const openExpand = Boolean(anchorEl);

  const handleExpand = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <Styled.ExpandButton
        onClick={handleExpand}
        size="small"
        startIcon={<VisibilityIcon />}>
        Expand
      </Styled.ExpandButton>
      <Popover
        open={openExpand}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}>
        <div
          style={{
            minWidth: 500,
            maxHeight: 500,
            display: 'flex',
            flexDirection: 'column',
            backgroundColor: '#f5f6fa',
            padding: '8px',
          }}>
          <Styled.TextAreaField
            autoFocus
            fullHeight
            value={children}
            timeout={timeout}
            {...props}
          />
          <Styled.ExpandDoneButton variant="contained" onClick={handleClose}>
            Done
          </Styled.ExpandDoneButton>
        </div>
      </Popover>
    </>
  );
};

export default ExpandFieldBtn;
