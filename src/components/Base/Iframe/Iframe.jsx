import React, {useState} from 'react';
import Box from '@mui/material/Box';
import LoadingIndicator from '@macanta/components/LoadingIndicator';

const Iframe = ({
  src,
  title,
  iframeStyle,
  onLoad,
  sx,
  style,
  className,
  ...props
}) => {
  const [hideLoading, setHideLoading] = useState(false);

  const hideLoadingIndicator = (...args) => {
    setHideLoading(true);

    onLoad?.(...args);
  };

  return (
    <Box
      sx={{
        position: 'relative',
        width: '100%',
        height: '100%',
        ...sx,
      }}
      style={style}
      className={className}>
      {!hideLoading && (
        <Box
          sx={{
            position: 'absolute',
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <LoadingIndicator />
        </Box>
      )}

      <iframe
        src={src}
        title={title}
        style={{
          display: 'block',
          width: '100%',
          height: '100%',
          ...iframeStyle,
        }}
        onLoad={hideLoadingIndicator}
        frameBorder="0"
        allowFullScreen
        // webkitallowfullscreen
        // mozallowfullscreen
        // style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"
        {...props}
      />
    </Box>
  );
};

export default Iframe;
