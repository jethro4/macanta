import React from 'react';
import {useFormikContext} from 'formik';

const withFormHandlers = (WrappedComponent) => {
  const EnhancedFormHandler = ({name, ...props}) => {
    const {values, errors, setFieldValue} = useFormikContext();

    const handleChange = (value, ...args) => {
      setFieldValue(name, value);

      props.onChange?.(value, ...args);
    };

    return (
      <WrappedComponent
        {...props}
        value={values[name]}
        error={errors[name]}
        onChange={handleChange}
      />
    );
  };

  return EnhancedFormHandler;
};

export default withFormHandlers;
