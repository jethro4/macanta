import React, {useState, useLayoutEffect} from 'react';
import {Responsive, WidthProvider} from 'react-grid-layout';
import {useFormikContext} from 'formik';
import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import Box from '@mui/material/Box';
import {PopoverButton, DialogButton} from '@macanta/components/Button';
import {SubSection} from '@macanta/containers/Section';
import * as WidgetStyled from '@macanta/modules/pages/AppContainer/Widgets/styles';
import {
  arrayConcatOrUpdateByKey,
  sortArrayByObjectKey,
  moveItem,
} from '@macanta/utils/array';
import FieldCard from './FieldCard';
import SubGroupForm from './SubGroupForm';
import DOFieldForm from './DOFieldForm';

const ResponsiveReactGridLayout = WidthProvider(Responsive);

const SubGroupColumn = ({
  sectionId,
  id,
  name,
  data,
  position,
  lastPosition,
  containerProps,
  type,
  hideFieldName,
  gridProps,
  ...props
}) => {
  const {setValues} = useFormikContext();

  const [layouts, setLayouts] = useState({});

  const handleDeleteSubGroup = () => {
    setValues((state) => {
      const newFieldsToDelete = [];
      const sectionItem = state.sectionsData.find(
        (section) => section.id === sectionId,
      );
      const subGroupItem = sectionItem.subGroups.find(
        (subGroup) => subGroup.id === id,
      );

      subGroupItem.data.forEach((d) => {
        if (!d.isNew) {
          newFieldsToDelete.push(d.id);
        }
      });

      return {
        ...state,
        sectionsData: arrayConcatOrUpdateByKey({
          arr: state.sectionsData,
          item: {
            ...sectionItem,
            subGroups: sectionItem.subGroups.filter(
              (subGroup) => subGroup.id !== id,
            ),
          },
        }),
        fieldsToDelete: state.fieldsToDelete.concat(newFieldsToDelete),
      };
    });
  };

  const handleDeleteField = (fieldId) => {
    setValues((state) => {
      const newFieldsToDelete = [];
      const sectionItem = state.sectionsData.find(
        (section) => section.id === sectionId,
      );
      const subGroupItem = sectionItem.subGroups.find(
        (subGroup) => subGroup.id === id,
      );
      const itemData = subGroupItem.data.find(
        (d) => d.id === fieldId && !d.isNew,
      );

      if (itemData) {
        newFieldsToDelete.push(itemData.id);
      }

      return {
        ...state,
        sectionsData: arrayConcatOrUpdateByKey({
          arr: state.sectionsData,
          item: {
            ...sectionItem,
            subGroups: arrayConcatOrUpdateByKey({
              arr: sectionItem.subGroups,
              item: {
                ...subGroupItem,
                data: subGroupItem.data.filter((d) => d.id !== fieldId),
              },
            }),
          },
        }),
        fieldsToDelete: state.fieldsToDelete.concat(newFieldsToDelete),
      };
    });
  };

  const handleSortData = (fromIndex, toIndex) => {
    setValues((state) => {
      const sectionItem = state.sectionsData.find(
        (section) => section.id === sectionId,
      );
      const subGroupItem = sectionItem.subGroups.find(
        (subGroup) => subGroup.id === id,
      );

      // const sortedData = sortArrayByPriority(subGroupItem.data, 'id', orderArr);
      const sortedData = moveItem({
        arr: subGroupItem.data,
        fromIndex,
        toIndex,
      });

      return {
        ...state,
        sectionsData: arrayConcatOrUpdateByKey({
          arr: state.sectionsData,
          item: {
            ...sectionItem,
            subGroups: arrayConcatOrUpdateByKey({
              arr: sectionItem.subGroups,
              item: {
                ...subGroupItem,
                data: sortedData,
              },
            }),
          },
        }),
      };
    });
  };

  const handleDragStop = (layout, oldItem, newItem) => {
    const hasSwitched = oldItem.x !== newItem.x || oldItem.y !== newItem.y;
    const sortedLayout = sortArrayByObjectKey(layout, 'y');

    if (hasSwitched) {
      const updatedLayouts = generateLayouts(
        sortedLayout.map((l) => ({id: l.i})),
      );

      setLayouts(updatedLayouts);

      handleSortData(oldItem.y, newItem.y);
    }
  };

  useLayoutEffect(() => {
    const updatedLayouts = generateLayouts(data);

    setLayouts(updatedLayouts);
  }, [data]);

  return (
    <Box
      style={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
      }}
      {...containerProps}>
      <SubSection
        title={name}
        titleStyle={{
          fontSize: '0.875rem',
          lineHeight: '0.875rem',
          marginRight: '1rem',
        }}
        style={{
          marginTop: '1.2rem',
          minWidth: 310,
          alignSelf: 'flex-start',
        }}
        bodyStyle={{
          backgroundColor: '#e4e4e4',
          overflowY: 'unset',
        }}
        headerStyle={{
          minHeight: '2.55rem',
        }}
        HeaderRightComp={
          <Box
            style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <PopoverButton
              style={{
                padding: 0,
              }}
              icon
              sx={{
                padding: '0.25rem',
                color: 'info.main',
              }}
              // onClick={handleAdd}
              size="small"
              TooltipProps={{
                title: 'Edit Sub-Section',
              }}
              PopoverProps={{
                title: 'Edit Sub-Section',
                contentWidth: 380,
                anchorOrigin: {
                  vertical: 'bottom',
                  horizontal: 'center',
                },
                transformOrigin: {
                  vertical: -8,
                  horizontal: 'center',
                },
              }}
              renderContent={(handleClose) => (
                <SubGroupForm
                  sectionId={sectionId}
                  id={id}
                  name={name}
                  position={position}
                  lastPosition={lastPosition}
                  onClose={handleClose}
                />
              )}>
              <EditIcon
                style={{
                  fontSize: 18,
                }}
              />
            </PopoverButton>
            <DialogButton
              style={{
                padding: 0,
              }}
              icon
              size="small"
              TooltipProps={{title: 'Delete Sub-Section'}}
              DialogProps={{
                title: 'Are you sure?',
                description: [
                  `You are about to delete "${name}" from sub-sections.`,
                  'This will also remove all fields within it. Do you want to proceed?',
                ],
                onActions: (handleClose) => {
                  return [
                    {
                      label: 'Delete',
                      onClick: () => {
                        handleDeleteSubGroup();
                        handleClose();
                      },
                      startIcon: <DeleteForeverIcon />,
                    },
                  ];
                },
              }}>
              <DeleteForeverIcon
                style={{
                  fontSize: 18,
                }}
                sx={{
                  color: 'grayDarker.main',
                }}
              />
            </DialogButton>
          </Box>
        }
        {...props}>
        {!!data.length && (
          <ResponsiveReactGridLayout
            layouts={layouts}
            rowHeight={56}
            cols={{lg: 1, md: 1, sm: 1, xs: 1, xxs: 1}}
            margin={[12, 12]}
            isBounded
            isResizable={false}
            compactType="vertical"
            onDragStop={handleDragStop}
            {...gridProps}>
            {data.map((item) => {
              return (
                <WidgetStyled.ReactGridItemContainer key={item.id}>
                  <FieldCard
                    item={item}
                    hideFieldName={hideFieldName || item.isNew}
                    onDelete={handleDeleteField}
                    renderContent={(handleClose) => (
                      <DOFieldForm
                        item={item}
                        sectionId={sectionId}
                        subGroupId={id}
                        onClose={handleClose}
                        type={type}
                        hideFieldName={hideFieldName}
                      />
                    )}
                  />
                </WidgetStyled.ReactGridItemContainer>
              );
            })}
          </ResponsiveReactGridLayout>
        )}
      </SubSection>

      <PopoverButton
        className="add-field-btn"
        style={{
          marginTop: '1rem',
        }}
        size="small"
        variant="contained"
        startIcon={<AddIcon />}
        PopoverProps={{
          title: 'Add Field',
          contentWidth: 1000,
          anchorOrigin: {
            vertical: 'center',
            horizontal: 'center',
          },
          transformOrigin: {
            vertical: 'center',
            horizontal: 'left',
          },
          // bodyStyle: {
          //   backgroundColor: '#f5f6fA',
          // },
        }}
        renderContent={(handleClose) => (
          <DOFieldForm
            sectionId={sectionId}
            subGroupId={id}
            onClose={handleClose}
            type={type}
          />
        )}>
        Add Field
      </PopoverButton>
    </Box>
  );
};

const generateLayouts = (items) => {
  return ['lg', 'md', 'sm', 'xs', 'xxs'].reduce((acc, col) => {
    if (items) {
      acc[col] = items.map((item, index) => {
        return {
          x: 0,
          y: index,
          w: 1,
          h: 1,
          i: String(item.id),
        };
      });
    }

    return acc;
  }, {});
};

export default SubGroupColumn;
