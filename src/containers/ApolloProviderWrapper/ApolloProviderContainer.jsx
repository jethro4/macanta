import React from 'react';
import {ApolloClient, ApolloProvider} from '@apollo/client';
import {cache, defaultOptions} from '@macanta/config/apolloConfig';
import link from '@macanta/config/apolloLink';
import {
  cache as mockCache,
  link as mockLink,
} from '@tests/jestSettings/mockClient';
import envConfig from '@macanta/config/envConfig';

const getClient = () => {
  const client = new ApolloClient(
    Object.assign(
      {},
      !envConfig.isTest
        ? {
            link,
            cache,
            defaultOptions,
          }
        : {
            cache: mockCache,
            link: mockLink,
          },
    ),
  );

  return client;
};

export const client = getClient();

const ApolloProviderContainer = ({children}) => {
  return <ApolloProvider client={client}>{children}</ApolloProvider>;
};

export default ApolloProviderContainer;
