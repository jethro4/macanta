import {gql} from '@apollo/client';

export default gql`
  mutation deleteNotifications(
    $deleteNotificationsInput: DeleteNotificationsInput!
  ) {
    deleteNotifications(input: $deleteNotificationsInput) {
      success
    }
  }
`;
