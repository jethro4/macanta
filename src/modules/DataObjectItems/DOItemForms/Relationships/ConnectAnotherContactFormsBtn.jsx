import React, {useState} from 'react';
import Modal from '@macanta/components/Modal';
import ConnectAnotherContactForms from './ConnectAnotherContactForms';
import Button from '@macanta/components/Button';
import PersonAddIcon from '@mui/icons-material/PersonAdd';

const ConnectAnotherContactFormsBtn = ({
  selectedContactIds,
  onRenderValue,
  relationshipOptions,
  onSuccess,
  renderRelationshipForms,
  schema,
  hideRelationshipsError,
  headerTitle,
  btnLabel,
  submitBtnLabel,
  loading,
  RelationshipsHeaderRightComp,
  usersOnly,
  ...props
}) => {
  const [showModal, setShowModal] = useState(false);

  const handleShowModal = () => {
    setShowModal(true);
  };
  const handleCloseModal = () => {
    setShowModal(false);
  };
  const handleSuccess = (...args) => {
    onSuccess && onSuccess(...args);
    setShowModal(false);
  };

  return (
    <>
      <Button
        onClick={handleShowModal}
        size="small"
        variant="contained"
        startIcon={<PersonAddIcon />}
        {...props}>
        {btnLabel}
      </Button>
      <Modal
        innerStyle={{
          maxWidth: 1000,
          backgroundColor: '#f5f6fa',
        }}
        disableScroll
        headerTitle={headerTitle}
        open={showModal}
        onClose={handleCloseModal}
        contentWidth={'100%'}
        contentHeight={'94%'}>
        <ConnectAnotherContactForms
          selectedContactIds={selectedContactIds}
          onRenderValue={onRenderValue}
          relationshipOptions={relationshipOptions}
          renderRelationshipForms={renderRelationshipForms}
          schema={schema}
          hideRelationshipsError={hideRelationshipsError}
          submitBtnLabel={submitBtnLabel}
          relationshipsLoading={loading}
          RelationshipsHeaderRightComp={RelationshipsHeaderRightComp}
          onSuccess={handleSuccess}
          usersOnly={usersOnly}
        />
      </Modal>
    </>
  );
};

ConnectAnotherContactFormsBtn.defaultProps = {
  btnLabel: 'Connect Another Contact',
  headerTitle: 'Connect Another Contact',
};

export default ConnectAnotherContactFormsBtn;
