import React from 'react';
import MUITooltip from '@mui/material/Tooltip';

const Tooltip = (props) => {
  return <MUITooltip {...props} />;
};

export default Tooltip;
