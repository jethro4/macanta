import React, {useState} from 'react';
import Box from '@mui/material/Box';
import TurnedInIcon from '@mui/icons-material/TurnedIn';
import Button from '@macanta/components/Button';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import FormField from '@macanta/containers/FormField';
import useAppSettings from '@macanta/hooks/admin/useAppSettings';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import {useMutation} from '@apollo/client';
import {GET_APP_SETTINGS, UPDATE_APP_SETTINGS} from '@macanta/graphql/app';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {TIMEZONES} from '@macanta/constants/timezones';
import * as NoteTaskFormsStyled from '@macanta/modules/NoteTaskForms/styles';
import * as applicationStyles from '@macanta/themes/applicationStyles';

const TimezoneForm = ({onSave}) => {
  const {displayMessage} = useProgressAlert();

  const appSettingsQuery = useAppSettings({
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const appTimeZone = appSettingsQuery.data?.appTimeZone;

  const [timezone, setTimezone] = useState(appTimeZone);

  const [callUpdateAppSettings, updateAppSettingsMutation] = useMutation(
    UPDATE_APP_SETTINGS,
    {
      onError() {},
      onCompleted: async (data) => {
        if (data?.updateAppSettings?.success) {
          displayMessage('Saved successfully!');
        }
      },
    },
  );

  const handleSave = async () => {
    const {data} = await callUpdateAppSettings({
      variables: {
        updateAppSettingsInput: {
          type: 'AppTimeZone',
          value: timezone,
        },
        __mutationkey: 'updateAppSettingsInput',
      },
    });

    if (data?.updateAppSettings?.success) {
      appSettingsQuery.client.writeQuery({
        query: GET_APP_SETTINGS,
        data: {
          getAppSettings: {
            __typename: 'AppSettings',
            ...appSettingsQuery.data,
            appTimeZone: timezone,
          },
        },
      });

      onSave();
    }
  };

  return (
    <>
      <Box
        style={{
          height: '100%',
          display: 'flex',
          flexDirection: 'column',
        }}>
        <Box
          style={{
            flex: 1,
            padding: '1rem',
          }}>
          <FormField
            type="Select"
            options={TIMEZONES}
            value={timezone}
            onChange={setTimezone}
            label="Timezone"
            fullWidth
            size="small"
            variant="outlined"
          />
        </Box>
        <NoteTaskFormsStyled.Footer
          sx={applicationStyles.footer}
          style={{
            padding: '1rem',
            margin: 0,
            borderTop: '1px solid #eee',
            width: '100%',
            backgroundColor: 'white',
          }}>
          <Button
            disabled={timezone === appTimeZone}
            variant="contained"
            startIcon={<TurnedInIcon />}
            onClick={handleSave}
            size="medium">
            Save
          </Button>
        </NoteTaskFormsStyled.Footer>
      </Box>
      <LoadingIndicator modal loading={updateAppSettingsMutation.loading} />
    </>
  );
};

export default TimezoneForm;
