const typename = 'AccessPermissions';

export default (overwrites) => {
  return {
    __typename: typename,

    isAdmin: true,
    isUser: false,
    permissionMeta: {
      allowUserDeleteContact: true,
      allowUserDeleteDOItem: true,
      allowUserAttachment: true,
      allowUserDeleteDOAttachment: true,
      allowUserToMergeContacts: false,
      allowEmailSync: true,
      allowAddRemoveEmailDomainFilter: true,
      allowContactExport: true,
      allowDataObjectExport: true,
      allowNoteTaskExport: true,
      allowAddDirectRelationship: true,
      userDOEditPermission: {
        otherUserRelationship: [],
        withOtherUserDataToggle: true,
        withOtherUserRelationshipToggle: true,
        withOtherUserRelationshipDataToggle: true,
        withOtherUserRelationshipRelationshipToggle: true,
        __typename: 'UserDOEditPermission',
      },
      hideActionButtonsForGroups: [],
      hideConnectButtonsForGroups: [],
      hidePurchaseButton: true,
      __typename: 'UserPermissionMeta',
    },
    mqbs: [
      {
        queryId: 'qry_LLnvOemrcoQwC00f',
        permission: 'READ_WRITE',
        __typename: 'MQBPermission',
      },
    ],
    mqbWidgetAccess: [
      {
        queryId: 'qry_LLnvOemrcoQwC00f',
        show: true,
        __typename: 'MQBWidgetAccess',
      },
    ],
    mqbWidgetEmail: [
      {
        queryId: 'qry_LLnvOemrcoQwC00f',
        show: false,
        __typename: 'MQBWidgetEmail',
      },
    ],
    tasks: [],
    userGuides: [
      {
        guideId: 'GeneralInterface-Search Function',
        type: 'GeneralInterface',
        value:
          '# Search Window\n<iframe width="560" height="315" src="https://www.youtube.com/embed/_t-d87plx7w" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
        subType: 'Search Function',
        meta: [],
        __typename: 'UserGuide',
      },
      {
        guideId: 'GeneralInterface-Notes & Tasks',
        type: 'GeneralInterface',
        value:
          '#Notes & Tasks\n<iframe width="560" height="315" src="https://www.youtube.com/embed/_t-d87plx7w" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
        subType: 'Notes & Tasks',
        meta: [],
        __typename: 'UserGuide',
      },
      {
        guideId: 'GeneralInterface-Contact Window',
        type: 'GeneralInterface',
        value: '**Sample Contact Window Guide**',
        subType: 'Contact Window',
        meta: [],
        __typename: 'UserGuide',
      },
      {
        guideId: 'SidebarTabs-Notes',
        type: 'SidebarTabs',
        value:
          '# Notes Tab\n<iframe width="560" height="315" src="https://www.youtube.com/embed/_t-d87plx7w" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
        subType: 'Notes',
        meta: [],
        __typename: 'UserGuide',
      },
      {
        guideId: 'SidebarTabs-Admin',
        type: 'SidebarTabs',
        value:
          '# Admin Tab\n<iframe width="560" height="315" src="https://www.youtube.com/embed/_t-d87plx7w" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
        subType: 'Admin',
        meta: [],
        __typename: 'UserGuide',
      },
      {
        guideId: 'SidebarTabs-Sites',
        type: 'SidebarTabs',
        value: '',
        subType: 'Sites',
        meta: [],
        __typename: 'UserGuide',
      },
      {
        guideId: 'SidebarTabs-Opportunities',
        type: 'SidebarTabs',
        value: 'Sample Opportunities Tab Guide',
        subType: 'Opportunities',
        meta: [],
        __typename: 'UserGuide',
      },
      {
        guideId: 'DataObject-undefined-General Information',
        type: 'DataObject',
        value: 'Test Companies Sections - General Information Guide',
        subType: 'General Information',
        meta: [
          {
            name: 'doType',
            value: 'undefined',
            __typename: 'UserGuideMeta',
          },
        ],
        __typename: 'UserGuide',
      },
      {
        guideId: 'DataObject-undefined-User Details',
        type: 'DataObject',
        value: '',
        subType: 'User Details',
        meta: [
          {
            name: 'doType',
            value: 'undefined',
            __typename: 'UserGuideMeta',
          },
        ],
        __typename: 'UserGuide',
      },
      {
        guideId: 'DataObject-undefined-Tracking',
        type: 'DataObject',
        value: 'Test Contact Object Sections - Tracking Guide',
        subType: 'Tracking',
        meta: [
          {
            name: 'doType',
            value: 'undefined',
            __typename: 'UserGuideMeta',
          },
        ],
        __typename: 'UserGuide',
      },
      {
        guideId: 'DataObject-undefined-Default Fields',
        type: 'DataObject',
        value: 'Test Contact Object Sections - Default Fields Guide',
        subType: 'Default Fields',
        meta: [
          {
            name: 'doType',
            value: 'undefined',
            __typename: 'UserGuideMeta',
          },
        ],
        __typename: 'UserGuide',
      },
      {
        guideId: 'DataObject-Sites-General Information',
        type: 'DataObject',
        value: '',
        subType: 'General Information',
        meta: [
          {
            name: 'doType',
            value: 'Sites',
            __typename: 'UserGuideMeta',
          },
        ],
        __typename: 'UserGuide',
      },
      {
        guideId: 'DataObject-Contact Object-Basic Info',
        type: 'DataObject',
        value: 'Test Basic Info Sample Guide',
        subType: 'Basic Info',
        meta: [
          {
            name: 'doType',
            value: 'Contact Object',
            __typename: 'UserGuideMeta',
          },
        ],
        __typename: 'UserGuide',
      },
      {
        guideId: 'DataObject-Contact Object-Default Fields',
        type: 'DataObject',
        value: 'Test Default Fields Sample Guide',
        subType: 'Default Fields',
        meta: [
          {
            name: 'doType',
            value: 'Contact Object',
            __typename: 'UserGuideMeta',
          },
        ],
        __typename: 'UserGuide',
      },
      {
        guideId: 'DataObject-Contact Object-General',
        type: 'DataObject',
        value: 'Test General Sample Guide',
        subType: 'General',
        meta: [
          {
            name: 'doType',
            value: 'Contact Object',
            __typename: 'UserGuideMeta',
          },
        ],
        __typename: 'UserGuide',
      },
      {
        guideId: 'DataObject-Contact Object-Tracking',
        type: 'DataObject',
        value: 'Test Tracking Sample Guide',
        subType: 'Tracking',
        meta: [
          {
            name: 'doType',
            value: 'Contact Object',
            __typename: 'UserGuideMeta',
          },
        ],
        __typename: 'UserGuide',
      },
      {
        guideId: 'DataObject-Opportunities-General Information',
        type: 'DataObject',
        value: 'Test Opportunities Sample Guide',
        subType: 'General Information',
        meta: [
          {
            name: 'doType',
            value: 'Opportunities',
            __typename: 'UserGuideMeta',
          },
        ],
        __typename: 'UserGuide',
      },
      {
        guideId: 'DataObject-Support Queries-Tracking',
        type: 'DataObject',
        value: 'Test Tracking Sample Guide',
        subType: 'Tracking',
        meta: [
          {
            name: 'doType',
            value: 'Support Queries',
            __typename: 'UserGuideMeta',
          },
        ],
        __typename: 'UserGuide',
      },
      {
        guideId: 'DataObject-Support Queries-General',
        type: 'DataObject',
        value: 'Test General Sample Guide',
        subType: 'General',
        meta: [
          {
            name: 'doType',
            value: 'Support Queries',
            __typename: 'UserGuideMeta',
          },
        ],
        __typename: 'UserGuide',
      },
    ],
    tabs: [
      {
        groupId: 'notes_tab',
        permission: 'READ_WRITE',
        __typename: 'TabPermission',
      },
      {
        groupId: 'ci_4458c47816f984ed',
        permission: 'NO_ACCESS',
        __typename: 'TabPermission',
      },
      {
        groupId: 'ci_aa82f4c085572d46',
        permission: 'NO_ACCESS',
        __typename: 'TabPermission',
      },
      {
        groupId: 'ci_ea6d915d5ad9acee',
        permission: 'NO_ACCESS',
        __typename: 'TabPermission',
      },
      {
        groupId: 'ci_kc2bram8',
        permission: 'READ_WRITE',
        __typename: 'TabPermission',
      },
      {
        groupId: 'co_customfields',
        permission: 'NO_ACCESS',
        __typename: 'TabPermission',
      },
      {
        groupId: 'com_history',
        permission: 'READ_WRITE',
        __typename: 'TabPermission',
      },
    ],
    sections: [
      {
        groupId: 'ci_4458c47816f984ed',
        access: [
          {
            name: 'General Information',
            permission: 'NO_ACCESS',
            __typename: 'Access',
          },
        ],
        __typename: 'SectionPermission',
      },
      {
        groupId: 'ci_aa82f4c085572d46',
        access: [
          {
            name: 'General Information',
            permission: 'NO_ACCESS',
            __typename: 'Access',
          },
        ],
        __typename: 'SectionPermission',
      },
      {
        groupId: 'ci_ea6d915d5ad9acee',
        access: [
          {
            name: 'General Information',
            permission: 'NO_ACCESS',
            __typename: 'Access',
          },
        ],
        __typename: 'SectionPermission',
      },
      {
        groupId: 'ci_kc2bram8',
        access: [
          {
            name: 'General',
            permission: 'NO_ACCESS',
            __typename: 'Access',
          },
          {
            name: 'Tracking',
            permission: 'READ_WRITE',
            __typename: 'Access',
          },
        ],
        __typename: 'SectionPermission',
      },
      {
        groupId: 'co_customfields',
        access: [
          {
            name: 'Basic Info',
            permission: 'NO_ACCESS',
            __typename: 'Access',
          },
          {
            name: 'Default Fields',
            permission: 'NO_ACCESS',
            __typename: 'Access',
          },
          {
            name: 'Test Fields',
            permission: 'NO_ACCESS',
            __typename: 'Access',
          },
          {
            name: 'General',
            permission: 'NO_ACCESS',
            __typename: 'Access',
          },
          {
            name: 'User Details',
            permission: 'NO_ACCESS',
            __typename: 'Access',
          },
          {
            name: 'Contact Status',
            permission: 'NO_ACCESS',
            __typename: 'Access',
          },
          {
            name: 'Tracking',
            permission: 'NO_ACCESS',
            __typename: 'Access',
          },
        ],
        __typename: 'SectionPermission',
      },
    ],
    contacts: [
      {
        groupId: 'Global',
        access: [
          {
            name: 'TypeA',
            permission: 'READ_WRITE',
            relationship: '',
            __typename: 'ContactAccess',
          },
          {
            name: 'TypeB',
            permission: 'READ_WRITE',
            relationship: '',
            __typename: 'ContactAccess',
          },
          {
            name: 'TypeC',
            permission: 'READ_WRITE',
            relationship: '',
            __typename: 'ContactAccess',
          },
        ],
        __typename: 'ContactPermission',
      },
    ],
    emailTemplates: [],
    workflowBoards: [
      {
        boardId: 'pm_2670ae20c8810c67',
        scope: 'All',
        __typename: 'WorkflowBoardPermission',
      },
      {
        boardId: 'pm_5009488ecd968490',
        scope: 'CurrentUser',
        __typename: 'WorkflowBoardPermission',
      },
      {
        boardId: 'pm_ca09ee32141269d0',
        scope: 'CurrentUser',
        __typename: 'WorkflowBoardPermission',
      },
    ],

    ...overwrites,
  };
};
