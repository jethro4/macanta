import {experimentalStyled as styled} from '@mui/material/styles';

import Box from '@mui/material/Box';
//TODO: use core IconButton for now
//cannot use @macanta/components/Button/IconButton because of weird Tooltip bug not showing on hover
import IconButton from '@mui/material/IconButton';

export const DOTypeButtonGroupContainer = styled(Box)`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: ${({theme}) => theme.spacing(2)};
`;

export const ActionButtonContainer = styled(Box)`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0 0.5rem;
`;

export const ActionButton = styled(IconButton)`
  padding: 0.25rem;
  color: ${({theme}) => theme.palette.info.main};
  ${({show}) => show && 'opacity: 1!important;'}
`;

export const PopoverHeader = styled(Box)`
  padding: ${({theme}) => `${theme.spacing(0.5)}`}
    ${({theme}) => `${theme.spacing(2)}`};
  background-color: ${({theme}) => `${theme.palette.primary.main}`};
  display: flex;
  justify-content: space-between;
  align-items: center;
  color: ${({theme}) => theme.palette.common.white};
`;
