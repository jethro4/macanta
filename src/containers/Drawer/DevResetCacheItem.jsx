import React from 'react';
import {useApolloClient} from '@apollo/client';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import RestartAltIcon from '@mui/icons-material/RestartAlt';
import envConfig from '@macanta/config/envConfig';
import Tooltip from '@macanta/components/Tooltip';
import {clearSessionVars} from '@macanta/graphql/cache/sessionVars';

const DevResetCacheItem = () => {
  const client = useApolloClient();

  const handleResetCache = () => {
    clearSessionVars();

    client.resetStore();
  };

  return (
    <Tooltip title="Reset Cache" placement="right">
      <ListItem button onClick={handleResetCache}>
        <ListItemIcon>
          <RestartAltIcon />
        </ListItemIcon>
        <ListItemText primary="Reset Cache" />
      </ListItem>
    </Tooltip>
  );
};

const DevResetCacheItemContainer = (props) =>
  envConfig.isDev ? <DevResetCacheItem {...props} /> : null;

export default DevResetCacheItemContainer;
