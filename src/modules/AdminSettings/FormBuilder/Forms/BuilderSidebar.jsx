import React from 'react';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import SidebarBasicInfo from './SidebarBasicInfo';
import * as Styled from './styles';

const BuilderSidebar = (props) => {
  return (
    <Styled.FormSidebarRoot {...props}>
      <Typography
        sx={{
          color: 'primary.main',
        }}
        style={{
          letterSpacing: '0.5px',
          fontWeight: 'bold',
        }}>
        Basic Information
      </Typography>
      <Divider
        style={{
          margin: '8px 0 24px',
          borderBottom: '1px solid #eee',
        }}
      />
      <SidebarBasicInfo />
    </Styled.FormSidebarRoot>
  );
};

export default BuilderSidebar;
