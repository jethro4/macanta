import React, {useState} from 'react';
import TurnedInIcon from '@mui/icons-material/TurnedIn';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import Button from '@macanta/components/Button';
import FormField from '@macanta/containers/FormField';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import useAppSettings from '@macanta/hooks/admin/useAppSettings';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {useMutation} from '@apollo/client';
import {GET_APP_SETTINGS, UPDATE_APP_SETTINGS} from '@macanta/graphql/app';
import * as NoteTaskFormsStyled from '@macanta/modules/NoteTaskForms/styles';
import * as applicationStyles from '@macanta/themes/applicationStyles';
import * as QueryBuilderStyled from '@macanta/modules/AdminSettings/QueryBuilder/styles';

const AddDomainForms = ({onSave, ...props}) => {
  const [domain, setDomain] = useState('');

  const {displayMessage} = useProgressAlert();

  const appSettingsQuery = useAppSettings({
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const filteredEmailDomain = appSettingsQuery.data?.filteredEmailDomain;

  const [callUpdateAppSettings, updateAppSettingsMutation] = useMutation(
    UPDATE_APP_SETTINGS,
    {
      onError() {},
      onCompleted: async (data) => {
        if (data?.updateAppSettings?.success) {
          displayMessage('Saved successfully!');
        }
      },
    },
  );

  const handleSave = async () => {
    const domains = filteredEmailDomain.concat(
      domain.split(',').map((item) => item.trim()),
    );

    const {data} = await callUpdateAppSettings({
      variables: {
        updateAppSettingsInput: {
          type: 'FilteredEmailDomain',
          value: `[${domains.map((item) => `"${item}"`).join(',')}]`,
        },
        __mutationkey: 'updateAppSettingsInput',
      },
    });

    if (data?.updateAppSettings?.success) {
      appSettingsQuery.client.writeQuery({
        query: GET_APP_SETTINGS,
        data: {
          getAppSettings: {
            __typename: 'AppSettings',
            ...appSettingsQuery.data,
            filteredEmailDomain: domains,
          },
        },
      });

      onSave();
    }
  };

  return (
    <QueryBuilderStyled.FormRoot
      style={{
        backgroundColor: 'white',
      }}
      {...props}>
      <QueryBuilderStyled.Body
        style={{
          padding: '1rem 0',
        }}>
        <FormField
          spellCheck="false"
          autoFocus
          style={{
            padding: '0 1rem',
          }}
          onChange={setDomain}
          label="Enter comma separated domains, ex. (google.com,gmail.com)"
          value={domain}
          fullWidth
          size="small"
          variant="outlined"
          onEnterPress={() => {
            if (domain) {
              handleSave();
            }
          }}
        />

        <LoadingIndicator modal loading={updateAppSettingsMutation.loading} />
      </QueryBuilderStyled.Body>
      <NoteTaskFormsStyled.Footer
        sx={applicationStyles.footer}
        style={{
          padding: '1rem',
          margin: 0,
          borderTop: '1px solid #eee',
          width: '100%',
          backgroundColor: 'white',
        }}>
        <Box
          style={{
            flex: 1,
            display: 'flex',
            justifyContent: 'center',
          }}>
          {!!updateAppSettingsMutation.error && (
            <Typography
              color="error"
              style={{
                fontSize: '0.75rem',
                textAlign: 'center',
                marginRight: '1rem',
              }}>
              {updateAppSettingsMutation.error.message}
            </Typography>
          )}
        </Box>
        <Button
          disabled={!domain}
          variant="contained"
          startIcon={<TurnedInIcon />}
          onClick={handleSave}
          size="medium">
          Save
        </Button>
      </NoteTaskFormsStyled.Footer>
    </QueryBuilderStyled.FormRoot>
  );
};

export default AddDomainForms;
