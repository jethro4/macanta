import {gql} from '@apollo/client';
import {QUERY_BUILDER_ITEM_ATTRIBUTES} from './fragments';

export default gql`
  mutation createOrUpdateQueryBuilder(
    $createOrUpdateQueryBuilderInput: CreateOrUpdateQueryBuilderInput!
  ) {
    createOrUpdateQueryBuilder(input: $createOrUpdateQueryBuilderInput) {
      ...queryBuilderItem
    }
  }
  ${QUERY_BUILDER_ITEM_ATTRIBUTES}
`;
