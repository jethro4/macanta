import React, {useState} from 'react';
import Section from '@macanta/containers/Section';
import DataTable from '@macanta/containers/DataTable';
import Typography from '@mui/material/Typography';
import DataObjectsQuery from '@macanta/modules/hoc/DataObjectsQuery';
import DOActionButtons from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/DOActionButtons';
import DOColumnsWithFieldsAndRelationships from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/DOColumnsWithFieldsAndRelationships';
import DOSearch from './DOSearch';
import AddDOItemBtn from './AddDOItemBtn';
import MultiDOConnectContactsBtn from './MultiDOConnectContactsBtn';
import useContactId from '@macanta/hooks/useContactId';
import Pagination from '@macanta/components/Pagination';
import * as NoteTaskFormsStyled from '@macanta/modules/NoteTaskForms/styles';
import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import * as Styled from './styles';

const DataObjectItems = ({groupId, doTitle: doTitleParam}) => {
  const doTitle = decodeURIComponent(doTitleParam);

  const contactId = useContactId();

  const [searchFilter, setSearchFilter] = useState('');
  const [filters, setFilters] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(25);
  const [sort, setSort] = useState();

  const handleSearch = ({search}) => {
    setSearchFilter(search);

    setPage(0);
  };

  const handleFilters = (filterVal) => {
    setFilters(filterVal);

    setPage(0);
  };

  const handleChangePage = (event, value) => {
    setPage(value);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value));
    setPage(0);
  };

  const handleSort = (property, sortDirection) => {
    setSort({
      order: sortDirection,
      orderBy: property,
    });

    setPage(0);
  };

  return (
    <DataObjectsQuery
      search={searchFilter}
      filters={filters}
      contactId={contactId}
      groupId={groupId}
      page={page}
      limit={rowsPerPage}
      order={sort?.order}
      orderBy={sort?.orderBy}>
      {({data = {}, selectedDO, loading: doLoading, refetch}) => {
        return (
          <DOColumnsWithFieldsAndRelationships
            groupId={groupId}
            items={selectedDO?.items}
            hideContacts
            contactId={contactId}>
            {({
              columns,
              doItemsData,
              loading: columnsLoading,
              doFields,
              relationships,
            }) => {
              const loading = Boolean(doLoading || columnsLoading);

              return (
                <NoteTaskHistoryStyled.FullHeightGrid container>
                  <NoteTaskHistoryStyled.FullHeightFlexGridColumn item xs={12}>
                    <Section
                      fullHeight
                      loading={loading}
                      HeaderLeftComp={
                        <Typography color="textPrimary">{doTitle}</Typography>
                      }
                      HeaderRightComp={
                        <Styled.HeaderRightContainer>
                          <DOSearch
                            style={{
                              marginRight: '1rem',
                            }}
                            FilterProps={{
                              PopoverProps: {
                                title: `${doTitle} Filters`,
                              },
                            }}
                            onSearch={handleSearch}
                            onFilter={handleFilters}
                            filters={filters}
                            groupId={groupId}
                          />
                          <AddDOItemBtn
                            style={{
                              marginRight: '1rem',
                            }}
                            doTitle={doTitle}
                            groupId={groupId}
                            doFields={doFields}
                            relationships={relationships}
                            onSave={refetch}
                          />
                          <MultiDOConnectContactsBtn
                            doTitle={doTitle}
                            groupId={groupId}
                            onSave={refetch}
                          />
                        </Styled.HeaderRightContainer>
                      }>
                      <DataTable
                        fullHeight
                        hideEmptyMessage={loading}
                        hidePagination
                        onSort={handleSort}
                        columns={columns}
                        data={doItemsData}
                        renderActionButtons={(actionBtnProps) => (
                          <DOActionButtons
                            {...actionBtnProps}
                            groupId={groupId}
                            doFields={doFields}
                            relationships={relationships}
                            doTitle={doTitle}
                          />
                        )}
                        numOfTextLines={2}
                      />
                      <NoteTaskFormsStyled.Footer
                        style={{
                          margin: 0,
                          borderTop: '1px solid #eee',
                          boxShadow: '0px -4px 10px -2px #eee',
                          width: '100%',
                          backgroundColor: 'white',
                          justifyContent: 'space-between',
                          zIndex: 1,
                        }}>
                        <Pagination
                          table
                          count={data?.total}
                          page={page}
                          onPageChange={handleChangePage}
                          rowsPerPage={rowsPerPage}
                          rowsPerPageOptions={[25, 50, 100]}
                          onRowsPerPageChange={handleChangeRowsPerPage}
                        />
                      </NoteTaskFormsStyled.Footer>
                    </Section>
                  </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
                </NoteTaskHistoryStyled.FullHeightGrid>
              );
            }}
          </DOColumnsWithFieldsAndRelationships>
        );
      }}
    </DataObjectsQuery>
  );
};

export default DataObjectItems;
