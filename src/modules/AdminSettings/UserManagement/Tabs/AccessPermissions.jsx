import React from 'react';
import FormField from '@macanta/containers/FormField';
import {ACCESS_LEVELS} from '@macanta/constants/accessLevels';
import * as Styled from './styles';
import * as DataObjectsSearchTableStyled from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/styles';

const ACCESS_PERMISSIONS = [
  {value: ACCESS_LEVELS.READ_ONLY},
  {
    value: ACCESS_LEVELS.READ_WRITE,
  },
];

export const AccessRadioGroup = ({value, onChange, style, sx, ...props}) => {
  return (
    <DataObjectsSearchTableStyled.ActionButtonContainer
      style={{
        width: '100%',
        padding: 0,
        ...style,
      }}
      sx={sx}>
      <FormField
        style={{
          width: '100%',
          marginBottom: 0,
        }}
        sx={{
          '& .MuiFormControlLabel-root': {
            flex: 1,
            margin: 0,
            display: 'flex',
            justifyContent: 'center',
          },
        }}
        itemContainerStyle={{
          padding: 0,
        }}
        type="Radio"
        row
        options={ACCESS_PERMISSIONS}
        value={value}
        onChange={onChange}
        fullWidth
        {...props}
      />
    </DataObjectsSearchTableStyled.ActionButtonContainer>
  );
};

export const AccessTableHeadCell = ({titles}) => {
  return (
    <Styled.HeadCell>
      {titles?.map((title) => (
        <Styled.Label key={title}>{title}</Styled.Label>
      )) || (
        <>
          <Styled.Label>Read Only</Styled.Label>
          <Styled.Label>Read Write</Styled.Label>
        </>
      )}
    </Styled.HeadCell>
  );
};
