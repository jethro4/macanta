import React, {useState} from 'react';
import Button from '@macanta/components/Button';
import AddIcon from '@mui/icons-material/Add';
import Modal from '@macanta/components/Modal';
import useHideActionButtonGroupIds from '@macanta/hooks/useHideActionButtonGroupIds';
import DOItemEditSection from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/DOItemEditSection';

const AddDOItemBtnContainer = (props) => {
  const hiddenGroupIds = useHideActionButtonGroupIds();

  return !hiddenGroupIds.includes(props.groupId) ? (
    <AddDOItemBtn {...props} />
  ) : null;
};

const AddDOItemBtn = ({
  doTitle,
  groupId,
  doFields,
  relationships,
  onSave,
  ...props
}) => {
  const [showForms, setShowForms] = useState(false);

  const handleAddDOItem = () => {
    if (doFields && relationships) {
      setShowForms(true);
    }
  };

  const handleCloseEditModal = () => {
    setShowForms(false);
  };

  const handleSave = () => {
    handleCloseEditModal();
    onSave();
  };

  return (
    <>
      <Button
        disabled={!(doFields && relationships)}
        onClick={handleAddDOItem}
        size="small"
        variant="contained"
        startIcon={<AddIcon />}
        {...props}>
        Add Item
      </Button>
      <Modal
        headerTitle={doTitle}
        open={showForms}
        onClose={handleCloseEditModal}
        contentWidth={'100%'}
        contentHeight={'96%'}>
        <DOItemEditSection
          groupId={groupId}
          fields={doFields}
          relationships={relationships}
          onSave={handleSave}
        />
      </Modal>
    </>
  );
};

export default AddDOItemBtnContainer;
