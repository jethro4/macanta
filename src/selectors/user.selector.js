import {getEmailDomain} from '@macanta/utils/uri';

const checkDNSAdded = (dnsArr) => !!dnsArr?.length;

export const getDomain = ({email, validDomains = []}) => {
  const emailDomain = getEmailDomain(email);
  const domain = validDomains.find((item) => item.domain === emailDomain);
  const dns = !domain?.dns
    ? []
    : domain?.dns.map((item) => ({
        host: item.host || '',
        type: item.type || '',
        data: item.data || '',
        valid: item.valid || false,
      }));

  return Object.assign(
    {
      name: emailDomain || '',
    },
    domain && {
      username: domain.username || '',
      subdomain: domain.subdomain || '',
      legacy: domain.legacy || false,
      dns,
      valid: domain.valid || false,
      dnsAdded: checkDNSAdded(dns),
    },
  );
};

export const checkDomainValid = (dnsArr) =>
  !!dnsArr?.length && dnsArr.every((item) => item.valid);

export const transformUser = (user = {}) => {
  return {
    id: user.id || '',
    firstName: user.firstName || '',
    lastName: user.lastName || '',
    email: user.email || '',
    contactLevel: user.contactLevel || '',
    templateId: user.templateId || '',
  };
};
