import React from 'react';
import * as Styled from './styles';

const TemplateName = () => (
  <Styled.Root data-testid="TemplateName">
    <h1>TemplateName component</h1>
  </Styled.Root>
);

export default TemplateName;
