import React, {useState} from 'react';
import Modal from '@macanta/components/Modal';
import useLocationChange from '@macanta/hooks/base/useLocationChange';
import ContactSupportForms from './ContactSupportForms';

const ContactSupportOption = ({
  modal,
  showModal: showModalProp = false,
  onClose,
  id,
  name,
  email,
}) => {
  const [showModal, setShowModal] = useState(showModalProp);

  const handleClose = () => {
    setShowModal(false);

    onClose();
  };

  let exportContactForms = (
    <ContactSupportForms
      contactId={id}
      contactName={name}
      contactEmail={email}
      onSuccess={handleClose}
    />
  );

  useLocationChange(handleClose);

  if (modal) {
    exportContactForms = (
      <Modal
        headerTitle={'Contact Support'}
        open={showModal}
        onClose={handleClose}>
        {exportContactForms}
      </Modal>
    );
  }

  return exportContactForms;
};

export default ContactSupportOption;
