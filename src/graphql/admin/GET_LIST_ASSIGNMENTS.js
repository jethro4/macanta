import {gql} from '@apollo/client';
import {LIST_ASSIGNMENT_FRAGMENT} from './fragments';

export default gql`
  query getListAssignments($appName: String!, $apiKey: String!, $queryId: ID!) {
    getListAssignments(appName: $appName, apiKey: $apiKey, queryId: $queryId) {
      ...listAssignmentFragment
    }
  }
  ${LIST_ASSIGNMENT_FRAGMENT}
`;
