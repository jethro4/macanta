import useQuery from '@macanta/hooks/apollo/useQuery';
import {GET_CUSTOM_TAB} from '@macanta/graphql/admin';

const useCustomTab = ({id}, options) => {
  const customTabQuery = useQuery(GET_CUSTOM_TAB, {
    variables: {
      id,
    },
    ...options,
    skip: !id || options?.skip,
  });

  const customTab = customTabQuery.data?.getCustomTab;

  return {...customTabQuery, data: customTab};
};

export default useCustomTab;
