import React from 'react';
import Image from '@macanta/components/Image';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import {makeStyles} from '@mui/material/styles';
import clsx from 'clsx';
import {GET_APP_SETTINGS} from '@macanta/graphql/app';
import useQuery from '@macanta/hooks/apollo/useQuery';

const useStyles = makeStyles(() => ({
  container: {
    width: '11.25rem',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'inherit',
    overflow: 'hidden',
  },
  image: {
    maxHeight: '100%',
  },
}));

const CompanyLogo = ({className, imageStyle, ...props}) => {
  const classes = useStyles();
  const appSettingsQuery = useQuery(GET_APP_SETTINGS);

  const customLogo =
    appSettingsQuery.data?.getAppSettings?.customLogo?.imageData;

  const base64Img = `data:image/png;base64,${customLogo}`;

  return (
    <div className={clsx(classes.container, className)} {...props}>
      {!customLogo && appSettingsQuery.loading && (
        <LoadingIndicator
          transparent
          style={{
            marginBottom: 20,
          }}
        />
      )}
      {customLogo && (
        <Image className={classes.image} src={base64Img} style={imageStyle} />
      )}
    </div>
  );
};

export default CompanyLogo;
