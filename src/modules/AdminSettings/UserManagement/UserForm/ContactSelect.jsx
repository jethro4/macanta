import React, {useState} from 'react';
import {useReactiveVar} from '@apollo/client';
import SelectField from '@macanta/components/Forms/ChoiceFields/SelectField';
import MultiSelectField from '@macanta/components/Forms/ChoiceFields/MultiSelectField';
import useValue from '@macanta/hooks/base/useValue';
import useAccumulatedItems from '@macanta/hooks/base/useAccumulatedItems';
// import useAutocompleteContacts from '@macanta/hooks/contact/useAutocompleteContacts';
import useContacts from '@macanta/hooks/contact/useContacts';
import {recentContactsVar} from '@macanta/graphql/cache/vars';

const ContactSelect = ({filteredContacts, onChange, multiple, ...props}) => {
  const recentContacts = useReactiveVar(recentContactsVar);

  const [search, setSearch] = useState('');

  // const autocompleteContactsQuery = useAutocompleteContacts(
  //   {search},
  //   {limit: 5},
  // );
  // const options = autocompleteContactsQuery.data;

  const {contacts, initLoading} = useContacts(
    {
      q: search,
      limit: 5,
    },
    {
      skip: !search,
    },
  );

  const optionsArr = !search ? recentContacts : contacts;

  const [options] = useValue(
    () =>
      !filteredContacts
        ? optionsArr
        : optionsArr?.filter((o) => !filteredContacts.includes(o.id)),
    [filteredContacts, optionsArr],
  );
  const {items: allOptions} = useAccumulatedItems(options, 'id');

  const handleSearch = (searchVal) => {
    setSearch(searchVal);
  };

  const handleRenderValue = (value) => {
    if (!multiple) {
      return props.getOptionLabel(allOptions.find((o) => o.id === value));
    }

    return value.length > 1
      ? `${value.length} selected`
      : value
          .map((v) => props.getOptionLabel(allOptions.find((o) => o.id === v)))
          .join(', ');
  };

  const FieldComp = !multiple ? SelectField : MultiSelectField;

  return (
    <FieldComp
      // loading={autocompleteContactsQuery.loading}
      searchValue={search}
      loading={initLoading}
      allowSearch
      searchPlaceholder="Search Contact..."
      onSearch={handleSearch}
      onChange={onChange}
      options={options}
      subLabelKey="email"
      valueKey="id"
      {...(!search && {
        emptyMessage: '',
      })}
      renderValue={handleRenderValue}
      {...props}
    />
  );
};

export default ContactSelect;
