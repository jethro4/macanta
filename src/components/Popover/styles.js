import {experimentalStyled as styled} from '@mui/material/styles';

import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import {IconButton} from '@macanta/components/Button';

export const PopoverHeader = styled(Box)`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding-left: ${({theme}) => theme.spacing(2)};
  background-color: ${({theme}) => theme.palette.primary.main};
`;

export const HeaderTitle = styled(Typography)`
  color: ${({theme}) => theme.palette.common.white};
  font-size: 1rem;
  line-height: 1rem;
`;

export const CloseButton = styled(IconButton)`
  padding: 8px;
  margin-right: 4px;
`;
