import React, {useMemo} from 'react';
import SendAndArchiveIcon from '@mui/icons-material/SendAndArchive';
import SendIcon from '@mui/icons-material/Send';
import Typography from '@mui/material/Typography';
import Form from '@macanta/components/Form';
import Button from '@macanta/components/Button';
import Tooltip from '@macanta/components/Tooltip';
import EmailAttachments from '@macanta/modules/ContactDetails/EmailAttachments';
import EmailPreview from '@macanta/modules/ContactDetails/EmailPreview';
import EmailSignatureBtn from '@macanta/modules/ContactDetails/EmailSignatureBtn';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import {useMutation} from '@apollo/client';
import {EMAIL} from '@macanta/graphql/admin';
import EmailBodyRTE from '@macanta/modules/ContactDetails/EmailBodyRTE';
import useSignature from '@macanta/hooks/admin/useSignature';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import EmailAttachmentOptions from '@macanta/modules/ContactDetails/EmailAttachmentOptions';
import {getURI} from '@macanta/utils/file';
import * as FormStyled from '@macanta/modules/NoteTaskForms/styles';
import * as ContactDetailsStyled from '@macanta/modules/ContactDetails/styles';
import * as Storage from '@macanta/utils/storage';

const EmailDOForms = ({
  itemId,
  doTitle,
  connectedContacts,
  onSuccess,
  ...props
}) => {
  const loggedInUserDetails = Storage.getItem('userDetails');

  const signatureQuery = useSignature();
  const {displayMessage} = useProgressAlert();

  const initValues = {
    to: [],
    from: loggedInUserDetails.email,
    subject: '',
    message: '',
    signature: signatureQuery.data,
    files: [],
  };

  const [callEmailMutation, emailMutation] = useMutation(EMAIL, {
    onCompleted(data) {
      if (data.email?.success) {
        onSuccess();
      }
    },
    onError() {},
  });

  const [callTestEmailMutation, testEmailMutation] = useMutation(EMAIL, {
    onCompleted() {
      displayMessage('Test email sent!');
    },
    onError() {},
  });

  const handleSendEmail = (values) => {
    const isTestEmail = values.isTestEmail;

    if (!isTestEmail && !values.to?.length) {
      alert('"To" is required!');
      return;
    }

    if (!values.subject) {
      const confirmed = confirm('Send this email without a subject?');

      if (!confirmed) {
        return;
      }
    }
    if (!values.message) {
      const confirmed = confirm('Send this email without a message?');

      if (!confirmed) {
        return;
      }
    }

    const callSendEmail = !isTestEmail
      ? callEmailMutation
      : callTestEmailMutation;

    const doFiles = values.files.filter((f) => f.type === 'do');
    const nonDOFiles = values.files.filter((f) => f.type !== 'do');

    callSendEmail({
      variables: {
        emailInput: {
          ...(isTestEmail
            ? {
                userId: loggedInUserDetails.userId,
                sendMethod: 'single',
                mode: 'Live',
                toContacts: loggedInUserDetails.userId,
                subject: values.subject,
                message: values.message,
                signature: values.signature,
                uploadedFileAttachments: values.files.map((f) => {
                  return {
                    fileName: f.fileName,
                    content: getURI(f.thumbnail, f.fileExt),
                  };
                }),
              }
            : {
                userId: loggedInUserDetails.userId,
                itemId,
                groupName: doTitle,
                sendMethod: 'do',
                mode: 'Live',
                toContacts: values.to.join(','),
                subject: values.subject,
                message: values.message,
                signature: values.signature,
                uploadedFileAttachments: nonDOFiles.map((f) => ({
                  fileName: f.fileName,
                  content: f.thumbnail,
                })),
                selectedFileAttachments: doFiles.map((f) => f.id).join(','),
              }),
        },
        __mutationkey: 'emailInput',
      },
    });
  };

  const formattedConnectedContacts = useMemo(() => {
    return connectedContacts.map((item) => ({
      ...item,
      name: `${item.firstName || ''} ${item.lastName || ''}`.trim(),
      relationships: item.relationships?.map((r) => r.role).join(', '),
    }));
  }, []);

  const error = emailMutation.error;

  return (
    <ContactDetailsStyled.EmailFormsContainer {...props}>
      {signatureQuery.loading && <LoadingIndicator fill align="top" />}
      {!signatureQuery.loading && (
        <Form
          initialValues={initValues}
          // validationSchema={emailValidationSchema}
          onSubmit={handleSendEmail}>
          {({values, errors, handleChange, setFieldValue, handleSubmit}) => (
            <>
              <ContactDetailsStyled.EmailFormsHeaderContainer>
                <ContactDetailsStyled.EmailTextField
                  select
                  multiple
                  chipped
                  options={formattedConnectedContacts}
                  labelKey="name"
                  valueKey="contactId"
                  subLabelKey="email"
                  captionKey="relationships"
                  error={errors.to}
                  onChange={(val) => {
                    setFieldValue('to', val);
                  }}
                  placeholder="To"
                  variant="filled"
                  value={values.to}
                  fullWidth
                  size="medium"
                />

                <ContactDetailsStyled.EmailTextField
                  autoFocus
                  noBorder
                  onChange={handleChange('subject')}
                  placeholder="Subject"
                  variant="filled"
                  value={values.subject}
                  fullWidth
                />
              </ContactDetailsStyled.EmailFormsHeaderContainer>

              <ContactDetailsStyled.EmailRichTextFieldContainer>
                <EmailBodyRTE
                  types="sender,contact,do_all"
                  whitelist={[doTitle]}
                  error={errors.message}
                  onChange={handleChange('message')}
                  placeholder="Type Your Message Here"
                  value={values.message}
                />
              </ContactDetailsStyled.EmailRichTextFieldContainer>

              <EmailAttachments
                files={values.files}
                onDelete={(fileId) => {
                  setFieldValue(
                    'files',
                    values.files.filter((file) => fileId !== file.id),
                  );
                }}
              />

              <FormStyled.Footer
                style={{
                  margin: '8px 0',
                  padding: '0 1rem',
                }}>
                {!!error && (
                  <Typography
                    style={{
                      flex: 1,
                      lineHeight: '0.875rem',
                    }}
                    color="error"
                    variant="subtitle2"
                    align="center">
                    {error.message}
                  </Typography>
                )}
                <EmailSignatureBtn
                  value={values.signature}
                  onEdit={(signature) => {
                    setFieldValue('signature', signature);
                  }}
                />
                <EmailAttachmentOptions
                  doItemId={itemId}
                  selectedAttachments={values.files.filter(
                    (f) => f.type === 'do',
                  )}
                  onSelect={async (data, type) => {
                    if (type === 'do') {
                      const newFiles = data.reduce((acc, d) => {
                        if (!values.files.some((f) => f.id === d.id)) {
                          return acc.concat(d);
                        }

                        return acc;
                      }, values.files);

                      setFieldValue('files', newFiles);
                    } else {
                      const updatedFiles = values.files.concat(data);

                      setFieldValue('files', updatedFiles);
                    }
                  }}
                />
                <EmailPreview values={values} attachments={values.files} />
                <Tooltip title="Send Me Test Email" placement="top">
                  <div>
                    <ContactDetailsStyled.EmailFooterIconButton
                      color="secondary"
                      onClick={() => {
                        setFieldValue('isTestEmail', true);
                        handleSubmit();
                      }}
                      size="small">
                      <SendAndArchiveIcon />
                    </ContactDetailsStyled.EmailFooterIconButton>
                  </div>
                </Tooltip>
                <Button
                  style={{
                    marginLeft: '1.5rem',
                  }}
                  // disabled={!values.message}
                  variant="contained"
                  startIcon={<SendIcon />}
                  onClick={() => {
                    setFieldValue('isTestEmail', false);
                    handleSubmit();
                  }}
                  size="medium">
                  Send Email
                </Button>
              </FormStyled.Footer>
            </>
          )}
        </Form>
      )}
      <LoadingIndicator
        modal
        loading={emailMutation.loading || testEmailMutation.loading}
      />
    </ContactDetailsStyled.EmailFormsContainer>
  );
};

EmailDOForms.defaultProps = {
  connectedContacts: [],
};

export default EmailDOForms;
