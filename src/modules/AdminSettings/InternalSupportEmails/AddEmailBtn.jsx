import React, {useState} from 'react';
import Button from '@macanta/components/Button';
import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';
import Modal from '@macanta/components/Modal';
import Tooltip from '@macanta/components/Tooltip';
import AddEmailForms from './AddEmailForms';
import * as DataObjectsSearchTableStyled from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/styles';

const AddEmailBtn = ({item}) => {
  const [showForms, setShowForms] = useState(false);

  const handleAdd = () => {
    setShowForms(true);
  };

  const handleClose = () => {
    setShowForms(false);
  };

  return (
    <>
      {!item ? (
        <Button
          onClick={handleAdd}
          size="small"
          variant="contained"
          startIcon={<AddIcon />}>
          Add Internal Support
        </Button>
      ) : (
        <Tooltip title="Edit">
          <DataObjectsSearchTableStyled.ActionButton
            show={showForms}
            onClick={handleAdd}
            size="small">
            <EditIcon
              style={{
                fontSize: 22,
              }}
            />
          </DataObjectsSearchTableStyled.ActionButton>
        </Tooltip>
      )}

      <Modal
        headerTitle={`${!item ? 'Add' : 'Edit'} Internal Support`}
        open={showForms}
        onClose={handleClose}
        contentWidth={500}
        contentHeight={330}>
        <AddEmailForms item={item} onSave={handleClose} />
      </Modal>
    </>
  );
};

export default AddEmailBtn;
