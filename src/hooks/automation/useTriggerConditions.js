import useQuery from '@macanta/hooks/apollo/useQuery';
import {GET_TRIGGER_CONDITIONS} from '@macanta/graphql/automation';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {findItem} from '@macanta/utils/array';

const FIELD_KEY_MAPPING = {
  queryId: 'id',
  queryName: 'name',
  queryDescription: 'description',
  queryConnectedDataType: 'doTitle',
  queryRestartAfter: 'restartAfter',

  queryCDField: 'doConditions',
  queryCDFieldName: 'name',
  queryCDFieldValue: 'value',
  queryCDFieldValues: 'values',
  queryCDFieldLogic: 'logic',
  queryCDFieldOperator: 'operator',

  queryContact: 'contactConditions',
  queryContactRelationship: 'relationship',
  queryContactRelationshipFieldLogic: 'logic',

  FileAttachedByUser: 'fileAttachedByUser',
  FileAttachedByContact: 'fileAttachedByContact',
};

const useTriggerConditions = (options) => {
  const doTypesQuery = useDOTypes({
    removeEmail: true,
    includeContactObject: true,
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });
  const query = useQuery(GET_TRIGGER_CONDITIONS, {
    keyMapping: FIELD_KEY_MAPPING,
    transformResult: (result) =>
      result?.items.map((item) => {
        const doType = findItem(doTypesQuery.result, {
          title: item.doTitle,
        });

        return {
          ...item,
          groupId: doType?.id,
        };
      }),
    dependencies: doTypesQuery.result,
    ...options,
  });

  return query;
};

export default useTriggerConditions;
