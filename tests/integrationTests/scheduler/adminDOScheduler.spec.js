import React from 'react';
import {waitApis} from '@tests/jestSettings/utils/selectorUtils';
import {
  handleMockDOTypes,
  handleMockRelationships,
} from '@tests/integrationTests/mockUtils';
import {
  hasLength,
  hasOptions,
  hasTexts,
  isInDocument,
} from '@tests/jestSettings/utils/assertUtils';
import {click} from '@tests/jestSettings/utils/eventUtils';
import {screen} from '@testing-library/react';
import {
  handleDOFields,
  handleDOSchedules,
  handleDOTypes,
} from '@tests/integrationTests/scheduler/testUtils';
import {renderApp} from '@tests/jestSettings/utils/renderUtils';
import {Router} from '@reach/router';
import DOScheduler from '@macanta/modules/AdminSettings/DOScheduler';

const setup = async () => {
  renderApp(
    <Router>
      <DOScheduler path={`/app/admin-settings/scheduler`} />
    </Router>,
  );

  handleDOTypes();
  handleDOSchedules();
  handleDOFields();
  handleMockRelationships();

  await waitApis('listDOSchedules');
};

describe('Admin DO Scheduler', () => {
  describe('Display Schedules', () => {
    it('show data groups sorted by default DO Types order', async () => {
      await setup();

      await hasLength(['subheader'], 2, {
        testId: 'DOSchedulesList',
      });

      await waitApis([
        'listDataObjectTypes',
        'listDataObjectFields',
        'listRelationships',
      ]);

      await hasTexts(['subheader'], ['DO Type 2', 'DO Type 1'], {
        testId: 'DOSchedulesList',
      });
    });

    it('show data for each schedule', async () => {
      await setup();

      await hasLength(['rows'], 3, {
        testId: 'DOSchedulesList',
      });

      await waitApis([
        'listDataObjectTypes',
        'listDataObjectFields',
        'listRelationships',
      ]);

      await hasTexts(
        ['rows'],
        [
          [
            'Sample Date Scheduler',
            'Sample Description',
            'Relationship 1',
            'Relationship 2',
            'Sample Date Field',
            'N/A',
            'Date',
          ],
          [
            'Last Login Scheduler',
            'This Shows User When Their Last Login',
            'Relationship 1',
            'Relationship 2',
            'Last Login',
            'N/A',
            'Date',
          ],
          [
            'Events Scheduler',
            'Schedules for events',
            'Relationship 1',
            'Relationship 2',
            'Event Start',
            'Event End',
            'Date & Time',
          ],
        ],
        {
          testId: 'DOSchedulesList',
        },
      );
    });
  });

  describe('Connect Schedules', () => {
    it('show DO Types selection then show form after selecting an option', async () => {
      setup();

      handleMockDOTypes();

      await waitApis('listDataObjectTypes');

      click('Add Schedule');

      await hasOptions(['DO Type 1', 'DO Type 2', 'DO Type 3', 'DO Type 4']);

      click('DO Type 1', 'selectItem');

      await isInDocument(screen.getByTestId('DOScheduleForm'));
    });

    // it('select Scheduling Relationship', async () => {
    //   renderApp(
    //     <Router>
    //       <DOScheduler path={`/app/admin-settings/scheduler`} />
    //     </Router>,
    //   );

    //   handleMockRelationships();

    //   await waitApis();

    //   const selectRelationship = selectSelect({order: 1});

    //   await toggleSelect(selectRelationship, 'Relationship 2');

    //   await waitUntil(() =>
    //     expect(selectRelationship).toHaveValue('Relationship2'),
    //   );
    // });

    // it('show DO Types selection then select an option', async () => {
    //   const mockOnSelect = jest.fn();
    //   const mockGetModalProps = jest.fn((doType) => ({
    //     headerTitle: `Add Schedule (${doType.title})`,
    //   }));

    //   renderApp(
    //     <DOTypesSelectButton
    //       onSelect={mockOnSelect}
    //       getModalProps={mockGetModalProps}
    //     />,
    //   );

    //   handleMockDOTypes();

    //   await waitApis('listDataObjectTypes');

    //   click('Add Schedule');

    //   await hasLength(['listItem'], 4);
    // });
  });
});
