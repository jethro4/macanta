import useQuery from '@macanta/hooks/apollo/useQuery';
import {GET_ACCESS_PERMISSIONS} from '@macanta/graphql/permissions';
import * as Storage from '@macanta/utils/storage';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import useContactId from './useContactId';

const useOtherUserDOPermission = ({validate, metaData}) => {
  const {userId: loggedInUserId, email: userEmail} = Storage.getItem(
    'userDetails',
  );
  const contactId = useContactId();

  const {data} = useQuery(GET_ACCESS_PERMISSIONS, {
    variables: {
      email: userEmail,
    },
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
    onError() {},
  });

  const isAdmin = data?.getAccessPermissions?.isAdmin;
  const isLoggedInUser = loggedInUserId === contactId;

  const userDOEditPermission =
    data?.getAccessPermissions?.permissionMeta?.userDOEditPermission;

  let hasAccess = true;

  if (isLoggedInUser || isAdmin) {
    hasAccess = true;
  } else if (userDOEditPermission && metaData.hasConnectedUser) {
    if (metaData.hasConnectedRelationships) {
      if (validate === 'data') {
        hasAccess = userDOEditPermission.withOtherUserRelationshipDataToggle;
      } else if (validate === 'relationship') {
        hasAccess =
          userDOEditPermission.withOtherUserRelationshipRelationshipToggle;
      }
    } else {
      if (validate === 'data') {
        hasAccess = userDOEditPermission.withOtherUserDataToggle;
      } else if (validate === 'relationship') {
        hasAccess = userDOEditPermission.withOtherUserRelationshipToggle;
      }
    }
  }

  return {hasAccess};
};

export default useOtherUserDOPermission;
