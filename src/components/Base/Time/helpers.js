export const getIntervalByUnit = (unit) => {
  let interval;

  switch (unit) {
    case 'second': {
      interval = 1;

      break;
    }
    case 'minute': {
      interval = 60;

      break;
    }
    default: {
      // 5 minutes default
      interval = 5 * 60;

      break;
    }
  }

  return interval * 1000;
};
