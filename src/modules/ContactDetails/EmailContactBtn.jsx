import React, {useState} from 'react';
import Modal from '@macanta/components/Modal';
import EmailContactForms from './EmailContactForms';
import EmailIcon from '@mui/icons-material/Email';
import Button from '@macanta/components/Button';
import useNylasInfo from '@macanta/hooks/admin/useNylasInfo';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import {nylasPostponedVar} from '@macanta/graphql/cache/vars';
import NylasModal from '@macanta/modules/Nylas/NylasModal';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import * as Storage from '@macanta/utils/storage';

const EmailContactBtn = ({to, contactId, ...props}) => {
  const loggedInUserDetails = Storage.getItem('userDetails');

  const [showModal, setShowModal] = useState(false);

  const {loading: loadingNylas, shouldNylasSync} = useNylasInfo();
  const {displayMessage} = useProgressAlert();

  const handleAddContact = () => {
    setShowModal(true);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  const handlePostpone = () => {
    nylasPostponedVar(true);
  };

  const onSuccess = () => {
    displayMessage('Email sent!');
    setShowModal(false);
  };

  return (
    <>
      <Button
        onClick={handleAddContact}
        size="small"
        startIcon={<EmailIcon />}
        {...props}>
        {loadingNylas && showModal ? (
          <LoadingIndicator transparent size={20} />
        ) : (
          'Email'
        )}
      </Button>
      {showModal && shouldNylasSync && (
        <NylasModal
          showModal
          onClose={handleCloseModal}
          onPostpone={handlePostpone}
        />
      )}
      <Modal
        headerTitle={`Compose Email (${loggedInUserDetails.email})`}
        open={!loadingNylas && !shouldNylasSync && showModal}
        onClose={handleCloseModal}
        contentWidth={'86%'}
        contentHeight={'94%'}>
        <EmailContactForms
          to={to}
          contactId={contactId}
          onSuccess={onSuccess}
        />
      </Modal>
    </>
  );
};

export default EmailContactBtn;
