import React from 'react';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import ErrorIcon from '@mui/icons-material/Error';
import HelpOutlineIcon from '@mui/icons-material/HelpOutline';
import Box from '@mui/material/Box';
import {ModalButton} from '@macanta/components/Button';
import useQuery from '@macanta/hooks/apollo/useQuery';
import useValue from '@macanta/hooks/base/useValue';
import {LIST_VALID_DOMAIN_EMAILS} from '@macanta/graphql/users';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {getDomain} from '@macanta/selectors/user.selector';
import ValidateDomainEmailForm from './ValidateDomainEmailForm';

const EmailValidationBtn = ({email}) => {
  const validDomainsQuery = useQuery(LIST_VALID_DOMAIN_EMAILS, {
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const validDomainsArr = validDomainsQuery.data?.listValidDomainEmails;

  const [domain] = useValue(
    () => getDomain({email, validDomains: validDomainsArr}),
    [email, validDomainsArr],
  );

  return (
    <Box
      style={{
        display: 'flex',
        justifyContent: 'center',
        paddingRight: 4,
      }}>
      {validDomainsQuery.initLoaded && (
        <ModalButton
          icon
          style={{
            padding: 0,
          }}
          size="small"
          variant="contained"
          color="info"
          TooltipProps={{
            title: !domain.dnsAdded || !domain.valid ? 'Validate' : 'Validated',
          }}
          ModalProps={{
            headerTitle: `Validate Domain (${domain.name})`,
            contentWidth: 800,
            // bodyStyle: {
            //   backgroundColor: '#f5f6fA',
            // },
          }}
          renderContent={(handleClose) => (
            <ValidateDomainEmailForm
              item={domain}
              onClose={handleClose}
              refresh={validDomainsQuery.refetch}
            />
          )}>
          {!domain.dnsAdded ? (
            <HelpOutlineIcon
              style={{
                color: '#ccc',
              }}
              size={20}
            />
          ) : domain.valid ? (
            <CheckCircleIcon
              style={{
                color: '#4caf50',
              }}
              size={20}
            />
          ) : (
            <ErrorIcon
              sx={{
                color: 'warning.main',
              }}
              size={20}
            />
          )}
        </ModalButton>
      )}
    </Box>
  );
};

export default EmailValidationBtn;
