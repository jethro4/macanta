import React from 'react';
import useWorkflowBoardsWithPermission from '@macanta/hooks/admin/useWorkflowBoardsWithPermission';
import BodySection from '@macanta/modules/AdminSettings/UserManagement/BodySection';
import PermissionsTable from '@macanta/modules/AdminSettings/UserManagement/Tabs/PermissionsTable';
import {arrayConcatOrUpdateByKey} from '@macanta/utils/array';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {
  AccessRadioGroup,
  AccessTableHeadCell,
} from '@macanta/modules/AdminSettings/UserManagement/Tabs/AccessPermissions';
import * as DataObjectsSearchTableStyled from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/styles';

const WORKFLOW_BOARD_PERMISSION_TYPES = [
  {value: 'CurrentUser'},
  {value: 'All'},
];

const WORKFLOW_BOARD_ACCESS_COLUMNS = [
  {
    id: 'title',
    label: 'Board Name',
    maxWidth: '50%',
  },
  {
    id: 'type',
    label: 'Data Object',
    maxWidth: '50%',
  },
  {
    id: 'scope',
    label: '',
    disableSort: true,
    disableResize: true,
    minWidth: 280,
    maxWidth: 280,
    renderHeadCellValue: () => {
      return (
        <AccessTableHeadCell titles={["Logged-in User's Cards", 'All Cards']} />
      );
    },
    renderBodyCellValue: ({item, handlers}) => {
      return (
        <DataObjectsSearchTableStyled.ActionButtonContainer
          style={{
            width: '100%',
            padding: 0,
          }}>
          <AccessRadioGroup
            options={WORKFLOW_BOARD_PERMISSION_TYPES}
            value={item.scope}
            onChange={(value) => {
              handlers.onChangeScope({item, value});
            }}
          />
        </DataObjectsSearchTableStyled.ActionButtonContainer>
      );
    },
  },
];

const WorkflowBoardAccessForm = ({permission, onChange, ...props}) => {
  const workflowBoardsQuery = useWorkflowBoardsWithPermission(
    {
      permission,
    },
    {
      fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
    },
  );

  const handleChangeScope = ({item, value}) => {
    const updatedPermission = {
      ...permission,
      workflowBoards: arrayConcatOrUpdateByKey({
        arr: permission.workflowBoards,
        item: {
          boardId: item.id,
          scope: value,
        },
        key: 'boardId',
        includeAll: true,
      }),
    };

    onChange(updatedPermission);
  };

  return (
    <BodySection {...props}>
      <PermissionsTable
        loading={workflowBoardsQuery.loading}
        columns={WORKFLOW_BOARD_ACCESS_COLUMNS}
        data={workflowBoardsQuery.data}
        handlers={{
          onChangeScope: handleChangeScope,
        }}
      />
    </BodySection>
  );
};

export default WorkflowBoardAccessForm;
