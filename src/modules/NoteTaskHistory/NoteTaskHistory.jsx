import React, {useState} from 'react';
import Section from '@macanta/containers/Section';
import List from '@macanta/containers/List';
import useHistoryQuery from './useHistoryQuery';
import NoteItem from './NoteItem';
import TaskItem from './TaskItem';
import ContactNoteForms from '@macanta/modules/NoteTaskForms/NoteForms/ContactNoteForms';
import NoteFormsBtn from '@macanta/modules/NoteTaskForms/NoteFormsBtn';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import HistorySearch from './HistorySearch';
import NotesTasksGuideBtn from './NotesTasksGuideBtn';
import useContactId from '@macanta/hooks/useContactId';
import useNotesPermissions from '@macanta/hooks/useNotesPermissions';
import * as Styled from './styles';

const NoteTaskHistoryContainer = (props) => {
  const notesPermission = useNotesPermissions();

  return notesPermission.isVisible ? <NoteTaskHistory {...props} /> : null;
};

const NoteTaskHistory = () => {
  const contactId = useContactId();
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(20);
  const [filterBy, setFilterBy] = useState('terms');
  const [filter, setFilter] = useState('');

  const {data, loading, total} = useHistoryQuery(
    contactId,
    page,
    rowsPerPage / 2,
    filterBy,
    filter,
  );
  const {displayMessage} = useProgressAlert();
  const notesPermission = useNotesPermissions();

  const handleMutationSuccess = () => {
    displayMessage('Saved successfully!');
    // callHistoryQuery({contactId, page: 0, limit: 10});
  };

  const handleChangeType = (type) => {
    setFilterBy(type);
  };

  const getKeyExtractor = (item, index) => item.id || index;

  const renderRow = (item) => {
    const isNote = !item.assignedTo;

    return (
      <Styled.ItemContainer>
        {isNote ? (
          <NoteItem
            item={item}
            NoteFormsBtnComp={
              <NoteFormsBtn
                isEdit={true}
                NoteFormsComp={
                  <ContactNoteForms
                    {...item}
                    onSuccess={handleMutationSuccess}
                  />
                }
              />
            }
          />
        ) : (
          <TaskItem item={item} />
        )}
      </Styled.ItemContainer>
    );
  };

  const handleChangePage = (event, value) => {
    setPage(value);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value));
    setPage(0);
  };

  const handleSearch = ({search}) => {
    setFilter(search);

    setPage(0);
  };

  return (
    <Styled.FullHeightGrid container>
      <Styled.FullHeightFlexGridColumn item xs={12} lg={6}>
        <Section
          fullHeight
          loading={loading}
          bodyStyle={{
            paddingBottom: 8,
            backgroundColor: '#f5f6fa',
            display: 'flex',
            flexDirection: 'column',
          }}
          title="History"
          TitleRightComp={<NotesTasksGuideBtn />}
          HeaderRightComp={
            <Styled.HeaderRightContainer>
              <HistorySearch
                onChangeCategory={handleChangeType}
                onSearch={handleSearch}
                filterBy={filterBy}
              />
              {!notesPermission.isReadOnly && (
                <Styled.NoteTaskFormsBtn
                  contactId={contactId}
                  onSuccess={handleMutationSuccess}
                />
              )}
            </Styled.HeaderRightContainer>
          }
          FooterComp={
            <Styled.Pagination
              table
              count={total}
              page={page}
              onPageChange={handleChangePage}
              rowsPerPage={rowsPerPage}
              onRowsPerPageChange={handleChangeRowsPerPage}
              rowsPerPageOptions={[20, 50, 100]}
            />
          }>
          <List
            data={data}
            keyExtractor={getKeyExtractor}
            renderRow={renderRow}
            hideEmptyMessage={loading}
          />
        </Section>
      </Styled.FullHeightFlexGridColumn>
    </Styled.FullHeightGrid>
  );
};

export default NoteTaskHistoryContainer;
