import {getAppInfo, isLoggedIn} from '@macanta/utils/app';
import * as Storage from '@macanta/utils/storage';
import * as Sentry from '@sentry/react';
import {BrowserTracing} from '@sentry/tracing';

const pjson = require('../../package.json');

const initialize = () => {
  const [appName] = getAppInfo();
  const sentryEnvVars = {
    environment: process.env.NODE_ENV,
    dsn: process.env.SENTRY_DSN,
    release: process.env.SENTRY_RELEASE,
  };
  const performanceTracingVars = {
    // This enables automatic instrumentation (highly recommended), but is not
    // necessary for purely manual usage
    integrations: [new BrowserTracing()],

    // To set a uniform sample rate
    tracesSampleRate: 0.2,
  };

  if (sentryEnvVars.dsn) {
    console.info('Setting up Sentry', sentryEnvVars);

    Sentry.init({
      ...sentryEnvVars,
      ...performanceTracingVars,
      beforeBreadcrumb(breadcrumb, hint) {
        if (['ui.click', 'ui.input'].includes(breadcrumb.category)) {
          const target = hint.event.target;
          if (target.ariaLabel) {
            breadcrumb.message = target.ariaLabel;
          } else {
            return null;
          }
        }
        return breadcrumb;
      },
    });
  } else {
    console.info('No Sentry Config', sentryEnvVars);
  }

  Sentry.setTag('app_name', appName);
  Sentry.setTag('app_version', pjson.version);
};

const captureException = ({
  err,
  operationName,
  errorType,
  useMessageAsFingerprint,
}) => {
  const fingerprintType = useMessageAsFingerprint
    ? err.message
    : [err.name, operationName, errorType].filter((t) => !!t).join(' - ');
  const userDetails = Storage.getItem('userDetails');

  Sentry.captureException(err, {
    fingerprint: [`Type: ${fingerprintType}`],
    ...(isLoggedIn() && {
      user: {
        id: userDetails?.userId,
        email: userDetails?.email,
      },
    }),
  });
};

export default {
  initialize,
  captureException,
};
