import React from 'react';

const TableContext = React.createContext({
  setResizedCellWidths: () => null,
});

export default TableContext;
