import {gql} from '@apollo/client';
import {USER_ATTRIBUTES} from './fragments';

export default gql`
  mutation createUser($input: CreateUserInput!) {
    createUser(input: $input) {
      ...userFragment
    }
  }
  ${USER_ATTRIBUTES}
`;
