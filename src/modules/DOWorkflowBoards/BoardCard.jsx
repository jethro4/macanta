import React from 'react';
import useQuery from '@macanta/hooks/apollo/useQuery';
import CheckBoxIcon from '@mui/icons-material/CheckBox';
import OpenInNewIcon from '@mui/icons-material/OpenInNew';
import Box from '@mui/material/Box';
import Tooltip from '@macanta/components/Tooltip';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import DOActionButtons from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/DOActionButtons';
import DOContactIdContext from '@macanta/modules/DataObjectItems/DOContactIdContext';
import WidgetContactPopover from '@macanta/modules/pages/AppContainer/Widgets/WidgetContactPopover';
import {DEFAULT_SELECTED_TAB} from '@macanta/modules/pages/AppContainer/ContactRecords/SidebarAccess/SidebarAccess';
import {GET_OUTSTANDING_TASK} from '@macanta/graphql/notesTasks';
import ShowMoreBtn from './ShowMoreBtn';
import * as WidgetStyled from '@macanta/modules/pages/AppContainer/Widgets/styles';
import * as DOSearchTableStyled from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/styles';
import * as Styled from './styles';
import {navigate} from 'gatsby';
const BoardCard = ({
  item,
  itemData,
  groupId,
  type,
  doFields,
  relationships,
  otherContactFields,
  otherData,
  contactLoading,
}) => {
  /* const navigate = useNavigate();*/

  const {data} = useQuery(GET_OUTSTANDING_TASK, {
    variables: {
      contactId: item.contactId,
    },
    onError() {},
  });

  const handleOpenContactView = () => {
    navigate(`/app/contact/${item.contactId}/${DEFAULT_SELECTED_TAB}`);
  };

  const hasTask = !!data?.getOutstandingTask?.hasTask;

  return (
    <WidgetStyled.Root>
      <Styled.ActionArea disabled>
        <Styled.Header
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <Styled.Title
            TooltipProps={{
              placement: 'top',
            }}>
            {item.id}
          </Styled.Title>
          <Box
            className="board-actions-container"
            style={{
              display: 'flex',
              alignItems: 'center',
            }}>
            <Tooltip title="Open Contact">
              <DOSearchTableStyled.ActionButton
                style={{
                  color: '#aaa',
                  marginRight: 4,
                }}
                onClick={handleOpenContactView}
                size="small">
                <OpenInNewIcon />
              </DOSearchTableStyled.ActionButton>
            </Tooltip>
            <WidgetContactPopover
              contactId={item.contactId}
              style={{
                marginRight: 4,
              }}
            />
            <DOContactIdContext.Provider
              value={{
                contactId: item.contactId,
              }}>
              <DOActionButtons
                item={item}
                groupId={groupId}
                doFields={doFields}
                relationships={relationships}
                doTitle={type}
              />
            </DOContactIdContext.Provider>
          </Box>
        </Styled.Header>
        <WidgetStyled.Content
          style={{
            width: '100%',
            flexDirection: 'column',
            justifyContent: 'flex-start',
            alignItems: 'flex-start',
            padding: '0.45rem 0',
          }}>
          {itemData.map(({name, value}, index) => {
            return (
              <Styled.FieldContainer key={`${name}-${index}`}>
                <Styled.FieldName>{name}</Styled.FieldName>
                <Box
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                  }}>
                  <Styled.FieldValue
                    {...(!value && {
                      style: {
                        color: '#aaa',
                      },
                    })}>
                    {value || 'N/A'}
                  </Styled.FieldValue>
                  <LoadingIndicator
                    style={{
                      marginLeft: '8px',
                    }}
                    loading={
                      otherContactFields.includes(name) && contactLoading
                    }
                    transparent
                    size={12}
                  />
                </Box>
              </Styled.FieldContainer>
            );
          })}
        </WidgetStyled.Content>
      </Styled.ActionArea>
      {hasTask && (
        <Tooltip title="Has outstanding tasks" placement="top">
          <CheckBoxIcon
            sx={{
              color: 'primary.main',
            }}
            style={{
              position: 'absolute',
              bottom: '6px',
              left: '11px',
              fontSize: 18,
            }}
          />
        </Tooltip>
      )}
      <ShowMoreBtn data={otherData} />
      <LoadingIndicator fill backdrop loading={!!item.saving} />
    </WidgetStyled.Root>
  );
};

export default BoardCard;
