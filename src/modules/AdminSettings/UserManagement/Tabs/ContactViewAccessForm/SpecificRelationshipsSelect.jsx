import React from 'react';
import FormField from '@macanta/containers/FormField';
import useDORelationships from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDORelationships';

const SpecificRelationshipsSelect = ({groupId, fetchAll, style, ...props}) => {
  const relationshipsQuery = useDORelationships(groupId, {fetchAll});

  const relationships = relationshipsQuery.data?.listRelationships;

  return (
    <FormField
      loading={relationshipsQuery.initLoading}
      style={{
        marginBottom: 0,
        width: '100%',
        ...style,
      }}
      labelPosition="normal"
      type="MultiSelect"
      size="small"
      options={relationships}
      labelKey="role"
      valueKey="id"
      placeholder="Select Relationships..."
      fullWidth
      variant="outlined"
      {...props}
    />
  );
};

export default SpecificRelationshipsSelect;
