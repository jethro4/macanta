import {useState, useLayoutEffect} from 'react';
import useUserGuides from '@macanta/hooks/useUserGuides';

const useSearchGuide = () => {
  const [searchGuide, setSearchGuide] = useState();
  const {general} = useUserGuides();

  useLayoutEffect(() => {
    setSearchGuide(
      general.find((guide) => guide.subType === 'Search Function'),
    );
  }, [general]);

  return searchGuide;
};

export default useSearchGuide;
