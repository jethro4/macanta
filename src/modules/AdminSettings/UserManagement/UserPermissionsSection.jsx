import React from 'react';
import AddIcon from '@mui/icons-material/Add';
import VisibilityIcon from '@mui/icons-material/Visibility';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import Box from '@mui/material/Box';
import {ModalButton, DialogButton} from '@macanta/components/Button';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import Section from '@macanta/containers/Section';
import DataTable from '@macanta/containers/DataTable';
import useMutation from '@macanta/hooks/apollo/useMutation';
import usePermissionTemplates from '@macanta/hooks/admin/usePermissionTemplates';
import {DELETE_PERMISSION_TEMPLATE} from '@macanta/graphql/permissions';
import UserPermissionsTemplateForm from './UserPermissionsTemplateForm';

const DEFAULT_USER_PERMISSION_TEMPLATE_COLUMNS = [
  {
    id: 'name',
    label: 'Name',
    minWidth: 250,
  },
  {
    id: 'description',
    label: 'Description',
    maxWidth: '100%',
  },
];

const UserPermissionsSection = (props) => {
  const permissionTemplatesQuery = usePermissionTemplates();

  const permissionTemplates = permissionTemplatesQuery.data;

  const [
    callDeletePermissionTemplateMutation,
    deletePermissionTemplateMutation,
  ] = useMutation(DELETE_PERMISSION_TEMPLATE, {
    update(cache, {data: {deletePermissionTemplate}}) {
      const normalizedId = cache.identify({
        id: deletePermissionTemplate.id,
        __typename: 'PermissionTemplate',
      });
      cache.evict({id: normalizedId});
      cache.gc();
    },
  });

  const handleDelete = async (id) => {
    await callDeletePermissionTemplateMutation({
      variables: {
        id,
      },
    });
  };

  return (
    <>
      <Section
        fullHeight
        title="Permission Templates"
        style={{
          marginTop: 0,
        }}
        HeaderRightComp={
          <ModalButton
            size="small"
            variant="contained"
            startIcon={<AddIcon />}
            ModalProps={{
              headerTitle: 'Create Template',
              contentWidth: 1250,
              contentHeight: '96%',
              bodyStyle: {
                backgroundColor: '#f5f6fa',
              },
            }}
            renderContent={(handleClose) => (
              <UserPermissionsTemplateForm onSave={handleClose} />
            )}>
            Create Template
          </ModalButton>
        }
        {...props}>
        <DataTable
          fullHeight
          loading={permissionTemplatesQuery.loading}
          columns={DEFAULT_USER_PERMISSION_TEMPLATE_COLUMNS}
          data={permissionTemplates}
          actionColumn={{
            show: true,
            style: {
              minWidth: 100,
            },
          }}
          renderActionButtons={({item}) => (
            <Box
              style={{
                marginRight: -10,
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <ModalButton
                icon
                size="small"
                // onClick={handleAdd}
                TooltipProps={{
                  title: 'Show Details',
                }}
                ModalProps={{
                  headerTitle: `Edit Template (${item.id})`,
                  contentWidth: 1250,
                  contentHeight: '96%',
                  bodyStyle: {
                    backgroundColor: '#f5f6fa',
                  },
                }}
                renderContent={(handleClose) => (
                  <UserPermissionsTemplateForm
                    item={item}
                    onSave={() => {
                      handleClose();

                      permissionTemplatesQuery.refetch();
                    }}
                  />
                )}>
                <VisibilityIcon
                  sx={{
                    color: 'info.main',
                  }}
                />
              </ModalButton>
              <DialogButton
                icon
                size="small"
                TooltipProps={{
                  title: 'Delete',
                }}
                DialogProps={{
                  title: 'Are you sure?',
                  description: [
                    `You are about to delete "${item.name}" from Permission Templates.`,
                    'This cannot be undone. Do you want to proceed?',
                  ],
                  onActions: (handleClose) => {
                    return [
                      {
                        label: 'Delete',
                        disabledDelay: 3,
                        onClick: async () => {
                          await handleDelete(item.id);
                          handleClose();
                        },
                        startIcon: <DeleteForeverIcon />,
                      },
                    ];
                  },
                }}>
                <DeleteForeverIcon
                  sx={{
                    color: 'grayDarker.main',
                  }}
                />
              </DialogButton>
            </Box>
          )}
        />
      </Section>
      <LoadingIndicator
        modal
        loading={deletePermissionTemplateMutation.loading}
      />
    </>
  );
};

export default UserPermissionsSection;
