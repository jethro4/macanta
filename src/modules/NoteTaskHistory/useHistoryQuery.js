import {useState, useLayoutEffect} from 'react';
import {LIST_NOTES, LIST_TASKS} from '@macanta/graphql/notesTasks';
import useQuery from '@macanta/hooks/apollo/useQuery';
import {removeTagSign} from '@macanta/modules/NoteTaskForms/AddTagsField';
import usePreviousTruthy from '@macanta/hooks/base/usePreviousTruthy';

const useHistoryQuery = (
  contactId,
  page = 0,
  limit = 10,
  filterBy,
  filterVal,
) => {
  const [data, setData] = useState([]);
  const [finalPage, setFinalPage] = useState(null);

  const filter =
    filterVal && filterBy === 'tags' ? removeTagSign(filterVal) : filterVal;

  const notesQuery = useQuery(LIST_NOTES, {
    variables: {
      contactId,
      page: finalPage?.page || page,
      limit: finalPage?.noteLimit || limit,
      ...(filter && {
        filterBy,
        filter,
      }),
    },
  });
  const tasksQuery = useQuery(LIST_TASKS, {
    variables: {
      contactId,
      page: finalPage?.page || page,
      limit: finalPage?.taskLimit || limit,
      ...(filter && {
        filterBy,
        filter,
      }),
    },
  });

  const notesTotal = usePreviousTruthy(notesQuery.data?.listNotes?.total, 0);
  const tasksTotal = usePreviousTruthy(tasksQuery.data?.listTasks?.total, 0);
  const total = notesTotal + tasksTotal;
  const isFinalPage = limit * 2 * (page + 1) > total;

  const loading = Boolean(notesQuery.loading || tasksQuery.loading);
  const error = notesQuery.error || tasksQuery.error;

  useLayoutEffect(() => {
    const notesCount = notesQuery.data?.listNotes?.count || 0;
    const tasksCount = tasksQuery.data?.listTasks?.count || 0;
    const isNotesFinalPage = !!notesQuery.data?.listNotes && notesCount < limit;
    const isTasksFinalPage = !!tasksQuery.data?.listTasks && tasksCount < limit;
    let finalPageVal = {page};

    finalPageVal.noteLimit =
      isTasksFinalPage && !isFinalPage ? limit * 2 - tasksCount : limit;
    finalPageVal.taskLimit =
      isNotesFinalPage && !isFinalPage ? limit * 2 - notesCount : limit;

    setFinalPage(finalPageVal);
  }, [page, data]);

  useLayoutEffect(() => {
    if (
      notesQuery.data?.listNotes?.items &&
      tasksQuery.data?.listTasks?.items
    ) {
      const sortedData = [
        ...(notesQuery.data?.listNotes.items || []),
        ...(tasksQuery.data?.listTasks.items || []),
      ].sort((item1, item2) => {
        if (item1.creationDate < item2.creationDate) {
          return 1;
        } else if (item1.creationDate > item2.creationDate) {
          return -1;
        }

        return 0;
      });

      setData(sortedData);
    }
  }, [notesQuery.data?.listNotes?.items, tasksQuery.data?.listTasks?.items]);

  return {
    data,
    loading,
    error,
    total,
  };
};

export default useHistoryQuery;
