import {useEffect} from 'react';
import moment from 'moment';
import sentryConfig from '@macanta/config/sentryConfig';
import {client} from '@macanta/containers/ApolloProviderWrapper';
import {GET_SESSION_STATUS} from '@macanta/graphql/auth';
import {sessionExpiredVar} from '@macanta/graphql/cache/vars';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {isLoggedIn} from '@macanta/utils/app';
import * as Storage from '@macanta/utils/storage';

class SessionValidateException extends Error {
  constructor(...params) {
    super(...params);
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, SessionValidateException);
    }
    this.name = 'SessionValidateException';
  }
}

const useSessionExpiry = () => {
  useEffect(() => {
    triggerCheck();

    let timeStart = moment();

    const logData = () => {
      if (document.visibilityState !== 'hidden') {
        const hiddenDuration = moment().diff(timeStart, 'second');

        if (hiddenDuration >= 30) {
          triggerCheck();
        }
      } else {
        timeStart = moment();
      }
    };

    if (isLoggedIn()) {
      document.addEventListener('visibilitychange', logData);
    }

    return () => {
      document.removeEventListener('visibilitychange', logData);
    };
  }, []);

  return null;
};

export const triggerCheck = async () => {
  const session = Storage.getItem('session');

  try {
    if (isLoggedIn() && !sessionExpiredVar()) {
      const {data, error} = await client.query({
        query: GET_SESSION_STATUS,
        variables: {
          sessionName: session?.sessionId,
        },
        fetchPolicy: FETCH_POLICIES.NETWORK_ONLY,
      });

      const sessionStatus = data?.getSessionStatus;
      const expired = !!sessionStatus?.expired;

      if (expired && !error?.graphQLErrors?.length) {
        console.info('Session has expired!');

        sessionExpiredVar(true);
      }

      return expired;
    }
  } catch (err) {
    if (!err.networkError) {
      const operationName = 'getSessionStatus';
      const errorType = 'SessionExpired';
      const errTitle = [operationName, errorType, err.message]
        .filter((t) => !!t)
        .join(' - ');

      sentryConfig.captureException({
        err: new SessionValidateException(errTitle),
        operationName,
        errorType,
      });
    }
  }

  return sessionExpiredVar();
};

export default useSessionExpiry;
