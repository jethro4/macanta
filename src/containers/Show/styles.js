import {experimentalStyled as styled} from '@mui/material/styles';
import Box from '@mui/material/Box';

export const Root = styled(Box)`
  height: 100%;
  ${(props) => !props.visible && 'display: none;'}
`;
