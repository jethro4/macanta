import * as yup from 'yup';

const smsValidationSchema = yup.object().shape({
  phoneNumber: yup.string(),
  message: yup.string().required('Required'),
});

export default smsValidationSchema;
