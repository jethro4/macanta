import useValue from '@macanta/hooks/base/useValue';
import useSidebarTabsWithPermission from '@macanta/hooks/contact/useSidebarTabsWithPermission';
import useUserGuides from '@macanta/hooks/useUserGuides';
import {getDefaultUserGuide} from '@macanta/modules/AdminSettings/UserManagement/Tabs/UserGuidesForm/helpers';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';

const useSidebarTabsUserGuides = ({permission}) => {
  const sidebarTabsQuery = useSidebarTabsWithPermission(permission, {
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const {sidebarTabs} = useUserGuides({permission});

  const [guides] = useValue(() => {
    return (
      sidebarTabsQuery.data?.map(({title: subType}) => {
        return (
          sidebarTabs.find((guide) => guide.subType === subType) ||
          getDefaultUserGuide({
            type: 'SidebarTabs',
            subType,
          })
        );
      }) || []
    );
  }, [sidebarTabs, sidebarTabsQuery.data]);

  return {initLoading: sidebarTabsQuery.initLoading, data: guides};
};

export default useSidebarTabsUserGuides;
