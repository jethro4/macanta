import React from 'react';
import Section from '@macanta/containers/Section';

const BodySection = ({style, bodyStyle, children, ...props}) => {
  return (
    <Section
      style={{
        marginTop: 0,
        marginBottom: 0,
        height: '100%',
        ...style,
      }}
      bodyStyle={{
        width: '100%',
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        ...bodyStyle,
      }}
      {...props}>
      {children}
    </Section>
  );
};

export default BodySection;
