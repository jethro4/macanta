import {experimentalStyled as styled} from '@mui/material/styles';

import Box from '@mui/material/Box';
import ButtonComp, {
  IconButton as IconButtonComp,
} from '@macanta/components/Button';
import FormField from '@macanta/containers/FormField';
import {SubSection as SubSectionComp} from '@macanta/containers/Section';
import SelectUserFieldComp from '@macanta/modules/NoteTaskForms/SelectUserField';
import * as applicationStyles from '@macanta/themes/applicationStyles';

export const Root = styled(Box)`
  height: 100%;
  padding: ${({theme}) => theme.spacing(2)} 0;
`;

export const FormRoot = styled(Box)`
  height: 100%;
  display: flex;
  flex-direction: column;
  background-color: #f5f6fa;
`;

export const Body = styled(Box)`
  flex: 1;
  position: relative;
`;

export const SubBody = styled(Box)`
  padding: 1rem 0;
  position: relative;
`;

export const Button = styled(ButtonComp)`
  width: 70px;
`;

export const IconButton = styled(IconButtonComp)`
  color: ${({theme}) => theme.palette.common.white};
  background-color: ${({theme}) => `${theme.palette.primary.main}`};
  width: 2rem;
  height: 2rem;

  &:hover {
    background-color: ${({theme}) => `${theme.palette.secondary.main}`};
  }
`;

export const ConditionIconButton = styled(IconButtonComp)`
  color: ${({disabled}) => (disabled ? '#eee!important' : '#aaa!important')};
  width: 2.5rem;
  height: 2.5rem;
  border-radius: 2.5rem;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const FormsContainer = styled(Box)`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 1rem;

  .MuiInputBase-root .MuiInputBase-input {
    ${applicationStyles.oneLineText}
  }
`;

export const NameField = styled(FormField)`
  max-width: 200px;
`;

export const OperatorField = styled(FormField)`
  max-width: 156px;
  min-width: 100px;
  padding: 1px 0;
`; //TODO: added temporary "padding: 1px 0; to prevent unrendered border lines"

export const ValueField = styled(FormField)`
  min-width: 100px;
  padding: 1px 0;
`; //TODO: added temporary "padding: 1px 0; to prevent unrendered border lines"

export const SelectUserField = styled(SelectUserFieldComp)`
  padding: 1px 0;
`; //TODO: added temporary "padding: 1px 0; to prevent unrendered border lines"

export const TextField = styled(FormField)``;

export const SubSection = styled(SubSectionComp)`
  ${({disabled}) =>
    disabled &&
    `
    display: none;
  `}
`;
