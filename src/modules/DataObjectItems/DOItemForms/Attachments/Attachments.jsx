import React, {useState, useLayoutEffect} from 'react';
import Section from '@macanta/containers/Section';
import {mergeAndAddArraysBySameKeys} from '@macanta/utils/array';
import UploadUrlFormsBtn from './UploadUrlFormsBtn';
import UploadFormsBtn from './UploadFormsBtn';
import AttachmentsViewer from './AttachmentsViewer';
import * as Styled from './styles';
import * as Storage from '@macanta/utils/storage';

const Attachments = ({
  loading,
  data,
  selectedValueAttachments,
  doItemId,
  onChange,
  style,
  ...props
}) => {
  const [attachments, setAttachments] = useState([]);

  const handleSuccess = (files, type) => {
    onChange(files, {
      type,
    });

    setAttachments((state) => state.concat(files));
  };

  const handleDelete = (fileId) => (event) => {
    event.stopPropagation();

    onChange({id: fileId}, {type: 'delete'});

    setAttachments((state) =>
      state.filter((attachment) => fileId !== attachment.id),
    );
  };

  useLayoutEffect(() => {
    setAttachments(data);
  }, [data]);

  useLayoutEffect(() => {
    const userDetails = Storage.getItem('userDetails');

    const mergedAttachments = mergeAndAddArraysBySameKeys(
      data,
      selectedValueAttachments.map((n) => ({
        ...n,
        ...(!n.createdBy &&
          !!n.tempId && {
            createdBy: `${userDetails.firstName || ''} ${
              userDetails.lastName || ''
            }`.trim(),
          }),
      })),
      {
        keysToCheck: ['id'],
        includeAll: true,
      },
    ).filter(({deleted}) => !deleted);

    setAttachments(mergedAttachments);
  }, [data, selectedValueAttachments]);

  return (
    <Section
      loading={loading}
      fullHeight
      style={{
        marginTop: 0,
        ...style,
      }}
      title="Attachments"
      HeaderRightComp={
        <Styled.HeaderRightContainer>
          <UploadUrlFormsBtn
            style={{
              marginRight: '1rem',
            }}
            onSuccess={handleSuccess}
          />
          <UploadFormsBtn onSuccess={handleSuccess} />
        </Styled.HeaderRightContainer>
      }
      {...props}>
      <AttachmentsViewer
        loading={loading}
        attachments={attachments}
        doItemId={doItemId}
        onDelete={handleDelete}
      />
    </Section>
  );
};

Attachments.defaultProps = {
  data: [],
  selectedValueAttachments: [],
};

export default Attachments;
