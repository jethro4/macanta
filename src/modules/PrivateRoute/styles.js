import {experimentalStyled as styled} from '@mui/material/styles';
import Box from '@mui/material/Box';

export const Root = styled(Box)`
  height: 100%;
  display: flex;
  flex-direction: column;

  & > div:first-child {
    display: flex;
    flex-direction: column;
    flex: 1;
  }
`;
