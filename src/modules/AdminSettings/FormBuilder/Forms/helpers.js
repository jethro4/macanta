import uniq from 'lodash/uniq';
import isEqual from 'lodash/isEqual';
import {
  MIME_TYPES_WITH_EXTENSION,
  ALL_MIME_TYPES,
  ALL_EXTENSIONS,
  ALL_MIME_TYPE_CATEGORIES,
  ALL_CATEGORIES,
} from '@macanta/constants/form';
import {sortArray} from '@macanta/utils/array';

export const validateLabelExists = ({fieldId, label = '', groups = []}) => {
  const exists = groups.some((group) => {
    return group.fields.some(
      (field) =>
        (!fieldId || field.fieldId !== fieldId) &&
        field.label &&
        field.label.toLowerCase().trim() === label.toLowerCase().trim(),
    );
  }, []);

  return exists ? 'Label is already used' : '';
};

export const getMimeTypes = (category = 'all') => {
  let mimeTypes = [];

  if (category === 'image') {
    mimeTypes = Object.keys(MIME_TYPES_WITH_EXTENSION['image']);
  } else if (category === 'document') {
    mimeTypes = [
      ...Object.keys(MIME_TYPES_WITH_EXTENSION['text']),
      ...Object.keys(MIME_TYPES_WITH_EXTENSION['application']),
    ];
  } else if (category === 'media') {
    mimeTypes = [
      ...Object.keys(MIME_TYPES_WITH_EXTENSION['audio']),
      ...Object.keys(MIME_TYPES_WITH_EXTENSION['video']),
    ];
  } else {
    mimeTypes = ALL_MIME_TYPES;
  }

  return mimeTypes;
};

export const getMimeTypeExtension = (mimeType) => {
  const mimeTypeValues = Object.values(MIME_TYPES_WITH_EXTENSION);

  for (let i = 0; i < mimeTypeValues.length; i++) {
    const mimeTypeValue = mimeTypeValues[i];
    const extensions = mimeTypeValue[mimeType];

    if (extensions) {
      return extensions[0];
    }
  }
};

export const getMimeCategory = (mimeTypes = []) => {
  const categoryMapping = mimeTypes.reduce((acc, mimeType) => {
    const accObj = {...acc};
    const [category] = mimeType.split('/');

    accObj[category] = true;

    return accObj;
  }, {});
  const categories = Object.keys(categoryMapping);

  return getCategoryByCombinations(categories);
};

export const getCategoryExtensions = (category) => {
  const categoryArr = [];
  const categoryExtensions = Object.values(MIME_TYPES_WITH_EXTENSION[category]);

  categoryExtensions.forEach((extensions) => {
    extensions.forEach((ext) => {
      if (!categoryArr.includes(ext)) {
        categoryArr.push(ext);
      }
    });
  });

  return categoryArr;
};

export const getCategories = (category = 'all') => {
  let categories = [];

  if (category === 'image') {
    categories = ['image'];
  } else if (category === 'document') {
    categories = ['text', 'application'];
  } else if (category === 'media') {
    categories = ['audio', 'video'];
  } else {
    categories = ALL_CATEGORIES;
  }

  return categories;
};

export const getDerivedCategory = (categoriesArg) => {
  let categories = categoriesArg || [];
  let derivedCategory;

  if (isEqual(sortArray(categories), sortArray(['image']))) {
    derivedCategory = 'image';
  } else if (
    isEqual(sortArray(categories), sortArray(['text', 'application']))
  ) {
    derivedCategory = 'document';
  } else if (isEqual(sortArray(categories), sortArray(['audio', 'video']))) {
    derivedCategory = 'media';
  } else {
    derivedCategory = 'all';
  }

  return derivedCategory;
};

export const getExtensions = (category = 'all') => {
  let extensions = [];

  if (category === 'image') {
    extensions = getCategoryExtensions('image');
  } else if (category === 'document') {
    extensions = uniq([
      ...getCategoryExtensions('text'),
      ...getCategoryExtensions('application'),
    ]);
  } else if (category === 'media') {
    extensions = uniq([
      ...getCategoryExtensions('audio'),
      ...getCategoryExtensions('video'),
    ]);
  } else {
    extensions = ALL_EXTENSIONS;
  }

  return extensions;
};

export const getExtensionCategory = (extensions = []) => {
  const categories = ALL_MIME_TYPE_CATEGORIES.reduce(
    (acc, {value: category}) => {
      const categoryExtensions = getExtensions(category);

      if (
        !acc.includes(category) &&
        categoryExtensions.some((ext) => extensions.includes(ext))
      ) {
        return acc.concat(category);
      }

      return acc;
    },
    [],
  );

  return getCategoryByCombinations(categories);
};

export const getCategoryByCombinations = (categories) => {
  if (categories.length !== 1) {
    return 'all';
  }

  return categories[0];
};

export const transformMergeField = (str = '', hasDefault) => {
  const strArr = str.split('.');
  if (['Sender', 'Contact', 'CD'].includes(strArr[0])) {
    return `~${str}~`;
  }

  return hasDefault ? str : '';
};

export const transformMergeFields = (arr = [], hasDefault) => {
  return arr
    .map((str) => transformMergeField(str, hasDefault))
    .filter((str) => !!str);
};

export const getMergeFields = (str = '') => {
  const strArr = getDisplayFields(str);

  return transformMergeFields(strArr);
};

export const getDisplayFields = (str = '') => {
  const strArr = str.split('~').filter((item) => !!item);

  return strArr.map((item) => item.trim());
};

export const transformDisplayFields = (arr = []) => {
  return arr.map((str) => transformMergeField(str, true)).join(' ');
};

export const transformDisplayFieldsWithConditions = (arr = []) => {
  return arr.map((str) => getDisplayFields(str)[0]).filter((str) => !!str);
};
