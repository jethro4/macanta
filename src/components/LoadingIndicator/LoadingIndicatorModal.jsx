import React, {forwardRef} from 'react';
import Modal from '@mui/material/Modal';
import LoadingIndicator from './LoadingIndicator';

let LoadingIndicatorModal = ({ModalProps, loading, ...props}, ref) => (
  <Modal ref={ref} open={loading} {...ModalProps}>
    <LoadingIndicator fill transparent color="white" {...props} />
  </Modal>
);

LoadingIndicatorModal = forwardRef(LoadingIndicatorModal);

export default LoadingIndicatorModal;
