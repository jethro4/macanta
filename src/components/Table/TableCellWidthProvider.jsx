import React, {useState} from 'react';
import TableContext from './TableContext';

const TableCellWidthProvider = ({children}) => {
  const [resizedCellWidths, setResizedCellWidths] = useState({});

  return (
    <TableContext.Provider
      value={{
        ...resizedCellWidths,
        setResizedCellWidths,
      }}>
      {children}
    </TableContext.Provider>
  );
};

export default TableCellWidthProvider;
