import React from 'react';
import {sortArrayByPriority} from '@macanta/utils/array';
import Container from './Container';

const Custom = ({children, transitions, animations, style, ...props}) => {
  const customStyles = useCSS([
    {
      attributeName: 'transition',
      value: transitions,
    },
    {
      attributeName: 'animation',
      value: animations,
    },
  ]);

  return (
    <Container
      style={{
        ...style,
        ...customStyles,
      }}
      {...props}>
      {children}
    </Container>
  );
};

const transformAttributeValues = (item, attributes) => {
  const valueKeys = sortArrayByPriority(Object.keys(item), null, attributes);
  const valuesArr = valueKeys.reduce((acc, key) => {
    const value = item[key];
    const accArr = [...acc];

    if (value) {
      accArr.push(value);
    }

    return accArr;
  }, []);

  return valuesArr.join(' ');
};

const transformCSS = (value, attributes) => {
  return value
    .map((item) => transformAttributeValues(item, attributes))
    .join(',');
};

const useCSS = (cssAttributes) => {
  return cssAttributes
    .filter(({value}) => !!value)
    .reduce((acc, {attributeName, value}) => {
      const accObj = {...acc};

      let transformedValue;

      switch (attributeName) {
        case 'transition': {
          transformedValue = transformCSS(value, [
            'property',
            'duration',
            'timingFunction',
            'delay',
          ]);
          break;
        }
        case 'animation': {
          transformedValue = transformCSS(value, [
            'name',
            'duration',
            'timingFunction',
            'delay',
          ]);
          break;
        }
        default: {
          transformedValue = value;
        }
      }

      accObj[attributeName] = transformedValue;

      return accObj;
    }, {});
};

Custom.defaultProps = {
  transitions: [],
  animations: [],
};

export default Custom;
