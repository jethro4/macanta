import {useState, useMemo, useCallback} from 'react';

const request = Object.freeze({
  get: async (url) => {
    const response = await fetch(url);

    return response.json();
  },
  post: async (url, body) => {
    const response = await fetch(url, {
      method: 'POST',
      body,
    });

    return response.json();
  },
});

const useHttp = ({onCompleted, onError, transformResponse}) => {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState();
  const [error, setError] = useState();

  const handleApiCall = useCallback(async (apiCallback) => {
    setLoading(true);

    try {
      let responseData = await apiCallback();

      if (transformResponse) {
        responseData = transformResponse(responseData);
      }

      onCompleted && onCompleted(responseData);
      setData(responseData);

      return responseData;
    } catch (e) {
      setError(e);
      onError && onError(e);
    } finally {
      setLoading(false);
    }
  }, []);

  const http = useMemo(
    () => ({
      get: (url) => handleApiCall(() => request.get(url)),
      post: (url, body) => handleApiCall(() => request.post(url, body)),
    }),
    [handleApiCall],
  );

  return {http, loading, data, error};
};

export default useHttp;
