import {gql} from '@apollo/client';

export default gql`
  mutation deleteQueryBuilder(
    $deleteQueryBuilderInput: DeleteQueryBuilderInput!
  ) {
    deleteQueryBuilder(input: $deleteQueryBuilderInput) {
      success
    }
  }
`;
