import React from 'react';
import TurnedInIcon from '@mui/icons-material/TurnedIn';
import Box from '@mui/material/Box';
import BasicInfoForm from '@macanta/modules/AdminSettings/Automation/TriggerActions/SubForms/BasicInfoForm';
import FlexGrid from '@macanta/components/Base/FlexGrid';
import withForm from '@macanta/components/Form/withForm';
import ActionTypesSection from '@macanta/modules/AdminSettings/Automation/TriggerActions/ActionTypesSection';
import CriteriaFiltersForm from '@macanta/modules/AdminSettings/Automation/CriteriaFiltersForm';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import FormField from '@macanta/containers/FormField';
import Button from '@macanta/components/Button';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {
  addIdToItems,
  getActions,
} from '@macanta/modules/AdminSettings/Automation/TriggerActions/helpers';

let TriggerActionForm = (props) => {
  const {values, setValues, dirty, handleSubmit, setFieldValue} = props;

  const handleChangeQuery = (val) => {
    setValues((state) => ({...state, ...val}));
  };

  const handleChangeDOType = (val, item) => {
    setFieldValue('doTitle', item.title);
  };

  const doTypesQuery = useDOTypes({
    removeEmail: true,
    includeContactObject: true,
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  return (
    <Box
      sx={{
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
      }}>
      <FlexGrid
        cols={2}
        margin={[0, '1.5rem']}
        // border={[0, 1]}
        sx={{
          flex: 1,
          padding: '1rem',
        }}>
        <Box colWidth={0.75}>
          <BasicInfoForm
            actionLabel="Trigger Action Name"
            renderOtherForms={() => (
              <FormField
                name="groupId"
                onChange={handleChangeDOType}
                loading={doTypesQuery.initLoading}
                type="Select"
                options={doTypesQuery.result}
                labelKey="title"
                valueKey="id"
                label="Data Object Type"
                fullWidth
                size="small"
                variant="outlined"
              />
            )}
          />
        </Box>

        <Box
          height="100%"
          sx={{
            display: 'flex',
            flexDirection: 'column',
          }}>
          <ActionTypesSection />

          <CriteriaFiltersForm
            hideContactConditions={values.groupId === 'co_customfields'}
            filters={{
              doConditions: values.doConditions,
              contactConditions: values.contactConditions,
            }}
            groupId={values.groupId}
            onChange={handleChangeQuery}
            DOConditionProps={{
              title: 'Field Values',
              noLogic: true,
              placeholder: 'Leave blank to use current value',
            }}
          />
        </Box>
      </FlexGrid>

      <Box
        style={{
          display: 'flex',
          justifyContent: 'flex-end',
          padding: '12px 1rem',
          borderTop: '1px solid #eee',
          boxShadow: '0px -4px 10px -2px #eee',
        }}>
        <Button
          disabled={!dirty}
          variant="contained"
          startIcon={<TurnedInIcon />}
          onClick={handleSubmit}
          size="medium">
          Save
        </Button>
      </Box>
    </Box>
  );
};

TriggerActionForm = withForm(TriggerActionForm, {
  mapPropsToValues: (props) => {
    const initialValues = {
      ...props.initialValues,
    };

    const actions = initialValues.actions || getActions();

    return {
      ...initialValues,
      groupId: initialValues.groupId || 'co_customfields',
      doTitle: initialValues.doTitle || 'Contact Object',
      doConditions: initialValues.doConditions
        ? addIdToItems(initialValues.doConditions)
        : [],
      contactConditions: initialValues.contactConditions
        ? addIdToItems(initialValues.contactConditions)
        : [],
      actions,
    };
  },
});

export default TriggerActionForm;
