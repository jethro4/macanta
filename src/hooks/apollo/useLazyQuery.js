import {useRef} from 'react';
import * as apollo from '@apollo/client';
import useCleanUp from '@macanta/hooks/base/useCleanUp';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import {createAbortController, getTagFieldName} from '@macanta/utils/graphql';

const useLazyQuery = (graphqlTag, optionsArg) => {
  const {displayMessage} = useProgressAlert();

  const abortFuncRef = useRef();

  const {
    errorMessage,
    alertOnError,
    onCompleted,
    onError,
    ...options
  } = Object.assign(
    {
      alertOnError: false,
    },
    optionsArg,
  );
  const tagFieldName = getTagFieldName(graphqlTag);

  const [callQuery, query] = apollo.useLazyQuery(graphqlTag, {
    ...options,
    skip: true,
    onCompleted(data) {
      const result = data[tagFieldName];

      onCompleted?.({...data, result});
    },
    onError(err) {
      console.error('Error on useLazyQuery', err);

      if (alertOnError) {
        displayMessage({
          message: errorMessage || err.message,
          severity: 'error',
        });
      }

      onError?.(err);
    },
  });

  const handleCallQuery = (opts) => {
    const controller = createAbortController();

    abortFuncRef.current = () => controller.abort();

    callQuery({
      ...opts,
      context: {
        removeAppInfo: true,
        fetchOptions: {signal: controller.signal},
        queryDeduplication: false,
        ...opts.context,
      },
    });
  };

  const abortQuery = () => {
    abortFuncRef.current?.();
  };

  useCleanUp(() => {
    abortQuery();
  });

  const data = query.data?.[tagFieldName];

  return [handleCallQuery, {...query, result: data, abort: abortQuery}];
};

export default useLazyQuery;
