import {gql} from '@apollo/client';

export default gql`
  mutation email($emailInput: EmailInput!) {
    email(input: $emailInput) {
      success
    }
  }
`;
