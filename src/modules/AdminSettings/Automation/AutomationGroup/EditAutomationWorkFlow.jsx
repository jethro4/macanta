import React from 'react';
import AddIcon from '@material-ui/icons/Add';
import {PopoverButton} from '@macanta/components/Button';
// import LoadingIndicator from '@macanta/components/LoadingIndicator';
import Section from '@macanta/containers/Section';
import DataTable from '@macanta/containers/DataTable';
import AutomationGroupForm from '@macanta/modules/AdminSettings/Automation/AutomationGroup/AutomationGroupForm';
import Breadcrumbs from '@macanta/components/Breadcrumbs';
import {ROOT_PATH_AUTOMATION} from '@macanta/modules/AdminSettings/Automation/Automation';
import ItemActionButtons from '@macanta/modules/AdminSettings/Automation/TriggerActions/ItemActionButtons';
import Switch from '@macanta/components/Forms/ChoiceFields/Switch';
import {
  useAutomationGroupList,
  useDeleteAutomationsMutation,
  usePostAutomationGroupMutation,
} from '@macanta/hooks/automation';
import {writeItems} from '@macanta/graphql/helpers';
import {AUTOMATION_GROUP_FRAGMENT} from '@macanta/graphql/automation/GET_AUTOMATION_GROUPS';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import {generateUniqueID} from '@macanta/utils/string';

const COLUMNS = [
  {
    id: 'name',
    label: 'Name',
  },
  {
    id: 'description',
    label: 'Description',
    maxWidth: '40%',
  },
  {
    id: 'conditionsActions',
    label: 'Automation Set',
    minWidth: 100,
    maxWidth: 100,
    transform: ({value}) => value?.length,
  },
];

const EditAutomationWorkFlow = (props) => {
  const automationGroupQuery = useAutomationGroupList({
    transformResult: (result) =>
      result?.map((item) => ({
        ...item,
        conditionsActions: item.conditionsActions.map((automationSet) => ({
          id: generateUniqueID(),
          ...automationSet,
        })),
      })),
  });

  const [callPostMutation, actionMutation] = usePostAutomationGroupMutation({
    update(cache, {result}, options) {
      writeItems({
        isCreate: !options.variables.input?.id,
        item: result,
        typeName: 'AutomationGroup',
        fragment: AUTOMATION_GROUP_FRAGMENT,
        listKey: 'getAutomationGroups',
        position: 'first',
      });
    },
  });

  const [callDeleteMutation, deleteMutation] = useDeleteAutomationsMutation({
    typeName: 'AutomationGroup',
  });

  const handleChangeStatus = (id, checked) => {
    console.debug('handleChangeStatus:', id, checked);
  };

  const handleSave = async (values) => {
    const variables = {
      id: values.id,
      name: values.name,
      description: values.description,
      active: values.active,
      conditionsActions: values.conditionsActions.map((item) => ({
        actionId: item.actionId,
        conditionId: item.conditionId,
        active: item.active,
        revalidate: item.revalidate,
        waitFor: parseInt(item.waitFor),
        unit: item.unit,
        time: item.time,
      })),
    };

    return callPostMutation({
      variables,
    });
  };

  const handleDelete = (id) => {
    return callDeleteMutation({
      variables: {
        id,
      },
    });
  };

  return (
    <>
      <Section
        fullHeight
        HeaderLeftComp={
          <Breadcrumbs
            title="Edit Automation Workflow"
            backTitle="Automation Settings"
            backPath={ROOT_PATH_AUTOMATION}
          />
        }
        HeaderRightComp={
          <PopoverButton
            size="small"
            variant="contained"
            startIcon={<AddIcon />}
            PopoverProps={{
              title: 'Add Automation Group',
              contentWidth: 1400,
              contentMinHeight: 200,
              bodyStyle: {
                backgroundColor: '#f5f6fA',
              },
            }}
            renderContent={(handleClose) => (
              <AutomationGroupForm onClose={handleClose} />
            )}>
            Add Automation Group
          </PopoverButton>
        }
        {...props}>
        <DataTable
          fullHeight
          loading={automationGroupQuery.loading}
          columns={COLUMNS}
          data={automationGroupQuery.result}
          actionColumn={{
            show: true,
            frontLabel: 'Active?',
            width: 50,
          }}
          renderFrontActionButtons={({item}) => (
            <Switch
              color="success"
              checked={item.active}
              onChange={(event) => {
                handleChangeStatus(item.id, event.target.checked);
              }}
            />
          )}
          renderActionButtons={({item}) => (
            <ItemActionButtons
              containerProps={{
                contentWidth: 1400,
                contentMinHeight: 200,
              }}
              modal
              showDetails
              type="Automation Groups"
              item={item}
              onDelete={handleDelete}
              renderFormComponent={(handleClose) => (
                <AutomationGroupForm
                  initialValues={item}
                  onSubmit={async (values) => {
                    const {result} = await handleSave(values);

                    if (result) {
                      handleClose();
                    }
                  }}
                />
              )}
            />
          )}
        />
      </Section>
      <LoadingIndicator
        modal
        loading={actionMutation.loading || deleteMutation.loading}
      />
    </>
  );
};

export default EditAutomationWorkFlow;
