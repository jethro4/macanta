import useQuery from '@macanta/hooks/apollo/useQuery';
import {GET_CONTACT_ACTIONS} from '@macanta/graphql/automation';
import {removeTagSign} from '@macanta/modules/NoteTaskForms/AddTagsField';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import {findItem} from '@macanta/utils/array';

const FIELD_KEY_MAPPING = {
  queryId: 'id',
  queryType: 'type',
  queryName: 'name',
  queryDescription: 'description',
  queryConnectedDataType: 'doTitle',
  queryContactRelationship: 'contactRelationship',
  actionType: 'actionType',
  noteTitle: 'noteTitle',
  noteTags: 'noteTags',
  noteText: 'noteText',
  taskTitle: 'taskTitle',
  taskText: 'taskText',
  taskDate: 'actionDate',
  assignType: 'assignType',
  assignTypeValue: 'assignTypeValue',
};

const useContactActions = (options) => {
  const doTypesQuery = useDOTypes({
    removeEmail: true,
    includeContactObject: true,
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const query = useQuery(GET_CONTACT_ACTIONS, {
    keyMapping: FIELD_KEY_MAPPING,
    transformResult: (result) =>
      result?.items.map((item) => {
        const isNote = item.actionType === 'Note';
        const doType = findItem(
          doTypesQuery.result,
          (d) => d.title === item.doTitle,
        );

        let actionTypeKeys = isNote
          ? {
              title: item.noteTitle,
              tags: item.noteTags?.split(',').map(removeTagSign),
              note: item.noteText,
            }
          : {
              title: item.taskTitle,
              note: item.taskText,
            };

        return {
          ...item,
          ...actionTypeKeys,
          groupId: doType?.id,
        };
      }),
    dependencies: [doTypesQuery.result],
    ...options,
  });

  return query;
};

export default useContactActions;
