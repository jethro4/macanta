import React from 'react';
import RawHTML from '@macanta/components/RawHTML';
import useAccessPermissions from '@macanta/hooks/admin/useAccessPermissions';
import useSessionExpiryOnce from '@macanta/hooks/admin/useSessionExpiryOnce';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {getAppInfo} from '@macanta/utils/app';
import * as Storage from '@macanta/utils/storage';

const LoadUsetiful = () => {
  const userDetails = Storage.getItem('userDetails');

  const [appName] = getAppInfo();

  const accessPermissionsQuery = useAccessPermissions({
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });
  const querySessionExpiry = useSessionExpiryOnce();

  const userLevel = accessPermissionsQuery.data?.isAdmin ? 'Admin' : 'User';

  return querySessionExpiry.loading || querySessionExpiry.expired ? null : (
    <RawHTML
      options={{
        parentSelector: 'body',
        cleanUpScripts: true,
      }}>{`
      <script>
        window.usetifulTags = { 
          userId : "${appName + '-' + userDetails.userId}",
          userLevel : "${userLevel}",
          loginStatus: "Logged-In"
        };
      </script>
      <script>
        (function (w, d, s) {
          var a = d.getElementsByTagName('head')[0];
          var r = d.createElement('script');
          r.async = 1;
          r.src = s;
          r.setAttribute('id', 'usetifulScript');
          r.dataset.token = "5e33de4d66eed49c1fc4367973eb2e7b";
                  a.appendChild(r);
        })(window, document, "https://www.usetiful.com/dist/usetiful.js");
      </script>
    `}</RawHTML>
  );
};

export default LoadUsetiful;
