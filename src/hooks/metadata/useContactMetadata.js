import useQuery from '@macanta/hooks/apollo/useQuery';
import {GET_CONTACT_METADATA} from '@macanta/graphql/contacts';

const useContactMetadata = ({email, ...options}) => {
  const {data, loading} = useQuery(GET_CONTACT_METADATA, {
    variables: {
      email,
    },
    ...options,
  });

  return {data, loading};
};

export default useContactMetadata;
