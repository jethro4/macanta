import {gql} from '@apollo/client';

export default gql`
  mutation deleteContact($deleteContactInput: DeleteContactInput!) {
    deleteContact(input: $deleteContactInput) {
      success
    }
  }
`;
