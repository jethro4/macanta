import React from 'react';
import Form from '@macanta/components/Form';
import UniversalSearch from '@macanta/modules/UniversalSearch';
import * as ContactFormStyled from '@macanta/modules/ContactForms/styles';
import * as ConnectAnotherContactFormsStyled from '@macanta/modules/DataObjectItems/DOItemForms/Relationships/ConnectAnotherContactForms/styles';

const DEFAULT_COLUMNS = [
  {id: 'FullName', label: 'Name', minWidth: 170},
  {id: 'Email', label: 'Email', minWidth: 170},
];

const SingleContactSearch = ({selectedContactIds, onSuccess, ...props}) => {
  const initValues = {
    contact: undefined,
  };

  const handleFormSubmit = ({contact}) => {
    onSuccess(contact);
  };

  return (
    <ConnectAnotherContactFormsStyled.Root {...props}>
      <Form initialValues={initValues} onSubmit={handleFormSubmit}>
        {({values, handleSubmit, setFieldValue}) => (
          <>
            <UniversalSearch
              style={{
                marginBottom: '1rem',
                padding: '0 1rem',
              }}
              autoFocus
              autoSearch
              hideSearchAndAdd
              defaultSearchQuery={{search: '', page: 0, limit: 10}}
              onSelectItem={(contact) => setFieldValue('contact', contact)}
              disableCategories
              disableSuggestions
              columns={DEFAULT_COLUMNS}
              TableProps={{
                noOddRowStripes: true,
                highlight: true,
                hidePagination: true,
                disabledIds: selectedContactIds,
              }}
            />
            <ContactFormStyled.FloatingButton
              style={{
                zIndex: 11,
                width: 90,
              }}
              disabled={!values.contact}
              variant="contained"
              onClick={handleSubmit}
              size="medium">
              Select
            </ContactFormStyled.FloatingButton>
          </>
        )}
      </Form>
    </ConnectAnotherContactFormsStyled.Root>
  );
};

export default SingleContactSearch;
