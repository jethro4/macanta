import React from 'react';
import DOColumnsWithFieldsAndRelationships from './DOColumnsWithFieldsAndRelationships';

const DOTypeWithFields = ({groupId, items: doItems, ...props}) => {
  return (
    <DOColumnsWithFieldsAndRelationships
      groupId={groupId}
      items={doItems}
      {...props}
    />
  );
};

DOTypeWithFields.defaultProps = {
  items: [],
};

export default DOTypeWithFields;
