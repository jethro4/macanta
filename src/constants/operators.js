export const OPERATORS = Object.freeze([
  {
    label: 'contains',
    value: 'contains',
  },
  {
    label: 'is',
    value: 'is',
  },
  {
    label: 'is not',
    value: 'is not',
  },
  {
    label: 'is null',
    value: 'is null',
  },
  {
    label: 'not null',
    value: 'not null',
  },
  {
    label: 'is greater than',
    value: '~>~',
  },
  {
    label: 'is less than',
    value: '~<~',
  },
]);

export const DEFAULT_OPERATOR = 'contains';
export const DEFAULT_USER_OPERATOR = 'is';
