import useQuery from '@macanta/hooks/apollo/useQuery';
import {GET_ACCESS_PERMISSIONS} from '@macanta/graphql/permissions';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import * as Storage from '@macanta/utils/storage';

const useIsAdmin = () => {
  const userDetails = Storage.getItem('userDetails');

  const {data} = useQuery(GET_ACCESS_PERMISSIONS, {
    variables: {
      email: userDetails?.email,
    },
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
    skip: !userDetails?.email,
    onError() {},
  });

  const isAdmin = data?.getAccessPermissions?.isAdmin;

  return isAdmin;
};

export default useIsAdmin;
