import {client} from '@macanta/containers/ApolloProviderWrapper';
import {asyncPromises} from '@macanta/utils/promise';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {queriesSessionActiveVar} from '@macanta/graphql/cache/sessionVars';
import {readQuery, resetQueriesCache} from '@macanta/graphql/helpers';
import {removeUndefinedValues} from '@macanta/utils/object';

export const getTagFieldName = (graphqlTag) =>
  graphqlTag.definitions?.[0]?.name.value;

export const generateQueueId = (variables) => JSON.stringify(variables || {});

export const addSessionQuery = (val, tagFieldName) => {
  queriesSessionActiveVar({
    ...queriesSessionActiveVar(),
    [tagFieldName]: val,
  });
};

export const transformArgsAndResetCache = (sessionQueries, tagFieldName) => {
  const dependencies = sessionQueries[tagFieldName].data;

  dependencies.forEach((jsonArgs) => {
    const args = JSON.parse(jsonArgs);

    resetQueriesCache({fieldName: tagFieldName, args});
  });
};

export const clearSessionQuery = (tagFieldName, hardClear) => {
  let allActiveSessionQueries = {...queriesSessionActiveVar()};

  if (tagFieldName) {
    if (hardClear) {
      transformArgsAndResetCache(allActiveSessionQueries, tagFieldName);
    }

    delete allActiveSessionQueries[tagFieldName];
  } else {
    if (hardClear) {
      const fieldNames = Object.keys(allActiveSessionQueries);

      fieldNames.forEach((fieldName) => {
        transformArgsAndResetCache(allActiveSessionQueries, fieldName);
      });
    }

    allActiveSessionQueries = {};
  }

  queriesSessionActiveVar(allActiveSessionQueries);
};

export const getQueueInfo = ({graphqlTag, options}) => {
  const queue = queriesSessionActiveVar();
  const tagFieldName = getTagFieldName(graphqlTag);
  const queueId = generateQueueId(options?.variables);
  const id = options?.dependencies?.join('-');
  const queueVal = {id, data: [], ...queue[tagFieldName]};
  const isNew = !queue[tagFieldName]?.id;
  const isSameSessionQueryId = !isNew && queueVal?.id === id;

  return {
    id,
    tagFieldName,
    queueVal,
    queueId,
    isSameSessionQueryId,
  };
};

export const getActiveSession = ({graphqlTag, options}) => {
  const {queueVal, queueId, isSameSessionQueryId} = getQueueInfo({
    graphqlTag,
    options,
  });

  const isActiveSession =
    isSameSessionQueryId && queueVal?.data?.includes(queueId);

  return isActiveSession;
};

export const addActiveQuerySession = ({graphqlTag, options}) => {
  const {
    id,
    tagFieldName,
    queueVal,
    queueId,
    isSameSessionQueryId,
  } = getQueueInfo({
    graphqlTag,
    options,
  });
  const isActiveSession = getActiveSession({graphqlTag, options});

  if (isSameSessionQueryId) {
    if (!isActiveSession) {
      addSessionQuery(
        {
          id,
          data: queueVal?.data?.concat(queueId),
        },
        tagFieldName,
      );
    }
  } else {
    addSessionQuery(
      {
        id,
        data: [queueId],
      },
      tagFieldName,
    );
  }
};

export const getQueryFetchPolicy = ({graphqlTag, options}) => {
  const isActiveSession = getActiveSession({graphqlTag, options});

  const fetchPolicy =
    options?.fetchPolicy ||
    (isActiveSession
      ? FETCH_POLICIES.CACHE_FIRST
      : FETCH_POLICIES.CACHE_AND_NETWORK);

  return fetchPolicy;
};

const inlineFetchPolicies = [
  FETCH_POLICIES.CACHE_ONLY,
  FETCH_POLICIES.CACHE_FIRST,
  FETCH_POLICIES.NETWORK_ONLY,
];

export const query = (graphqlTag, options) => {
  const fetchPolicy = inlineFetchPolicies.includes(options?.fetchPolicy)
    ? options.fetchPolicy
    : FETCH_POLICIES.NETWORK_ONLY;
  const queryDeduplication = fetchPolicy !== FETCH_POLICIES.NETWORK_ONLY;

  return client.query({
    ...options,
    context: {
      queryDeduplication,
      ...options?.context,
    },
    query: graphqlTag,
    fetchPolicy,
  });
};

export const batchQuery = (graphqlTag, optionsArr, cacheOnly) => {
  const controllers = optionsArr.map(createAbortController);

  const batchPromise = asyncPromises(optionsArr, async (options, index) => {
    const {data} = await query(
      graphqlTag,
      Object.assign(
        {
          context: {
            fetchOptions: {signal: controllers[index].signal},
            ...options?.context,
          },
        },
        options,
        cacheOnly && {
          fetchPolicy: FETCH_POLICIES.CACHE_ONLY,
        },
      ),
    );

    const tagFieldName = getTagFieldName(graphqlTag);
    const result = data[tagFieldName];

    return removeUndefinedValues(result);
  });

  const abortAll = () =>
    controllers.forEach((controller) => controller.abort());

  return {promise: batchPromise, abortAll};
};

export const getBatchResult = (graphqlTag, options, cacheOnly) => {
  const optionsArr =
    options?.variablesArr?.map((variables) => ({
      variables,
      fetchPolicy: getQueryFetchPolicy({
        graphqlTag,
        options: {
          ...options,
          variables,
        },
      }),
    })) || [];

  const response = batchQuery(graphqlTag, optionsArr, cacheOnly);

  return response;
};

export const getBatchCache = (graphqlTag, options) => {
  const batchResult =
    options?.variablesArr?.map((variables) => {
      return readQuery({graphqlTag, variables});
    }) || [];

  return removeUndefinedValues(batchResult);
};

export const createAbortController = () => {
  const abortController = new AbortController();

  return abortController;
};
