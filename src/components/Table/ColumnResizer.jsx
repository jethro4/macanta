import React, {useEffect, useRef, useCallback} from 'react';
import throttle from 'lodash/throttle';
import * as Styled from './styles';
import withShow from '@macanta/hoc/withShow';

let ColumnResizer = ({onStartSlide, onSliding, onEndSlide, disabled}) => {
  const boxRef = useRef();
  const startXRef = useRef();
  const prevClientXRef = useRef();

  const handleStartSlide = (event) => {
    cleanup();

    startXRef.current = event.clientX;

    document.body.addEventListener(
      'mousemove',
      handleSliding,
      passiveEventSupported,
    );
    document.body.addEventListener(
      'mouseup',
      handleEndSlide,
      passiveEventSupported,
    );

    onStartSlide();
  };

  const handleSliding = useCallback(
    throttle(
      (event) => {
        const clientX = event.clientX;

        if (clientX !== prevClientXRef.current) {
          prevClientXRef.current = clientX;

          const deltaX = clientX - startXRef.current;

          onSliding(deltaX);
        }
      },
      30,
      {leading: true, trailing: true},
    ),
    [],
  );

  const handleEndSlide = useCallback(() => {
    cleanup();

    onEndSlide();

    window.dispatchEvent(new Event('resize'));
  }, []);

  const cleanup = () => {
    document.body.removeEventListener('mousemove', handleSliding);
    document.body.removeEventListener('mouseup', handleEndSlide);
  };

  useEffect(() => {
    cleanup();

    return cleanup;
  }, []);

  return (
    <Styled.ColumnResizerContainer
      ref={boxRef}
      disabled={disabled}
      onMouseDown={handleStartSlide}>
      <Styled.ColumnResizerChild />
    </Styled.ColumnResizerContainer>
  );
};

let passiveSupported = null;
function passiveEventSupported() {
  // memoize support to avoid adding multiple test events
  if (typeof passiveSupported === 'boolean') return passiveSupported;

  let supported = false;
  try {
    const options = {
      get passive() {
        supported = true;
        return false;
      },
    };

    window.addEventListener('test', null, options);
    window.removeEventListener('test', null, options);
  } catch (err) {
    supported = false;
  }
  passiveSupported = supported;
  return passiveSupported;
}

ColumnResizer = withShow(ColumnResizer, ({disabled}) => !disabled);

export default ColumnResizer;
