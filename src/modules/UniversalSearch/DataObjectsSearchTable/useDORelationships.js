import useSessionQuery from '@macanta/hooks/apollo/useSessionQuery';
import {LIST_RELATIONSHIPS} from '@macanta/graphql/dataObjects';

const useDORelationships = (groupId, optionsProp = {}) => {
  const {fetchAll, ...options} = optionsProp;

  const skip = (!fetchAll && !groupId) || options?.skip;

  const doRelationshipsQuery = useSessionQuery(LIST_RELATIONSHIPS, {
    ...(!fetchAll && {
      variables: {
        groupId,
      },
    }),
    ...options,
    skip,
  });

  return {
    ...doRelationshipsQuery,
    items: doRelationshipsQuery.data?.listRelationships,
  };
};

export default useDORelationships;
