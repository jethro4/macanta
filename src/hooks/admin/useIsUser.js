import useUsers from '@macanta/hooks/admin/useUsers';

const useIsUser = (userId) => {
  const usersQuery = useUsers();

  const updatedUsers = usersQuery.data;

  return !!updatedUsers?.some((item) => item.id === userId);
};

export default useIsUser;
