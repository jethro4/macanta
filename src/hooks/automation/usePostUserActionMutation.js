import useMutation from '@macanta/hooks/apollo/useMutation';
import {POST_USER_ACTION} from '@macanta/graphql/automation';

const usePostUserActionMutation = (options) => {
  const mutation = useMutation(POST_USER_ACTION, options);

  return mutation;
};

export default usePostUserActionMutation;
