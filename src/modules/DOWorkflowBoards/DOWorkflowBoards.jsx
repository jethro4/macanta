import React, {useState, useLayoutEffect} from 'react';
import {useLocation, useMatch} from '@reach/router';
import FormField from '@macanta/containers/FormField';
import Section from '@macanta/containers/Section';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import useWorkflowBoards from '@macanta/hooks/admin/useWorkflowBoards';
import {ROOT_PATH_WORKFLOW_BOARDS} from '.';
import * as ContactDetailsStyled from '@macanta/modules/ContactDetails/styles';
import * as Styled from './styles';
import {navigate} from 'gatsby';

const DOWorkflowBoards = ({children}) => {
  /* const navigate = useNavigate();*/

  const location = useLocation();
  const matchedPath = useMatch(ROOT_PATH_WORKFLOW_BOARDS);
  const isRootPath = !!matchedPath;

  const [selectedBoard, setSelectedBoard] = useState(null);

  const doTypesQuery = useDOTypes();
  const workflowBoardsQuery = useWorkflowBoards();

  const doTypes = doTypesQuery.data?.listDataObjectTypes;
  const doType = doTypes?.find((type) => type.title === selectedBoard?.type);
  const groupId = doType?.id;
  const boards = workflowBoardsQuery.data;

  const handleSelectBoard = (val) => {
    setSelectedBoard(boards.find((board) => val === board.id));
  };

  useLayoutEffect(() => {
    if (isRootPath && boards?.[0]) {
      setSelectedBoard({...boards[0]});
    } else {
      const boardId = location.pathname.split('/').pop();

      handleSelectBoard(boardId);
    }
  }, [isRootPath, boards]);

  useLayoutEffect(() => {
    if (groupId && selectedBoard) {
      navigate(
        `${ROOT_PATH_WORKFLOW_BOARDS}/${groupId}/${selectedBoard.title}/${selectedBoard.id}`,
      );
    }
  }, [groupId, selectedBoard]);

  return (
    <Section
      loading={workflowBoardsQuery.initLoading}
      fullHeight
      title="Workflow Boards"
      bodyStyle={{
        backgroundColor: '#f5f6fa',
        padding: '1rem 0',
      }}
      HeaderMidComp={
        <ContactDetailsStyled.InterfaceAlertsContainer>
          {selectedBoard && (
            <Styled.BoardDetailsInfoContainer>
              <Styled.BoardDetailsInfo>
                <Styled.BoardDetailsInfoLabel>
                  Type:
                </Styled.BoardDetailsInfoLabel>
                <Styled.BoardDetailsInfoValue>
                  {selectedBoard?.type}
                </Styled.BoardDetailsInfoValue>
              </Styled.BoardDetailsInfo>
              <Styled.BoardDetailsInfo>
                <Styled.BoardDetailsInfoLabel>
                  Key Field:
                </Styled.BoardDetailsInfoLabel>
                <Styled.BoardDetailsInfoValue>
                  {selectedBoard?.keyField}
                </Styled.BoardDetailsInfoValue>
              </Styled.BoardDetailsInfo>
              <Styled.BoardDetailsInfo>
                <Styled.BoardDetailsInfoLabel>
                  Relationship:
                </Styled.BoardDetailsInfoLabel>
                <Styled.BoardDetailsInfoValue>
                  {selectedBoard?.relationship || 'Any'}
                </Styled.BoardDetailsInfoValue>
              </Styled.BoardDetailsInfo>
              {selectedBoard?.description && (
                <Styled.BoardDetailsInfo>
                  <Styled.BoardDetailsInfoLabel>
                    Description:
                  </Styled.BoardDetailsInfoLabel>
                  <Styled.BoardDetailsInfoValue>
                    {selectedBoard?.description}
                  </Styled.BoardDetailsInfoValue>
                </Styled.BoardDetailsInfo>
              )}
            </Styled.BoardDetailsInfoContainer>
          )}
          {/* {interfaceAlertsLoading && (
          <LoadingIndicator fill transparent size={20} />
        )} */}
        </ContactDetailsStyled.InterfaceAlertsContainer>
      }
      HeaderRightComp={
        <FormField
          size="small"
          labelPosition="normal"
          style={{
            margin: 0,
            width: 270,
            marginLeft: '1.5rem',
          }}
          type="Select"
          label="Select Board"
          options={boards}
          value={selectedBoard?.id}
          labelKey="title"
          valueKey="id"
          onChange={handleSelectBoard}
        />
      }>
      {children}
    </Section>
  );
};

export default DOWorkflowBoards;
