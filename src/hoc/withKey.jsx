import React from 'react';

const withKey = (WrappedComponent, rerenderKeys) => {
  const EnhancedComponent = (props) => {
    const updatedKey = rerenderKeys?.map((key) => props[key]).join('-');

    return (
      <WrappedComponent
        {...(updatedKey && {
          key: updatedKey,
        })}
        {...props}
      />
    );
  };

  return EnhancedComponent;
};

export default withKey;
