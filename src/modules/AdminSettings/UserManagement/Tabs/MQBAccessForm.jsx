import React from 'react';
import Switch from '@macanta/components/Forms/ChoiceFields/Switch';
import useQueriesWithPermission from '@macanta/hooks/admin/useQueriesWithPermission';
import BodySection from '@macanta/modules/AdminSettings/UserManagement/BodySection';
import PermissionsTable from '@macanta/modules/AdminSettings/UserManagement/Tabs/PermissionsTable';
import {arrayConcatOrUpdateByKey} from '@macanta/utils/array';
import {ACCESS_LEVELS} from '@macanta/constants/accessLevels';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {
  AccessRadioGroup,
  AccessTableHeadCell,
} from '@macanta/modules/AdminSettings/UserManagement/Tabs/AccessPermissions';
import * as DataObjectsSearchTableStyled from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/styles';

const MQB_ACCESS_COLUMNS = [
  {
    id: 'hasAccess',
    label: '',
    disableSort: true,
    disableResize: true,
    minWidth: 80,
    maxWidth: 80,
    renderBodyCellValue: ({item, value, handlers}) => {
      return (
        <Switch
          color="success"
          checked={value}
          onChange={(event) => {
            handlers.onChangeAccess({
              item,
              value: event.target.checked
                ? ACCESS_LEVELS.READ_ONLY
                : ACCESS_LEVELS.NO_ACCESS,
            });
          }}
        />
      );
    },
  },
  {
    id: 'name',
    label: 'Queries',
    maxWidth: '100%',
    getBodyCellStyle: ({item}) =>
      !item.hasAccess && {
        color: 'grayDarkest.main',
      },
  },
  {
    id: 'others',
    label: '',
    disableSort: true,
    disableResize: true,
    minWidth: 270,
    maxWidth: 270,
    renderHeadCellValue: () => {
      return (
        <AccessTableHeadCell titles={['Add To Dashboard', 'Batch Email']} />
      );
    },
    renderBodyCellValue: ({item, handlers}) => {
      return (
        item.hasAccess && (
          <DataObjectsSearchTableStyled.ActionButtonContainer
            style={{
              width: '100%',
              padding: 0,
            }}>
            <Switch
              style={{
                flex: 1,
                justifyContent: 'center',
              }}
              activeColor="primary"
              checked={item.addToDashboard}
              onChange={(event) => {
                handlers.onOtherAccess({
                  item,
                  value: event.target.checked,
                  key: 'addToDashboard',
                });
              }}
            />
            <Switch
              style={{
                flex: 1,
                justifyContent: 'center',
              }}
              activeColor="primary"
              checked={item.addToBatchEmail}
              onChange={(event) => {
                handlers.onOtherAccess({
                  item,
                  value: event.target.checked,
                  key: 'addToBatchEmail',
                });
              }}
            />
          </DataObjectsSearchTableStyled.ActionButtonContainer>
        )
      );
    },
  },
  {
    id: 'permission',
    label: '',
    disableSort: true,
    disableResize: true,
    minWidth: 200,
    maxWidth: 200,
    renderHeadCellValue: () => {
      return <AccessTableHeadCell />;
    },
    renderBodyCellValue: ({item, value, handlers}) => {
      return (
        item.hasAccess && (
          <AccessRadioGroup
            value={value}
            onChange={(val) => {
              handlers.onChangeAccess({item, value: val});
            }}
          />
        )
      );
    },
  },
];

const MQBAccessForm = ({permission, onChange, ...props}) => {
  const queriesQuery = useQueriesWithPermission(
    {
      permission,
      isDataObjectType: true,
    },
    {
      fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
    },
  );

  const handleChangeAccess = ({item, value}) => {
    const updatedPermission = {
      ...permission,
      mqbs: arrayConcatOrUpdateByKey({
        arr: permission.mqbs,
        item: {
          queryId: item.id,
          permission: value,
        },
        key: 'queryId',
        includeAll: true,
      }),
    };

    onChange(updatedPermission);
  };

  const handleOtherAccess = ({item, key, value}) => {
    const permissionKey =
      key === 'addToDashboard' ? 'mqbWidgetAccess' : 'mqbWidgetEmail';

    const updatedPermission = {
      ...permission,
      [permissionKey]: arrayConcatOrUpdateByKey({
        arr: permission[permissionKey],
        item: {
          queryId: item.id,
          show: value,
        },
        key: 'queryId',
        includeAll: true,
      }),
    };

    onChange(updatedPermission);
  };

  const getKeyExtractor = (item) => {
    return item.queryId;
  };

  return (
    <BodySection {...props}>
      <PermissionsTable
        keyExtractor={getKeyExtractor}
        loading={queriesQuery.loading}
        columns={MQB_ACCESS_COLUMNS}
        data={queriesQuery.data}
        handlers={{
          onChangeAccess: handleChangeAccess,
          onOtherAccess: handleOtherAccess,
        }}
      />
    </BodySection>
  );
};

export default MQBAccessForm;
