import React from 'react';
import ButtonGroup from '@macanta/components/ButtonGroup';
import Button from '@macanta/components/Button';
import PermissionsTable from '@macanta/modules/AdminSettings/UserManagement/Tabs/PermissionsTable';
import useDOSectionsUserGuides from '@macanta/hooks/app/useDOSectionsUserGuides';
import UserGuidesColumns from './UserGuidesColumns';
import {getKeyExtractor} from './helpers';

const DOSectionsGuideForm = ({permission, onChange}) => {
  const {doTypes, setSelectedType, initLoading, data} = useDOSectionsUserGuides(
    {
      permission,
    },
  );

  return (
    <>
      <ButtonGroup
        increaseBtnMinWidth
        sx={{
          '& .MuiButton-root': {
            position: 'relative',

            '&:after': {
              content: '""',
              position: 'absolute',
              width: '0.5px',
              right: '-0.5px',
              top: 0,
              bottom: 0,
              backgroundColor: 'primary.main',
              borderRadius: '4px',
            },
          },
        }}
        style={{
          flexWrap: 'wrap',
          margin: '1rem',
        }}
        size="small"
        variant="outlined"
        aria-label="primary button group">
        {doTypes?.map((doType) => {
          return (
            <Button
              key={doType.id}
              onClick={(index) => setSelectedType(doTypes[index])}>
              {doType.title}
            </Button>
          );
        })}
      </ButtonGroup>

      <PermissionsTable
        loading={initLoading}
        keyExtractor={getKeyExtractor}
        columns={UserGuidesColumns}
        data={data}
        handlers={{
          onChange,
        }}
      />
    </>
  );
};

export default DOSectionsGuideForm;
