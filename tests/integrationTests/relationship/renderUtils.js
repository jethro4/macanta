import {
  selectButton,
  wait,
  waitUntil,
} from '@tests/jestSettings/utils/selectorUtils';
import {screen} from '@testing-library/react';

import {click} from '@tests/jestSettings/utils/eventUtils';
import {
  hasLength,
  isNotInDocument,
} from '@tests/jestSettings/utils/assertUtils';

export const getConnectDOTypesList = async () => {
  const connectDOButton = selectButton('Connect DO');

  click(connectDOButton);

  const doTypeItems = await hasLength(['listItem'], 4, {
    testId: 'DOTypesList',
  });

  return doTypeItems;
};

export const getConnectDOSearchTable = async () => {
  await screen.findByTestId('ConnectDORelationshipForm');
  await wait();

  const searchedItems = await hasLength(['rows'], 2, {
    testId: 'ConnectDORelationshipForm',
  });

  return searchedItems;
};

export const triggerConnectDODirectSteps = async () => {
  const doTypeItems = await getConnectDOTypesList();
  click(doTypeItems[0]);

  const searchedItems = await getConnectDOSearchTable();

  const connectButton = selectButton('Connect (0)');
  expect(connectButton).toBeDisabled();

  click(searchedItems[0]);
  click(searchedItems[1]);

  await waitUntil(() => expect(connectButton).toBeEnabled());

  click(connectButton);

  await isNotInDocument('ConnectDORelationshipForm', {
    isTestId: true,
  });
};
