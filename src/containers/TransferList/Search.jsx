import React from 'react';
import * as Styled from './styles';

const Search = ({style, size, onSearch}) => {
  return (
    <Styled.Search
      style={style}
      size={size}
      autoSearch
      disableCategories
      disableSuggestions
      onSearch={onSearch}
    />
  );
};

Search.defaultProps = {
  onSelectItem: () => {},
  autoSelect: false,
  modal: false,
};

export default Search;
