import {experimentalStyled as styled} from '@mui/material/styles';

import Divider from '@mui/material/Divider';

export const Root = styled(Divider)`
  border-bottom: 1px solid #eee;
`;
