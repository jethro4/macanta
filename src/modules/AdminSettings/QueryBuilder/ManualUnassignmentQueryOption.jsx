import React, {useState} from 'react';
import Modal from '@macanta/components/Modal';
import ManualUnassignmentForms from './ManualUnassignmentForms';

const ManualUnassignmentQueryOption = ({
  modal,
  showModal: showModalProp = false,
  onClose,
  id,
  name,
  onGoToAssignList,
}) => {
  const [showModal, setShowModal] = useState(showModalProp);

  const handleClose = () => {
    setShowModal(false);

    onClose();
  };

  const handleGoToAssignList = () => {
    handleClose();
    onGoToAssignList();
  };

  let manualUnassignmentForms = (
    <ManualUnassignmentForms
      id={id}
      onSuccess={handleClose}
      onGoToAssignList={handleGoToAssignList}
    />
  );

  if (modal) {
    manualUnassignmentForms = (
      <Modal
        headerTitle={`Manual Un-Assignment (${name})`}
        open={showModal}
        onClose={handleClose}
        contentWidth={500}
        contentHeight={350}>
        {manualUnassignmentForms}
      </Modal>
    );
  }

  return manualUnassignmentForms;
};

export default ManualUnassignmentQueryOption;
