import {InMemoryCache} from '@apollo/client';
import {CachePersistor, LocalStorageWrapper} from 'apollo3-cache-persist';
// import uniqBy from 'lodash/uniqBy';
// import {mergeAndAddArraysBySameKeys} from '@macanta/utils/array';
import envConfig from './envConfig';

export const MAX_RETRIES = 1;

export const cache = new InMemoryCache({
  typePolicies: {
    // Deity: {
    //   keyFields: ['name'],
    //   fields: {
    //     offspring(existingOffspring, {canRead}) {
    //       // Filter out any dangling references left over from removing
    //       // offspring, supplying a default empty array if there are no
    //       // offspring left.
    //       return existingOffspring ? existingOffspring.filter(canRead) : [];
    //     },
    //   },
    // },
    Query: {
      fields: {
        // ruler(existingRuler, {canRead, toReference}) {
        //   // If there is no existing ruler, Apollo becomes the ruling deity
        //   return canRead(existingRuler)
        //     ? existingRuler
        //     : toReference({
        //         __typename: 'Deity',
        //         name: 'Apollo',
        //       });
        // },
        listDataObjectFields: {
          keyArgs: ['groupId', 'userEmail'],
        },
        listRelationships: {
          keyArgs: ['groupId'],
        },

        // listValidDomainEmails: {
        //   merge(existing = [], incoming) {
        //     const uniqueIncoming = uniqBy(incoming, '__ref');
        //     const mergedDomainEmails = mergeAndAddArraysBySameKeys(
        //       existing,
        //       uniqueIncoming,
        //       {
        //         keysToCheck: ['__ref'],
        //         includeAll: true,
        //         removeMissing: true,
        //       },
        //     );
        //     return mergedDomainEmails;
        //   },
        // },
      },
    },

    Relationship: {
      // keyFields atteribute is helpful especially if a type uses a
      // field (or fields!) besides id or _id as its unique identifier.
      // This will be the cache keys that will control cache response
      keyFields: ['id', 'groupId'],

      // fields: {
      //   limit: {
      //     merge(existing, incoming) {
      //       return existing || incoming;
      //     },
      //   },
      //   autoAssignLoggedInUser: {
      //     merge(existing, incoming) {
      //       return existing || incoming;
      //     },
      //   },
      //   autoAssignContact: {
      //     merge(existing, incoming) {
      //       return existing || incoming;
      //     },
      //   },

      //   // groupId: {
      //   //   // read(groupId, {variables, toReference, readField, ...args}) {
      //   //   //   return groupId || variables?.groupId;
      //   //   // },
      //   //   merge(existing, incoming) {
      //   //     return existing || incoming;
      //   //   },
      //   // },
      // },
    },
    ValidDomainEmail: {
      keyFields: ['domain'],
    },
    TriggerActionItems: {
      keyFields: ['queryId'],
    },
    TriggerConditionsItems: {
      keyFields: ['queryId'],
    },
    EmailActionsItems: {
      keyFields: ['queryId'],
    },
    SmsActionItems: {
      keyFields: ['queryId'],
    },
    FieldActionItems: {
      keyFields: ['queryId'],
    },
    ContactActionItem: {
      keyFields: ['queryId'],
    },
    UserActionItems: {
      keyFields: ['queryId'],
    },
    WebhookActionItems: {
      keyFields: ['queryId'],
    },
  },
});

export const defaultOptions = {
  watchQuery: {
    fetchPolicy: 'cache-and-network',
    nextFetchPolicy: 'cache-first',
    errorPolicy: 'all',
  },
  query: {
    fetchPolicy: 'network-only',
    errorPolicy: 'all',
  },
  mutate: {
    errorPolicy: 'all',
  },
};

export const getPersistor = () => {
  const persistor = new CachePersistor({
    cache,
    storage: new LocalStorageWrapper(window.localStorage),
    debug: envConfig.isDev,
    trigger: 'write',
  });

  return persistor;
};
