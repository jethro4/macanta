import {useEffect} from 'react';
import isEqual from 'lodash/isEqual';
import usePrevious from '@macanta/hooks/usePrevious';

const useDeepCompareEffect = (callback, dependencies) => {
  const prevDependencies = usePrevious(dependencies);

  useEffect(() => {
    if (isEqual(dependencies, prevDependencies)) {
      return;
    }

    callback();
  }, dependencies);
};

export default useDeepCompareEffect;
