import * as yup from 'yup';

const workflowBoardValidationSchema = yup.object().shape({
  title: yup.string().required('Required'),
  type: yup.string().required('Required'),
  keyField: yup.string().required('Required'),
  displayFields: yup
    .array()
    .required('Required')
    .min(1, 'Please select at least one field'),
});

export default workflowBoardValidationSchema;
