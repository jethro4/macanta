import {gql} from '@apollo/client';

export default gql`
  query listFormBuilders {
    listFormBuilders {
      id
      title
      description
      type
    }
  }
`;
