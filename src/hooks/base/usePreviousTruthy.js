import {useEffect, useRef} from 'react';

const usePreviousTruthy = (value, defaultValue) => {
  const ref = useRef(value || defaultValue);

  useEffect(() => {
    if (value) {
      ref.current = value;
    }
  });

  return ref.current;
};

export default usePreviousTruthy;
