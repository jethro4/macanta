import useContacts from '@macanta/hooks/contact/useContacts';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import * as Storage from '@macanta/utils/storage';

const LoadLoggedInContact = () => {
  const userDetails = Storage.getItem('userDetails');

  useContacts(
    {
      q: userDetails?.userId,
    },
    {
      fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
    },
  );

  return null;
};

export default LoadLoggedInContact;
