import React, {useState} from 'react';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import ConfirmationDialog from '@macanta/containers/ConfirmationDialog';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import {useMutation} from '@apollo/client';
import {DELETE_CONTACT} from '@macanta/graphql/contacts';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import * as Storage from '@macanta/utils/storage';
import {navigate} from 'gatsby';

const DeleteContactQueryOption = ({showDialog = false, onClose, contact}) => {
  const {displayMessage} = useProgressAlert();
  /* const navigate = useNavigate();*/

  const [showDeleteDialog, setShowDeleteDialog] = useState(showDialog);

  const [callDeleteContactMutation, deleteContactMutation] = useMutation(
    DELETE_CONTACT,
    {
      onCompleted() {
        const {userId} = Storage.getItem('userDetails');

        navigate(`/app/contact/${userId}/notes`);

        handleClose();

        displayMessage(`Deleted contact successfully!`);
      },
    },
  );

  const handleClose = () => {
    setShowDeleteDialog(false);

    onClose();
  };

  const handleDeleteContact = () => {
    callDeleteContactMutation({
      variables: {
        deleteContactInput: {
          id: contact.id,
        },
        __mutationkey: 'deleteContactInput',
      },
    });
  };

  return (
    <>
      <ConfirmationDialog
        open={!!showDeleteDialog}
        onClose={handleClose}
        title={`Delete contact?`}
        description={[
          `You are about to delete "${`${contact.firstName} ${contact.lastName}`.trim()}" as a contact.`,
          'This will also remove the contact from any Data Objects it has a relationship with.',
          'This cannot be undone. Do you want to proceed?',
        ]}
        actions={[
          {
            label: 'Delete',
            disabledDelay: 3,
            onClick: handleDeleteContact,
            startIcon: <DeleteForeverIcon />,
          },
        ]}
      />
      <LoadingIndicator modal loading={deleteContactMutation.loading} />
    </>
  );
};

export default DeleteContactQueryOption;
