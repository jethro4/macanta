import React from 'react';
import CircleIcon from '@mui/icons-material/Circle';
import Box from '@mui/material/Box';
import ScheduleItem from '@macanta/modules/UserSchedules/ScheduleItem';
import useValue from '@macanta/hooks/base/useValue';
import Stepper from '@macanta/components/Stepper';
import StepLabel from '@mui/material/StepLabel';
import StepConnector from '@macanta/components/Stepper/StepConnector';
import Step from '@macanta/components/Stepper/Step';
import Colors from '@macanta/utils/colors';

const STEP_CONNECTOR_HEIGHT = 24;
const UPCOMING_COLOR = Colors.info;
// const ONGOIMG_COLOR = Colors.success;
// const PAST_COLOR = Colors.grayDark;

const VerticalStepper = ({data, ...props}) => {
  const [schedulesList] = useValue(
    () =>
      data.reduce((acc, d) => {
        const accArr = [...acc];

        d.items.forEach((item) => {
          accArr.push({...item, date: d.date});
        });

        return accArr;
      }, []),
    [data],
  );

  return (
    <Stepper activeStep={-1} orientation="vertical" connector={null} {...props}>
      {schedulesList.map((item) => {
        // const isAllDay = !item.startTime;

        return (
          <Step key={String(item.id)}>
            <StepLabel
              icon={null}
              sx={{
                '.MuiStepLabel-iconContainer': {
                  paddingRight: '12px',
                  alignSelf: 'flex-start',
                },
              }}
              StepIconProps={{
                // completed: true,
                icon: (
                  <Box
                    sx={{
                      display: 'flex',
                      marginTop: '5px',
                      position: 'relative',

                      '&:after': {
                        content: '""',
                        position: 'absolute',
                        display: 'block',
                        top: 11 + STEP_CONNECTOR_HEIGHT,
                        marginLeft: '5px',
                        minHeight: STEP_CONNECTOR_HEIGHT,
                        width: '1px',
                        backgroundColor: UPCOMING_COLOR,
                      },
                    }}>
                    {/* <Box sx={{
                            border: 'info.main',
                            width: 12,
                            height: 12,
                            borderRadius: 12
                          }} /> */}
                    <CircleIcon
                      sx={{
                        color: UPCOMING_COLOR,
                        width: 12,
                        height: 12,
                      }}
                    />
                    <StepConnector
                      sx={{
                        position: 'absolute',
                        top: 11,
                        marginLeft: '5px',
                        minHeight: STEP_CONNECTOR_HEIGHT,

                        '.MuiStepConnector-line': {
                          borderLeftColor: UPCOMING_COLOR,
                        },
                      }}
                    />
                  </Box>
                ),
                // sx: {
                //   width: 12,
                //   height: 12,

                //   '.MuiStepIcon-text': {
                //     display: 'none',
                //   },
                // },
              }}>
              <ScheduleItem key={String(item.id)} item={item} />
            </StepLabel>
          </Step>
        );
      })}
    </Stepper>
  );
};

export default VerticalStepper;
