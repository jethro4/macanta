import useMutation from '@macanta/hooks/apollo/useMutation';
import {POST_EMAIL_ACTION_TEMPLATE} from '@macanta/graphql/automation';

const usePostEmailActionTemplateMutation = (options) => {
  const mutation = useMutation(POST_EMAIL_ACTION_TEMPLATE, options);

  return mutation;
};

export default usePostEmailActionTemplateMutation;
