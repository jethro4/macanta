import {gql} from '@apollo/client';
import {VALID_DOMAIN_FRAGMENT} from './fragments';

export default gql`
  query listValidDomainEmails($domain: String) {
    listValidDomainEmails(domain: $domain) {
      ...validDomainFragment
    }
  }
  ${VALID_DOMAIN_FRAGMENT}
`;
