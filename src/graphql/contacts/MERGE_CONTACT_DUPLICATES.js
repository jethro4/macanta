import {gql} from '@apollo/client';

export default gql`
  mutation mergeContactDuplicates(
    $mergeContactDuplicatesInput: MergeContactDuplicatesInput!
  ) {
    mergeContactDuplicates(input: $mergeContactDuplicatesInput) {
      primaryContactId
      success
    }
  }
`;
