import React, {useEffect} from 'react';
import useDragHelper from '@macanta/hooks/base/useDragHelper';
import {sortArrayByObjectGroups} from '@macanta/utils/array';
import useGridLayout from './useGridLayout';
import useGridItems from './useGridItems';
import * as Styled from './styles';

const GridLayout = ({
  data: dataProp,
  groupBy,
  renderGridItem,
  onSort,
  maxColumn: maxColumnProp,
  responsive,
  autoResizeItem,
  autoHeight,
  isDraggable,
  isItemDraggable,
  ...props
}) => {
  const {data, breakpoints, cols, compactType} = useGridLayout({
    data: dataProp,
    groupBy,
    maxColumn: maxColumnProp,
    responsive,
  });

  const {
    // handleDragStart,
    handleDragging,
    handleDragStop,
    // handleDrop,
    // handleDropDragOver,
  } = useDragHelper({
    onSwitch: (layout, oldItem, newItem) => {
      const sortedLayout = sortArrayByObjectGroups(layout, ['x', 'y']);
      const sortedIds = sortedLayout.map((l) => l.i);

      onSort({
        id: oldItem.i,
        x: [oldItem.x, newItem.x],
        y: [oldItem.y, newItem.y],
        sortedIds,
        sortedLayout,
      });
    },
  });

  const {initialized, loadedAllItems, largestHeight, gridItems} = useGridItems({
    data,
    rowHeight: props.rowHeight,
    renderGridItem,
    autoResizeItem,
    marginY: props.margin ? props.margin[1] : 0,
    isItemDraggable,
  });

  useEffect(() => {
    if (data.length) {
      setTimeout(function () {
        window.dispatchEvent(new Event('resize'));
      }, 1);
    }
  }, [initialized, loadedAllItems]);

  const Root = !responsive ? Styled.GridLayout : Styled.ResponsiveGridLayout;

  return (
    <Root
      {...(autoHeight &&
        largestHeight && {
          style: {
            height: largestHeight,
          },
        })}
      {...(isDraggable === false && {
        sx: {
          '.react-grid-item': {
            cursor: 'default',
          },
        },
      })}
      key={loadedAllItems}
      isDraggable={isDraggable}
      autoHeight={autoHeight}
      initialized={initialized}
      compactType={compactType}
      breakpoints={breakpoints}
      cols={cols}
      // onDragStart={handleDragStart}
      onDrag={handleDragging}
      onDragStop={handleDragStop}
      // onDrop={handleDrop}
      // onDropDragOver={handleDropDragOver}
      // isResizable={false}
      // layouts={layouts}
      {...props}>
      {gridItems}
    </Root>
  );
};

GridLayout.defaultProps = {
  data: [],
  transparent: false,
  isResizable: false,
};

export default GridLayout;
