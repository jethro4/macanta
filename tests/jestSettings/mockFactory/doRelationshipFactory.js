import {nextFactoryId} from './factory';

const typename = 'Relationship';

export default (overwrites) => {
  const {id, currentIndex} = nextFactoryId(typename);

  return {
    __typename: typename,
    id,
    role: `Relationship ${currentIndex}`,
    description: '',
    exclusive: 'no',
    limit: 0,
    autoAssignLoggedInUser: true,
    autoAssignContact: false,
    ...overwrites,
  };
};
