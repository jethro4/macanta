import React, {
  useState,
  useEffect,
  useLayoutEffect,
  useCallback,
  useRef,
  forwardRef,
  useImperativeHandle,
} from 'react';
import debounce from 'lodash/debounce';
import uniq from 'lodash/uniq';
import CloseIcon from '@mui/icons-material/Close';
import FilterAltIcon from '@mui/icons-material/FilterAlt';
import MenuItem from '@mui/material/MenuItem';
import Divider from '@mui/material/Divider';
import InputAdornment from '@mui/material/InputAdornment';
import Badge from '@mui/material/Badge';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import Tooltip from '@macanta/components/Tooltip';
import {PopoverButton} from '@macanta/components/Button';
import useValue from '@macanta/hooks/base/useValue';
import * as Storage from '@macanta/utils/storage';
import * as Styled from './styles';

const getSuggestionsStringKey = (category) => {
  let suggestionsStringKey = '';

  switch (category) {
    case 'contact': {
      suggestionsStringKey = 'contact_search_suggestions';
      break;
    }
    default: {
      suggestionsStringKey = category;
      break;
    }
  }

  return suggestionsStringKey;
};

const getCachedSuggestions = (category) => {
  const suggestionsStringKey = getSuggestionsStringKey(category);

  if (!suggestionsStringKey) {
    return [];
  }

  const cachedSuggestions = Storage.getItem(suggestionsStringKey);

  return cachedSuggestions?.suggestions || [];
};

let Search = (
  {
    autoFocus,
    autoSearch,
    size,
    defaultValue,
    value: valueProp,
    categories,
    CategoryProps,
    defaultCategory,
    suggestions: suggestionsProp,
    filterSuggestions,
    TextFieldProps,
    AutoCompleteProps,
    onSearch,
    onChange,
    onClear,
    onEnterValue,
    loading,
    onChangeCategory,
    getOptionLabel,
    getOptionValue,
    filterOptions,
    placeholder,
    disableCategories,
    disableSuggestions,
    RightComp,
    error,
    FilterProps,
    ...props
  },
  ref,
) => {
  const filterCachedSuggestions = (category) => {
    if (!disableSuggestions) {
      const updatedSuggestions =
        suggestionsProp || getCachedSuggestions(category);
      const updatedFilteredSuggestions = !filterSuggestions
        ? updatedSuggestions
        : filterSuggestions(updatedSuggestions);

      return updatedFilteredSuggestions;
    }

    return [];
  };

  const [value, setValue] = useValue(defaultValue);
  const [category, setCategory] = useState(defaultCategory);
  const [suggestions, setSuggestions] = useState([]);
  const [showEmptyWarning, setShowEmptyWarning] = useState(false);

  const textFieldRef = useRef(null);
  const showEmptyWarningIntervalRef = useRef(null);

  const handleCategoryChange = (newVal) => {
    setCategory(newVal);
    setSuggestions(filterCachedSuggestions(newVal));

    onChangeCategory(newVal);
  };

  const handleCacheSuggestions = (newVal) => {
    if (!disableSuggestions && newVal) {
      const cachedSuggestions = filterCachedSuggestions(category);
      const newSuggestions = uniq([newVal].concat(cachedSuggestions));
      const latest100suggestions = newSuggestions.slice(0, 100);

      Storage.setItem(getSuggestionsStringKey(category), {
        suggestions: latest100suggestions,
      });

      setSuggestions(newSuggestions);
    }
  };

  const handleSearch = (newVal) => {
    onSearch(newVal);

    if (!autoSearch) {
      handleCacheSuggestions(newVal);
    }
  };

  const handleChangeText = (newVal) => {
    onChange && onChange(newVal);
  };

  const handleAutoSearch = useCallback(
    debounce((newVal) => {
      handleSearch(newVal);
      handleChangeText(newVal);
    }, 500),
    [],
  );

  const handleSearchTextChange = (newVal) => {
    setValue(newVal);

    if (autoSearch) {
      if (!newVal && autoSearch === 'with-value-only') {
        setShowEmptyWarning(true);
      } else {
        setShowEmptyWarning(false);

        handleAutoSearch(newVal);
      }
    } else {
      handleChangeText(newVal);
    }
  };

  const handleSelectSuggestion = (suggestion) => {
    setValue(suggestion);
    handleSearch(suggestion);
    handleChangeText(suggestion);
  };

  const handleClickOrEnterSearch = () => {
    if (!autoSearch) {
      handleSearch(value);
    }
  };

  const handleCategoryRenderValue = (cvalue) => {
    const selectedCategory = categories.find((c) => c.value === cvalue);

    return selectedCategory.label;
  };

  const handleClear = () => {
    handleSearchTextChange('');

    textFieldRef.current.focus();

    onClear && onClear();
  };

  useEffect(() => {
    if (valueProp && valueProp !== value) {
      handleSearchTextChange(valueProp);
    }
  }, [valueProp]);

  useLayoutEffect(() => {
    setSuggestions(filterCachedSuggestions(category));
  }, [disableSuggestions, suggestionsProp]);

  useImperativeHandle(
    ref,
    () => ({
      blur: () => {
        textFieldRef.current.blur();
      },
      focus: () => {
        textFieldRef.current.focus();
      },
    }),
    [],
  );

  return (
    <Styled.Root {...props}>
      {!disableCategories && (
        <Styled.SearchSelect
          select
          size={size}
          value={category}
          onChange={handleCategoryChange}
          variant="filled"
          InputProps={{
            disableUnderline: true,
            style: {
              borderRadius: 0,
            },
          }}
          renderValue={handleCategoryRenderValue}
          MenuProps={{
            anchorOrigin: {
              vertical: 'bottom',
              horizontal: 'left',
            },
            transformOrigin: {
              vertical: 'top',
              horizontal: 'left',
            },
          }}
          {...CategoryProps}>
          {categories.map((cat) =>
            cat.isSubheader ? (
              <MenuItem
                key={cat.label}
                value={cat.value}
                style={{
                  color: '#ccc',
                  ...cat.style,
                }}>
                {cat.label}
              </MenuItem>
            ) : (
              <MenuItem key={cat.value} value={cat.value} style={cat.style}>
                {cat.label}
              </MenuItem>
            ),
          )}
        </Styled.SearchSelect>
      )}

      <Tooltip
        open={showEmptyWarning}
        disableInteractive={true}
        title="Search cannot be empty"
        placement="bottom-start">
        <div
          style={{
            flex: 1,
            position: 'relative',
          }}>
          <Styled.SearchTextField
            ref={textFieldRef}
            autoFocus={autoFocus}
            search
            size={size}
            error={error}
            value={value}
            variant="outlined"
            onChange={handleSearchTextChange}
            onSelectOption={handleSelectSuggestion}
            placeholder={placeholder}
            disableSuggestions={disableSuggestions}
            AutoCompleteProps={{
              options: suggestions,
              getOptionLabel,
              getOptionValue,
              filterOptions,
              ...AutoCompleteProps,
            }}
            onEnterPress={(event, enteredValue) => {
              event.stopPropagation();

              if (!autoSearch) {
                if (onEnterValue && enteredValue) {
                  onEnterValue(enteredValue);
                } else if (!value) {
                  setShowEmptyWarning(true);

                  textFieldRef.current.focus();

                  clearTimeout(showEmptyWarningIntervalRef.current);

                  showEmptyWarningIntervalRef.current = setTimeout(() => {
                    setShowEmptyWarning(false);
                  }, 800);
                } else {
                  handleClickOrEnterSearch();
                }
              }
            }}
            {...TextFieldProps}
            InputProps={{
              startAdornment: (
                <InputAdornment
                  position="start"
                  style={{
                    marginLeft: 12,
                    marginRight: 8,
                  }}>
                  {!loading ? (
                    <Styled.SearchIcon
                      sx={{
                        color: '#ccc',
                        fontSize: 22,
                        // color: !autoSearch ? '#aaa' : '#ccc',
                      }}
                    />
                  ) : (
                    <LoadingIndicator
                      style={{width: 22}}
                      transparent
                      size={18}
                    />
                  )}
                </InputAdornment>
              ),
              ...TextFieldProps?.InputProps,
              style: {
                padding: 0,
                ...(!disableCategories && {
                  borderRadius: 0,
                }),
                ...TextFieldProps?.InputProps?.style,
              },
            }}
            inputProps={{
              ...TextFieldProps?.inputProps,
              style: {
                boxSizing: 'border-box',
                paddingLeft: 0,
                paddingRight: 10,
                ...TextFieldProps?.inputProps?.style,
              },
            }}
          />
          <Styled.ActionButtonsContainer>
            {!!value && (
              <Styled.SearchIconButton
                disableFocusRipple
                disableRipple
                onClick={handleClear}>
                <CloseIcon
                  style={{
                    fontSize: 20,
                    color: '#aaa',
                  }}
                />
              </Styled.SearchIconButton>
            )}

            {FilterProps?.renderContent && (
              <>
                <Divider
                  vertical
                  sx={{
                    border: 0,
                    width: '1px',
                    height: '60%',
                    backgroundColor: '#ddd',
                  }}
                />
                <PopoverButton
                  icon
                  sx={{
                    padding: '0.5rem',
                  }}
                  // onClick={handleAdd}
                  size="small"
                  {...FilterProps}
                  TooltipProps={{
                    title: 'Filters',
                  }}
                  PopoverProps={{
                    title: 'Filters',
                    contentWidth: 700,
                    anchorOrigin: {
                      vertical: 'bottom',
                      horizontal: 'right',
                    },
                    transformOrigin: {
                      vertical: -8,
                      horizontal: 'right',
                    },
                    ...FilterProps.PopoverProps,
                  }}>
                  <Badge badgeContent={FilterProps.badgeCount} color="info">
                    <FilterAltIcon
                      style={{
                        fontSize: 20,
                      }}
                      sx={{
                        color: 'secondary.main',
                      }}
                    />
                  </Badge>
                </PopoverButton>
              </>
            )}
          </Styled.ActionButtonsContainer>
        </div>
      </Tooltip>
      {RightComp}
    </Styled.Root>
  );
};

Search = forwardRef(Search);

Search.defaultProps = {
  autoFocus: false,
  size: 'small',
  defaultValue: '',
  categories: [],
  defaultCategory: '',
  loading: false,
  onSearch: () => {},
  onChangeCategory: () => {},
  placeholder: 'Search...',
};

export default Search;
