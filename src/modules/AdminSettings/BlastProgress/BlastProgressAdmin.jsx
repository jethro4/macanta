import React, {useState} from 'react';
import {navigate} from 'gatsby';
import Breadcrumbs from '@mui/material/Breadcrumbs';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import Typography from '@mui/material/Typography';
import Section from '@macanta/containers/Section';
import BlastProgressTable from '@macanta/modules/BlastProgress/BlastProgressTable';
import useBlastProgress from '@macanta/hooks/admin/useBlastProgress';
import {ROOT_PATH_ADMIN} from '@macanta/modules/AdminSettings/AdminSettings';
import SelectUserFilter from './SelectUserFilter';
import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import * as AdminSettingsStyled from '@macanta/modules/AdminSettings/styles';
import * as Storage from '@macanta/utils/storage';

const BlastProgressAdmin = () => {
  const userDetails = Storage.getItem('userDetails');

  /* const navigate = useNavigate();*/

  const [userId, setUserId] = useState(userDetails.userId);

  const {data, loading} = useBlastProgress(userId);

  const handleBack = () => {
    navigate(ROOT_PATH_ADMIN);
  };

  const handleUserChange = (id) => {
    setUserId(id);
  };

  return (
    <NoteTaskHistoryStyled.FullHeightGrid container>
      <NoteTaskHistoryStyled.FullHeightFlexGridColumn item xs={12}>
        <Section
          fullHeight
          bodyStyle={{
            backgroundColor: 'white',
            padding: 0,
          }}
          loading={loading}
          HeaderLeftComp={
            <Breadcrumbs aria-label="breadcrumb">
              <AdminSettingsStyled.TitleCrumbBtn
                startIcon={
                  <ArrowBackIcon
                    style={{
                      color: '#333',
                    }}
                  />
                }
                onClick={handleBack}
                disabled={false}>
                <Typography color="textPrimary">Admin Settings</Typography>
              </AdminSettingsStyled.TitleCrumbBtn>
              <Typography>Blast Progress</Typography>
            </Breadcrumbs>
          }
          HeaderRightComp={
            <SelectUserFilter userId={userId} onChange={handleUserChange} />
          }>
          <BlastProgressTable data={data} hideEmptyMessage={loading} />
        </Section>
      </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
    </NoteTaskHistoryStyled.FullHeightGrid>
  );
};

export default BlastProgressAdmin;
