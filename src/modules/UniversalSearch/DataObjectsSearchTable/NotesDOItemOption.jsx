import React, {useState, useEffect} from 'react';
import useQuery from '@macanta/hooks/apollo/useQuery';
import Modal from '@macanta/components/Modal';
import Button from '@macanta/components/Button';
import TurnedInIcon from '@mui/icons-material/TurnedIn';
import {GET_DATA_OBJECT} from '@macanta/graphql/dataObjects';
import DONotes from '@macanta/modules/DataObjectItems/DOItemForms/DONotes';
import * as FormStyled from '@macanta/modules/NoteTaskForms/styles';

const NotesDOItemOption = ({
  itemId,
  modal,
  showModal: showModalProp = false,
  onClose,
  onSave,
}) => {
  const [changes, setChanges] = useState([]);
  const [showModal, setShowModal] = useState(showModalProp);

  const {loading, data} = useQuery(GET_DATA_OBJECT, {
    variables: {
      itemId,
    },
  });

  const notes = data?.getDataObject?.notes;

  const handleCloseDOHistory = () => {
    setShowModal(false);

    onClose && onClose();
  };

  const handleValueChange = (formData) => {
    const note = changes.find((n) =>
      n.id ? n.id === formData.id : n.tempId === formData.tempId,
    );
    let updatedNotes = [];

    if (note) {
      updatedNotes = changes.map((n) => {
        if (n.id ? n.id === formData.id : n.tempId === formData.tempId) {
          return formData;
        }

        return n;
      });
    } else {
      updatedNotes = changes.concat(formData);
    }

    setChanges(updatedNotes);
  };

  const handleSubmit = () => {
    onSave({
      notes: changes.map((n) => ({
        id: n.id,
        note: n.note,
        tags: n.tags,
        title: n.title,
      })),
    });
  };

  useEffect(() => {
    setShowModal(showModalProp);
  }, [showModalProp]);

  let doNotes = (
    <DONotes
      style={{
        margin: 0,
      }}
      loading={loading}
      data={notes}
      onChange={handleValueChange}
      FooterComp={
        <FormStyled.Footer
          style={{
            padding: '1rem',
            margin: 0,
            borderTop: '1px solid #eee',
            width: '100%',
            backgroundColor: '#f5f6fa',
          }}>
          <Button
            disabled={!changes.length}
            variant="contained"
            startIcon={<TurnedInIcon />}
            onClick={handleSubmit}
            size="medium">
            Save Changes
          </Button>
        </FormStyled.Footer>
      }
    />
  );

  if (modal) {
    doNotes = (
      <Modal
        headerTitle={itemId}
        open={showModal}
        onClose={handleCloseDOHistory}
        contentWidth={600}
        contentHeight={'90%'}>
        {doNotes}
      </Modal>
    );
  }

  return doNotes;
};

export default NotesDOItemOption;
