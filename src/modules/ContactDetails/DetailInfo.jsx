import React from 'react';
import Box from '@mui/material/Box';
import * as Styled from './styles';

const DetailInfo = ({
  label,
  value,
  children,
  style,
  noMargin,
  vertical,
  disableOneLine,
}) => (
  <Box
    style={{
      display: 'flex',
      alignItems: 'flex-end',
      ...(vertical && {flexDirection: 'column', alignItems: 'flex-start'}),
      ...(!noMargin && {marginBottom: '0.5rem'}),
      ...style,
    }}>
    <Styled.DetailLabel>
      {label}
      {!vertical ? ':' : ''}
    </Styled.DetailLabel>
    {value ? (
      <Styled.DetailValue disableOneLine={disableOneLine}>
        {value || children}
      </Styled.DetailValue>
    ) : (
      children
    )}
  </Box>
);

export default DetailInfo;
