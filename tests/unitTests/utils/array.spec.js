import isNumber from 'lodash/isNumber';
import isString from 'lodash/isString';

import {
  findItem,
  filterItems,
  hasItemValue,
  hasItem,
  hasDuplicates,
  contains,
  containsAll,
  splice,
  every,
  groupBy,
  convertPropertiesToArray,
  convertArrayToObjectByKeys,
  getPropertiesWithValues,
  convertArrayToObject,
  sortArray,
  sortArrayByObjectKey,
  sortArrayByPriority,
  sortArrayByObjectGroups,
  sortArrayByKeyCondition,
  sortArrayByKeyValues,
  sortArrayByKeyValuesIncludeAll,
  mergeArraysBySameKeys,
  mergeAndAddArraysBySameKeys,
  arrayConcatOrUpdateByKey,
  arraySwapItems,
  insert,
  update,
  insertOrUpdate,
  insertOrRemove,
  remove,
  moveItem,
  toggleItem,
  traverseRead,
  traverseAddEdit,
  traverseDelete,
  uniqByKeys,
  updateAll,
} from '@macanta/utils/array';
import {generateQueueId} from '@macanta/utils/graphql';
import doToDORelationshipFactory from '@tests/jestSettings/mockFactory/doToDORelationshipFactory';

describe('Array Utils', () => {
  const initialValues = {
    type: 'All',
    title: '',
    note: '',
    tags: [],
    show: 'Notes Only',
  };

  describe('overrides', () => {
    describe('findItem', () => {
      it('find first item found based on key / value mapping object', () => {
        const arr = [
          {key: 'key1', a: 1},
          {key: 'key2', a: 2},
          {key: 'key3', a: 1, d: 4},
          {key: 'key4', a: 3},
        ];

        const actualResult1 = findItem(arr, {a: 1});
        const actualResult2 = findItem(arr, {key: 'key3', a: 1});
        const noResult = findItem([{a: 1}, {b: 2}, {c: 3}, {d: 4}], {e: 5});

        expect(actualResult1).toEqual({key: 'key1', a: 1});
        expect(actualResult2).toEqual({key: 'key3', a: 1, d: 4});
        expect(noResult).toEqual(undefined);
      });

      it('negative paths', () => {
        const undefinedResult = findItem(undefined);
        const nullResult = findItem(null);

        expect(undefinedResult).toEqual(undefined);
        expect(nullResult).toEqual(undefined);
      });
    });

    describe('filterItems', () => {
      it('find all items found based on key / value mapping object', () => {
        const arr = [
          {key: 'key1', a: 1},
          {key: 'key2', a: 2},
          {key: 'key3', a: 1, d: 4},
          {key: 'key4', a: 3},
        ];

        const actualResult1 = filterItems(arr, {a: 1});
        const actualResult2 = filterItems(arr, {key: 'key3', a: 1});
        const noResult = filterItems([{a: 1}, {b: 2}, {c: 3}, {d: 4}], {e: 5});

        expect(actualResult1).toEqual([
          {key: 'key1', a: 1},
          {key: 'key3', a: 1, d: 4},
        ]);
        expect(actualResult2).toEqual([{key: 'key3', a: 1, d: 4}]);
        expect(noResult).toEqual([]);
      });

      it('negative paths', () => {
        const undefinedResult = filterItems(undefined);
        const nullResult = filterItems(null);

        expect(undefinedResult).toEqual([]);
        expect(nullResult).toEqual([]);
      });
    });

    describe('contains', () => {
      it('check if array contains at least one item in subset', () => {
        const arr = [1, 2, 3];

        const actualResult1 = contains(arr, [1]);
        const actualResult2 = contains(arr, [1, 2, 3]);
        const actualResult3 = contains(arr, [1, 4]);
        const actualResult4 = contains(arr, [4]);

        expect(actualResult1).toEqual(true);
        expect(actualResult2).toEqual(true);
        expect(actualResult3).toEqual(true);
        expect(actualResult4).toEqual(false);
      });

      it('negative paths', () => {
        const undefinedResult = contains();
        const nullResult = contains(null, null);

        expect(undefinedResult).toEqual(false);
        expect(nullResult).toEqual(false);
      });
    });

    describe('containsAll', () => {
      it('check if array contains all items in subset', () => {
        const arr = [1, 2, 3];

        const actualResult1 = containsAll(arr, [1, 2, 3]);
        const actualResult2 = containsAll(arr, [2, 3]);
        const actualResult3 = containsAll(arr, [1, 4]);

        expect(actualResult1).toEqual(true);
        expect(actualResult2).toEqual(true);
        expect(actualResult3).toEqual(false);
      });

      it('negative paths', () => {
        const undefinedResult = containsAll();
        const nullResult = containsAll(null, null);

        expect(undefinedResult).toEqual(false);
        expect(nullResult).toEqual(false);
      });
    });

    describe('splice', () => {
      it('with index only', () => {
        const arrayToMutate = [1, 2, 3, 4];

        const {currentItems, removedItems, isLastItem} = splice(
          arrayToMutate,
          1,
        );

        expect(currentItems).toEqual([1]);
        expect(removedItems).toEqual([2, 3, 4]);
        expect(isLastItem).toEqual(false);
      });

      it('with index & howmany', () => {
        const arrayToMutate = [1, 2, 3, 4];

        const {currentItems, removedItems, isLastItem} = splice(
          arrayToMutate,
          2,
          1,
        );

        expect(currentItems).toEqual([1, 2, 4]);
        expect(removedItems).toEqual([3]);
        expect(isLastItem).toEqual(false);
      });

      it('not isLastItem', () => {
        const arrayToMutate = [1, 2, 3, 4];

        const {currentItems, removedItems, isLastItem} = splice(
          arrayToMutate,
          1,
          2,
        );

        expect(currentItems).toEqual([1, 4]);
        expect(removedItems).toEqual([2, 3]);
        expect(isLastItem).toEqual(false);
      });

      it('isLastItem', () => {
        const arrayToMutate = [1, 2, 3, 4];

        const {currentItems, removedItems, isLastItem} = splice(
          arrayToMutate,
          0,
        );

        expect(currentItems).toEqual([]);
        expect(removedItems).toEqual([1, 2, 3, 4]);
        expect(isLastItem).toEqual(true);
      });
    });

    describe('every', () => {
      it('without callback', () => {
        const allTrue = [true, true, true];
        const mixed = [true, true, false];

        const actualResult1 = every(allTrue, true);
        const actualResult2 = every(mixed, true);

        expect(actualResult1).toEqual(true);
        expect(actualResult2).toEqual(false);
      });

      it('with callback', () => {
        const allNumbers = [-1, 0, 1];
        const allStrings = ['', 'qwe', 'klj'];
        const mixed = ['', -1, 0, 1];

        const actualResult1 = every(allNumbers, (item) => isNumber(item));
        const actualResult2 = every(allStrings, (item) => isString(item));
        const actualResult3 = every(mixed, (item) => isString(item));

        expect(actualResult1).toEqual(true);
        expect(actualResult2).toEqual(true);
        expect(actualResult3).toEqual(false);
      });
    });

    describe('grouping arrays', () => {
      let arr = [
        doToDORelationshipFactory({
          doItemId: `ItemDataObject1`,
          groupId: 'DataObject1',
          type: 'direct',
        }),
        doToDORelationshipFactory({
          doItemId: `ItemDataObject2`,
          groupId: 'DataObject2',
          type: 'direct',
        }),
        doToDORelationshipFactory({
          doItemId: `ItemDataObject3`,
          groupId: 'DataObject2',
          type: 'direct',
        }),
        doToDORelationshipFactory({
          doItemId: `ItemDataObject5`,
          groupId: 'DataObject2',
          type: 'indirect',
        }),
      ];

      it('happy - non-nested group array by key 1', () => {
        const key = 'type';

        const actualResult = groupBy(arr, key);
        const expectedResult = [
          {
            type: 'direct',
            items: [
              doToDORelationshipFactory({
                doItemId: `ItemDataObject1`,
                groupId: 'DataObject1',
                type: 'direct',
              }),
              doToDORelationshipFactory({
                doItemId: `ItemDataObject2`,
                groupId: 'DataObject2',
                type: 'direct',
              }),
              doToDORelationshipFactory({
                doItemId: `ItemDataObject3`,
                groupId: 'DataObject2',
                type: 'direct',
              }),
            ],
          },
          {
            type: 'indirect',
            items: [
              doToDORelationshipFactory({
                doItemId: `ItemDataObject5`,
                groupId: 'DataObject2',
                type: 'indirect',
              }),
            ],
          },
        ];

        expect(actualResult).toEqual(expectedResult);
      });

      it('happy - non-nested group array by key 2', () => {
        const key = 'groupId';

        const actualResult = groupBy(arr, key);
        const expectedResult = [
          {
            groupId: 'DataObject1',
            items: [
              doToDORelationshipFactory({
                doItemId: `ItemDataObject1`,
                groupId: 'DataObject1',
                type: 'direct',
              }),
            ],
          },
          {
            groupId: 'DataObject2',
            items: [
              doToDORelationshipFactory({
                doItemId: `ItemDataObject2`,
                groupId: 'DataObject2',
                type: 'direct',
              }),
              doToDORelationshipFactory({
                doItemId: `ItemDataObject3`,
                groupId: 'DataObject2',
                type: 'direct',
              }),
              doToDORelationshipFactory({
                doItemId: `ItemDataObject5`,
                groupId: 'DataObject2',
                type: 'indirect',
              }),
            ],
          },
        ];

        expect(actualResult).toEqual(expectedResult);
      });

      it('happy - nested group array by keys', () => {
        const keys = ['type', 'groupId'];

        const actualResultGrouped = groupBy(arr, keys);
        const expectedResultGrouped = [
          {
            type: 'direct',
            items: [
              {
                groupId: 'DataObject1',
                items: [
                  doToDORelationshipFactory({
                    doItemId: `ItemDataObject1`,
                    groupId: 'DataObject1',
                    type: 'direct',
                  }),
                ],
              },
              {
                groupId: 'DataObject2',
                items: [
                  doToDORelationshipFactory({
                    doItemId: `ItemDataObject2`,
                    groupId: 'DataObject2',
                    type: 'direct',
                  }),
                  doToDORelationshipFactory({
                    doItemId: `ItemDataObject3`,
                    groupId: 'DataObject2',
                    type: 'direct',
                  }),
                ],
              },
            ],
          },
          {
            type: 'indirect',
            items: [
              {
                groupId: 'DataObject2',
                items: [
                  doToDORelationshipFactory({
                    doItemId: `ItemDataObject5`,
                    groupId: 'DataObject2',
                    type: 'indirect',
                  }),
                ],
              },
            ],
          },
        ];

        expect(actualResultGrouped).toEqual(expectedResultGrouped);

        const actualResultFiltered = actualResultGrouped.find(
          (d) => d.type === 'direct',
        );
        const doItemId1 = actualResultFiltered.items[0].items[0].doItemId;
        const doItemId2 = actualResultFiltered.items[1].items[0].doItemId;

        expect(doItemId1).toEqual('ItemDataObject1');
        expect(doItemId2).toEqual('ItemDataObject2');
      });

      it('happy - return empty array if passed array argument is null/undefined', () => {
        const keys = ['type', 'groupId'];

        expect(groupBy(null, keys)).toEqual([]);
        expect(groupBy(undefined, keys)).toEqual([]);
      });

      it('sad - throw error if keys are not passed in argument', () => {
        const actualFunc = () => groupBy(arr);

        expect(actualFunc).toThrow();
      });
    });
  });

  describe('convertPropertiesToArray', () => {
    let obj;

    beforeEach(() => {
      obj = initialValues;
    });

    it('happy - convert object to array items by default key and value attributes', () => {
      const actualResult = convertPropertiesToArray(obj);
      const expectedResult = [
        {
          key: 'type',
          value: 'All',
        },
        {
          key: 'title',
          value: '',
        },
        {
          key: 'note',
          value: '',
        },
        {
          key: 'tags',
          value: [],
        },
        {
          key: 'show',
          value: 'Notes Only',
        },
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - convert object to array items by custom key and value attributes', () => {
      const sampleObj = {
        id: 'item_9892d0f6113b155b',
        'Opportunity Stage': 'New',
        'Opportunity Title': 'Test Opportunities 1',
        'Sample Checkbox': 'Choice 2',
      };
      const actualResult = convertPropertiesToArray(sampleObj, {
        labelKey: 'fieldName',
        valueKey: 'value',
      });
      const expectedResult = [
        {
          fieldName: 'id',
          value: 'item_9892d0f6113b155b',
        },
        {
          fieldName: 'Opportunity Stage',
          value: 'New',
        },
        {
          fieldName: 'Opportunity Title',
          value: 'Test Opportunities 1',
        },
        {
          fieldName: 'Sample Checkbox',
          value: 'Choice 2',
        },
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - omit specific key', () => {
      const sampleObj = {
        id: 'item_9892d0f6113b155b',
        'Opportunity Stage': 'New',
        'Opportunity Title': 'Test Opportunities 1',
        'Sample Checkbox': 'Choice 2',
      };
      const actualResult = convertPropertiesToArray(sampleObj, {
        labelKey: 'fieldName',
        valueKey: 'value',
        omitKeys: ['id'],
      });
      const expectedResult = [
        {
          fieldName: 'Opportunity Stage',
          value: 'New',
        },
        {
          fieldName: 'Opportunity Title',
          value: 'Test Opportunities 1',
        },
        {
          fieldName: 'Sample Checkbox',
          value: 'Choice 2',
        },
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('sad - return empty array when object is empty', () => {
      obj = {};
      const actualResult = convertPropertiesToArray(obj);
      const expectedResult = [];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - convert array to object by keys', () => {
      const arr = [
        {
          fieldId: 1,
          name: 'Opportunity Type',
        },
        {
          fieldId: 2,
          name: 'Opportunity Stage',
        },
      ];
      const actualResult = convertArrayToObjectByKeys(arr, {
        labelKey: 'name',
        valueKey: 'fieldId',
      });
      const expectedResult = {
        'Opportunity Type': 1,
        'Opportunity Stage': 2,
      };

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - convert array to object without valueKey', () => {
      const arr = [
        {
          fieldId: 1,
          name: 'Opportunity Type',
        },
        {
          fieldId: 2,
          name: 'Opportunity Stage',
        },
      ];
      const actualResult = convertArrayToObjectByKeys(arr, {
        labelKey: 'name',
      });
      const expectedResult = {
        'Opportunity Type': {
          fieldId: 1,
          name: 'Opportunity Type',
        },
        'Opportunity Stage': {
          fieldId: 2,
          name: 'Opportunity Stage',
        },
      };

      expect(actualResult).toEqual(expectedResult);
    });

    it('negative paths - convertArrayToObjectByKeys', () => {
      const emptyArrayResult = convertArrayToObjectByKeys([], {
        labelKey: 'name',
      });
      const undefinedResult = convertArrayToObjectByKeys(undefined, {
        labelKey: 'name',
      });
      const nullResult = convertArrayToObjectByKeys(null, {
        labelKey: 'name',
      });

      expect(emptyArrayResult).toEqual({});
      expect(undefinedResult).toEqual({});
      expect(nullResult).toEqual({});
      expect(() => {
        convertArrayToObjectByKeys(null);
      }).toThrow();
    });

    it('happy - merge arrays by same keys', () => {
      const arr = [
        {
          fieldId: 1,
          value: 'White Label',
        },
        {
          fieldId: 2,
          value: 'Clarity Call Scheduled',
        },
      ];
      const arr2 = [
        {
          fieldId: 1,
          name: 'Opportunity Type',
          permission: 'READ_WRITE',
        },
        {
          fieldId: 2,
          name: 'Opportunity Stage',
          permission: 'READ_ONLY',
        },
      ];
      const actualResult = mergeArraysBySameKeys(arr, arr2, {
        keysToCheck: ['fieldId'],
        keysToMerge: ['name'],
      });
      const expectedResult = [
        {
          fieldId: 1,
          value: 'White Label',
          name: 'Opportunity Type',
        },
        {
          fieldId: 2,
          value: 'Clarity Call Scheduled',
          name: 'Opportunity Stage',
        },
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - merge arrays by same keys - includeAll=true', () => {
      const arr = [
        {
          fieldId: 1,
          value: 'White Label',
        },
        {
          fieldId: 2,
          value: 'Clarity Call Scheduled',
        },
      ];
      const arr2 = [
        {
          fieldId: 1,
          value: 'Black Label',
          name: 'Opportunity Type',
          permission: 'READ_WRITE',
        },
        {
          fieldId: 2,
          name: 'Opportunity Stage',
          permission: 'READ_ONLY',
        },
      ];
      const actualResult = mergeArraysBySameKeys(arr, arr2, {
        keysToCheck: ['fieldId'],
        includeAll: true,
      });
      const expectedResult = [
        {
          fieldId: 1,
          value: 'Black Label',
          name: 'Opportunity Type',
          permission: 'READ_WRITE',
        },
        {
          fieldId: 2,
          value: 'Clarity Call Scheduled',
          name: 'Opportunity Stage',
          permission: 'READ_ONLY',
        },
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - merge arrays by same keys with multiple keysToCheck and keysToMerge', () => {
      const arr = [
        {
          fieldId: 1,
          fieldId2: 3,
          value: 'White Label',
        },
        {
          fieldId: 2,
          value: 'Clarity Call Scheduled',
        },
        {
          fieldId: 4,
          fieldId2: null,
          sampleAttr: 'Test',
        },
      ];
      const arr2 = [
        {
          fieldId: 1,
          fieldId2: 3,
          name: 'Opportunity Type',
          permission: 'READ_WRITE',
        },
        {
          fieldId: 2,
          name: 'Opportunity Stage',
          permission: 'READ_ONLY',
        },
        {
          fieldId: 4,
          fieldId2: null,
          sampleAttr: 'Edited',
        },
      ];
      const actualResult = mergeArraysBySameKeys(arr, arr2, {
        keysToCheck: ['fieldId', 'fieldId2'],
        keysToMerge: ['name', 'permission', 'sampleAttr'],
      });
      const expectedResult = [
        {
          fieldId: 1,
          fieldId2: 3,
          value: 'White Label',
          name: 'Opportunity Type',
          permission: 'READ_WRITE',
        },
        {
          fieldId: 2,
          value: 'Clarity Call Scheduled',
          name: 'Opportunity Stage',
          permission: 'READ_ONLY',
        },
        {
          fieldId: 4,
          fieldId2: null,
          sampleAttr: 'Edited',
        },
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('sad - return items that only match by same keysToCheck value', () => {
      const arr = [
        {
          fieldId: 1,
          value: 'White Label',
        },
        {
          fieldId: 2,
          value: 'Clarity Call Scheduled',
        },
      ];
      const arr2 = [
        {
          fieldId: 12,
          name: 'Opportunity Type',
        },
        {
          fieldId: 23,
          name: 'Opportunity Stage',
        },
      ];
      const actualResult = mergeArraysBySameKeys(arr, arr2, {
        keysToCheck: ['fieldId'],
        keysToMerge: ['name'],
      });
      const expectedResult = [];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - merge and add arrays - includeAll=true', () => {
      const arr = [
        {
          fieldId: 1,
          value: 'White Label',
          relationships: ['Test'],
        },
        {
          fieldId: 12,
          value: 'Clarity Call Scheduled',
          name: 'Description',
        },
        {
          fieldId: 2,
          value: 'Clarity Call Scheduled',
          name: 'Description 2',
        },
      ];
      const arr2 = [
        {
          fieldId: 1,
          value: 'White Label',
          relationships: [],
        },
        {
          fieldId: 12,
          name: 'Opportunity Type',
        },
        {
          fieldId: 23,
          name: 'Opportunity Stage',
        },
      ];
      const actualResult = mergeAndAddArraysBySameKeys(arr, arr2, {
        keysToCheck: ['fieldId'],
        includeAll: true,
      });
      const expectedResult = [
        {
          fieldId: 1,
          value: 'White Label',
          relationships: [],
        },
        {
          fieldId: 12,
          value: 'Clarity Call Scheduled',
          name: 'Opportunity Type',
        },
        {
          fieldId: 2,
          value: 'Clarity Call Scheduled',
          name: 'Description 2',
        },
        {
          fieldId: 23,
          name: 'Opportunity Stage',
        },
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - merge and add arrays by same keys', () => {
      const arr = [
        {
          fieldId: 1,
          value: 'White Label',
        },
        {
          fieldId: 12,
          value: 'Clarity Call Scheduled',
        },
        {
          fieldId: 2,
          value: 'Clarity Call Scheduled',
        },
      ];
      const arr2 = [
        {
          fieldId: 12,
          name: 'Opportunity Type',
        },
        {
          fieldId: 23,
          name: 'Opportunity Stage',
        },
      ];
      const actualResult = mergeAndAddArraysBySameKeys(arr, arr2, {
        keysToCheck: ['fieldId'],
        keysToMerge: ['name'],
      });
      const expectedResult = [
        {
          fieldId: 1,
          value: 'White Label',
        },
        {
          fieldId: 12,
          value: 'Clarity Call Scheduled',
          name: 'Opportunity Type',
        },
        {
          fieldId: 2,
          value: 'Clarity Call Scheduled',
        },
        {
          fieldId: 23,
          name: 'Opportunity Stage',
        },
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - merge and add arrays by same keys - empty arrToMerge array', () => {
      const arr = [
        {
          fieldId: 1,
          value: 'White Label',
        },
        {
          fieldId: 2,
          value: 'Clarity Call Scheduled',
        },
      ];
      const arr2 = [];
      const actualResult = mergeAndAddArraysBySameKeys(arr, arr2, {
        keysToCheck: ['fieldId'],
        keysToMerge: ['name'],
      });
      const expectedResult = [
        {
          fieldId: 1,
          value: 'White Label',
        },
        {
          fieldId: 2,
          value: 'Clarity Call Scheduled',
        },
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - merge and add arrays by same keys - empty original array - includeAll=true', () => {
      const arr = [];
      const arr2 = [
        {
          fieldId: 1,
          value: 'Black Label',
          name: 'Opportunity Type',
          permission: 'READ_WRITE',
        },
        {
          fieldId: 2,
          name: 'Opportunity Stage',
          permission: 'READ_ONLY',
        },
      ];
      const actualResult = mergeAndAddArraysBySameKeys(arr, arr2, {
        keysToCheck: ['fieldId'],
        keysToMerge: ['name'],
        includeAll: true,
      });
      const expectedResult = [
        {
          fieldId: 1,
          value: 'Black Label',
          name: 'Opportunity Type',
          permission: 'READ_WRITE',
        },
        {
          fieldId: 2,
          name: 'Opportunity Stage',
          permission: 'READ_ONLY',
        },
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - merge and add arrays by same keys with multiple keysToCheck - includeAll=true', () => {
      const arr = [
        {
          fieldId: 1,
          fieldId2: 3,
          value: 'White Label',
        },
        {
          fieldId: 100,
          value: 'Clarity Call Scheduled',
        },
        {
          fieldId: 4,
          fieldId2: null,
          sampleAttr: 'Test',
        },
        {
          fieldId: 5,
          fieldId2: undefined,
          sampleAttr: 'Test 2',
        },
      ];
      const arr2 = [
        {
          fieldId: 1,
          fieldId2: 3,
          value: 'Black Label',
          name: 'Opportunity Type',
          permission: 'READ_WRITE',
        },
        {
          fieldId: 101,
          value: 'Pending',
        },
        {
          fieldId: 4,
          fieldId2: null,
          sampleAttr: 'Edited',
        },
        {
          fieldId: 5,
          sampleAttr: 'Edited 2',
        },
      ];
      const actualResult = mergeAndAddArraysBySameKeys(arr, arr2, {
        keysToCheck: ['fieldId', 'fieldId2'],
        includeAll: true,
      });
      const expectedResult = [
        {
          fieldId: 1,
          fieldId2: 3,
          value: 'Black Label',
          name: 'Opportunity Type',
          permission: 'READ_WRITE',
        },
        {
          fieldId: 100,
          value: 'Clarity Call Scheduled',
        },
        {
          fieldId: 4,
          fieldId2: null,
          sampleAttr: 'Edited',
        },
        {
          fieldId: 5,
          sampleAttr: 'Edited 2',
        },
        {
          fieldId: 101,
          value: 'Pending',
        },
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - merge and add arrays by same keys - remove missing item in incoming array - includeAll=true', () => {
      const arr = [
        {
          fieldId: 1,
          value: 'White Label',
        },
        {
          fieldId: 2,
          value: 'Clarity Call Scheduled',
        },
        {
          fieldId: 3,
          name: 'Opportunity Stage',
          permission: 'READ_ONLY',
        },
      ];
      const arr2 = [
        {
          fieldId: 1,
          value: 'Black Label',
          name: 'Opportunity Type',
          permission: 'READ_WRITE',
        },
        {
          fieldId: 3,
          name: 'Opportunity Stage Edited',
        },
      ];
      const actualResult = mergeAndAddArraysBySameKeys(arr, arr2, {
        keysToCheck: ['fieldId'],
        keysToMerge: ['name'],
        includeAll: true,
        removeMissing: true,
      });
      const expectedResult = [
        {
          fieldId: 1,
          value: 'Black Label',
          name: 'Opportunity Type',
          permission: 'READ_WRITE',
        },
        {
          fieldId: 3,
          name: 'Opportunity Stage Edited',
          permission: 'READ_ONLY',
        },
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - merge and add arrays by same keys with priorities', () => {
      const arr = [
        {
          fieldId: 1,
          value: 'White Label',
        },
        {
          fieldId: 12,
          value: 'Clarity Call Scheduled',
        },
        {
          fieldId: 2,
          value: 'Clarity Call Scheduled',
        },
      ];
      const arr2 = [
        {
          fieldId: 12,
          name: 'Opportunity Type',
        },
        {
          fieldId: 23,
          name: 'Opportunity Stage',
        },
      ];
      const actualResult = mergeAndAddArraysBySameKeys(arr, arr2, {
        keysToCheck: ['fieldId'],
        keysToMerge: ['name'],
        priorities: [23, 1, 12, 2],
      });
      const expectedResult = [
        {
          fieldId: 23,
          name: 'Opportunity Stage',
        },
        {
          fieldId: 1,
          value: 'White Label',
        },
        {
          fieldId: 12,
          value: 'Clarity Call Scheduled',
          name: 'Opportunity Type',
        },
        {
          fieldId: 2,
          value: 'Clarity Call Scheduled',
        },
      ];

      expect(actualResult).toEqual(expectedResult);
    });
  });

  describe('getPropertiesWithValues', () => {
    let obj;

    beforeEach(() => {
      obj = initialValues;
    });

    it('happy - convert object property key pair to array items with truthy values', () => {
      const actualResult = getPropertiesWithValues(obj);
      const expectedResult = [
        {
          key: 'type',
          id: 'type',
          value: 'All',
        },
        {
          key: 'show',
          id: 'show',
          value: 'Notes Only',
        },
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - array properties should be flattened in object by appending index in key', () => {
      obj = {
        type: 'All',
        title: '',
        note: '',
        tags: ['Tag_filter', 'Tag_filter_2', 'Tag_filter_3'],
        show: 'Notes Only',
      };
      const actualResult = getPropertiesWithValues(obj, ['All']);
      const expectedResult = [
        {
          key: 'tags',
          id: 'tags0',
          value: 'Tag_filter',
        },
        {
          key: 'tags',
          id: 'tags1',
          value: 'Tag_filter_2',
        },
        {
          key: 'tags',
          id: 'tags2',
          value: 'Tag_filter_3',
        },
        {
          key: 'show',
          id: 'show',
          value: 'Notes Only',
        },
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('sad - should not include an item with excluded value', () => {
      const actualResult = getPropertiesWithValues(obj, ['All']);
      const expectedResult = [
        {
          key: 'show',
          id: 'show',
          value: 'Notes Only',
        },
      ];

      expect(actualResult).toEqual(expectedResult);
    });
  });

  describe('convertArrayToObject', () => {
    let arr, obj;

    beforeEach(() => {
      arr = [
        {
          key: 'title',
          value: 'Test Title 2',
          id: 'title',
        },
        {
          key: 'show',
          value: 'Notes Only',
          id: 'show',
        },
        {
          key: 'tags',
          value: 'Test_tag_1',
          id: 'tags0',
        },
        {
          key: 'tags',
          value: 'Test_tag_2',
          id: 'tags1',
        },
      ];

      obj = initialValues;
    });

    it('happy - convert array to object key value pair with array items', () => {
      const actualResult = convertArrayToObject(arr, obj);
      const expectedResult = {
        type: 'All',
        title: 'Test Title 2',
        note: '',
        tags: ['Test_tag_1', 'Test_tag_2'],
        show: 'Notes Only',
      };

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - with empty array items', () => {
      arr = [
        {
          key: 'title',
          value: 'Test Title 2',
          id: 'title',
        },
        {
          key: 'show',
          value: 'Notes Only',
          id: 'show',
        },
      ];

      const actualResult = convertArrayToObject(arr, obj);
      const expectedResult = {
        type: 'All',
        title: 'Test Title 2',
        note: '',
        tags: [],
        show: 'Notes Only',
      };

      expect(actualResult).toEqual(expectedResult);
    });
  });

  describe('sorting arrays', () => {
    let arr;

    beforeEach(() => {
      arr = [{value: 'a'}, {value: 'b'}, {value: 'c'}];
    });

    it('happy - sort array primitive values', () => {
      const arr1 = ['b', 'c', 'a'];
      const arr2 = [2, 3, 1];

      const actualResult1 = sortArray(arr1);
      const actualResult2 = sortArray(arr2);

      const expectedResult1 = ['a', 'b', 'c'];
      const expectedResult2 = [1, 2, 3];

      expect(actualResult1).toEqual(expectedResult1);
      expect(actualResult2).toEqual(expectedResult2);
    });

    it('happy - sort array by key', () => {
      arr = [
        {
          id: '87',
          fileName: '3E41C5C5-17BC-49B2-8C2D-2FC78356FDFE.jpg',
          thumbnail: true,
          downloadUrl:
            'https://f000.backblazeb2.com/file/macantacrm-connected-data-attachments/staging/item_fba12522d88f99ed/3E41C5C5-17BC-49B2-8C2D-2FC78356FDFE.jpg',
        },
        {
          id: '70',
          fileName: 'flowers.jpeg',
          thumbnail: true,
          downloadUrl:
            'https://f000.backblazeb2.com/file/macantacrm-connected-data-attachments/staging/item_fba12522d88f99ed/flowers.jpeg',
        },
        {
          id: '61',
          fileName: 'vali_2_manual.pdf',
          thumbnail: true,
          downloadUrl:
            'https://f000.backblazeb2.com/file/macantacrm-connected-data-attachments/staging/item_fba12522d88f99ed/vali_2_manual.pdf',
        },
        {
          id: '90',
          fileName: 'FE30B663-270D-4558-B8E9-F01473B30EB4.jpg',
          thumbnail: true,
          downloadUrl:
            'https://f000.backblazeb2.com/file/macantacrm-connected-data-attachments/staging/item_fba12522d88f99ed/FE30B663-270D-4558-B8E9-F01473B30EB4.jpg',
        },
      ];

      const actualResult = sortArrayByObjectKey(arr, 'fileName');

      const expectedResult = [
        {
          id: '87',
          fileName: '3E41C5C5-17BC-49B2-8C2D-2FC78356FDFE.jpg',
          thumbnail: true,
          downloadUrl:
            'https://f000.backblazeb2.com/file/macantacrm-connected-data-attachments/staging/item_fba12522d88f99ed/3E41C5C5-17BC-49B2-8C2D-2FC78356FDFE.jpg',
        },
        {
          id: '90',
          fileName: 'FE30B663-270D-4558-B8E9-F01473B30EB4.jpg',
          thumbnail: true,
          downloadUrl:
            'https://f000.backblazeb2.com/file/macantacrm-connected-data-attachments/staging/item_fba12522d88f99ed/FE30B663-270D-4558-B8E9-F01473B30EB4.jpg',
        },
        {
          id: '70',
          fileName: 'flowers.jpeg',
          thumbnail: true,
          downloadUrl:
            'https://f000.backblazeb2.com/file/macantacrm-connected-data-attachments/staging/item_fba12522d88f99ed/flowers.jpeg',
        },
        {
          id: '61',
          fileName: 'vali_2_manual.pdf',
          thumbnail: true,
          downloadUrl:
            'https://f000.backblazeb2.com/file/macantacrm-connected-data-attachments/staging/item_fba12522d88f99ed/vali_2_manual.pdf',
        },
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - sort array by key 2', () => {
      arr = [
        {val: 'ContactId', index: 17},
        {val: 'FirstName', index: 8},
        {val: 'LastName', index: 0},
      ];

      const actualResult = sortArrayByObjectKey(arr, 'index');

      const expectedResult = [
        {val: 'LastName', index: 0},
        {val: 'FirstName', index: 8},
        {val: 'ContactId', index: 17},
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - sort array by key and descending sortDirection', () => {
      arr = [
        {
          id: '87',
          fileName: '3E41C5C5-17BC-49B2-8C2D-2FC78356FDFE.jpg',
          thumbnail: true,
          downloadUrl:
            'https://f000.backblazeb2.com/file/macantacrm-connected-data-attachments/staging/item_fba12522d88f99ed/3E41C5C5-17BC-49B2-8C2D-2FC78356FDFE.jpg',
        },
        {
          id: '70',
          fileName: 'flowers.jpeg',
          thumbnail: true,
          downloadUrl:
            'https://f000.backblazeb2.com/file/macantacrm-connected-data-attachments/staging/item_fba12522d88f99ed/flowers.jpeg',
        },
        {
          id: '61',
          fileName: 'vali_2_manual.pdf',
          thumbnail: true,
          downloadUrl:
            'https://f000.backblazeb2.com/file/macantacrm-connected-data-attachments/staging/item_fba12522d88f99ed/vali_2_manual.pdf',
        },
        {
          id: '90',
          fileName: 'FE30B663-270D-4558-B8E9-F01473B30EB4.jpg',
          thumbnail: true,
          downloadUrl:
            'https://f000.backblazeb2.com/file/macantacrm-connected-data-attachments/staging/item_fba12522d88f99ed/FE30B663-270D-4558-B8E9-F01473B30EB4.jpg',
        },
      ];

      const actualResult = sortArrayByObjectKey(arr, 'fileName', 'desc');

      const expectedResult = [
        {
          id: '61',
          fileName: 'vali_2_manual.pdf',
          thumbnail: true,
          downloadUrl:
            'https://f000.backblazeb2.com/file/macantacrm-connected-data-attachments/staging/item_fba12522d88f99ed/vali_2_manual.pdf',
        },
        {
          id: '70',
          fileName: 'flowers.jpeg',
          thumbnail: true,
          downloadUrl:
            'https://f000.backblazeb2.com/file/macantacrm-connected-data-attachments/staging/item_fba12522d88f99ed/flowers.jpeg',
        },
        {
          id: '90',
          fileName: 'FE30B663-270D-4558-B8E9-F01473B30EB4.jpg',
          thumbnail: true,
          downloadUrl:
            'https://f000.backblazeb2.com/file/macantacrm-connected-data-attachments/staging/item_fba12522d88f99ed/FE30B663-270D-4558-B8E9-F01473B30EB4.jpg',
        },
        {
          id: '87',
          fileName: '3E41C5C5-17BC-49B2-8C2D-2FC78356FDFE.jpg',
          thumbnail: true,
          downloadUrl:
            'https://f000.backblazeb2.com/file/macantacrm-connected-data-attachments/staging/item_fba12522d88f99ed/3E41C5C5-17BC-49B2-8C2D-2FC78356FDFE.jpg',
        },
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - sort array of objects by key with priority array of objects', () => {
      const priority = [{value: 'c'}, {value: 'a'}];
      const actualResult = sortArrayByPriority(arr, 'value', priority);
      const expectedResult = [{value: 'c'}, {value: 'a'}, {value: 'b'}];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - sort array of objects by key with priority array of objects with priority key', () => {
      const priority = [{id: 'c'}, {id: 'a'}];
      const actualResult = sortArrayByPriority(arr, 'value', priority, 'id');
      const expectedResult = [{value: 'c'}, {value: 'a'}, {value: 'b'}];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - sort array of objects by key with priority array of strings', () => {
      const actualResult = sortArrayByPriority(arr, 'value', ['c', 'a']);
      const expectedResult = [{value: 'c'}, {value: 'a'}, {value: 'b'}];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - sort array of strings with priority array of strings', () => {
      const customArr = ['a', 'b', 'c'];
      const actualResult = sortArrayByPriority(customArr, null, ['c', 'a']);
      const expectedResult = ['c', 'a', 'b'];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - sort array of objects by key with null/undefined values with priority array of strings', () => {
      arr = arr.concat([{value: null}, {value: undefined}]);
      const actualResult = sortArrayByPriority(arr, 'value', [null, undefined]);
      const expectedResult = [
        {value: null},
        {value: undefined},
        {value: 'a'},
        {value: 'b'},
        {value: 'c'},
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - sort array of objects by key with missing key name with priority array of strings', () => {
      arr = [
        {value: 'z', createdAt: 2},
        {value: 'x', createdAt: 1},
        {value: 'a'},
        {value: 'b'},
        {value: 'c'},
        {value: 'j', createdAt: null},
        {value: 'k', createdAt: undefined},
      ];
      const actualResult = sortArrayByPriority(arr, 'createdAt', [
        null,
        undefined,
      ]);
      const expectedResult = [
        {value: 'j', createdAt: null},
        {value: 'a'},
        {value: 'b'},
        {value: 'c'},
        {value: 'k', createdAt: undefined},
        {value: 'z', createdAt: 2},
        {value: 'x', createdAt: 1},
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - sort array with multiple items with priority of string value', () => {
      arr = arr.concat({value: 'c'});
      const actualResult = sortArrayByPriority(arr, 'value', 'c');
      const expectedResult = [
        {value: 'c'},
        {value: 'c'},
        {value: 'a'},
        {value: 'b'},
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - sort array with multiple items with priority array of strings', () => {
      arr = arr.concat({value: 'c'});
      const actualResult = sortArrayByPriority(arr, 'value', ['c']);
      const expectedResult = [
        {value: 'c'},
        {value: 'c'},
        {value: 'a'},
        {value: 'b'},
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - sort arrayByKeyCondition', () => {
      arr = arr.concat({value: 'c'});
      const actualResult1 = sortArrayByKeyCondition(
        arr,
        'value',
        (value) => value === 'c',
      );
      const actualResult2 = sortArrayByKeyCondition(
        arr,
        'value',
        (value) => value === 'a',
      );

      const expectedResult1 = [
        {value: 'c'},
        {value: 'c'},
        {value: 'a'},
        {value: 'b'},
      ];
      const expectedResult2 = [
        {value: 'a'},
        {value: 'b'},
        {value: 'c'},
        {value: 'c'},
      ];

      expect(actualResult1).toEqual(expectedResult1);
      expect(actualResult2).toEqual(expectedResult2);
    });

    it('happy - sort arrayByKeyCondition - real data', () => {
      const allTabs = [
        {id: 'notes', title: 'Notes', type: 'notes'},
        {
          __typename: 'DataObject',
          id: 'ci_jt739fbp',
          title: 'Memberships',
          type: 'data-object',
        },
        {
          __typename: 'DataObject',
          id: 'ci_jt74il6h',
          title: 'Workshops',
          type: 'data-object',
        },
        {
          __typename: 'DataObject',
          id: 'ci_jt74o9rj',
          title: 'FFP Seminars',
          type: 'data-object',
        },
        {
          __typename: 'DataObject',
          id: 'ci_kpm4hrjf',
          title: 'Realtors Demo Group',
          type: 'data-object',
        },
        {
          __typename: 'DataObject',
          id: 'ci_knpqliq0',
          title: 'Properties',
          type: 'data-object',
        },
        {
          __typename: 'DataObject',
          id: 'ci_kojqmt7n',
          title: 'Expo Passes',
          type: 'data-object',
        },
      ];
      const macantaTabOrder = [
        'Memberships',
        'Notes',
        'Communication History',
        'Workshops',
        'FFP Seminars',
        'Case Studies',
        'Road Trips',
        'Classes and Other',
        'Expos and Masters',
        'Education Progress',
        'Mentorships',
        'Subscription Preferences',
        'Transactions',
        'Webinar Schedule',
        'Promo Codes',
        'Properties',
        'Admin',
        'Expo Passes',
      ];

      const actualResult = sortArrayByKeyValuesIncludeAll(
        allTabs,
        'title',
        macantaTabOrder,
      );
      const expectedResult = [
        {
          __typename: 'DataObject',
          id: 'ci_jt739fbp',
          title: 'Memberships',
          type: 'data-object',
        },
        {id: 'notes', title: 'Notes', type: 'notes'},
        {
          __typename: 'DataObject',
          id: 'ci_jt74il6h',
          title: 'Workshops',
          type: 'data-object',
        },
        {
          __typename: 'DataObject',
          id: 'ci_jt74o9rj',
          title: 'FFP Seminars',
          type: 'data-object',
        },
        {
          __typename: 'DataObject',
          id: 'ci_knpqliq0',
          title: 'Properties',
          type: 'data-object',
        },
        {
          __typename: 'DataObject',
          id: 'ci_kojqmt7n',
          title: 'Expo Passes',
          type: 'data-object',
        },
        {
          __typename: 'DataObject',
          id: 'ci_kpm4hrjf',
          title: 'Realtors Demo Group',
          type: 'data-object',
        },
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - sort array with all items in priority', () => {
      const customArr = ['a', 'b', 'c'];
      const actualResult = sortArrayByPriority(customArr, null, [
        'a',
        'b',
        'c',
      ]);
      const expectedResult = ['a', 'b', 'c'];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - sort array with excess in priority', () => {
      const customArr = ['a', 'b', 'c'];
      const actualResult = sortArrayByPriority(customArr, null, [
        'c',
        'b',
        'a',
        'd',
      ]);
      const expectedResult = ['c', 'b', 'a'];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - real data sample - sort array with excess in priority', () => {
      const customArr = ['Macanta User', 'App Owner'];
      const actualResult = sortArrayByPriority(customArr, null, [
        'Macanta User',
        'App Owner',
        'Developer Agent',
      ]);
      const expectedResult = ['Macanta User', 'App Owner'];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - sort array by groups based on keys with numeric values', () => {
      arr = [
        {
          id: 'field_kbgam62l',
          sectionTagIdPosition: 1,
          fieldSubGroupPosition: 4,
        },
        {
          id: 'field_kbganwac',
          sectionTagIdPosition: 1,
          fieldSubGroupPosition: 3,
        },
        {
          id: 'field_kbgc5i84',
          sectionTagIdPosition: 1,
          fieldSubGroupPosition: 5,
        },
        {
          id: 'field_kbgc6jsz',
          sectionTagIdPosition: 1,
          fieldSubGroupPosition: 2,
        },
        {
          id: 'field_kbggzyrg',
          sectionTagIdPosition: 1,
          fieldSubGroupPosition: 4,
        },
        {
          id: 'field_kc1y67q7',
          sectionTagIdPosition: 3,
          fieldSubGroupPosition: 1,
        },
        {
          id: 'field_kc6cz3z5',
          sectionTagIdPosition: 3,
          fieldSubGroupPosition: 2,
        },
        {
          id: 'field_kc6fvlgq',
          sectionTagIdPosition: 3,
          fieldSubGroupPosition: 1,
        },
        {
          id: 'field_kijad3ex',
          sectionTagIdPosition: 1,
          fieldSubGroupPosition: 2,
        },
        {
          id: 'field_kj1585gm',
          sectionTagIdPosition: 1,
          fieldSubGroupPosition: 5,
        },
        {
          id: 'field_kksbyqgn',
          sectionTagIdPosition: 1,
          fieldSubGroupPosition: 3,
        },
        {
          id: 'field_klsc63yx',
          sectionTagIdPosition: 2,
          fieldSubGroupPosition: 1,
        },
        {
          id: 'field_klscgane',
          sectionTagIdPosition: 2,
          fieldSubGroupPosition: 1,
        },
        {
          id: 'field_klsciv50',
          sectionTagIdPosition: 1,
          fieldSubGroupPosition: 1,
        },
      ];

      const actualResult = sortArrayByObjectGroups(arr, [
        'sectionTagIdPosition',
        'fieldSubGroupPosition',
      ]);
      const expectedResult = [
        {
          id: 'field_klsciv50',
          sectionTagIdPosition: 1,
          fieldSubGroupPosition: 1,
        },
        {
          id: 'field_kbgc6jsz',
          sectionTagIdPosition: 1,
          fieldSubGroupPosition: 2,
        },
        {
          id: 'field_kijad3ex',
          sectionTagIdPosition: 1,
          fieldSubGroupPosition: 2,
        },
        {
          id: 'field_kbganwac',
          sectionTagIdPosition: 1,
          fieldSubGroupPosition: 3,
        },
        {
          id: 'field_kksbyqgn',
          sectionTagIdPosition: 1,
          fieldSubGroupPosition: 3,
        },
        {
          id: 'field_kbgam62l',
          sectionTagIdPosition: 1,
          fieldSubGroupPosition: 4,
        },
        {
          id: 'field_kbggzyrg',
          sectionTagIdPosition: 1,
          fieldSubGroupPosition: 4,
        },
        {
          id: 'field_kbgc5i84',
          sectionTagIdPosition: 1,
          fieldSubGroupPosition: 5,
        },
        {
          id: 'field_kj1585gm',
          sectionTagIdPosition: 1,
          fieldSubGroupPosition: 5,
        },
        {
          id: 'field_klsc63yx',
          sectionTagIdPosition: 2,
          fieldSubGroupPosition: 1,
        },
        {
          id: 'field_klscgane',
          sectionTagIdPosition: 2,
          fieldSubGroupPosition: 1,
        },
        {
          id: 'field_kc1y67q7',
          sectionTagIdPosition: 3,
          fieldSubGroupPosition: 1,
        },
        {
          id: 'field_kc6fvlgq',
          sectionTagIdPosition: 3,
          fieldSubGroupPosition: 1,
        },
        {
          id: 'field_kc6cz3z5',
          sectionTagIdPosition: 3,
          fieldSubGroupPosition: 2,
        },
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - sort array by groups based on keys with string values', () => {
      arr = [
        {
          id: 'field_kbgam62l',
          sectionTag: 'General',
          subGroup: 'Initial Call',
        },
        {
          id: 'field_kbganwac',
          sectionTag: 'General',
          subGroup: 'Clarity Call',
        },
        {
          id: 'field_kbgc5i84',
          sectionTag: 'General',
          subGroup: 'Sales Call',
        },
        {
          id: 'field_kbgc6jsz',
          sectionTag: 'General',
          subGroup: 'Overview',
        },
        {
          id: 'field_kbggzyrg',
          sectionTag: 'General',
          subGroup: 'Initial Call',
        },
        {
          id: 'field_kc1y67q7',
          sectionTag: 'Tracking',
          subGroup: 'Invoicing',
        },
        {
          id: 'field_kc6cz3z5',
          sectionTag: 'Tracking',
          subGroup: 'Status',
        },
        {
          id: 'field_kc6fvlgq',
          sectionTag: 'Tracking',
          subGroup: 'Invoicing',
        },
        {
          id: 'field_kijad3ex',
          sectionTag: 'General',
          subGroup: 'Overview',
        },
        {
          id: 'field_kj1585gm',
          sectionTag: 'General',
          subGroup: 'Sales Call',
        },
        {
          id: 'field_kksbyqgn',
          sectionTag: 'General',
          subGroup: 'Clarity Call',
        },
        {
          id: 'field_klsc63yx',
          sectionTag: 'Status Tracking',
          subGroup: 'Status Options',
        },
        {
          id: 'field_klscgane',
          sectionTag: 'Status Tracking',
          subGroup: 'Status Options',
        },
        {
          id: 'field_klsciv50',
          sectionTag: 'General',
          subGroup: 'Status',
        },
      ];

      const actualResult = sortArrayByObjectGroups(arr, [
        'sectionTag',
        'subGroup',
      ]);
      const expectedResult = [
        {
          id: 'field_kbganwac',
          sectionTag: 'General',
          subGroup: 'Clarity Call',
        },
        {
          id: 'field_kksbyqgn',
          sectionTag: 'General',
          subGroup: 'Clarity Call',
        },
        {
          id: 'field_kbgam62l',
          sectionTag: 'General',
          subGroup: 'Initial Call',
        },
        {
          id: 'field_kbggzyrg',
          sectionTag: 'General',
          subGroup: 'Initial Call',
        },
        {
          id: 'field_kbgc6jsz',
          sectionTag: 'General',
          subGroup: 'Overview',
        },
        {
          id: 'field_kijad3ex',
          sectionTag: 'General',
          subGroup: 'Overview',
        },
        {
          id: 'field_kbgc5i84',
          sectionTag: 'General',
          subGroup: 'Sales Call',
        },
        {
          id: 'field_kj1585gm',
          sectionTag: 'General',
          subGroup: 'Sales Call',
        },
        {
          id: 'field_klsciv50',
          sectionTag: 'General',
          subGroup: 'Status',
        },
        {
          id: 'field_klsc63yx',
          sectionTag: 'Status Tracking',
          subGroup: 'Status Options',
        },
        {
          id: 'field_klscgane',
          sectionTag: 'Status Tracking',
          subGroup: 'Status Options',
        },
        {
          id: 'field_kc1y67q7',
          sectionTag: 'Tracking',
          subGroup: 'Invoicing',
        },
        {
          id: 'field_kc6fvlgq',
          sectionTag: 'Tracking',
          subGroup: 'Invoicing',
        },
        {
          id: 'field_kc6cz3z5',
          sectionTag: 'Tracking',
          subGroup: 'Status',
        },
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - sort array by key index', () => {
      arr = [
        {
          id: 'ci_k30e5zmn',
          title: 'Macanta Installs',
        },
        {
          id: 'ci_kbgaluau',
          title: 'Opportunities',
        },
        {
          id: 'ci_kc2bram8',
          title: 'Support Queries',
        },
        {
          id: 'ci_keoawaqy',
          title: 'Podcast Episodes',
        },
        {
          id: 'ci_kgkftkzb',
          title: 'Guided Setups',
        },
        {
          id: 'ci_kkcu0uyc',
          title: 'Partnerships',
        },
        {
          id: 'ci_klht6zrt',
          title: 'Loans',
        },
      ];

      const arrValues = [
        'Notes',
        'Macanta_Installs',
        'Loans',
        'Opportunities',
        'Embed_Test_2',
        'Communication_History',
        'Embed_Test',
        'Support_Queries',
        'Podcast_Episodes',
        'Guided_Setups',
        'Partnerships',
        'Admin',
      ].map((val) => val.replace(/_+/g, ' '));

      const actualResult = sortArrayByKeyValues(arr, 'title', arrValues);

      const expectedResult = [arr[0], arr[6], ...arr.slice(1, arr.length - 1)];

      expect(actualResult).toEqual(expectedResult);
    });
  });

  describe('mutating array', () => {
    let arrayToMutate;

    beforeEach(() => {
      arrayToMutate = [
        {
          id: 're_09a525c71679400d',
          role: 'Project Lead',
          exclusive: null,
          limit: null,
          autoAssignLoggedInUser: null,
          autoAssignContact: null,
          __typename: 'Relationship',
        },
        {
          id: 're_0bf9cc0c32c27fa5',
          role: 'Secondary Contact',
          exclusive: null,
          limit: null,
          autoAssignLoggedInUser: null,
          autoAssignContact: null,
          __typename: 'Relationship',
        },
        {
          id: 're_0ede51667d7844a3',
          role: 'Technical Agent',
          exclusive: null,
          limit: null,
          autoAssignLoggedInUser: null,
          autoAssignContact: null,
          __typename: 'Relationship',
        },
      ];
    });

    it('happy - uniqByKeys 1', () => {
      arrayToMutate = [
        {name: 'Name1'},
        {name: 'Name1'},
        {name: 'Name2'},
        {name: 'Name2'},
        {name: 'Name3'},
      ];

      const actualResult = uniqByKeys(arrayToMutate, ['name']);
      const expectedResult = [
        {name: 'Name1'},
        {name: 'Name2'},
        {name: 'Name3'},
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - uniqByKeys 2', () => {
      arrayToMutate = [
        {name: 'Name1', id: 1},
        {name: 'Name1', id: 1},
        {name: 'Name2', id: 2},
        {name: 'Name2', id: 3},
        {name: 'Name3', id: 4},
      ];

      const actualResult = uniqByKeys(arrayToMutate, ['name', 'id']);
      const expectedResult = [
        {name: 'Name1', id: 1},
        {name: 'Name2', id: 2},
        {name: 'Name2', id: 3},
        {name: 'Name3', id: 4},
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - includeAll:false - update item in array if key not found', () => {
      const newItem = {
        role: 'Technical Agent',
        limit: 4,
        autoAssignLoggedInUser: true,
        autoAssignContact: true,
        __typename: 'Relationship',
      };

      const actualResult = update({
        arr: arrayToMutate,
        item: newItem,
        key: 'role',
        // includeAll: true,
      });
      const expectedResult = [
        arrayToMutate[0],
        arrayToMutate[1],
        {
          role: 'Technical Agent',
          limit: 4,
          autoAssignLoggedInUser: true,
          autoAssignContact: true,
          __typename: 'Relationship',
        },
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - includeAll:true - update item in array if key not found', () => {
      const newItem = {
        role: 'Technical Agent',
        limit: 4,
        autoAssignLoggedInUser: true,
        autoAssignContact: true,
        __typename: 'Relationship',
      };

      const actualResult = update({
        arr: arrayToMutate,
        item: newItem,
        key: 'role',
        includeAll: true,
      });
      const expectedResult = [
        arrayToMutate[0],
        arrayToMutate[1],
        {
          id: 're_0ede51667d7844a3',
          role: 'Technical Agent',
          exclusive: null,
          limit: 4,
          autoAssignLoggedInUser: true,
          autoAssignContact: true,
          __typename: 'Relationship',
        },
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - concat item to array if key not found', () => {
      const newItem = {
        id: 're_2079234510f8838e',
        role: 'Podcast Host',
        exclusive: null,
        limit: null,
        autoAssignLoggedInUser: null,
        autoAssignContact: null,
        __typename: 'Relationship',
      };

      const actualResult = arrayConcatOrUpdateByKey({
        arr: arrayToMutate,
        item: newItem,
      });
      const expectedResult = arrayToMutate.concat(newItem);

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - insert item in beginning of array if first=true', () => {
      const newItem = {
        id: 're_2079234510f8838e',
        role: 'Podcast Host',
        exclusive: null,
        limit: null,
        autoAssignLoggedInUser: null,
        autoAssignContact: null,
        __typename: 'Relationship',
      };

      const actualResult = arrayConcatOrUpdateByKey({
        arr: arrayToMutate,
        item: newItem,
        first: true,
      });
      const expectedResult = [newItem, ...arrayToMutate];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - update item in array if specific key is found', () => {
      const newItem = {
        id: 're_0ede51667d7844a3',
        role: 'Technical Agent',
        exclusive: null,
        limit: 3,
        autoAssignLoggedInUser: null,
        autoAssignContact: null,
        __typename: 'Relationship',
      };

      const key = 'role';
      const actualResult = arrayConcatOrUpdateByKey({
        arr: arrayToMutate,
        item: newItem,
        key,
      });

      const expectedResult = arrayToMutate.map((d) =>
        d[key] === newItem[key] ? newItem : d,
      );

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - swap primitive items in array if specific key is found', () => {
      arrayToMutate = [{name: 'Name1'}, {name: 'Name2'}, {name: 'Name3'}];

      const actualResult = arraySwapItems({
        arr: arrayToMutate,
        values: ['Name1', 'Name3'],
        key: 'name',
      });

      const expectedResult = [
        {name: 'Name3'},
        {name: 'Name2'},
        {name: 'Name1'},
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - swap object items in array if specific key is found', () => {
      arrayToMutate = [{name: 'Name1'}, {name: 'Name2'}, {name: 'Name3'}];

      const actualResult = arraySwapItems({
        arr: arrayToMutate,
        values: [
          {name: 'Name1', value: 1},
          {name: 'Name2', value: 2},
        ],
        key: 'name',
      });

      const expectedResult = [
        {name: 'Name2', value: 2},
        {name: 'Name1', value: 1},
        {name: 'Name3'},
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - swap mixed items in array if specific key is found', () => {
      arrayToMutate = [
        {name: 'Name1'},
        {name: 'Name2', value: 1, groups: [1, 2]},
        {name: 'Name3'},
      ];

      const actualResult = arraySwapItems({
        arr: arrayToMutate,
        values: [1, {name: 'Name2', value: 2}],
        key: 'name',
      });

      const expectedResult = [
        {name: 'Name2', groups: [1, 2], value: 2},
        {name: 'Name1'},
        {name: 'Name3'},
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - swap items in array based on index values', () => {
      arrayToMutate = [{name: 'Name1'}, {name: 'Name2'}, {name: 'Name3'}];

      const actualResult = arraySwapItems({
        arr: arrayToMutate,
        values: [2, 3],
      });

      const expectedResult = [
        {name: 'Name1'},
        {name: 'Name3'},
        {name: 'Name2'},
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - insert primitive value at end of array by default', () => {
      arrayToMutate = ['Name1', 'Name2', 'Name3'];

      let actualResult = insert({
        arr: arrayToMutate,
        item: 'Name4',
      });

      actualResult = insert({
        arr: actualResult,
        item: 'Name5',
      });

      const expectedResult = ['Name1', 'Name2', 'Name3', 'Name4', 'Name5'];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - insert item at end of array by default', () => {
      arrayToMutate = [{name: 'Name1'}, {name: 'Name2'}, {name: 'Name3'}];

      let actualResult = insert({
        arr: arrayToMutate,
        item: {name: 'Name4'},
      });

      actualResult = insert({
        arr: actualResult,
        item: {name: 'Name5'},
      });

      const expectedResult = [
        {name: 'Name1'},
        {name: 'Name2'},
        {name: 'Name3'},
        {name: 'Name4'},
        {name: 'Name5'},
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - insert item in beginning of array by index:0', () => {
      arrayToMutate = [{name: 'Name1'}, {name: 'Name2'}, {name: 'Name3'}];

      const actualResult = insert({
        arr: arrayToMutate,
        item: {name: 'Name4'},
        index: 0,
      });

      const expectedResult = [
        {name: 'Name4'},
        {name: 'Name1'},
        {name: 'Name2'},
        {name: 'Name3'},
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - insert item in array based on index', () => {
      arrayToMutate = [{name: 'Name1'}, {name: 'Name2'}, {name: 'Name3'}];

      const actualResult = insert({
        arr: arrayToMutate,
        item: {name: 'Name4'},
        index: 1,
      });

      const expectedResult = [
        {name: 'Name1'},
        {name: 'Name4'},
        {name: 'Name2'},
        {name: 'Name3'},
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('sad - insert item in undefined/null array', () => {
      arrayToMutate = undefined;
      const arrayToMutate2 = null;

      const actualResult = insert({
        arr: arrayToMutate,
        item: {name: 'Name4'},
        index: 0,
      });
      const actualResult2 = insert({
        arr: arrayToMutate2,
        item: {name: 'Name4'},
        index: 0,
      });

      const expectedResult = [{name: 'Name4'}];

      expect(actualResult).toEqual(expectedResult);
      expect(actualResult2).toEqual(expectedResult);
    });

    it('happy - update item in array by key of objects', () => {
      arrayToMutate = [{name: 'Name1'}, {name: 'Name2'}, {name: 'Name3'}];

      const actualResult = update({
        arr: arrayToMutate,
        item: {name: 'Name2', value: 'test'},
        key: 'name',
      });

      const expectedResult = [
        {name: 'Name1'},
        {name: 'Name2', value: 'test'},
        {name: 'Name3'},
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - update item in array by key and value of objects', () => {
      arrayToMutate = [{name: 'Name1'}, {name: 'Name2'}, {name: 'Name3'}];

      const actualResult = update({
        arr: arrayToMutate,
        item: {name: 'Name5'},
        key: 'name',
        value: 'Name2',
      });

      const expectedResult = [
        {name: 'Name1'},
        {name: 'Name5'},
        {name: 'Name3'},
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - update item in array based on index', () => {
      arrayToMutate = [{name: 'Name1'}, {name: 'Name2'}, {name: 'Name3'}];

      const actualResult = update({
        arr: arrayToMutate,
        item: {name: 'Name5'},
        index: 0,
      });

      const expectedResult = [
        {name: 'Name5'},
        {name: 'Name2'},
        {name: 'Name3'},
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - includeAll:false - update multiple items in array by key of objects', () => {
      arrayToMutate = [
        {name: 'Name1'},
        {name: 'Name2', status: 'Done'},
        {name: 'Name3'},
      ];

      const actualResult = update({
        arr: arrayToMutate,
        item: [
          {name: 'Name2', value: 'test 1'},
          {name: 'Name3', value: 'test 2'},
        ],
        key: 'name',
        includeAll: false,
      });

      const expectedResult = [
        {name: 'Name1'},
        {name: 'Name2', value: 'test 1'},
        {name: 'Name3', value: 'test 2'},
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - includeAll:true - update multiple items in array by key of objects', () => {
      arrayToMutate = [
        {name: 'Name1'},
        {name: 'Name2', status: 'Done'},
        {name: 'Name3'},
      ];

      const actualResult = update({
        arr: arrayToMutate,
        item: [
          {name: 'Name2', value: 'test 1', status: 'Not Done'},
          {name: 'Name3', value: 'test 2'},
        ],
        key: 'name',
        includeAll: true,
      });

      const expectedResult = [
        {name: 'Name1'},
        {name: 'Name2', value: 'test 1', status: 'Not Done'},
        {name: 'Name3', value: 'test 2'},
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - update all object items in array by key', () => {
      arrayToMutate = [
        {name: 'Name1'},
        {name: 'Name2', status: 'Done'},
        {name: 'Name3'},
      ];

      const actualResult = updateAll({
        arr: arrayToMutate,
        key: 'status',
        value: 'Not Done',
      });

      const expectedResult = [
        {name: 'Name1', status: 'Not Done'},
        {name: 'Name2', status: 'Not Done'},
        {name: 'Name3', status: 'Not Done'},
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - insert by index primitive value in array', () => {
      arrayToMutate = ['Name1', 'Name2', 'Name3'];

      const actualResult = insertOrUpdate({
        arr: arrayToMutate,
        item: 'Name5',
        index: 1,
      });

      const expectedResult = ['Name1', 'Name5', 'Name2', 'Name3'];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - move to index if primitive value exists in array', () => {
      arrayToMutate = ['Name1', 'Name2', 'Name3', 'Name5'];

      const actualResult = insertOrUpdate({
        arr: arrayToMutate,
        item: 'Name5',
        index: 1,
      });

      const expectedResult = ['Name1', 'Name5', 'Name2', 'Name3'];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - insert by index or update item in array by key', () => {
      arrayToMutate = [{name: 'Name1'}, {name: 'Name2'}, {name: 'Name3'}];

      const actualResult = insertOrUpdate({
        arr: arrayToMutate,
        item: {name: 'Name5'},
        key: 'name',
        index: 1,
      });

      const expectedResult = [
        {name: 'Name1'},
        {name: 'Name5'},
        {name: 'Name2'},
        {name: 'Name3'},
      ];

      expect(actualResult).toEqual(expectedResult);

      const actualResult2 = insertOrUpdate({
        arr: arrayToMutate,
        item: {name: 'Name2', randomField: true},
        key: 'name',
        index: 1,
      });

      const expectedResult2 = [
        {name: 'Name1'},
        {name: 'Name2', randomField: true},
        {name: 'Name3'},
      ];

      expect(actualResult2).toEqual(expectedResult2);
    });

    it("happy - insert primitive value at end of array if it doesn't exist", () => {
      arrayToMutate = ['Name1', 'Name2', 'Name3'];

      const actualResult = insertOrRemove({
        arr: arrayToMutate,
        item: 'Name4',
      });

      const expectedResult = ['Name1', 'Name2', 'Name3', 'Name4'];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - remove primitive value from array if it exists based on key', () => {
      arrayToMutate = ['Name1', 'Name2', 'Name3', 'Name4'];

      const actualResult = insertOrRemove({
        arr: arrayToMutate,
        item: 'Name4',
      });

      const expectedResult = ['Name1', 'Name2', 'Name3'];

      expect(actualResult).toEqual(expectedResult);
    });

    it("happy - insert item at end of array if it doesn't exist based on key", () => {
      arrayToMutate = [{name: 'Name1'}, {name: 'Name2'}, {name: 'Name3'}];

      const actualResult = insertOrRemove({
        arr: arrayToMutate,
        item: {name: 'Name4'},
        key: 'name',
      });

      const expectedResult = [
        {name: 'Name1'},
        {name: 'Name2'},
        {name: 'Name3'},
        {name: 'Name4'},
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - remove item from array if it exists based on key', () => {
      arrayToMutate = [
        {name: 'Name1'},
        {name: 'Name2'},
        {name: 'Name3'},
        {name: 'Name4'},
      ];

      const actualResult = insertOrRemove({
        arr: arrayToMutate,
        item: {name: 'Name4'},
        key: 'name',
      });

      const expectedResult = [
        {name: 'Name1'},
        {name: 'Name2'},
        {name: 'Name3'},
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - remove item in array based on index', () => {
      arrayToMutate = [{name: 'Name1'}, {name: 'Name2'}, {name: 'Name3'}];

      const actualResult = remove({
        arr: arrayToMutate,
        index: 1,
      });

      const expectedResult = [{name: 'Name1'}, {name: 'Name3'}];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - remove item in array based on key and value of objects', () => {
      arrayToMutate = [{name: 'Name1'}, {name: 'Name2'}, {name: 'Name3'}];

      const actualResult = remove({
        arr: arrayToMutate,
        key: 'name',
        value: 'Name2',
      });

      const expectedResult = [{name: 'Name1'}, {name: 'Name3'}];

      expect(actualResult).toEqual(expectedResult);
    });

    it('sad - remove item but missed value', () => {
      arrayToMutate = [{name: 'Name1'}, {name: 'Name2'}, {name: 'Name3'}];

      const actualResult = remove({
        arr: arrayToMutate,
        key: 'name',
        value: 'Name4',
      });

      const expectedResult = arrayToMutate;

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - move item in array based on from and to indexes', () => {
      arrayToMutate = [
        {name: 'Name1'},
        {name: 'Name2'},
        {name: 'Name3'},
        {name: 'Name4'},
      ];

      const actualResult = moveItem({
        arr: arrayToMutate,
        fromIndex: 3,
        toIndex: 0,
      });

      const expectedResult = [
        {name: 'Name4'},
        {name: 'Name1'},
        {name: 'Name2'},
        {name: 'Name3'},
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - move item in array based on key and value of objects', () => {
      arrayToMutate = [
        {name: 'Name1'},
        {name: 'Name2'},
        {name: 'Name3'},
        {name: 'Name4'},
      ];

      const actualResult = moveItem({
        arr: arrayToMutate,
        key: 'name',
        value: 'Name2',
        toIndex: 0,
      });

      const expectedResult = [
        {name: 'Name2'},
        {name: 'Name1'},
        {name: 'Name3'},
        {name: 'Name4'},
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - move item in array based on value of primitives', () => {
      arrayToMutate = ['Name1', 'Name2', 'Name3', 'Name4'];

      const actualResult = moveItem({
        arr: arrayToMutate,
        value: 'Name3',
        toIndex: 1,
      });

      const expectedResult = ['Name1', 'Name3', 'Name2', 'Name4'];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - add / remove primitive item in array if exists / not exists', () => {
      arrayToMutate = ['Name1', 'Name2', 'Name3', 'Name4'];

      const actualResult = toggleItem({
        arr: arrayToMutate,
        item: 'Name2',
      });

      const expectedResult = ['Name1', 'Name3', 'Name4'];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - add / remove object item in array if exists / not exists based on key and value', () => {
      arrayToMutate = [
        {name: 'Name1'},
        {name: 'Name2'},
        {name: 'Name3'},
        {name: 'Name4'},
      ];

      const actualResult = toggleItem({
        arr: arrayToMutate,
        item: {name: 'Name2'},
        key: 'name',
      });

      const expectedResult = [
        {name: 'Name1'},
        {name: 'Name3'},
        {name: 'Name4'},
      ];

      const actualResult2 = toggleItem({
        arr: arrayToMutate,
        item: {name: 'Name5'},
        key: 'name',
      });

      const expectedResult2 = [
        {name: 'Name1'},
        {name: 'Name2'},
        {name: 'Name3'},
        {name: 'Name4'},
        {name: 'Name5'},
      ];

      expect(actualResult).toEqual(expectedResult);
      expect(actualResult2).toEqual(expectedResult2);
    });

    it('happy - toggle primitive item in array if exists / not exists', () => {
      arrayToMutate = ['Name1'];

      const actualResult = toggleItem({
        arr: arrayToMutate,
        item: 'Name1',
      });

      const expectedResult = [];

      const actualResult2 = toggleItem({
        arr: expectedResult,
        item: 'Name1',
      });

      const expectedResult2 = ['Name1'];

      expect(actualResult).toEqual(expectedResult);
      expect(actualResult2).toEqual(expectedResult2);
    });
  });

  describe('traversing objects / arrays with CRUD', () => {
    let sampleObj, contactGroups, formBuilderItem;

    beforeEach(() => {
      sampleObj = {
        id: 'SAMPLE_OBJ_ID',
      };
      contactGroups = [
        {
          id: 'subGroup-General',
          subGroupName: 'General',
          fields: [
            {
              id: 'field_email',
              fieldId: 'field_email',
              label: 'Email',
              tooltip: 'This will be used as your ID',
              required: true,
              column: 1,
            },
          ],
          columns: 3,
        },
      ];
      formBuilderItem = {
        contactGroups,
      };
    });

    // it('happy - traverse object then read item attribute from nested array attribute', () => {
    //   const actualResult = traversRead(formBuilderItem, [
    //     'contactGroups:id:subGroup-General',
    //     'fields',
    //   ]);

    //   const expectedResult = {
    //     contactGroups: [
    //       {
    //         id: 'subGroup-General',
    //         subGroupName: 'General',
    //         fields: [
    //           {
    //             id: 'field_email',
    //             fieldId: 'field_email',
    //             label: 'Email',
    //             tooltip: 'This will be used as your ID',
    //             required: true,
    //             column: 1,
    //           },
    //           {
    //             id: 'field_firstname',
    //             fieldId: 'field_firstname',
    //             label: 'First Name',
    //             tooltip: null,
    //             required: false,
    //             column: 2,
    //           },
    //         ],
    //         columns: 3,
    //       },
    //     ],
    //   };

    //   expect(actualResult).toEqual(expectedResult);
    // });

    it('happy - traverseRead object 1 - traverse then read value', () => {
      const actualResult = traverseRead(sampleObj, ['id']);
      const actualResult2 = traverseRead(sampleObj, 'id');

      const expectedResult = 'SAMPLE_OBJ_ID';

      expect(actualResult).toEqual(expectedResult);
      expect(actualResult2).toEqual(expectedResult);
    });

    it('happy - traverseRead object 2 - traverse then read value in nested array attribute', () => {
      const actualResult = traverseRead(formBuilderItem, [
        'contactGroups',
        'id:subGroup-General',
        'fields',
      ]);

      const expectedResult = [
        {
          id: 'field_email',
          fieldId: 'field_email',
          label: 'Email',
          tooltip: 'This will be used as your ID',
          required: true,
          column: 1,
        },
      ];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - traverseRead object 3 - traverse then read value in nested array attribute', () => {
      const actualResult = traverseRead(formBuilderItem, [
        'contactGroups',
        'id:subGroup-General',
        'fields',
        'fieldId:field_email',
      ]);

      const expectedResult = {
        id: 'field_email',
        fieldId: 'field_email',
        label: 'Email',
        tooltip: 'This will be used as your ID',
        required: true,
        column: 1,
      };

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - traverseRead object 4 - traverse then read not existing id in nested array attribute', () => {
      const actualResult = traverseRead(formBuilderItem, [
        'contactGroups',
        'id:non_existing_id',
      ]);

      const expectedResult = undefined;

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - traverseRead array 1 - traverse then read object', () => {
      const actualResult = traverseRead(contactGroups, ['id:subGroup-General']);
      const actualResult2 = traverseRead(contactGroups, 'id:subGroup-General');

      const expectedResult = contactGroups[0];

      expect(actualResult).toEqual(expectedResult);
      expect(actualResult2).toEqual(expectedResult);
    });

    it('happy - traverseRead array 2 - traverse then read object value', () => {
      const actualResult = traverseRead(contactGroups, [
        'id:subGroup-General',
        'subGroupName',
      ]);

      const expectedResult = 'General';

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - traverse object then add item in nested array attribute', () => {
      const newField = {
        id: 'field_firstname',
        fieldId: 'field_firstname',
        label: 'First Name',
        tooltip: null,
        required: false,
        column: 2,
      };

      const actualResult = traverseAddEdit(
        formBuilderItem,
        ['contactGroups', 'id:subGroup-General', 'fields'],
        newField,
      );

      const expectedResult = {
        contactGroups: [
          {
            id: 'subGroup-General',
            subGroupName: 'General',
            fields: [
              {
                id: 'field_email',
                fieldId: 'field_email',
                label: 'Email',
                tooltip: 'This will be used as your ID',
                required: true,
                column: 1,
              },
              {
                id: 'field_firstname',
                fieldId: 'field_firstname',
                label: 'First Name',
                tooltip: null,
                required: false,
                column: 2,
              },
            ],
            columns: 3,
          },
        ],
      };

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - traverseAddEdit Edit 1 - edit object immediate primitive attribute', () => {
      const newField = {
        id: 'field_firstname',
        fieldId: 'field_firstname',
        label: 'First Name',
        tooltip: null,
        required: false,
        column: 2,
      };

      formBuilderItem.contactGroups[0].fields.push(newField);

      const actualResult = traverseAddEdit(
        newField,
        'fieldId',
        'Edited Attribute',
      );

      const expectedResult = {
        id: 'field_firstname',
        fieldId: 'Edited Attribute',
        label: 'First Name',
        tooltip: null,
        required: false,
        column: 2,
      };

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - traverseAddEdit Edit 1 - edit object array attribute', () => {
      const newField = {
        id: 'field_firstname',
        fieldId: 'field_firstname',
        label: 'First Name',
        tooltip: null,
        required: false,
        column: 2,
        testArr: [
          {
            id: '1',
            b: 'test',
          },
        ],
      };

      formBuilderItem.contactGroups[0].fields.push(newField);

      const actualResult = traverseAddEdit(
        newField,
        ['testArr', 'id:1', 'b'],
        'Edited Attribute',
      );

      const expectedResult = {
        id: 'field_firstname',
        fieldId: 'field_firstname',
        label: 'First Name',
        tooltip: null,
        required: false,
        column: 2,
        testArr: [
          {
            id: '1',
            b: 'Edited Attribute',
          },
        ],
      };

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - traverseAddEdit Edit 2 - edit object nested attribute', () => {
      const newField = {
        id: 'field_firstname',
        fieldId: 'field_firstname',
        label: 'First Name',
        tooltip: null,
        required: false,
        column: 2,
        test: {
          a: 1,
        },
      };

      formBuilderItem.contactGroups[0].fields.push(newField);

      const actualResult = traverseAddEdit(
        newField,
        ['test', 'a'],
        'Edited Attribute',
      );

      const expectedResult = {
        id: 'field_firstname',
        fieldId: 'field_firstname',
        label: 'First Name',
        tooltip: null,
        required: false,
        column: 2,
        test: {
          a: 'Edited Attribute',
        },
      };

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - traverseAddEdit Edit 3 - traverse object then edit item in nested array attribute', () => {
      const newField = {
        id: 'field_firstname',
        fieldId: 'field_firstname',
        label: 'First Name',
        tooltip: null,
        required: false,
        column: 2,
      };

      formBuilderItem.contactGroups[0].fields.push(newField);

      const actualResult = traverseAddEdit(
        formBuilderItem,
        ['contactGroups', 'id:subGroup-General', 'fields'],
        {
          ...newField,
          tooltip: 'Required',
          column: 1,
        },
      );

      const expectedResult = {
        contactGroups: [
          {
            id: 'subGroup-General',
            subGroupName: 'General',
            fields: [
              {
                id: 'field_email',
                fieldId: 'field_email',
                label: 'Email',
                tooltip: 'This will be used as your ID',
                required: true,
                column: 1,
              },
              {
                id: 'field_firstname',
                fieldId: 'field_firstname',
                label: 'First Name',
                tooltip: 'Required',
                required: false,
                column: 1,
              },
            ],
            columns: 3,
          },
        ],
      };

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - traverseAddEdit Edit 4 - traverse object then edit item in nested array attribute', () => {
      const newField = {
        id: 'field_firstname',
        fieldId: 'field_firstname',
        label: 'First Name',
        tooltip: null,
        required: false,
        column: 2,
      };

      formBuilderItem.contactGroups[0].fields.push(newField);

      const actualResult = traverseAddEdit(
        formBuilderItem,
        ['contactGroups', 'id:subGroup-General', 'fields'],
        {
          id: 'field_email',
          fieldId: 'field_email',
          label: 'Email',
          tooltip: 'This has changed',
          required: true,
          column: 1,
        },
      );

      const expectedResult = {
        contactGroups: [
          {
            id: 'subGroup-General',
            subGroupName: 'General',
            fields: [
              {
                id: 'field_email',
                fieldId: 'field_email',
                label: 'Email',
                tooltip: 'This has changed',
                required: true,
                column: 1,
              },
              {
                id: 'field_firstname',
                fieldId: 'field_firstname',
                label: 'First Name',
                tooltip: null,
                required: false,
                column: 2,
              },
            ],
            columns: 3,
          },
        ],
      };

      expect(actualResult).toEqual(expectedResult);
    });

    it('sad - traverse object without notations', () => {
      const newField = {
        id: 'field_firstname',
        fieldId: 'field_firstname',
        label: 'First Name',
        tooltip: null,
        required: false,
        column: 2,
      };

      formBuilderItem.contactGroups[0].fields.push(newField);

      const actualFunc = () =>
        traverseAddEdit(formBuilderItem, [], {
          tooltip: 'Required',
          column: 1,
        });

      expect(actualFunc).toThrowError(/required/);
    });

    it('happy - traverse object then delete item from nested array attribute', () => {
      const newField = {
        id: 'field_firstname',
        fieldId: 'field_firstname',
        label: 'First Name',
        tooltip: null,
        required: false,
        column: 2,
      };

      formBuilderItem.contactGroups[0].fields.push(newField);

      const actualResult = traverseDelete(formBuilderItem, [
        'contactGroups',
        'id:subGroup-General',
        'fields',
        'id:field_firstname',
      ]);

      const expectedResult = {
        contactGroups: [
          {
            id: 'subGroup-General',
            subGroupName: 'General',
            fields: [
              {
                id: 'field_email',
                fieldId: 'field_email',
                label: 'Email',
                tooltip: 'This will be used as your ID',
                required: true,
                column: 1,
              },
            ],
            columns: 3,
          },
        ],
      };

      expect(actualResult).toEqual(expectedResult);
    });
  });

  describe('validations', () => {
    let groupObj, commHistory, contactGroups;

    beforeEach(() => {
      groupObj = {
        id: 'subGroup-General',
        subGroupName: 'General',
        fields: [
          {
            id: 'field_email',
            fieldId: 'field_email',
            label: 'Email',
            tooltip: 'This will be used as your ID',
            required: true,
            addDivider: false,
            column: 1,
          },
        ],
        columns: 3,
        boolTrue: true,
        boolFalse: false,
      };
      commHistory = [
        {
          id: generateQueueId({
            contactId: '18',
            limit: 10,
            page: 0,
            type: 'email',
          }),
          tagFieldName: 'listCommunicationHistory',
          variables: {
            contactId: '18',
            limit: 10,
            page: 0,
            type: 'email',
          },
          boolTrue: true,
          boolFalse: false,
        },
      ];
      contactGroups = [groupObj];
    });

    it('hasItem - happy 1 - check if item exists in array of objects', async () => {
      const queueId = generateQueueId({
        contactId: '18',
        limit: 10,
        page: 0,
        type: 'email',
      });

      const actualResult = hasItemValue(commHistory, {id: queueId});

      expect(actualResult).toEqual(true);
    });

    it('hasItem - happy 2 - check if item exists in array of objects', async () => {
      const actualResult = hasItem(contactGroups, ['id:subGroup-General']);
      const actualResult2 = hasItem(contactGroups, 'id:subGroup-General');

      const expectedResult = true;

      expect(actualResult).toEqual(expectedResult);
      expect(actualResult2).toEqual(expectedResult);
    });

    it('hasItem - happy 3 - check if item exists in array of nested objects', async () => {
      const actualResult = hasItem(contactGroups, [
        'id:subGroup-General',
        'fields',
        'fieldId:field_email',
      ]);

      const expectedResult = true;

      expect(actualResult).toEqual(expectedResult);
    });

    it('hasItem - happy 4 - check if specific boolean value exists in object', async () => {
      const actualResult1 = hasItem(groupObj, ['boolTrue']);
      const actualResult2 = hasItem(groupObj, ['boolFalse']);

      const expectedResult1 = true;
      const expectedResult2 = false;

      expect(actualResult1).toEqual(expectedResult1);
      expect(actualResult2).toEqual(expectedResult2);
    });

    it('hasItem - happy 4 - check if specific boolean value exists in nested objects', async () => {
      const actualResult1 = hasItem(groupObj, [
        'fields',
        'fieldId:field_email',
        'required',
      ]);
      const actualResult2 = hasItem(groupObj, [
        'fields',
        'fieldId:field_email',
        'addDivider',
      ]);

      const expectedResult1 = true;
      const expectedResult2 = false;

      expect(actualResult1).toEqual(expectedResult1);
      expect(actualResult2).toEqual(expectedResult2);
    });

    it('hasItem - happy 5 - check if specific boolean value exists in array of nested objects', async () => {
      const actualResult1 = hasItem(contactGroups, [
        'id:subGroup-General',
        'fields',
        'fieldId:field_email',
        'required',
      ]);
      const actualResult2 = hasItem(contactGroups, [
        'id:subGroup-General',
        'fields',
        'fieldId:field_email',
        'addDivider',
      ]);

      const expectedResult1 = true;
      const expectedResult2 = false;

      expect(actualResult1).toEqual(expectedResult1);
      expect(actualResult2).toEqual(expectedResult2);
    });

    it('hasItem - sad 1 - check if item exists in array of objects', async () => {
      const actualResult = hasItem(contactGroups, ['id:non_existing_id']);
      const actualResult2 = hasItem(contactGroups, 'id:non_existing_id');

      const expectedResult = false;

      expect(actualResult).toEqual(expectedResult);
      expect(actualResult2).toEqual(expectedResult);
    });

    it('hasItem - sad 2 - check if item exists in array of objects', async () => {
      const actualResult = hasItem(contactGroups, [
        'id:subGroup-General',
        'fields',
        'fieldId:non_existing_id',
      ]);

      const expectedResult = false;

      expect(actualResult).toEqual(expectedResult);
    });

    it('hasDuplicates - happy - check if array of primitive values has duplicates', async () => {
      const arr = [1, 2, 1];

      const actualResult = hasDuplicates(arr);
      const expectedResult = true;

      expect(actualResult).toEqual(expectedResult);
    });

    it('hasDuplicates - sad - check if array of primitive values has duplicates', async () => {
      const arr = [1, 2];

      const actualResult = hasDuplicates(arr);
      const expectedResult = false;

      expect(actualResult).toEqual(expectedResult);
    });

    it('hasDuplicates - happy - check if array of objects has duplicates by key', async () => {
      const arr = [{a: 1}, {a: 2}, {a: 1}];

      const actualResult = hasDuplicates(arr, 'a');
      const expectedResult = true;

      expect(actualResult).toEqual(expectedResult);
    });

    it('hasDuplicates - sad - check if array of objects has duplicates by key', async () => {
      const arr = [{a: 1}, {a: 2}];

      const actualResult = hasDuplicates(arr, 'a');
      const expectedResult = false;

      expect(actualResult).toEqual(expectedResult);
    });
  });
});
