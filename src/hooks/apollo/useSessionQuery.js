import useQuery from './useQuery';
import useValue from '@macanta/hooks/base/useValue';
import {
  addActiveQuerySession,
  getQueryFetchPolicy,
  getTagFieldName,
} from '@macanta/utils/graphql';
import useFixedValue from '@macanta/hooks/base/useFixedValue';

const useSessionQuery = (graphqlTag, optionsArg) => {
  const tagFieldName = getTagFieldName(graphqlTag);

  const pathname = useFixedValue(window.location.pathname);
  const options = Object.assign({}, optionsArg, {
    dependencies: optionsArg?.dependencies || [pathname],
  });

  const [fetchPolicy] = useValue(
    () => getQueryFetchPolicy({graphqlTag, options}),
    [tagFieldName, options],
  );

  return useQuery(graphqlTag, {
    ...options,
    fetchPolicy,
    onCompleted() {
      addActiveQuerySession({graphqlTag, options});
    },
  });
};

export default useSessionQuery;
