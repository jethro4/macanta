import React from 'react';
import TurnedInIcon from '@mui/icons-material/TurnedIn';
import Box from '@mui/material/Box';
import Button from '@macanta/components/Button';
import {useFormikContext} from 'formik';

const ActionForm = ({children, btnProps, ...props}) => {
  const {dirty, handleSubmit} = useFormikContext();

  return (
    <Box
      sx={{
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
      }}
      {...props}>
      <Box
        sx={{
          flex: 1,
        }}>
        {children}
      </Box>

      <Box
        style={{
          display: 'flex',
          justifyContent: 'flex-end',
          padding: '12px 1rem',
          borderTop: '1px solid #eee',
          boxShadow: '0px -4px 10px -2px #eee',
        }}>
        <Button
          disabled={!dirty}
          variant="contained"
          startIcon={<TurnedInIcon />}
          onClick={handleSubmit}
          size="medium"
          {...btnProps}>
          Save
        </Button>
      </Box>
    </Box>
  );
};

export default ActionForm;
