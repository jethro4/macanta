import {
  buildDate,
  createDate,
  getNow,
  isFuture,
  isPast,
  isToday,
  isWithin,
} from '@macanta/utils/time';

describe('Time Utils', () => {
  beforeEach(() => {
    // Now = Oct 20, 2022 8:01 PM
    Date.now = jest.fn(() => new Date('2022-10-20T20:01:09+08:00'));
  });

  describe('buildDate', () => {
    it('October | 2022', () => {
      const month = 9,
        year = 2022;

      const actualResult = buildDate({month, year}).format('YYYY-MM-DD hh:mmA');
      const expectedResult = '2022-10-20 08:01PM';

      expect(actualResult).toEqual(expectedResult);
    });
  });

  describe('getNow', () => {
    it('with format', () => {
      const actualResult = getNow('YYYY-MM-DD hh:mmA');
      const expectedResult = '2022-10-20 08:01PM';

      expect(actualResult).toEqual(expectedResult);
    });
  });

  describe('isToday', () => {
    it('date value', () => {
      const oct19 = createDate('2022-10-19 00:00AM', 'YYYY-MM-DD hh:mmA');
      const oct20 = createDate('2022-10-20 00:00AM', 'YYYY-MM-DD hh:mmA');
      const oct21 = createDate('2022-10-21 00:00AM', 'YYYY-MM-DD hh:mmA');

      const actualResult1 = isToday(oct19);
      const actualResult2 = isToday(oct20);
      const actualResult3 = isToday(oct21);

      expect(actualResult1).toEqual(false);
      expect(actualResult2).toEqual(true);
      expect(actualResult3).toEqual(false);
    });

    it('string value - valid format', () => {
      const oct19 = '2022-10-19T00:00:00+08:00';
      const oct20 = '2022-10-20T00:00:00+08:00';
      const oct21 = '2022-10-21T00:00:00+08:00';

      const actualResult1 = isToday(oct19);
      const actualResult2 = isToday(oct20);
      const actualResult3 = isToday(oct21);

      expect(actualResult1).toEqual(false);
      expect(actualResult2).toEqual(true);
      expect(actualResult3).toEqual(false);
    });

    it('string value - invalid format', () => {
      const oct19 = '2022-10-19 00:00AM';
      const oct20 = '2022-10-20 00:00AM';
      const oct21 = '2022-10-21 00:00AM';

      const actualResult1 = isToday(oct19, {format: 'YYYY-MM-DD hh:mmA'});
      const actualResult2 = isToday(oct20, {format: 'YYYY-MM-DD hh:mmA'});
      const actualResult3 = isToday(oct21, {format: 'YYYY-MM-DD hh:mmA'});

      expect(actualResult1).toEqual(false);
      expect(actualResult2).toEqual(true);
      expect(actualResult3).toEqual(false);
    });
  });

  describe('isPast', () => {
    it('date value', () => {
      const oct19 = createDate('2022-10-19 00:00AM', 'YYYY-MM-DD hh:mmA');
      const oct20 = createDate('2022-10-20 00:00AM', 'YYYY-MM-DD hh:mmA');
      const oct21 = createDate('2022-10-21 00:00AM', 'YYYY-MM-DD hh:mmA');

      const actualResult1 = isPast(oct19);
      const actualResult2 = isPast(oct20);
      const actualResult3 = isPast(oct21);

      expect(actualResult1).toEqual(true);
      expect(actualResult2).toEqual(false);
      expect(actualResult3).toEqual(false);
    });

    it('string value - valid format', () => {
      const oct19 = '2022-10-19T00:00:00+08:00';
      const oct20 = '2022-10-20T00:00:00+08:00';
      const oct21 = '2022-10-21T00:00:00+08:00';

      const actualResult1 = isPast(oct19);
      const actualResult2 = isPast(oct20);
      const actualResult3 = isPast(oct21);

      expect(actualResult1).toEqual(true);
      expect(actualResult2).toEqual(false);
      expect(actualResult3).toEqual(false);
    });

    it('string value - invalid format', () => {
      const oct19 = '2022-10-19 00:00AM';
      const oct20 = '2022-10-20 00:00AM';
      const oct21 = '2022-10-21 00:00AM';

      const actualResult1 = isPast(oct19, {format: 'YYYY-MM-DD hh:mmA'});
      const actualResult2 = isPast(oct20, {format: 'YYYY-MM-DD hh:mmA'});
      const actualResult3 = isPast(oct21, {format: 'YYYY-MM-DD hh:mmA'});

      expect(actualResult1).toEqual(true);
      expect(actualResult2).toEqual(false);
      expect(actualResult3).toEqual(false);
    });
  });

  describe('isFuture', () => {
    it('date value', () => {
      const oct19 = createDate('2022-10-19 00:00AM', 'YYYY-MM-DD hh:mmA');
      const oct20 = createDate('2022-10-20 00:00AM', 'YYYY-MM-DD hh:mmA');
      const oct21 = createDate('2022-10-21 00:00AM', 'YYYY-MM-DD hh:mmA');

      const actualResult1 = isFuture(oct19);
      const actualResult2 = isFuture(oct20);
      const actualResult3 = isFuture(oct21);

      expect(actualResult1).toEqual(false);
      expect(actualResult2).toEqual(false);
      expect(actualResult3).toEqual(true);
    });

    it('string value - valid format', () => {
      const oct19 = '2022-10-19T00:00:00+08:00';
      const oct20 = '2022-10-20T00:00:00+08:00';
      const oct21 = '2022-10-21T00:00:00+08:00';

      const actualResult1 = isFuture(oct19);
      const actualResult2 = isFuture(oct20);
      const actualResult3 = isFuture(oct21);

      expect(actualResult1).toEqual(false);
      expect(actualResult2).toEqual(false);
      expect(actualResult3).toEqual(true);
    });

    it('string value - invalid format', () => {
      const oct19 = '2022-10-19 00:00AM';
      const oct20 = '2022-10-20 00:00AM';
      const oct21 = '2022-10-21 00:00AM';

      const actualResult1 = isFuture(oct19, {format: 'YYYY-MM-DD hh:mmA'});
      const actualResult2 = isFuture(oct20, {format: 'YYYY-MM-DD hh:mmA'});
      const actualResult3 = isFuture(oct21, {format: 'YYYY-MM-DD hh:mmA'});

      expect(actualResult1).toEqual(false);
      expect(actualResult2).toEqual(false);
      expect(actualResult3).toEqual(true);
    });
  });

  describe('isWithin', () => {
    it('date value', () => {
      const oct19 = createDate('2022-10-19 00:00AM', 'YYYY-MM-DD hh:mmA');
      const oct20 = createDate('2022-10-20 00:00AM', 'YYYY-MM-DD hh:mmA');
      const oct21 = createDate('2022-10-21 00:00AM', 'YYYY-MM-DD hh:mmA');
      const oct22 = createDate('2022-10-22 00:00AM', 'YYYY-MM-DD hh:mmA');

      const actualResult1 = isWithin(oct19, oct20);
      const actualResult2 = isWithin(oct20, oct21);
      const actualResult3 = isWithin(oct19, oct21);
      const actualResult4 = isWithin(oct21, oct22);

      expect(actualResult1).toEqual(true);
      expect(actualResult2).toEqual(true);
      expect(actualResult3).toEqual(true);
      expect(actualResult4).toEqual(false);
    });

    it('string value - valid format', () => {
      const oct19 = '2022-10-19T00:00:00+08:00';
      const oct20 = '2022-10-20T00:00:00+08:00';
      const oct21 = '2022-10-21T00:00:00+08:00';
      const oct22 = '2022-10-22T00:00:00+08:00';

      const actualResult1 = isWithin(oct19, oct20);
      const actualResult2 = isWithin(oct20, oct21);
      const actualResult3 = isWithin(oct19, oct21);
      const actualResult4 = isWithin(oct21, oct22);

      expect(actualResult1).toEqual(true);
      expect(actualResult2).toEqual(true);
      expect(actualResult3).toEqual(true);
      expect(actualResult4).toEqual(false);
    });

    it('string value - invalid format', () => {
      const oct19 = '2022-10-19 00:00AM';
      const oct20 = '2022-10-20 00:00AM';
      const oct21 = '2022-10-21 00:00AM';
      const oct22 = '2022-10-22 00:00AM';

      const actualResult1 = isWithin(oct19, oct20, {
        format: 'YYYY-MM-DD hh:mmA',
      });
      const actualResult2 = isWithin(oct20, oct21, {
        format: 'YYYY-MM-DD hh:mmA',
      });
      const actualResult3 = isWithin(oct19, oct21, {
        format: 'YYYY-MM-DD hh:mmA',
      });
      const actualResult4 = isWithin(oct21, oct22, {
        format: 'YYYY-MM-DD hh:mmA',
      });

      expect(actualResult1).toEqual(true);
      expect(actualResult2).toEqual(true);
      expect(actualResult3).toEqual(true);
      expect(actualResult4).toEqual(false);
    });
  });
});
