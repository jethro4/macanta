import {gql} from '@apollo/client';
import {WORKFLOW_BOARD_FRAGMENT} from './fragments';

export default gql`
  query listWorkflowBoards($type: String) {
    listWorkflowBoards(type: $type) {
      ...workflowBoardFragment
    }
  }
  ${WORKFLOW_BOARD_FRAGMENT}
`;
