import React from 'react';
import ConstructionIcon from '@mui/icons-material/Construction';
import SettingCard from '@macanta/modules/AdminSettings/SettingCard';
import {navigate} from 'gatsby';

const QueryBuilderCard = () => {
  /* const navigate = useNavigate();*/

  const handleQueryBuilder = () => {
    navigate('/app/admin-settings/query-builder');
  };

  return (
    <SettingCard
      IconComp={ConstructionIcon}
      title="Query Builder"
      description="Build queries to show specific data in dashboard widgets"
      onClick={handleQueryBuilder}
    />
  );
};

export default QueryBuilderCard;
