import React, {useState} from 'react';
import Panel from './Panel';
import * as Styled from './styles';

const Accordion = ({
  removeHidden,
  expanded: expandedProp,
  panels,
  ...props
}) => {
  const [expanded, setExpanded] = useState(expandedProp);

  const handleChange = (val) => {
    setExpanded(val);
  };

  return panels.length ? (
    <Styled.Root {...props}>
      {panels.map((item, index) => {
        const panelId = `panel${index + 1}`;

        return (
          <Panel
            key={panelId}
            expanded={expanded === panelId}
            id={panelId}
            item={item}
            onChange={handleChange}
            removeHidden={removeHidden}
          />
        );
      })}
    </Styled.Root>
  ) : null;
};

Accordion.defaultProps = {
  expanded: 'panel1',
  panels: [],
};

export default Accordion;
