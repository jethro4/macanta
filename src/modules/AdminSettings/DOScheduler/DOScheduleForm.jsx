import React from 'react';
import TurnedInIcon from '@mui/icons-material/TurnedIn';
import Box from '@mui/material/Box';
import Button from '@macanta/components/Button';
import Divider from '@macanta/components/Base/Divider';
import FormField from '@macanta/containers/FormField';
import withForm from '@macanta/components/Form/withForm';
import useDOFields from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOFields';
import useDORelationships from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDORelationships';
import FlexGrid from '@macanta/components/Base/FlexGrid';
import useValue from '@macanta/hooks/base/useValue';

const SCHEDULE_TYPES = [
  {
    label: 'Date',
    value: 'Date',
  },
  {
    label: 'Date & Time',
    value: 'DateTime',
  },
];

let DOScheduleForm = ({onSave, ...props}) => {
  const {values, errors, handleChange, dirty, setValues} = props;

  const isNew = !values.id;
  const isDateOnly = values.schedulerType === 'Date';

  const doFieldsQuery = useDOFields(values.groupId, {
    removeEmail: true,
  });
  const doRelationshipsQuery = useDORelationships(values.groupId);

  const doFields = doFieldsQuery.data?.listDataObjectFields;
  const doRelationships = doRelationshipsQuery.data?.listRelationships;

  const [filteredDOFields] = useValue(() => {
    return (
      doFields?.filter((item) =>
        isDateOnly ? item.type === 'Date' : item.type === 'DateTime',
      ) || []
    );
  }, [isDateOnly, doFields]);

  const handleChangeType = (val) => {
    setValues((state) => ({
      ...state,
      schedulerType: val,
      startDateFieldId: '',
      endDateFieldId: '',
    }));
  };

  const handleSave = () => {
    onSave(values);
  };

  return (
    <>
      <Box
        data-testid="DOScheduleForm"
        sx={{
          padding: '1rem 1rem 2rem',
          display: 'flex',
          flexDirection: 'column',
        }}>
        <FormField
          autoFocus={isNew}
          required
          labelPosition="normal"
          value={values.name}
          error={errors.name}
          onChange={handleChange('name')}
          label="Name"
          fullWidth
          size="small"
          variant="outlined"
        />

        <FormField
          type="TextArea"
          hideExpand
          labelPosition="normal"
          value={values.description}
          onChange={handleChange('description')}
          label="Description"
          fullWidth
          size="small"
          variant="outlined"
        />

        <FlexGrid cols={2} margin={['1.5rem', '1rem']}>
          <FormField
            loading={doRelationshipsQuery.initLoading}
            type="Select"
            labelPosition="normal"
            options={doRelationships}
            labelKey="role"
            valueKey="id"
            value={values.contactRelationshipId}
            error={errors.contactRelationshipId}
            onChange={handleChange('contactRelationshipId')}
            label="Contact Relationship"
            fullWidth
            size="small"
            variant="outlined"
          />

          <FormField
            loading={doRelationshipsQuery.initLoading}
            type="Select"
            labelPosition="normal"
            options={doRelationships}
            labelKey="role"
            valueKey="id"
            value={values.userRelationshipId}
            error={errors.userRelationshipId}
            onChange={handleChange('userRelationshipId')}
            label="User Relationship"
            fullWidth
            size="small"
            variant="outlined"
          />
        </FlexGrid>

        <Divider
          sx={{
            margin: '1.5rem 0 1rem',
          }}
        />

        <FormField
          style={{
            marginBottom: '1.5rem',
          }}
          type="Radio"
          row
          labelPosition="normal"
          options={SCHEDULE_TYPES}
          value={values.schedulerType}
          error={errors.schedulerType}
          onChange={handleChangeType}
        />

        <FlexGrid cols={2} margin={['1.5rem', '1rem']}>
          <FormField
            loading={doFieldsQuery.initLoading}
            style={{
              marginBottom: 0,
            }}
            type="Select"
            labelPosition="normal"
            options={filteredDOFields}
            labelKey="name"
            valueKey="id"
            value={values.startDateFieldId}
            error={errors.startDateFieldId}
            onChange={handleChange('startDateFieldId')}
            label={isDateOnly ? 'Date' : 'Start'}
            fullWidth
            size="small"
            variant="outlined"
          />

          {!isDateOnly && (
            <FormField
              loading={doFieldsQuery.initLoading}
              type="Select"
              labelPosition="normal"
              options={filteredDOFields}
              labelKey="name"
              valueKey="id"
              value={values.endDateFieldId}
              error={errors.endDateFieldId}
              onChange={handleChange('endDateFieldId')}
              label="End"
              fullWidth
              size="small"
              variant="outlined"
            />
          )}
        </FlexGrid>
      </Box>
      <Box
        style={{
          display: 'flex',
          justifyContent: 'flex-end',
          padding: '12px 1rem',
          borderTop: '1px solid #eee',
        }}>
        <Button
          disabled={!dirty}
          size="medium"
          variant="contained"
          startIcon={<TurnedInIcon />}
          onClick={handleSave}>
          Save
        </Button>
      </Box>
    </>
  );
};

DOScheduleForm = withForm(DOScheduleForm, {
  mapPropsToValues: (props) => ({
    ...props.item,
  }),
});

export default DOScheduleForm;
