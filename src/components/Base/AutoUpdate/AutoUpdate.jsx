import useInterval from '@macanta/hooks/base/useInterval';
import useValue from '@macanta/hooks/base/useValue';

const AutoUpdate = ({interval, renderComponent}) => {
  const {startedTime, triggeredTime, count, done} = useInterval({
    interval,
  });

  let [childrenComp] = useValue(() => {
    return renderComponent({startedTime, triggeredTime, count, done});
  }, [triggeredTime]);

  return childrenComp;
};

AutoUpdate.defaultProps = {
  interval: 60000,
};

export default AutoUpdate;
