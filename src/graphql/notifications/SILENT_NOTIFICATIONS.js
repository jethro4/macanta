import {gql} from '@apollo/client';

export default gql`
  mutation silentNotifications(
    $silentNotificationsInput: SilentNotificationsInput!
  ) {
    silentNotifications(input: $silentNotificationsInput) {
      success
    }
  }
`;
