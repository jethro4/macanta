import {gql} from '@apollo/client';

export default gql`
  mutation unassignListUsers($unassignListUsersInput: UnassignListUsersInput!) {
    unassignListUsers(input: $unassignListUsersInput) {
      success
    }
  }
`;
