import {isNumeric} from '@macanta/utils/numeric';
import isObject from 'lodash/isObject';
import isArray from 'lodash/isArray';
import isFunction from 'lodash/isFunction';

export const isEmpty = (value) => {
  return !isNumeric(value) && !value;
};

export const isNotObject = (value) => {
  return !isObject(value) && !isArray(value);
};

export const isPureObject = (value) => {
  return !isNotObject(value) && !isFunction(value);
};
