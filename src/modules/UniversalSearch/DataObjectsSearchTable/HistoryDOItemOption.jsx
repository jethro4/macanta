import React, {useState, useEffect} from 'react';
import DOItemHistory from './DOItemHistory';
import Modal from '@macanta/components/Modal';

const HistoryDOItemOption = ({
  itemId,
  modal,
  showModal: showModalProp = false,
  onClose,
}) => {
  const [showModal, setShowModal] = useState(showModalProp);

  const handleCloseDOHistory = () => {
    setShowModal(false);

    onClose && onClose();
  };

  useEffect(() => {
    setShowModal(showModalProp);
  }, [showModalProp]);

  let doItemHistory = <DOItemHistory itemId={itemId} />;

  if (modal) {
    doItemHistory = (
      <Modal
        headerTitle={`History (${itemId})`}
        open={showModal}
        onClose={handleCloseDOHistory}
        contentWidth={'100%'}
        contentHeight={'94%'}>
        {doItemHistory}
      </Modal>
    );
  }

  return doItemHistory;
};

export default HistoryDOItemOption;
