export const DEFAULT_MACANTA_PRIMARY_COLOR = '#663399';

export const generateColorsByThemeBaseColor = (color) => {
  return {
    primary: color,
    secondary: `${color}aa`,
    tertiary: `${color}88`,
    tint1: `${color}66`,
    tint2: `${color}44`,
    // static theme colors
    info: '#2196f3',
    warning: '#ff9800',
    error: '#d50000',
    success: '#0cb213',
    background: '#F4F4F4',
    textPrimary: '#333333',
    textSecondary: '#aaaaaa',
    textGray: '#555555',
    textGrayLight: '#888888',
    textBlue: '#7f8fa6',
    linkPrimary: '#2196f3',
    gray: '#f2f4f5',
    grayLight: '#F5F6FA',
    grayLighter: '#FAFBFC',
    grayLightest: '#FCFCFD',
    grayDark: '#cccccc',
    grayDarker: '#aaaaaa',
    grayDarkest: '#888888',
    white: '#FFFFFF',
    black: '#000000',
    green: '#4caf50',
    border: '#dcdde1',
    borderLight: '#e8e8e8',
    borderLighter: '#f0f0f0',
    borderLightest: '#f4f4f4',
  };
};

const Colors = generateColorsByThemeBaseColor(DEFAULT_MACANTA_PRIMARY_COLOR);

// export const setPrimaryColor = (color) => {
//   Colors.primary = color;
//   Colors.secondary = `${color}aa`;
//   Colors.tertiary = `${color}88`;
//   Colors.tint1 = `${color}66`;
//   Colors.tint2 = `${color}44`;
// };

// export const resetColors = () => {
//   Colors.primary = PRIMARY;
//   Colors.secondary = SECONDARY;
//   Colors.tertiary = TERTIARY;
//   Colors.tint1 = TINT1;
//   Colors.tint2 = TINT2;
// };

export default Colors;
