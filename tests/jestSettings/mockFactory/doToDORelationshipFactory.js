import {nextFactoryId} from './factory';

const typename = 'DOToDORelationship';

export default (overwrites) => {
  const {id} = nextFactoryId(typename);

  const item = {
    doItemId: `ItemDataObject1`,
    type: 'direct',
    data: [],
    ...overwrites,
  };

  return Object.assign(
    {
      __typename: typename,
      id,
      groupId: `DataObject1`,
    },
    item,
    overwrites,
  );
};
