import React from 'react';
import Section from '@macanta/containers/Section';
import NylasIntegrationCard from './NylasIntegrationCard';
import SignatureCard from './SignatureCard';
import BlastProgressCard from './BlastProgressCard';
import UserLevelTiersCard from './UserLevelTiersCard';
import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import * as Styled from './styles';

const UserSettings = () => {
  return (
    <NoteTaskHistoryStyled.FullHeightGrid container>
      <NoteTaskHistoryStyled.FullHeightFlexGridColumn item xs={12}>
        <Section
          fullHeight
          bodyStyle={{backgroundColor: '#f5f6fa', padding: '0.5rem'}}
          // loading={loading}
          // style={
          //   data.length && {
          //     backgroundColor: '#f5f6fa',
          //   }
          // }
          title="User Settings">
          <Styled.GridContainer container>
            <Styled.GridItem item lg={3} md={4} sm={6} xs={12}>
              <NylasIntegrationCard />
            </Styled.GridItem>
            <Styled.GridItem item lg={3} md={4} sm={6} xs={12}>
              <SignatureCard />
            </Styled.GridItem>
            <Styled.GridItem item lg={3} md={4} sm={6} xs={12}>
              <BlastProgressCard />
            </Styled.GridItem>
            <Styled.GridItem item lg={3} md={4} sm={6} xs={12}>
              <UserLevelTiersCard />
            </Styled.GridItem>
          </Styled.GridContainer>
        </Section>
      </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
    </NoteTaskHistoryStyled.FullHeightGrid>
  );
};

export default UserSettings;
