import isString from 'lodash/isString';
import isArray from 'lodash/isArray';
import {screen, within} from '@testing-library/react';
import {
  getParentScope,
  selectAll,
  selectByValue,
  selectLabelText,
  selectByText,
  selectors,
  selectText,
  transformOptions,
  waitUntil,
} from '@tests/jestSettings/utils/selectorUtils';
import {asyncPromises} from '@macanta/utils/promise';

// pass the test programmatically
export const passTest = () => expect(true).toBeTruthy();

export const inDocument = (elm) =>
  waitUntil(() => expect(elm).toBeInTheDocument());

export const notInDocument = async (elm) => {
  if (isArray(elm)) {
    if (!elm.length) {
      passTest();

      return;
    }

    await asyncPromises(elm, async (elmItem) => {
      await notInDocument(elmItem);
    });
  }

  await waitUntil(() => expect(elm).not.toBeInTheDocument());
};

export const isInDocument = async (value, options) => {
  let elm;

  if (value.nodeType) {
    elm = value;
  } else if (!isArray(value) && selectors[value]) {
    elm = selectAll(value, options);
  } else if (isArray(value) && isString(value[0])) {
    elm = selectText(value, options);
  } else if (isString(value)) {
    if (options?.isTestId) {
      elm = screen.getByTestId(value, options);
    } else if (options?.isLabelText) {
      elm = selectLabelText(value, options);
    } else if (options?.isValue) {
      elm = selectByValue(value, options);
    } else {
      elm = selectText(value, options);
    }
  }

  return !isArray(elm)
    ? await inDocument(elm)
    : await asyncPromises(elm, async (e) => {
        return await inDocument(e);
      });
};

export const isNotInDocument = async (value, options) => {
  if (value.nodeType) {
    await waitUntil(() => {
      const elm = value;

      expect(elm).not.toBeInTheDocument();
    });
  } else if (!isArray(value) && selectors[value]) {
    const parentScope = getParentScope(options);
    const elms = parentScope.querySelectorAll(selectors[value]);
    const elmsArr = Array.from(elms);

    await notInDocument(elmsArr);
  } else if (isString(value)) {
    const parentScope = getParentScope(options);
    const scope = within(parentScope);

    if (options?.isTestId) {
      await waitUntil(() => {
        expect(scope.queryByTestId(value)).not.toBeInTheDocument();
      });
    } else if (options?.isValue) {
      await waitUntil(() => {
        expect(scope.queryByDisplayValue(value)).not.toBeInTheDocument();
      });
    } else {
      await waitUntil(() => {
        expect(scope.queryByText(value)).not.toBeInTheDocument();
      });
    }
  } else {
    await waitUntil(() => {
      const elm = selectAll(value, transformOptions(options));

      expect(within(elm).queryByText(value)).not.toBeInTheDocument();
    });
  }
};

export const isChecked = async (selectors, options) => {
  const elm = selectAll(selectors, transformOptions(options));

  await waitUntil(() => expect(elm).toBeChecked());
};

export const isNotChecked = async (selectors, options) => {
  const elm = selectAll(selectors, transformOptions(options));

  await waitUntil(() => expect(elm).not.toBeChecked());
};

export const hasLength = async (selectors, value, options) => {
  const elms =
    !selectors[0] || !!selectors[0]?.nodeType
      ? selectors
      : selectAll(selectors, options);

  await waitUntil(() => expect(elms).toHaveLength(value));

  return elms;
};

export const hasTexts = async (selectorsArg, valuesArg, options) => {
  const selectors = isArray(selectorsArg) ? selectorsArg : [selectorsArg];
  const values = isArray(valuesArg) ? valuesArg : [valuesArg];
  const elms =
    !selectors[0] || !!selectors[0]?.nodeType
      ? selectors
      : selectAll(selectors, options);

  await asyncPromises(elms, async (element, index) => {
    const val = values[index];

    return !isArray(element)
      ? await isInDocument(val, element)
      : await asyncPromises(element, async (elm, index2) => {
          if (!val[index2]) {
            throw new MatcherException(
              `No text found on element index: ${index2}\n`,
              'Error on "hasTexts"',
              valuesArg,
            );
          }

          return await isInDocument(val[index2], elm);
        });
  });

  return elms;
};

export const hasLabelTexts = async (valuesArg) => {
  const values = isArray(valuesArg) ? valuesArg : [valuesArg];
  const elms = selectByText(valuesArg, 'listItem');

  await asyncPromises(elms, async (element, index) => {
    const val = values[index];

    if (!val) {
      throw new MatcherException(
        `No "aria-label" found on element index: ${index}\n`,
        'Error on "hasLabelTexts"',
        valuesArg,
      );
    }

    await inDocument(element);
  });

  return elms;
};

export const hasOptions = async (valuesArg) => {
  const values = isArray(valuesArg) ? valuesArg : [valuesArg];
  const elms = selectByText(valuesArg, 'selectItem');

  await asyncPromises(elms, async (element, index) => {
    const val = values[index];

    if (!val) {
      throw new MatcherException(
        `No option found on element index: ${index}\n`,
        'Error on "hasOptions"',
        valuesArg,
      );
    }

    await inDocument(element);
  });

  return elms;
};

export const hasValues = async (selectorsArg, valuesArg, options) => {
  const selectors = isArray(selectorsArg) ? selectorsArg : [selectorsArg];
  const values = isArray(valuesArg) ? valuesArg : [valuesArg];
  const elms =
    !selectors[0] || !!selectors[0]?.nodeType
      ? selectors
      : selectAll(selectors, options);

  await asyncPromises(values, async (val, index) => {
    if (!val) {
      throw new MatcherException(
        `No "value" found on element index: ${index}\n`,
        'Error on "hasValues"',
        valuesArg,
      );
    }

    await isInDocument(val, {
      parent: elms[index],
      isLabelText: true,
    });
  });

  return elms;
};

class MatcherException extends Error {
  constructor(message, title = '', value) {
    const newMessage = `${title}\nValue: ${value}\nMessage: ${message}`;
    super(newMessage);

    this.name = 'MatcherException';
  }
}
