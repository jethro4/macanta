import {experimentalStyled as styled} from '@mui/material/styles';

import Box from '@mui/material/Box';
import Button from '@macanta/components/Button';
import TextFieldComp from '@macanta/components/Forms';
import FormField from '@macanta/containers/FormField';
import AddTagsFieldComp from './AddTagsField';
import * as applicationStyles from '@macanta/themes/applicationStyles';

export const Root = styled(Box)`
  padding: 1rem 1rem 0;
`;

export const fieldStyles = () => `
  display: flex;
`;

export const TextField = styled(FormField)`
  ${fieldStyles}
`;

export const Select = styled(TextFieldComp)`
  ${fieldStyles}
`;

export const AddTagsField = styled(AddTagsFieldComp)`
  ${fieldStyles}
  margin-bottom: 0;
`;

export const AddTagsFieldRoot = styled(Box)`
  margin-bottom: ${({theme}) => theme.spacing(2)};
`;

export const AddTagsFieldFooter = styled(Box)`
  margin: ${({theme}) => theme.spacing(1)} 0;
  height: 3rem;
`;

export const Container = styled(Box)`
  padding: 0px ${({theme}) => theme.spacing(2)};
`;

export const FormGroup = styled(Box)`
  margin: ${({theme}) => theme.spacing(2)} 0px;
`;

export const Footer = styled(Box)`
  height: ${applicationStyles.DEFAULT_FOOTER_HEIGHT}px;
  display: flex;
  justify-content: flex-end;
  align-items: center;
  margin: ${({theme}) => theme.spacing(2)} 0;
`;

export const FooterButton = styled(Button)`
  margin-left: ${({theme}) => theme.spacing(2)};
`;
