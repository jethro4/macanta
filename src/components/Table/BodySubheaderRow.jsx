import React from 'react';
import clsx from 'clsx';
import * as Styled from './styles';

const BodySubheaderRow = ({
  item,
  columnsLength,
  className,
  transformSubheader,
  renderSubheader,
  ...props
}) => {
  let title = item.title;

  if (transformSubheader) {
    title = transformSubheader({value: title, item});
  }

  let tableBodySubheaderCellText = (
    <Styled.TableBodyCellLabel sx={item.textStyle}>
      {title}
    </Styled.TableBodyCellLabel>
  );

  if (renderSubheader) {
    tableBodySubheaderCellText = renderSubheader({
      value: title,
      origValue: item.title,
      item,
    });
  }

  return (
    <Styled.BodySubheaderRow
      className={clsx(className, 'item-subheader')}
      nested={item.nested}
      {...props}>
      <Styled.TableBodyCell colSpan={columnsLength + 1} sx={item.cellStyle}>
        {tableBodySubheaderCellText}
      </Styled.TableBodyCell>
    </Styled.BodySubheaderRow>
  );
};

export default BodySubheaderRow;
