import {useParams} from '@reach/router';
import useDOContactId from '@macanta/hooks/useDOContactId';
import * as Storage from '@macanta/utils/storage';

const useContactId = (options) => {
  const {userId: loggedInUserId} = Storage.getItem('userDetails');

  const params = useParams();
  const doPrimaryContactId = useDOContactId();

  const contactId =
    doPrimaryContactId ||
    params?.contactId ||
    (!options?.noDefault && loggedInUserId);

  return contactId;
};

export default useContactId;
