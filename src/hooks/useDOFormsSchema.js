import {useState, useEffect} from 'react';
import * as yup from 'yup';

const useDOFormsSchema = (fields) => {
  const [schema, setSchema] = useState(() => yup.object());

  useEffect(() => {
    if (fields?.length) {
      const schemaShapeFormat = fields.reduce((shape, field) => {
        if (field.required) {
          shape[field.name] = yup.string().required('Required');
        }

        return shape;
      }, {});
      const generatedSchema = yup.object({
        details: yup.object().shape(schemaShapeFormat),
        relationships: yup
          .array()
          .of(
            yup.object().shape({
              connectedContactId: yup.string(),
              relationships: yup
                .array()
                .required('Required')
                .min(1, 'Required at least one relationship'),
            }),
          )
          .required('Required')
          .min(1, 'Required at least one connected contact'),
      });

      setSchema(generatedSchema);
    }
  }, [fields]);

  return schema;
};

export default useDOFormsSchema;
