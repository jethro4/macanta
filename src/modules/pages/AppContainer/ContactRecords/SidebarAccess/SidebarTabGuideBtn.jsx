import React from 'react';
import HelperTextBtn from '@macanta/containers/FormField/HelperTextBtn';
import useSidebarTabGuide from '@macanta/hooks/useSidebarTabGuide';

const SidebarTabGuideBtn = ({tab}) => {
  const sidebarTabGuide = useSidebarTabGuide(tab);

  return <HelperTextBtn>{sidebarTabGuide?.value}</HelperTextBtn>;
};

export default SidebarTabGuideBtn;
