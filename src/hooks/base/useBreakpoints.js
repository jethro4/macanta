import {useTheme} from '@mui/material/styles';
import useWindowDimensions from '@macanta/hooks/base/useWindowDimensions';
import {sortDescendingBreakpoints} from '@macanta/components/Layout/Grid/helpers';

const useBreakpoints = () => {
  const dimensions = useWindowDimensions();
  const theme = useTheme();

  const sortedBreakpoints = sortDescendingBreakpoints(theme.breakpoints.values);

  const {
    filteredBreakpoints,
    currentBreakpoint,
    currentBreakpointVal,
  } = sortedBreakpoints.reduce(
    (acc, entry) => {
      let accObj = {...acc};
      const breakpointName = entry[0];
      const breakpointValue = entry[1];

      if (breakpointValue <= dimensions.width) {
        const filteredBreakpointsObj = accObj.filteredBreakpoints;

        accObj.filteredBreakpoints = {
          ...filteredBreakpointsObj,
          [breakpointName]: breakpointValue,
        };

        if (!accObj.currentBreakpoint) {
          accObj.currentBreakpoint = breakpointName;
          accObj.currentBreakpointVal = breakpointValue;
        }
      }

      return accObj;
    },
    {filteredBreakpoints: {}},
  );

  const breakpoints = sortedBreakpoints.reduce((acc, entry) => {
    const accObj = {...acc};

    const breakpointName = entry[0];
    const breakpointValue = entry[1];

    accObj[breakpointName] = breakpointValue;

    return accObj;
  }, {});

  return {
    breakpoints,
    filteredBreakpoints,
    currentBreakpoint,
    currentBreakpointVal,
  };
};

export default useBreakpoints;
