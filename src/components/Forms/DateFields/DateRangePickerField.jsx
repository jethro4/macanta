import React, {useState} from 'react';
import Box from '@mui/material/Box';
import AdapterDateFns from '@mui/x-date-pickers/AdapterDateFns';
import LocalizationProvider from '@mui/x-date-pickers/LocalizationProvider';
import DateRangePicker from '@mui/x-date-pickers/DateRangePicker';
import TextField from '@macanta/components/Forms/InputFields/TextField';
import {
  convertToDate,
  formatDateToLocaleString,
} from '@macanta/components/Forms/InputFields/helpers';
import useNextEffect from '@macanta/hooks/base/useNextEffect';

const DATE_INPUT_FORMAT = 'yyyy-MM-dd';
const DATE_INPUT_MASK = '____-__-__';
const DATE_OUTPUT_FORMAT = 'YYYY-MM-DD';

const DateRangePickerField = ({
  DateRangePickerProps,
  startValue,
  endValue,
  onChange,
  ...props
}) => {
  const [selectedDateRange, setSelectedDateRange] = useState(() => [
    convertToDate(startValue, DATE_OUTPUT_FORMAT),
    convertToDate(endValue, DATE_OUTPUT_FORMAT),
  ]);

  const handleDateChange = ([fromDate, toDate]) => {
    setSelectedDateRange([fromDate, toDate]);
    onChange([
      formatDateToLocaleString(fromDate, DATE_OUTPUT_FORMAT),
      formatDateToLocaleString(toDate, DATE_OUTPUT_FORMAT),
    ]);
  };

  useNextEffect(() => {
    setSelectedDateRange([
      convertToDate(startValue, DATE_OUTPUT_FORMAT),
      convertToDate(endValue, DATE_OUTPUT_FORMAT),
    ]);
  }, [startValue, endValue]);

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <DateRangePicker
        startText="From"
        endText="To"
        value={selectedDateRange}
        onChange={handleDateChange}
        renderInput={(startProps, endProps) => (
          <React.Fragment>
            <TextField {...startProps} {...props} />
            <Box sx={{mx: 2}}> to </Box>
            <TextField {...endProps} {...props} />
          </React.Fragment>
        )}
        {...DateRangePickerProps}
        inputFormat={DATE_INPUT_FORMAT}
        mask={DATE_INPUT_MASK}
      />
    </LocalizationProvider>
  );
};

export default DateRangePickerField;
