import React from 'react';
import SelectAction from '@macanta/modules/AdminSettings/Automation/TriggerActions/SubForms/SelectAction';
import {useFieldActions} from '@macanta/hooks/automation';

const Select = ({...props}) => {
  const fieldActionsQuery = useFieldActions({
    variables: {
      id: '',
    },
  });

  return (
    <SelectAction
      loading={fieldActionsQuery.initLoading}
      type="Select"
      options={fieldActionsQuery.result}
      labelKey="name"
      valueKey="id"
      {...props}
    />
  );
};

export default Select;
