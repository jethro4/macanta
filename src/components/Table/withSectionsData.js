import React from 'react';
import useValue from '@macanta/hooks/base/useValue';
import {groupBy} from '@macanta/utils/array';

const withSectionsData = (WrappedComponent, optionsArg) => {
  const {groupBy: groupByAttr, titleKey, dataKey} = Object.assign(
    {dataKey: 'data'},
    optionsArg,
  );

  return (props) => {
    const dataProp = props[dataKey];

    const [data] = useValue(() => {
      if (!dataProp) {
        return [];
      }

      const groupedArr = !groupByAttr
        ? dataProp
        : groupBy(dataProp, groupByAttr);

      return groupedArr.reduce((acc, d) => {
        const accArr = [...acc];

        accArr.push({
          isSubheader: true,
          title: d[titleKey || groupByAttr],
        });

        d.items.forEach((item) => {
          accArr.push(item);
        });

        return accArr;
      }, []);
    }, [dataProp]);

    return <WrappedComponent {...props} data={data} />;
  };
};

export default withSectionsData;
