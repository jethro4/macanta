import React, {useRef} from 'react';
import AddIcon from '@mui/icons-material/Add';
import Chip from '@mui/material/Chip';
import MenuList from '@mui/material/MenuList';
import {SelectMenuItems} from '@macanta/components/Forms/ChoiceFields/SelectField';
import Button, {PopoverButton} from '@macanta/components/Button';
import useTransformChoices from '@macanta/hooks/base/useTransformChoices';
import useValue from '@macanta/hooks/base/useValue';
import TextAreaField from '@macanta/components/Forms/InputFields/TextAreaField';

let SearchCustomField = ({
  value: valueProp,
  options: optionsProp,
  labelKey,
  valueKey,
  subLabelKey,
  captionKey,
  sort,
  actionButtonTitle,
  actionButtonProps,
  renderValue,
  renderChipValue,
  renderChipValues,
  hideChipValues,
  onChange,
  style,
  inputRef,
  ...props
}) => {
  const options = useTransformChoices({
    options: optionsProp,
    sort,
    labelKey,
    valueKey,
    subLabelKey,
    captionKey,
    IconComp: (
      <AddIcon
        color="primary"
        style={{
          marginLeft: -10,
          marginRight: 8,
        }}
      />
    ),
  });

  const handleRenderValue = (val) => {
    return renderValue ? renderValue(val) : val || '';
  };

  const [value, setValue] = useValue(() => handleRenderValue(valueProp), [
    valueProp,
  ]);

  const textFieldRef = useRef(inputRef);

  const registerCursorPosition = (position) => {
    textFieldRef.current.selectionStart = textFieldRef.current.selectionEnd = position;
  };

  const handleKeyDown = (e) => {
    registerCursorPosition(e.target.selectionStart);
  };

  const handleChange = (val) => {
    setValue(val);
    onChange && onChange(val);
  };

  const handleSelectOption = (newVal) => {
    let finalVal = value.trim().concat(`${value ? ' ' : ''}${newVal} `);

    handleChange(finalVal);

    setTimeout(() => {
      registerCursorPosition(finalVal.length + 2);

      textFieldRef.current.focus();
    }, 0);
  };

  const handleDeleteMultiItem = (deletedVal) => () => {
    alert('Delete feature is in progress', deletedVal);
    // handleSelectOption(value.filter((val) => val !== deletedVal));
  };

  const handlePopoverMouseEvent = (event) => {
    event.stopPropagation();
  };

  return (
    <PopoverButton
      size="small"
      variant="contained"
      PopoverProps={{
        contentWidth: 380 - 32,
        hideHeader: true,
        disableAutoFocus: true,
        transformOrigin: {
          vertical: -44,
          horizontal: 'right',
        },
      }}
      renderButton={(handleOpen) => (
        <TextAreaField
          {...props}
          inputRef={textFieldRef}
          autoSize
          value={value}
          onChange={handleChange}
          onKeyDown={handleKeyDown}
          style={{
            width: '100%',
            height: '100%',
            ...style,
          }}
          InputProps={{
            ...(value.length && {
              startAdornment:
                !hideChipValues &&
                (renderChipValues
                  ? renderChipValues(value, options)
                  : value.map((val) => {
                      const selectedItem = options.find(
                        (option) => option.value === val,
                      );

                      return selectedItem ? (
                        renderChipValue ? (
                          renderChipValue(selectedItem)
                        ) : (
                          <Chip
                            key={val}
                            label={selectedItem.label}
                            sx={{
                              mt: '4px',
                              mr: '8px',
                              '.MuiChip-label': {
                                pointerEvents: 'none',
                              },
                              '.MuiChip-deleteIcon': {
                                fontSize: '16px',
                              },
                            }}
                            style={{
                              fontSize: 13,
                              lineHeight: '13px',
                              height: 28,
                            }}
                            onClick={handlePopoverMouseEvent}
                            onMouseUp={handlePopoverMouseEvent}
                            onMouseDown={handlePopoverMouseEvent}
                            onDelete={handleDeleteMultiItem(val)}
                          />
                        )
                      ) : null;
                    })),
            }),
            endAdornment: (
              <Button
                sx={{
                  '.MuiButton-endIcon': {
                    marginLeft: '4px',
                  },
                }}
                style={{
                  padding: 0,
                  position: 'absolute',
                  right: 0,
                  top: -26,
                }}
                color="info"
                size="small"
                endIcon={<AddIcon />}
                onClick={handleOpen}
                {...actionButtonProps}>
                {actionButtonTitle}
              </Button>
            ),
            ...props.InputProps,
            style: {
              display: 'flex',
              flexWrap: 'wrap',
              ...props.InputProps?.style,
            },
          }}
          inputProps={{
            ...props.inputProps,
            style: {
              fontSize: 14,
              flexGrow: 1,
              flexBasis: 0,
              overflowX: 'hidden',
              minWidth: 100,
              ...props.inputProps?.style,
            },
          }}
        />
      )}
      renderContent={(handleClose) => (
        <MenuList
          style={{
            height: '100%',
            padding: 0,
          }}>
          <SelectMenuItems
            popover
            withDivider
            // value={value}
            options={options}
            allowSearch={options.length > 8}
            onChange={(val) => {
              handleSelectOption(val);
              handleClose();
            }}
          />
          {/* <MultiSelectItems
            // loading={loading}
            options={options}
            // searchValue={search}
            // subLabelKey="email"
            selectedValue={value}
            // onSearch={handleSearch}
            onChange={(val) => {
              handleSelectOption(val);
              // handleClose();
            }}
            allowSearch={false}
            // disableSearchAutoFocus
            disableAutoSort
          /> */}
        </MenuList>
      )}
    />
  );
};

SearchCustomField.defaultProps = {
  actionButtonTitle: 'Add',
};

export default SearchCustomField;
