import {useRef, useMemo} from 'react';
import isEqual from 'lodash/isEqual';
import usePrevious from '@macanta/hooks/usePrevious';

const useDeepCompareMemo = (callback, dependencies) => {
  const prevDependencies = usePrevious(dependencies);
  const prevMemoizedValueRef = useRef(null);

  const memoizedValue = useMemo(() => {
    if (isEqual(dependencies, prevDependencies)) {
      return prevMemoizedValueRef.current;
    }

    prevMemoizedValueRef.current = callback();

    return prevMemoizedValueRef.current;
  }, dependencies);

  return memoizedValue;
};

export default useDeepCompareMemo;
