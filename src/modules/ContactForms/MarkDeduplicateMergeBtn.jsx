import React, {useState} from 'react';
import MergeTypeIcon from '@mui/icons-material/MergeType';
import UndoIcon from '@mui/icons-material/Undo';
import Button from '@macanta/components/Button';
import useShowContactMergeButton from '@macanta/hooks/contact/useShowContactMergeButton';

const MarkDeduplicateMergeBtn = ({onTrigger, style}) => {
  const [deduplicateTriggered, setDeduplicateTriggered] = useState(false);

  const showContactMergeButton = useShowContactMergeButton();

  const handleMarkContacts = () => {
    const isTriggered = !deduplicateTriggered;

    setDeduplicateTriggered(isTriggered);
    onTrigger(isTriggered);
  };

  return (
    showContactMergeButton && (
      <Button
        style={style}
        onClick={handleMarkContacts}
        color="primary"
        size="small"
        variant="outlined"
        startIcon={!deduplicateTriggered ? <MergeTypeIcon /> : <UndoIcon />}>
        {!deduplicateTriggered ? 'Mark' : 'Undo'} Contacts for De-Duplication
      </Button>
    )
  );
};

export default MarkDeduplicateMergeBtn;
