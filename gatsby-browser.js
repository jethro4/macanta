/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/browser-apis/
 */

import sentryConfig from '@macanta/config/sentryConfig';
import {clearSessionVars} from '@macanta/graphql/cache/sessionVars';
import {setIsLoggedIn, setIsNewVersionAvailable} from '@macanta/utils/app';
// import envConfig from '@macanta/config/envConfig';
import * as Storage from '@macanta/utils/storage';

// // Start the mocking conditionally.
// if (envConfig.isDev) {
//   const {worker} = require('@macanta/jestSettings/mockBrowser');
//   worker.start();
// }

require('typeface-roboto');

export {wrapRootElement} from './gatsby-ssr';

export const onClientEntry = () => {
  const session = Storage.getItem('session');
  Storage.setItem('session', {...session, lastUrl: ''});

  sentryConfig.initialize();

  clearSessionVars();

  setIsLoggedIn(!!session?.sessionId && !!session?.apiKey);
  setIsNewVersionAvailable(false);
};

export const onServiceWorkerUpdateReady = () => {
  console.info('Service worker has updated');
};
