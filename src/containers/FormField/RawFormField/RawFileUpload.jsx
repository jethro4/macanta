import React from 'react';
import FileUpload from '@macanta/components/FileUpload';

const RawFileUpload = (props) => {
  return (
    <FileUpload
      style={{
        pointerEvents: 'none',
      }}
      {...props}
    />
  );
};

export default RawFileUpload;
