import React, {useState} from 'react';
import ButtonGroup from '@macanta/components/ButtonGroup';
import Show from '@macanta/containers/Show';
import BodySection from '@macanta/modules/AdminSettings/UserManagement/BodySection';
import {arrayConcatOrUpdateByKey} from '@macanta/utils/array';
import GeneralInterfaceGuideForm from './GeneralInterfaceGuideForm';
import SidebarTabsGuideForm from './SidebarTabsGuideForm';
import DOSectionsGuideForm from './DOSectionsGuideForm';
import * as DOStyled from '@macanta/modules/DataObjectItems/styles';

const USER_GUIDE_FORM_TABS = [
  {
    label: 'General Interface Guide',
    value: 'generalInterface',
    component: GeneralInterfaceGuideForm,
  },
  {
    label: 'Sidebar Tabs',
    value: 'sidebarTabs',
    component: SidebarTabsGuideForm,
  },
  {
    label: 'Data Object Sections',
    value: 'sections',
    component: DOSectionsGuideForm,
  },
];

const UserGuidesForm = ({permission, onChange, ...props}) => {
  const [selectedTab, setSelectedTab] = useState(USER_GUIDE_FORM_TABS[0].value);

  const handleSelectTab = (val) => () => {
    setSelectedTab(val);
  };

  const handleChange = ({item, value}) => {
    const updatedPermission = {
      ...permission,
      userGuides: arrayConcatOrUpdateByKey({
        arr: permission?.userGuides,
        item: {
          ...item,
          value,
        },
        key: 'guideId',
        includeAll: true,
      }),
    };

    onChange(updatedPermission);
  };

  return (
    <BodySection
      HeaderRightComp={
        <ButtonGroup variant="text">
          {USER_GUIDE_FORM_TABS.map((tab) => {
            return (
              <DOStyled.DOItemNavBtn
                key={tab.value}
                onClick={handleSelectTab(tab.value)}>
                {tab.label}
              </DOStyled.DOItemNavBtn>
            );
          })}
        </ButtonGroup>
      }
      {...props}>
      {USER_GUIDE_FORM_TABS.map((tab) => {
        return (
          <Show key={tab.value} removeHidden in={selectedTab === tab.value}>
            <tab.component
              title={tab.label}
              permission={permission}
              onChange={handleChange}
            />
          </Show>
        );
      })}
    </BodySection>
  );
};

export default UserGuidesForm;
