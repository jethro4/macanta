import * as React from 'react';
import {FixedSizeList} from 'react-window';

const DEFAULT_OVERSCAN_COUNT = 4;

const VirtualizedList = ({data, renderRow, keyExtractor, ...props}) => {
  return (
    <FixedSizeList
      itemData={data}
      itemCount={data.length}
      itemKey={keyExtractor}
      overscanCount={DEFAULT_OVERSCAN_COUNT}
      {...props}>
      {renderRow}
    </FixedSizeList>
  );
};

VirtualizedList.defaultProps = {
  data: [],
  renderRow: () => null,
  keyExtractor: (item, index) => item?.id || index,
};

export default VirtualizedList;
