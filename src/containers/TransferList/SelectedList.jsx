import React, {useState, useLayoutEffect} from 'react';
import {Responsive, WidthProvider} from 'react-grid-layout';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import {sortArrayByObjectKey, moveItem} from '@macanta/utils/array';
import Search from './Search';
import SelectedCard from './SelectedCard';
import * as WidgetStyled from '@macanta/modules/pages/AppContainer/Widgets/styles';
import * as Styled from './styles';

const ResponsiveReactGridLayout = WidthProvider(Responsive);

const SelectedList = ({
  data,
  onSearch,
  hideEmptyMessage,
  emptyMessage,
  onSort,
  onDelete,
  error,
}) => {
  const [layouts, setLayouts] = useState({});

  const handleDragStop = (layout, oldItem, newItem) => {
    const hasSwitched = oldItem.x !== newItem.x || oldItem.y !== newItem.y;
    const sortedLayout = sortArrayByObjectKey(layout, 'y');

    if (hasSwitched) {
      const updatedLayouts = generateLayouts(
        sortedLayout.map((l) => ({id: l.i})),
      );

      setLayouts(updatedLayouts);

      handleSortData(oldItem.y, newItem.y);
    }
  };

  const handleSortData = (fromIndex, toIndex) => {
    const sortedData = moveItem({
      arr: data,
      fromIndex,
      toIndex,
    });

    onSort(sortedData);
  };

  useLayoutEffect(() => {
    const updatedLayouts = generateLayouts(data);

    setLayouts(updatedLayouts);
  }, [data]);

  return (
    <Box
      style={{
        display: 'flex',
        flexDirection: 'column',
        flexGrow: 1,
        flexBasis: 0,
        overflowX: 'hidden',
        padding: '1rem',
      }}>
      {onSearch && (
        <Search
          style={{
            marginBottom: '4px',
          }}
          size="xsmall"
          onSearch={onSearch}
        />
      )}
      <Paper
        style={{
          flex: 1,
          overflowX: 'hidden',
          overflowY: 'auto',
          borderTop: '1px solid #F5F5F5',
          backgroundColor: '#e4e4e4',
        }}>
        {!!data.length && (
          <ResponsiveReactGridLayout
            layouts={layouts}
            rowHeight={40}
            margin={[12, 8]}
            cols={{lg: 1, md: 1, sm: 1, xs: 1, xxs: 1}}
            isBounded
            isResizable={false}
            compactType="vertical"
            onDragStop={handleDragStop}>
            {data.map((item) => {
              return (
                <WidgetStyled.ReactGridItemContainer key={item.value}>
                  <SelectedCard item={item} onDelete={onDelete} />
                </WidgetStyled.ReactGridItemContainer>
              );
            })}
          </ResponsiveReactGridLayout>
        )}

        {!!error && data.length === 0 && (
          <Typography
            color="error"
            style={{
              fontSize: '0.75rem',
              textAlign: 'center',
              marginTop: '1rem',
            }}>
            {error}
          </Typography>
        )}

        {!hideEmptyMessage && data.length === 0 && (
          <Styled.EmptyMessage
            style={{
              fontSize: '1rem',
              paddingTop: '2rem',
              paddingBottom: '2rem',
            }}
            align="center"
            color="#888">
            {emptyMessage}
          </Styled.EmptyMessage>
        )}
      </Paper>
    </Box>
  );
};

const generateLayouts = (items) => {
  return ['lg', 'md', 'sm', 'xs', 'xxs'].reduce((acc, col) => {
    if (items) {
      acc[col] = items.map((item, index) => {
        return {
          x: 0,
          y: index,
          w: 1,
          h: 1,
          i: String(item.id),
        };
      });
    }

    return acc;
  }, {});
};

SelectedList.defaultProps = {
  emptyMessage: 'No items selected',
};

export default SelectedList;
