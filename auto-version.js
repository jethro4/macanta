const {exec} = require('child_process');
const fs = require('fs');
const os = require('os');

const packageFileName = './package.json';
const versionFileName = './static/version.json';
const pjson = require(packageFileName);

const stage = process.env.NODE_ENV;

if (!stage) {
  console.info('Env stage is undefined');
} else {
  console.info('Auto version stage: ' + stage);

  const envFileName = `./.env.${stage}`;

  const type = process.argv[2];

  const versionArr = pjson.version.split('.');
  let major = parseInt(versionArr[0]);
  let minor = parseInt(versionArr[1]);
  let patch = parseInt(versionArr[2]);

  let buildNumber = parseInt(pjson.buildNumber);

  switch (type) {
    case 'major': {
      major++;
      minor = 0;
      patch = 0;
      break;
    }
    case 'minor': {
      minor++;
      patch = 0;
      break;
    }
    case 'patch':
    default: {
      patch++;
      break;
    }
  }

  pjson.version = [major, minor, patch].join('.');
  pjson.buildNumber = String(++buildNumber);

  const packageJsonData = JSON.stringify(pjson, null, 2);
  const versionJsonData = JSON.stringify(
    {
      version: pjson.version,
    },
    null,
    2,
  );

  console.info('VERSIONS & BUILDER NUMBER', pjson.version, pjson.buildNumber);

  updatePackageJson(packageJsonData);
  updateVersionJson(versionJsonData);
  prettyPrintJsonFilees();
  updateReleaseInEnv(envFileName);
}

function updatePackageJson(jsonData) {
  fs.writeFile(packageFileName, jsonData, function writeJSON(err) {
    if (err) return console.info(err);
    console.info(JSON.stringify(pjson));
    console.info('writing to ' + packageFileName);
  });
}

function updateVersionJson(jsonData) {
  fs.writeFile(versionFileName, jsonData, 'utf8', function (err) {
    if (err) {
      console.error(
        'An error occured while writing JSON Object to version.json',
      );
      return console.error(err);
    }

    console.info('version.json file has been saved with latest version number');
  });
}

function prettyPrintJsonFilees() {
  exec(
    `npx prettier --write ${packageFileName} ${versionFileName}`,
    (error, stdout, stderr) => {
      if (error) {
        console.error(`error: ${error.message}`);
        return;
      }
      if (stderr) {
        console.error(`stderr: ${stderr}`);
        return;
      }
      console.info(`stdout: ${stdout}`);
    },
  );
}

function updateReleaseInEnv(envFileName) {
  function setEnvValue(key, value) {
    // read file from hdd & split if from a linebreak to a array
    const ENV_VARS = fs.readFileSync(envFileName, 'utf8').split(os.EOL);

    // find the env we want based on the key
    const target = ENV_VARS.indexOf(
      ENV_VARS.find((line) => {
        return line.match(new RegExp(key));
      }),
    );

    // replace the key/value with the new value
    ENV_VARS.splice(target, 1, `${key}=${value}`);

    // write everything back to the file system
    fs.writeFileSync(envFileName, ENV_VARS.join(os.EOL));
  }

  setEnvValue('SENTRY_RELEASE', JSON.stringify(pjson.version));
}
