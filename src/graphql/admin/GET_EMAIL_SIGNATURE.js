import {gql} from '@apollo/client';

export default gql`
  query getEmailSignature(
    $appName: String!
    $apiKey: String!
    $userId: String!
  ) {
    getEmailSignature(appName: $appName, apiKey: $apiKey, userId: $userId) {
      html
    }
  }
`;
