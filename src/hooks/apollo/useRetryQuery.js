import {useEffect, useRef} from 'react';
import useQuery from './useQuery';
import {MAX_RETRIES} from '@macanta/config/apolloConfig';

const useRetryQuery = (graphqlTag, options, retryOptions) => {
  const maxRetries = retryOptions?.maxRetries || MAX_RETRIES;

  const retryCountRef = useRef(0);

  const enableRetry = retryCountRef.current < maxRetries;

  const retryQuery = useQuery(graphqlTag, options);

  const restart = () => {
    retryCountRef.current = 0;

    retryQuery.refetch();
  };

  useEffect(() => {
    if (
      retryQuery.error?.graphQLErrors?.length &&
      retryQuery.error.graphQLErrors[0].errorType === 'ExecutionTimeout' &&
      enableRetry
    ) {
      retryCountRef.current++;

      retryQuery.refetch();
    }
  }, [retryQuery.error]);

  return {...retryQuery, restart};
};

export default useRetryQuery;
