import {experimentalStyled as styled} from '@mui/material/styles';

import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import {IconButton} from '@macanta/components/Button';
import Card from '@macanta/components/Card';
import CardActionAreaComp from '@mui/material/CardActionArea';
import TextField from '@macanta/components/Forms';

export const NotificationItemContainer = styled(Box)`
  padding: ${({theme}) => theme.spacing(1)} ${({theme}) => theme.spacing(2)} 0;
`;

export const NotificationItemCard = styled(Card)`
  height: 100%;
  position: relative;
`;

export const CardActionArea = styled(CardActionAreaComp)`
  height: 100%;
  padding: 12px ${({theme}) => theme.spacing(2)}
    ${({theme}) => theme.spacing(2)};
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;

  ${({isNew}) =>
    isNew &&
    `
    background-color: #4caf50;

    * {
      color: white !important;
    }
  `}
`;

export const Header = styled(Box)`
  margin-bottom: ${({theme}) => theme.spacing(1)};
  display: flex;
  justify-content: space-between;
`;

export const HeaderText = styled(Typography)`
  font-size: 0.8125rem;
  color: #aaa;
`;

export const DismissButton = styled(IconButton)`
  position: absolute;
  top: 0;
  right: 0;
`;

export const DeepworkContainer = styled(Box)`
  display: flex;
  align-items: center;
`;

export const DeepworkLabel = styled(Typography)`
  font-size: 0.875rem;
  color: #888;
  margin-right: ${({theme}) => theme.spacing(1)};
`;

export const DeepworkSelect = styled(TextField)``;
