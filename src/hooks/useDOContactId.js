import {useContext} from 'react';
import DOContactIdContext from '@macanta/modules/DataObjectItems/DOContactIdContext';

const useDOContactId = () => {
  const {contactId} = useContext(DOContactIdContext);

  return contactId;
};

export default useDOContactId;
