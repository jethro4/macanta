import {experimentalStyled as styled} from '@mui/material/styles';

import Typography from '@mui/material/Typography';

export const Root = styled(Typography)`
  ${({variant}) => !variant && 'font-size: inherit;'}
`;
