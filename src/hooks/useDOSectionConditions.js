import {useState, useEffect, useMemo} from 'react';
import useIsAdmin from '@macanta/hooks/useIsAdmin';
import {LIST_DO_SECTION_CONDITIONS} from '@macanta/graphql/dataObjects';
import useSessionQuery from '@macanta/hooks/apollo/useSessionQuery';

const calculatePassedCondition = (condition, value) => {
  switch (condition.operator) {
    case 'is null': {
      return !value;
    }
    case 'not null': {
      return !!value;
    }
    case '~>~': {
      return value > condition.value;
    }
    case '~<~': {
      return value < condition.value;
    }
    case 'is not': {
      return condition.values?.length
        ? !condition.values?.includes(value)
        : condition.value !== value;
    }
    default: {
      return condition.values?.length
        ? condition.values?.includes(value)
        : condition.value === value;
    }
  }
};

const calculateSectionConditions = (calculatedConditions) => {
  const initialConditionHasPassed = !!calculatedConditions.find(
    (condition) => !condition.logic,
  )?.hasPassedCondition;
  const additionalConditions = calculatedConditions.filter(
    (condition) => !!condition.logic,
  );
  const triggerShowSection = additionalConditions.reduce((acc, condition) => {
    return condition.logic === 'and'
      ? condition.hasPassedCondition && acc
      : condition.hasPassedCondition || acc;
  }, initialConditionHasPassed);

  return triggerShowSection;
};

const useDOSectionConditions = ({groupId, details, options}) => {
  const isAdmin = useIsAdmin();

  const [sections, setSections] = useState({
    additionalSections: [],
    blacklistedSections: [],
  });

  const {data, loading} = useSessionQuery(LIST_DO_SECTION_CONDITIONS, {
    variables: {
      groupId,
    },
    onError() {},
    skip: !groupId,
    ...options,
  });

  const doSectionConditions = data?.listDOSectionConditions;

  const [calculatedSections, calculatedBlacklistedSections] = useMemo(() => {
    if (!isAdmin && doSectionConditions?.length && details) {
      const [
        calculatedSectionsArr,
        blacklistedSectionsArr,
      ] = doSectionConditions.reduce(
        (acc, section) => {
          const calculatedConditions = section.conditions.map((condition) => {
            const currentFieldValue = details[condition.name]?.value;

            return {
              fieldName: condition.name,
              hasPassedCondition: calculatePassedCondition(
                condition,
                currentFieldValue,
              ),
              logic: condition.logic,
            };
          });

          const triggerShowSection = calculateSectionConditions(
            calculatedConditions,
          );

          if (triggerShowSection) {
            return [acc[0].concat(section.sectionName), acc[1]];
          } else {
            return [acc[0], acc[1].concat(section.sectionName)];
          }
        },
        [[], []],
      );

      return [
        calculatedSectionsArr.join(','),
        blacklistedSectionsArr.join(','),
      ];
    }

    return ['', ''];
  }, [doSectionConditions, details]);

  useEffect(() => {
    if (
      calculatedSections !== sections.additionalSections.join(',') ||
      calculatedBlacklistedSections !== sections.blacklistedSections.join(',')
    ) {
      setSections({
        additionalSections: calculatedSections.split(','),
        blacklistedSections: calculatedBlacklistedSections.split(','),
      });
    }
  }, [calculatedSections, calculatedBlacklistedSections]);

  return {data: doSectionConditions, loading, ...sections};
};

export default useDOSectionConditions;
