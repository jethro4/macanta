import React, {useState} from 'react';
import Box from '@mui/material/Box';
import ButtonGroup from '@macanta/components/ButtonGroup';
import Button from '@macanta/components/Button';
import FormField from '@macanta/containers/FormField';
import CopyButton from './CopyButton';

const EMBED_TYPES = [
  {
    type: 'jsCode',
    label: 'JavaScript',
  },
  {
    type: 'iFrameCode',
    label: 'iFrame',
  },
  {
    type: 'htmlCode',
    label: 'HTML',
  },
];

const EmbedCode = ({jsCode, iFrameCode, htmlCode}) => {
  const [selectedCodeType, setSelectedCodeType] = useState(EMBED_TYPES[0].type);

  let text = '';

  switch (selectedCodeType) {
    case 'jsCode': {
      text = jsCode;
      break;
    }
    case 'iFrameCode': {
      text = iFrameCode;
      break;
    }
    case 'htmlCode': {
      text = htmlCode;
      break;
    }
  }

  text = text?.trim() || '';

  const handleSelectEmbedType = (type) => () => {
    const embed = EMBED_TYPES.find((h) => h.type === type);

    setSelectedCodeType(embed.type);
  };

  return (
    <Box>
      <ButtonGroup
        increaseBtnMinWidth
        style={{
          marginBottom: 16,
        }}
        size="small"
        variant="outlined"
        aria-label="primary button group">
        {EMBED_TYPES.map((history) => {
          return (
            <Button
              key={history.type}
              onClick={handleSelectEmbedType(history.type)}>
              {history.label}
            </Button>
          );
        })}
      </ButtonGroup>

      <Box
        style={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'flex-end',
        }}>
        <FormField
          style={{
            width: '100%',
            marginBottom: 12,
          }}
          disabled
          hideExpand
          type="TextArea"
          size="small"
          value={text}
          // onChange={handleChange('columns')}
          label="Copy this code to your website"
          variant="outlined"
        />

        <CopyButton text={text} />
      </Box>
    </Box>
  );
};

export default EmbedCode;
