import React, {useState} from 'react';
import Popover from '@macanta/components/Popover';
import ButtonIndex from './index';

const PopoverButton = ({
  PopoverProps,
  renderButton,
  renderContent,
  style,
  ...props
}) => {
  const {
    contentWidth,
    contentHeight,
    contentMinWidth = 180,
    contentMinHeight = 32,
    contentMaxWidth = '100%',
    contentMaxHeight,
    bodyStyle,
    anchorOrigin = {
      vertical: 'bottom',
      horizontal: 'right',
    },
    transformOrigin = {
      vertical: -8,
      horizontal: 'right',
    },
    onOpen,
    onClose,
    ...otherPopoverProps
  } = PopoverProps;

  const [anchorEl, setAnchorEl] = useState(null);

  const open = !!anchorEl;

  const handleOpen = (event, ...args) => {
    setAnchorEl(event.currentTarget);

    onOpen && onOpen(event, ...args);
  };

  const handleClose = (...args) => {
    setAnchorEl(null);

    onClose && onClose(...args);
  };

  return (
    <>
      {renderButton ? (
        renderButton(handleOpen)
      ) : (
        <ButtonIndex
          onClick={handleOpen}
          style={Object.assign(
            {},
            style,
            open && {
              opacity: 1,
            },
          )}
          {...props}
        />
      )}

      {!props.loading && (
        <Popover
          open={open}
          anchorEl={anchorEl}
          onClose={handleClose}
          anchorOrigin={anchorOrigin}
          transformOrigin={transformOrigin}
          {...otherPopoverProps}>
          <div
            style={{
              width: contentWidth,
              height: contentHeight,
              minWidth: contentMinWidth,
              minHeight: contentMinHeight,
              maxWidth: contentMaxWidth,
              maxHeight: contentMaxHeight,
              ...bodyStyle,
            }}>
            {open && renderContent && renderContent(handleClose)}
          </div>
        </Popover>
      )}
    </>
  );
};

export default PopoverButton;
