import React from 'react';
import * as Styled from './styles';

const Section = ({
  children,
  title,
  titleStyle,
  TitleRightComp,
  hideHeader,
  HeaderLeftComp,
  HeaderMidComp,
  HeaderRightComp,
  FooterComp,
  fullHeight,
  style,
  headerStyle,
  bodyContainerStyle,
  bodyStyle,
  footerStyle,
  loading,
  ...props
}) => {
  return (
    <Styled.Root
      style={{
        ...style,
        ...(fullHeight && {height: '100%'}),
      }}
      {...props}>
      {!hideHeader && (
        <Styled.SectionHeader style={headerStyle}>
          <Styled.TitleContainer>
            {title && (
              <>
                <Styled.Title style={titleStyle}>{title}</Styled.Title>
                {TitleRightComp}
              </>
            )}
            {HeaderLeftComp}
          </Styled.TitleContainer>
          {HeaderMidComp}
          {!!HeaderMidComp && !HeaderRightComp && <div />}
          {HeaderRightComp}
        </Styled.SectionHeader>
      )}
      <Styled.SectionBody style={bodyContainerStyle}>
        <Styled.BodyChildren
          fullHeight={fullHeight}
          style={{
            ...(FooterComp && {
              borderRadius: 0,
            }),
            ...bodyStyle,
          }}>
          {children}
        </Styled.BodyChildren>
        {loading && <Styled.LoadingIndicator fill align="top" />}
      </Styled.SectionBody>
      {FooterComp && (
        <Styled.SectionFooter style={footerStyle}>
          {FooterComp}
        </Styled.SectionFooter>
      )}
    </Styled.Root>
  );
};

export default Section;
