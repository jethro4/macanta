import React from 'react';
import Breadcrumbs from '@mui/material/Breadcrumbs';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import Typography from '@mui/material/Typography';
import Section from '@macanta/containers/Section';
import DataTable from '@macanta/containers/DataTable';
import {ROOT_PATH_ADMIN} from '@macanta/modules/AdminSettings/AdminSettings';
import useWorkflowBoards from '@macanta/hooks/admin/useWorkflowBoards';
import {capitalizeFirstLetter} from '@macanta/utils/string';
import AddOrEditWorkflowItemBtn from './AddOrEditWorkflowItemBtn';
import DeleteWorkflowItemBtn from './DeleteWorkflowItemBtn';
import * as AdminSettingsStyled from '@macanta/modules/AdminSettings/styles';
import * as DataObjectsSearchTableStyled from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/styles';
import {navigate} from 'gatsby';

const WORKFLOW_BOARDS_COLUMNS = [
  {
    id: 'title',
    label: 'Name',
  },
  {
    id: 'description',
    label: 'Description',
    minWidth: 170,
    maxWidth: 240,
  },
  {
    id: 'type',
    label: 'Type',
  },
  {
    id: 'keyField',
    label: 'Key Field',
    minWidth: 170,
    maxWidth: 240,
  },
  {
    id: 'relationship',
    label: 'Relationship',
    transform: ({value}) => {
      return value || 'Any';
    },
  },
  {
    id: 'displayFields',
    label: 'Display Fields',
    minWidth: 170,
    maxWidth: 240,
    transform: ({value: fieldNames}) => {
      return fieldNames.join(', ');
    },
  },
  {
    id: 'activeColumnsColor',
    label: 'Columns Color',
    transform: ({value: status}) => {
      return capitalizeFirstLetter(status);
    },
    renderBodyCellValue: ({value, origValue}) => {
      return (
        <Typography
          sx={{
            fontSize: 'inherit',
            color: origValue === 'active' ? 'success.main' : 'grayDarker.main',
          }}>
          {value}
        </Typography>
      );
    },
  },
];

const WorkflowBoards = () => {
  /* const navigate = useNavigate();*/

  const workflowBoardsQuery = useWorkflowBoards();

  const workflowBoards = workflowBoardsQuery.data;

  const handleBack = () => {
    navigate(ROOT_PATH_ADMIN);
  };

  return (
    <Section
      fullHeight
      bodyStyle={{
        backgroundColor: '#f5f6fa',
        padding: 0,
      }}
      HeaderLeftComp={
        <Breadcrumbs aria-label="breadcrumb">
          <AdminSettingsStyled.TitleCrumbBtn
            startIcon={
              <ArrowBackIcon
                style={{
                  color: '#333',
                }}
              />
            }
            onClick={handleBack}
            disabled={false}>
            <Typography color="textPrimary">Admin Settings</Typography>
          </AdminSettingsStyled.TitleCrumbBtn>
          <Typography>Workflow Boards</Typography>
        </Breadcrumbs>
      }
      HeaderRightComp={<AddOrEditWorkflowItemBtn />}>
      <DataTable
        loading={workflowBoardsQuery.loading}
        fullHeight
        columns={WORKFLOW_BOARDS_COLUMNS}
        data={workflowBoards}
        orderBy="title"
        rowsPerPage={25}
        rowsPerPageOptions={[25, 50, 100]}
        renderActionButtons={({item}) => (
          <DataObjectsSearchTableStyled.ActionButtonContainer>
            <AddOrEditWorkflowItemBtn item={item} />
            <DeleteWorkflowItemBtn id={item.id} name={item.title} />
          </DataObjectsSearchTableStyled.ActionButtonContainer>
        )}
        actionColumn={{
          show: true,
          style: {
            minWidth: 100,
          },
        }}
      />
    </Section>
  );
};

export default WorkflowBoards;
