import React, {useState} from 'react';
import {useFormikContext} from 'formik';
import SearchIcon from '@mui/icons-material/Search';
import Box from '@mui/material/Box';
import Button from '@macanta/components/Button';
import Modal from '@macanta/components/Modal';
import QueryBuilderForms from '@macanta/modules/AdminSettings/QueryBuilder/QueryBuilderForms';
import SingleQuerySearch from '@macanta/modules/AdminSettings/DataTools/SingleQuerySearch';
import RunQueryBtn from './RunQueryBtn';

const AdvancedSearchForms = (props) => {
  const renderBasicFields = () => {
    return <AdvancedSearchChooseQuery />;
  };

  const renderActionButtons = () => {
    return <RunQueryBtn />;
  };

  return (
    <QueryBuilderForms
      hideNameDescription
      hideContactNotifications
      hidePreview
      renderBasicFields={renderBasicFields}
      renderActionButtons={renderActionButtons}
      {...props}
    />
  );
};

const AdvancedSearchChooseQuery = () => {
  const {setValues} = useFormikContext();

  const [showModal, setShowModal] = useState(false);

  const handleShowModal = () => {
    setShowModal(true);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  const handleSelectData = (item) => {
    setValues((state) => ({
      ...state,
      doType: item.doType,
      chosenFields: item.chosenFields,
      contactConditions: item.contactConditions.map((condition, index) => {
        const id = Date.now() + index;

        const tempItem = {id, ...condition};
        delete tempItem['__typename'];

        return tempItem;
      }),
      doConditions: item.doConditions.map((condition, index) => {
        const id = Date.now() + index;

        const tempItem = {id, ...condition};
        delete tempItem['__typename'];

        return tempItem;
      }),
      userConditions: item.userConditions.map((condition, index) => {
        const id = Date.now() + index;

        const tempItem = {id, ...condition};
        delete tempItem['__typename'];

        return tempItem;
      }),
    }));

    handleCloseModal();
  };

  return (
    <>
      <Box
        sx={{
          display: 'flex',
          justifyContent: 'flex-end',
        }}
        style={{
          marginTop: 10,
        }}>
        <Button
          variant="text"
          color="info"
          startIcon={<SearchIcon />}
          onClick={handleShowModal}
          size="small">
          Choose Saved Query
        </Button>
      </Box>
      <Modal
        disableScroll
        headerTitle="Choose Saved Query"
        open={showModal}
        onClose={handleCloseModal}
        contentWidth={650}
        contentHeight={700}>
        <SingleQuerySearch onSuccess={handleSelectData} />
      </Modal>
    </>
  );
};

export default AdvancedSearchForms;
