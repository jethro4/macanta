import React from 'react';
import * as Styled from './styles';
import useItemsSlice from '@macanta/hooks/base/useItemsSlice';

const DORelationshipItemsData = ({data}) => {
  const {items, setRef} = useItemsSlice(data, 130);

  return (
    <Styled.ItemBodyDataContainer ref={setRef}>
      {items.map((item) => {
        return (
          <Styled.FieldContainer
            key={item.fieldName}
            className="item-data-field">
            <Styled.FieldName>{item.fieldName}</Styled.FieldName>
            <Styled.FieldValue
              {...(!item.value && {
                style: {
                  color: '#aaa',
                },
              })}>
              {item.value || 'N/A'}
            </Styled.FieldValue>
          </Styled.FieldContainer>
        );
      })}
    </Styled.ItemBodyDataContainer>
  );
};

export default DORelationshipItemsData;
