import {useEffect, useRef} from 'react';
import {useLocation} from '@reach/router';

const useLocationChange = (onChange) => {
  const hasInitializedRef = useRef(false);
  const location = useLocation();

  useEffect(() => {
    if (hasInitializedRef.current) {
      onChange();
    } else {
      hasInitializedRef.current = true;
    }
  }, [location]);
};

export default useLocationChange;
