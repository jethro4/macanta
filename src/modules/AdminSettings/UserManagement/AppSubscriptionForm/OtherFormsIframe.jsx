import React from 'react';
import Iframe from '@macanta/components/Base/Iframe';

const OtherFormsIframe = ({option}) => {
  return <Iframe src={option.subscriptionUrl} />;
};

export default OtherFormsIframe;
