import useValue from '@macanta/hooks/base/useValue';
import {ACCESS_LEVELS} from '@macanta/constants/accessLevels';
import useQueries from './useQueries';

const useQueriesWithPermission = ({permission, ...args}, options) => {
  const queriesQuery = useQueries(args, options);

  const queries = queriesQuery.items;

  const [data] = useValue(() => {
    return queries?.map((query) => {
      const permissionItem = permission?.mqbs?.find(
        (p) => p.queryId === query.id,
      );
      const queryPermission =
        permissionItem?.permission || ACCESS_LEVELS.NO_ACCESS;

      return {
        ...query,
        ...permissionItem,
        queryId: query.id,
        hasAccess: [ACCESS_LEVELS.READ_ONLY, ACCESS_LEVELS.READ_WRITE].includes(
          queryPermission,
        ),
        permission: queryPermission,
        addToDashboard: permission?.mqbWidgetAccess?.find(
          (p) => p.queryId === query.id,
        )?.show,
        addToBatchEmail: permission?.mqbWidgetEmail?.find(
          (p) => p.queryId === query.id,
        )?.show,
      };
    });
  }, [queries, permission]);

  return {...queriesQuery, data};
};

export default useQueriesWithPermission;
