import {nextFactoryId} from './factory';

const typename = 'DataObject';

export default (overwrites) => {
  const {id, currentIndex} = nextFactoryId(typename);

  return {
    __typename: typename,
    id,
    title: `DO Type ${currentIndex}`,
    ...overwrites,
  };
};
