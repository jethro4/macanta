import React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import * as Styled from './styles';
import FlexGrid from '@macanta/components/Base/FlexGrid';
import TaskSwitch from '@macanta/modules/UserSchedules/TaskSwitch';
import useValue from '@macanta/hooks/base/useValue';
import {formatDate} from '@macanta/utils/time';
import AutoUpdate from '@macanta/components/Base/AutoUpdate';
import Tooltip from '@macanta/components/Tooltip';
import {SCHEDULE_STATUS_OPTIONS_MAP} from '@macanta/modules/UserSchedules/helpers';

export const ROW_HEIGHT = 50;
export const VERTICAL_MARGIN = 32;

const STATUS_DIMENSIONS = 10;
// const statusLineHeight = (ROW_HEIGHT + VERTICAL_MARGIN) / 2;

const FIELD_COLUMNS = [
  {
    label: 'Title',
    value: 'title',
  },
  {
    label: 'Description',
    value: 'description',
  },
];

const ScheduleItem = ({item, status, ...props}) => {
  const fieldsData = FIELD_COLUMNS.reduce((acc, col) => {
    const accArr = [...acc];

    accArr.push({
      label: col.label,
      value: item[col.value],
    });

    return accArr;
  }, []);

  const [formattedDate] = useValue(() => formatDate(item.date, 'D MMM'), [
    item.date,
  ]);

  let titleColor = 'text.blue';
  let valueColor = 'text.primary';

  if (status === 'upcoming') {
    titleColor = 'text.secondary';
    valueColor = 'text.grayLight';
  } else if (status === 'overdue') {
    titleColor = 'text.secondary';
    valueColor = 'text.secondary';
  }

  return (
    <FlexGrid
      className="item-data"
      cols={6}
      rowHeight={ROW_HEIGHT}
      // divider={{
      //   w: 5,
      //   h: 20,
      // }}
      margin={[0, '1rem']}
      sx={{
        alignItems: 'center',
      }}
      {...props}>
      <Box
        colWidth={0.1}
        sx={{
          alignSelf: 'flex-start',
        }}>
        <AutoUpdate
          renderComponent={() => {
            return (
              <Tooltip title={SCHEDULE_STATUS_OPTIONS_MAP[status].title}>
                {/* <CircleIcon
                  sx={{
                    color: SCHEDULE_STATUS_OPTIONS_MAP[status].color,
                    width: 12,
                    height: 12,
                  }}
                /> */}
                <Box
                  sx={{
                    // border: `3px solid ${SCHEDULE_STATUS_OPTIONS_MAP[status].color}`,
                    // backgroundColor: 'common.white',
                    backgroundColor: SCHEDULE_STATUS_OPTIONS_MAP[status].color,
                    marginTop: `${STATUS_DIMENSIONS * 0.6}px`,
                    width: `${STATUS_DIMENSIONS}px`,
                    height: `${STATUS_DIMENSIONS}px`,
                    borderRadius: `${STATUS_DIMENSIONS}px`,
                  }}
                />
              </Tooltip>
            );
          }}
        />

        {/* <StatusConnectorLine status={nextStatus && status} />
        <StatusConnectorLine status={nextStatus} second /> */}
      </Box>

      <Box colWidth={0.7} dividerRight={1}>
        <Typography
          sx={{
            color: ['upcoming', 'overdue'].includes(status)
              ? 'text.secondary'
              : 'text.primary',
            fontSize: '1rem',
            lineHeight: 1.6,
            fontWeight: 'bold',
          }}>
          {formattedDate}
        </Typography>

        <Typography
          sx={{
            color: '#aaa',
            fontSize: '0.6875em',
          }}>
          All Day
        </Typography>
      </Box>

      <Box colWidth={0.8}>
        <Box
          sx={
            {
              // maxWidth: 'fit-content',
              // margin: '0 auto',
              // textAlign: 'center',
            }
          }>
          <Styled.FieldName
            sx={{
              color: titleColor,
            }}>
            Type
          </Styled.FieldName>
          <Styled.FieldValue
            sx={{
              color: valueColor,
            }}>
            {item.type}
          </Styled.FieldValue>
          {/* <Chip
            sx={{
              fontSize: '0.8125em',
              height: '1.5em',
            }}
            label={item.type}
          /> */}
        </Box>
      </Box>

      {fieldsData.map((it) => {
        return (
          <Styled.FieldContainer key={it.label} className="item-data-field">
            <Styled.FieldName
              sx={{
                color: titleColor,
              }}>
              {it.label}
            </Styled.FieldName>
            <Styled.FieldValue
              sx={{
                color: !it.value ? 'text.secondary' : valueColor,
              }}>
              {it.value || 'N/A'}
            </Styled.FieldValue>
          </Styled.FieldContainer>
        );
      })}

      <Box colWidth={0.5}>
        <TaskSwitch id={item.id} />
      </Box>
    </FlexGrid>
  );
};

// const StatusConnectorLine = ({status, second}) => {
//   return !status ? null : (
//     <Box
//       sx={{
//         position: 'absolute',
//         top: 18 + (second ? statusLineHeight : 0),
//         left: 5,
//         width: '1px',
//         height: `${statusLineHeight}px`,
//         backgroundColor: SCHEDULE_STATUS_OPTIONS_MAP[status].color,
//       }}
//     />
//   );
// };

export default ScheduleItem;
