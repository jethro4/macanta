import {gql} from '@apollo/client';

export const SMS_ACTIONS_FRAGMENT = gql`
  fragment smsActionsFragment on SmsActionItems {
    queryId
    queryType
    queryDescription
    queryName
    queryPhoneField
    querySMSMessage
  }
`;

export default gql`
  query getSmsActions($id: ID) {
    getSmsActions(id: $id) {
      action
      status
      count
      items {
        ...smsActionsFragment
      }
    }
  }
  ${SMS_ACTIONS_FRAGMENT}
`;
