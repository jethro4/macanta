import React, {useState} from 'react';
import Modal from '@macanta/components/Modal';
import useLocationChange from '@macanta/hooks/base/useLocationChange';
import ExportSection from '@macanta/modules/AdminSettings/DataTools/ExportSection';

const ExportContactQueryOption = ({
  modal,
  showModal: showModalProp = false,
  onClose,
  id,
  name,
  email,
}) => {
  const [showModal, setShowModal] = useState(showModalProp);

  const handleClose = () => {
    setShowModal(false);

    onClose();
  };

  let exportContactForms = (
    <ExportSection
      style={{
        margin: 0,
      }}
      headerStyle={{
        display: 'none',
      }}
      initialValues={{
        option: 'SingleContact',
        selectedData: {
          id,
          name,
          email,
        },
      }}
      onSuccess={handleClose}
    />
  );

  useLocationChange(handleClose);

  if (modal) {
    exportContactForms = (
      <Modal
        headerTitle={`Export Contact (${email})`}
        open={showModal}
        onClose={handleClose}
        contentWidth={450}
        contentHeight={350}>
        {exportContactForms}
      </Modal>
    );
  }

  return exportContactForms;
};

export default ExportContactQueryOption;
