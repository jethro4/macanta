import React from 'react';
import {useFormikContext} from 'formik';
import TurnedInIcon from '@mui/icons-material/TurnedIn';
import LinkIcon from '@mui/icons-material/Link';
import CodeIcon from '@mui/icons-material/Code';
import RestartAltIcon from '@mui/icons-material/RestartAlt';
import Box from '@mui/material/Box';
import ButtonGroup from '@macanta/components/ButtonGroup';
import Button, {PopoverButton, DialogButton} from '@macanta/components/Button';
import Section from '@macanta/containers/Section';
import MacantaBuilderForm from './MacantaBuilderForm';
import EmbedCode from './EmbedCode';
import {BUILDER_MODES} from './BuilderRoot';
import CopyButton from './CopyButton';

const BuilderBody = ({doTypeTitle, contactGroups, doGroups, ...props}) => {
  const {
    initialValues,
    values,
    setValues,
    setFieldValue,
    setErrors,
    handleSubmit,
    dirty,
  } = useFormikContext();

  const handleSelectMode = (mode) => () => {
    setFieldValue('selectedMode', mode);
  };

  const handleResetAll = () => {
    setValues(initialValues);
    setErrors({});
  };

  return (
    <Section
      fullHeight
      style={{
        margin: 0,
      }}
      headerStyle={{
        // minHeight: '2.55rem',
        // boxShadow: 'none',
        borderBottom: '1px solid #eee',
      }}
      bodyStyle={{
        padding: '1.5rem 1rem',
      }}
      HeaderLeftComp={
        <ButtonGroup
          increaseBtnMinWidth
          size="small"
          variant="outlined"
          aria-label="primary button group">
          {BUILDER_MODES.map((mode, index) => {
            return (
              <Button
                key={index}
                startIcon={mode.iconComp}
                onClick={handleSelectMode(mode.label)}>
                {mode.label}
              </Button>
            );
          })}
        </ButtonGroup>
      }
      HeaderRightComp={
        <Box
          style={{
            display: 'flex',
            alignItems: 'center',
          }}>
          <DialogButton
            disabled={!dirty}
            style={{
              marginRight: '1rem',
            }}
            size="small"
            variant="outlined"
            startIcon={<RestartAltIcon />}
            DialogProps={{
              title: 'Are you sure?',
              description: [
                `This will reset ALL changes made.`,
                'Do you want to proceed?',
              ],
              onActions: (handleClose) => {
                return [
                  {
                    label: 'Reset All',
                    onClick: () => {
                      handleResetAll();
                      handleClose();
                    },
                  },
                ];
              },
            }}>
            Reset All
          </DialogButton>
          {values.type === '2-Way Synced' && !!values.link && (
            <CopyButton
              color="primary"
              style={{
                marginRight: '1rem',
              }}
              label="Copy Link"
              text={values.link}
              iconComp={<LinkIcon />}
              tooltipTitle="Copied link"
            />
          )}
          {values.type !== '2-Way Synced' && !!values.jsCode && (
            <PopoverButton
              style={{
                marginRight: '1rem',
              }}
              size="small"
              variant="contained"
              startIcon={<CodeIcon />}
              PopoverProps={{
                title: 'Embed Code',
                contentWidth: 500,
                bodyStyle: {
                  padding: '1rem',
                },
              }}
              renderContent={() => (
                <EmbedCode
                  jsCode={values.jsCode}
                  iFrameCode={values.iFrameCode}
                  htmlCode={values.htmlCode}
                />
              )}>
              Embed Code
            </PopoverButton>
          )}
          <Button
            disabled={!dirty}
            variant="contained"
            startIcon={<TurnedInIcon />}
            onClick={handleSubmit}
            size="small">
            Save Form
          </Button>
        </Box>
      }
      {...props}>
      <MacantaBuilderForm
        doTypeTitle={doTypeTitle}
        contactGroups={contactGroups}
        doGroups={doGroups}
      />
    </Section>
  );
};

export default BuilderBody;
