import * as yup from 'yup';

const addDirectRelationshipValidationSchema = yup.object().shape({
  contact: yup.object().required('Select a contact'),
  relationships: yup.array().required('Required').min(1, 'Required'),
  directRelationId: yup.string().required('Required'),
});

export default addDirectRelationshipValidationSchema;
