import {gql} from '@apollo/client';

export default gql`
  mutation importData($importDataInput: ImportDataInput!) {
    importData(input: $importDataInput) {
      success
    }
  }
`;
