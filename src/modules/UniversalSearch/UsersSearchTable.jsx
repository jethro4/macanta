import React, {useState, useLayoutEffect, useMemo} from 'react';
import DataTable from '@macanta/containers/DataTable';
import useUsers from '@macanta/hooks/admin/useUsers';

const UsersSearchTableContainer = ({searchQuery, ...props}) => {
  const [page, setPage] = useState(0);

  const usersQuery = useUsers();

  const contacts = useMemo(() => {
    if (searchQuery?.search && usersQuery.data) {
      const filteredItems = usersQuery.data.filter((d) =>
        `${d.email} ${d.firstName || ''} ${d.lastName || ''}`
          .trim()
          .includes(searchQuery.search),
      );

      return filteredItems;
    }

    return usersQuery.data;
  }, [searchQuery?.search, usersQuery.data]);

  useLayoutEffect(() => {
    setPage(0);
  }, [searchQuery]);

  return (
    <UsersSearchTable
      contacts={contacts}
      loading={usersQuery.loading}
      page={page}
      onTablePageChange={setPage}
      {...props}
    />
  );
};

const UsersSearchTable = ({
  onSelectItem,
  columns,
  contacts,
  loading,
  ...props
}) => {
  const [tableData, setTableData] = useState([]);

  useLayoutEffect(() => {
    if (columns.length && contacts) {
      const formattedContacts = contacts.map((item) => ({
        FullName: `${item.firstName || ''} ${item.lastName || ''}`.trim(),
        FirstName: item.firstName,
        LastName: item.lastName,
        Email: item.email,
        ...item,
      }));

      setTableData(formattedContacts);
    }
  }, [columns, contacts]);

  return (
    <>
      <DataTable
        fullHeight
        loading={loading}
        hideEmptyMessage={loading}
        columns={columns}
        selectable
        data={tableData}
        onSelectItem={onSelectItem}
        {...props}
        hidePagination={false}
      />
    </>
  );
};

export default UsersSearchTableContainer;
