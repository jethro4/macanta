import {useContext} from 'react';
import ProgressAlertContext from '@macanta/containers/Alert/ProgressAlertContext';

const useProgressAlert = () => {
  const {displayMessage} = useContext(ProgressAlertContext);

  return {displayMessage};
};

export default useProgressAlert;
