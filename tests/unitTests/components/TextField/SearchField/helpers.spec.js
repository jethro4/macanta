import {
  matchAndTransformStrings,
  matchAndTransformStringsWithMetadata,
  getStringsMetadataByMatch,
  transformStringMetadata,
  DEFAULT_REGEX,
} from '@macanta/components/Forms/InputFields/SearchField/helpers';

describe('SearchField Transformers', () => {
  describe('Strings', () => {
    let newVal = '';
    let oldVal = '';
    let match = null;

    beforeEach(() => {
      newVal = '~CD.App Name~ on ~CD.Status~ or ~CD.Test Upload - Uploaded';
      oldVal = '~CD.App Name~ on ~CD.Status~ or ~CD.Test Upload~ - Uploaded';
      match = DEFAULT_REGEX;
    });

    describe('matchAndTransformStrings', () => {
      it('happy - get transformed final value based on match', () => {
        const actualResult = matchAndTransformStrings({newVal, oldVal, match});
        const expectedResult = '~CD.App Name~ on ~CD.Status~ or  - Uploaded';

        expect(actualResult).toEqual(expectedResult);
      });

      it('happy - get transformed final value based on match 2', () => {
        newVal = 'CD.App Name~ on ~CD.Status~ or ~CD.Test Upload~ - Uploaded';

        const actualResult = matchAndTransformStrings({newVal, oldVal, match});
        const expectedResult = ' on ~CD.Status~ or ~CD.Test Upload~ - Uploaded';

        expect(actualResult).toEqual(expectedResult);
      });

      it('happy - get transformed final value based on match 3', () => {
        newVal = '~CD.App Name on ~CD.Status~ or ~CD.Test Upload - Uploaded';

        const actualResult = matchAndTransformStrings({
          newVal: newVal,
          oldVal,
          match,
        });
        const expectedResult = ' on ~CD.Status~ or  - Uploaded';

        expect(actualResult).toEqual(expectedResult);
      });

      it('happy - get transformed final value with current index', () => {
        newVal = '~CD.App Name on ~CD.Status~ or ~CD.Test Upload - Uploaded';

        const actualResult = matchAndTransformStringsWithMetadata({
          newVal: newVal,
          oldVal,
          match,
        });
        const expectedResult = {
          value: ' on ~CD.Status~ or  - Uploaded',
          currentIndex: 0,
        };

        expect(actualResult).toEqual(expectedResult);
      });

      it('happy - get transformed final value with current index 2', () => {
        newVal = '~CD.App Name~ on ~CD.Status or ~CD.Test Upload - Uploaded';

        const actualResult = matchAndTransformStringsWithMetadata({
          newVal: newVal,
          oldVal,
          match,
        });
        const expectedResult = {
          value: '~CD.App Name~ on  or  - Uploaded',
          currentIndex: 17,
        };

        expect(actualResult).toEqual(expectedResult);
      });

      it('happy - get transformed final value with current index 3', () => {
        newVal = '~CD.App Name~ on ~CD.Status~ or ~CD.Test Upload~ - Upload';

        const actualResult = matchAndTransformStringsWithMetadata({
          newVal: newVal,
          oldVal,
          match,
        });
        const expectedResult = {
          value: newVal,
        };

        expect(actualResult).toEqual(expectedResult);
      });

      it('happy - multiple matched strings', () => {
        newVal = '~CD.App Name~ on ~CD.Status or ~CD.Test Upload - Uploaded';

        const actualResult = matchAndTransformStrings({newVal, oldVal, match});
        const expectedResult = '~CD.App Name~ on  or  - Uploaded';

        expect(actualResult).toEqual(expectedResult);
      });

      it('happy - multiple matched strings 2', () => {
        newVal = '~CD.App Name on ~CD.Status~ or ~CD.Test Upload - Uploaded';

        const actualResult = matchAndTransformStrings({newVal, oldVal, match});
        const expectedResult = ' on ~CD.Status~ or  - Uploaded';

        expect(actualResult).toEqual(expectedResult);
      });

      it('sad - copy updated value but no strings were matched', () => {
        newVal = '~CD.App Name~ on ~CD.Status~ or ~CD.Test Upload~ - Upload';

        const actualResult = matchAndTransformStrings({newVal, oldVal, match});
        const expectedResult = newVal;

        expect(actualResult).toEqual(expectedResult);
      });
    });

    describe('getStringsMetadataByMatch', () => {
      it('happy - get strings metadata based on match', () => {
        const actualResult = getStringsMetadataByMatch({newVal, oldVal, match});
        const expectedResult = [
          {
            str: '~CD.Test Upload~',
            startIndex: 32,
            endIndex: 48,
          },
        ];

        expect(actualResult).toEqual(expectedResult);
      });

      it('happy - get strings metadata based on match 2', () => {
        newVal = '~CD.App Name on ~CD.Status~ or ~CD.Test Upload~ - Uploaded';

        const actualResult = getStringsMetadataByMatch({newVal, oldVal, match});
        const expectedResult = [
          {
            str: '~CD.App Name~',
            startIndex: 0,
            endIndex: 13,
          },
        ];

        expect(actualResult).toEqual(expectedResult);
      });
    });

    describe('transformStringMetadata', () => {
      it('happy - get transformed string based on metadata', () => {
        const actualResult = transformStringMetadata({
          input: oldVal,
          search: '~CD.Test Upload~',
          replace: '',
          start: 32,
          end: 48,
        });
        const expectedResult = '~CD.App Name~ on ~CD.Status~ or  - Uploaded';

        expect(actualResult).toEqual(expectedResult);
      });
    });
  });
});
