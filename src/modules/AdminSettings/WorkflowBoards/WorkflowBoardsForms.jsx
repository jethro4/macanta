import React, {useState, useEffect, useMemo} from 'react';
import {useFormikContext} from 'formik';
import TurnedInIcon from '@mui/icons-material/TurnedIn';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import Button from '@macanta/components/Button';
import {SubSection} from '@macanta/containers/Section';
import TransferList from '@macanta/containers/TransferList';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import useDOFields from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOFields';
import useDORelationships from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDORelationships';
import useContactFields from '@macanta/modules/ContactDetails/useContactFields';
import Form from '@macanta/components/Form';
import workflowBoardValidationSchema from '@macanta/validations/workflowBoard';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import usePrevious from '@macanta/hooks/usePrevious';
import {useMutation} from '@apollo/client';
import {
  CREATE_OR_UPDATE_WORKFLOW_BOARD,
  LIST_WORKFLOW_BOARDS,
} from '@macanta/graphql/admin';
import {sortArrayByObjectKey} from '@macanta/utils/array';
import {CONTACT_DEFAULT_FIELDS} from '@macanta/constants/contactFields';
import * as NoteTaskFormsStyled from '@macanta/modules/NoteTaskForms/styles';
import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import * as QueryBuilderStyled from '@macanta/modules/AdminSettings/QueryBuilder/styles';
import * as FormFieldStyled from '@macanta/containers/FormField/styles';
import * as applicationStyles from '@macanta/themes/applicationStyles';

const COLUMNS_COLOR_OPTIONS = [
  {
    label: 'Active',
    value: 'active',
  },
  {
    label: 'Inactive',
    value: 'inactive',
  },
];

const WorkflowBoardsFormsContainer = ({item, ...props}) => {
  const {displayMessage} = useProgressAlert();

  const [
    callCreateOrUpdateWorkflowBoard,
    {loading: savingWorkflowBoard},
  ] = useMutation(CREATE_OR_UPDATE_WORKFLOW_BOARD, {
    update(cache, {data: {createOrUpdateWorkflowBoard}}) {
      cache.modify({
        fields: {
          listWorkflowBoards(listWorkflowBoardsRef) {
            cache.modify({
              id: listWorkflowBoardsRef.__ref,
              fields: {
                items(existingItems = []) {
                  const newItemRef = cache.writeQuery({
                    data: createOrUpdateWorkflowBoard,
                    query: LIST_WORKFLOW_BOARDS,
                  });

                  return [...existingItems, newItemRef];
                },
              },
            });
          },
        },
      });
    },
    onError() {},
    onCompleted: (data) => {
      if (data?.createOrUpdateWorkflowBoard) {
        displayMessage('Saved successfully!');
      }
    },
  });

  const getInitValues = () => {
    return {
      id: item?.id,
      title: item?.title || '',
      description: item?.description || '',
      type: item?.type || '',
      displayFields: item?.displayFields || [],
      keyField: item?.keyField || '',
      relationship: item?.relationship || 'Any',
      activeColumnsColor: item?.activeColumnsColor || 'inactive',
    };
  };

  const [initValues, setInitValues] = useState(() => getInitValues());
  const prevInitValues = usePrevious(initValues);

  const handleFormSubmit = async (values) => {
    callCreateOrUpdateWorkflowBoard({
      variables: {
        createOrUpdateWorkflowBoardInput: {
          id: values.id,
          title: values.title,
          description: values.description,
          type: values.type,
          displayFields: values.displayFields,
          keyField: values.keyField,
          relationship:
            values.relationship === 'Any' ? '' : values.relationship,
          activeColumnsColor: values.activeColumnsColor,
        },
        __mutationkey: 'createOrUpdateWorkflowBoardInput',
        removeAppInfo: true,
      },
    });
  };

  useEffect(() => {
    if (item && prevInitValues === initValues) {
      setInitValues(getInitValues());
    }
  }, [item]);

  return (
    <>
      <Form
        initialValues={initValues}
        enableReinitialize
        validationSchema={workflowBoardValidationSchema}
        onSubmit={handleFormSubmit}>
        {({values}) => {
          return <WorkflowBoardsForms item={values} {...props} />;
        }}
      </Form>
      <LoadingIndicator modal loading={savingWorkflowBoard} />
    </>
  );
};

const WorkflowBoardsForms = ({item, ...props}) => {
  const {
    errors,
    handleChange,
    setFieldValue,
    setFieldError,
    handleSubmit,
    dirty,
  } = useFormikContext();

  const doTypesQuery = useDOTypes({
    removeEmail: true,
  });

  const doType = doTypesQuery.data?.listDataObjectTypes.find(
    (type) => type.title === item?.type,
  );
  const groupId = doType?.id;

  const doFieldsQuery = useDOFields(groupId, {
    removeEmail: true,
  });
  const relationshipsQuery = useDORelationships(groupId);
  const contactCustomFieldsQuery = useContactFields(
    null,
    'contactCustomFields',
    {
      skip: !item?.type,
      removeEmail: true,
    },
  );

  const doTypes = doTypesQuery.data?.listDataObjectTypes;
  const doFields = doFieldsQuery.data?.listDataObjectFields;
  const relationships = relationshipsQuery.data?.listRelationships;
  const contactCustomFields =
    contactCustomFieldsQuery.data?.listDataObjectFields;

  const sortedDOFields = useMemo(() => {
    if (doFields) {
      return [
        {
          isSubheader: true,
          name: `${item?.type} Fields`,
        },
      ].concat(sortArrayByObjectKey(doFields, 'name'));
    }

    return [];
  }, [doFields]);

  const allFields = useMemo(() => {
    if (doFields && contactCustomFields) {
      return [
        ...sortedDOFields,
        ...[
          {
            isSubheader: true,
            label: 'Contact Basic Fields',
          },
        ].concat(CONTACT_DEFAULT_FIELDS),
        ...[
          {
            isSubheader: true,
            name: 'Contact Custom Fields',
          },
        ].concat(sortArrayByObjectKey(contactCustomFields, 'name')),
      ];
    }

    return [];
  }, [sortedDOFields, contactCustomFields]);

  const allRelationships = useMemo(() => {
    let relationshipsArr = [{role: 'Any'}];

    if (relationships) {
      relationshipsArr = relationshipsArr.concat(relationships);
    }

    return relationshipsArr;
  }, [relationships]);

  const loading =
    (!groupId || !doFields || !relationships) &&
    Boolean(
      doTypesQuery.loading ||
        doFieldsQuery.loading ||
        relationshipsQuery.loading,
    );

  return (
    <QueryBuilderStyled.FormRoot {...props}>
      <QueryBuilderStyled.Body>
        <NoteTaskHistoryStyled.FullHeightGrid
          style={{
            flexDirection: 'row',
          }}
          sx={applicationStyles.fillScroll}
          container>
          <NoteTaskHistoryStyled.FullHeightFlexGridColumn item sm={12} md={5}>
            <QueryBuilderStyled.SubBody>
              <SubSection
                headerStyle={{
                  backgroundColor: 'white',
                  minHeight: '3rem',
                }}
                style={{
                  marginTop: 0,
                }}
                title="Basic Information">
                <QueryBuilderStyled.TextField
                  required
                  value={item?.title}
                  error={errors.title}
                  onChange={handleChange('title')}
                  label="Name"
                  fullWidth
                  size="small"
                  variant="outlined"
                />
                <QueryBuilderStyled.TextField
                  type="TextArea"
                  hideExpand
                  value={item?.description}
                  onChange={handleChange('description')}
                  label="Description"
                  fullWidth
                  size="small"
                  variant="outlined"
                />
                <QueryBuilderStyled.TextField
                  required
                  type="Select"
                  options={doTypes}
                  labelKey="title"
                  valueKey="title"
                  value={item?.type}
                  error={errors.type}
                  onChange={handleChange('type')}
                  label="Data Object Type"
                  fullWidth
                  size="small"
                  variant="outlined"
                />
                {!!item?.type && (
                  <>
                    <FormFieldStyled.Divider
                      style={{
                        margin: '1.5rem 0',
                      }}
                    />
                    <QueryBuilderStyled.TextField
                      required
                      type="Select"
                      options={sortedDOFields}
                      labelKey="name"
                      valueKey="name"
                      value={item?.keyField}
                      error={errors.keyField}
                      onChange={handleChange('keyField')}
                      label="Key Field"
                      fullWidth
                      size="small"
                      variant="outlined"
                    />

                    <QueryBuilderStyled.TextField
                      type="Select"
                      options={allRelationships}
                      labelKey="role"
                      valueKey="role"
                      value={item?.relationship}
                      error={errors.relationship}
                      onChange={handleChange('relationship')}
                      label="Relationship"
                      fullWidth
                      size="small"
                      variant="outlined"
                    />
                  </>
                )}
              </SubSection>
            </QueryBuilderStyled.SubBody>
          </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
          <NoteTaskHistoryStyled.FullHeightFlexGridColumn item xs={12} md={7}>
            <QueryBuilderStyled.SubBody>
              <QueryBuilderStyled.SubSection
                disabled={!item?.type}
                headerStyle={{
                  backgroundColor: 'white',
                  minHeight: '3rem',
                }}
                bodyStyle={{
                  padding: '0.5rem 1rem',
                }}
                style={{
                  marginTop: 0,
                }}
                title="Columns Colour">
                <QueryBuilderStyled.TextField
                  style={{
                    marginBottom: 0,
                  }}
                  required
                  type="Radio"
                  options={COLUMNS_COLOR_OPTIONS}
                  value={item?.activeColumnsColor}
                  error={errors.activeColumnsColor}
                  onChange={handleChange('activeColumnsColor')}
                  fullWidth
                  size="small"
                  variant="outlined"
                />
              </QueryBuilderStyled.SubSection>

              <QueryBuilderStyled.SubSection
                disabled={!item?.type}
                headerStyle={{
                  backgroundColor: 'white',
                  minHeight: '3rem',
                }}
                bodyStyle={{
                  height: 400,
                }}
                title="Display Fields">
                <TransferList
                  error={errors.displayFields}
                  loading={loading}
                  options={allFields}
                  labelKey="name"
                  valueKey="name"
                  selectedItems={item?.displayFields}
                  hideEmptyMessage={loading}
                  onChangeFields={(newFields) => {
                    if (newFields.length) {
                      setFieldError('displayFields', '');
                    }

                    setFieldValue('displayFields', newFields);
                  }}
                />
              </QueryBuilderStyled.SubSection>
            </QueryBuilderStyled.SubBody>
          </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
        </NoteTaskHistoryStyled.FullHeightGrid>
        <LoadingIndicator
          fill
          align="top"
          style={{
            top: '2.5rem',
          }}
          loading={loading}
        />
      </QueryBuilderStyled.Body>
      <NoteTaskFormsStyled.Footer
        sx={applicationStyles.footer}
        style={{
          padding: '1rem',
          margin: 0,
          borderTop: '1px solid #eee',
          width: '100%',
          backgroundColor: '#f5f6fa',
        }}>
        <Button
          disabled={!dirty}
          variant="contained"
          startIcon={<TurnedInIcon />}
          onClick={handleSubmit}
          size="medium">
          Save Query
        </Button>
      </NoteTaskFormsStyled.Footer>
    </QueryBuilderStyled.FormRoot>
  );
};

export default WorkflowBoardsFormsContainer;
