import * as yup from 'yup';

export const forgotPasswordValidationSchema = yup.object().shape({
  email: yup.string().required('Required').email('Enter a valid email'),
});

export const verificationCodeValidationSchema = yup.object().shape({
  verificationCode: yup.string().required('Required').min(6, 'Invalid code'),
});

export const resetPasswordValidationSchema = yup.object().shape({
  newPassword: yup
    .string()
    .required('Required')
    .min(8, 'Must be at least 8 characters'),
});
