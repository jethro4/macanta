import React from 'react';
import Checkbox from '@mui/material/Checkbox';
import Typography from '@mui/material/Typography';
import PersonAddIcon from '@mui/icons-material/PersonAdd';
import AddIcon from '@mui/icons-material/Add';
import {SubSection} from '@macanta/containers/Section';
import Form from '@macanta/components/Form';
import Button from '@macanta/components/Button';
import connectContactValidationSchema from '@macanta/validations/connectContact';
import UniversalSearch from '@macanta/modules/UniversalSearch';
import FormControlLabel from '@mui/material/FormControlLabel';
import ContactFormsIconBtn from '@macanta/modules/ContactForms/ContactFormsIconBtn';
import * as NoteTaskFormsStyled from '@macanta/modules/NoteTaskForms/styles';
import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import * as applicationStyles from '@macanta/themes/applicationStyles';
import * as Styled from './styles';

const DEFAULT_COLUMNS = [
  {id: 'FullName', label: 'Name', minWidth: 170},
  {id: 'Email', label: 'Email', minWidth: 170},
];

const ConnectAnotherContactForms = ({
  selectedContactIds,
  relationshipOptions,
  onSuccess,
  renderRelationshipForms,
  schema,
  hideRelationshipsError,
  submitBtnLabel,
  relationshipsLoading,
  RelationshipsHeaderRightComp,
  usersOnly,
  ...props
}) => {
  const initValues = {
    contact: undefined,
    relationships: [],
  };

  const handleFormSubmit = ({contact, relationships, ...customData}) => {
    onSuccess(contact, relationships, {...customData});
  };

  return (
    <Styled.Root {...props}>
      <Form
        initialValues={initValues}
        validationSchema={schema}
        onSubmit={handleFormSubmit}>
        {({values, errors, handleSubmit, setFieldValue}) => (
          <>
            <NoteTaskHistoryStyled.FullHeightGrid container>
              <NoteTaskHistoryStyled.FullHeightFlexGridColumn
                item
                xs={12}
                md={6}>
                <SubSection
                  fullHeight
                  headerStyle={{
                    backgroundColor: 'white',
                  }}
                  bodyStyle={{
                    overflowY: 'hidden',
                    padding: '1rem',
                  }}
                  style={{
                    marginTop: 0,
                  }}
                  title={`Select a ${!usersOnly ? 'Contact' : 'User'}`}
                  HeaderRightComp={
                    <ContactFormsIconBtn
                      onSaveContact={(contact) => {
                        setFieldValue('contact', contact);
                      }}
                      renderButton={(handleAddContact) => {
                        return (
                          <Button
                            onClick={handleAddContact}
                            size="small"
                            variant="contained"
                            startIcon={<AddIcon />}>
                            Create Contact
                          </Button>
                        );
                      }}
                    />
                  }>
                  {!!errors.contact && (
                    <Typography
                      color="error"
                      style={{
                        fontSize: '0.75rem',
                        textAlign: 'center',
                        marginBottom: '0.5rem',
                      }}>
                      {errors.contact}
                    </Typography>
                  )}
                  <UniversalSearch
                    style={{
                      marginBottom: '1rem',
                    }}
                    autoFocus
                    autoSearch
                    hideSearchAndAdd
                    defaultSearchQuery={{search: '', page: 0, limit: 10}}
                    onSelectItem={(contact) =>
                      setFieldValue('contact', contact)
                    }
                    disableCategories
                    disableSuggestions
                    columns={DEFAULT_COLUMNS}
                    TableProps={{
                      noOddRowStripes: true,
                      highlight: true,
                      hidePagination: true,
                      disabledIds: selectedContactIds,
                    }}
                    {...(usersOnly && {
                      defaultCategory: 'users',
                    })}
                  />
                </SubSection>
              </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
              <NoteTaskHistoryStyled.FullHeightFlexGridColumn
                item
                xs={12}
                md={6}>
                <SubSection
                  loading={relationshipsLoading}
                  headerStyle={{
                    backgroundColor: 'white',
                  }}
                  bodyContainerStyle={{
                    overflowY: 'auto',
                  }}
                  bodyStyle={{
                    padding: '1rem',
                    minHeight: '8rem',
                    display: 'flex',
                    flexDirection: 'column',
                  }}
                  style={{
                    marginTop: 0,
                    maxHeight: '100%',
                  }}
                  title="Select Relationships"
                  HeaderRightComp={RelationshipsHeaderRightComp}>
                  {!hideRelationshipsError && !!errors.relationships && (
                    <Typography
                      color="error"
                      style={{
                        fontSize: '0.75rem',
                        textAlign: 'center',
                      }}>
                      {errors.relationships}
                    </Typography>
                  )}
                  {renderRelationshipForms ? (
                    renderRelationshipForms({
                      values,
                      errors,
                      setRelationships: (relationships) =>
                        setFieldValue('relationships', relationships),
                      setCustomData: setFieldValue,
                    })
                  ) : (
                    <>
                      {relationshipOptions?.map((option) => {
                        const checked = values.relationships.includes(
                          option.value,
                        );

                        return (
                          <FormControlLabel
                            key={option.value}
                            label={option.label}
                            control={
                              <Checkbox
                                color="primary"
                                checked={checked}
                                onChange={() => {
                                  let checkedValues = values.relationships;

                                  if (!checked) {
                                    checkedValues = values.relationships.concat(
                                      option.value,
                                    );
                                  } else {
                                    checkedValues = values.relationships.filter(
                                      (v) => v !== option.value,
                                    );
                                  }

                                  setFieldValue('relationships', checkedValues);
                                }}
                              />
                            }
                          />
                        );
                      })}
                      {relationshipOptions?.length === 0 && (
                        <Styled.EmptyMessage align="center" color="#888">
                          Relationships limit reached
                        </Styled.EmptyMessage>
                      )}
                    </>
                  )}
                </SubSection>
              </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
            </NoteTaskHistoryStyled.FullHeightGrid>
            <NoteTaskFormsStyled.Footer
              sx={applicationStyles.footer}
              style={{
                padding: '1rem',
                margin: 0,
                borderTop: '1px solid #eee',
                width: '100%',
                backgroundColor: '#f5f6fa',
              }}>
              <Button
                variant="contained"
                startIcon={<PersonAddIcon />}
                onClick={handleSubmit}
                size="medium">
                {submitBtnLabel}
              </Button>
            </NoteTaskFormsStyled.Footer>
          </>
        )}
      </Form>
    </Styled.Root>
  );
};

ConnectAnotherContactForms.defaultProps = {
  schema: connectContactValidationSchema,
  submitBtnLabel: 'Connect Contact',
};

export default ConnectAnotherContactForms;
