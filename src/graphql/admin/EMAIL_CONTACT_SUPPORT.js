import {gql} from '@apollo/client';

export default gql`
  mutation emailContactSupport(
    $emailContactSupportInput: EmailContactSupportInput!
  ) {
    emailContactSupport(input: $emailContactSupportInput) {
      success
    }
  }
`;
