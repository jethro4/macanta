import React, {useState, useEffect, useMemo} from 'react';
import {useFormikContext} from 'formik';
import TurnedInIcon from '@mui/icons-material/TurnedIn';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import Button from '@macanta/components/Button';
import Form from '@macanta/components/Form';
import {SubSection} from '@macanta/containers/Section';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';
import useDOFields from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOFields';
import useDORelationships from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDORelationships';
import queryBuilderValidationSchema from '@macanta/validations/queryBuilder';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import usePrevious from '@macanta/hooks/usePrevious';
import useAllFields from '@macanta/hooks/dataObject/useAllFields';
import CriteriaForms from './CriteriaForms';
import {useMutation} from '@apollo/client';
import {
  CREATE_OR_UPDATE_TRIGGER_CONDITION,
  LIST_QUERY_BUILDERS,
} from '@macanta/graphql/admin';
import {
  DEFAULT_OPERATOR,
  DEFAULT_USER_OPERATOR,
} from '@macanta/constants/operators';
import * as NoteTaskFormsStyled from '@macanta/modules/NoteTaskForms/styles';
import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import * as applicationStyles from '@macanta/themes/applicationStyles';
import * as Styled from './styles';

export const getType = (type) => {
  if (['Date', 'DateTime', 'TextArea'].includes(type)) {
    return 'Text';
  }

  return type;
};

const getNewItem = (type, logic) => {
  switch (type) {
    case 'doConditions': {
      return {
        id: Date.now(),
        name: '',
        logic,
        operator: DEFAULT_OPERATOR,
        values: null,
        value: '',
      };
    }
    case 'contactConditions': {
      return {
        id: Date.now(),
        relationship: '',
        logic,
      };
    }
    case 'userConditions': {
      return {
        id: Date.now(),
        relationship: '',
        logic,
        operator: DEFAULT_USER_OPERATOR,
        userId: '',
      };
    }
  }
};

const TriggerConditionFormsContainer = ({item, ...props}) => {
  const {displayMessage} = useProgressAlert();

  const [
    callCreateOrUpdateQueryBuilder,
    {loading: savingQueryBuilder},
  ] = useMutation(CREATE_OR_UPDATE_TRIGGER_CONDITION, {
    update(cache, {data: {createOrUpdateQueryBuilder}}) {
      cache.modify({
        fields: {
          listQueryBuilders(listQueryBuildersRef) {
            cache.modify({
              id: listQueryBuildersRef.__ref,
              fields: {
                items(existingItems = []) {
                  const newItemRef = cache.writeQuery({
                    data: createOrUpdateQueryBuilder,
                    query: LIST_QUERY_BUILDERS,
                  });

                  return [...existingItems, newItemRef];
                },
              },
            });
          },
        },
      });
    },
    onError() {},
    onCompleted: (data) => {
      if (data?.createOrUpdateQueryBuilder) {
        displayMessage('Saved successfully!');
      }
    },
  });

  const getInitValues = () => {
    return {
      id: item?.id,
      doType: item?.doType || '',
      name: item?.name || '',
      description: item?.description || '',
      doConditions: item?.doConditions
        ? item.doConditions.map((condition, index) => {
            const id = Date.now() + index;

            return {
              id,
              ...condition,
            };
          })
        : [
            {
              id: Date.now(),
              name: '',
              logic: '',
              operator: DEFAULT_OPERATOR,
              values: null,
              value: '',
            },
          ],
      contactConditions: item?.contactConditions
        ? item.contactConditions.map((condition, index) => {
            const id = Date.now() + index;

            return {
              id,
              ...condition,
            };
          })
        : [],
      userConditions: item?.userConditions
        ? item.userConditions.map((condition, index) => {
            const id = Date.now() + index;

            return {
              id,
              ...condition,
            };
          })
        : [],
      chosenFields: item?.chosenFields || [],
      criteriaMessageA: item?.criteriaMessageA,
      criteriaMessageB: item?.criteriaMessageB,
    };
  };

  const [initValues, setInitValues] = useState(() => getInitValues());
  const prevInitValues = usePrevious(initValues);

  const removeConditionIdAndTypename = (condition) => {
    const tempCondition = {...condition};

    delete tempCondition.id;
    delete tempCondition.__typename;

    return tempCondition;
  };

  const handleFormSubmit = async (values) => {
    callCreateOrUpdateQueryBuilder({
      variables: {
        createOrUpdateQueryBuilderInput: {
          id: values.id,
          doType: values.doType,
          name: values.name,
          description: values.description,
          doConditions: values.doConditions.map(removeConditionIdAndTypename),
          restartAfter: values.restartAfter,
          contactConditions: values.contactConditions.map(
            removeConditionIdAndTypename,
          ),
          userConditions: values.userConditions.map(
            removeConditionIdAndTypename,
          ),
          chosenFields: values.chosenFields,
          criteriaMessageA: values.criteriaMessageA,
          criteriaMessageB: values.criteriaMessageB,
        },
        __mutationkey: 'createOrUpdateQueryBuilderInput',
      },
    });
  };

  useEffect(() => {
    if (item && prevInitValues === initValues) {
      setInitValues(getInitValues());
    }
  }, [item]);

  return (
    <>
      <Form
        initialValues={initValues}
        enableReinitialize
        validationSchema={queryBuilderValidationSchema}
        onSubmit={handleFormSubmit}>
        {({values}) => {
          return <TriggerConditionForms item={values} {...props} />;
        }}
      </Form>
      <LoadingIndicator modal loading={savingQueryBuilder} />
    </>
  );
};

const TriggerConditionForms = ({
  item,
  hideNameDescription,
  renderBasicFields,
  renderActionButtons,
  ...props
}) => {
  const {
    errors,
    handleChange,
    setValues,
    handleSubmit,
    dirty,
  } = useFormikContext();

  const doTypesQuery = useDOTypes({
    removeEmail: true,
    includeContactObject: true,
  });

  const doType = doTypesQuery.data?.listDataObjectTypes?.find(
    (type) => type.title === item?.doType,
  );
  const groupId = doType?.id;

  const doFieldsQuery = useDOFields(groupId, {
    removeEmail: true,
  });
  const relationshipsQuery = useDORelationships(groupId);

  const doTypes = doTypesQuery.data?.listDataObjectTypes;
  const doFields = doFieldsQuery.data?.listDataObjectFields;
  const relationships = relationshipsQuery.data?.listRelationships;
  const isContactObjectType = item?.doType === 'Contact Object';

  const allFieldsQuery = useAllFields({
    groupId,
    isContactObjectType,
  });

  const allFields = allFieldsQuery.data;

  const allRelationships = useMemo(() => {
    if (relationships) {
      return relationships.map((r) => ({
        label: r.role,
        value: r.role,
      }));
    }

    return [];
  }, [relationships]);

  const loading =
    (!groupId || !doFields || !relationships) &&
    Boolean(
      doTypesQuery.loading ||
        doFieldsQuery.loading ||
        relationshipsQuery.loading,
    );

  return (
    <Styled.FormRoot {...props}>
      <Styled.Body>
        <NoteTaskHistoryStyled.FullHeightGrid
          style={{
            flexDirection: 'row',
          }}
          sx={applicationStyles.fillScroll}
          container>
          <NoteTaskHistoryStyled.FullHeightFlexGridColumn item sm={12} md={5}>
            <Styled.SubBody>
              <SubSection
                headerStyle={{
                  backgroundColor: 'white',
                  minHeight: '3rem',
                }}
                style={{
                  marginTop: 0,
                }}
                title="Basic Information">
                {!hideNameDescription && (
                  <>
                    <Styled.TextField
                      required
                      value={item?.name}
                      error={errors.name}
                      onChange={handleChange('name')}
                      label="Name"
                      fullWidth
                      size="small"
                      variant="outlined"
                    />
                    <Styled.TextField
                      type="TextArea"
                      hideExpand
                      value={item?.description}
                      onChange={handleChange('description')}
                      label="Description"
                      fullWidth
                      size="small"
                      variant="outlined"
                    />
                  </>
                )}
                <Styled.TextField
                  required
                  style={{
                    width: '64%',
                    display: 'inline-flex',
                    flexFlow: 'wrap',
                    marginBottom: 0,
                  }}
                  type="Select"
                  options={doTypes}
                  labelKey="title"
                  valueKey="title"
                  value={item?.doType}
                  error={errors.doType}
                  onChange={(val) => {
                    setValues((values) => ({
                      ...values,
                      id: values.id,
                      name: values.name,
                      description: values.description,
                      restartAfter: values.restartAfter,
                      doType: val,
                      doConditions: [
                        {
                          id: Date.now(),
                          name: '',
                          logic: '',
                          operator: DEFAULT_OPERATOR,
                          values: null,
                          value: '',
                        },
                      ],
                      contactConditions: [],
                      userConditions: [],
                      chosenFields: [],
                      criteriaMessageA: '',
                      criteriaMessageB: '',
                    }));
                  }}
                  label="Data Object Type"
                  fullWidth
                  size="small"
                  variant="outlined"
                />
                <Styled.TextField
                  style={{
                    width: '30%',
                    display: 'inline-flex',
                    marginBottom: 0,
                    marginLeft: 20,
                  }}
                  onChange={handleChange('restartAfter')}
                  type="Text"
                  placeholder="e.g. 1 hour or leave blank"
                  title="e.g. 1 hour or leave blank"
                  labelKey="title"
                  valueKey="title"
                  label="Restart After"
                  fullWidth
                  size="small"
                  variant="outlined"
                />
                {renderBasicFields && renderBasicFields()}
              </SubSection>
            </Styled.SubBody>
          </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
          <NoteTaskHistoryStyled.FullHeightFlexGridColumn item xs={12} md={7}>
            <Styled.SubBody>
              <CriteriaForms
                loading={loading}
                disabled={!item?.doType}
                style={{
                  marginTop: 0,
                }}
                title={`${item?.doType || 'DO'} Criteria`}
                data={item?.doConditions}
                required
                type="doConditions"
                choices={allFields}
                error={errors.doConditions}
                getNewItem={getNewItem}
              />

              {!isContactObjectType && (
                <>
                  <CriteriaForms
                    loading={loading}
                    disabled={!item?.doType}
                    title="Contact Relationship and Criteria"
                    data={item?.contactConditions}
                    type="contactConditions"
                    choices={allRelationships}
                    error={errors.contactConditions}
                    getNewItem={getNewItem}
                  />
                </>
              )}
            </Styled.SubBody>
          </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
        </NoteTaskHistoryStyled.FullHeightGrid>
        <LoadingIndicator
          fill
          align="top"
          style={{
            top: '2.5rem',
          }}
          loading={loading}
        />
      </Styled.Body>
      <NoteTaskFormsStyled.Footer
        sx={applicationStyles.footer}
        style={{
          padding: '1rem',
          margin: 0,
          borderTop: '1px solid #eee',
          width: '100%',
          backgroundColor: '#f5f6fa',
        }}>
        {renderActionButtons ? (
          renderActionButtons()
        ) : (
          <Button
            disabled={!dirty}
            variant="contained"
            startIcon={<TurnedInIcon />}
            onClick={handleSubmit}
            size="medium">
            Save Trigger Condition
          </Button>
        )}
      </NoteTaskFormsStyled.Footer>
    </Styled.FormRoot>
  );
};

export default TriggerConditionFormsContainer;
