import {
  convertWithSections,
  transformSectionsData,
} from '@macanta/selectors/field.selector';
import * as yup from 'yup';

const checkDetailsAndRelationships = function () {
  const values = this.parent;
  const {details, fields} = this.parent.context;

  const flatDetails = Object.entries(details).reduce((acc, [fieldName, d]) => {
    acc[fieldName] = d.value;
    return acc;
  }, {});
  const mergedDetails = {
    ...flatDetails,
    ...values.details,
  };
  const requiredDetailFields = fields.filter((field) => field.required);
  const errorRequiredDetailFields = requiredDetailFields.filter(
    ({name}) => !mergedDetails[name],
  );

  const errorRequiredRelationships = values.relationships.filter((contact) => {
    if (!contact.deleted && !contact.relationships.length) {
      return true;
    }

    return false;
  });
  const errorHasRequiredRelationships = !!errorRequiredRelationships.length;

  if (errorRequiredDetailFields.length || errorHasRequiredRelationships) {
    let errorMessage = '';

    const errorsWithSections = convertWithSections(errorRequiredDetailFields);
    const transformedErrorSectionsData = transformSectionsData(
      errorsWithSections,
    );

    if (transformedErrorSectionsData.length) {
      errorMessage += `\nDetails (Required Fields)\n`;
    }

    transformedErrorSectionsData.forEach((section) => {
      errorMessage += `  * ${section.sectionName}\n`;

      section.subGroups.forEach((subGroup) => {
        errorMessage += `    • ${subGroup.subGroupName}\n`;

        subGroup.data.forEach((d) => {
          errorMessage += `      - ${d.name}\n`;
        });
      });
    });

    if (errorHasRequiredRelationships) {
      errorMessage += `\nRelationships (Cannot have empty relationship)\n`;
    }

    return this.createError({
      path: `requiredFieldsMap`,
      message: {
        text: errorMessage,
        details: errorRequiredDetailFields,
        relationships: errorRequiredRelationships,
      },
    });
  } else {
    this.parent.mergedDetails = mergedDetails;
  }

  return true;
};

const schema = yup.object().shape({
  details: yup.object().test('details', '', checkDetailsAndRelationships),
});

export default schema;
