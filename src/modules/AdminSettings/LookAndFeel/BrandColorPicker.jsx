import React, {useContext} from 'react';
import {useFormikContext} from 'formik';
import TurnedInIcon from '@mui/icons-material/TurnedIn';
import Button from '@macanta/components/Button';
import Form from '@macanta/components/Form';
import ColorPicker from '@macanta/components/ColorPicker';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import Section from '@macanta/containers/Section';
import ThemeColorContext from '@macanta/modules/BaseTheme/ThemeColorContext';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import useAppSettings from '@macanta/hooks/admin/useAppSettings';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {useMutation} from '@apollo/client';
import {GET_APP_SETTINGS, UPDATE_APP_SETTINGS} from '@macanta/graphql/app';
import * as FormStyled from '@macanta/modules/NoteTaskForms/styles';
import * as Styled from './styles';
import * as Storage from '@macanta/utils/storage';

const BrandColorPickerContainer = () => {
  const {displayMessage} = useProgressAlert();

  const appSettingsQuery = useAppSettings({
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const [callUpdateAppSettings, updateAppSettingsMutation] = useMutation(
    UPDATE_APP_SETTINGS,
    {
      onError() {},
      onCompleted: async (data) => {
        if (data?.updateAppSettings?.success) {
          displayMessage('Theme color updated!');
        }
      },
    },
  );

  const handleFormSubmit = async (values) => {
    const {data} = await callUpdateAppSettings({
      variables: {
        updateAppSettingsInput: {
          type: 'ui_colour',
          value: values.color,
        },
        __mutationkey: 'updateAppSettingsInput',
      },
    });

    if (data?.updateAppSettings?.success) {
      Storage.setItem('primaryColor', values.color);

      appSettingsQuery.client.writeQuery({
        query: GET_APP_SETTINGS,
        data: {
          getAppSettings: {
            __typename: 'AppSettings',
            ...appSettingsQuery.data,
            uiColour: values.color,
          },
        },
      });
    }
  };

  return (
    <>
      <Form initialValues={{}} onSubmit={handleFormSubmit}>
        <BrandColorPicker />
      </Form>
      <LoadingIndicator modal loading={updateAppSettingsMutation.loading} />
    </>
  );
};

const BrandColorPicker = () => {
  const cachedPrimaryColor = Storage.getItem('primaryColor');

  const {themeColor, setThemeColor} = useContext(ThemeColorContext);

  const {values, setFieldValue, handleSubmit} = useFormikContext();

  const handleReset = () => {
    setThemeColor(cachedPrimaryColor);
    setFieldValue('color', cachedPrimaryColor);
  };

  const disabledSave = !values.color || values.color === cachedPrimaryColor;

  return (
    <>
      <Section
        fullHeight
        headerStyle={{
          backgroundColor: 'white',
        }}
        bodyStyle={{
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'space-between',
        }}
        style={{
          marginTop: 0,
        }}
        title="Choose Your Colour">
        <Styled.ColorPickerContainer>
          <ColorPicker
            color={themeColor}
            style={{
              width: '100%',
              height: 380,
            }}
            onChange={(val) => {
              setFieldValue('color', val);
              setThemeColor(val);
            }}
          />
        </Styled.ColorPickerContainer>
        <FormStyled.Footer
          style={{
            padding: '1rem',
            margin: 0,
            borderTop: '1px solid #eee',
            boxShadow: '0px -4px 10px -2px #eee',
            width: '100%',
            justifyContent: 'space-between',
          }}>
          <Button
            style={{
              ...(!disabledSave && {
                color: values.color || themeColor,
                borderColor: values.color || themeColor,
              }),
            }}
            disabled={disabledSave}
            variant="outlined"
            onClick={handleReset}
            size="medium">
            Reset
          </Button>

          <Button
            style={{
              ...(!disabledSave && {
                backgroundColor: values.color,
              }),
            }}
            disabled={disabledSave}
            variant="contained"
            startIcon={<TurnedInIcon />}
            onClick={handleSubmit}
            size="medium">
            Save & Apply
          </Button>
        </FormStyled.Footer>
      </Section>
    </>
  );
};

export default BrandColorPickerContainer;
