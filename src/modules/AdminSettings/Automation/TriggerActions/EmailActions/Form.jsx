import React from 'react';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import FlexGrid from '@macanta/components/Base/FlexGrid';
import BasicInfoForm from '@macanta/modules/AdminSettings/Automation/TriggerActions/SubForms/BasicInfoForm';
import FormField from '@macanta/containers/FormField';
import withForm from '@macanta/components/Form/withForm';
import Divider from '@macanta/components/Base/Divider';
import BodySection from '@macanta/modules/AdminSettings/UserManagement/BodySection';
import {useEmailAction} from '@macanta/hooks/automation';
import withSelectorQuery from '@macanta/hoc/withSelectorQuery';
import EmailMarkdown from '@macanta/modules/AdminSettings/Automation/TriggerActions/EmailActions/EmailMarkdown';
import EditEmailButton from '@macanta/modules/AdminSettings/Automation/TriggerActions/EmailActions/EditEmailButton';
import ActionForm from '@macanta/modules/AdminSettings/Automation/TriggerActions/SubForms/ActionForm';

let Form = (props) => {
  const {values} = props;

  return (
    <ActionForm>
      <FlexGrid
        {...props}
        cols={2}
        margin={[0, '2rem']}
        // border={[0, 1]}
        sx={{
          padding: '1rem',
          ...props.sx,
        }}>
        <Box colWidth={0.7}>
          <BasicInfoForm
            labelPosition="normal"
            actionLabel="Email Action Name"
          />
        </Box>

        <BodySection
          title="Email Settings"
          bodyStyle={{
            padding: '1rem',
          }}>
          <FlexGrid cols={2} margin={['1rem', '1.5rem']}>
            <FormField
              name="fromAddress"
              labelPosition="normal"
              label="From"
              size="small"
              variant="outlined"
            />

            <FormField
              name="fromName"
              labelPosition="normal"
              label="From Name"
              size="small"
              variant="outlined"
            />

            <FormField
              name="subject"
              labelPosition="normal"
              label="Subject"
              size="small"
              variant="outlined"
            />

            <FormField
              name="signature"
              labelPosition="normal"
              label="Signature"
              size="small"
              variant="outlined"
            />

            <FormField
              name="previewText"
              labelPosition="normal"
              label="Preview Text"
              size="small"
              variant="outlined"
            />
          </FlexGrid>

          <Divider
            style={{
              margin: '1.5rem 0 1rem',
              borderBottom: '1px dashed #eee',
            }}
          />

          <Box
            sx={{
              display: 'flex',
              justifyContent: 'space-between',
              marginBottom: '8px',
            }}>
            <Typography
              sx={{
                color: 'text.secondary',
                fontWeight: 'bold',
              }}>
              Email Preview
            </Typography>

            <EditEmailButton />
          </Box>

          <Box
            sx={{
              flexBasis: '300px',
              flexGrow: 1,
              overflow: 'auto',
            }}>
            <EmailMarkdown
              values={{
                subject: values.subject,
                message: values.emailHtml,
                signature: values.signature,
              }}
            />
          </Box>
        </BodySection>
      </FlexGrid>
    </ActionForm>
  );
};

Form = withForm(Form, {
  mapPropsToValues: (props) => {
    return {
      ...props.initialValues,
      emailHtml: props.emailActionQuery.result?.emailTemplate?.emailHtml || '',
    };
  },
});

Form = withSelectorQuery(Form, useEmailAction, 'emailActionQuery');

export default Form;
