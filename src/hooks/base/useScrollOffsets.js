import {useCallback, useEffect, useState, useRef} from 'react';
import debounce from 'lodash/debounce';

const useScrollOffsets = (debounceDelay = 400) => {
  const [target, setTarget] = useState(null);
  const [offsets, setOffsets] = useState({
    x: 0,
    y: 0,
  });

  const highestYRef = useRef(0);

  const scrollTargetRef = useCallback((node) => {
    setTarget(node);
  }, []);

  const handleScroll = useCallback(
    debounce(
      (event) => {
        const updatedOffsets = {
          x: getScrollLeft(event.target),
          y: getScrollTop(event.target),
        };

        if (highestYRef.current < updatedOffsets.y) {
          highestYRef.current = updatedOffsets.y;
        }

        setOffsets(updatedOffsets);
      },
      debounceDelay,
      {leading: false, trailing: true},
    ),
    [],
  );

  useEffect(() => {
    if (target) {
      target.addEventListener('scroll', handleScroll);
    }

    return () => {
      if (target) {
        target.removeEventListener('scroll', handleScroll);
      }
    };
  }, [target]);

  return {
    x: offsets.x,
    y: offsets.y,
    highestY: highestYRef.current,
    scrollTargetRef,
  };
};

const getScrollLeft = (target) => {
  if (target) return target.scrollLeft;
  return (
    window.scrollX ||
    window.pageXOffset ||
    document.body.scrollLeft ||
    (document.documentElement && document.documentElement.scrollLeft) ||
    0
  );
};

const getScrollTop = (target) => {
  if (target) return target.scrollTop;
  return (
    window.scrollY ||
    window.pageYOffset ||
    document.body.scrollTop ||
    (document.documentElement && document.documentElement.scrollTop) ||
    0
  );
};

export default useScrollOffsets;
