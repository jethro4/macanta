import {gql} from '@apollo/client';
import {DO_SCHEDULE_FRAGMENT} from '@macanta/graphql/scheduler/fragments';

export default gql`
  mutation createOrUpdateDOSchedule($input: CreateOrUpdateDOScheduleInput!) {
    createOrUpdateDOSchedule(input: $input) {
      ...doScheduleFragment
    }
  }
  ${DO_SCHEDULE_FRAGMENT}
`;
