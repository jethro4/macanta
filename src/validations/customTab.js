import * as yup from 'yup';

const schema = yup.object().shape({
  title: yup.string().required('Required'),
});

export default schema;
