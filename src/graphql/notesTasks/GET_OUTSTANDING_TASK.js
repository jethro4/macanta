import {gql} from '@apollo/client';

export default gql`
  query getOutstandingTask($contactId: ID!) {
    getOutstandingTask(contactId: $contactId) {
      hasTask
    }
  }
`;
