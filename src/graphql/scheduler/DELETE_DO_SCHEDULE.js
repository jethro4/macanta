import {gql} from '@apollo/client';

export default gql`
  mutation deleteDOSchedule($input: DeleteDOScheduleInput!) {
    deleteDOSchedule(input: $input) {
      id
      success
    }
  }
`;
