import React from 'react';
import useSessionExpiry from '@macanta/hooks/admin/useSessionExpiry';
import Modal from '@macanta/components/Modal';
import LoginPage from '@macanta/modules/pages/LoginPage';
import {sessionExpiredVar} from '@macanta/graphql/cache/vars';
import {useApolloClient, useReactiveVar} from '@apollo/client';

const CheckSessionExpiry = () => {
  const client = useApolloClient();
  const sessionExpired = useReactiveVar(sessionExpiredVar);

  useSessionExpiry();

  const handleLogin = () => {
    sessionExpiredVar(false);

    client.resetStore();
  };

  return (
    <Modal
      style={{
        padding: 0,
      }}
      hideHeader
      disableBackdropClose
      open={sessionExpired}
      contentWidth={'100%'}
      contentHeight={'100%'}>
      <LoginPage onLogin={handleLogin} />
    </Modal>
  );
};

export default CheckSessionExpiry;
