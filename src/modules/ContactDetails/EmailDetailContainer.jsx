import React from 'react';
import EmailContactBtn from './EmailContactBtn';
import DetailWithActionArea from './DetailWithActionArea';

const EmailDetailContainer = ({
  label,
  email,
  contactId,
  noMargin,
  disableGrid,
}) => {
  return (
    <DetailWithActionArea
      disableGrid={disableGrid}
      label={label}
      value={email}
      noMargin={noMargin}
      ButtonComp={
        !!email && <EmailContactBtn to={email} contactId={contactId} />
      }
      leftItemCol={7.5}
      rightItemCol={4.5}
    />
  );
};

export default EmailDetailContainer;
