import React from 'react';
import Section from '@macanta/containers/Section';
import Grid from '@mui/material/Grid';
import useContactMetadata from '@macanta/hooks/metadata/useContactMetadata';
import ContactWindowGuideBtn from './ContactWindowGuideBtn';
import ActionButtons from './ActionButtons';
import InterfaceAlerts from './InterfaceAlerts';
import DetailInfo from './DetailInfo';
import EmailDetailContainer from './EmailDetailContainer';
import PhoneDetailContainer from './PhoneDetailContainer';
import useContactFields from './useContactFields';
import * as Styled from './styles';

const ContactDetails = ({contact, ...props}) => {
  useContactMetadata({email: contact.email});

  const contactFieldsQuery = useContactFields(
    contact.email,
    'contactCustomFields',
  );

  return (
    <Section
      title={`${contact.firstName || ''} ${contact.lastName || ''}`.trim()}
      titleStyle={{
        fontWeight: 'bold',
        fontSize: '1.25rem',
        letterSpacing: 0.3,
      }}
      TitleRightComp={<ContactWindowGuideBtn />}
      HeaderMidComp={<InterfaceAlerts contactId={contact.id} />}
      HeaderRightComp={<ActionButtons contact={contact} />}
      {...props}
      loading={props.loading || contactFieldsQuery.loading}>
      <Styled.DetailsBody>
        <Styled.DetailsBodyHeader>
          <Styled.BackgroundBox>
            <Grid container>
              <Grid item xs={12} sm={6} md={2.5}>
                <DetailInfo noMargin label="ID" value={contact.id} />
              </Grid>
              <Grid item xs={12} sm={6} md={4}>
                <EmailDetailContainer
                  disableGrid
                  noMargin
                  label="Email"
                  email={contact.email}
                  contactId={contact.id}
                />
              </Grid>
              <Grid item xs={12} sm={6} md={3}>
                <PhoneDetailContainer
                  disableGrid
                  noMargin
                  label="Phone"
                  defaultPhoneNumber={
                    contact.phoneNumbers && contact.phoneNumbers[0]
                  }
                  phoneNumbers={contact.phoneNumbers}
                  contactId={contact.id}
                />
              </Grid>
              <Grid item xs={12} sm={6} md={2.5}>
                <DetailInfo
                  noMargin
                  label="Date Created"
                  value={contact.createdDate}
                />
              </Grid>
            </Grid>
          </Styled.BackgroundBox>
        </Styled.DetailsBodyHeader>
      </Styled.DetailsBody>
    </Section>
  );
};

export default ContactDetails;
