import React from 'react';
import clsx from 'clsx';
import Divider from '@mui/material/Divider';
import DrawerComp from '@mui/material/Drawer';
import HomeIcon from '@mui/icons-material/Home';
import SettingsAccessibilityIcon from '@mui/icons-material/SettingsAccessibility';
import SettingsIcon from '@mui/icons-material/Settings';
import DeveloperBoardIcon from '@mui/icons-material/DeveloperBoard';
import DateRangeIcon from '@mui/icons-material/DateRange';
import List from '@mui/material/List';
import {makeStyles} from '@mui/material/styles';
import CompanyLogo from '@macanta/modules/CompanyLogo';
import useIsAdmin from '@macanta/hooks/useIsAdmin';
import HelpCentreItem from './HelpCentreItem';
import LogoutItem from './LogoutItem';
import LoggedInUserItem from './LoggedInUserItem';
import ExpandItem from './ExpandItem';
import DrawerListItem from './DrawerListItem';
import DevResetCacheItem from './DevResetCacheItem';
import * as Styled from './styles';
import {getAppInfo} from '@macanta/utils/app';
var pjson = require('../../../package.json');

export const drawerWidth = 240;

export const getDrawerClosedWidth = (theme) =>
  `calc(${theme.spacing(7)} + 1px)`;

const [appName] = getAppInfo();

const DEFAULT_GENERAL_TABS = [
  {id: 'dashboard', label: 'Dashboard', IconComp: HomeIcon},
  {
    id: 'workflow-boards',
    label: 'Workflow Boards',
    IconComp: DeveloperBoardIcon,
  },
  {
    id: 'schedules',
    label: 'Schedules',
    IconComp: DateRangeIcon,
    hidden: appName !== 'staging',
  },
  {
    id: 'admin-settings',
    label: 'Admin Settings',
    IconComp: SettingsIcon,
    permission: 'admin',
  },
  // {id: 'widgets', label: 'Widgets', IconComp: WidgetsIcon},
  // {id: 'admin', label: 'Admin', IconComp: SupervisorAccountIcon},
];

// const DEFAULT_SUPPORT_TABS = [
//   {id: 'knowledgeBase', label: 'Knowledge Base', IconComp: LibraryBooksIcon},
// ];

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  appBar: {
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
    },
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  drawerPaper: {
    backgroundColor: 'inherit !important',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
    boxSizing: 'border-box',
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: getDrawerClosedWidth(theme),
    // [theme.breakpoints.up('sm')]: {
    //   width: `calc(${theme.spacing(9)} + 1px)`,
    // },
  },
  sidebarLogo: {
    width: 80,

    '& img': {
      margin: 'unset',
    },

    justifyContent: 'flex-start',
  },
  sidebarLogoSm: {
    padding: '0 5px',
  },
}));

const Drawer = ({drawerOpen, onDrawerToggle, ...props}) => {
  const classes = useStyles();
  const isAdmin = useIsAdmin();

  const drawerContent = (
    <Styled.DrawerContent {...props}>
      <Styled.ContentToolbarSpacer expanded={drawerOpen}>
        <Styled.CompanyLogoLink to="/app/dashboard">
          <CompanyLogo
            className={clsx({
              [classes.sidebarLogo]: drawerOpen,
              [classes.sidebarLogoSm]: !drawerOpen,
            })}
            imageStyle={{
              maxHeight: drawerOpen ? '70%' : '90%',
            }}
          />
          {drawerOpen && (
            <Styled.CompanyNameContainer>
              <Styled.CompanyName>{`v${pjson.version}`}</Styled.CompanyName>
            </Styled.CompanyNameContainer>
          )}
        </Styled.CompanyLogoLink>
      </Styled.ContentToolbarSpacer>
      <Divider />
      {/* <Styled.ContentToolbarSpacer /> */}
      <List>
        {DEFAULT_GENERAL_TABS.filter(
          (item) =>
            !item.hidden &&
            (!item.permission || (item.permission === 'admin' && isAdmin)),
        ).map((item) => {
          return (
            <DrawerListItem
              key={item.id}
              id={`${item.id}-button`}
              to={`/app/${item.id}`}
              label={item.label}
              IconComp={item.IconComp}
            />
          );
        })}
      </List>
      <Divider />
      {/* <List>
        {DEFAULT_SUPPORT_TABS.map((item) => {
          return (
            <DrawerListItem
              key={item.id}
              to={`/app/${item.id}`}
              label={item.label}
              IconComp={item.IconComp}
            />
          );
        })}
      </List> */}
      <Divider />
      <List>
        <LoggedInUserItem expanded={drawerOpen} onClick={onDrawerToggle} />
      </List>
      <Styled.BottomList>
        <DrawerListItem
          id={`user-settings-button`}
          to={`/app/user-settings`}
          label="User Settings"
          IconComp={SettingsAccessibilityIcon}
        />
        <HelpCentreItem />
        <LogoutItem />
        <DevResetCacheItem />
        <Divider
          style={{
            marginTop: 8,
          }}
        />
        <ExpandItem expanded={drawerOpen} onClick={onDrawerToggle} />
      </Styled.BottomList>
    </Styled.DrawerContent>
  );

  return (
    <DrawerComp
      variant="permanent"
      onClose={onDrawerToggle}
      className={clsx(classes.drawer, {
        [classes.drawerOpen]: drawerOpen,
        [classes.drawerClose]: !drawerOpen,
      })}
      classes={{
        paper: clsx({
          [classes.drawerPaper]: true,
          [classes.drawerOpen]: drawerOpen,
          [classes.drawerClose]: !drawerOpen,
        }),
      }}>
      {drawerContent}
    </DrawerComp>
  );
};

export default Drawer;
