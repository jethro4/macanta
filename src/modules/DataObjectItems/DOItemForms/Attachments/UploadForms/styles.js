import {experimentalStyled as styled} from '@mui/material/styles';

import Box from '@mui/material/Box';
import FileUploadComp from '@macanta/components/FileUpload';

export const Root = styled(Box)`
  flex: 1;
  display: flex;
  flex-direction: column;
  padding: ${({theme}) => theme.spacing(2)};
`;

export const FileUpload = styled(FileUploadComp)`
  flex: 1;
  display: flex;
  flex-direction: column;

  .MuiDropzoneArea-root {
    flex: 1;
  }
`;
