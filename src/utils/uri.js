export const getSubdomain = (hostname) => {
  var subdomain = hostname.split('.')[0];

  return subdomain;
};

export const getEmailDomain = (email) => email?.split('@').pop() || '';

const applyHash = (hashMap) => {
  const hashArr = Object.entries(hashMap).reduce(
    (acc, [hashKey, hashValue]) => {
      const hashPair = `${hashKey}=${hashValue}`;

      return acc.concat(hashPair);
    },
    [],
  );

  window.location.hash = hashArr.join('&');
};

export const getHash = (key) => {
  const hash = window.location.hash.substr(1);

  const result = hash.split('&').reduce(function (res, item) {
    const [hashKey, hashValue] = item.split('=');

    if (hashKey) {
      res[hashKey] = hashValue;
    }

    return res;
  }, {});

  return !key ? result : result[key];
};

export const setHash = (key, value) => {
  if (key) {
    const currentHashMap = getHash();

    currentHashMap[key] = value;

    applyHash(currentHashMap);
  }
};

export const removeHash = (key) => {
  if (key) {
    const currentHashMap = getHash();

    delete currentHashMap[key];

    applyHash(currentHashMap);
  } else {
    applyHash({});
  }
};
