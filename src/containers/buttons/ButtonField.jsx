import React from 'react';
import Box from '@mui/material/Box';
import FormField from '@macanta/containers/FormField';

const ButtonField = ({PopoverProps, value, children, ...props}) => {
  const {
    bodyStyle,
    contentWidth,
    contentHeight,
    contentMinWidth = 180,
    contentMinHeight = 32,
    contentMaxWidth = '100%',
    contentMaxHeight,
  } = PopoverProps;

  const handleRenderValue = () => {
    return value;
  };

  return (
    <FormField
      type="Select"
      showDoneBtn
      labelPosition="normal"
      size="small"
      variant="outlined"
      {...props}
      value={value}
      renderValue={handleRenderValue}
      style={{
        marginBottom: 0,
        ...props.style,
      }}>
      <Box
        style={{
          width: contentWidth,
          height: contentHeight,
          minWidth: contentMinWidth,
          minHeight: contentMinHeight,
          maxWidth: contentMaxWidth,
          maxHeight: contentMaxHeight,
          ...bodyStyle,
        }}>
        {children}
      </Box>
    </FormField>
  );
};

export default ButtonField;
