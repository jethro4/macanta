import React from 'react';

const RawSelect = ({options, ...props}) => {
  return !options ? null : (
    <select className="form-select" {...props}>
      <option value>Choose...</option>
      {options.map((o) => (
        <option key={o} value={o}>
          {o}
        </option>
      ))}
    </select>
  );
};

export default RawSelect;
