import isNil from 'lodash/isNil';
import isUndefined from 'lodash/isUndefined';
import isArray from 'lodash/isArray';
import isFunction from 'lodash/isFunction';
import isEqual from 'lodash/isEqual';
import {isEmpty, isPureObject} from '@macanta/utils/value';

export const parseObj = (obj) => {
  return {
    ...obj,
    hasProperty: (prop) => obj.hasOwnProperty(prop),
    hasPropertyTruthy: (prop) => obj.hasOwnProperty(prop) && obj[prop],
  };
};

export const traverseProperties = ({
  value: objArg,
  conditionCallback,
  valueCallback,
  keyMapping,
}) => {
  const shouldRemove = !valueCallback;
  const valueIsArray = isArray(objArg);

  let obj = Object.assign({}, objArg);

  if (!valueIsArray && keyMapping) {
    obj = Object.keys(obj).reduce((acc, key) => {
      const accObj = {...acc};

      const mappedKey = keyMapping[key] || key;
      const value = obj[key];

      accObj[mappedKey] = value;

      return accObj;
    }, {});
  }

  let entries = Object.entries(obj);

  let traversedData = entries.reduce(
    (acc, [key, value]) => {
      const accObj = {...acc};

      if (!isPureObject(value)) {
        const conditionPassed =
          !conditionCallback ||
          conditionCallback(value, {key, reference: objArg});

        if (conditionPassed) {
          if (shouldRemove) {
            accObj[key] = value;
          } else {
            const derivedValue = valueCallback?.(value, {
              key,
              reference: objArg,
            });

            accObj[key] = !isUndefined(derivedValue) ? derivedValue : value;
          }
        }
      } else {
        accObj[key] = traverseProperties({
          value,
          conditionCallback,
          valueCallback,
          keyMapping,
        });
      }

      return accObj;
    },
    shouldRemove ? {} : obj,
  );

  if (valueIsArray) {
    traversedData = Object.values(traversedData);
  }

  return traversedData;
};

export const readProperties = (value, conditionalValueOrFunc, keyToCompare) => {
  let propertiesArr = [];

  const valueCallback = (...args) => {
    return isFunction(conditionalValueOrFunc)
      ? conditionalValueOrFunc(...args)
      : conditionalValueOrFunc;
  };

  const conditionCallback = (val, {key, reference}) => {
    const passed =
      (!keyToCompare || keyToCompare === key) && isEqual(val, valueCallback());

    if (passed) {
      propertiesArr.push(keyToCompare ? reference : {key, reference});
    }

    return passed;
  };

  traverseProperties({value, conditionCallback, valueCallback});

  return propertiesArr;
};

export const replaceProperties = (value, conditionCallback, valueOrFunc) => {
  const valueCallback = (...args) => {
    return isFunction(valueOrFunc) ? valueOrFunc(...args) : valueOrFunc;
  };

  return traverseProperties({
    value,
    conditionCallback,
    valueCallback,
  });
};

export const removeProperties = (value, conditionCallbackArg) => {
  const conditionCallback = (...args) => !conditionCallbackArg(...args);

  return traverseProperties({value, conditionCallback});
};

export const removeNilValues = (value) => removeProperties(value, isNil);

export const removeUndefinedValues = (value) =>
  removeProperties(value, isUndefined);

export const removeEmptyValues = (value) => removeProperties(value, isEmpty);

export const removeFunctions = (value) => removeProperties(value, isFunction);

export const replaceKeys = (value, keyMapping, valueOrFunc) => {
  const valueCallback = (...args) => {
    return isFunction(valueOrFunc) ? valueOrFunc(...args) : valueOrFunc;
  };

  return traverseProperties({
    value,
    keyMapping,
    valueCallback,
  });
};
