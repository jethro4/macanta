import React, {useState} from 'react';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import Tooltip from '@macanta/components/Tooltip';
import Popover from '@macanta/components/Popover';
import ContactRelationshipsQueryOption from './ContactRelationshipsQueryOption';
import ExportContactQueryOption from './ExportContactQueryOption';
import ContactSupportOption from './ContactSupportOption';
import DeleteContactQueryOption from './DeleteContactQueryOption';
import FamilyRestroomIcon from '@mui/icons-material/FamilyRestroom';
import DraftsIcon from '@mui/icons-material/Drafts';
import PhoneIcon from '@mui/icons-material/Phone';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import Permission from '@macanta/modules/hoc/Permission';
import PermissionMetaQuery from '@macanta/modules/hoc/PermissionMetaQuery';
import ContactNewPurchaseSelectionItem from '@macanta/modules/Metadata/ContactNewPurchaseSelectionItem';
import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import * as DataObjectsSearchTableStyled from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/styles';
import * as Storage from '@macanta/utils/storage';

const OptionView = ({contact, selectedType, onClose}) => {
  let optionView = null;

  switch (selectedType) {
    case 'contactRelationships': {
      optionView = (
        <ContactRelationshipsQueryOption
          modal
          showModal
          onClose={onClose}
          id={contact.id}
          name={`${contact.firstName || ''} ${contact.lastName || ''}`.trim()}
          email={contact.email}
        />
      );
      break;
    }
    case 'export': {
      optionView = (
        <ExportContactQueryOption
          modal
          showModal
          onClose={onClose}
          id={contact.id}
          name={`${contact.firstName || ''} ${contact.lastName || ''}`.trim()}
          email={contact.email}
        />
      );
      break;
    }
    case 'contactSupport': {
      optionView = (
        <ContactSupportOption
          modal
          showModal
          onClose={onClose}
          id={contact.id}
          name={`${contact.firstName || ''} ${contact.lastName || ''}`.trim()}
          email={contact.email}
        />
      );
      break;
    }
    case 'delete': {
      optionView = (
        <DeleteContactQueryOption
          showDialog
          onClose={onClose}
          contact={contact}
        />
      );
      break;
    }
  }

  return optionView;
};

const MoreOptions = ({contact, ...props}) => {
  const {userId} = Storage.getItem('userDetails');

  const [anchorEl, setAnchorEl] = useState(false);
  const [selectedType, setSelectedType] = useState('');

  const isLoggedInUser = userId === contact.id;
  const openMoreOptions = Boolean(anchorEl);

  const handleMoreOptions = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleCloseMoreOptions = () => {
    setAnchorEl(null);
  };

  const handleSelectType = (type) => () => {
    setSelectedType(type);
    handleCloseMoreOptions();
  };

  const handleClose = () => {
    setSelectedType('');
  };

  return (
    <>
      <Tooltip title="More">
        <DataObjectsSearchTableStyled.ActionButton
          style={{
            color: '#aaa',
          }}
          show={openMoreOptions}
          onClick={handleMoreOptions}
          {...props}>
          <MoreVertIcon />
        </DataObjectsSearchTableStyled.ActionButton>
      </Tooltip>
      <Popover
        open={openMoreOptions}
        anchorEl={anchorEl}
        onClose={handleCloseMoreOptions}
        anchorOrigin={{
          vertical: 'center',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'center',
          horizontal: 'right',
        }}>
        <div
          style={{
            width: 230,
          }}>
          <List
            style={{
              padding: 0,
            }}>
            <ContactNewPurchaseSelectionItem email={contact.email} />
            <NoteTaskHistoryStyled.SelectionItem
              button
              onClick={handleSelectType('contactRelationships')}>
              <FamilyRestroomIcon />
              <NoteTaskHistoryStyled.SelectionItemText primary="Contact Relationships" />
            </NoteTaskHistoryStyled.SelectionItem>
            <Divider />
            <Permission
              keys={[
                'allowContactExport',
                'allowDataObjectExport',
                'allowNoteTaskExport',
              ]}
              condition="allFalse">
              <NoteTaskHistoryStyled.SelectionItem
                button
                onClick={handleSelectType('export')}>
                <DraftsIcon />
                <NoteTaskHistoryStyled.SelectionItemText primary="Export" />
              </NoteTaskHistoryStyled.SelectionItem>
            </Permission>
            <Divider />
            <NoteTaskHistoryStyled.SelectionItem
              button
              onClick={handleSelectType('contactSupport')}>
              <PhoneIcon />
              <NoteTaskHistoryStyled.SelectionItemText primary="Contact Support" />
            </NoteTaskHistoryStyled.SelectionItem>
            {!isLoggedInUser && (
              <PermissionMetaQuery validate="deleteContact">
                <PermissionMetaQuery validate="deleteContact">
                  <Divider />
                  <NoteTaskHistoryStyled.SelectionItem
                    button
                    onClick={handleSelectType('delete')}>
                    <DeleteForeverIcon />
                    <NoteTaskHistoryStyled.SelectionItemText primary="Delete" />
                  </NoteTaskHistoryStyled.SelectionItem>
                </PermissionMetaQuery>
              </PermissionMetaQuery>
            )}
          </List>
        </div>
      </Popover>
      <OptionView
        contact={contact}
        selectedType={selectedType}
        onClose={handleClose}
      />
    </>
  );
};

MoreOptions.defaultProps = {
  relationships: [],
  connectedContacts: [],
  refetch: () => {},
};

export default MoreOptions;
