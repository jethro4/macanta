import React from 'react';
import Box from '@mui/material/Box';
import FlexGrid from '@macanta/components/Base/FlexGrid';
import BasicInfoForm from '@macanta/modules/AdminSettings/Automation/TriggerActions/SubForms/BasicInfoForm';
import withForm from '@macanta/components/Form/withForm';
import ActionForm from '@macanta/modules/AdminSettings/Automation/TriggerActions/SubForms/ActionForm';
import withPropHandler from '@macanta/hoc/withPropHandler';
import {dispatchResize} from '@macanta/utils/react';
import AutomationSetForm from '@macanta/modules/AdminSettings/Automation/AutomationGroup/AutomationSetForm';

let AutomationGroupForm = (props) => {
  const {values, setFieldValue} = props;

  const handleChange = (callback) => {
    setFieldValue('conditionsActions', callback(values.conditionsActions));
  };

  return (
    <ActionForm>
      <FlexGrid
        {...props}
        cols={2}
        margin={[0, '2rem']}
        // border={[0, 1]}
        sx={{
          padding: '1rem',
          height: '100%',
          ...props.sx,
        }}>
        <Box colWidth={0.7}>
          <BasicInfoForm labelPosition="normal" actionLabel="Group Name" />
        </Box>

        <AutomationSetForm
          data={values.conditionsActions}
          onChange={handleChange}
        />
      </FlexGrid>
    </ActionForm>
  );
};

AutomationGroupForm = withPropHandler(AutomationGroupForm, {
  propHandlers: {
    conditionsActions: dispatchResize,
  },
  parentPath: 'values',
  conditionCallback: (currValue, prevValue) =>
    currValue?.length !== prevValue?.length,
});

AutomationGroupForm = withForm(AutomationGroupForm, {
  mapPropsToValues: (props) => {
    return {
      conditionsActions: props.initialValues?.conditionsActions || [],
    };
  },
});

export default AutomationGroupForm;
