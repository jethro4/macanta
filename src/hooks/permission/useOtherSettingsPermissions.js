import get from 'lodash/get';
import useValue from '@macanta/hooks/base/useValue';

export const APP_SPECIFIC_PERMISSIONS = [
  {
    label: 'Hide Purchase Button?',
    value: 'hidePurchaseButton',
    apps: ['tk217'],
  },
];

export const OTHER_SETTINGS_PERMISSION_TYPES = [
  {
    title: 'Email',
    permissions: [
      {
        label: 'Allow User to Attach Files?',
        value: 'allowUserAttachment',
      },
      {
        label: 'Allow Users To Sync Their Email?',
        value: 'allowEmailSync',
      },
    ],
  },
  {
    title: 'Admin',
    permissions: [
      {
        label: 'Allow Users To Add/Remove Email Domain Filter?',
        value: 'allowAddRemoveEmailDomainFilter',
      },
      {
        label: 'Allow Contact Export?',
        value: 'allowContactExport',
      },
      {
        label: 'Allow Note/Task Export?',
        value: 'allowNoteTaskExport',
      },
      {
        label: 'Allow Data Object Export?',
        value: 'allowDataObjectExport',
      },
    ],
  },
  {
    title: 'Contact',
    permissions: [
      {
        label: 'Allow User to Add Direct Relationship?',
        value: 'allowAddDirectRelationship',
      },
      {
        label: 'Allow User to Merge Duplicate Contacts?',
        value: 'allowUserToMergeContacts',
      },
      {
        label: 'Allow User to Delete Contact Record?',
        value: 'allowUserDeleteContact',
      },
    ],
  },
  {
    title: 'Data Object',
    permissions: [
      {
        label: 'Allow User to Delete DO Item?',
        value: 'allowUserDeleteDOItem',
      },
      {
        label: 'Allow User to Delete DO Attachment?',
        value: 'allowUserDeleteDOAttachment',
      },

      {
        label: 'Hide Add Button For Specific Types',
        value: 'hideActionButtonsForGroups',
        hasOptions: true,
      },
      {
        label: 'Hide Connect Button For Specific Types',
        value: 'hideConnectButtonsForGroups',
        hasOptions: true,
      },
      {
        label: 'To a Data Object',
        value: 'doEditConnectedUser',
        isDOEdit: true,
      },
      {
        label: 'To a Data Object with Relationships',
        value: 'doEditConnectedUserWithRelationship',
        isDOEdit: true,
      },
    ],
  },
];

const useOtherSettingsPermissions = ({permission}) => {
  const [data] = useValue(() => {
    return OTHER_SETTINGS_PERMISSION_TYPES.map((otherType) => {
      const permissionsMap = otherType.permissions.reduce((acc, item) => {
        const accObj = {...acc};
        let permissionValue = get(permission?.permissionMeta, item.value);

        if (item.isDOEdit) {
          permissionValue = {
            ...permission?.permissionMeta?.userDOEditPermission,
          };
        }

        accObj[item.value] = {
          id: item.value,
          label: item.label,
          permissionValue,
          hasOptions: item.hasOptions,
          hasDOEditOptions: item.isDOEdit,
        };

        return accObj;
      }, {});

      return {
        title: otherType.title,
        permissionsMap,
      };
    });
  }, [permission]);

  return {data};
};

export default useOtherSettingsPermissions;
