import React, {useState, useEffect} from 'react';
import Box from '@mui/material/Box';
import MUIPopover from '@mui/material/Popover';
import CloseIcon from '@mui/icons-material/Close';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import * as Styled from './styles';

const Popover = ({
  showContentDelay,
  children,
  hideHeader,
  headerStyle,
  containerStyle,
  title,
  sx,
  ...props
}) => {
  const handleStopMouseEvent = (event) => {
    event.stopPropagation();
  };

  return (
    <MUIPopover
      sx={{
        ...sx,
        '.MuiPopover-paper': {
          display: 'flex',
          flexDirection: 'column',
          overflowY: 'hidden',
          ...sx?.['.MuiPopover-paper'],
        },
      }}
      onMouseUp={handleStopMouseEvent}
      onMouseDown={handleStopMouseEvent}
      {...props}>
      {!hideHeader && (
        <Styled.PopoverHeader style={headerStyle}>
          <Styled.HeaderTitle>{title}</Styled.HeaderTitle>
          <Styled.CloseButton
            aria-haspopup="true"
            onClick={props.onClose}
            {...(title
              ? {
                  style: {
                    padding: 6,
                    marginRight: 6,
                  },
                }
              : {
                  style: {
                    padding: 4,
                    marginRight: 8,
                  },
                })}>
            <CloseIcon style={{color: 'white'}} />
          </Styled.CloseButton>
        </Styled.PopoverHeader>
      )}
      <Box
        style={{
          flex: 1,
          overflowY: 'auto',
          ...containerStyle,
        }}>
        <PopoverBody showContentDelay={showContentDelay}>
          {children}
        </PopoverBody>
      </Box>
    </MUIPopover>
  );
};

const PopoverBody = ({children, showContentDelay}) => {
  const [showContent, setShowContent] = useState(!showContentDelay);

  useEffect(() => {
    if (!showContent) {
      setTimeout(() => {
        setShowContent(true);
      }, showContentDelay);
    }
  }, []);

  return showContent ? children : <LoadingIndicator fill />;
};

Popover.defaultProps = {
  disableEscapeKeyDown: true,
};

export default Popover;
