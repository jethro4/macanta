import React from 'react';
import taskValidationSchema from '@macanta/validations/task';
import useTaskMutation from './useTaskMutation';
import * as Storage from '@macanta/utils/storage';
import useValue from '@macanta/hooks/base/useValue';
import useContact from '@macanta/hooks/contact/useContact';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import TaskForms from '@macanta/modules/NoteTaskForms/TaskForms/TaskForms';

const ContactTaskForms = ({
  id,
  contactId,
  userId,
  title = '',
  note = '',
  tags,
  onSuccess,
}) => {
  const isEdit = !!id;

  const contactQuery = useContact(contactId, {
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const [initValues] = useValue(() => {
    const userEmail = contactQuery.initLoaded ? contactQuery.data.email : '';

    const initValuesObj = {
      ...(isEdit ? {id} : {contactId}),
      title,
      userEmail,
      note,
      userId,
      tags,
    };

    return initValuesObj;
  }, [contactQuery.initLoaded]);

  const {callCreateTaskMutation, loading, error} = useTaskMutation({onSuccess});

  const handleFormSubmit = (values) => {
    const session = Storage.getItem('session');

    callCreateTaskMutation({
      variables: {
        createTaskInput: {...values, sessionName: session?.sessionId},
        __mutationkey: 'createTaskInput',
      },
    });
  };

  return (
    <TaskForms
      initValues={initValues}
      validationSchema={taskValidationSchema}
      onSubmit={handleFormSubmit}
      error={error}
      loading={loading}
    />
  );
};

ContactTaskForms.defaultProps = {
  tags: [],
};

export default ContactTaskForms;
