import React, {useState, useEffect, useRef} from 'react';
import useQuery from '@macanta/hooks/apollo/useQuery';
import Button from '@macanta/components/Button';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import ButtonGroup from '@macanta/components/ButtonGroup';
import Form from '@macanta/components/Form';
import Section from '@macanta/containers/Section';
import TurnedInIcon from '@mui/icons-material/TurnedIn';
import Typography from '@mui/material/Typography';
import DOItemForms from '@macanta/modules/DataObjectItems/DOItemForms';
import ContactQuery from '@macanta/modules/hoc/ContactQuery';
import useDOItemMutation from '@macanta/modules/DataObjectItems/DOItemForms/useDOItemMutation';
import {
  transformConnectedContacts,
  transformAttachments,
  filterAndTransformFieldValues,
  getDefaultConnectedContacts as getDefaultConnectedContactsHelper,
} from '@macanta/selectors/dataObject.selector';
import {getFieldValue} from '@macanta/selectors/field.selector';
import useValue from '@macanta/hooks/base/useValue';
import useContactId from '@macanta/hooks/useContactId';
import useContact from '@macanta/hooks/contact/useContact';
import {GET_DATA_OBJECT} from '@macanta/graphql/dataObjects';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import dataObjectValidationSchema from '@macanta/validations/dataObject';
import DORequiredFieldsDialog from './DORequiredFieldsDialog';
import * as DOStyled from '@macanta/modules/DataObjectItems/styles';
import * as FormStyled from '@macanta/modules/NoteTaskForms/styles';
import * as Storage from '@macanta/utils/storage';
import {
  getFullName,
  transformContact,
} from '@macanta/selectors/contact.selector';

const DO_ITEM_FORM_TABS = [
  {
    label: 'Details',
    value: 'details',
  },
  {
    label: 'Relationships',
    value: 'relationships',
  },
  {
    label: 'Additional Info',
    value: 'additionalInfo',
  },
];

const DOItemEditSectionWithContact = ({
  defaultContactId,
  defaultConnectedContact,
  item: itemProp,
  groupId,
  fields,
  relationships,
  onSave,
  loadInfo,
}) => {
  const doItemId = itemProp?.id;
  const isEdit = !!doItemId;

  const isPrevEmptyContact = isEdit && !defaultContactId;

  const [selectedTab, setSelectedTab] = useState(DO_ITEM_FORM_TABS[0].value);

  const {loading: doItemLoading, data: doItemData} = useQuery(GET_DATA_OBJECT, {
    skip: !doItemId,
    variables: {
      itemId: doItemId,
    },
  });

  const updatedItem = doItemData?.getDataObject;
  const item = updatedItem || itemProp;

  const getDefaultRelationships = () => {
    const {userId: loggedInUserId} = Storage.getItem('userDetails');

    const isLoggedInUser = loggedInUserId === defaultContactId;

    return relationships.filter((r) =>
      isLoggedInUser ? r.autoAssignLoggedInUser : r.autoAssignContact,
    );
  };

  const getPrimaryRelationship = (defaultRelValues) => {
    if (!isEdit) {
      const contactRelationship = defaultRelValues.find(
        (c) => c.connectedContactId === defaultContactId,
      );
      const defaultRelationships = getDefaultRelationships();

      return {
        connectedContactId: defaultConnectedContact?.id,
        relationships: contactRelationship
          ? contactRelationship.relationships.join(',')
          : defaultRelationships.map((r) => r.role).join(','),
      };
    } else {
      const hasPrimaryContact =
        isPrevEmptyContact ||
        defaultRelValues.some((c) => c.contactId === defaultContactId);

      const updatedConnectedContacts =
        hasPrimaryContact && defaultRelValues?.length
          ? defaultRelValues
              .filter(
                (relContact) =>
                  !relContact.relationships.some(
                    (relName) => !isPrevEmptyContact || relName === 'unlink',
                  ),
              )
              .map((relContact) => ({
                contactId: relContact.connectedContactId,
                relationships: relContact.relationships.map((relName) =>
                  relationships.find((rel) => rel.role === relName),
                ),
              }))
          : item.connectedContacts;

      if (updatedConnectedContacts.length) {
        const contactRelationship =
          updatedConnectedContacts.find(
            (c) => c.contactId === defaultContactId,
          ) || updatedConnectedContacts[0];

        const selectedRelationships = contactRelationship?.relationships?.length
          ? contactRelationship.relationships
          : [];

        return {
          connectedContactId: contactRelationship?.contactId,
          relationships: selectedRelationships
            .map((r) => {
              const rel = relationships.find((rs) => rs.id === r.id);

              return rel?.role;
            })
            .filter((r) => !!r)
            .join(','),
        };
      }
    }

    return {};
  };

  const getDefaultConnectedContactRelationships = () => {
    const loggedInUser = Storage.getItem('userDetails');

    const isLoggedInUser = loggedInUser.userId === defaultContactId;

    const loggedInUserRelationships =
      relationships?.filter((r) => r.autoAssignLoggedInUser) || [];
    const contactRelationships =
      relationships?.filter((r) => r.autoAssignContact) || [];

    let relationshipsArr = [];

    if (
      isLoggedInUser ||
      (!isLoggedInUser && loggedInUserRelationships.length)
    ) {
      relationshipsArr.push({
        contactId: loggedInUser.userId,
        email: loggedInUser.email,
        firstName: loggedInUser.firstName,
        lastName: loggedInUser.lastName,
        relationships: loggedInUserRelationships,
      });
    }

    if (!isLoggedInUser && defaultConnectedContact) {
      relationshipsArr.push({
        contactId: defaultConnectedContact.id,
        email: defaultConnectedContact.email,
        firstName: defaultConnectedContact.firstName,
        lastName: defaultConnectedContact.lastName,
        relationships: contactRelationships,
      });
    }

    return relationshipsArr;
  };

  const getInitRelationships = () => {
    const connectedContactRelationships = getDefaultConnectedContactRelationships();

    return doItemId
      ? []
      : connectedContactRelationships.map((connectedContact) => ({
          firstName: connectedContact.firstName,
          lastName: connectedContact.lastName,
          email: connectedContact.email,
          connectedContactId: connectedContact.contactId,
          relationships: connectedContact.relationships.map(({role}) => role),
        }));
  };

  const [contactId] = useValue(() => {
    const primaryRelationship = getPrimaryRelationship(getInitRelationships());
    return defaultContactId || primaryRelationship?.connectedContactId;
  }, [item]);

  const {saving, save, error} = useDOItemMutation(
    item?.id,
    groupId,
    defaultConnectedContact?.email,
    {
      onCompleted: ({values}) => {
        onSave({values, itemId: item?.id, contactId});
      },
    },
  );

  const handleSelectTab = (val) => () => {
    setSelectedTab(val);
  };

  const getDefaultConnectedContacts = () => {
    return getDefaultConnectedContactsHelper({
      connectedContacts: item?.connectedContacts,
      relationships,
      contactId,
      defaultConnectedContact,
    });
  };

  const getDefaultDetails = () => {
    const data = updatedItem?.data || itemProp?.data;
    const connectedContacts = getDefaultConnectedContacts();

    const details =
      fields?.reduce((acc, field) => {
        const itemData = data?.find((d) => d.fieldId === field.id);
        const contactSpecificValues = connectedContacts.map((contact) => {
          const contactValue = itemData?.contactSpecificValues?.find(
            (contactValueItem) =>
              contactValueItem.contactId === contact.contactId,
          )?.value;
          return {
            ...contact,
            value: contactValue,
          };
        });
        const attachments = transformAttachments({
          attachments: updatedItem?.attachments,
          connectedContacts,
          field,
          contactId,
        });

        acc[field.name] = {
          fieldId: field.id,
          value: getFieldValue({isEdit, field, itemData, contactId}),
          contactSpecificValues,
          attachments,
        };

        return acc;
      }, {}) || {};

    return details;
  };

  const getDefaultAttachments = () => {
    const connectedContacts = getDefaultConnectedContacts();

    return transformAttachments({
      attachments: updatedItem?.attachments,
      connectedContacts,
      fields,
      contactId,
    });
  };

  const getInitValues = () => {
    return {
      details: getDefaultDetails(),
      connectedContacts: getDefaultConnectedContacts(),
      notes: item?.notes || [],
      attachments: getDefaultAttachments(),
      loading: doItemLoading,
    };
  };

  const initDefaultDetails = useRef(getDefaultDetails()).current;
  const defaultValues = useRef({
    groupId,
    doItemId,
    details: {},
    relationships: getInitRelationships(),
    notes: [],
    attachments: [],
    directDORelationships: {data: []},
    deletedDirectDORelationships: {data: []},
    context: {
      details: initDefaultDetails,
      fields,
      connectedContacts: getDefaultConnectedContacts(),
    },
  }).current;
  const [initValues, setInitValues] = useState({
    details: initDefaultDetails,
    connectedContacts: getDefaultConnectedContacts(),
    notes: item?.notes || [],
    attachments: getDefaultAttachments(),
    loading: true,
  });

  const handleSave = async (values, actions) => {
    const primaryRelationship = getPrimaryRelationship(values.relationships);

    if (!isEdit && !values.relationships.length) {
      alert('Required at least one relationship');

      return;
    }

    const flatDetails = Object.entries(initValues.details).reduce(
      (acc, [fieldName, d]) => {
        acc[fieldName] = d.value;
        return acc;
      },
      {},
    );

    let details =
      !isEdit || isPrevEmptyContact
        ? {
            ...flatDetails,
            ...values.details,
          }
        : values.details;

    await save({
      ...values,
      details: filterAndTransformFieldValues(details, fields),
      notes: values.notes.map((n) => ({
        id: n.id,
        note: n.note,
        tags: n.tags,
        title: n.title,
      })),
      primaryRelationship,
      relationships: !doItemId
        ? values.relationships.filter(
            (rel) => rel.connectedContactId !== contactId,
          )
        : values.relationships,
      directDORelationships: values.directDORelationships,
      deletedDirectDORelationships: values.deletedDirectDORelationships,
    });

    setTimeout(() => {
      actions.resetForm({});
    }, 0);
  };

  useEffect(() => {
    if (fields && relationships && updatedItem && !doItemLoading) {
      setInitValues(getInitValues());
    }
  }, [fields, relationships, updatedItem, doItemLoading]);

  return (
    <>
      <Form
        initialValues={defaultValues}
        enableReinitialize={true}
        validationSchema={dataObjectValidationSchema}
        onSubmit={handleSave}>
        {({values, setFieldValue, handleSubmit, dirty}) => {
          const primaryContactId =
            getPrimaryRelationship(values.relationships)?.connectedContactId ||
            defaultContactId;

          const handleValueChange = (doFormKey, formData, options = {}) => {
            if (doFormKey === 'relationships') {
              const rels = values[doFormKey];
              const rel = rels.find(
                (r) => r.connectedContactId === formData.connectedContactId,
              );
              let updatedRelationships = [];

              if (rel) {
                const transformedConnectedContacts = transformConnectedContacts(
                  item?.connectedContacts,
                  relationships,
                );

                if (
                  options.type === 'delete' &&
                  !transformedConnectedContacts?.some((contact) => {
                    return contact.contactId === formData.connectedContactId;
                  })
                ) {
                  updatedRelationships = rels.filter(
                    (r) => r.connectedContactId !== formData.connectedContactId,
                  );
                } else {
                  updatedRelationships = rels.map((r) => {
                    if (r.connectedContactId === formData.connectedContactId) {
                      return {
                        ...r,
                        connectedContactId: formData.connectedContactId,
                        relationships: formData.relationships,
                        ...(options.type === 'delete' && {
                          deleted: true,
                        }),
                      };
                    }

                    return r;
                  });
                }
              } else {
                const {firstName, lastName, email} = formData.contact;

                updatedRelationships = rels.concat({
                  firstName,
                  lastName,
                  email,
                  connectedContactId: formData.connectedContactId,
                  relationships: formData.relationships,
                  ...(options.type === 'delete' && {
                    deleted: true,
                  }),
                });
              }

              setFieldValue(doFormKey, updatedRelationships);
            }

            if (doFormKey === 'notes') {
              const notes = values[doFormKey];
              const note = notes.find((n) =>
                n.id ? n.id === formData.id : n.tempId === formData.tempId,
              );
              let updatedNotes = [];

              if (note) {
                updatedNotes = notes.map((n) => {
                  if (
                    n.id ? n.id === formData.id : n.tempId === formData.tempId
                  ) {
                    return formData;
                  }

                  return n;
                });
              } else {
                updatedNotes = notes.concat(formData);
              }

              setFieldValue(doFormKey, updatedNotes);
            }

            if (doFormKey === 'attachments') {
              const attachments = values[doFormKey];
              const attachment = attachments.find((a) => a.id === formData.id);
              let updatedAttachments = [];

              if (attachment) {
                if (options.type === 'delete') {
                  updatedAttachments = attachments.filter(
                    (a) => a.id !== formData.id,
                  );
                } else {
                  updatedAttachments = attachments.map((a) => {
                    if (a.id === formData.id) {
                      return formData;
                    }

                    return a;
                  });
                }
              } else {
                if (options.type === 'delete') {
                  updatedAttachments = attachments.concat({
                    id: formData.id,
                    deleted: true,
                  });
                } else {
                  updatedAttachments = attachments.concat(formData);
                }
              }

              setFieldValue(doFormKey, updatedAttachments);
            }

            if (doFormKey === 'directDORelationships') {
              if (!formData.isDelete) {
                setFieldValue('directDORelationships', {
                  ...formData,
                  data: formData.data,
                });
              } else {
                setFieldValue('deletedDirectDORelationships', {
                  data: formData.data,
                });
              }
            }
          };

          return (
            <>
              <Section
                fullHeight
                style={{
                  margin: 0,
                  boxShadow: 'none',
                }}
                bodyStyle={{backgroundColor: '#f5f6fa'}}
                HeaderLeftComp={
                  <DOItemHeaderLeft
                    id={item?.id}
                    primaryContactId={primaryContactId}
                  />
                }
                HeaderRightComp={
                  <ButtonGroup variant="text" aria-label="text button group">
                    {DO_ITEM_FORM_TABS.map((tab) => {
                      return (
                        <DOStyled.DOItemNavBtn
                          key={tab.value}
                          onClick={handleSelectTab(tab.value)}>
                          {tab.label}
                        </DOStyled.DOItemNavBtn>
                      );
                    })}
                  </ButtonGroup>
                }
                FooterComp={
                  <FormStyled.Footer
                    style={{
                      padding: '1rem',
                      margin: 0,
                      borderTop: '1px solid #eee',
                      width: '100%',
                      backgroundColor: '#f5f6fa',
                    }}>
                    {!!error && (
                      <Typography
                        style={{
                          flex: 1,
                          marginRight: '1rem',
                        }}
                        color="error"
                        variant="subtitle2"
                        align="center">
                        {error.message}
                      </Typography>
                    )}
                    <Button
                      disabled={!dirty && !error}
                      variant="contained"
                      startIcon={<TurnedInIcon />}
                      onClick={handleSubmit}
                      size="medium">
                      Save Changes
                    </Button>
                  </FormStyled.Footer>
                }>
                <DOItemForms
                  loadingItem={loadInfo && initValues.loading}
                  doItemId={doItemId}
                  isEdit={isEdit}
                  groupId={groupId}
                  relationships={relationships}
                  fields={fields}
                  onChange={handleValueChange}
                  selectedTab={selectedTab}
                  onSelectTab={handleSelectTab}
                  data={initValues}
                />
              </Section>
              <DORequiredFieldsDialog onSave={handleSubmit} />
            </>
          );
        }}
      </Form>
      <LoadingIndicator modal loading={saving} />
    </>
  );
};

const DOItemHeaderLeft = ({id, primaryContactId}) => {
  const isEdit = !!id;

  const {
    initLoading: initLoadingPrimaryContact,
    data: primaryConnectedContact,
  } = useContact(primaryContactId, {
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const [primaryContact] = useValue(
    () => transformContact(primaryConnectedContact),
    [primaryConnectedContact],
  );

  return (
    <Typography color="grey" fontWeight="bold">
      {`${!isEdit ? 'Add Item' : id}${
        initLoadingPrimaryContact || !primaryContactId
          ? ''
          : ` (${getFullName(primaryContact)})`
      }`}
    </Typography>
  );
};

const DOItemEditSection = (props) => {
  const isEdit = !!props.item?.id;
  const contactId = useContactId({noDefault: isEdit});

  return (
    <ContactQuery
      id={contactId}
      options={{fetchPolicy: FETCH_POLICIES.CACHE_FIRST}}>
      {({data: selectedContact}) => {
        return (
          <DOItemEditSectionWithContact
            defaultContactId={contactId}
            defaultConnectedContact={selectedContact}
            {...props}
          />
        );
      }}
    </ContactQuery>
  );
};

export default DOItemEditSection;
