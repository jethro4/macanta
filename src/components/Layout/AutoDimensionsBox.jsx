import React from 'react';
import Box from '@mui/material/Box';
import useDimensions from '@macanta/hooks/base/useDimensions';
import useValue from '@macanta/hooks/base/useValue';

const AutoDimensionsBox = ({children, onResize, ...props}) => {
  const {dimensions, setRef} = useDimensions(onResize);

  const [boxDimensions] = useValue(
    () => ({
      width: dimensions.width,
      height: dimensions.height,
    }),
    [dimensions.width, dimensions.height],
  );

  return (
    <Box
      ref={setRef}
      {...props}
      {...Object.assign({
        width: boxDimensions.width || 'fit-content',
        height: boxDimensions.height || 'fit-content',
      })}>
      {children}
    </Box>
  );
};

export default AutoDimensionsBox;
