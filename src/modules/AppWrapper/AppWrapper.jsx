import React from 'react';
import {useReactiveVar} from '@apollo/client';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import useAppSettings from '@macanta/hooks/admin/useAppSettings';
import useAccessPermissions from '@macanta/hooks/admin/useAccessPermissions';
import useCacheBuster from '@macanta/hooks/useCacheBuster';
import {loggedInVar, sessionExpiredVar} from '@macanta/graphql/cache/vars';
import Bootstrap from './Bootstrap';

const AppWrapper = ({children}) => {
  const loggedIn = useReactiveVar(loggedInVar);
  const sessionExpired = useReactiveVar(sessionExpiredVar);

  const cacheBuster = useCacheBuster();
  const appSettingsQuery = useAppSettings();
  const accessPermissionsQuery = useAccessPermissions();

  const appSettings = appSettingsQuery.data;
  const accessPermissions = accessPermissionsQuery.data;

  if (
    cacheBuster.loading ||
    !appSettings ||
    (loggedIn && !sessionExpired && !accessPermissions)
  ) {
    return <LoadingIndicator modal loading />;
  }

  return (
    <>
      <Bootstrap />
      {children}
    </>
  );
};

export default AppWrapper;
