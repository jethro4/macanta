import React from 'react';
import BlastProgressTable from './BlastProgressTable';
import Modal from '@macanta/components/Modal';

const BlastProgressModal = ({showModal = false, onClose, data, ...props}) => {
  return (
    <Modal
      headerTitle="Blast Progress"
      open={showModal}
      onClose={onClose}
      contentWidth={'100%'}
      contentHeight={'94%'}
      {...props}>
      <BlastProgressTable data={data} />
    </Modal>
  );
};

export default BlastProgressModal;
