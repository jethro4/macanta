import React from 'react';
import PropTypes from 'prop-types';
import TextField from '@macanta/components/Forms/InputFields/TextField';

const TextAreaField = ({autoSize, fullHeight, rows, ...props}) => {
  return (
    <TextField
      {...props}
      {...(!autoSize &&
        !fullHeight && {
          rows,
        })}
      {...(fullHeight && {
        InputProps: {
          ...props.InputProps,
          style: {
            flex: 1,
            flexDirection: 'column',
            alignItems: 'flex-start',
            ...props.InputProps?.style,
          },
        },
        inputProps: {
          ...props.inputProps,
          style: {
            height: 'auto',
            overflow: 'auto',
            flex: 1,
            ...props.inputProps?.style,
          },
        },
      })}
    />
  );
};

TextAreaField.propTypes = {
  multiline: PropTypes.bool,
  fullHeight: PropTypes.bool,
  autoSize: PropTypes.bool,
  rows: PropTypes.number,
};

TextAreaField.defaultProps = {
  multiline: true,
  fullHeight: false,
  autoSize: false,
  rows: 3,
};

export default TextAreaField;
