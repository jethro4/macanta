import React from 'react';
import RestartAltIcon from '@mui/icons-material/RestartAlt';
import DeleteIcon from '@mui/icons-material/Delete';
import Tooltip from '@macanta/components/Tooltip';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import {useMutation} from '@apollo/client';
import {DELETE_WIDGET} from '@macanta/graphql/admin';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import EditWidgetIconBtn from './EditWidgetIconBtn';
import * as Styled from './styles';

const WidgetCard = ({
  item,
  widgetCounts,
  onClick,
  onEdit,
  onDelete,
  loading: forceLoading,
  onForceReload,
  ...props
}) => {
  const {displayMessage} = useProgressAlert();

  const [callWidgetMutation, widgetMutation] = useMutation(DELETE_WIDGET, {
    onCompleted(data) {
      if (data.deleteWidget?.success) {
        displayMessage('Deleted widget successfully!');
        onDelete(data.deleteWidget.widgetId);
      }
    },
    onError() {},
  });

  const handleStopMouseEvent = (event) => {
    event.stopPropagation();
  };

  const handleDelete = (event) => {
    handleStopMouseEvent(event);

    const newWidget = {
      id: item.id,
      userId: item.userId,
      queryId: item.queryId,
    };

    callWidgetMutation({
      variables: {
        deleteWidgetInput: newWidget,
        __mutationkey: 'deleteWidgetInput',
      },
    });
  };

  const handleClick = (range) => () => {
    onClick(range);
  };

  const hasActionButtons = !(
    item.type === 'MacantaTaskSearch' && item.addedBy === 'System'
  );

  const currentCount = widgetCounts?.currentCount || item.currentCount;
  const last7DayCount = widgetCounts?.last7DayCount || item.last7DayCount;
  const last30DayCount = widgetCounts?.last30DayCount || item.last30DayCount;
  const last90DayCount = widgetCounts?.last90DayCount || item.last90DayCount;
  const allCount = widgetCounts?.allCount || item.allCount;

  return (
    <Styled.Root {...props}>
      <Styled.ActionArea onClick={handleClick('main')}>
        <Styled.Header>
          <Styled.Title
            TooltipProps={{
              placement: 'top',
            }}>
            {item.title}
          </Styled.Title>
        </Styled.Header>
        <Styled.Content>
          <Styled.Quantity>{currentCount}</Styled.Quantity>
        </Styled.Content>
      </Styled.ActionArea>
      {hasActionButtons && (
        <Styled.CardActions disableSpacing className="card-actions">
          <EditWidgetIconBtn
            widget={item}
            onSuccess={onEdit}
            TooltipProps={{title: 'Edit'}}
          />
          <Styled.ActionIconButton
            disabled={forceLoading}
            onClick={onForceReload}
            onMouseUp={handleStopMouseEvent}
            onMouseDown={handleStopMouseEvent}
            TooltipProps={{title: 'Reload Count'}}>
            <RestartAltIcon
              style={{
                fontSize: '1rem',
                color: 'white',
              }}
            />
          </Styled.ActionIconButton>
          <Styled.ActionIconButton
            onClick={handleDelete}
            onMouseUp={handleStopMouseEvent}
            onMouseDown={handleStopMouseEvent}
            TooltipProps={{title: 'Delete'}}>
            <DeleteIcon
              style={{
                fontSize: '1rem',
                color: 'white',
              }}
            />
          </Styled.ActionIconButton>
        </Styled.CardActions>
      )}
      <Styled.Footer>
        <Tooltip title="7 Days" placement="top">
          <Styled.FooterButtonContainer>
            <Styled.FooterButton fullWidth onClick={handleClick('7')}>
              <Styled.FooterText>{last7DayCount}</Styled.FooterText>
            </Styled.FooterButton>
          </Styled.FooterButtonContainer>
        </Tooltip>
        <Tooltip title="30 Days" placement="top">
          <Styled.FooterButtonContainer>
            <Styled.FooterButton fullWidth onClick={handleClick('30')}>
              <Styled.FooterText>{last30DayCount}</Styled.FooterText>
            </Styled.FooterButton>
          </Styled.FooterButtonContainer>
        </Tooltip>
        <Tooltip title="90 Days" placement="top">
          <Styled.FooterButtonContainer>
            <Styled.FooterButton fullWidth onClick={handleClick('90')}>
              <Styled.FooterText>{last90DayCount}</Styled.FooterText>
            </Styled.FooterButton>
          </Styled.FooterButtonContainer>
        </Tooltip>
        <Tooltip title="History" placement="top">
          <Styled.FooterButtonContainer>
            <Styled.FooterButton fullWidth onClick={handleClick('all')}>
              <Styled.FooterText>{allCount}</Styled.FooterText>
            </Styled.FooterButton>
          </Styled.FooterButtonContainer>
        </Tooltip>
      </Styled.Footer>
      <LoadingIndicator
        fill
        backdrop
        loading={forceLoading}
        backdropStyle={{
          pointerEvents: 'none',
        }}
      />
      <LoadingIndicator modal loading={widgetMutation.loading} />
    </Styled.Root>
  );
};

export default WidgetCard;
