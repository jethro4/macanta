/* eslint-disable react/jsx-filename-extension */
import React, {useEffect} from 'react';
import LoginPage from '@macanta/modules/pages/LoginPage';
import AppContainer from '@macanta/modules/pages/AppContainer';
import BaseTheme from '@macanta/modules/BaseTheme';
import {navigate} from 'gatsby';
import {isLoggedIn} from '@macanta/utils/app';

const IndexPage = () => {
  useEffect(() => {
    if (isLoggedIn() && window.location.pathname === '/') {
      console.info('Redirecting to dashboard...');

      navigate('/app/dashboard');
    }
  }, []);

  return (
    <BaseTheme>{!isLoggedIn() ? <LoginPage /> : <AppContainer />}</BaseTheme>
  );
};

export default IndexPage;
