import {gql} from '@apollo/client';
import {LIST_ASSIGNMENT_FRAGMENT} from './fragments';

export default gql`
  mutation createOrUpdateListAssignment(
    $createOrUpdateListAssignmentInput: CreateOrUpdateListAssignmentInput!
  ) {
    createOrUpdateListAssignment(input: $createOrUpdateListAssignmentInput) {
      ...listAssignmentFragment
    }
  }
  ${LIST_ASSIGNMENT_FRAGMENT}
`;
