import {gql} from '@apollo/client';

export default gql`
  mutation forgotPassword($forgotPasswordInput: ForgotPasswordInput!) {
    forgotPassword(input: $forgotPasswordInput) {
      success
    }
  }
`;
