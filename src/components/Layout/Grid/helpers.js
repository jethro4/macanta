import groupBy from 'lodash/groupBy';
import sortBy from 'lodash/sortBy';

export const getDataGrid = ({id, index, maxColumn = 1, ...custom}) => {
  return {
    w: 1,
    h: 1,
    minW: 1,
    minH: 1,
    i: String(id),
    ...(maxColumn && {
      x: index % maxColumn,
      y: Math.floor(index / maxColumn),
    }),
    ...custom,
  };
};

export const getGroupedItems = (data, groupByProp) => {
  const groupedItemsObj = groupBy(data, groupByProp);

  const groupedItemsArr = Object.values(groupedItemsObj).reduce(
    (acc, groupedItems, columnIndex) => {
      const accArr = acc.slice();

      groupedItems.forEach((item, rowIndex) => {
        accArr.push({
          ...item,
          dataGrid: getDataGrid({
            id: item.id,
            x: columnIndex,
            y: rowIndex,
          }),
        });
      });

      return accArr;
    },
    [],
  );

  return groupedItemsArr;
};

export const sortDescendingBreakpoints = (breakpointsObj) => {
  const breakpoints = Object.entries(breakpointsObj);

  return sortBy(breakpoints, (entry) => -entry[1]);
};

export const getCols = (breakpoints, maxColumn) => {
  const sortedBreakpoints = sortDescendingBreakpoints(breakpoints);

  return sortedBreakpoints.reduce((acc, entry, index) => {
    const accObj = {...acc};

    const breakpointName = entry[0];
    accObj[breakpointName] = maxColumn || sortedBreakpoints.length - index;

    return accObj;
  }, {});
};
