import React from 'react';
import Box from '@mui/material/Box';
import EditIcon from '@mui/icons-material/Edit';
import VisibilityIcon from '@mui/icons-material/Visibility';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import {
  DialogButton,
  ModalButton,
  PopoverButton,
} from '@macanta/components/Button';

const ItemActionButtons = ({
  type,
  item,
  renderFormComponent,
  onDelete,
  showDetails,
  modal,
  containerProps,
  ...props
}) => {
  const Button = modal ? ModalButton : PopoverButton;
  const containerPropsKey = modal ? 'ModalProps' : 'PopoverProps';
  const titlePropKey = modal ? 'headerTitle' : 'title';

  const label = item.name ? `"${item.name}"` : 'this item';

  return (
    <Box
      {...props}
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        ...props.style,
      }}>
      <Button
        style={{
          padding: 0,
        }}
        icon
        sx={{
          padding: '0.25rem',
          color: 'info.main',
        }}
        size="small"
        TooltipProps={{
          title: showDetails ? 'Show Details' : 'Edit',
        }}
        {...{
          [containerPropsKey]: {
            [titlePropKey]: showDetails ? item.id : `Edit (${item.id})`,
            contentWidth: 1100,
            contentMinHeight: 350,
            bodyStyle: {
              backgroundColor: '#f5f6fA',
            },
            ...containerProps,
          },
        }}
        renderContent={(handleClose) => renderFormComponent?.(handleClose)}>
        {showDetails ? (
          <VisibilityIcon />
        ) : (
          <EditIcon
            style={{
              fontSize: 20,
            }}
          />
        )}
      </Button>

      <DialogButton
        icon
        size="small"
        TooltipProps={{
          title: 'Delete',
        }}
        DialogProps={{
          title: 'Are you sure?',
          description: [
            `You are about to delete ${label} from ${type}.`,
            'This cannot be undone. Do you want to proceed?',
          ],
          onActions: (handleClose) => {
            return [
              {
                label: 'Delete',
                onClick: async () => {
                  await onDelete(item.id);

                  handleClose();
                },
                startIcon: <DeleteForeverIcon />,
              },
            ];
          },
        }}>
        <DeleteForeverIcon
          sx={{
            color: 'grayDarker.main',
          }}
        />
      </DialogButton>
    </Box>
  );
};

export default ItemActionButtons;
