import React, {useState} from 'react';
import Modal from '@macanta/components/Modal';
import TriggerConditionForms from './TriggerConditionForms';
import AddIcon from '@mui/icons-material/Add';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import Button from '@macanta/components/Button';

const StartAutomationGroupBtn = ({onSuccess, ...props}) => {
  const [showModal, setShowModal] = useState(false);
  const {displayMessage} = useProgressAlert();
  const handleShowModal = () => {
    console.log('Selected:', props);
    const workFlowAction = props?.workFlowAction || false;
    if (workFlowAction && workFlowAction === 'create') {
      setShowModal(true);
    }
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  const handleSave = () => {
    displayMessage('Successfully added query');

    onSuccess();
    handleCloseModal();
  };

  return (
    <>
      <Button
        onClick={handleShowModal}
        size="small"
        variant="contained"
        startIcon={<AddIcon />}
        {...props}>
        Start
      </Button>
      <Modal
        headerTitle={`Create A New Trigger Condition`}
        open={showModal}
        onClose={handleCloseModal}
        contentWidth={1400}
        contentHeight={'96%'}>
        <TriggerConditionForms onSave={handleSave} />
      </Modal>
    </>
  );
};

export default StartAutomationGroupBtn;
