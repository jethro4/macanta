import React from 'react';

const RawRadio = ({id, options, ...props}) => {
  return !options
    ? null
    : options.map((o, index) => {
        const transformedId = `${id}-${index}`;

        return (
          <div key={transformedId} className="form-radio">
            <input type="radio" id={transformedId} value={o} {...props} />
            <label className="form-radio-label" htmlFor={transformedId}>
              {o}
            </label>
          </div>
        );
      });
};

export default RawRadio;
