import React from 'react';
import clsx from 'clsx';
import sumBy from 'lodash/sumBy';
import Box from '@mui/material/Box';
import {calculateGridInfo} from '@macanta/components/Base/FlexGrid/helpers';
import useUIState, {
  convertWithUnit,
} from '@macanta/components/Base/FlexGrid/useUIState';
import {isNumeric} from '@macanta/utils/numeric';

const getColumnWidth = (cols) => 100 / cols;
const getColumnWidthPercentage = (cols) => `${getColumnWidth(cols)}%`;

const getMaxWidthPercentage = ({cols, colWidth, hasColWidthSingleRow}) => {
  return !hasColWidthSingleRow
    ? getColumnWidth(cols)
    : getColumnWidth(cols) * colWidth;
};

const FlexGridItems = ({
  children,
  cols: colsProp,
  rowHeight,
  margin,
  containerPadding,
  border,
  borderColor,
  borderStyle,
  fitContent,
  hideOverflow,
}) => {
  const uiState = useUIState({
    margin,
    padding: containerPadding,
    border,
  });

  const childrenArray = React.Children.toArray(children);
  const cols = !colsProp ? childrenArray.length : colsProp;

  const isSingleRow = childrenArray.length === cols;
  const hasColWidthSingleRow =
    isSingleRow && childrenArray.some((child) => !!child.props?.colWidth);
  const hasFixedColWidthSingleRow =
    isSingleRow &&
    childrenArray.some(
      (child) => !!child.props?.colWidth && !isNumeric(child.props?.colWidth),
    );
  const totalColWidth =
    sumBy(childrenArray, (child) => {
      return isNumeric(child.props?.colWidth) ? child.props.colWidth : 0;
    }) || 1;
  const totalColWidthRemaining = cols - totalColWidth;
  const withoutColWidthLength = childrenArray.filter(
    (child) => !child.props?.colWidth,
  ).length;
  const noColWidth = totalColWidthRemaining / withoutColWidthLength;
  const itemsColWidthMap = childrenArray.reduce((acc, child, index) => {
    const accObj = {...acc};

    accObj[index] = child.props?.colWidth || noColWidth;

    return accObj;
  }, {});

  return childrenArray.map((child, index) => {
    if (!child) {
      return null;
    }

    const {
      positionX,
      positionY,
      isFirstItemInColumn,
      isFirstItemInRow,
      isLastItemInColumn,
      isLastItemInRow,
    } = calculateGridInfo({index, cols, total: children.length});

    const colWidth = fitContent ? 'fit-content' : itemsColWidthMap[index];
    const maxWidth = getMaxWidthPercentage({
      cols,
      colWidth,
      hasColWidthSingleRow,
    });

    const childElm = React.cloneElement(child, {
      style: {
        margin: 0,
        ...child.props.style,
      },
    });

    return (
      <Box
        key={String(index)}
        className={clsx(
          child.props.className,
          'grid-item',
          `grid-column-${positionX}`,
          `grid-row-${positionY}`,
          // isFirstItemInColumn && 'grid-column-first',
          // isFirstItemInRow && 'grid-row-first',
          isLastItemInColumn && 'grid-column-last',
          isLastItemInRow && 'grid-row-last',
        )}
        style={Object.assign(
          {},
          {
            position: 'relative',
            flexGrow: 1,
            flexBasis: `calc(${getColumnWidthPercentage(cols)} - ${
              uiState.horizontalMargin
            })`,
            maxWidth: !isNumeric(colWidth)
              ? colWidth
              : `calc(${maxWidth}% - ${uiState.horizontalMargin} / 2)`,
            marginRight: isLastItemInColumn ? 0 : uiState.horizontalMargin,
            marginBottom: isLastItemInRow ? 0 : uiState.verticalMargin,
            padding: `${uiState.verticalPadding} ${uiState.horizontalPadding}`,
          },
          uiState.addBorder && {
            borderTopWidth: 0,
            borderLeftWidth: 0,
            borderBottomWidth:
              !isFirstItemInRow && isLastItemInRow ? 0 : uiState.border.bottom,
            borderRightWidth:
              !isFirstItemInColumn && isLastItemInColumn
                ? 0
                : uiState.border.right,
            borderColor,
            borderStyle,
          },
          rowHeight && {
            height: convertWithUnit(rowHeight),
          },
          hideOverflow && {
            overflowX: 'hidden',
          },
          !child.props.colWidth &&
            hasFixedColWidthSingleRow && {
              maxWidth: 'unset',
            },
        )}
        sx={Object.assign(
          {},
          child.props.dividerRight && {
            maxWidth: !isNumeric(colWidth)
              ? colWidth
              : `calc(${maxWidth}% + ${uiState.horizontalMargin}) !important`,
            paddingRight: `${uiState.horizontalMargin} !important`,

            '&:after': Object.assign(
              {
                content: '""',
                display: 'block',
                position: 'absolute',
                top: '50%',
                transform: 'translateY(-50%)',
                right: 0,
                backgroundColor: 'borderLight.main',
                height: '65%',
              },
              isNumeric(child.props.dividerRight)
                ? {
                    width: convertWithUnit(child.props.dividerRight),
                  }
                : child.props.dividerRight,
            ),
          },
        )}>
        {childElm}
      </Box>
    );
  });
};

export default FlexGridItems;
