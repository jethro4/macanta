import isString from 'lodash/isString';
import isArray from 'lodash/isArray';
import moment from 'moment';
import {createDate} from '@macanta/utils/time';

export const transformAllItems = (items, keys = {}) => {
  const {itemLabelKey = 'label', itemValueKey = 'value'} = keys;

  return !items
    ? []
    : items
        .filter((item) => !!item)
        .map((item) =>
          !isString(item)
            ? {
                label: item[itemLabelKey],
                value: item[itemValueKey],
                ...item,
              }
            : {
                label: item,
                value: item,
              },
        );
};

export const transformValue = (value, multiple) => {
  if (multiple) {
    return (
      (!isArray(value) ? value?.split(',').filter((v) => !!v) : value) || []
    );
  }

  return value;
};

export const convertToDate = (value, format) => {
  if (!value) {
    return null;
  } else if (typeof value === 'string') {
    let date = createDate(value, format);

    if (!date.isValid()) {
      date = createDate(value);
    }

    return new Date(date.format('lll'));
  }
  return value;
};

export const formatDateToLocaleString = (dateObj, format) => {
  return dateObj
    ? moment(dateObj).utcOffset(-dateObj.getTimezoneOffset()).format(format)
    : '';
};
