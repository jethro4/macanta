import React from 'react';

const RawTextArea = (props) => {
  return <textarea {...props} />;
};

export default RawTextArea;
