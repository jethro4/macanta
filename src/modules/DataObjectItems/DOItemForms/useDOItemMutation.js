import {useState, useRef} from 'react';
import isEmpty from 'lodash/isEmpty';
import {
  CREATE_OR_UPDATE_DO_ITEM,
  CONNECT_OR_DISCONNECT_RELATIONSHIP,
  CREATE_OR_UPDATE_DO_NOTE,
  GET_DATA_OBJECT,
} from '@macanta/graphql/dataObjects';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import {useMutation as useMutationOld, useQuery} from '@apollo/client';
import useMutation from '@macanta/hooks/apollo/useMutation';
import {getAppInfo} from '@macanta/utils/app';
import {GET_CONTACT_METADATA} from '@macanta/graphql/contacts';
import {sortArrayByObjectGroups} from '@macanta/utils/array';
import {syncPromises} from '@macanta/utils/promise';
import useUpload from './useUpload';
import * as Storage from '@macanta/utils/storage';
import envConfig from '@macanta/config/envConfig';
import {
  CONNECT_DO_TO_DO_RELATIONSHIP,
  DO_TO_DO_RELATIONSHIP_FRAGMENT,
  DELETE_DO_TO_DO_RELATIONSHIP,
} from '@macanta/graphql/relationships';
import {deleteItem, writeItems} from '@macanta/graphql/helpers';

const useDOItemMutation = (id, groupId, contactEmail, queryOptions) => {
  const {displayMessage} = useProgressAlert();

  const doItemIdRef = useRef(id);
  const valuesParamsRef = useRef({});

  const [saving, setSaving] = useState(false);

  const {refetch: refetchContactMetadata} = useQuery(GET_CONTACT_METADATA, {
    skip: true,
  });

  const [callDOItemMutation, doItemMutation] = useMutationOld(
    CREATE_OR_UPDATE_DO_ITEM,
    {
      onCompleted(data = {}) {
        if (data.createOrUpdateDataObjectItem?.id) {
          doItemIdRef.current = data.createOrUpdateDataObjectItem.id;
        }
      },
      onError() {},
    },
  );

  const [callDORelationshipMutation, relationshipMutation] = useMutationOld(
    CONNECT_OR_DISCONNECT_RELATIONSHIP,
    {
      onError() {},
    },
  );

  const [callDONoteMutation, doNoteMutation] = useMutationOld(
    CREATE_OR_UPDATE_DO_NOTE,
    {
      onError() {},
    },
  );

  const [callConnectDORelationship, connectDORelationship] = useMutation(
    CONNECT_DO_TO_DO_RELATIONSHIP,
    {
      update(cache, {data: {connectDOToDORelationship}}) {
        connectDOToDORelationship?.forEach((item) => {
          writeItems({
            isCreate: true,
            item,
            typeName: 'DOToDORelationship',
            fragment: DO_TO_DO_RELATIONSHIP_FRAGMENT,
            listKey: 'listDOToDORelationships',
            position: 'last',
          });
        });
      },
      alertOnComplete: false,
      alertOnError: false,
    },
  );
  const [callDeleteDORelationship, deleteDORelationship] = useMutation(
    DELETE_DO_TO_DO_RELATIONSHIP,
    {
      update(cache, {data: {deleteDOToDORelationship}}) {
        deleteItem({
          keyField: 'id',
          value: deleteDOToDORelationship.id,
          typeName: 'DOToDORelationship',
        });
      },
      alertOnComplete: false,
      alertOnError: false,
    },
  );

  const {callUpload, callUploadURL, error: uploadError} = useUpload();

  const error =
    doItemMutation.error ||
    relationshipMutation.error ||
    doNoteMutation.error ||
    connectDORelationship.error ||
    deleteDORelationship.error ||
    uploadError;

  const doItemQuery = useQuery(GET_DATA_OBJECT, {
    skip: true,
  });

  const save = async (values, options = {}) => {
    valuesParamsRef.current = values;

    if (!options.disableLoading) {
      setSaving(true);
    }

    const defaultItemId = options.itemId || doItemIdRef.current;
    let itemId = defaultItemId;
    let groupIdFinal = options.groupId || groupId;

    const saveForm = async () => {
      if (defaultItemId && !isEmpty(values.relationships)) {
        await Promise.all(
          values.relationships.map(async (data) => {
            await saveRelationship(itemId, groupIdFinal, data);
          }),
        );
      }
      if (
        values.primaryRelationship?.connectedContactId &&
        (!isEmpty(values.details) || options.details)
      ) {
        itemId = await saveDetails(
          itemId,
          groupIdFinal,
          options.details || values.details,
          values.primaryRelationship,
        );
        // Code Specific to tk217 Memberships Balance Payment URL to reload saved changes
        if (groupIdFinal === 'ci_jt739fbp' && contactEmail) {
          await refetchContactMetadata({
            email: contactEmail,
          });
        }
      }
      if (!defaultItemId && !isEmpty(values.relationships)) {
        await Promise.all(
          values.relationships.map(async (data) => {
            await saveRelationship(itemId, groupIdFinal, data);
          }),
        );
      }
      if (!isEmpty(values.notes)) {
        await Promise.all(
          values.notes.map(async (data) => {
            await saveNote(itemId, data);
          }),
        );
      }
      if (!isEmpty(values.attachments)) {
        const groupedAttachments = sortArrayByObjectGroups(values.attachments, [
          'fileExt',
          'file.size',
        ]);
        const sortedGroupedAttachments = Object.values(
          groupedAttachments,
        ).reduce((acc, file) => {
          const accArr = [...acc];

          return accArr.concat(async () => {
            await Promise.all(
              [file].map(async (data) => {
                await saveAttachment(itemId, data);
              }),
            );
          });
        }, []);

        await syncPromises(sortedGroupedAttachments);
      }
      if (!isEmpty(values.directDORelationships?.data)) {
        await saveDORelationships({
          doItemId: itemId,
          groupId: groupIdFinal,
          data: values.directDORelationships.data,
        });
      }
      if (!isEmpty(values.deletedDirectDORelationships?.data)) {
        const {data} = values.deletedDirectDORelationships;

        await deleteDORelationships({data});
      }

      if (queryOptions?.onCompleted) {
        queryOptions.onCompleted({
          values: valuesParamsRef.current,
        });
      }
    };

    await saveForm().finally(async () => {
      try {
        await doItemQuery.refetch({
          itemId,
        });
      } finally {
        displayMessage('Saved successfully!');

        setSaving(false);
      }
    });
  };

  const saveDetails = async (doItemId, groupIdFinal, fieldsData, relData) => {
    const session = Storage.getItem('session');

    const {data = {}} = await callDOItemMutation({
      variables: {
        createOrUpdateDataObjectItemInput: {
          sessionName: session?.sessionId,
          groupId: groupIdFinal,
          doItemId,
          contactId: relData.connectedContactId,
          relationship: relData.relationships,
          fields: Object.entries(fieldsData).map(([key, value]) => ({
            key,
            value,
          })),
        },
        __mutationkey: 'createOrUpdateDataObjectItemInput',
      },
    });

    return data.createOrUpdateDataObjectItem?.id;
  };

  const saveRelationship = (doItemId, groupIdFinal, data) => {
    const session = Storage.getItem('session');

    return callDORelationshipMutation({
      variables: {
        connectOrDisconnectRelationshipInput: {
          sessionName: session?.sessionId,
          doItemId,
          contactId: data.connectedContactId,
          groupId: groupIdFinal,
          role: data.relationships.join(','),
        },
        __mutationkey: 'connectOrDisconnectRelationshipInput',
      },
    });
  };

  const saveNote = (doItemId, data) => {
    const session = Storage.getItem('session');

    return callDONoteMutation({
      variables: {
        createOrUpdateDONoteInput: {
          ...data,
          doItemId,
          sessionName: session?.sessionId,
        },
        __mutationkey: 'createOrUpdateDONoteInput',
      },
    });
  };

  const saveAttachment = (doItemId, data) => {
    if (data.deleted) {
      const [appName, apiKey] = getAppInfo();

      const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/data_object/delete_attachment/${data.id}?api_key=${apiKey}`;

      return fetch(url);
    } else if (data.isUrl) {
      return callUploadURL(data, doItemId);
    } else {
      return callUpload(data.file, doItemId);
    }
  };

  const saveDORelationships = ({doItemId, groupId, data}) => {
    return callConnectDORelationship({
      variables: {
        doItemId,
        groupId,
        data: data.map((item) => {
          return {
            doItemId: item.doItemId,
            groupId: item.groupId,
          };
        }),
      },
    });
  };

  const deleteDORelationships = ({data}) => {
    return syncPromises(data, async (item) => {
      return callDeleteDORelationship({
        variables: item,
      });
    });
  };

  return {
    doItemId: doItemIdRef.current,
    saving,
    save,
    error,
  };
};

export default useDOItemMutation;
