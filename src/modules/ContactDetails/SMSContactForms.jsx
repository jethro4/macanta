import React from 'react';
import Typography from '@mui/material/Typography';
import Form from '@macanta/components/Form';
import SendIcon from '@mui/icons-material/Send';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import smsValidationSchema from '@macanta/validations/sms';
import {useMutation} from '@apollo/client';
import {SMS} from '@macanta/graphql/admin';
import * as NoteTaskFormsStyled from '@macanta/modules/NoteTaskForms/styles';
import * as Storage from '@macanta/utils/storage';

const SMSContactForms = ({
  defaultPhoneNumber,
  phoneNumbers,
  onSuccess,
  contactId,
  ...props
}) => {
  const {userId: loggedInUserId} = Storage.getItem('userDetails');

  const [callSMSMutation, smsMutation] = useMutation(SMS, {
    onCompleted(data) {
      if (data.sms?.success) {
        onSuccess();
      }
    },
    onError() {},
  });

  const handleSendSMS = (values) => {
    callSMSMutation({
      variables: {
        smsInput: {
          userId: loggedInUserId,
          sendMethod: 'single',
          contactId,
          toPhone: values.phoneNumber,
          txtMessage: values.message,
        },
        __mutationkey: 'smsInput',
      },
    });
  };

  const initValues = {
    phoneNumber: defaultPhoneNumber,
    message: '',
  };

  return (
    <NoteTaskFormsStyled.Root {...props}>
      <NoteTaskFormsStyled.Container>
        <Form
          initialValues={initValues}
          validationSchema={smsValidationSchema}
          onSubmit={handleSendSMS}>
          {({values, errors, handleChange, handleSubmit}) => (
            <>
              <NoteTaskFormsStyled.FormGroup>
                {phoneNumbers.length > 0 && (
                  <NoteTaskFormsStyled.TextField
                    labelPosition="normal"
                    select
                    options={phoneNumbers}
                    error={errors.phoneNumber}
                    onChange={handleChange('phoneNumber')}
                    label="To"
                    variant="outlined"
                    value={values.phoneNumber}
                    fullWidth
                    size="medium"
                  />
                )}

                <NoteTaskFormsStyled.TextField
                  labelPosition="normal"
                  type="TextArea"
                  hideExpand
                  error={errors.message}
                  onChange={handleChange('message')}
                  label="Message"
                  variant="outlined"
                  value={values.message}
                  fullWidth
                  size="medium"
                />
              </NoteTaskFormsStyled.FormGroup>

              {!!smsMutation.error && (
                <Typography
                  color="error"
                  style={{
                    fontSize: '0.75rem',
                    textAlign: 'center',
                    marginTop: '1rem',
                  }}>
                  {smsMutation.error.message}
                </Typography>
              )}

              <NoteTaskFormsStyled.Footer>
                <NoteTaskFormsStyled.FooterButton
                  disabled={!values.message}
                  variant="contained"
                  startIcon={<SendIcon />}
                  onClick={handleSubmit}
                  size="medium">
                  Send
                </NoteTaskFormsStyled.FooterButton>
              </NoteTaskFormsStyled.Footer>
            </>
          )}
        </Form>
      </NoteTaskFormsStyled.Container>
      <LoadingIndicator modal loading={smsMutation.loading} />
    </NoteTaskFormsStyled.Root>
  );
};

export default SMSContactForms;
