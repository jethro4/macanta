import React, {useState, useMemo} from 'react';
import isEqual from 'lodash/isEqual';
import FilterAltIcon from '@mui/icons-material/FilterAlt';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Button from '@macanta/components/Button';
import Form from '@macanta/components/Form';
import CriteriaForms from '@macanta/modules/AdminSettings/QueryBuilder/CriteriaForms';
import useAllFields from '@macanta/hooks/dataObject/useAllFields';
import {DEFAULT_OPERATOR} from '@macanta/constants/operators';
import * as TableStyled from '@macanta/components/Table/styles';

const getNewItem = (type, logic) => {
  return {
    id: Date.now(),
    name: '',
    logic,
    operator: DEFAULT_OPERATOR,
    values: null,
    value: '',
  };
};

const DEFAULT_FILTERS = {
  name: '',
  logic: 'AND',
  operator: DEFAULT_OPERATOR,
  values: null,
  value: '',
};

const DOFiltersForm = ({filters: filtersProp, groupId, onSubmit}) => {
  const defaultFiltersProp = filtersProp.filter((f) => !!f.default);
  const filteredFiltersProp = filtersProp.filter((f) => !f.default);

  const [filters, setFilters] = useState(
    filteredFiltersProp?.length
      ? filteredFiltersProp
      : [
          {
            id: Date.now(),
            ...DEFAULT_FILTERS,
          },
        ],
  );

  const {initLoading, data: options} = useAllFields({groupId});

  const handleChange = (value) => {
    setFilters(value['filters']);
  };

  const handleClearAll = () => {
    setFilters([...defaultFiltersProp]);

    onSubmit([...defaultFiltersProp]);
  };

  const handleSubmit = () => {
    onSubmit([...defaultFiltersProp, ...filters]);
  };

  const initValues = useMemo(() => {
    return {
      filters,
    };
  }, []);

  const disableSubmit =
    !filters.length ||
    filters.some((f) => {
      const fObj = {...f};

      delete fObj.id;

      return isEqual(fObj, DEFAULT_FILTERS);
    });

  return (
    <>
      <Box
        style={{
          minHeight: 100,
          paddingBottom: '2rem',
          position: 'relative',
        }}>
        {!filters.length ? (
          <TableStyled.EmptyMessageContainer>
            <Typography align="center" color="#888">
              No filters found
            </Typography>
          </TableStyled.EmptyMessageContainer>
        ) : (
          <Form initialValues={initValues}>
            <DOCriteriaForms
              loading={initLoading}
              filters={filters}
              options={options}
              onChange={handleChange}
            />
          </Form>
        )}
      </Box>

      <Box
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          padding: '12px 1rem',
          borderTop: '1px solid #eee',
        }}>
        <Box>
          {!!filteredFiltersProp.length && (
            <Button variant="outlined" onClick={handleClearAll} size="medium">
              Clear Filters
            </Button>
          )}
        </Box>

        <Button
          disabled={disableSubmit}
          variant="contained"
          startIcon={<FilterAltIcon />}
          onClick={handleSubmit}
          size="medium">
          Apply Filters
        </Button>
      </Box>
    </>
  );
};

const DOCriteriaForms = ({loading, filters, options, onChange}) => {
  return (
    <CriteriaForms
      loading={loading}
      headerStyle={{
        display: 'none',
        boxShadow: 'none',
      }}
      style={{
        marginTop: 0,
        borderRadius: 0,
        margin: 0,
        boxShadow: 'none',
      }}
      bodyStyle={{
        padding: '1rem',
      }}
      data={filters}
      required
      type="filters"
      choices={options}
      getNewItem={getNewItem}
      onChange={onChange}
    />
  );
};

DOFiltersForm.defaultProps = {
  filters: [],
};

export default DOFiltersForm;
