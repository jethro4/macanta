import {gql} from '@apollo/client';

export default gql`
  query getContactMetadata(
    $appName: String!
    $apiKey: String!
    $email: String!
  ) {
    getContactMetadata(appName: $appName, apiKey: $apiKey, email: $email) {
      items {
        key
        value
      }
    }
  }
`;
