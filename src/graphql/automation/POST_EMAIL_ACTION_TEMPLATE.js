import {gql} from '@apollo/client';

export default gql`
  mutation postEmailActionTemplate($input: EmailActionTemplateInput!) {
    postEmailActionTemplate(input: $input) {
      action
      data {
        CompiledEmailHTML
        EmailCSS
        EmailHTML
        EmailId
        TemplateCSS
        TemplateHTML
        Title
      }
      id
      name
      status
    }
  }
`;
