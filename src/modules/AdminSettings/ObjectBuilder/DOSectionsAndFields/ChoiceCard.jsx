import React from 'react';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import Box from '@mui/material/Box';
import {OverflowTip} from '@macanta/components/Tooltip';
import {IconButton} from '@macanta/components/Button';

const SelectedCard = ({item, hideDelete, onDelete}) => {
  const handleDelete = () => {
    onDelete(item.value);
  };

  return (
    <Box
      style={{
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: 'white',
        padding: '4px 12px',
        height: '100%',
        borderBottom: '1px solid #e4e4e4',
      }}>
      <OverflowTip
        style={{
          fontSize: 14,
        }}>
        {item.label}
      </OverflowTip>

      {!hideDelete && (
        <Box
          style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            marginLeft: '1rem',
          }}>
          <IconButton
            style={{
              padding: 0,
            }}
            size="small"
            TooltipProps={{title: 'Delete'}}
            onClick={handleDelete}>
            <DeleteForeverIcon
              style={{
                fontSize: 18,
              }}
              sx={{
                color: 'grayDarker.main',
              }}
            />
          </IconButton>
        </Box>
      )}
    </Box>
  );
};

export default SelectedCard;
