import React, {useState, useEffect, useRef, useMemo} from 'react';
import clsx from 'clsx';
import isEmpty from 'lodash/isEmpty';
import isFunction from 'lodash/isFunction';
import SearchIcon from '@mui/icons-material/Search';
import CloseIcon from '@mui/icons-material/Close';
import InputAdornment from '@mui/material/InputAdornment';
import {makeStyles} from '@mui/material/styles';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import ListSubheader from '@mui/material/ListSubheader';
import MenuItem from '@mui/material/MenuItem';
import {IconButton} from '@macanta/components/Button';
import {OverflowTip} from '@macanta/components/Tooltip';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import VirtualizedList from '@macanta/containers/List/VirtualizedList';
import useValue from '@macanta/hooks/base/useValue';
import useTransformChoices from '@macanta/hooks/base/useTransformChoices';
import {sortArrayByPriority, toggleItem} from '@macanta/utils/array';
import TextField from '@macanta/components/Forms/InputFields/TextField';
import {transformValue} from '@macanta/components/Forms/InputFields/helpers';
import * as Styled from './styles';
import PopoverFooter, {
  FOOTER_HEIGHT,
} from '@macanta/components/Forms/ChoiceFields/PopoverFooter';

const SEARCH_HEIGHT = 38;
const ROWS_IN_VIEW_LIMIT = 10;
const ALLOW_SEARCH_THRESHOLD = 8;

export const getItemHeight = ({withDivider, withSubLabel}) => {
  let itemHeight = 32;

  if (withDivider) {
    itemHeight = 38;
  }
  if (withSubLabel) {
    itemHeight = 52;
  }

  return itemHeight;
};

export const getListHeight = ({itemHeight, showDoneBtn}) => {
  const footerHeight = showDoneBtn ? FOOTER_HEIGHT * 1.1 : 0;

  return SEARCH_HEIGHT + footerHeight + ROWS_IN_VIEW_LIMIT * itemHeight;
};

const getVirtualListHeight = (itemHeight, itemCount) => {
  const finalRowLimit =
    itemCount <= ROWS_IN_VIEW_LIMIT ? itemCount : ROWS_IN_VIEW_LIMIT;

  return finalRowLimit * itemHeight;
};

export const getIsSubHeader = ({isSubheader, nextIsSubheader, isLastItem}) => {
  return isSubheader && !(nextIsSubheader || isLastItem);
};

export const useStyles = makeStyles(() => ({
  menuPaperDefault: {
    display: 'flex',
    flexDirection: 'column',
    overflowY: 'hidden',
  },
  menuPaper: {
    marginTop: 6,
    maxHeight: (props) => `${props.listHeight}px !important`,
  },
  menuPaperFixedWidth: {
    width: 300,
    overflowY: 'auto',
  },
  menuPaperWithChildren: {
    '& > .MuiList-root': {
      overflowY: 'unset !important',

      '& > .MuiListItem-root:not(.li-select-search)': {
        height: 32,
      },
    },
  },
  menuWithPadding: {
    '& > .MuiList-root': {
      paddingTop: '6px !important',
      paddingBottom: '6px !important',
    },
  },
  menuListDefault: {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    overflowY: 'hidden',
    paddingTop: '0 !important',
    paddingBottom: '0 !important',
  },
}));

const getKeyExtractor = (index, itemData) => {
  const item = itemData[index];

  return item.value;
};

const SelectField = ({
  loading,
  children,
  renderValue,
  placeholder,
  MenuProps,
  options: optionsProp,
  getOptionLabel,
  labelKey,
  valueKey,
  subLabelKey,
  captionKey,
  value: valueProp,
  onSearch,
  onChange,
  allowSearch: allowSearchProp,
  searchThreshold = ALLOW_SEARCH_THRESHOLD,
  emptyMessage,
  searchPlaceholder,
  searchValue,
  blankOption,
  showDoneBtn,
  onClear,
  ...props
}) => {
  const childrenArray = React.Children.toArray(children);
  const hasChildren = childrenArray.length > 0;

  const [open, setOpen] = useState(false);

  const hasClickedChildrenRef = useRef(false);

  const [value, setValue] = useValue(() => transformValue(valueProp, false), [
    valueProp,
  ]);

  const options = useTransformChoices({
    options: optionsProp,
    getOptionLabel,
    blankOption,
    labelKey,
    valueKey,
    subLabelKey,
    captionKey,
  });

  const allowSearch =
    onSearch ||
    (allowSearchProp &&
      options.filter((option) => !option.isSubheader).length > searchThreshold);

  const getCallbackArgs = (val) => [
    val,
    options.find((o) => o.value === val),
    options,
  ];

  const handleChange = (newVal) => {
    setValue(newVal);

    onChange?.(...getCallbackArgs(newVal));

    handleClose();
  };

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleClick = () => {
    if (!hasClickedChildrenRef.current) {
      setOpen(!open);
    } else {
      hasClickedChildrenRef.current = false;

      setOpen(true);
    }
  };

  const handleClear = () => {
    onClear?.(...getCallbackArgs(value));
  };

  const handleClearValue = () => {
    handleClear();

    setValue('');
  };

  const handleRenderValue = (valToRender) => {
    let displayedText = blankOption
      ? options[0].label
      : placeholder || 'Select...';

    const derivedValue = renderValue?.(valToRender) || valToRender;

    if (!isEmpty(derivedValue)) {
      if (!renderValue) {
        const selected = options.find((item) => valToRender === item.value);

        return <OverflowTip>{selected?.label}</OverflowTip>;
      } else {
        return <OverflowTip>{derivedValue}</OverflowTip>;
      }
    } else {
      return blankOption ? (
        displayedText
      ) : (
        <Typography className="select-placeholder">{displayedText}</Typography>
      );
    }
  };

  const withSubLabel = !!subLabelKey;
  const itemHeight = getItemHeight({withSubLabel});
  const listHeight = getListHeight({itemHeight, showDoneBtn});

  const classes = useStyles({listHeight});

  const menuItemsProps = {
    popover: true,
    selectedValue: value,
    options,
    withSubLabel,
    allowSearch,
    onSearch,
    onChange: handleChange,
    loading,
    emptyMessage,
    searchPlaceholder,
    searchValue,
    showDoneBtn,
    onClose: handleClose,
    onClear: handleClearValue,
  };

  let popoverComp;

  if (hasChildren) {
    popoverComp = isFunction(children) ? children(menuItemsProps) : children;

    if (showDoneBtn) {
      const handleChildrenMouseEvent = () => {
        hasClickedChildrenRef.current = true;
      };

      popoverComp = (
        <Box
          onMouseUp={handleChildrenMouseEvent}
          onMouseDown={handleChildrenMouseEvent}>
          {popoverComp}

          <PopoverFooter
            onDone={handleClose}
            onClear={onClear && handleClear}
          />
        </Box>
      );
    }
  } else {
    popoverComp = <SelectMenuItems {...menuItemsProps} />;
  }

  return (
    <SelectTextField
      {...props}
      loading={loading}
      value={value}
      open={open}
      onClick={handleClick}
      onChange={handleChange}
      NewSelectProps={{
        onOpen: handleOpen,
        onClose: handleClose,
        renderValue: handleRenderValue,
        MenuProps,
        classes: {
          menu: {
            paper: {
              [classes.menuPaper]: true,
              [classes.menuPaperWithChildren]: hasChildren,
              [classes.menuWithPadding]: !showDoneBtn && hasChildren,
            },
          },
        },
        allowSearch,
      }}>
      {popoverComp}
    </SelectTextField>
  );
};

SelectField.defaultProps = {
  value: '',
  allowSearch: true,
  showDoneBtn: false,
};

export default SelectField;

export const SelectTextField = ({
  multiple = false,
  value,
  open,
  onChange,
  NewSelectProps,
  loading,
  ...props
}) => {
  const classes = useStyles();

  return (
    <TextField
      SelectProps={Object.assign(
        {
          open,
          onOpen: NewSelectProps.onOpen,
          onClose: NewSelectProps.onClose,
          multiple,
          displayEmpty: !props.label,
          renderValue: NewSelectProps.renderValue,
          MenuProps: {
            anchorOrigin: {
              vertical: 'bottom',
              horizontal: 'left',
            },
            transformOrigin: {
              vertical: 'top',
              horizontal: 'left',
            },
            getContentAnchorEl: null,
            ...NewSelectProps.MenuProps,
            classes: {
              ...NewSelectProps.MenuProps?.classes,
              paper: clsx({
                [classes.menuPaperDefault]: true,
                [classes.menuPaperFixedWidth]: NewSelectProps.allowSearch,
                ...NewSelectProps.classes?.menu?.paper,
                ...NewSelectProps.MenuProps?.classes?.paper,
              }),
              list: clsx({
                [classes.menuListDefault]: true,
                ...NewSelectProps.classes?.menu?.list,
                ...NewSelectProps.MenuProps?.classes?.list,
              }),
            },
          },
        },
        loading &&
          !open && {
            IconComponent: () => {
              // const argsMap = args.reduce((acc, item) => {
              //   return {...acc, ...item};
              // }, {});

              return (
                <LoadingIndicator
                  // {...argsMap}
                  sx={{
                    position: 'absolute',
                    right: '1rem',
                  }}
                  transparent
                  size={18}
                />
              );
            },
          },
      )}
      {...props}
      value={value}
      onChange={onChange}
      select
    />
  );
};

export const SelectSubHeader = ({style, label}) => {
  const handlePopoverMouseEvent = (event) => {
    event.stopPropagation();
  };

  return (
    <ListSubheader
      key={`subheader-${label}`}
      disableSticky
      value={`subheader-${label}`}
      style={{
        padding: 0,
        backgroundColor: '#F2F2F2',
        boxShadow: '#eee 0px 2px 8px -2px',
        borderBottom: '1px solid #eee',
        ...style,
      }}>
      <Box
        style={{
          padding: '0 1rem',
          width: '100%',
        }}
        onClick={handlePopoverMouseEvent}
        onMouseUp={handlePopoverMouseEvent}
        onMouseDown={handlePopoverMouseEvent}>
        {label}
      </Box>
    </ListSubheader>
  );
};

export const SelectMenuItem = ({
  disabled,
  multiple,
  isSubheader,
  selected,
  label,
  subLabel,
  caption,
  LeftComp,
  itemProps,
  ...props
}) => {
  const handlePopoverMouseEvent = (event) => {
    event.stopPropagation();
  };

  if (isSubheader) {
    return <SelectSubHeader style={props.style} label={label} />;
  }

  return (
    <MenuItem
      {...props}
      {...(disabled && {
        onClick: handlePopoverMouseEvent,
        sx: {
          '& .MuiTypography-root': {
            color: '#aaa',
          },
        },
      })}
      onMouseUp={handlePopoverMouseEvent}
      onMouseDown={handlePopoverMouseEvent}
      {...itemProps}>
      {LeftComp || (multiple && <Styled.Checkbox checked={selected} />)}
      {selected && (
        <Box
          style={{
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
          }}
          sx={{
            backgroundColor: 'secondary.main',
            opacity: 0.15,
          }}
        />
      )}
      <Box
        style={{
          flexGrow: 1,
          flexBasis: 0,
          overflowX: 'hidden',
        }}>
        <OverflowTip>{label}</OverflowTip>
        {!!subLabel && (
          <OverflowTip
            sx={{
              color: 'text.secondary',
            }}
            style={{
              fontSize: '0.8125rem',
              lineHeight: '0.9375rem',
            }}>
            {subLabel}
          </OverflowTip>
        )}
      </Box>
      {/* <ListItemText primary={label} secondary={'Test'} /> */}
      {caption && (
        <Typography
          style={{
            fontSize: '0.75rem',
            color: '#888',
            position: 'absolute',
            right: '1.5rem',
          }}>
          {caption}
        </Typography>
      )}
    </MenuItem>
  );
};

export const SelectSearch = ({
  searchValue,
  onChange,
  onClear,
  disableSearchAutoFocus,
  placeholder,
  ...props
}) => {
  const handleKeyDown = (e) => {
    e.stopPropagation(); // Prevent option search when typing into the InputBase
  };

  const handlePopoverMouseEvent = (event) => {
    event.stopPropagation();
  };

  return (
    <ListSubheader
      className="li-select-search"
      value=""
      sx={{
        backgroundColor: 'grayLight.main',
      }}
      style={{
        lineHeight: 'unset',
        padding: 0,
        boxShadow: '#ddd 0px 2px 8px -2px',
        borderBottom: '1px solid #eee',
        zIndex: 2,
      }}
      {...props}>
      <Box
        style={{
          width: '100%',
          cursor: 'default',
        }}
        onClick={handlePopoverMouseEvent}
        onMouseUp={handlePopoverMouseEvent}
        onMouseDown={handlePopoverMouseEvent}>
        <TextField
          timeout={500}
          value={searchValue}
          sx={{
            '& .MuiInputBase-input::placeholder': {
              color: '#555 !important',
            },
          }}
          variant="standard"
          inputProps={{
            style: {
              height: SEARCH_HEIGHT,
              fontSize: 15,
              lineHeight: '15px',
              color: '#888',
              padding: '4px 1rem 4px 0',
            },
          }}
          InputProps={{
            startAdornment: (
              <InputAdornment
                position="start"
                style={{
                  marginLeft: 12,
                  pointerEvents: 'none',
                }}>
                <SearchIcon
                  sx={{
                    color: '#ccc',
                    fontSize: 20,
                  }}
                />
              </InputAdornment>
            ),
            endAdornment: (
              <InputAdornment
                position="end"
                style={{
                  marginRight: '0.5rem',
                }}>
                {!!searchValue && (
                  <IconButton
                    style={{
                      height: 36,
                      width: 36,
                    }}
                    disableFocusRipple
                    disableRipple
                    onClick={onClear}>
                    <CloseIcon
                      style={{
                        color: '#aaa',
                      }}
                    />
                  </IconButton>
                )}
              </InputAdornment>
            ),
          }}
          autoFocus={!disableSearchAutoFocus}
          noBorder
          onChange={onChange}
          fullWidth
          size="small"
          onKeyDown={handleKeyDown}
          placeholder={placeholder}
        />
      </Box>
    </ListSubheader>
  );
};

SelectSearch.defaultProps = {
  placeholder: 'Search...',
};

export const SelectEmptyMessage = ({
  emptyMessage = 'No items found',
  emptyMessageStyle,
}) =>
  !emptyMessage ? null : (
    <Typography
      align="center"
      color="#aaa"
      style={{
        color: '#bbb',
        fontSize: '0.9375rem',
        padding: '1rem 0',
        ...emptyMessageStyle,
      }}>
      {emptyMessage}
    </Typography>
  );

export const SelectVirtualizedList = ({
  outerRef,
  options,
  itemHeight,
  listHeight,
  renderVirtualizedOptionRow,
}) => (
  <VirtualizedList
    outerRef={outerRef}
    height={listHeight}
    width="100%"
    data={options}
    keyExtractor={getKeyExtractor}
    renderRow={renderVirtualizedOptionRow}
    itemSize={itemHeight}
    overscanCount={ROWS_IN_VIEW_LIMIT}
    style={{
      paddingBottom: '0.5rem',
    }}
  />
);

export const SelectMenuItems = ({
  loading,
  multiple,
  popover,
  selectedValue: value,
  options,
  withSubLabel,
  withDivider,
  allowSearch,
  onSearch,
  onChange,
  emptyMessage,
  emptyMessageStyle,
  disableSearchAutoFocus,
  disableAutoSort,
  sx,
  searchPlaceholder,
  searchValue: searchValueProp,
  showDoneBtn,
  onClose,
  onClear,
  ...props
}) => {
  const [searchValue, setSearchValue] = useState(searchValueProp);
  const [finalOptions, setFinalOptions] = useState(options);

  const itemsContainerRef = useRef(null);
  const boxContainerRef = useRef(null);

  const handleSearchValueChange = (searchVal) => {
    setSearchValue(searchVal);

    onSearch?.(searchVal);
  };

  const handleClearSearch = () => {
    handleSearchValueChange('');
  };

  const handleClick = (newVal) => (event) => {
    event.stopPropagation();

    let updatedValue = newVal;

    if (multiple) {
      const toggledVal = toggleItem({
        arr: value,
        item: newVal,
      });

      updatedValue =
        disableAutoSort || !options.length
          ? toggledVal
          : sortArrayByPriority(
              toggledVal,
              null,
              options.map((item) => item.value),
            );
    }

    onChange(updatedValue);
  };

  const renderVirtualizedOptionRow = ({data: itemData, index, style}) => {
    const option = itemData[index];

    const selected = multiple
      ? value.includes(option.value)
      : option.value === value;
    const showSubheader = getIsSubHeader({
      isSubheader: option.isSubheader,
      nextIsSubheader: finalOptions[index + 1]?.isSubheader,
      isLastItem: index === finalOptions.length - 1,
    });

    return (
      (!option.isSubheader || showSubheader) && (
        <SelectMenuItem
          style={style}
          key={option.value}
          disabled={option.disabled}
          multiple={multiple}
          isSubheader={option.isSubheader}
          selected={selected}
          value={option.value}
          label={option.label}
          subLabel={option.subLabel}
          caption={option.caption}
          onClick={handleClick(option.value)}
          {...(option.IconComp && {
            LeftComp: option.IconComp,
          })}
          itemProps={option.itemProps}
        />
      )
    );
  };

  const virtualized =
    (!popover ? options : finalOptions).length > ROWS_IN_VIEW_LIMIT * 2.5;
  const itemHeight = getItemHeight({withDivider, withSubLabel});
  const virtualListHeight = getVirtualListHeight(
    itemHeight,
    finalOptions.length,
  );
  let containerPadding = 0;

  if (!allowSearch && popover) {
    containerPadding = '6px 0';
  }

  useEffect(() => {
    setFinalOptions(
      onSearch
        ? options
        : options.filter(
            (option) =>
              !searchValue ||
              !!option.isSubheader ||
              `${option.label || ''}${option.subLabel || ''}`
                ?.toLowerCase()
                .includes(searchValue.toLowerCase()),
          ),
    );
  }, [searchValue, options]);

  useEffect(() => {
    if (value && itemsContainerRef.current && finalOptions?.length) {
      const selectedIndex = finalOptions.findIndex((o) =>
        multiple ? value.includes(o.value) : o.value === value,
      );

      itemsContainerRef.current?.scrollTo?.(
        0,
        selectedIndex * itemHeight - itemHeight * 3,
      );
    }
  }, [finalOptions]);

  const listHeight = useMemo(() => {
    if (virtualized && boxContainerRef.current) {
      const {height = virtualListHeight} =
        boxContainerRef.current?.getBoundingClientRect() || {};

      return virtualListHeight < height ? height : virtualListHeight;
    }

    return virtualListHeight || 0;
  }, [virtualized, virtualListHeight, boxContainerRef.current]);

  const finalOptionsLength = finalOptions.filter(
    (option) => !option.isSubheader,
  ).length;

  return (
    <>
      {allowSearch && (
        <SelectSearch
          searchValue={searchValue}
          onChange={handleSearchValueChange}
          onClear={handleClearSearch}
          disableSearchAutoFocus={disableSearchAutoFocus}
          placeholder={searchPlaceholder}
        />
      )}

      <Box
        ref={!virtualized ? itemsContainerRef : boxContainerRef}
        sx={{
          position: 'relative',
          height: `calc(100% - ${allowSearch ? `${SEARCH_HEIGHT}px` : '0px'})`,
          padding: containerPadding,
          '& .MuiListItem-root, & .MuiListSubheader-root': {
            height: `${itemHeight}px`,
            lineHeight: `${itemHeight}px`,
            ...((withDivider || withSubLabel) && {
              borderBottom: '1px solid #eee',
            }),
          },
          ...(virtualized
            ? {
                paddingBottom: 0,
              }
            : {
                overflowY: 'auto',
              }),
          ...sx,
        }}
        {...props}>
        {!loading && finalOptionsLength === 0 && (
          <SelectEmptyMessage
            emptyMessage={emptyMessage}
            emptyMessageStyle={emptyMessageStyle}
          />
        )}

        {finalOptionsLength > 0 && (
          <>
            {virtualized ? (
              <SelectVirtualizedList
                outerRef={itemsContainerRef}
                options={finalOptions}
                itemHeight={itemHeight}
                listHeight={listHeight}
                renderVirtualizedOptionRow={renderVirtualizedOptionRow}
              />
            ) : (
              finalOptions.map((option, index) => {
                const selected = multiple
                  ? value.includes(option.value)
                  : option.value === value;
                const showSubheader = getIsSubHeader({
                  isSubheader: option.isSubheader,
                  nextIsSubheader: finalOptions[index + 1]?.isSubheader,
                  isLastItem: index === finalOptions.length - 1,
                });

                return (
                  (!option.isSubheader || showSubheader) && (
                    <SelectMenuItem
                      key={option.value}
                      disabled={option.disabled}
                      multiple={multiple}
                      isSubheader={option.isSubheader}
                      selected={selected}
                      value={option.value}
                      label={option.label}
                      subLabel={option.subLabel}
                      caption={option.caption}
                      onClick={handleClick(option.value)}
                      {...(option.IconComp && {
                        LeftComp: option.IconComp,
                      })}
                      itemProps={option.itemProps}
                    />
                  )
                );
              })
            )}
          </>
        )}

        <LoadingIndicator
          loading={loading}
          {...(finalOptionsLength > 0
            ? {
                fill: true,
                align: 'top',
              }
            : {
                transparent: true,
                style: {
                  display: 'flex',
                  justifyContent: 'center',
                  padding: '1rem 0',
                },
              })}
        />
      </Box>

      {showDoneBtn && <PopoverFooter onDone={onClose} onClear={onClear} />}
    </>
  );
};

SelectMenuItems.defaultProps = {
  loading: false,
  searchValue: '',
};
