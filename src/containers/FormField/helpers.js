import RadioGroup from '@macanta/components/Forms/ChoiceFields/RadioGroup';
import CheckboxGroup from '@macanta/components/Forms/ChoiceFields/CheckboxGroup';
import FileUpload from '@macanta/components/FileUpload';
import Switch from '@macanta/components/Forms/ChoiceFields/Switch';
import withProps from '@macanta/hoc/withProps';
import TimePickerField from '@macanta/components/Forms/DateFields/TimePickerField';

let SwitchForm = withProps(Switch, {
  value: (value) => {
    return {
      checked: value,
    };
  },
  onChange: (callback) => (event) => callback?.(event.target.checked),
});

export const getFieldOptions = (type) => {
  let fieldOptions = {};

  switch (type) {
    case 'TextArea': {
      fieldOptions = {
        textArea: true,
        multiline: true,
      };
      break;
    }
    case 'RichText': {
      fieldOptions = {
        richText: true,
      };
      break;
    }
    case 'Select': {
      fieldOptions = {
        select: true,
      };
      break;
    }
    case 'MultiSelect': {
      fieldOptions = {
        select: true,
        multiple: true,
      };
      break;
    }
    case 'Date': {
      fieldOptions = {
        date: true,
        // subLabel: 'Date',
      };
      break;
    }
    case 'DateTime': {
      fieldOptions = {
        datetime: true,
        // subLabel: 'Date & Time',
      };
      break;
    }
    case 'DateRange': {
      fieldOptions = {
        daterange: true,
      };
      break;
    }
    case 'Time': {
      fieldOptions = {
        Component: TimePickerField,
      };
      break;
    }
    case 'Radio': {
      fieldOptions = {
        Component: RadioGroup,
      };
      break;
    }
    case 'Checkbox': {
      fieldOptions = {
        Component: CheckboxGroup,
      };
      break;
    }
    case 'Number': {
      fieldOptions = {
        number: true,
        // subLabel: 'Whole Number',
      };
      break;
    }
    case 'Currency': {
      fieldOptions = {
        number: true,
        format: 'decimal',
        // subLabel: 'Decimal',
      };
      break;
    }
    case 'Email': {
      fieldOptions = {
        autoComplete: 'email',
      };
      break;
    }
    case 'Password': {
      fieldOptions = {
        password: true,
        // Temporary workaround to turn off autoComplete
        // https://stackoverflow.com/questions/69233118/react-js-autocomplete-off-not-working-chrome
        autoComplete: 'new-password',
      };
      break;
    }
    case 'FileUpload': {
      fieldOptions = {
        Component: FileUpload,
      };
      break;
    }
    case 'Switch': {
      fieldOptions = {
        Component: SwitchForm,
      };
      break;
    }
  }

  return fieldOptions;
};
