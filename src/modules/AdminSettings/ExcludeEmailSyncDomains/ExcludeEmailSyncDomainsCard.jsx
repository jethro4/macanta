import React from 'react';
import GroupIcon from '@mui/icons-material/Group';
import SettingCard from '@macanta/modules/AdminSettings/SettingCard';
import {navigate} from 'gatsby';

const ExcludeEmailSyncDomainsCard = () => {
  /* const navigate = useNavigate();*/

  const handleExcludeEmailSyncDomains = () => {
    navigate('/app/admin-settings/exclude-email-sync-domains');
  };

  return (
    <SettingCard
      IconComp={GroupIcon}
      title="Email Sync Domain Management"
      description="Exclude domains from email sync"
      onClick={handleExcludeEmailSyncDomains}
    />
  );
};

export default ExcludeEmailSyncDomainsCard;
