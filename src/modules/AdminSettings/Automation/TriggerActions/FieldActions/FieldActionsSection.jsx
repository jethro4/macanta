import React from 'react';
import invert from 'lodash/invert';
import pick from 'lodash/pick';
import AddIcon from '@material-ui/icons/Add';
import DataTable from '@macanta/containers/DataTable';
import {
  useDeleteAutomationsMutation,
  useFieldActions,
  usePostFieldActionMutation,
} from '@macanta/hooks/automation';
import ItemActionButtons from '@macanta/modules/AdminSettings/Automation/TriggerActions/ItemActionButtons';
import {FieldActionsForm} from '@macanta/modules/AdminSettings/Automation/TriggerActions/FieldActions';
import Section from '@macanta/containers/Section';
import {writeItemInListFragment} from '@macanta/graphql/helpers';
import {FIELD_ACTIONS_FRAGMENT} from '@macanta/graphql/automation/GET_FIELD_ACTIONS';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import {FIELD_KEY_MAPPING} from '@macanta/hooks/automation/useFieldActions';
import {findItem} from '@macanta/utils/array';
import {FIELD_ACTION_TYPES} from '@macanta/constants/automation';
import {replaceKeys} from '@macanta/utils/object';
import ValueTransform from '@macanta/modules/AdminSettings/DOScheduler/ValueTransform';
import useDORelationships from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDORelationships';
import EditButton from '@macanta/containers/buttons/EditButton';

const FIELD_ACTION_TYPE_COLUMNS = [
  {
    id: 'name',
    label: 'Name',
  },
  {
    id: 'description',
    label: 'Description',
    maxWidth: '25%',
  },
  {
    id: 'doTitle',
    label: 'Data Object Type',
  },
  {
    id: 'contactRelationship',
    label: 'Contact Relationship',
    renderBodyCellValue: ({origValue, item}) => {
      return (
        <ValueTransform
          value={origValue}
          labelKey="role"
          valueKey="id"
          selector={useDORelationships}
          options={item.groupId}
        />
      );
    },
  },
  {
    id: 'actionType',
    label: 'Action Type',
  },
  {
    id: 'action',
    label: '',
    disableSort: true,
    disableResize: true,
    minWidth: 100,
    maxWidth: 100,
    renderBodyCellValue: ({item, handlers}) => {
      return (
        <ItemActionButtons
          modal={handlers.modal}
          type="Field Actions"
          item={item}
          onDelete={handlers.onDelete}
          renderFormComponent={(handleClose) => (
            <FieldActionsForm
              options={{
                variables: {
                  id: item.id,
                },
              }}
              initialValues={item}
              onSubmit={async (values) => {
                const {result} = await handlers.onSave(values);

                if (result) {
                  handleClose();
                }
              }}
            />
          )}
        />
      );
    },
  },
];

const FieldActionsSection = ({modal, ...props}) => {
  const fieldActionsQuery = useFieldActions();

  const [callPostMutation, actionMutation] = usePostFieldActionMutation({
    update(cache, {result}, options) {
      writeItemInListFragment({
        isCreate: !options.variables.input?.id,
        keyField: 'queryId',
        item: result,
        typeName: 'FieldActionItems',
        fragment: FIELD_ACTIONS_FRAGMENT,
        parentKey: 'getFieldActions',
        listKey: 'items',
        position: 'first',
      });
    },
  });

  const [callDeleteMutation, deleteMutation] = useDeleteAutomationsMutation({
    typeName: 'FieldActionItems',
  });

  const handleSave = async (values) => {
    const fieldKeys = findItem(FIELD_ACTION_TYPES, {
      value: values.actionType,
    })?.fieldKeys;
    const invertedFieldKeyMapping = invert(FIELD_KEY_MAPPING);
    let fieldValues = pick(values, fieldKeys);
    fieldValues = replaceKeys(fieldValues, invertedFieldKeyMapping);

    const variables = {
      id: values.id,
      name: values.name,
      description: values.description,
      doType: values.doTitle,
      actionType: values.actionType,
      fieldValues,
      contactRelationship: values.contactRelationship,
    };

    return callPostMutation({
      variables,
    });
  };

  const handleDelete = (id) => {
    return callDeleteMutation({
      variables: {
        id,
      },
    });
  };

  return (
    <>
      <Section
        style={{
          margin: 0,
        }}
        fullHeight
        title={`Field Actions`}
        HeaderRightComp={
          <EditButton
            modal={modal}
            size="small"
            variant="contained"
            startIcon={<AddIcon />}
            PopoverProps={{
              title: `Add Field Action`,
              contentWidth: 1100,
              contentMinHeight: 350,
              bodyStyle: {
                backgroundColor: '#f5f6fA',
              },
            }}
            renderContent={(handleClose) => (
              <FieldActionsForm
                onSubmit={async (values) => {
                  const {result} = await handleSave(values);

                  if (result) {
                    handleClose();
                  }
                }}
              />
            )}>
            Add Field Action
          </EditButton>
        }>
        <DataTable
          loading={fieldActionsQuery.loading}
          data={fieldActionsQuery.result}
          fullHeight
          rowsPerPage={25}
          rowsPerPageOptions={[25, 50, 100]}
          {...props}
          columns={FIELD_ACTION_TYPE_COLUMNS}
          handlers={{
            modal,
            onSave: handleSave,
            onDelete: handleDelete,
          }}
        />
      </Section>
      <LoadingIndicator
        modal
        loading={actionMutation.loading || deleteMutation.loading}
      />
    </>
  );
};

export default FieldActionsSection;
