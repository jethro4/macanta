import React from 'react';
import DataTable from '@macanta/containers/DataTable';

const DEFAULT_BLAST_PROGRESS_COLUMNS = [
  {id: 'subject', label: 'Subject', minWidth: 170},
  {
    id: 'senderEmail',
    label: 'Sender Email',
    minWidth: 170,
  },
  {
    id: 'groupName',
    label: 'Group Name',
    minWidth: 170,
  },
  {
    id: 'widgetName',
    label: 'Widget Name',
    minWidth: 170,
  },
  {
    id: 'queryId',
    label: 'Query Id',
    minWidth: 170,
    maxWidth: 370,
  },
  {
    id: 'type',
    label: 'Type',
    minWidth: 140,
  },
  {
    id: 'progress',
    label: 'Progress',
    minWidth: 140,
  },
  {
    id: 'created',
    label: 'Created',
    minWidth: 170,
  },
  {
    id: 'updated',
    label: 'Updated',
    minWidth: 140,
  },
  {
    id: 'status',
    label: 'Status',
    minWidth: 140,
  },
  {
    id: 'runtime',
    label: 'Runtime',
    minWidth: 140,
  },
];

const BlastProgressTable = ({data, ...props}) => {
  return (
    <DataTable
      fullHeight
      columns={DEFAULT_BLAST_PROGRESS_COLUMNS}
      data={data}
      order="desc"
      orderBy="created"
      rowsPerPage={25}
      rowsPerPageOptions={[25, 50, 100]}
      {...props}
    />
  );
};

BlastProgressTable.defaultProps = {
  data: [],
};

export default BlastProgressTable;
