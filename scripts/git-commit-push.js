const {exec} = require('child_process');
const {getLastCommit} = require('./checkLastCommit');

const commitPush = async () => {
  const lastCommit = await getLastCommit();
  const argsMsg = process.argv.slice(2).join(' ').trim();

  const message = argsMsg || lastCommit.subject;

  // console.info(
  //   'Executing command:',
  //   `git add . && git commit -m "${message}" && git push -u origin ${lastCommit.branch}`,
  // );

  exec(
    `git add . && git commit -m "${message}" && git push -u origin ${lastCommit.branch}`,
    (error, stdout, stderr) => {
      if (error) {
        console.error(`error: ${error.message}`);
        return;
      }
      if (stderr) {
        console.error(`stderr: ${stderr}`);
        return;
      }
      console.info(`stdout: ${stdout}`);
    },
  );
};

commitPush();
