import {gql} from '@apollo/client';
import {CONTACT_RELATIONSHIP_DIRECT_FRAGMENT} from './fragments';

export default gql`
  mutation createOrUpdateContactRelationship(
    $input: CreateOrUpdateContactRelationshipInput!
  ) {
    createOrUpdateContactRelationship(input: $input) {
      ...contactRelationshipDirectFragment
    }
  }
  ${CONTACT_RELATIONSHIP_DIRECT_FRAGMENT}
`;
