import React, {useState, useLayoutEffect} from 'react';
import isNumber from 'lodash/isNumber';
import Box from '@mui/material/Box';
import * as Styled from './styles';

const Pagination = ({count: countProp, RightComp, ...props}) => {
  const [count, setCount] = useState(countProp || 0);

  useLayoutEffect(() => {
    if (isNumber(countProp)) {
      setCount(countProp);
    }
  }, [countProp]);

  let paginationComp = (
    <Styled.Root color="secondary" shape="circular" count={count} {...props} />
  );

  if (RightComp) {
    paginationComp = (
      <Box
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          paddingRight: 24,
        }}>
        <Box>{paginationComp}</Box>
        <Box
          style={{
            display: 'flex',
            alignItems: 'center',
          }}>
          {RightComp}
        </Box>
      </Box>
    );
  }

  return paginationComp;
};

export default Pagination;
