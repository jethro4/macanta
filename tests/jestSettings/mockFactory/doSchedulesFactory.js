import moment from 'moment';
import {nextFactoryId} from './factory';

const typename = 'DOSchedule';

export default (overwrites) => {
  const {id, currentIndex} = nextFactoryId(typename);

  return {
    __typename: typename,
    id,
    groupId: 'DataObject1',
    name: `DO Schedule ${currentIndex}`,
    description: '',
    userRelationshipId: 'Relationship1',
    contactRelationshipId: 'Relationship2',
    startDateFieldId: '',
    endDateFieldId: '',
    schedulerType: '',
    dateCreated: moment().subtract(1, 'hour').add(currentIndex, 'minute'),
    ...overwrites,
  };
};
