import {useState} from 'react';

const useTargetRef = () => {
  const [anchorEl, setAnchorEl] = useState(null);

  const setRef = (target) => {
    setAnchorEl(target);
  };

  return {anchorEl, setRef};
};

export default useTargetRef;
