import React from 'react';
import doAttachmentValidationSchema from '@macanta/validations/doAttachment';
import Form from '@macanta/components/Form';
import TurnedInIcon from '@mui/icons-material/TurnedIn';
import {toDataURL} from '@macanta/utils/file';
import {getMimeTypeExtension} from '@macanta/modules/AdminSettings/FormBuilder/Forms/helpers';
import * as Styled from './styles';
import * as NoteTaskFormsStyled from '@macanta/modules/NoteTaskForms/styles';

const UploadUrlForms = ({onSubmit, ...props}) => {
  const handleSubmitForm = async (values) => {
    const isDataURI = values.url.startsWith('data:');
    const mimeType = isDataURI
      ? values.url.split(';')[0].replace('data:', '')
      : '';
    const fileExt = isDataURI
      ? getMimeTypeExtension(mimeType)
      : values.url.trim().split('.').pop();
    const base64Url = await toDataURL(values.url);

    const file = {
      id: String(Date.now()),
      isNew: true,
      thumbnail: base64Url,
      fileName: `${values.fileName}.${fileExt}`,
      fileExt,
      isPDF: fileExt === 'pdf',
      isUrl: true,
    };

    onSubmit(file, 'url');
  };

  return (
    <Styled.Root {...props}>
      <Form
        initialValues={{}}
        validationSchema={doAttachmentValidationSchema}
        onSubmit={handleSubmitForm}>
        {({values, errors, handleChange, handleSubmit}) => (
          <>
            <NoteTaskFormsStyled.TextField
              labelPosition="normal"
              error={errors.fileName}
              onChange={handleChange('fileName')}
              label="File Title"
              variant="outlined"
              value={values.fileName}
              fullWidth
              size="medium"
            />

            <NoteTaskFormsStyled.TextField
              labelPosition="normal"
              error={errors.url}
              onChange={handleChange('url')}
              label="Enter URL"
              variant="outlined"
              value={values.url}
              fullWidth
              size="medium"
            />

            <NoteTaskFormsStyled.Footer>
              <NoteTaskFormsStyled.FooterButton
                disabled={!values.fileName || !values.url}
                variant="contained"
                startIcon={<TurnedInIcon />}
                onClick={handleSubmit}
                size="medium">
                Attach
              </NoteTaskFormsStyled.FooterButton>
            </NoteTaskFormsStyled.Footer>
          </>
        )}
      </Form>
    </Styled.Root>
  );
};

export default UploadUrlForms;
