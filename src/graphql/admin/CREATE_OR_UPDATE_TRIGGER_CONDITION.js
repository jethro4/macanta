import {gql} from '@apollo/client';
import {TRIGGER_CONDITION_ITEM_ATTRIBUTES} from './fragments';

export default gql`
  mutation createOrUpdateTriggerCondition(
    $createOrUpdateTriggerConditionInput: CreateOrUpdateTriggerConditionInput!
  ) {
    createOrUpdateTriggerCondition(
      input: $createOrUpdateTriggerConditionInput
    ) {
      ...triggerConditionItem
    }
  }
  ${TRIGGER_CONDITION_ITEM_ATTRIBUTES}
`;
