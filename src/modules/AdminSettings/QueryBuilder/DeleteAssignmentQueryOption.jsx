import React, {useState} from 'react';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import ConfirmationDialog from '@macanta/containers/ConfirmationDialog';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import {DELETE_QUERY_BUILDER} from '@macanta/graphql/admin';
import {useMutation} from '@apollo/client';

const DeleteAssignmentQueryOption = ({
  showDialog = false,
  onClose,
  id,
  name,
}) => {
  const {displayMessage} = useProgressAlert();

  const [showDeleteDialog, setShowDeleteDialog] = useState(showDialog);

  const [
    callDeleteQueryBuilderMutation,
    deleteQueryBuilderMutation,
  ] = useMutation(DELETE_QUERY_BUILDER, {
    update(cache) {
      const normalizedId = cache.identify({id, __typename: 'QueryBuilder'});
      cache.evict({id: normalizedId});
      cache.gc();
    },
    onCompleted(data) {
      if (data.deleteQueryBuilder?.success) {
        displayMessage('Deleted query successfully');

        handleClose();
      }
    },
    onError() {},
  });

  const handleClose = () => {
    setShowDeleteDialog(false);

    onClose();
  };

  const handleDeleteAssignment = () => {
    callDeleteQueryBuilderMutation({
      variables: {
        deleteQueryBuilderInput: {
          queryId: id,
        },
        __mutationkey: 'deleteQueryBuilderInput',
      },
    });
  };

  return (
    <>
      <ConfirmationDialog
        open={!!showDeleteDialog}
        onClose={handleClose}
        title="Are you sure?"
        description={[
          `You are about to delete "${name}" as a query.`,
          'This cannot be undone. Do you want to proceed?',
        ]}
        actions={[
          {
            label: 'Delete',
            onClick: handleDeleteAssignment,
            startIcon: <DeleteForeverIcon />,
          },
        ]}
      />
      <LoadingIndicator modal loading={deleteQueryBuilderMutation.loading} />
    </>
  );
};

export default DeleteAssignmentQueryOption;
