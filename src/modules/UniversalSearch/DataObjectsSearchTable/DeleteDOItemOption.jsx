import React, {useState, useEffect} from 'react';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import ConfirmationDialog from '@macanta/containers/ConfirmationDialog';
import useMutation from '@macanta/hooks/apollo/useMutation';
import {DELETE_DO_ITEM} from '@macanta/graphql/dataObjects';
import LoadingIndicator from '@macanta/components/LoadingIndicator';

const DeleteDOItemOption = ({itemId, showDialog = false, onClose}) => {
  const [confirmDelete, setConfirmDelete] = useState(showDialog);

  const [callDeleteDOItemMutation, deleteDOItemMutation] = useMutation(
    DELETE_DO_ITEM,
    {
      successMessage: 'Deleted data object successfully',
      update(cache, {data: {deleteDataObjectItem}}) {
        const normalizedId = cache.identify({
          id: deleteDataObjectItem.id,
          __typename: 'DataObjectSearchResponseItemDataObject',
        });
        cache.evict({id: normalizedId});
        cache.gc();
      },
      onError() {
        handleCloseDeleteDialog();
      },
      onCompleted() {
        handleCloseDeleteDialog();
      },
    },
  );

  const handleCloseDeleteDialog = () => {
    setConfirmDelete(false);

    onClose && onClose();
  };

  const handleDeleteDOItem = () => {
    callDeleteDOItemMutation({
      variables: {
        id: itemId,
      },
    });
  };

  useEffect(() => {
    setConfirmDelete(showDialog);
  }, [showDialog]);

  return (
    <>
      <ConfirmationDialog
        open={!!confirmDelete}
        onClose={handleCloseDeleteDialog}
        title={`Delete data object?`}
        description={[
          `You are about to delete "${itemId}".`,
          'This will remove it from ALL contacts who hold a Relationship to this item.',
          'This cannot be undone. Do you want to proceed?',
        ]}
        actions={[
          {
            label: 'Delete',
            onClick: handleDeleteDOItem,
            startIcon: <DeleteForeverIcon />,
          },
        ]}
      />
      <LoadingIndicator modal loading={deleteDOItemMutation.loading} />
    </>
  );
};

export default DeleteDOItemOption;
