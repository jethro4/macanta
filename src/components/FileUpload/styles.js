import {experimentalStyled as styled} from '@mui/material/styles';
import Box from '@mui/material/Box';

export const Root = styled(Box)`
  .MuiDropzoneArea-root {
    min-height: 100px;
    display: flex;
    justify-content: center;
    align-items: center;

    .MuiTypography-root {
      color: ${({theme}) => theme.palette.grayDarkest.main};
      font-size: 16px;
    }
    .MuiSvgIcon-root {
      color: ${({theme}) => theme.palette.grayDarkest.main};
      font-size: 16px;
    }
  }
`;
