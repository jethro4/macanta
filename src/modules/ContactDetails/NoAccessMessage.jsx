import React from 'react';
import Typography from '@mui/material/Typography';
import * as TableStyled from '@macanta/components/Table/styles';

const NoAccessMessage = (props) => (
  <TableStyled.EmptyMessageContainer {...props}>
    <Typography align="center" color="#888">
      No data access
    </Typography>
  </TableStyled.EmptyMessageContainer>
);

export default NoAccessMessage;
