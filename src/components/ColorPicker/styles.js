import {experimentalStyled as styled} from '@mui/material/styles';
import Box from '@mui/material/Box';

export const Root = styled(Box)`
  display: flex;
  flex-direction: column;
  align-items: center;

  & .react-colorful {
    width: 100%;
    height: 100%;
  }

  & input {
    display: block;
    box-sizing: border-box;
    width: 140px;
    height: 40px;
    padding: 6px;
    border: 0;
    background-color: #f5f6fa;
    outline: none;
    font: inherit;
    text-transform: uppercase;
    text-align: center;
  }
`;

export const ActionContainer = styled(Box)`
  width: 100%;
  margin-top: 32px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const InputContainer = styled(Box)`
  border: 1px solid #ddd;
`;
