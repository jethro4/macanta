import {gql} from '@apollo/client';
import {PERMISSIONS_FRAGMENT} from '@macanta/graphql/permissions';

export default gql`
  query login($appName: String!, $email: String!, $password: String!) {
    login(appName: $appName, email: $email, password: $password) {
      sessionId
      autoLoginKey
      apiKey
      firstName
      lastName
      userId
      signature
      permission {
        isAdmin
        isUser
        ...permissionsFragment
      }
    }
  }
  ${PERMISSIONS_FRAGMENT}
`;
