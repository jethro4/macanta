import React from 'react';
import FormField from '@macanta/containers/FormField';
import BodySection from '@macanta/modules/AdminSettings/UserManagement/BodySection';

const BasicInfoForm = ({
  labelPosition,
  actionLabel,
  renderOtherForms,
  ...props
}) => {
  return (
    <BodySection
      {...props}
      title="Basic Info"
      style={{
        height: 'auto',
        margin: 0,
        ...props.style,
      }}
      bodyStyle={{
        padding: '1rem',
      }}>
      <FormField
        name="name"
        labelPosition={labelPosition}
        required
        label={actionLabel}
        fullWidth
        size="small"
        variant="outlined"
      />

      <FormField
        name="description"
        labelPosition={labelPosition}
        type="TextArea"
        hideExpand
        label="Description"
        fullWidth
        size="small"
        variant="outlined"
      />

      {renderOtherForms?.()}
    </BodySection>
  );
};

export default BasicInfoForm;
