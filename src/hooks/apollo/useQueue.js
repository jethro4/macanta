import {useEffect} from 'react';
import {useReactiveVar} from '@apollo/client';
import useDeepCompareEffect from '@macanta/hooks/base/useDeepCompareEffect';
import usePrevious from '@macanta/hooks/usePrevious';
import {
  apiQueueVar,
  batchAPIQueueVar,
} from '@macanta/graphql/cache/sessionVars';
import {
  arrayConcatOrUpdateByKey,
  update,
  remove,
  hasItem,
} from '@macanta/utils/array';
import {generateQueueId} from '@macanta/utils/graphql';

const useQueue = ({tagFieldName, variables, callsNetwork, limit}) => {
  const queueId = generateQueueId(variables);
  const prevQueueId = usePrevious(queueId);
  const queue = useReactiveVar(apiQueueVar);
  const batchQueue = useReactiveVar(batchAPIQueueVar);

  const addQuery = (id) => {
    if (callsNetwork) {
      const updatedQueue = apiQueueVar();

      apiQueueVar(
        arrayConcatOrUpdateByKey({
          arr: updatedQueue,
          item: {id, tagFieldName, variables, completed: false},
        }),
      );
    }
  };

  const removeQuery = (id) => {
    const updatedQueue = apiQueueVar();

    apiQueueVar(
      remove({
        arr: updatedQueue,
        key: 'id',
        value: id,
      }),
    );
  };

  const completeQuery = (id) => {
    if (callsNetwork) {
      const updatedBatchQueue = batchAPIQueueVar();

      if (updatedBatchQueue.length) {
        batchAPIQueueVar(
          update({
            arr: updatedBatchQueue,
            item: {id, completed: true},
            key: 'id',
            includeAll: true,
          }),
        );
      }
    }
  };

  const handleCompleteQuery = () => {
    completeQuery(queueId);
  };

  useDeepCompareEffect(() => {
    removeQuery(prevQueueId);

    addQuery(queueId);

    return () => {
      removeQuery(queueId);
    };
  }, [callsNetwork, tagFieldName, variables]);

  useEffect(() => {
    const allCompleted =
      !!batchQueue.length && !batchQueue.some((q) => !q.completed);

    if (!batchQueue.length) {
      const nextBatchQueue = queue.filter((q) => !q.completed).slice(0, limit);

      if (nextBatchQueue.some((bq) => bq.id === queueId)) {
        batchAPIQueueVar(nextBatchQueue);
      }
    } else if (allCompleted) {
      const updatedQueue = apiQueueVar();

      const updatedCompleteQueue = queue.filter((q) =>
        batchQueue.some((bq) => bq.id === q.id),
      );

      updatedCompleteQueue.forEach((q) => {
        apiQueueVar(
          update({
            arr: updatedQueue,
            item: {id: q.id, completed: true},
            key: 'id',
            includeAll: true,
          }),
        );
      });

      batchAPIQueueVar([]);
    }
  }, [queue, batchQueue]);

  // const processingQueue = queue.filter((q) => !q.completed).slice(0, limit);
  const completed = hasItem(queue, [`id:${queueId}`, 'completed']);
  // const processing = !completed;
  const pending = queue.some(
    (q) =>
      (q.id === queueId && !batchQueue.length) ||
      !batchQueue.some((bq) => bq.id === queueId),
  );

  return {
    queueId,
    pending,
    // processing,
    completed,
    batchQueue,
    total: queue.length,
    handleCompleteQuery,
  };
};

export default useQueue;
