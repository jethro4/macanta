import React, {useState} from 'react';
import Button from '@macanta/components/Button';
import AddIcon from '@mui/icons-material/Add';
import VisibilityIcon from '@mui/icons-material/Visibility';
import Modal from '@macanta/components/Modal';
import Tooltip from '@macanta/components/Tooltip';
import CustomTabForms from './CustomTabForms';
import * as DataObjectsSearchTableStyled from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/styles';

const AddCustomTabBtn = ({item, onSave}) => {
  const [showForms, setShowForms] = useState(false);

  const handleAddCustomTab = () => {
    setShowForms(true);
  };

  const handleClose = () => {
    setShowForms(false);
  };

  const handleSave = () => {
    handleClose();
    onSave && onSave();
  };

  return (
    <>
      {!item ? (
        <Button
          onClick={handleAddCustomTab}
          size="small"
          variant="contained"
          startIcon={<AddIcon />}>
          Add Custom Tab
        </Button>
      ) : (
        <Tooltip title="Show Details">
          <DataObjectsSearchTableStyled.ActionButton
            show={showForms}
            onClick={handleAddCustomTab}
            size="small">
            <VisibilityIcon />
          </DataObjectsSearchTableStyled.ActionButton>
        </Tooltip>
      )}

      <Modal
        headerTitle={!item ? 'Add Custom Tab' : `Edit Custom Tab (${item.id})`}
        open={showForms}
        onClose={handleClose}
        contentWidth={1400}
        contentHeight={'96%'}>
        <CustomTabForms id={item?.id} title={item?.title} onSave={handleSave} />
      </Modal>
    </>
  );
};

export default AddCustomTabBtn;
