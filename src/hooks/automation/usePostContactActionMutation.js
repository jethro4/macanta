import useMutation from '@macanta/hooks/apollo/useMutation';
import {POST_CONTACT_ACTION} from '@macanta/graphql/automation';

const usePostContactActionMutation = (options) => {
  const mutation = useMutation(POST_CONTACT_ACTION, options);

  return mutation;
};

export default usePostContactActionMutation;
