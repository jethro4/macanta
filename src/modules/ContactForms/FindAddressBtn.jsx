import React, {useState} from 'react';
import Box from '@mui/material/Box';
import Modal from '@macanta/components/Modal';
import FindAddressForms from './FindAddressForms';
import SearchIcon from '@mui/icons-material/Search';
import Button from '@macanta/components/Button';

const FindAddressBtn = ({type, onSelect}) => {
  const [showModal, setShowModal] = useState(false);

  const handleFindAddress = () => {
    setShowModal(true);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  const handleSelect = (...args) => {
    onSelect(...args);

    handleCloseModal();
  };

  return (
    <>
      <Button
        style={{
          marginTop: '1rem',
        }}
        fullWidth
        onClick={handleFindAddress}
        startIcon={<SearchIcon />}>
        Find Address
      </Button>
      <Modal
        headerTitle={`Find ${
          type === 'billing' ? 'Billing' : 'Shipping'
        } Address`}
        open={showModal}
        onClose={handleCloseModal}
        contentWidth="400px"
        contentHeight="500px">
        <Box
          style={{
            display: 'flex',
            flexDirection: 'column',
            height: '100%',
          }}>
          <FindAddressForms type={type} onSelect={handleSelect} />
        </Box>
      </Modal>
    </>
  );
};

export default FindAddressBtn;
