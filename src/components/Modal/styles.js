import {experimentalStyled as styled} from '@mui/material/styles';

import MUIModal from '@mui/material/Modal';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import {IconButton} from '@macanta/components/Button';

export const Root = styled(MUIModal)`
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding: ${({theme}) => `${theme.spacing(2)}`};

  ${({theme}) => theme.breakpoints.up('sm')} {
    padding: ${({theme}) => `${theme.spacing(2)} ${theme.spacing(8)}`};
  }
`;

export const Container = styled(Box)`
  display: flex;
  justify-content: center;
  max-height: 100%;
  pointer-events: none;
`;

export const InnerContainer = styled(Box)`
  display: flex;
  flex-direction: column;
  background-color: ${({theme}) => theme.palette.common.white};
  border-radius: 4px;
  min-width: 320px;
  pointer-events: auto;
  max-height: 100%;
  position: relative;
  overflow: hidden;

  ${({theme}) => theme.breakpoints.up('sm')} {
    min-width: 500px;
  }
`;

export const ModalHeader = styled(Box)`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding-left: ${({theme}) => theme.spacing(2)};
  background-color: ${({theme}) => theme.palette.primary.main};
`;

export const HeaderTitle = styled(Typography)`
  color: ${({theme}) => theme.palette.common.white};
  font-size: 1rem;
  line-height: 1rem;
`;

export const ModalBody = styled(Box)`
  position: relative;
  flex: 1;
  display: flex;
  flex-direction: column;
  overflow: auto;
`;

export const CloseButton = styled(IconButton)`
  padding: 8px;
  margin-right: 8px;
`;
