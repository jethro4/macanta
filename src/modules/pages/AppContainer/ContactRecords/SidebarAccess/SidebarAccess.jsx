import React, {useLayoutEffect} from 'react';
import {useMutation} from '@apollo/client';
import Section from '@macanta/containers/Section';
import {useMatch} from '@reach/router';
import useNotesPermissions from '@macanta/hooks/useNotesPermissions';
import useIsUser from '@macanta/hooks/admin/useIsUser';
import useSidebarTabs from '@macanta/hooks/contact/useSidebarTabs';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import useAppSettings from '@macanta/hooks/admin/useAppSettings';
import {GET_APP_SETTINGS, UPDATE_APP_SETTINGS} from '@macanta/graphql/app';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import SidebarTabs from './SidebarTabs';
import {navigate} from 'gatsby';
export const DEFAULT_SELECTED_TAB = 'notes';

export const getTabLink = (
  contactId,
  id,
  type = DEFAULT_SELECTED_TAB,
  title,
) => {
  let link = '';

  if (type === 'notes') {
    link = `/app/contact/${contactId}/notes`;
  } else if (type === 'data-object') {
    link = `/app/contact/${contactId}/data-object/${id}/${title}`;
  } else if (type === 'custom') {
    link = `/app/contact/${contactId}/custom/${id}/${title}`;
  } else if (type === 'communication') {
    link = `/app/contact/${contactId}/communication`;
  }

  return encodeURI(link);
};

const SidebarAccess = ({contactId}) => {
  /* const navigate = useNavigate();*/

  const {displayMessage} = useProgressAlert();

  const isUser = useIsUser(contactId);
  const notesPermission = useNotesPermissions();
  const {data: tabs, loading: loadingSidebar} = useSidebarTabs({
    hideNotes: !notesPermission.isVisible,
    hideCommunicationHistory: isUser,
  });
  const appSettingsQuery = useAppSettings({
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });
  const [callUpdateAppSettings, updateAppSettingsMutation] = useMutation(
    UPDATE_APP_SETTINGS,
    {
      onError() {},
      onCompleted: async (appSettingsData) => {
        if (appSettingsData?.updateAppSettings?.success) {
          displayMessage('Saved tabs order');
        }
      },
    },
  );

  const notesTabSelected = useMatch(
    encodeURI(getTabLink(contactId, 'notes', 'notes', 'Notes')),
  );

  const handleSort = async (updatedTabTitles) => {
    const {data: appSettingsData} = await callUpdateAppSettings({
      variables: {
        updateAppSettingsInput: {
          type: 'MacantaTabOrder',
          value: JSON.stringify(updatedTabTitles),
        },
        __mutationkey: 'updateAppSettingsInput',
      },
    });

    if (appSettingsData?.updateAppSettings?.success) {
      appSettingsQuery.client.writeQuery({
        query: GET_APP_SETTINGS,
        data: {
          getAppSettings: {
            __typename: 'AppSettings',
            ...appSettingsQuery.data,
            macantaTabOrder: updatedTabTitles,
          },
        },
      });
    }
  };

  useLayoutEffect(() => {
    if (notesTabSelected && !notesPermission.isVisible && tabs[0]) {
      navigate(getTabLink(contactId, tabs[0].id, tabs[0].type, tabs[0].title));
    }
  }, [notesTabSelected, notesPermission.isVisible, tabs[0]]);

  const loading = loadingSidebar || updateAppSettingsMutation.loading;

  return (
    <Section fullHeight loading={loading} title="Access">
      <SidebarTabs data={tabs} contactId={contactId} onSort={handleSort} />
    </Section>
  );
};

export default SidebarAccess;
