import React, {useState} from 'react';
import VisibilityIcon from '@mui/icons-material/Visibility';
import DOItemEditSection from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/DOItemEditSection';
import Tooltip from '@macanta/components/Tooltip';
import Popover from '@macanta/components/Popover';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import * as Styled from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/styles';

const WidgetDOEditPopover = ({
  loading,
  doItem,
  doFields,
  relationships,
  onSave,
}) => {
  const [anchorEl, setAnchorEl] = useState(false);

  const handleViewOtherDetails = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleCloseOtherDetails = () => {
    setAnchorEl(null);
  };

  const handleSave = (...args) => {
    onSave && onSave(...args);
  };

  const openOtherDetails = Boolean(anchorEl);
  const contact = doItem?.connectedContacts[0];

  return (
    <>
      <Tooltip title="Show Details">
        <Styled.ActionButton
          show={openOtherDetails}
          onClick={handleViewOtherDetails}
          size="small">
          <VisibilityIcon />
        </Styled.ActionButton>
      </Tooltip>
      <Popover
        open={openOtherDetails}
        anchorEl={anchorEl}
        onClose={handleCloseOtherDetails}
        anchorOrigin={{
          vertical: 'center',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'center',
          horizontal: 'right',
        }}>
        <div
          style={{
            width: 1600,
            maxWidth: '100%',
            height: '86vh',
          }}>
          {loading ? (
            <LoadingIndicator fill loading={loading} />
          ) : (
            <DOItemEditSection
              loadInfo
              item={doItem}
              contact={contact}
              groupId={doItem?.groupId}
              fields={doFields}
              relationships={relationships}
              onSave={handleSave}
            />
          )}
        </div>
      </Popover>
    </>
  );
};

WidgetDOEditPopover.defaultProps = {
  doFields: [],
  relationships: [],
};

export default WidgetDOEditPopover;
