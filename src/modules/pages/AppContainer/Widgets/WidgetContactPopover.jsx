import React, {useState} from 'react';
import ContactPageIcon from '@mui/icons-material/ContactPage';
import Tooltip from '@macanta/components/Tooltip';
import Popover from '@macanta/components/Popover';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import ContactQuery from '@macanta/modules/hoc/ContactQuery';
import ContactDetails from '@macanta/modules/ContactDetails/ContactDetails';
import DOContactIdContext from '@macanta/modules/DataObjectItems/DOContactIdContext';
import NoteTaskHistory from '@macanta/modules/NoteTaskHistory';
import * as DOSearchTableStyled from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/styles';

const WidgetContactPopover = ({contactId, ...props}) => {
  const [anchorEl, setAnchorEl] = useState(false);

  const handleViewOtherDetails = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleCloseOtherDetails = () => {
    setAnchorEl(null);
  };

  const openOtherDetails = Boolean(anchorEl);

  return (
    <>
      <Tooltip title="Show Contact">
        <DOSearchTableStyled.ActionButton
          show={openOtherDetails}
          onClick={handleViewOtherDetails}
          size="small"
          {...props}>
          <ContactPageIcon />
        </DOSearchTableStyled.ActionButton>
      </Tooltip>
      <Popover
        open={openOtherDetails}
        anchorEl={anchorEl}
        onClose={handleCloseOtherDetails}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}>
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            width: '86vw',
            maxWidth: 1200,
            height: 700,
            backgroundColor: '#f5f6fa',
            paddingBottom: '1rem',
          }}>
          <ContactQuery id={contactId}>
            {({data: updatedContact, loading: contactLoading}) => {
              return updatedContact ? (
                <>
                  <ContactDetails
                    style={{
                      margin: 0,
                    }}
                    contact={updatedContact}
                    loading={contactLoading}
                  />
                  <DOContactIdContext.Provider
                    value={{
                      contactId,
                    }}>
                    <NoteTaskHistory />
                  </DOContactIdContext.Provider>
                </>
              ) : (
                <LoadingIndicator fill />
              );
            }}
          </ContactQuery>
        </div>
      </Popover>
    </>
  );
};

WidgetContactPopover.defaultProps = {
  doFields: [],
  relationships: [],
};

export default WidgetContactPopover;
