<h3>Preview:</h3>

```jsx
import React from 'react';
import FlexGrid from '@macanta/components/Base/FlexGrid';
import FormField from '@macanta/containers/FormField';

<FlexGrid cols={3} margin={['1.5rem', '1.5rem']}>
  <FormField
    required
    type="Text"
    variant="outlined"
    label="Required"
    value="Hello World"
    onChange={(value) => {
      console.info('Value has changed:', value);
    }}
    size="small"
  />
  <FormField
    disabled
    type="Text"
    variant="outlined"
    label="Disabled"
    value="Hello World"
    onChange={(value) => {
      console.info('Value has changed:', value);
    }}
    size="small"
  />
  <FormField
    type="Number"
    variant="outlined"
    label="Whole Number"
    value="100"
    onChange={(value) => {
      console.info('Value has changed:', value);
    }}
    size="small"
  />

  <FormField
    required
    type="Text"
    variant="filled"
    label="Required"
    value="Hello World"
    onChange={(value) => {
      console.info('Value has changed:', value);
    }}
    size="small"
  />
  <FormField
    disabled
    type="Text"
    variant="filled"
    label="Disabled"
    value="Hello World"
    onChange={(value) => {
      console.info('Value has changed:', value);
    }}
    size="small"
  />
  <FormField
    type="Number"
    variant="filled"
    label="Whole Number"
    value="100"
    onChange={(value) => {
      console.info('Value has changed:', value);
    }}
    size="small"
  />

  <FormField
    type="TextArea"
    label="Text Area"
    value=""
    onChange={(value) => {
      console.info('Value has changed:', value);
    }}
    size="small"
  />
  <FormField
    labelPosition="normal"
    type="Text"
    label="Floating label"
    value="Hello World"
    onChange={(value) => {
      console.info('Value has changed:', value);
    }}
    size="small"
  />
</FlexGrid>;
```
