import {gql} from '@apollo/client';
import {USER_ATTRIBUTES} from './fragments';

export default gql`
  query listUsers {
    listUsers {
      items {
        ...userFragment
      }
      count
      total
    }
  }
  ${USER_ATTRIBUTES}
`;
