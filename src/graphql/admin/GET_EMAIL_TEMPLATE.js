import {gql} from '@apollo/client';

export default gql`
  query getEmailTemplate(
    $appName: String!
    $apiKey: String!
    $templateId: String!
  ) {
    getEmailTemplate(
      appName: $appName
      apiKey: $apiKey
      templateId: $templateId
    ) {
      html
    }
  }
`;
