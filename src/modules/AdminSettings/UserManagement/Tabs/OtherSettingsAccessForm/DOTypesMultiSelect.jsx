import React from 'react';
import FormField from '@macanta/containers/FormField';
import useDOTypes from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/useDOTypes';

const DOTypesMultiSelect = ({style, ...props}) => {
  const doTypesQuery = useDOTypes({
    removeEmail: true,
  });

  const doTypes = doTypesQuery.data?.listDataObjectTypes;

  return (
    <FormField
      loading={doTypesQuery.initLoading}
      style={{
        marginBottom: 0,
        width: '100%',
        ...style,
      }}
      labelPosition="normal"
      type="MultiSelect"
      size="small"
      options={doTypes}
      labelKey="title"
      valueKey="id"
      placeholder="Select Types..."
      fullWidth
      variant="outlined"
      {...props}
    />
  );
};

export default DOTypesMultiSelect;
