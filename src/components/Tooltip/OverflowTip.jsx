import React, {useRef, useEffect, useState, useCallback} from 'react';
import throttle from 'lodash/throttle';
import Tooltip from '@macanta/components/Tooltip';
import * as Styled from './styles';
import withKey from '@macanta/hoc/withKey';

let OverflowTip = ({
  style,
  children,
  timeout,
  keyCompare,
  TooltipProps,
  ...props
}) => {
  // Create Ref
  const textElementRef = useRef();

  const compareSize = useCallback(
    throttle(
      () => {
        if (textElementRef.current) {
          const compare =
            textElementRef.current.scrollWidth >
            textElementRef.current.clientWidth;

          setHover(compare);
        }
      },
      timeout,
      {leading: true, trailing: true},
    ),
    [],
  );

  // compare once and add resize listener on "hook dependency value change"
  useEffect(() => {
    compareSize();
  }, [keyCompare]);

  // add resize listener on "componentDidMount"
  useEffect(() => {
    window.addEventListener('resize', compareSize);
  }, []);

  // remove resize listener again on "componentWillUnmount"
  useEffect(
    () => () => {
      window.removeEventListener('resize', compareSize);
    },
    [],
  );

  // Define state and function to update the value
  const [hoverStatus, setHover] = useState(false);

  return (
    <Tooltip
      title={children}
      disableHoverListener={!hoverStatus}
      {...TooltipProps}>
      <Styled.Root
        ref={textElementRef}
        style={{
          whiteSpace: 'nowrap',
          overflow: 'hidden',
          textOverflow: 'ellipsis',
          ...style,
        }}
        {...props}>
        {children}
      </Styled.Root>
    </Tooltip>
  );
};

OverflowTip.defaultProps = {
  timeout: 30,
};

OverflowTip = withKey(OverflowTip, ['children']);

export default OverflowTip;
