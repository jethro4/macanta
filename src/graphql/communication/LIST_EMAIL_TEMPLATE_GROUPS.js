import {gql} from '@apollo/client';

export default gql`
  query listEmailTemplateGroups {
    listEmailTemplateGroups {
      items {
        id
        title
      }
      count
    }
  }
`;
