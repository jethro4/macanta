import {experimentalStyled as styled} from '@mui/material/styles';

import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';

export const Root = styled(Box)`
  flex: 1;
  display: flex;
  flex-direction: column;
`;

export const Container = styled(Box)`
  flex: 1;
  display: flex;
  flex-direction: column;
  padding: ${({theme}) => theme.spacing(2)};
`;

export const Header = styled(Typography)`
  font-size: 1.375rem;
  font-weight: bold;
  margin-bottom: ${({theme}) => theme.spacing(2)};
`;

export const Description = styled(Typography)``;
