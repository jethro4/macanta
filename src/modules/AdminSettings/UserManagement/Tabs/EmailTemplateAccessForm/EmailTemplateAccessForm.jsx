import React from 'react';
import Switch from '@macanta/components/Forms/ChoiceFields/Switch';
import BodySection from '@macanta/modules/AdminSettings/UserManagement/BodySection';
import PermissionsTable from '@macanta/modules/AdminSettings/UserManagement/Tabs/PermissionsTable';
import useEmailTemplateGroups from '@macanta/hooks/communication/useEmailTemplateGroups';
import {insert, remove} from '@macanta/utils/array';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';

const EMAIL_TEMPLATE_ACCESS_COLUMNS = [
  {
    id: 'hasAccess',
    label: '',
    disableSort: true,
    disableResize: true,
    minWidth: 80,
    maxWidth: 80,
    renderBodyCellValue: ({item, value, handlers}) => {
      return (
        <Switch
          color="success"
          checked={value}
          onChange={(event) => {
            handlers.onChangeAccess({
              item,
              value: event.target.checked,
            });
          }}
        />
      );
    },
  },
  {
    id: 'title',
    label: 'Email Template Name',
    maxWidth: '100%',
    getBodyCellStyle: ({item}) =>
      !item.hasAccess && {
        color: 'grayDarkest.main',
      },
  },
];

const EmailTemplateAccessForm = ({permission, onChange, ...props}) => {
  const emailTemplateGroupsQuery = useEmailTemplateGroups(permission, {
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const handleChangeAccess = ({item, value}) => {
    const updatedPermission = {
      ...permission,
      emailTemplates: value
        ? insert({
            arr: permission.emailTemplates,
            item: {
              templateGroupId: item.id,
              name: item.title,
            },
          })
        : remove({
            arr: permission.emailTemplates,
            key: 'templateGroupId',
            value: item.id,
          }),
    };

    onChange(updatedPermission);
  };

  return (
    <BodySection {...props}>
      <PermissionsTable
        loading={emailTemplateGroupsQuery.loading}
        columns={EMAIL_TEMPLATE_ACCESS_COLUMNS}
        data={emailTemplateGroupsQuery.data}
        handlers={{
          onChangeAccess: handleChangeAccess,
        }}
      />
    </BodySection>
  );
};

export default EmailTemplateAccessForm;
