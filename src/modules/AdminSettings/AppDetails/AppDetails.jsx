import React from 'react';
import {navigate} from 'gatsby';
import Breadcrumbs from '@mui/material/Breadcrumbs';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import EditIcon from '@mui/icons-material/Edit';
import OpenInNewIcon from '@mui/icons-material/OpenInNew';
import Typography from '@mui/material/Typography';
import Button, {ModalButton} from '@macanta/components/Button';
import Section from '@macanta/containers/Section';
import {ROOT_PATH_ADMIN} from '@macanta/modules/AdminSettings/AdminSettings';
import SettingCard from '@macanta/modules/UserSettings/SettingCard';
import SettingInfo from '@macanta/modules/UserSettings/SettingInfo';
import useAppSettings from '@macanta/hooks/admin/useAppSettings';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {TIMEZONES_LABEL_MAPPING} from '@macanta/constants/timezones';
import {getAppInfo} from '@macanta/utils/app';
import TimezoneForm from './TimezoneForm';
import EmailFormatForm from './EmailFormatForm';
import * as UserSettingsStyled from '@macanta/modules/UserSettings/styles';
import * as AdminSettingsStyled from '@macanta/modules/AdminSettings/styles';

const QueryBuilder = () => {
  /* const navigate = useNavigate();*/

  const [appName, apiKey] = getAppInfo();

  const appSettingsQuery = useAppSettings({
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const appTimeZone = appSettingsQuery.data?.appTimeZone;
  const generatedEmailFormat = appSettingsQuery.data?.generatedEmailFormat;
  const formattedTimezone = TIMEZONES_LABEL_MAPPING[appTimeZone];

  const handleBack = () => {
    navigate(ROOT_PATH_ADMIN);
  };

  const handleViewAPI = () => {
    window.open(
      encodeURI('https://macanta.org/docs/api/#macanta-introduction'),
    );
  };

  return (
    <Section
      fullHeight
      bodyStyle={{backgroundColor: '#f5f6fa', padding: '0.5rem'}}
      // loading={loading}
      // style={
      //   data.length && {
      //     backgroundColor: '#f5f6fa',
      //   }
      // }
      HeaderLeftComp={
        <Breadcrumbs aria-label="breadcrumb">
          <AdminSettingsStyled.TitleCrumbBtn
            startIcon={
              <ArrowBackIcon
                style={{
                  color: '#333',
                }}
              />
            }
            onClick={handleBack}
            disabled={false}>
            <Typography color="textPrimary">Admin Settings</Typography>
          </AdminSettingsStyled.TitleCrumbBtn>
          <Typography>App Details</Typography>
        </Breadcrumbs>
      }>
      <UserSettingsStyled.GridContainer container>
        <UserSettingsStyled.GridItem item lg={3} md={4} sm={6} xs={12}>
          <SettingCard
            title="General"
            BodyComp={
              <>
                <SettingInfo label="App Name" value={appName} />
                <SettingInfo label="API Key" value={apiKey} />
                <Button
                  style={{
                    marginTop: 16,
                    width: 120,
                  }}
                  variant="contained"
                  onClick={handleViewAPI}
                  startIcon={<OpenInNewIcon />}
                  size="small">
                  View API
                </Button>
              </>
            }
          />
        </UserSettingsStyled.GridItem>
        <UserSettingsStyled.GridItem item lg={3} md={4} sm={6} xs={12}>
          <SettingCard
            title="Settings"
            BodyComp={
              <>
                <SettingInfo
                  style={{
                    paddingRight: 30,
                  }}
                  label="Timezone"
                  value={formattedTimezone}
                  RightComp={
                    <ModalButton
                      icon
                      sx={{
                        padding: '0.25rem',
                        color: 'info.main',
                      }}
                      // onClick={handleAdd}
                      size="small"
                      TooltipProps={{
                        title: 'Edit',
                      }}
                      ModalProps={{
                        headerTitle: 'Edit Timezone',
                        contentWidth: 500,
                        contentHeight: 300,
                      }}
                      renderContent={(handleClose) => (
                        <TimezoneForm onSave={handleClose} />
                      )}>
                      <EditIcon
                        style={{
                          fontSize: 22,
                        }}
                      />
                    </ModalButton>
                  }
                />
              </>
            }
          />
        </UserSettingsStyled.GridItem>
        <UserSettingsStyled.GridItem item lg={3} md={4} sm={6} xs={12}>
          <SettingCard
            title="Email"
            BodyComp={
              <>
                <SettingInfo
                  style={{
                    paddingRight: 30,
                  }}
                  label="Generated Format"
                  value={generatedEmailFormat}
                  RightComp={
                    <ModalButton
                      icon
                      sx={{
                        padding: '0.25rem',
                        color: 'info.main',
                      }}
                      // onClick={handleAdd}
                      size="small"
                      TooltipProps={{
                        title: 'Edit',
                      }}
                      ModalProps={{
                        headerTitle: 'Edit Email Format',
                        contentWidth: 500,
                      }}
                      renderContent={(handleClose) => (
                        <EmailFormatForm onSave={handleClose} />
                      )}>
                      <EditIcon
                        style={{
                          fontSize: 22,
                        }}
                      />
                    </ModalButton>
                  }
                />
              </>
            }
          />
        </UserSettingsStyled.GridItem>
      </UserSettingsStyled.GridContainer>
    </Section>
  );
};

export default QueryBuilder;
