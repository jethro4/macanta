import * as yup from 'yup';

export const widgetValidationSchema = yup.object().shape({
  title: yup.string().required('Required'),
  widgetId: yup.string().required('Required'),
});

export default widgetValidationSchema;
