import {gql} from '@apollo/client';
import {CONTACT_RELATIONSHIP_DIRECT_FRAGMENT} from './fragments';

export default gql`
  query listDirectContactRelationships {
    listDirectContactRelationships {
      ...contactRelationshipDirectFragment
    }
  }
  ${CONTACT_RELATIONSHIP_DIRECT_FRAGMENT}
`;
