import React from 'react';
import useHTML from '@macanta/hooks/useHTML';

const RawHTML = ({children, options}) => {
  const htmlElements = useHTML(children, options);

  if (options?.parentSelector) {
    return null;
  }

  return (
    <div
      key={children}
      className="html-container"
      ref={(nodeElement) => {
        if (
          nodeElement &&
          !nodeElement.childElementCount &&
          htmlElements?.length
        ) {
          htmlElements.forEach((elm) => {
            nodeElement.appendChild(elm);
          });
        }
      }}
    />
  );
};

export default RawHTML;
