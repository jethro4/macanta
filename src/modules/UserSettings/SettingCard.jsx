import React from 'react';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import * as Styled from './styles';

const SettingCard = ({
  title,
  HeaderRightComp,
  BodyComp,
  bodyStyle,
  FooterComp,
  loading,
  ...props
}) => {
  return (
    <Styled.SettingCard {...props}>
      <Styled.Header>
        <Styled.Title>{title}</Styled.Title>
        {HeaderRightComp}
      </Styled.Header>
      <Styled.Body style={bodyStyle}>{BodyComp}</Styled.Body>
      {FooterComp && <Styled.Footer>{FooterComp}</Styled.Footer>}
      <LoadingIndicator fill backdrop loading={loading} />
    </Styled.SettingCard>
  );
};

SettingCard.defaultProps = {
  info: [],
  loading: false,
};

export default SettingCard;
