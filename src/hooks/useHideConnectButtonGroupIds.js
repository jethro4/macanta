import {useMemo} from 'react';
import useQuery from '@macanta/hooks/apollo/useQuery';
import {GET_ACCESS_PERMISSIONS} from '@macanta/graphql/permissions';
import * as Storage from '@macanta/utils/storage';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';

const useHideConnectButtonGroupIds = () => {
  const {email: userEmail} = Storage.getItem('userDetails');

  const {data} = useQuery(GET_ACCESS_PERMISSIONS, {
    variables: {
      email: userEmail,
    },
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
    onError() {},
  });

  const isAdmin = data?.getAccessPermissions?.isAdmin;

  const hideConnectButtonsForGroups =
    data?.getAccessPermissions?.permissionMeta?.hideConnectButtonsForGroups;

  const groupIds = useMemo(
    () =>
      !isAdmin && hideConnectButtonsForGroups?.length
        ? hideConnectButtonsForGroups
        : [],
    [],
  );

  return groupIds;
};

export default useHideConnectButtonGroupIds;
