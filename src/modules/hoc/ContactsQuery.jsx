import useContacts from '@macanta/hooks/contact/useContacts';
import isFunction from 'lodash/isFunction';
import {writeQueryItem} from '@macanta/hooks/contact/useContact';
// import * as Storage from '@macanta/utils/storage';

export const DEFAULT_PAGE = 0;
export const DEFAULT_PAGE_LIMIT = 25;
export const DEFAULT_ORDER = '';
export const DEFAULT_ORDER_BY = '';

const ContactsQuery = ({
  search,
  page = DEFAULT_PAGE,
  limit = DEFAULT_PAGE_LIMIT,
  onCompleted,
  onError,
  options = {},
  children,
  order = DEFAULT_ORDER,
  orderBy = DEFAULT_ORDER_BY,
}) => {
  const variables = {
    q: search,
    page,
    limit,
    order,
    orderBy,
  };

  const contactsQuery = useContacts(variables, {
    onCompleted: (responseData) => {
      if (
        responseData?.listContacts?.items.length &&
        responseData?.listContacts?.items[0].id !== search
      ) {
        responseData?.listContacts?.items.forEach((item) => {
          writeQueryItem(item);
        });
      }

      onCompleted &&
        onCompleted(responseData, {client: contactsQuery.client, variables});
    },
    onError,
    ...options,
  });

  return isFunction(children) ? children(contactsQuery) : null;
};

export default ContactsQuery;
