import {gql} from '@apollo/client';

export const TRIGGER_CONDITIONS_FRAGMENT = gql`
  fragment triggerConditionsFragment on TriggerConditionsItems {
    queryId
    queryType
    queryName
    queryDescription
    queryConnectedDataType
    queryRestartAfter
    queryCDField {
      queryCDFieldLogic
      queryCDFieldName
      queryCDFieldOperator
      queryCDFieldValue
      queryCDFieldValues
    }
    queryContact {
      queryContactRelationship
      queryContactRelationshipFieldLogic
    }
    queryCustomField {
      queryCDFieldLogic
      queryCDFieldName
      queryCDFieldOperator
      queryCDFieldValue
      queryCDFieldValues
    }
    FileAttachedByUser
    FileAttachedByContact
  }
`;

export default gql`
  query getTriggerConditions($id: ID) {
    getTriggerConditions(id: $id) {
      action
      status
      count
      items {
        ...triggerConditionsFragment
      }
    }
  }
  ${TRIGGER_CONDITIONS_FRAGMENT}
`;
