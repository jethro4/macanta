import React from 'react';
import EditIcon from '@mui/icons-material/Edit';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import {OverflowTip} from '@macanta/components/Tooltip';
import {PopoverButton, DialogButton} from '@macanta/components/Button';
import DORelationshipForms, {AUTO_ASSIGN_OPTIONS} from './DORelationshipForms';
import * as ContactDetailsStyled from '@macanta/modules/ContactDetails/styles';

const RelationshipCard = ({item, onDelete}) => {
  let autoAssignValue = [];

  AUTO_ASSIGN_OPTIONS.forEach((o) => {
    if (item[o.value]) {
      autoAssignValue.push(o.label);
    }
  });

  autoAssignValue = autoAssignValue.join(', ');

  return (
    <Box
      style={{
        display: 'flex',
        alignItems: 'center',
        backgroundColor: 'white',
        padding: '4px 1rem',
        borderRadius: 8,
        boxShadow: '#ccc 0px 2px 4px -2px',
        height: '100%',
      }}>
      <OverflowTip
        style={{
          fontSize: 15,
          flexGrow: 0.7,
          flexBasis: 0,
          marginRight: '1rem',
        }}>
        {item.role}
      </OverflowTip>

      <Box
        style={{
          display: 'flex',
          alignItems: 'center',
          flexGrow: 0.8,
          flexBasis: 0,
          marginRight: '1rem',
        }}>
        <ContactDetailsStyled.DetailLabel
          style={{
            fontSize: 15,
          }}>
          Limit:
        </ContactDetailsStyled.DetailLabel>
        <Typography
          style={{
            fontSize: 15,
          }}>
          {item.limit || 'Unlimited'}
        </Typography>
      </Box>

      <Box
        style={{
          display: 'flex',
          alignItems: 'center',
          flexGrow: 1,
          flexBasis: 0,
          marginRight: '1rem',
        }}>
        <ContactDetailsStyled.DetailLabel
          style={{
            fontSize: 15,
          }}>
          Auto-assign:
        </ContactDetailsStyled.DetailLabel>
        <Typography
          style={{
            fontSize: 15,
            ...(!autoAssignValue && {
              color: '#aaa',
            }),
          }}>
          {autoAssignValue || 'N/A'}
        </Typography>
      </Box>

      <Box
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <PopoverButton
          style={{
            padding: 0,
          }}
          icon
          sx={{
            padding: '0.25rem',
            color: 'info.main',
          }}
          // onClick={handleAdd}
          size="small"
          TooltipProps={{
            title: 'Edit',
          }}
          PopoverProps={{
            contentWidth: 380,
            anchorOrigin: {
              vertical: 'top',
              horizontal: 'left',
            },
            transformOrigin: {
              vertical: 20,
              horizontal: 'right',
            },
          }}
          renderContent={(handleClose) => (
            <DORelationshipForms item={item} onClose={handleClose} />
          )}>
          <EditIcon
            style={{
              fontSize: 20,
            }}
          />
        </PopoverButton>
        <DialogButton
          style={{
            padding: 0,
          }}
          icon
          size="small"
          TooltipProps={{title: 'Delete'}}
          DialogProps={{
            title: 'Are you sure?',
            description: [
              `You are about to delete "${item.role}" from Selected Relationships.`,
              'Recheck this relationship to undo. Do you want to proceed?',
            ],
            onActions: (handleClose) => {
              return [
                {
                  label: 'Delete',
                  onClick: () => {
                    onDelete(item.id);
                    handleClose();
                  },
                  startIcon: <DeleteForeverIcon />,
                },
              ];
            },
          }}>
          <DeleteForeverIcon
            style={{
              fontSize: 20,
            }}
            sx={{
              color: 'grayDarker.main',
            }}
          />
        </DialogButton>
      </Box>
    </Box>
  );
};

export default RelationshipCard;
