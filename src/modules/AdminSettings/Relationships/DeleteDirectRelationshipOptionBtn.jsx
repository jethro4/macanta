import React, {useState} from 'react';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import Tooltip from '@macanta/components/Tooltip';
import ConfirmationDialog from '@macanta/containers/ConfirmationDialog';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import {DELETE_DIRECT_RELATIONSHIP_OPTION} from '@macanta/graphql/contacts';
import {useMutation} from '@apollo/client';
import * as DataObjectsSearchTableStyled from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/styles';

const DeleteDirectRelationshipOptionBtn = ({id, name}) => {
  const {displayMessage} = useProgressAlert();

  const [showDeleteDialog, setShowDeleteDialog] = useState(false);

  const [
    callDeleteDirectRelationshipOptionMutation,
    deleteDirectRelationshipOptionMutation,
  ] = useMutation(DELETE_DIRECT_RELATIONSHIP_OPTION, {
    update(cache) {
      const normalizedId = cache.identify({
        id,
        __typename: 'ContactRelationshipDirect',
      });
      cache.evict({id: normalizedId});
      cache.gc();
    },
    onCompleted(data) {
      if (data.deleteDirectRelationshipOption?.success) {
        displayMessage('Deleted relationship successfully');

        handleClose();
      }
    },
    onError() {},
  });

  const handleClose = () => {
    setShowDeleteDialog(false);
  };

  const handleShowDialog = () => {
    setShowDeleteDialog(true);
  };

  const handleDelete = () => {
    callDeleteDirectRelationshipOptionMutation({
      variables: {
        deleteDirectRelationshipOptionInput: {
          relationId: id,
        },
        __mutationkey: 'deleteDirectRelationshipOptionInput',
        removeAppInfo: true,
      },
    });
  };

  return (
    <>
      <Tooltip title="Delete">
        <DataObjectsSearchTableStyled.ActionButton
          style={{
            color: '#aaa',
          }}
          show={showDeleteDialog}
          onClick={handleShowDialog}>
          <DeleteForeverIcon />
        </DataObjectsSearchTableStyled.ActionButton>
      </Tooltip>
      <ConfirmationDialog
        open={!!showDeleteDialog}
        onClose={handleClose}
        title="Are you sure?"
        description={[
          `You are about to delete "${name}" from Contact Relationships.`,
          'This cannot be undone. Do you want to proceed?',
        ]}
        actions={[
          {
            label: 'Delete',
            onClick: handleDelete,
            startIcon: <DeleteForeverIcon />,
          },
        ]}
      />
      <LoadingIndicator
        modal
        loading={deleteDirectRelationshipOptionMutation.loading}
      />
    </>
  );
};

export default DeleteDirectRelationshipOptionBtn;
