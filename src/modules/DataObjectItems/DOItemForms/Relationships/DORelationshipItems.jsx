import React from 'react';
import VisibilityIcon from '@mui/icons-material/Visibility';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import Box from '@mui/material/Box';
import {IconButton, ModalButton} from '@macanta/components/Button';
import * as Styled from './styles';
import NotifDOItemEdit from '@macanta/modules/Notifications/NotifDOItemEdit';
import DORelationshipItemsData from '@macanta/modules/DataObjectItems/DOItemForms/Relationships/DORelationshipItemsData';

const DORelationshipItems = ({items, doTitle, onDelete}) => {
  return items.map((d) => {
    return (
      <Styled.ItemBodyContainer key={d.id} className="item-data">
        <Box
          sx={{
            flex: 1,
            overflowX: 'hidden',
          }}>
          <Styled.ItemId>{d.doItemId}</Styled.ItemId>
          <DORelationshipItemsData data={d.data} />
        </Box>
        <Box
          sx={{
            marginTop: '-4px',
          }}>
          <ModalButton
            icon
            sx={{
              paddingTop: 0,
              paddingBottom: 0,
            }}
            size="small"
            // onClick={handleAdd}
            TooltipProps={{
              title: 'Show Details',
              placement: 'bottom-end',
            }}
            ModalProps={{
              headerTitle: doTitle,
              contentWidth: 1400,
              contentHeight: '80vh',
            }}
            renderContent={(handleClose) => (
              <NotifDOItemEdit itemId={d.doItemId} onSave={handleClose} />
            )}>
            <VisibilityIcon
              sx={{
                color: 'info.main',
              }}
            />
          </ModalButton>
          {!!onDelete && (
            <IconButton
              sx={{
                paddingTop: 0,
                paddingBottom: 0,
              }}
              size="small"
              TooltipProps={{
                title: 'Delete',
              }}
              onClick={() => {
                onDelete({doItemId: d.doItemId, groupId: d.groupId});
              }}>
              <DeleteForeverIcon
                sx={{
                  color: 'grayDarker.main',
                }}
              />
            </IconButton>
          )}
        </Box>
      </Styled.ItemBodyContainer>
    );
  });
};

export default DORelationshipItems;
