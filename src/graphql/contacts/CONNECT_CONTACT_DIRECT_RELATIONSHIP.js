import {gql} from '@apollo/client';

export default gql`
  mutation connectContactDirectRelationship(
    $connectContactDirectRelationshipInput: ConnectContactDirectRelationshipInput!
  ) {
    connectContactDirectRelationship(
      input: $connectContactDirectRelationshipInput
    ) {
      success
    }
  }
`;
