import React from 'react';
import Step from '@mui/material/Step';
import StepLabel from '@mui/material/StepLabel';
import * as Styled from './styles';

const Stepper = ({steps, children, ...props}) => {
  return (
    <Styled.Stepper {...props}>
      {children ||
        steps.map((step) => (
          <Step key={step.value || step.label}>
            <StepLabel>{step.label}</StepLabel>
          </Step>
        ))}
    </Styled.Stepper>
  );
};

Stepper.defaultProps = {
  activeStep: 0,
};

export default Stepper;
