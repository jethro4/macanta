import React from 'react';
import isNil from 'lodash/isNil';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import useRetryQuery from '@macanta/hooks/apollo/useRetryQuery';
import {LIST_COMMUNICATION_HISTORY} from '@macanta/graphql/contacts';

const EmailStats = ({children, emailId, type}) => {
  const emailStatsQuery = useRetryQuery(LIST_COMMUNICATION_HISTORY, {
    variables: {
      emailId,
      stats: true,
    },
    onError() {},
    skip: !emailId,
  });

  const data = emailStatsQuery.data?.listCommunicationHistory?.[type];
  const initLoading = isNil(data) && emailStatsQuery.loading;

  return initLoading ? (
    <LoadingIndicator transparent size={16} />
  ) : (
    children?.({...emailStatsQuery, data: data || 0}) || children || null
  );
};

export default EmailStats;
