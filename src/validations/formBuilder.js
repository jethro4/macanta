import * as yup from 'yup';

const checkRequiredRelationships = function (values) {
  const isContactsOnly = this.parent?.doType === 'contactsOnly';

  if (!isContactsOnly && !values.length) {
    return false;
  }

  return true;
};

const checkIntegrationField = function (value) {
  const isIntegrationForm = this.parent?.type === 'Integrated';

  if (isIntegrationForm) {
    if (!value.conditions?.length) {
      return false;
    }
  }

  return true;
};

const checkGroups = function (values) {
  if (this.path === 'contactGroups' || this.parent?.doType !== 'contactsOnly') {
    if (!values.length) {
      return this.createError({
        message: 'Required at least one group',
      });
    } else {
      const emptyGroups = values.reduce((acc, group) => {
        const accObj = {...acc};
        if (!group.fields.length) {
          accObj[group.id] = true;
        }

        return accObj;
      }, {});

      if (Object.keys(emptyGroups).length) {
        return this.createError({
          path: `${this.path}Empty`,
          message: {
            text: 'Required at least one field',
            groupsMap: emptyGroups,
          },
        });
      } else if (this.path === 'contactGroups') {
        const hasEmailField = values.some((group) =>
          group.fields.some((field) => field.fieldId === 'field_email'),
        );

        if (!hasEmailField) {
          return this.createError({
            path: `${this.path}Empty`,
            message: {
              text: 'Email field should be added',
              groupsMap: {
                [values[0].id]: true,
              },
            },
          });
        }
      } else if (
        this.parent?.type === 'Integrated' &&
        this.path === 'doGroups'
      ) {
        const hasIntegrationField = values.some((group) =>
          group.fields.some((field) => field.fieldId === 'field_integration'),
        );

        if (!hasIntegrationField) {
          return this.createError({
            path: `${this.path}Empty`,
            message: {
              text: 'Integration field should be added',
              groupsMap: {
                [values[0].id]: true,
              },
            },
          });
        }
      }
    }
  }

  return true;
};

const schema = yup.object().shape({
  title: yup.string().required('Required'),
  relationships: yup
    .array()
    .test(
      'checkRequiredRelationships',
      'Required at least one relationship',
      checkRequiredRelationships,
    ),
  submitLabel: yup.string().required('Required'),
  integrationField: yup
    .object()
    .test('checkIntegrationField', 'Required filter/s', checkIntegrationField),
  contactGroups: yup.array().test('contactGroups', '', checkGroups),
  doGroups: yup.array().test('doGroups', '', checkGroups),
});

export default schema;
