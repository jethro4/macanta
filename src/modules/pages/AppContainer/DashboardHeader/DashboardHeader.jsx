import React from 'react';
import Header from '@macanta/containers/Header';
import useMediaQuery from '@mui/material/useMediaQuery';
// import {experimentalStyled as styled} from '@mui/material/styles';
// import Typography from '@mui/material/Typography';
import ContactFormsIconBtn from '@macanta/modules/ContactForms/ContactFormsIconBtn';
import NotificationsIconBtn from '@macanta/modules/Notifications/NotificationsIconBtn';

const DashboardHeader = ({children, onSaveContact, ...props}) => {
  const matches = useMediaQuery('(min-width:400px)');
  // const companyName = 'Macanta';

  const renderDesktopMenu = (
    <>
      <ContactFormsIconBtn onSaveContact={onSaveContact} />
      <NotificationsIconBtn onSaveContact={onSaveContact} />
    </>
  );

  const renderMobileMenu = (
    <>
      <ContactFormsIconBtn mobile onSaveContact={onSaveContact} />
      <NotificationsIconBtn mobile onSaveContact={onSaveContact} />
    </>
  );

  return (
    <>
      <Header
        renderDesktopMenu={renderDesktopMenu}
        renderMobileMenu={renderMobileMenu}
        {...props}>
        {/* {Boolean(companyName) && (
          <Title variant="h6" color="inherit" noWrap>
            {companyName}
          </Title>
        )} */}
        {matches && children}
      </Header>
      {!matches && children}
    </>
  );
};

export default DashboardHeader;
