import React from 'react';
import FormComponents from '@macanta/modules/DevModeContainer/FormComponents';

const DevModeContainer = () => {
  return <FormComponents />;
};

export default DevModeContainer;
