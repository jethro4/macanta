import React from 'react';

const DOContactIdContext = React.createContext({
  contactId: null,
});

export default DOContactIdContext;
