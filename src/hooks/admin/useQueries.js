import useQuery from '@macanta/hooks/apollo/useQuery';
import useValue from '@macanta/hooks/base/useValue';
import {LIST_QUERY_BUILDERS} from '@macanta/graphql/admin';

const useQueries = (filters, options) => {
  const {name, doType} = filters || {};

  const queriesQuery = useQuery(LIST_QUERY_BUILDERS, options);

  const items = queriesQuery.data?.listQueryBuilders;

  const [queryBuilders] = useValue(() => {
    return items?.filter((item) => {
      const namePassed = !name || item.name === name;
      const doTypePassed = !doType || item.doType === doType;

      return namePassed && doTypePassed;
    });
  }, [name, doType, items]);

  return {...queriesQuery, items: queryBuilders};
};

export default useQueries;
