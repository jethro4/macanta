import {
  getCalendarMonthlyDays,
  getCalendarMonthlyItems,
  getPrevMonthItems,
  getCurrentMonthItems,
  getNextMonthItems,
  outputFormat,
  totalItems,
  transformDateLabel,
  getFirstAndLastDates,
  inputFormat,
} from '@macanta/modules/UserSchedules/helpers';
import {getNow, subtract} from '@macanta/utils/time';

describe('Scheduler Helpers', () => {
  beforeEach(() => {
    // Now = Oct 20, 2022 8:01 PM
    Date.now = jest.fn(() => new Date('2022-10-20T20:01:09+08:00'));
  });

  describe('getPrevMonthItems', () => {
    it('get all days for previous month - october', async () => {
      const now = getNow(outputFormat);

      const actualResult = getPrevMonthItems(now, inputFormat);
      const expectedResult = [
        {value: '2022-09-25', dateOfMonth: 25},
        {value: '2022-09-26', dateOfMonth: 26},
        {value: '2022-09-27', dateOfMonth: 27},
        {value: '2022-09-28', dateOfMonth: 28},
        {value: '2022-09-29', dateOfMonth: 29},
        {value: '2022-09-30', dateOfMonth: 30},
      ];

      expect(actualResult).toEqual(expectedResult);
      expect(actualResult).toHaveLength(6);
    });

    it('get all days for previous month - september', async () => {
      const now = subtract(getNow(), 'month');

      const actualResult = getPrevMonthItems(now);
      const expectedResult = [
        {value: '2022-08-28', dateOfMonth: 28},
        {value: '2022-08-29', dateOfMonth: 29},
        {value: '2022-08-30', dateOfMonth: 30},
        {value: '2022-08-31', dateOfMonth: 31},
      ];

      expect(actualResult).toEqual(expectedResult);
      expect(actualResult).toHaveLength(4);
    });
  });

  describe('getCurrentMonthItems', () => {
    it('get all days for current month - october', async () => {
      const now = getNow(outputFormat);

      const actualResult = getCurrentMonthItems(now, inputFormat);
      const expectedResult = [
        {value: '2022-10-01', dateOfMonth: 1},
        {value: '2022-10-02', dateOfMonth: 2},
        {value: '2022-10-03', dateOfMonth: 3},
        {value: '2022-10-04', dateOfMonth: 4},
        {value: '2022-10-05', dateOfMonth: 5},
        {value: '2022-10-06', dateOfMonth: 6},
        {value: '2022-10-07', dateOfMonth: 7},
        {value: '2022-10-08', dateOfMonth: 8},
        {value: '2022-10-09', dateOfMonth: 9},
        {value: '2022-10-10', dateOfMonth: 10},
        {value: '2022-10-11', dateOfMonth: 11},
        {value: '2022-10-12', dateOfMonth: 12},
        {value: '2022-10-13', dateOfMonth: 13},
        {value: '2022-10-14', dateOfMonth: 14},
        {value: '2022-10-15', dateOfMonth: 15},
        {value: '2022-10-16', dateOfMonth: 16},
        {value: '2022-10-17', dateOfMonth: 17},
        {value: '2022-10-18', dateOfMonth: 18},
        {value: '2022-10-19', dateOfMonth: 19},
        {value: '2022-10-20', dateOfMonth: 20},
        {value: '2022-10-21', dateOfMonth: 21},
        {value: '2022-10-22', dateOfMonth: 22},
        {value: '2022-10-23', dateOfMonth: 23},
        {value: '2022-10-24', dateOfMonth: 24},
        {value: '2022-10-25', dateOfMonth: 25},
        {value: '2022-10-26', dateOfMonth: 26},
        {value: '2022-10-27', dateOfMonth: 27},
        {value: '2022-10-28', dateOfMonth: 28},
        {value: '2022-10-29', dateOfMonth: 29},
        {value: '2022-10-30', dateOfMonth: 30},
        {value: '2022-10-31', dateOfMonth: 31},
      ];

      expect(actualResult).toEqual(expectedResult);
      expect(actualResult).toHaveLength(31);
    });

    it('get all days for current month - september', async () => {
      const now = subtract(getNow(), 'month');

      const actualResult = getCurrentMonthItems(now);
      const expectedResult = [
        {value: '2022-09-01', dateOfMonth: 1},
        {value: '2022-09-02', dateOfMonth: 2},
        {value: '2022-09-03', dateOfMonth: 3},
        {value: '2022-09-04', dateOfMonth: 4},
        {value: '2022-09-05', dateOfMonth: 5},
        {value: '2022-09-06', dateOfMonth: 6},
        {value: '2022-09-07', dateOfMonth: 7},
        {value: '2022-09-08', dateOfMonth: 8},
        {value: '2022-09-09', dateOfMonth: 9},
        {value: '2022-09-10', dateOfMonth: 10},
        {value: '2022-09-11', dateOfMonth: 11},
        {value: '2022-09-12', dateOfMonth: 12},
        {value: '2022-09-13', dateOfMonth: 13},
        {value: '2022-09-14', dateOfMonth: 14},
        {value: '2022-09-15', dateOfMonth: 15},
        {value: '2022-09-16', dateOfMonth: 16},
        {value: '2022-09-17', dateOfMonth: 17},
        {value: '2022-09-18', dateOfMonth: 18},
        {value: '2022-09-19', dateOfMonth: 19},
        {value: '2022-09-20', dateOfMonth: 20},
        {value: '2022-09-21', dateOfMonth: 21},
        {value: '2022-09-22', dateOfMonth: 22},
        {value: '2022-09-23', dateOfMonth: 23},
        {value: '2022-09-24', dateOfMonth: 24},
        {value: '2022-09-25', dateOfMonth: 25},
        {value: '2022-09-26', dateOfMonth: 26},
        {value: '2022-09-27', dateOfMonth: 27},
        {value: '2022-09-28', dateOfMonth: 28},
        {value: '2022-09-29', dateOfMonth: 29},
        {value: '2022-09-30', dateOfMonth: 30},
      ];

      expect(actualResult).toEqual(expectedResult);
      expect(actualResult).toHaveLength(30);
    });
  });

  describe('getNextMonthItems', () => {
    it('get all days for next month - october', async () => {
      const now = getNow(outputFormat);

      const actualResult = getNextMonthItems(now, inputFormat);
      const expectedResult = [
        {value: '2022-11-01', dateOfMonth: 1},
        {value: '2022-11-02', dateOfMonth: 2},
        {value: '2022-11-03', dateOfMonth: 3},
        {value: '2022-11-04', dateOfMonth: 4},
        {value: '2022-11-05', dateOfMonth: 5},
      ];

      expect(actualResult).toEqual(expectedResult);
      expect(actualResult).toHaveLength(5);
    });

    it('get all days for next month - september', async () => {
      const now = subtract(getNow(), 'month');

      const actualResult = getNextMonthItems(now);
      const expectedResult = [
        {value: '2022-10-01', dateOfMonth: 1},
        {value: '2022-10-02', dateOfMonth: 2},
        {value: '2022-10-03', dateOfMonth: 3},
        {value: '2022-10-04', dateOfMonth: 4},
        {value: '2022-10-05', dateOfMonth: 5},
        {value: '2022-10-06', dateOfMonth: 6},
        {value: '2022-10-07', dateOfMonth: 7},
        {value: '2022-10-08', dateOfMonth: 8},
      ];

      expect(actualResult).toEqual(expectedResult);
      expect(actualResult).toHaveLength(8);
    });
  });

  describe('getCalendarMonthlyDays', () => {
    it('find total days in current month', async () => {
      const now = getNow(outputFormat);

      const actualResult = getCalendarMonthlyDays(now, inputFormat);
      const expectedResult = 31;

      expect(actualResult).toEqual(expectedResult);
    });
  });

  describe('getCalendarMonthlyItems', () => {
    it('find monthly items by date - october', async () => {
      const now = getNow(outputFormat);

      const actualResult = getCalendarMonthlyItems(now, inputFormat);
      const firstItem = actualResult[0];
      const lastItem = actualResult[totalItems - 1];
      const firstDayItems = actualResult.filter(
        (item) => item.dateOfMonth === 1,
      );

      expect(actualResult).toHaveLength(totalItems);
      expect(firstItem).toEqual({
        value: '2022-09-25',
        dateOfMonth: 25,
        day: 0,
      });
      expect(firstDayItems).toEqual([
        {
          value: '2022-10-01',
          dateOfMonth: 1,
          day: 6,
        },
        {
          value: '2022-11-01',
          dateOfMonth: 1,
          day: 2,
        },
      ]);
      expect(lastItem).toEqual({
        value: '2022-11-05',
        dateOfMonth: 5,
        day: 6,
      });
    });

    it('find monthly items by date - september', async () => {
      const now = subtract(getNow(), 'month');

      const actualResult = getCalendarMonthlyItems(now);
      const firstItem = actualResult[0];
      const lastItem = actualResult[totalItems - 1];
      const firstDayItems = actualResult.filter(
        (item) => item.dateOfMonth === 1,
      );

      expect(actualResult).toHaveLength(totalItems);
      expect(firstItem).toEqual({
        value: '2022-08-28',
        dateOfMonth: 28,
        day: 0,
      });
      expect(firstDayItems).toEqual([
        {
          value: '2022-09-01',
          dateOfMonth: 1,
          day: 4,
        },
        {
          value: '2022-10-01',
          dateOfMonth: 1,
          day: 6,
        },
      ]);
      expect(lastItem).toEqual({
        value: '2022-10-08',
        dateOfMonth: 8,
        day: 6,
      });
    });
  });

  describe('transformDateLabel', () => {
    it('show Month label if date is first day of month', async () => {
      const oct1Label = transformDateLabel({
        value: '2022-10-01',
        dateOfMonth: 1,
      });
      const nov1Label = transformDateLabel({
        value: '2022-11-01',
        dateOfMonth: 1,
      });

      expect(oct1Label).toEqual('Oct 1');
      expect(nov1Label).toEqual('Nov 1');
    });
  });

  describe('getFirstAndLastDates', () => {
    it('get first and last days of month', async () => {
      const nowWithoutFormat = getNow();
      const nowWithFormat = getNow(outputFormat);

      const withoutFormat = getFirstAndLastDates(nowWithoutFormat);
      const withFormat = getFirstAndLastDates(nowWithFormat, inputFormat);

      const expectedResult = ['2022-10-01', '2022-10-31'];

      expect(withoutFormat).toEqual(expectedResult);
      expect(withFormat).toEqual(expectedResult);
    });
  });
});
