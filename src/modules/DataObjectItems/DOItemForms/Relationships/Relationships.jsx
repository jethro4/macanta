import React, {useState, useEffect, useLayoutEffect} from 'react';
import {useParams, useMatch} from '@reach/router';
import AssignmentIndIcon from '@mui/icons-material/AssignmentInd';
import {useTheme} from '@mui/material/styles';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import ConfirmationDialog from '@macanta/containers/ConfirmationDialog';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import Section from '@macanta/containers/Section';
import FormField from '@macanta/containers/FormField';
import {
  sortArrayByKeyCondition,
  sortArrayByPriority,
  mergeAndAddArraysBySameKeys,
  convertArrayToObjectByKeys,
} from '@macanta/utils/array';
import uniq from 'lodash/uniq';
import flatMap from 'lodash/flatMap';
import {
  getContactRelationships,
  getFilteredRelationshipsByLimit,
  getRelationshipOptions,
} from '@macanta/selectors/relationship.selector';
import ConnectAnotherContactFormsBtn from './ConnectAnotherContactFormsBtn';
import Button, {ButtonLink} from '@macanta/components/Button';
import useContactId from '@macanta/hooks/useContactId';
import useIsAdmin from '@macanta/hooks/useIsAdmin';
import useDeepCompareEffect from '@macanta/hooks/base/useDeepCompareEffect';
import * as ListStyled from '@macanta/containers/List/styles';
import * as Styled from './styles';
import * as Storage from '@macanta/utils/storage';

const Relationships = ({
  isEdit,
  connectedContacts: relationshipConnectedContacts,
  relationships: data,
  selectedValueRelationships,
  onChange,
  style,
  loading,
  hasConnectedUser,
  hasOtherUserDOPermission,
  renderFooterComp,
  ...props
}) => {
  const theme = useTheme();

  const {userId: loggedInUserId} = Storage.getItem('userDetails');

  const primaryContactId = useContactId();
  const isAdmin = useIsAdmin();
  const isLoggedInUser = loggedInUserId === primaryContactId;

  const [connectedContacts, setConnectedContacts] = useState([]);
  const [sortedContacts, setSortedContacts] = useState(null);
  const [filteredRelationships, setFilteredRelationships] = useState(null);
  // const [connectedContactValues, setConnectedContactValues] = useState({});
  const [contactToBeDeleted, setContactToBeDeleted] = useState(null);

  const mergedConnectedContacts = mergeAndAddArraysBySameKeys(
    connectedContacts.map((c) => ({
      connectedContactId: c.contactId,
      relationships: c.relationships.map(({role}) => role),
    })),
    selectedValueRelationships,
    {
      keysToCheck: ['connectedContactId'],
      includeAll: true,
    },
  ).filter(({deleted}) => !deleted);
  const connectedContactValues = convertArrayToObjectByKeys(
    mergedConnectedContacts,
    {
      labelKey: 'connectedContactId',
      valueKey: 'relationships',
    },
  );

  const allRelationshipRoles = data?.map((r) => r.role);

  const handleSave = (contact, relationships, options) => {
    onChange(
      {
        connectedContactId: contact.id,
        relationships,
        contact,
      },
      options,
    );
  };

  const handleChange = ({id, firstName, lastName, email}) => (newVal) => {
    const relationships = newVal;

    const updatedConnectedContactValues = {
      ...connectedContactValues,
      [id]: relationships,
    };

    const allUsedRelationships = uniq(
      flatMap(Object.values(updatedConnectedContactValues)),
    );

    const filteredRelationshipsByLimit = getFilteredRelationshipsByLimit(
      allUsedRelationships,
      data,
    );

    setFilteredRelationships(filteredRelationshipsByLimit);

    handleSave({id, firstName, lastName, email}, relationships, {
      disableLoading: true,
    });
  };

  const handleConnectContact = (
    {id, firstName, lastName, email},
    relationships,
  ) => {
    handleSave({id, firstName, lastName, email}, relationships);
    setConnectedContacts((state) => [
      ...state.map((connectedContact) => {
        return {
          ...connectedContact,
          relationships: connectedContactValues[
            connectedContact.contactId
          ].map((role) => ({role})),
        };
      }),
      {
        contactId: id,
        firstName,
        lastName,
        email,
        relationships: relationships.map((role) => ({role})),
      },
    ]);
  };

  const handleDeleteConnectedContact = (connectedContact) => () => {
    setContactToBeDeleted(connectedContact);
  };

  const handleCloseDeleteDialog = () => {
    setContactToBeDeleted(null);
  };

  const handleDeleteContact = (connectedContact) => () => {
    handleSave({id: connectedContact.id}, [], {type: 'delete'});
    setConnectedContacts((state) =>
      state.filter((item) => item.contactId !== connectedContact.id),
    );
    handleCloseDeleteDialog();
  };

  const handleRenderValue = (selected) => {
    const sortedRenderedValue = sortArrayByPriority(
      selected,
      null,
      allRelationshipRoles,
    );

    return sortedRenderedValue.length > 2
      ? `${sortedRenderedValue.length} selected`
      : sortedRenderedValue.join(', ');
  };

  const handleConnectYourself = () => {
    const userDetails = Storage.getItem('userDetails');
    const userAutoAssignedRelationships = data
      .filter((r) => r.autoAssignLoggedInUser)
      .map(({role}) => role);

    handleConnectContact(
      {
        id: userDetails.userId,
        email: userDetails.email,
        firstName: userDetails.firstName,
        lastName: userDetails.lastName,
      },
      userAutoAssignedRelationships,
    );
  };

  useLayoutEffect(() => {
    const mergedConnectedContacts2 = mergeAndAddArraysBySameKeys(
      relationshipConnectedContacts,
      selectedValueRelationships.map((c) => ({
        contactId: c.connectedContactId,
        email: c.email,
        firstName: c.firstName,
        lastName: c.lastName,
        relationships: c.relationships.map((role) => ({role})),
        deleted: c.deleted,
      })),
      {
        keysToCheck: ['contactId'],
        includeAll: true,
      },
    ).filter(({deleted}) => !deleted);

    setConnectedContacts(mergedConnectedContacts2);
  }, [relationshipConnectedContacts, selectedValueRelationships]);

  useEffect(() => {
    setSortedContacts(
      sortArrayByKeyCondition(
        connectedContacts.map(({relationships, ...connectedContact}) => {
          let obj = {...connectedContact, relationships};
          const updatedCValues =
            connectedContactValues[connectedContact.contactId];

          if (updatedCValues) {
            obj = {
              ...obj,
              relationships: relationships.filter((r) =>
                updatedCValues.includes(r.role),
              ),
            };
          }

          return obj;
        }),
        'contactId',
        (value) => value === primaryContactId,
      ),
    );
  }, [connectedContacts, primaryContactId]);

  useDeepCompareEffect(() => {
    if (mergedConnectedContacts && data.length) {
      const contactRelationships = getContactRelationships(
        mergedConnectedContacts.map((c) => ({
          ...c,
          relationships: c.relationships.map((role) => ({role})),
        })),
      );
      const filteredRelationshipsByLimit = getFilteredRelationshipsByLimit(
        contactRelationships,
        data,
      );

      setFilteredRelationships(filteredRelationshipsByLimit);
    }
  }, [mergedConnectedContacts, data]);

  const params = useParams();
  const openContactRecords = useMatch(
    encodeURI(
      `/app/contact/${primaryContactId}/data-object/${params?.groupId}/${params?.doTitle}`,
    ),
  );

  if (!filteredRelationships) {
    return null;
  }

  const isSelfConnected = sortedContacts.some(
    (item) => item.contactId === loggedInUserId,
  );

  const isReadOnly =
    isEdit &&
    !isAdmin &&
    !isLoggedInUser &&
    (loading || (hasConnectedUser && !hasOtherUserDOPermission));

  const connectAnotherUserComp = (
    <Box
      style={{
        padding: '12px 1rem',
        borderTop: '1px solid #eee',
        backgroundColor: 'white',
        width: '100%',
      }}>
      <ConnectAnotherContactFormsBtn
        usersOnly
        btnLabel="Connect Another User"
        headerTitle="Connect Another User"
        size="small"
        selectedContactIds={Object.keys(connectedContactValues)}
        onRenderValue={handleRenderValue}
        relationshipOptions={getRelationshipOptions(
          allRelationshipRoles,
          [],
          filteredRelationships,
        )}
        onSuccess={handleConnectContact}
      />
    </Box>
  );

  return (
    <>
      <Section
        data-testid="DORelationshipsForm"
        loading={loading}
        fullHeight
        style={{
          marginTop: 0,
          ...style,
        }}
        bodyStyle={{
          padding: 0,
          backgroundColor: theme.palette.grayLight.main,
        }}
        title="Contact Relationships"
        HeaderRightComp={
          !isReadOnly && (
            <Box
              style={{
                display: 'flex',
                alignItems: 'center',
              }}>
              {!loading && !isSelfConnected && (
                <Button
                  style={{
                    marginRight: '1rem',
                  }}
                  onClick={handleConnectYourself}
                  size="small"
                  variant="contained"
                  startIcon={<AssignmentIndIcon />}
                  {...props}>
                  Self Connect
                </Button>
              )}
              <ConnectAnotherContactFormsBtn
                loading={loading}
                selectedContactIds={Object.keys(connectedContactValues)}
                onRenderValue={handleRenderValue}
                relationshipOptions={getRelationshipOptions(
                  allRelationshipRoles,
                  [],
                  filteredRelationships,
                )}
                onSuccess={handleConnectContact}
              />
            </Box>
          )
        }
        footerStyle={{
          boxShadow: 'none',
        }}
        FooterComp={
          !renderFooterComp
            ? connectAnotherUserComp
            : renderFooterComp(connectAnotherUserComp)
        }
        {...props}>
        {!loading && sortedContacts.length === 0 && (
          <ListStyled.EmptyMessage align="center" color="#888">
            No connected contacts
          </ListStyled.EmptyMessage>
        )}
        {sortedContacts.map(
          (
            {contactId: connectedContactId, firstName, lastName, email},
            contactIndex,
          ) => {
            const isLastItem = sortedContacts.length - 1 === contactIndex;
            const isPrimaryContact = connectedContactId === primaryContactId;
            const name = `${firstName} ${lastName}`.trim();
            const selectedRelationships =
              connectedContactValues[connectedContactId] || [];
            const relationshipOptions = getRelationshipOptions(
              allRelationshipRoles,
              selectedRelationships,
              filteredRelationships,
            );
            const label = (
              <>
                {isPrimaryContact && openContactRecords ? (
                  name
                ) : (
                  <ButtonLink
                    to={`/app/contact/${connectedContactId}/notes`}
                    style={{
                      color: '#2196f3',
                      padding: 0,
                    }}>
                    {name}
                  </ButtonLink>
                )}
                <Typography
                  variant="caption"
                  style={{display: 'block', color: '#aaa'}}>
                  {email}
                </Typography>
              </>
            );
            const TopRightComp = !isReadOnly && !isPrimaryContact && (
              <Styled.TopRightComp
                aria-haspopup="true"
                TooltipProps={{
                  title: 'Unlink',
                }}
                size="small"
                onClick={handleDeleteConnectedContact({
                  id: connectedContactId,
                  name: `${firstName} ${lastName}`.trim(),
                  email,
                })}>
                <DeleteForeverIcon />
              </Styled.TopRightComp>
            );

            return (
              <Styled.ItemContainer
                key={connectedContactId}
                style={Object.assign(
                  {},
                  contactIndex > 1 && {
                    marginTop: 0,
                  },
                  !isLastItem &&
                    contactIndex >= 1 && {
                      borderBottom: 0,
                    },
                )}>
                {isPrimaryContact && contactIndex === 0 && (
                  <Styled.ItemHeader>Primary Contact</Styled.ItemHeader>
                )}
                {!isPrimaryContact && contactIndex === 1 && (
                  <Styled.ItemHeader>Other Related Contacts</Styled.ItemHeader>
                )}
                <Styled.ItemBody>
                  <FormField
                    style={{
                      marginBottom: 0,
                    }}
                    type="MultiSelect"
                    options={relationshipOptions}
                    renderValue={handleRenderValue}
                    // error={errors[field.name]}
                    onChange={handleChange({
                      id: connectedContactId,
                      firstName,
                      lastName,
                      email,
                    })}
                    label={label}
                    variant="outlined"
                    // placeholder={field.placeholder}
                    // value={values[field.name]}
                    value={selectedRelationships}
                    readOnly={isReadOnly}
                    fullWidth
                    size="small"
                    TopRightComp={TopRightComp}
                    // {...fieldAttrs}
                  />
                </Styled.ItemBody>
              </Styled.ItemContainer>
            );
          },
        )}
      </Section>
      <ConfirmationDialog
        open={!!contactToBeDeleted}
        onClose={handleCloseDeleteDialog}
        title="Are you sure?"
        description={`Remove ${
          loggedInUserId === contactToBeDeleted?.id
            ? 'yourself'
            : contactToBeDeleted?.name
        } as connected contact?`}
        actions={[
          {
            label: 'Delete',
            onClick: handleDeleteContact(contactToBeDeleted),
            startIcon: <DeleteForeverIcon />,
          },
        ]}
      />
    </>
  );
};

Relationships.defaultProps = {
  relationships: [],
  connectedContacts: [],
  selectedValueRelationships: [],
};

export default Relationships;
