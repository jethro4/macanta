import React, {useState, useEffect, useRef} from 'react';
import CloudUploadIcon from '@mui/icons-material/CloudUpload';
import AttachmentsGrid from '@macanta/modules/ContactDetails/AttachmentsGrid';
import {toDataURL} from '@macanta/utils/file';
import * as Styled from './styles';
import * as FormStyled from '@macanta/modules/NoteTaskForms/styles';

const UploadForms = ({
  attachments: attachmentsProp,
  onSubmit,
  uploadLabel,
  uploadButtonProps,
  style,
  containerStyle,
  imageContainerStyle,
  footerStyle,
  filesLimit,
  onChange,
  gridProps,
  hideGrid,
  hideSubmitBtn,
  ...props
}) => {
  const [attachments, setAttachments] = useState(attachmentsProp);
  const thumbnailsRef = useRef([]);

  const handleSubmit = () => {
    onSubmit(attachments);
  };

  const handleChange = async (files) => {
    const filesWithBase64Url = await Promise.all(
      files.map(async (f, index) => {
        const objectUrl = URL.createObjectURL(f);
        const base64Url = await toDataURL(objectUrl);

        return {
          id: String(Date.now()) + index,
          isNew: true,
          thumbnail: base64Url,
          fileName: f.name,
          fileExt: f.name.split('.').pop(),
          isPDF: f.type === 'application/pdf',
          file: f,
        };
      }),
    );

    if (!filesLimit || filesLimit > 1) {
      setAttachments(attachments.concat(filesWithBase64Url));
    } else {
      setAttachments(filesWithBase64Url);
    }

    filesWithBase64Url.forEach((f) => {
      if (!thumbnailsRef.current.includes(f.thumbnail)) {
        thumbnailsRef.current = thumbnailsRef.current.concat(f.thumbnail);
      }
    });

    if (!hideSubmitBtn || filesWithBase64Url.length) {
      onChange && onChange(filesWithBase64Url);
    }
  };

  const handleDelete = (fileId) => async (event) => {
    event.stopPropagation();

    setAttachments(attachments.filter((file) => fileId !== file.id));
  };

  useEffect(() => {
    // free memory when ever this component is unmounted
    return () => {
      thumbnailsRef.current.forEach((thumbnail) => {
        URL.revokeObjectURL(thumbnail);
      });
    };
  }, []);

  useEffect(() => {
    if (attachmentsProp !== attachments) {
      setAttachments(attachmentsProp);
    }
  }, [attachmentsProp]);

  return (
    <Styled.Root style={style}>
      {/* TODO: Remove ErrorBoundary when material-ui-dropzone has base support for Material UI v5 */}
      <Styled.FileUpload
        style={containerStyle}
        attachments={attachments}
        onChange={handleChange}
        filesLimit={filesLimit}
        {...props}
      />

      {!hideGrid && (
        <AttachmentsGrid
          single={filesLimit === 1}
          items={attachments}
          hideDownload
          onDelete={handleDelete}
          imageContainerStyle={imageContainerStyle}
          {...gridProps}
        />
      )}

      {!hideSubmitBtn && (
        <FormStyled.Footer style={footerStyle}>
          <FormStyled.FooterButton
            disabled={attachments.length === 0}
            variant="contained"
            startIcon={<CloudUploadIcon />}
            onClick={handleSubmit}
            size="medium"
            {...uploadButtonProps}>
            {uploadLabel}
          </FormStyled.FooterButton>
        </FormStyled.Footer>
      )}
    </Styled.Root>
  );
};

UploadForms.defaultProps = {
  attachments: [],
  uploadLabel: 'Upload',
};

export default UploadForms;
