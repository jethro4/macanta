import React, {useState} from 'react';
import {NOTE_TAG} from '@macanta/constants/noteTaskTypes';
import Switch from '@macanta/components/Forms/ChoiceFields/Switch';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import {COMPLETE_TASK} from '@macanta/graphql/notesTasks';
import useMutation from '@macanta/hooks/apollo/useMutation';

export const filterNoteTags = (tags) => {
  return tags.filter((v) => !NOTE_TAG.TYPES.includes(v.toLowerCase()));
};

const CompleteTaskForms = ({id, completed, onSuccess, isReadOnly}) => {
  const [isComplete, setIsComplete] = useState(completed);

  const [callCompleteTaskMutation, completeTaskMutation] = useMutation(
    COMPLETE_TASK,
    {
      successMessage: 'Task completed',
      onCompleted({result}) {
        setIsComplete(true);

        onSuccess(result);
      },
    },
  );

  const handleCompleteTask = () => {
    callCompleteTaskMutation({
      variables: {
        id,
      },
    });
  };

  return (
    <>
      <Switch
        color="success"
        checked={isComplete}
        disabled={isComplete || isReadOnly}
        confirmDialog={{
          title: 'Complete task?',
          description: "You won't be able to undo this action.",
          actionLabel: 'Complete',
          onAction: handleCompleteTask,
        }}
      />
      <LoadingIndicator modal loading={completeTaskMutation.loading} />
    </>
  );
};

CompleteTaskForms.defaultProps = {
  onSuccess: () => {},
};

export default CompleteTaskForms;
