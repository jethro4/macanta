import range from 'lodash/range';
import {
  add,
  createDate,
  formatDate,
  getDateOfMonth,
  getDayOfWeek,
  getDaysInMonth,
  getEndOf,
  getStartOf,
  isFuture,
  isToday,
  isWithin,
  subtract,
} from '@macanta/utils/time';
import {zeroPad} from '@macanta/utils/numeric';
import Colors from '@macanta/utils/colors';

export const prefixFormat = 'YYYY-MM';
export const labelFormat = 'MMM D';
export const inputFormat = 'YYYY-MM-DD hh:mmA';
export const outputFormat = 'YYYY-MM-DD hh:mmA';
export const sortedDays = [
  {
    label: 'Sunday',
    short: 'Sun',
    value: 0,
  },
  {
    label: 'Monday',
    short: 'Mon',
    value: 1,
  },
  {
    label: 'Tuesday',
    short: 'Tue',
    value: 2,
  },
  {
    label: 'Wednesday',
    short: 'Wed',
    value: 3,
  },
  {
    label: 'Thursday',
    short: 'Thu',
    value: 4,
  },
  {
    label: 'Friday',
    short: 'Fri',
    value: 5,
  },
  {
    label: 'Saturday',
    short: 'Sat',
    value: 6,
  },
];
export const totalDays = sortedDays.length;
export const totalColumns = totalDays - 1;
export const totalItems = totalDays * totalColumns;

export const getPrevMonthItems = (date, format) => {
  const currentDate = createDate(date, format);

  const startOfMonth = getStartOf(currentDate, 'month');
  const prevMonthLength = getDayOfWeek(startOfMonth);
  const prevMonth = subtract(startOfMonth);
  const lastDateOfPrevMonth = getDateOfMonth(prevMonth);
  const items = generateDailyDates(
    lastDateOfPrevMonth - prevMonthLength + 1,
    lastDateOfPrevMonth,
    prevMonth,
  );

  return items;
};

export const getCurrentMonthItems = (date, format) => {
  const currentDate = createDate(date, format);

  const lastDateOfMonth = getDaysInMonth(currentDate);
  const items = generateDailyDates(1, lastDateOfMonth, currentDate);

  return items;
};

export const getNextMonthItems = (date, format) => {
  const currentDate = createDate(date, format);

  const startOfMonth = getStartOf(currentDate, 'month');
  const endOfMonth = getEndOf(currentDate, 'month');
  const prevMonthLength = getDayOfWeek(startOfMonth);
  const currentMonthLength = getDaysInMonth(currentDate);
  const nextMonthLength = totalItems - currentMonthLength - prevMonthLength;
  const nextMonth = add(endOfMonth);
  const firstDateOfNextMonth = getDateOfMonth(nextMonth);
  const items = generateDailyDates(
    firstDateOfNextMonth,
    firstDateOfNextMonth + nextMonthLength - 1,
    nextMonth,
  );

  return items;
};

export const getCalendarMonthlyDays = (date, format) => {
  const currentDate = createDate(date, format);

  const days = getDaysInMonth(currentDate);

  return days;
};

export const getDatePrefix = (date) => formatDate(date, prefixFormat);

export const getLabelFormat = (date) => formatDate(date, labelFormat);

export const getValue = (datePrefix, day) => `${datePrefix}-${zeroPad(day)}`;

export const generateDailyDates = (start = 1, end, date) => {
  const datePrefix = getDatePrefix(date);

  return range(start, end + 1).reduce((acc, day) => {
    const value = getValue(datePrefix, day);

    return acc.concat({
      value,
      dateOfMonth: day,
    });
  }, []);
};

export const getCalendarMonthlyItems = (date, format) => {
  const currentDate = createDate(date, format);

  const items = [
    ...getPrevMonthItems(currentDate),
    ...getCurrentMonthItems(currentDate),
    ...getNextMonthItems(currentDate),
  ];
  const transformedItems = items.map((item, index) => {
    return {
      value: item.value,
      dateOfMonth: item.dateOfMonth,
      day: sortedDays[index % totalDays].value,
    };
  });

  return transformedItems;
};

export const transformDateLabel = ({value, dateOfMonth}) => {
  const isFirstDay = dateOfMonth === 1;

  return isFirstDay ? getLabelFormat(value) : String(dateOfMonth);
};

export const getFirstAndLastDates = (date, format) => {
  const currentDate = createDate(date, format);

  const lastDateOfMonth = getDaysInMonth(currentDate);
  const datePrefix = getDatePrefix(currentDate);

  const firstDate = getValue(datePrefix, 1);
  const lastDate = getValue(datePrefix, lastDateOfMonth);

  return [firstDate, lastDate];
};

export const isWeekend = (day) => [0, 6].includes(day);

export const SCHEDULE_FILTER_TYPES = [
  {
    label: 'Ongoing',
    value: 'sched_ongoing',
  },
  {
    label: 'Overdue',
    value: 'sched_overdue',
  },
  {
    label: 'Upcoming',
    value: 'sched_upcoming',
  },
];

export const SCHEDULE_STATUS_OPTIONS_MAP = {
  ongoing: {
    color: Colors.success,
    title: 'Ongoing',
  },
  upcoming: {
    color: Colors.info,
    // color: Colors.grayDark,
    title: 'Upcoming',
  },
  overdue: {
    color: Colors.grayDarker,
    title: 'Overdue',
  },
};

export const getStatus = (item) => {
  if (!item) {
    return null;
  }

  const isAllDay = !item.endDateTime;
  let status = 'overdue';

  if (isFuture(item.date)) {
    status = 'upcoming';
  } else {
    const allDayOngoing = isAllDay && isToday(item.date);
    const rangeOngoing =
      !isAllDay &&
      isWithin(item.startDateTime, item.endDateTime, {
        format: 'YYYY-MM-DD hh:mmA',
      });

    if (allDayOngoing || rangeOngoing) {
      status = 'ongoing';
    }
  }

  return status;
};

export const getStatusColor = (item) => {
  const status = getStatus(item);

  return SCHEDULE_STATUS_OPTIONS_MAP[status].color;
};
