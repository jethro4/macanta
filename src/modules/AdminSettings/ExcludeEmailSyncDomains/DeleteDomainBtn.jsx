import React, {useState} from 'react';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import LoadingIndicator from '@macanta/components/LoadingIndicator';
import Tooltip from '@macanta/components/Tooltip';
import ConfirmationDialog from '@macanta/containers/ConfirmationDialog';
import useProgressAlert from '@macanta/hooks/useProgressAlert';
import {GET_APP_SETTINGS, UPDATE_APP_SETTINGS} from '@macanta/graphql/app';
import useAppSettings from '@macanta/hooks/admin/useAppSettings';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import {useMutation} from '@apollo/client';
import * as DataObjectsSearchTableStyled from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/styles';

const DeleteDomainBtn = ({name}) => {
  const {displayMessage} = useProgressAlert();

  const [showDeleteDialog, setShowDeleteDialog] = useState(false);

  const appSettingsQuery = useAppSettings({
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const filteredEmailDomain = appSettingsQuery.data?.filteredEmailDomain;

  const [callUpdateAppSettings, updateAppSettingsMutation] = useMutation(
    UPDATE_APP_SETTINGS,
    {
      onError() {},
      onCompleted: async (data) => {
        if (data?.updateAppSettings?.success) {
          displayMessage('Deleted successfully!');
        }
      },
    },
  );

  const handleClose = () => {
    setShowDeleteDialog(false);
  };

  const handleShowDialog = () => {
    setShowDeleteDialog(true);
  };

  const handleDelete = async () => {
    const domains = filteredEmailDomain.filter((domain) => domain !== name);

    const {data} = await callUpdateAppSettings({
      variables: {
        updateAppSettingsInput: {
          type: 'FilteredEmailDomain',
          value: `[${domains.map((item) => `"${item}"`).join(',')}]`,
        },
        __mutationkey: 'updateAppSettingsInput',
      },
    });

    if (data?.updateAppSettings?.success) {
      appSettingsQuery.client.writeQuery({
        query: GET_APP_SETTINGS,
        data: {
          getAppSettings: {
            __typename: 'AppSettings',
            ...appSettingsQuery.data,
            filteredEmailDomain: domains,
          },
        },
      });

      handleClose();
    }
  };

  return (
    <>
      <Tooltip title="Delete">
        <DataObjectsSearchTableStyled.ActionButton
          style={{
            color: '#aaa',
          }}
          show={showDeleteDialog}
          onClick={handleShowDialog}>
          <DeleteForeverIcon />
        </DataObjectsSearchTableStyled.ActionButton>
      </Tooltip>
      <ConfirmationDialog
        open={!!showDeleteDialog}
        onClose={handleClose}
        title="Are you sure?"
        description={[
          `You are about to delete "${name}" from email sync excluded domains.`,
          'This cannot be undone. Do you want to proceed?',
        ]}
        actions={[
          {
            label: 'Delete',
            onClick: handleDelete,
            startIcon: <DeleteForeverIcon />,
          },
        ]}
      />
      <LoadingIndicator modal loading={updateAppSettingsMutation.loading} />
    </>
  );
};

export default DeleteDomainBtn;
