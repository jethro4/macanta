const {exec} = require('child_process');

const message = process.argv[2];

function gitAddCommitPush() {
  exec(
    `git add . && git commit -m "${message}" && git push`,
    (error, stdout, stderr) => {
      if (error) {
        console.error(`error: ${error.message}`);
        return;
      }
      if (stderr) {
        console.error(`stderr: ${stderr}`);
        return;
      }
      console.info(`stdout: ${stdout}`);
    },
  );
}

gitAddCommitPush();
