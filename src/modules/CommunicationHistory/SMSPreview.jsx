import React, {useState} from 'react';
import Typography from '@mui/material/Typography';
import PreviewIcon from '@mui/icons-material/Preview';
import Tooltip from '@macanta/components/Tooltip';
import Popover from '@macanta/components/Popover';
import * as ContactDetailsStyled from '@macanta/modules/ContactDetails/styles';

const SMSPreview = ({to, message, style}) => {
  const [anchorEl, setAnchorEl] = useState(false);

  const openOtherDetails = Boolean(anchorEl);

  const handleShowSMSPreview = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleCloseSMSPreview = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <Tooltip title="SMS Preview" placement="top">
        <div>
          <ContactDetailsStyled.EmailFooterIconButton
            color="secondary"
            onClick={handleShowSMSPreview}
            size="small"
            style={style}>
            <PreviewIcon />
          </ContactDetailsStyled.EmailFooterIconButton>
        </div>
      </Tooltip>
      <Popover
        title="SMS Preview"
        open={openOtherDetails}
        anchorEl={anchorEl}
        onClose={handleCloseSMSPreview}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}>
        <div
          style={{
            width: 300,
            height: 200,
            display: 'flex',
            flexDirection: 'column',
            padding: '1rem',
          }}>
          <Typography
            style={{
              marginBottom: '1rem',
              color: '#888',
            }}>
            To: {to}
          </Typography>
          <Typography>{message}</Typography>
        </div>
      </Popover>
    </>
  );
};

export default SMSPreview;
