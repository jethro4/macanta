import {trimEndString} from '@macanta/utils/string';

export const getWaitDescription = ({waitFor, unit, time}) => {
  let description = '';

  if (waitFor && unit) {
    const singularUnit = trimEndString(unit, 1);
    const unitLabel = waitFor > 1 ? unit : singularUnit;

    description += `${waitFor} ${unitLabel}`;
  }
  if (time) {
    description += `${description ? ' at ' : ''}${time}`;
  }

  return description;
};
