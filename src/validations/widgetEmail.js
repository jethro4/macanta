import * as yup from 'yup';

const widgetEmailValidationSchema = yup.object().shape({
  widgetIds: yup.array().required('Required').min(1, 'Required at least one'),
  message: yup.string().required('Required'),
});

export default widgetEmailValidationSchema;
