import {experimentalStyled as styled} from '@mui/material/styles';
import Card from '@mui/material/Card';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import {OverflowTip} from '@macanta/components/Tooltip';
import * as ContactDetailsStyled from '@macanta/modules/ContactDetails/styles';
import * as applicationStyles from '@macanta/themes/applicationStyles';

export const SettingCard = styled(Card)`
  height: 248px;
  display: flex;
  flex-direction: column;
  position: relative;
  background-color: ${({theme}) => theme.palette.common.white};

  padding: 0 ${({theme}) => theme.spacing(2)};
`;

export const Header = styled(Box)`
  width: 100%;
  padding: 18px 0 ${({theme}) => theme.spacing(1)};
  display: flex;
  justify-content: space-between;
  align-items: center;
  border-bottom: 2px solid ${({theme}) => theme.palette.primary.main};
`;

export const Body = styled(Box)`
  flex: 1;
  padding-top: 12px;
  overflow: auto;
`;

export const Footer = styled(Box)`
  display: flex;
  justify-content: flex-end;
  padding: 12px 0 12px;
`;

export const Title = styled(Typography)`
  ${applicationStyles.oneLineText}
  font-size: 1.05rem;
`;

export const DetailContainer = styled(Box)`
  display: flex;
  align-items: center;
  margin-bottom: 0.5rem;
  position: relative;
`;

export const DetailLabel = styled(ContactDetailsStyled.DetailLabel)`
  font-size: 0.9375rem;
  white-space: nowrap;
`;

export const DetailValue = styled(OverflowTip)`
  font-size: 0.9375rem;
`;

export const InfoRightCompContainer = styled(Box)`
  position: absolute;
  right: 0;
  top: 42%;
  transform: translateY(-50%);
`;

export const GridContainer = styled(Grid)``;

export const GridItem = styled(Grid)`
  padding: ${({theme}) => theme.spacing(1)};
`;

export const SignaturePreviewContainer = styled(Box)`
  height: 100%;

  line-height: 1.7;
  font-family: Arial, Helvetica, sans-serif;
  font-size: 14px;

  p {
    margin: 0;
  }
`;
