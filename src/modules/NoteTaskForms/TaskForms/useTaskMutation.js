import {useEffect, useState} from 'react';
import {
  CREATE_TASK,
  UPDATE_TASK,
  LIST_TASKS,
} from '@macanta/graphql/notesTasks';
import {useMutation} from '@apollo/client';

const useTaskMutation = ({onSuccess}) => {
  const [callCreateTaskMutation, createTaskMutation] = useMutation(
    CREATE_TASK,
    {
      update(cache, {data: {createTask}}) {
        cache.modify({
          fields: {
            listTasks(listTasksRef) {
              cache.modify({
                id: listTasksRef.__ref,
                fields: {
                  items(existingItems = []) {
                    const newItemRef = cache.writeQuery({
                      data: createTask,
                      query: LIST_TASKS,
                    });

                    return [newItemRef, ...existingItems];
                  },
                },
              });
            },
          },
        });
      },
      onCompleted(data) {
        if (data?.createTask) {
          onSuccess();
        }
      },
      onError() {},
    },
  );
  const [callUpdateTaskMutation, updateTaskMutation] = useMutation(
    UPDATE_TASK,
    {
      onCompleted(data) {
        if (data?.updateTask) {
          onSuccess();
        }
      },
      onError() {},
    },
  );
  const [data, setData] = useState();
  const loading = Boolean(
    createTaskMutation.loading || updateTaskMutation.loading,
  );
  const error = createTaskMutation.error || updateTaskMutation.error;

  useEffect(() => {
    const newTask =
      createTaskMutation.data?.createTask ||
      updateTaskMutation.data?.updateTask;
    if (newTask && !loading) {
      setData(newTask);
    }
  }, [
    createTaskMutation.data?.createTask,
    updateTaskMutation.data?.updateTask,
    loading,
  ]);

  return {callCreateTaskMutation, callUpdateTaskMutation, data, loading, error};
};

export default useTaskMutation;
