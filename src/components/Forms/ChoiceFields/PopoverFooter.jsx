import React from 'react';
import Box from '@mui/material/Box';
import Button from '@macanta/components/Button';

export const FOOTER_HEIGHT = 38;

const PopoverFooter = ({onClear, onDone, ...props}) => {
  const handleStopClickEvent = (event) => {
    event.stopPropagation();
  };

  return (
    <Box
      {...props}
      sx={{
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: FOOTER_HEIGHT,
        padding: '2px 1rem',
        borderTop: '1px solid #eee',
        // boxShadow: '0px -4px 10px -2px #eee',
        // backgroundColor: '#F2F2F2',
        ...props.sx,
      }}
      onClick={handleStopClickEvent}>
      <Box>
        {!!onClear && (
          <Button
            onClick={onClear}
            size="small"
            sx={{
              color: 'grayDarker.main',
              marginLeft: '-10px',
              fontWeight: 'bold',
              fontSize: '14px',
            }}>
            Clear
          </Button>
        )}
      </Box>

      {!!onDone && (
        <Button
          onClick={onDone}
          size="small"
          color="info"
          sx={{
            // color: 'grayDarkest.main',
            marginRight: '-10px',
            fontWeight: 'bold',
            fontSize: '14px',
          }}>
          Done
        </Button>
      )}
    </Box>
  );
};

export default PopoverFooter;
