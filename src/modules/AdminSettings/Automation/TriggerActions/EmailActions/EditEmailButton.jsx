import React from 'react';
import EditIcon from '@mui/icons-material/Edit';
import Box from '@mui/material/Box';
import Button, {PopoverButton} from '@macanta/components/Button';
import FormField from '@macanta/containers/FormField';
import EmailBodyRTE from '@macanta/modules/ContactDetails/EmailBodyRTE';

const EditEmailButton = ({loading, style, PopoverProps, ...props}) => {
  return (
    <PopoverButton
      loading={loading}
      variant="contained"
      startIcon={<EditIcon />}
      size="small"
      style={style}
      PopoverProps={{
        title: 'Edit Email',
        contentWidth: 900,
        contentHeight: 540,
        bodyStyle: {
          padding: '1rem',
          overflowY: 'auto',
          boxSizing: 'content-box',
        },
        anchorOrigin: {
          vertical: 'center',
          horizontal: 'left',
        },
        transformOrigin: {
          vertical: 'center',
          horizontal: 'right',
        },
        ...PopoverProps,
      }}
      renderContent={(handleClose) => (
        <Box
          style={{
            height: '100%',
            display: 'flex',
            flexDirection: 'column',
          }}>
          <Box
            style={{
              flex: 1,
            }}>
            <FormField
              style={{
                height: '100%',
                marginBottom: 0,
              }}
              name="emailHtml"
              labelPosition="normal"
              label="Message"
              renderFieldComponent={(fieldProps) => (
                <EmailBodyRTE
                  types="sender,contact,do_all"
                  placeholder="Type Your Message Here"
                  {...fieldProps}
                />
              )}
            />
          </Box>

          <Box
            style={{
              display: 'flex',
              justifyContent: 'flex-end',
              padding: '1rem 0 0',
              borderTop: '1px solid #eee',
              boxShadow: '0px -4px 10px -2px #eee',
            }}>
            <Button variant="contained" onClick={handleClose} size="medium">
              Done
            </Button>
          </Box>
        </Box>
      )}
      {...props}>
      Edit Email
    </PopoverButton>
  );
};

export default EditEmailButton;
