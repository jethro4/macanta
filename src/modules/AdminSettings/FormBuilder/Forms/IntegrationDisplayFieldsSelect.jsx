import React from 'react';
import useEmailMergeFields from '@macanta/hooks/useEmailMergeFields';
import useDOType from '@macanta/hooks/dataObject/useDOType';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import * as Styled from './styles';

const IntegrationDisplayFieldsSelect = ({groupId, ...props}) => {
  const {data: selectedDOType} = useDOType(groupId, {
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });
  const {doMergeItems} = useEmailMergeFields({
    types: 'do_all',
    options: {
      fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
    },
  });

  const doMergeFields = doMergeItems.find(
    (mergeItem) => mergeItem.category === selectedDOType?.title,
  )?.entries;

  // const handleRenderValue = () => {
  //   return '';
  // };

  return (
    <Styled.SettingsField
      required
      search
      custom
      hideChipValues
      options={doMergeFields}
      actionButtonTitle="Merged Fields"
      // renderValue={handleRenderValue}
      // labelPosition="normal"
      size="small"
      label="Option Label"
      variant="outlined"
      {...props}
    />
  );
};

export default IntegrationDisplayFieldsSelect;
