import React from 'react';
import {TASK_TAG} from '@macanta/constants/noteTaskTypes';
import Typography from '@mui/material/Typography';
import * as HistoryStyled from '@macanta/modules/NoteTaskHistory/styles';
import * as NoteStyled from '@macanta/modules/NoteTaskHistory/NoteItem/styles';
import * as Styled from './styles';
import Tags from '@macanta/modules/NoteTaskHistory/Tags';
import Box from '@mui/material/Box';
import moment from 'moment';
import CompleteTaskForms from './CompleteTaskForms';
import ShowMoreNoteBtn from '@macanta/modules/NoteTaskHistory/NoteItem/ShowMoreNoteBtn';
import useNotesPermissions from '@macanta/hooks/useNotesPermissions';

export const includeDefaultTag = (tags = []) => {
  return tags?.includes(TASK_TAG.DEFAULT) ? tags : [TASK_TAG.DEFAULT, ...tags];
};

const TaskItem = ({item}) => {
  const notesPermission = useNotesPermissions();

  const tags = includeDefaultTag(item.tags);

  return (
    <NoteStyled.Root>
      <NoteStyled.Header>
        <Box>
          <Typography variant="caption">
            {moment(item.creationDate).format('ddd DD MMM YYYY hh:mmA')}
          </Typography>
          <NoteStyled.Title>{item.title}</NoteStyled.Title>
        </Box>
        <Box
          style={{
            display: 'flex',
            flexDirection: 'column',
          }}>
          <Typography
            variant="caption"
            style={{
              color: '#888',
              fontSize: '.68rem',
            }}>
            Due date: {moment(item.actionDate).format('YYYY-MM-DD')}
          </Typography>
          <Box
            style={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'flex-end',
            }}>
            <Typography
              variant="caption"
              style={{
                marginRight: 10,
              }}>
              Done
            </Typography>
            <CompleteTaskForms
              {...item}
              isReadOnly={notesPermission.isReadOnly}
            />
          </Box>
        </Box>
      </NoteStyled.Header>
      <HistoryStyled.Divider />
      {item.note && (
        <NoteStyled.Body>
          <ShowMoreNoteBtn note={item.note} />
        </NoteStyled.Body>
      )}
      <Styled.AssignedContainer>
        <Typography
          variant="caption"
          style={{
            color: '#888',
            fontSize: '.68rem',
          }}>
          Assigned By: {item.assignedBy}
        </Typography>
        <Typography
          variant="caption"
          style={{
            color: '#888',
            fontSize: '.68rem',
          }}>
          Assigned To: {item.assignedTo}
        </Typography>
      </Styled.AssignedContainer>
      <Tags data={tags} />
    </NoteStyled.Root>
  );
};

export default TaskItem;
