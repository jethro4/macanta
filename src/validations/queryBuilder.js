import * as yup from 'yup';

const checkRequiredDOCondition = async function (values) {
  if (
    values.some(
      (v) =>
        !v.name ||
        (!['is null', 'not null'].includes(v.operator) &&
          !v.values?.length &&
          !v.value),
    )
  ) {
    return false;
  }

  return true;
};

const checkRequiredRelationship = async function (values) {
  if (values.length && values.some((v) => !v.relationship)) {
    return false;
  }

  return true;
};

const checkRequiredUserRelationship = async function (values) {
  if (values.length && values.some((v) => !v.relationship || !v.userId)) {
    return false;
  }

  return true;
};

const schema = yup.object().shape({
  name: yup.string().required('Required'),
  doType: yup.string().required('Required'),
  doConditions: yup
    .array()
    .test(
      'checkRequiredDOCondition',
      'Please fill out all condition fields and values',
      checkRequiredDOCondition,
    ),
  contactConditions: yup
    .array()
    .test(
      'checkRequiredContactRelationship',
      'Please fill out all relationship fields',
      checkRequiredRelationship,
    ),
  userConditions: yup
    .array()
    .test(
      'checkRequiredUserRelationship',
      'Please fill out all relationship fields and values',
      checkRequiredUserRelationship,
    ),
  chosenFields: yup
    .array()
    .required('Required')
    .min(1, 'Please select at least one column'),
});

export default schema;
