import React, {useState} from 'react';
import {useFormikContext} from 'formik';
import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';
import Box from '@mui/material/Box';
import Button from '@macanta/components/Button';
import FormField from '@macanta/containers/FormField';
import {arrayConcatOrUpdateByKey} from '@macanta/utils/array';

export const AUTO_ASSIGN_OPTIONS = [
  {
    label: 'Logged-in User',
    value: 'autoAssignLoggedInUser',
  },
  {
    label: 'Contact',
    value: 'autoAssignContact',
  },
];

const DORelationshipForms = ({item: itemProp, onClose}) => {
  const {setValues} = useFormikContext();

  const [item, setItem] = useState({
    autoAssignOptions: [],
    ...itemProp,
  });

  const isEdit = !!itemProp.id;

  const handleChange = (key) => (value) => {
    setItem((state) => ({...state, [key]: value}));
  };

  const handleAdd = () => {
    const parsedLimit = parseInt(item.limit);
    const updatedValue = {
      id: item.id || String(Date.now()),
      role: item.role,
      exclusive: parsedLimit === 1 ? 'yes' : 'no',
      limit: parsedLimit || 0,
      autoAssignLoggedInUser: !!item.autoAssignOptions.includes(
        'autoAssignLoggedInUser',
      ),
      autoAssignContact: !!item.autoAssignOptions.includes('autoAssignContact'),
      autoAssignOptions: item.autoAssignOptions,
      isNew: !itemProp.id || itemProp.isNew,
      updated: true,
    };

    setValues((state) => ({
      ...state,
      relationships: arrayConcatOrUpdateByKey({
        arr: state.relationships,
        item: updatedValue,
      }),
    }));

    onClose();
  };

  return (
    <>
      <Box
        style={{
          padding: '1rem',
        }}>
        <FormField
          // labelPosition="normal"
          type="Number"
          addDivider
          value={item.limit || ''}
          onChange={handleChange('limit')}
          label="Limit - contacts with the same relationship"
          subLabel="Leave blank for unlimited"
          size="small"
          variant="outlined"
        />

        <FormField
          value={item.autoAssignOptions}
          onChange={handleChange('autoAssignOptions')}
          type="Checkbox"
          row
          options={AUTO_ASSIGN_OPTIONS}
          label="Auto-Assign Options"
          fullWidth
          size="small"
        />
      </Box>
      <Box
        style={{
          display: 'flex',
          justifyContent: 'flex-end',
          padding: '12px 1rem',
          borderTop: '1px solid #eee',
        }}>
        <Button
          disabled={!item.role}
          variant="contained"
          startIcon={!isEdit ? <AddIcon /> : <EditIcon />}
          onClick={handleAdd}
          size="medium">
          {!isEdit ? 'Add' : 'Save Details'}
        </Button>
      </Box>
    </>
  );
};

export default DORelationshipForms;
