import React from 'react';
import useScrollOffsets from '@macanta/hooks/base/useScrollOffsets';
import ScrollableAreaContext from './ScrollableAreaContext';
import * as Styled from './styles';

const ScrollableAreaProvider = ({debounceDelay, children, ...props}) => {
  const {x, y, highestY, scrollTargetRef} = useScrollOffsets(debounceDelay);

  return (
    <Styled.Root ref={scrollTargetRef} {...props}>
      <ScrollableAreaContext.Provider
        value={{
          x,
          y,
          highestY,
        }}>
        {children}
      </ScrollableAreaContext.Provider>
    </Styled.Root>
  );
};

export default ScrollableAreaProvider;
