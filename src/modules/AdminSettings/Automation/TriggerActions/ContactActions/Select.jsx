import React from 'react';
import SelectAction from '@macanta/modules/AdminSettings/Automation/TriggerActions/SubForms/SelectAction';
import {useContactActions} from '@macanta/hooks/automation';

const Select = ({...props}) => {
  const contactActionsQuery = useContactActions({
    variables: {
      id: '',
    },
  });

  return (
    <SelectAction
      loading={contactActionsQuery.initLoading}
      type="Select"
      options={contactActionsQuery.result}
      labelKey="name"
      valueKey="id"
      {...props}
    />
  );
};

export default Select;
