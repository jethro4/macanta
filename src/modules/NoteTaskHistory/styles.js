import {experimentalStyled as styled} from '@mui/material/styles';

import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import MUIDivider from '@mui/material/Divider';
import NoteTaskFormsBtnComp from './NoteTaskFormsBtn';
import PaginationComp from '@macanta/components/Pagination';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import TextField from '@macanta/components/Forms';

export const ItemContainer = styled(Box)`
  margin-top: 8px;
  background-color: white;
  border-bottom: 1px solid #e4e4e4;
`;

export const Divider = styled(MUIDivider)`
  background-color: #fafafa;
  margin: 0 ${({theme}) => theme.spacing(2)};
`;

export const NoteTaskFormsBtn = styled(NoteTaskFormsBtnComp)``;

export const FullHeightGrid = styled(Grid)`
  height: 100%;
`;

export const FullHeightFlexGridColumn = styled(Grid)`
  height: 100%;
  display: flex;
  flex-direction: column;
`;

export const Pagination = styled(PaginationComp)``;

export const HeaderRightContainer = styled(Box)`
  display: flex;
  align-items: center;
`;

export const SelectionItem = styled(ListItem)`
  padding: ${({theme}) => theme.spacing(1)};
  white-space: nowrap;

  .MuiSvgIcon-root {
    color: ${({theme}) => theme.palette.primary.main};
    margin-right: ${({theme}) => theme.spacing(1.5)};
  }
`;

export const SelectionItemText = styled(ListItemText)``;

export const HistoryTypeSelect = styled(TextField)`
  width: 100px;
`;
