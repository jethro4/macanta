import React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import RawHTML from '@macanta/components/RawHTML';
import Section from '@macanta/containers/Section';
import useCustomTab from '@macanta/hooks/admin/useCustomTab';
import ExpandBtn from './ExpandBtn';
import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';

const CustomTab = ({id, title: titleParam}) => {
  const title = decodeURIComponent(titleParam);

  const customTabQuery = useCustomTab({id});

  const customTab = customTabQuery.data;

  return (
    <NoteTaskHistoryStyled.FullHeightGrid container>
      <NoteTaskHistoryStyled.FullHeightFlexGridColumn item xs={12}>
        <Section
          fullHeight
          loading={customTabQuery.loading}
          bodyStyle={{
            backgroundColor: '#f5f6fa',
          }}
          HeaderLeftComp={<Typography color="textPrimary">{title}</Typography>}
          HeaderRightComp={
            <ExpandBtn
              title={title}
              content={
                <Box
                  style={{
                    width: '100%',
                    maxWidth: 1200,
                    padding: '1rem',
                  }}>
                  <RawHTML>{customTab?.content}</RawHTML>
                </Box>
              }
            />
          }>
          <Box
            style={{
              width: '100%',
              maxWidth: 1400,
              padding: '1rem',
            }}>
            <RawHTML>{customTab?.content}</RawHTML>
          </Box>
        </Section>
      </NoteTaskHistoryStyled.FullHeightFlexGridColumn>
    </NoteTaskHistoryStyled.FullHeightGrid>
  );
};

export default CustomTab;
