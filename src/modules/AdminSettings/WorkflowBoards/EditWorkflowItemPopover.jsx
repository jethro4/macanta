import React, {useState} from 'react';
import VisibilityIcon from '@mui/icons-material/Visibility';
import Tooltip from '@macanta/components/Tooltip';
import Popover from '@macanta/components/Popover';
import WorkflowBoardsForms from './WorkflowBoardsForms';
import * as DataObjectsSearchTableStyled from '@macanta/modules/UniversalSearch/DataObjectsSearchTable/styles';

const EditWorkflowItemPopover = ({item}) => {
  const [anchorEl, setAnchorEl] = useState(false);

  const handleViewOtherDetails = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleCloseOtherDetails = () => {
    setAnchorEl(null);
  };

  const handleSave = () => {
    // onSave && onSave(...args);
  };

  const openOtherDetails = Boolean(anchorEl);
  // const contact = doItem?.connectedContacts[0];

  return (
    <>
      <Tooltip title="Show">
        <DataObjectsSearchTableStyled.ActionButton
          show={openOtherDetails}
          onClick={handleViewOtherDetails}
          size="small">
          <VisibilityIcon />
        </DataObjectsSearchTableStyled.ActionButton>
      </Tooltip>
      <Popover
        title={`${item.name} (${item.id})`}
        open={openOtherDetails}
        anchorEl={anchorEl}
        onClose={handleCloseOtherDetails}
        anchorOrigin={{
          vertical: 'center',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'center',
          horizontal: 'right',
        }}>
        <div
          style={{
            width: '90vw',
            height: '88vh',
            maxWidth: 1400,
          }}>
          <WorkflowBoardsForms item={item} onSave={handleSave} />
        </div>
      </Popover>
    </>
  );
};

export default EditWorkflowItemPopover;
