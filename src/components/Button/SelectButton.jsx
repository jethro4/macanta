import React from 'react';
import AddIcon from '@mui/icons-material/Add';
import List from '@macanta/containers/List';
import Divider from '@mui/material/Divider';
import * as Styled from '@macanta/components/Button/styles';
import PopoverButton from '@macanta/components/Button/PopoverButton';
import useTransformChoices from '@macanta/hooks/base/useTransformChoices';

const SelectButton = ({
  options: optionsProp,
  onSelect,
  PopoverProps,
  getOptionLabel,
  labelKey,
  valueKey,
  sort,
  ...props
}) => {
  const options = useTransformChoices({
    options: optionsProp,
    sort,
    getOptionLabel,
    labelKey,
    valueKey,
  });

  const renderRow = (item) => {
    return (
      <>
        <Styled.SelectListItem
          button
          onClick={() => {
            onSelect(item);
          }}>
          <AddIcon
            sx={{
              marginRight: '12px',
              color: 'primary.main',
            }}
          />
          <Styled.SelectListItemText
            aria-label={item.label}
            primary={item.label}
          />
        </Styled.SelectListItem>
        <Divider />
      </>
    );
  };

  return (
    <PopoverButton
      size="small"
      variant="contained"
      PopoverProps={{
        contentMinWidth: 180,
        ...PopoverProps,
      }}
      renderContent={(handleClose) => (
        <List data={options} renderRow={renderRow} onClick={handleClose} />
      )}
      {...props}
    />
  );
};

SelectButton.defaultProps = {
  startIcon: <AddIcon />,
};

export default SelectButton;
