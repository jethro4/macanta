import useSelectorQuery from '@macanta/hooks/apollo/useSelectorQuery';
import React from 'react';

const withSelectorQuery = (WrappedComponent, selector, attribute) => {
  if (!attribute) {
    throw `Missing "attribute" is required`;
  }

  const EnhancedComponent = ({
    selectorArgs,
    options,
    itemKey,
    itemValue,
    ...props
  }) => {
    const selectorQuery = useSelectorQuery({
      selector,
      selectorArgs,
      options,
      itemKey,
      itemValue,
    });

    return <WrappedComponent {...{[attribute]: selectorQuery}} {...props} />;
  };

  return EnhancedComponent;
};

export default withSelectorQuery;
