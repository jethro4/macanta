import useQuery from '@macanta/hooks/apollo/useQuery';
import {LIST_CUSTOM_TABS} from '@macanta/graphql/admin';
import * as Storage from '@macanta/utils/storage';

const useCustomTabs = (optionsProp = {}) => {
  const {email: userEmail} = Storage.getItem('userDetails');

  const {removeEmail, ...options} = optionsProp;

  const customTabsQuery = useQuery(LIST_CUSTOM_TABS, {
    variables: {
      ...(!removeEmail && {
        userEmail,
      }),
    },
    ...options,
  });

  const customTabs = customTabsQuery.data?.listCustomTabs;

  return {...customTabsQuery, data: customTabs};
};

export default useCustomTabs;
