import React, {useRef, useState} from 'react';
import isEmpty from 'lodash/isEmpty';
import isFunction from 'lodash/isFunction';
import {makeStyles} from '@mui/material/styles';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Chip from '@mui/material/Chip';
import useValue from '@macanta/hooks/base/useValue';
import useTransformChoices from '@macanta/hooks/base/useTransformChoices';
import {sortArrayByPriority} from '@macanta/utils/array';
import {transformValue} from '@macanta/components/Forms/InputFields/helpers';
import {
  SelectTextField,
  SelectMenuItems,
  getItemHeight,
  getListHeight,
} from '@macanta/components/Forms/ChoiceFields/SelectField';
import PopoverFooter from '@macanta/components/Forms/ChoiceFields/PopoverFooter';

const ALLOW_SEARCH_THRESHOLD = 8;

const useStyles = makeStyles(() => ({
  menuPaperMulti: {
    marginTop: 6,
    maxHeight: (props) => `${props.listHeight}px !important`,
  },
  menuListDefaultMulti: {
    overflowY: 'auto',

    '& > .MuiListItem-root:not(.li-select-search)': {
      height: 38,
    },
  },
  menuListDefaultMultiSubLabel: {
    '& > .MuiListItem-root:not(.li-select-search)': {
      height: 58,
      borderBottom: '1px solid #eee',
    },
  },
}));

const MultiSelectField = ({
  loading,
  children,
  chipped,
  renderValue,
  placeholder,
  MenuProps,
  options: optionsProp,
  getOptionLabel,
  labelKey,
  valueKey,
  subLabelKey,
  captionKey,
  value: valueProp,
  onSearch,
  onChange,
  virtualized,
  sort,
  allowSearch: allowSearchProp,
  searchThreshold = ALLOW_SEARCH_THRESHOLD,
  emptyMessage,
  searchPlaceholder,
  searchValue,
  disableAutoSort,
  showDoneBtn,
  onClear,
  ...props
}) => {
  const childrenArray = React.Children.toArray(children);
  const hasChildren = childrenArray.length > 0;

  const [open, setOpen] = useState(false);

  const hasClickedChildrenRef = useRef(false);

  const [value, setValue] = useValue(() => transformValue(valueProp, true), [
    valueProp,
  ]);

  const options = useTransformChoices({
    options: optionsProp,
    sort,
    getOptionLabel,
    labelKey,
    valueKey,
    subLabelKey,
    captionKey,
  });

  const allowSearch =
    onSearch ||
    (allowSearchProp &&
      options.filter((option) => !option.isSubheader).length > searchThreshold);

  const handleChange = (newVal) => {
    const sortedValue =
      disableAutoSort || !options.length
        ? newVal
        : sortArrayByPriority(
            options.filter((item) => newVal.includes(item.value)),
            'label',
            options.map((item) => item.label),
          ).map((item) => item.value);

    setValue(sortedValue);

    onChange?.(
      sortedValue,
      sortedValue.map((val) => options.find((o) => o.value === val)),
      options,
    );
  };

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleClick = () => {
    if (!hasClickedChildrenRef.current) {
      setOpen(!open);
    } else {
      hasClickedChildrenRef.current = false;

      setOpen(true);
    }
  };

  const handleClear = () => {
    onClear?.(value);
  };

  const handleClearValue = () => {
    setValue([]);

    handleClear();
  };

  const handlePopoverMouseEvent = (event) => {
    event.stopPropagation();
  };

  const handleDeleteMultiItem = (deletedVal) => () => {
    handleChange(value.filter((val) => val !== deletedVal));
  };

  const handleRenderValue = (valToRender) => {
    let displayedText = placeholder || 'Select...';

    if (!isEmpty(valToRender)) {
      if (!renderValue) {
        if (chipped) {
          return (
            <Box sx={{display: 'flex', flexWrap: 'wrap', pb: '4px'}}>
              {valToRender.map((val) => {
                const selectedItem = options.find(
                  (option) => option.value === val,
                );
                return selectedItem ? (
                  <Chip
                    key={val}
                    label={selectedItem.label}
                    sx={{mt: '4px', mr: '4px'}}
                    onClick={handlePopoverMouseEvent}
                    onMouseUp={handlePopoverMouseEvent}
                    onMouseDown={handlePopoverMouseEvent}
                    {...(!props.disabled && {
                      onDelete: handleDeleteMultiItem(selectedItem.value),
                    })}
                  />
                ) : null;
              })}
            </Box>
          );
        }

        const sortedRenderedValue = sortArrayByPriority(
          options.filter((item) => valToRender.includes(item.value)),
          'label',
          options.map((item) => item.label),
        );

        return sortedRenderedValue.length > 2
          ? `${sortedRenderedValue.length} selected`
          : sortedRenderedValue.map((item) => item.label).join(', ');
      } else {
        return renderValue(valToRender);
      }
    } else {
      return (
        <Typography className="select-placeholder">{displayedText}</Typography>
      );
    }
  };

  const withSubLabel = !!subLabelKey;
  const itemHeight = getItemHeight({withSubLabel});
  const listHeight = getListHeight({itemHeight, showDoneBtn});

  const classes = useStyles({listHeight});

  const menuItemsProps = {
    multiple: true,
    popover: true,
    selectedValue: value,
    options,
    withSubLabel,
    allowSearch,
    onSearch,
    onChange: handleChange,
    loading,
    emptyMessage,
    searchPlaceholder,
    searchValue,
    showDoneBtn,
    onClose: handleClose,
    onClear: handleClearValue,
  };

  let popoverComp;

  if (hasChildren) {
    const handleChildrenClickEvent = () => {
      hasClickedChildrenRef.current = true;
    };

    popoverComp = (
      <Box onClick={handleChildrenClickEvent}>
        {isFunction(children) ? children(menuItemsProps) : children}

        {showDoneBtn && (
          <PopoverFooter
            onDone={handleClose}
            onClear={onClear && handleClear}
          />
        )}
      </Box>
    );
  } else {
    popoverComp = <SelectMenuItems {...menuItemsProps} />;
  }

  return (
    <SelectTextField
      {...props}
      multiple
      value={value}
      open={open}
      onClick={handleClick}
      onChange={handleChange}
      NewSelectProps={{
        onOpen: handleOpen,
        onClose: handleClose,
        renderValue: handleRenderValue,
        MenuProps,
        classes: {
          menu: {
            paper: {
              [classes.menuPaperMulti]: true,
            },
            list: {
              [classes.menuListDefaultMulti]: true,
              [classes.menuListDefaultMultiSubLabel]: !!subLabelKey,
            },
          },
        },
        allowSearch: allowSearch,
        virtualized: virtualized,
      }}>
      {popoverComp}
    </SelectTextField>
  );
};

MultiSelectField.defaultProps = {
  allowSearch: true,
  disableAutoSort: false,
  showDoneBtn: true,
};

export default MultiSelectField;
