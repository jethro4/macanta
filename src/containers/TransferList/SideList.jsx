import React from 'react';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Search from './Search';
import MultiSelectItems from '@macanta/components/List/MultiSelectItems';

const SideList = ({options, checked, onToggle, onSearch, emptyMessage}) => {
  return (
    <Box
      style={{
        display: 'flex',
        flexDirection: 'column',
        flexGrow: 1,
        flexBasis: 0,
        overflowX: 'hidden',
        padding: '1rem',
      }}>
      {onSearch && (
        <Search
          style={{
            marginBottom: '4px',
          }}
          size="xsmall"
          onSearch={onSearch}
        />
      )}
      <Paper
        style={{
          flex: 1,
          borderTop: '1px solid #F5F5F5',
        }}>
        <MultiSelectItems
          options={options}
          selectedValue={checked}
          onChange={onToggle}
          emptyMessage={emptyMessage}
          emptyMessageStyle={{
            paddingTop: '2rem',
            paddingBottom: '2rem',
          }}
        />
      </Paper>
    </Box>
  );
};

SideList.defaultProps = {
  emptyMessage: 'No items available',
};

export default SideList;
