import React, {forwardRef} from 'react';
import * as Styled from './styles';

let Paper = (props, ref) => <Styled.Root ref={ref} {...props} />;

Paper = forwardRef(Paper);

export default Paper;
