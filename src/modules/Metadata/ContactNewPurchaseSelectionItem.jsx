import React from 'react';
import Divider from '@mui/material/Divider';
import LocalMallIcon from '@mui/icons-material/LocalMall';
import useContactMetadata from '@macanta/hooks/metadata/useContactMetadata';
import useHidePurchaseButton from '@macanta/hooks/metadata/useHidePurchaseButton';
import {FETCH_POLICIES} from '@macanta/constants/fetchPolicies';
import * as NoteTaskHistoryStyled from '@macanta/modules/NoteTaskHistory/styles';

const ContactNewPurchaseSelectionItem = ({email}) => {
  const {data: contactMetadata} = useContactMetadata({
    email,
    fetchPolicy: FETCH_POLICIES.CACHE_FIRST,
  });

  const newPurchaseData = contactMetadata?.getContactMetadata?.items.find(
    (item) => item.key === 'newPurchase',
  );

  const hidePurchaseButton = useHidePurchaseButton();

  const handleNewPuchase = () => {
    window.open(newPurchaseData.value);
  };

  return !hidePurchaseButton && newPurchaseData ? (
    <>
      <NoteTaskHistoryStyled.SelectionItem button onClick={handleNewPuchase}>
        <LocalMallIcon />
        <NoteTaskHistoryStyled.SelectionItemText primary="New Purchase" />
      </NoteTaskHistoryStyled.SelectionItem>
      <Divider />
    </>
  ) : null;
};

export default ContactNewPurchaseSelectionItem;
