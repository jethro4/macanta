import {gql} from '@apollo/client';
import {
  WIDGET_ATTRIBUTES,
  WIDGET_OPTION_ATTRIBUTES,
  WIDGET_COUNT_FRAGMENT,
} from './fragments';

export default gql`
  query listWidgets($appName: String!, $apiKey: String!, $userId: String!) {
    listWidgets(appName: $appName, apiKey: $apiKey, userId: $userId) {
      addedWidgets {
        ...widgetResults
        ...widgetCountResults
      }
      availableTaskWidgets {
        ...widgetOptionResults
      }
      availableQueryWidgets {
        ...widgetOptionResults
      }
    }
  }
  ${WIDGET_ATTRIBUTES}
  ${WIDGET_OPTION_ATTRIBUTES}
  ${WIDGET_COUNT_FRAGMENT}
`;
