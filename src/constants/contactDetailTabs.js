export const CONTACT_DETAIL_TABS = Object.freeze([
  {
    key: 'quickInfo',
    label: 'Quick Info',
  },
  {
    key: 'defaultFields',
    label: 'Default Fields',
  },
  {
    key: 'userDetails',
    label: 'User Details',
  },
  {
    key: 'contactStatus',
    label: 'Contact Status',
  },
  {
    key: 'tracking',
    label: 'Tracking',
  },
]);
