import {nextFactoryId} from './factory';

const typename = 'QueryBuilder';

export default (overwrites) => {
  const {id, currentIndex} = nextFactoryId(typename);

  return {
    __typename: typename,
    id,
    name: `Query ${currentIndex}`,
    doType: `DataObject${currentIndex}`,
    description: '',
    doConditions: [
      {
        name: 'Query Status',
        logic: '',
        operator: '',
        values: ['New'],
        value: null,
        __typename: 'QueryBuilderDOCondition',
      },
    ],
    contactConditions: [
      {
        relationship: 'Relationship 1',
        logic: '',
        __typename: 'QueryBuilderContactCondition',
      },
    ],
    userConditions: [],
    chosenFields: [
      'FirstName',
      'LastName',
      'Email',
      'Phone1',
      'Query Date',
      'Query Description',
      'Query ID',
    ],
    criteriaMessageA: 'This contact has at least one New Support Ticket',
    criteriaMessageB:
      'This contact does not have at least one New Support Ticket',
    ...overwrites,
  };
};
