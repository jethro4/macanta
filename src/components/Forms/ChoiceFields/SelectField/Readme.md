Preview:

```jsx
import React from 'react';
import FormField from '@macanta/containers/FormField';

const options = [
  {
    label: 'Choice 1',
    value: 'choice1',
  },
  {
    label: 'Choice 2',
    value: 'choice2',
  },
  {
    label: 'Choice 3',
    value: 'choice3',
  },
];

<FormField
  type="Select"
  options={options}
  variant="outlined"
  label="Sample label"
  onChange={(value) => {
    console.info('Value has changed:', value);
  }}
  size="small"
/>;
```
