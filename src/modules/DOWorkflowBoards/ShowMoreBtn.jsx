import React, {useState} from 'react';
import Popover from '@macanta/components/Popover';
import * as NoteItemStyled from '@macanta/modules/NoteTaskHistory/NoteItem/styles';
import * as Styled from './styles';

const ShowMoreBtn = ({data, ...props}) => {
  const [anchorEl, setAnchorEl] = useState(false);

  const openShowMore = Boolean(anchorEl);

  const handleShowMore = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    !!data.length && (
      <>
        <NoteItemStyled.ShowMoreButton
          style={{
            bottom: '5px',
            right: '11px',
            fontSize: 12,
          }}
          onClick={handleShowMore}
          size="small"
          {...props}>
          Show More
        </NoteItemStyled.ShowMoreButton>
        <Popover
          open={openShowMore}
          anchorEl={anchorEl}
          onClose={handleClose}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}>
          <div
            style={{
              maxWidth: 280,
              minWidth: 250,
              maxHeight: 500,
            }}>
            <Styled.ShowMoreContainer>
              {data.map(({name, value}, index) => {
                return (
                  <Styled.FieldContainer key={`${name}-${index}`}>
                    <Styled.FieldName
                      style={{
                        whiteSpace: 'unset',
                        overflow: 'unset',
                        textOverflow: 'unset',
                      }}>
                      {name}
                    </Styled.FieldName>
                    <Styled.FieldValue
                      style={{
                        whiteSpace: 'unset',
                        overflow: 'unset',
                        textOverflow: 'unset',
                        ...(!value && {
                          color: '#aaa',
                        }),
                      }}>
                      {value || 'N/A'}
                    </Styled.FieldValue>
                  </Styled.FieldContainer>
                );
              })}
            </Styled.ShowMoreContainer>
          </div>
        </Popover>
      </>
    )
  );
};

ShowMoreBtn.defaultProps = {
  data: [],
};

export default ShowMoreBtn;
