import React from 'react';
import ActionButtons from './ActionButtons';
import * as Styled from './styles';

const ActionAreaContainer = ({
  children,
  renderActionButtons,
  renderForm,
  hideDelete,
  onDelete,
  DialogProps,
  buttonsContainerStyle,
  hideActionButtons,
  hideBorder,
  editIcon,
  editTitle,
  ...props
}) => {
  let rootComp = children;

  if (!hideActionButtons) {
    rootComp = (
      <>
        {rootComp}
        <Styled.FormActionButtonsContainer style={buttonsContainerStyle}>
          <ActionButtons
            renderActionButtons={renderActionButtons}
            renderForm={renderForm}
            hideDelete={hideDelete}
            onDelete={onDelete}
            DialogProps={DialogProps}
            editIcon={editIcon}
            editTitle={editTitle}
          />
        </Styled.FormActionButtonsContainer>
      </>
    );
  }

  if (!hideBorder) {
    rootComp = (
      <Styled.FormUIContainer {...props}>{rootComp}</Styled.FormUIContainer>
    );
  }

  return rootComp;
};

export default ActionAreaContainer;
